include(WinCC-API.pri)


### TO MAKE IT BUILD CORRECTLY WE NEED QT BUG FIX
### https://bugreports.qt.io/browse/QTBUG-51782
### the fix is in the mkspecs/features/resolve_target.prf
### which we copy also here

TEMPLATE = app

CONFIG -= qt

CONFIG += \
    warn_off \
    thread

INCLUDEPATH += $(DIP_DISTRIBUTION_DIR)/include

win32 {
    CONFIG += console
    LIBS += -lwsock32
    LIBS += -lnetapi32
    LIBS += -lodbc32
    INCLUDEPATH += "."
    INCLUDEPATH += "src"
    DEFINES += WIN32

    QMAKE_LFLAGS += /nodefaultlib:libc
    QMAKE_LFLAGS += /nodefaultlib:libcp

    # This is to have PDB files also on Release target
    QMAKE_CXXFLAGS+=/Zi
    QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug
        
    QMAKE_CXXFLAGS -= -Zc:strictStrings
    QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings
}

unix {
    LIBS += -lpthread

    # This is to separate debug info
    CONFIG += separate_debug_info

    QMAKE_CLEAN += ${TARGET} ${TARGET}.debug
}
