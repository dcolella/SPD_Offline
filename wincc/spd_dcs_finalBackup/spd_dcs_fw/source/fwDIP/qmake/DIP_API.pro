TEMPLATE = app

CONFIG -= qt
CONFIG += \
    warn_off \
    thread

# Points to the root of your DIP API (maven) module
DIP_SRC_DIR = /workspace/dip-git/modules/dip	
    
	
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/Dip.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDataClient.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDataExtractor.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDataInserter.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDataService.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimBrowser.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimClient.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimData.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimFactory.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimPublication.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimServer.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipDimSubscription.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipException.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipSyntaxEnforcer.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipTypeClient.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipTypeService.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/DipVersion.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/Field.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/Options.cpp
SOURCES += $(DIP_SRC_DIR)/src/main/cpp/DimImplementation/Service_DIPDataMapper.cpp

INCLUDEPATH += $(DIP_SRC_DIR)/src/main/include/DimImplementation


LIBS += -L$(DIPDIR)/lib64 -llog4cplus

win32 {
    CONFIG += console
    LIBS += -lwsock32
    LIBS += -lnetapi32
    LIBS += -lodbc32
    DEFINES += WIN32

    QMAKE_LFLAGS += /nodefaultlib:libc
    QMAKE_LFLAGS += /nodefaultlib:libcp

    # This is to have PDB files also on Release target
    QMAKE_CXXFLAGS+=/Zi
    QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug
        
    QMAKE_CXXFLAGS -= -Zc:strictStrings
    QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings

}

unix {
    LIBS += -lpthread

    # This is to separate debug info
    CONFIG += separate_debug_info

    QMAKE_LFLAGS += '-Wl,--exclude-libs,ALL -Wl,-rpath,\'\$$ORIGIN\''
    QMAKE_RPATHDIR += /opt/WinCC_OA/3.14/bin

    QMAKE_CLEAN += ${TARGET} ${TARGET}.debug

}
