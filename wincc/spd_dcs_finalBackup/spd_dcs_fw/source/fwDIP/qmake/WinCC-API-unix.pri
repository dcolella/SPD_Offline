include (WinCC-VersInfo.pri)

PVSS_DLL_APP =

MANAGER_LIBS += -lgssapi_krb5

LIBS += -L$$(API_ROOT)/lib.linux

DEFINES += \
    "DLLEXP_BASICS=" \
    "DLLEXP_CONFIGS=" \
    "DLLEXP_DATAPOINT=" \
    "DLLEXP_MESSAGES=" \
    "DLLEXP_MANAGER=" \
    "DLLEXP_CTRL=" \
    "DLLEXP_BCM=" \
    "PVSS_VERS_DLL=$${PVSS_VERS_DLL}" \
    "PVSS_PLATFORM=\\\"$$(PLATFORM)"\\\"

LIB_ROOT=$$(API_ROOT)/lib.$$(PLATFORM)

LIBS += -L $${LIB_ROOT} \
        -lManager$${PVSS_DLL_VERS} \
        -lMessages$${PVSS_DLL_VERS} \
        -lDatapoint$${PVSS_DLL_VERS} \
        -lConfigs$${PVSS_DLL_VERS} \
        -lBasics$${PVSS_DLL_VERS} \
        -lbcm$${PVSS_DLL_VERS} \
        $${MATHLIB} \
        -lz


ADDVERINFO_SRC = $$(API_ROOT)/addVerInfo.cxx
generateVerInfo.name = Generate addVerInfo.cxx
generateVerInfo.target = addVerInfo.cxx
generateVerInfo.input = ADDVERINFO_SRC
generateVerInfo.commands = cp -f $$(API_ROOT)/addVerInfo.cxx addVerInfo.cxx
generateVerInfo.output = addVerInfo.cxx
generateVerInfo.variable_out = SOURCES
generateVerInfo.depends = $$(API_ROOT)/addVerInfo.cxx

unix:QMAKE_EXTRA_COMPILERS += generateVerInfo
