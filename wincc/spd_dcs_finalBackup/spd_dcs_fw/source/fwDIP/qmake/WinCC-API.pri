INCL_ROOT=$$(API_ROOT)/include

unix: include (WinCC-API-unix.pri)
win32:include (WinCC-API-win32.pri)

INCLUDEPATH += $${INCL_ROOT} \
            $${INCL_ROOT}/winnt/pvssincl \
            $${INCL_ROOT}/Basics/Utilities \
            $${INCL_ROOT}/Basics/Variables \
            $${INCL_ROOT}/Basics/NoPosix \
            $${INCL_ROOT}/Basics/DpBasics \
            $${INCL_ROOT}/BCMNew \
            $${INCL_ROOT}/Configs \
            $${INCL_ROOT}/Configs/DrvConfigs/DrvCommon \
            $${INCL_ROOT}/Configs/DrvConfigs/ConvSmooth \
            $${INCL_ROOT}/Datapoint \
            $${INCL_ROOT}/Messages \
            $${INCL_ROOT}/Manager \
            $${INCL_ROOT}/ComDrv \
            $${INCL_ROOT}/Ctrl
