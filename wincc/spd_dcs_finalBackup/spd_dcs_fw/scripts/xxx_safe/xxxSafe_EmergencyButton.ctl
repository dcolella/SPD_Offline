//Emergency Button
//ALICE xxx - Marco Boccioli
//This script must be used to ramp down the HV 
//and to bring any other device to a safe state
//in case the Emergency button is pressed.

//NOTE: Do not modify the code

#uses "xxx_safe/xxxSafe_GoSafeInstructions.ctl"
#uses "xxx_safe/xxxSafe_GoSuperSafeInstructions.ctl"

string s_dpSafe = "xxx_part_test:xxxEmergencyButton";	
string s_dpGoSafe = "xxx_part_test:xxxEmergencyButton.Actions.goSafe";	
string s_dpIsSafe = "xxx_part_test:xxxEmergencyButton.Actual.isSafe";	
string s_dpIsSafeDelay = "xxx_part_test:xxxEmergencyButton.Settings.isSafeLoopDelaySec";	
string s_dpGotGoSafe = "xxx_part_test:xxxEmergencyButton.Actual.gotGoSafe";	
string s_dpHeartBeat = "xxx_part_test:xxxEmergencyButton.Actual.heartBeat";	

string s_dpGoSuperSafe = "xxx_part_test:xxxEmergencyButton.Actions.goSuperSafe";	
string s_dpIsSuperSafe = "xxx_part_test:xxxEmergencyButton.Actual.isSuperSafe";	
string s_dpGotGoSuperSafe = "xxx_part_test:xxxEmergencyButton.Actual.gotGoSuperSafe";	

main()
{
	int i_try =1;
	int i_maxTries =6;     

      xxxSafe_GoSafeActionsInit(s_dpSafe);//this function can be found in xxxSafe_goSafeInstructions.ctl
	while(i_try <= i_maxTries)
	{
          DebugTN("xxxEmergencyButton.ctl: trying to connect to " + s_dpGoSafe + " (" + i_try + "/" + i_maxTries + ")");
 	    if (dpExists(s_dpGoSafe)) 
          {       
            DebugTN("EmergencyButton: connected to " + s_dpGoSafe);    
      	      dpSet(s_dpGoSafe, false);//set it to false at startup of the script
      	      dpConnect("xxxSafe_ConnectGoSafe",s_dpGoSafe);//check the status of the DP.            
      	      dpSet(s_dpGoSuperSafe, false);//set it to false at startup of the script
      	      dpConnect("xxxSafe_ConnectGoSuperSafe",s_dpGoSuperSafe);//check the status of the DP.            
            break;
          }
          i_try++;                
          if(i_try > i_maxTries) DebugTN("EmergencyButton: ERROR: could not connect to " + s_dpGoSafe);    
          delay(10);  
        }

	timedFunc("xxxSafe_ConnectIsSafe", "_xxxSafe_isSafeCheck");

	while(true)
	{
		bool b_heartBeat;
		dpGet(s_dpHeartBeat, b_heartBeat);
		dpSet(s_dpHeartBeat,! b_heartBeat);
		delay(2);
	}
}

void xxxSafe_ConnectGoSafe(string dp, int goSafe)
{
	xxxSafe_GoSafe();
}


void xxxSafe_ConnectIsSafe(string dpTF,time t1, time t2)
{
	dpSet(s_dpIsSafe, xxxSafe_IsSafeActions());//this function can be found in xxxSafe_goSafeInstructions.ctl
	dpSet(s_dpIsSuperSafe, xxxSafe_IsSuperSafeActions());//this function can be found in xxxSafe_goSuperSafeInstructions.ctl
}


int xxxSafe_GoSafe()
{
	bool b_goSafe;	
	int i_ret;
	dpGet(s_dpGoSafe, b_goSafe);
	if(!b_goSafe) return 1;
	dpSet(s_dpGotGoSafe,1);
	i_ret = xxxSafe_GoSafeActions();//this function can be found in xxxSafe_goSafeInstructions.ctl
	delay(1);
	if(i_ret == 0) dpSet(s_dpGotGoSafe,0);
      DebugTN("xxxEmergencyButton.ctl: got Go Safe command.");
	return i_ret;
}


int xxxSafe_GoSuperSafe()
{
	bool b_goSuperSafe;	
	int i_ret;
	dpGet(s_dpGoSuperSafe, b_goSuperSafe);
	if(!b_goSuperSafe) return 1;
	dpSet(s_dpGotGoSuperSafe,1);
	i_ret = xxxSafe_GoSuperSafeActions();//this function can be found in xxxSafe_goSuperSafeInstructions.ctl
	delay(1);
	if(i_ret == 0) dpSet(s_dpGotGoSuperSafe,0);
      DebugTN("xxxEmergencyButton.ctl: got Go SuperSafe command.");
	return i_ret;
}


void xxxSafe_ConnectGoSuperSafe(string dp, int goSafe)
{
	xxxSafe_GoSuperSafe();
}


