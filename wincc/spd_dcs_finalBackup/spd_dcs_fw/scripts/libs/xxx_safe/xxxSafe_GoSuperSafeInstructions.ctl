bool xxxSafe_IsSuperSafeActions()
{
  bool b_isSuperSafe;//use this variable to set superSafe/not superSafe
////////////////////////////////////////////////////
//write here the code to check if your detector is in safe state.
//BEGIN OF YOUR CODE
  b_isSuperSafe = false;//use this variable to set safe/not safe
  DebugTN("xxxEmergencyButton.ctl: IsSuperSafe condition to be implemented!!");
  delay(1);//a delay just as example: remove it.

//END OF YOUR CODE
////////////////////////////////////////////////////

//do not delete this line:
	return b_isSuperSafe;
}

int xxxSafe_GoSuperSafeActions()
{
////////////////////////////////////////////////////
//write here the code to bring your detector to super safe state.
//BEGIN OF YOUR CODE
  DebugN("xxxEmergencyButton.ctl: SuperSafe Button Pressed!!!!!!!!!");
  delay(5);//a delay just as example: remove it.

//END OF YOUR CODE
////////////////////////////////////////////////////

//do not delete this line:
	return 0;
}
