main()
{
  string sDistributedControlDpName;
  string sSystemName, sHostName, sPortNumber;
  int iRes;
  dyn_string exceptionInfo;

// get the local system
  sSystemName = getSystemName();

  sDistributedControlDpName = "_unDistributedControl_"+substr(sSystemName, 0, strpos(sSystemName, ":"));
  
// create distributedControl dp for the local system.
  DebugN("unicos DistributedControl component: creating internal data point");
  if(!dpExists(sDistributedControlDpName)) {
    dpCreate(sDistributedControlDpName, "_UnDistributedControl");
    if(!dpExists(sDistributedControlDpName)) {
      DebugN("********** ERROR: unable to create: "+sDistributedControlDpName);
      DebugN("********** Contact UNICOS support team");
      fwException_raise(exceptionInfo, "ERROR", "unDistributedControl.postInstall: unable to create: "+sDistributedControlDpName, "");
    }
    else {
      unDistributedControl_convertHostPort(sHostName, sPortNumber);
      unDistributedControl_setDeviceConfig(getSystemName(), getSystemId(), sHostName, sPortNumber, exceptionInfo);
      if(dynlen(exceptionInfo) > 0) {
        DebugN("********** ERROR: unable to set: "+sDistributedControlDpName);
        DebugN("********** Contact UNICOS support team");
        fwException_raise(exceptionInfo, "ERROR", "unDistributedControl.postInstall: unable to set: "+sDistributedControlDpName, "");
      }
      else
        DebugN("unicos DistributedControl component: sucessfully installed");
    }
  }
  else
    DebugN("unicos DistributedControl component: sucessfully installed");

  iRes = unDistributedControl_postInstallUpdate();
  
  if(dynlen(exceptionInfo) > 0)
  {
    DebugTN(exceptionInfo);
    DebugTN("********** ERROR during System configuration: Contact UNICOS support team");
    if(isFunctionDefined("fwInstallation_setComponentInstallationStatus"))
      fwInstallation_setComponentInstallationStatus("unDistributedControl", false);
  }
  else {
    DebugTN("INFO:unDistributedControl configuration sucessfully done");
    if(isFunctionDefined("fwInstallation_setComponentInstallationStatus"))
      fwInstallation_setComponentInstallationStatus("unDistributedControl", true);
  }
}

int unDistributedControl_postInstallUpdate()
{
  dyn_string dsSystemName, dsHostName, exceptionInfo;
  dyn_int diSystemId, diPortNumber;

  dyn_string dsFileHostName, dsFileReduHostName;
  dyn_int diFileSystemId, diFilePortNumber;

  int i, len;
  string sPortNumber;
  
  fwInstallation_throw("unDistributedControl.postInstall: checking config", "INFO", 10);
  unDistributedControl_getAllDeviceConfig(dsSystemName, diSystemId, dsHostName, diPortNumber);
  dyn_int uniqueDiSystemId = diSystemId;
  
  dynUnique(uniqueDiSystemId);
  if(dynlen(uniqueDiSystemId) != dynlen(diSystemId))
  {
    fwInstallation_throw("ERROR: unDistributedControl ERROR -> Various system have the same number. Please, correct this before proceeding...", "WARNING", 10);
  }
  
  dynUnique(diFileSystemId);
  dynSortAsc(diSystemId);
  dynSortAsc(diFileSystemId);
  
  len = dynlen(dsSystemName);
  for(i=1;i<=len;i++) {
    if(diPortNumber[i] == 0)
      sPortNumber = "";
    else
      sPortNumber = diPortNumber[i];
    unDistributedControl_convertHostPort(dsHostName[i], sPortNumber);
    unDistributedControl_setDeviceConfig(dsSystemName[i], diSystemId[i], dsHostName[i], sPortNumber, exceptionInfo);
  }
  
  //if it is not a UNICOS project, skip updating the config file of the project
  string version = "";
  bool unicos = fwInstallation_isComponentInstalled("unCore", version);
  if(unicos){
    fwInstallation_throw("unDistributedControl.postInstall: updating config file", "INFO", 10);  
    unDistributedControl_addToConfigFile(exceptionInfo);
    if(dynlen(exceptionInfo) > 0){
      fwInstallation_throw("unDistributedControl.postInstall: failed to update the config file", "WARNING", 10);
    }
  }
  //Verify that the project config file and the internal dps are in sync
  unDistributedControl_getAllDeviceConfig(dsSystemName, diSystemId, dsHostName, diPortNumber);
  unDistributedControl_getAllDeviceConfigFromFile(diFileSystemId, dsFileHostName, diFilePortNumber, dsFileReduHostName, exceptionInfo);
  dynUnique(diSystemId);
  dynUnique(diFileSystemId);
  dynSortAsc(diSystemId);
  dynSortAsc(diFileSystemId);
  
  if(diSystemId == diFileSystemId){
    fwInstallation_throw("unDistributedControl.postInstall: configuration OK", "INFO", 10);
  }
  else{
    fwInstallation_throw("unDistributedControl.postInstall: some distPeers listed in the config file are not defined for unDistributedControl. Correct this from the component configuration panel...", "WARNING", 10);
  }
    
  //finally, append unDistributedControl CTRL manager to the console:
  fwInstallationManager_add("WCCOActrl", "always", 30, 3, 2, "unDistributedControl.ctl");
  fwInstallation_throw("unDistributedControl.postInstall: execution finished", "INFO", 10);

  return dynlen(exceptionInfo);
}

