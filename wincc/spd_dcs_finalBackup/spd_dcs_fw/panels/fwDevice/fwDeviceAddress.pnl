V 12
1
LANG:1 8 (NoName)
PANEL,-1 -1 685 450 N "_3DFace" 2
"$bHierarchyBrowser"
"$sDpName"
"main()
{
	dyn_bool dbDefaultsAllowed;
	dyn_string exceptionInfo;
  
	initialiseTable();
	
	fwDevice_canHaveDefaults(makeDynString($sDpName), dbDefaultsAllowed, exceptionInfo);
  enableApplyButtons(dbDefaultsAllowed[fwDEVICE_DEFAULT_CONFIGS]);
  
  if(isDollarDefined(\"$bHierarchyBrowser\"))
	{
    showCloseButton($bHierarchyBrowser);
  }
    
  dpname.text = $sDpName;
}



" 0
 E E E E 1 -1 -1 0  0 0
""0  1
E "
// ------------------------------------- ScopeLib -------------------------------------------------

// Constants to access the columns in the tables
int CONFIG_COLUMN		  = 1;
int ELEMENT_COLUMN		= 2;
int DIRECTION_COLUMN	= 3;
int ITEM_COLUMN			  = 4;
int MODE_COLUMN			  = 5;

// ------------------------------------------------------------------------------------------------

/**
  Loads all necessary information to be shown in the table and populates the table with it.
*/
void initialiseTable()
{
	int iConfigType;
	string sConfig, sDeviceModel, sDefinitionDp, sLabel;
	dyn_bool dbAdressAllowed, dbDefaultsAllowed;
	dyn_int diModes;
	dyn_string dsElements, dsAddressParameters, exceptionInfo;
  
  elementTable.tableMode(TABLE_SELECT_MULTIPLE); 
	elementTable.selectByClick(TABLE_SELECT_LINE);
  
  elementTable.deleteAllLines();
	
	fwDevice_canHaveDefaults(makeDynString($sDpName), dbDefaultsAllowed, exceptionInfo);
	fwDevice_getDefinitionDp(makeDynString($sDpName), sDefinitionDp, exceptionInfo);

	if(dbDefaultsAllowed[fwDEVICE_DEFAULT_CONFIGS])
	{
		fwDevice_getModel(makeDynString($sDpName), sDeviceModel, exceptionInfo);
		fwDevice_getAddressDefaultParams(dpTypeName($sDpName), dsAddressParameters, exceptionInfo, sDeviceModel);
		
		dpGet(sDefinitionDp + \".configuration.address.\" + dsAddressParameters[fwDevice_ADDRESS_TYPE] + \".direction\", diModes);
	}
		
	dpGet(sDefinitionDp + \".properties.dpes\", dsElements,	
			  sDefinitionDp + \".configuration.address.canHave\", dbAdressAllowed);

	for (int elementIndex = 1; elementIndex <= dynlen(dsElements); elementIndex++)
	{
		if(dbAdressAllowed[elementIndex])
		{
			dpGet($sDpName + dsElements[elementIndex] + \":_address.._type\", iConfigType);

			if(iConfigType == DPCONFIG_NONE)
      {
				sConfig = \"No\";
      }
			else
      {
				sConfig = \"Yes\";
      }
						
			if(dynlen(diModes) < elementIndex)
      {
				diModes[elementIndex] = fwDevice_DPCONFIG_NONE;
      }
			
			fwDevice_getAddressModeLabel(diModes[elementIndex], sLabel, exceptionInfo);
			
			elementTable.appendLine(\"config\", sConfig, 
								              \"element\", dsElements[elementIndex],    
								              \"direction\", sLabel);
		}
	}
	
	elementTable.lineVisible(0);
	
	enableApplyButtons(TRUE);
}

// ------------------------------------------------------------------------------------------------

/**
  Everythime the address of a DPE was altered, the information in the table needs to be updated.

@param [in] sTableName       string, name of the table shape
@param [in] iRow             int, number of the current row
@param [in] sDpe             string, DPE name
*/
void updateRow(string sTableName, int iRow, string sDpe)
{
	int iConfigType;
	
	dpGet(sDpe + \":_address.._type\", iConfigType);					

	if(iConfigType == DPCONFIG_NONE)
  {
		setValue(sTableName, \"cellValueRC\", iRow, \"config\", \"No\");
  }
	else
  {
		setValue(sTableName, \"cellValueRC\", iRow, \"config\", \"Yes\");
  }
			
}

// ------------------------------------------------------------------------------------------------

/**
  Shows modal dialog allowing the user to change address information for the selected DPE.

@param [in] iRow             int, number of the current row
@param [in] sColumnName      string, name of the current column
@param [in] sValue           string, value of the current cell
*/
void configureElementAddress(int iRow, string sColumnName, string sValue)
{
	dyn_float dfReturn;
	dyn_string dsReturn, exceptionInfo;
  string sPanelName = \"fwConfigs/fwPeriphAddressPopup.pnl\";

	if (sColumnName == \"element\")
	{
		if(getPath(PANELS_REL_PATH, sPanelName) != \"\")
		{
			ChildPanelOnCentralModalReturn(sPanelName,
										                 \"Peripheral Address Configuration for \" + $sDpName + sValue,
										                 makeDynString(\"$sDpName:\" + $sDpName,
														                       \"$sDpe:\" + $sDpName + sValue,
														                       \"$dsAddressTypes:\"),
                										 dfReturn, dsReturn);
									
		  updateRow(\"elementTable\", iRow, $sDpName + sValue);
		}
    else
    {     
      fwException_raise(exceptionInfo, \"WARNING\", \"The panel \\\"\" + sPanelName + \"\\\" could not be found.\", \"\");
			fwExceptionHandling_display(exceptionInfo);
    }	
	}
}

// ------------------------------------------------------------------------------------------------

/**
  Sets the address for the selected dpes (or all dpes).

@param [in] sReferenceName   string, the string to be prepared
*/
void setAddress(string sSelection)
{
	dyn_int diLines;
	dyn_float dfReturn;
	dyn_string dsAddressParameters, dsDpes, dsRow, exceptionInfo;

	enableApplyButtons(FALSE);

	// get which lines to work on 	
	if(sSelection == fwDevice_ADDRESS_DPES_ALL)
	{
		for(int elementIndex = 1; elementIndex <= elementTable.lineCount(); elementIndex++)
		{
			diLines[elementIndex] = elementIndex - 1;
		}	
	}
	else
	{
		diLines = elementTable.getSelectedLines();
	}
	
	// if no lines were selected raise an exception an return
	if(dynlen(diLines) < 1)
	{
		fwException_raise(	exceptionInfo, 
							\"WARNING\", 
							\"Select at least one element.\",
							\"\");
		fwExceptionHandling_display(exceptionInfo);
		enableApplyButtons(TRUE);
		return;
	}
	
	// get list of selected dpes	
	for(int lineIndex = 1; lineIndex <= dynlen(diLines); lineIndex++)
	{
		dsRow = elementTable.getLineN(diLines[lineIndex]);
		dsDpes[lineIndex] = dsRow[ELEMENT_COLUMN];
	}	

	ChildPanelOnCentralModalReturn(	\"fwDevice/fwDeviceAddressCommonSettings.pnl\",
									\"Choose the address common settings for all elements\",
									makeDynString(\"$sDpName:\" + $sDpName),
									dfReturn, dsAddressParameters);
							
	// check if operation was cancelled
	if(dynlen(dsAddressParameters) == 0)
	{
		enableApplyButtons(TRUE);
		return;
	}
	
	fwOpenProgressBar(\"Address configuration\", \"Setting addresses...\", 1, 10);
	
	fwDevice_setAddress($sDpName, dsAddressParameters, exceptionInfo, dsAddressParameters[fwDevice_ADDRESS_ROOT_NAME], dsDpes);
	
	// update table information
	for(int k = 1; k <= dynlen(diLines); k++)
	{
		updateRow(\"elementTable\", diLines[k], $sDpName + dsDpes[k]);	
	}

	enableApplyButtons(TRUE);

	if(dynlen(exceptionInfo) > 0)
	{
		fwCloseProgressBar(\"There were problems setting the addresses.\");
		fwExceptionHandling_display(exceptionInfo);
	}
	else
	{
		fwCloseProgressBar(\"Addresses configured successfully.\");
	}
}

// ------------------------------------------------------------------------------------------------

/**
  Enables or disables the apply buttons.

@param [in] bEnabled         bool, intended state of the buttons
*/
void enableApplyButtons(bool bEnabled)
{
	applySelectedButton.enabled(bEnabled);
	applyAllButton.enabled(bEnabled);	
}

// ------------------------------------------------------------------------------------------------

/**
  Adjust action buttons depending on the context the panel is opened from.
  
@param [in] bEmbeddedPanel   bool, panel is embedded or stand-alone
*/
void showCloseButton(bool bEmbeddedPanel)
{
  if(bEmbeddedPanel)
	{
		closeButton.visible = FALSE;
    positionApplyButtons();
	}
	else
	{
		closeButton.visible = TRUE;
	}
}

// ------------------------------------------------------------------------------------------------

/**
  Re-positioning of some buttons is necessary depending on the context the panel is opened from.
*/
void positionApplyButtons()
{
  int xPos, yPos;
	
  getValue(\"applySelectedButton\", \"position\", xPos, yPos);
  applySelectedButton.position(xPos+83, yPos);
  getValue(\"applyAllButton\", \"position\", xPos, yPos);
  applyAllButton.position(xPos+83, yPos);
}

// --------------------------------- End of ScopeLib ----------------------------------------------


" 0
 2
"CBRef" "1"
"EClose" E
""
NC
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 6 Layer1
6 210
"bgRectangle"
""
1 20 530 E E E 1 E 1 E N "_Transparent" E N "FwCorporateColor" E E
 E E
154 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E -1 -1 1268 29
2 100
"title"
""
1 8 10 E E E 1 E 1 E N "white" E N "_Transparent" E E
 E E
49 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 10 4 201 26
0 2 0 "0s" 0 0 0 192 0 0  10 4 1
1
LANG:1 26 Arial,-1,19,5,40,0,0,0,0,0
0 1
LANG:1 21 Address Configuration
13 29
"applyAllButton"
""
1 477 415 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
30 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  475 413 593 441

T 
1
LANG:1 17 Configure all ...
"main()
{
	setAddress(fwDevice_ADDRESS_DPES_ALL);
}" 0
 E E E
13 189
"applySelectedButton"
""
1 320 415 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
128 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  318 413 471 441

T 
1
LANG:1 22 Configure selected ...
"
main()
{
	setAddress(\"\");	
}" 0
 E E E
25 203
"elementTable"
""
1 10 73 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
139 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  8 71 676 406
E"main(int iRow, string sColumnName, string value)
{
	configureElementAddress(iRow, sColumnName, value);
}

" 0
 1 0 1 3 1 "config" 6 1 0 "s" 1
LANG:1 10 Configured
E
1
LANG:1 0 

80 "element" 43 1 0 "s" 1
LANG:1 7 Element
E
1
LANG:1 0 

485 "direction" 6 1 0 "s" 1
LANG:1 9 Direction
E
1
LANG:1 0 

80 
20 20 "" 1 1
LANG:1 2 #1
8 30
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0 0 2 1 1 7
1 0
13 205
"closeButton"
""
1 584 420 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
140 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  598 413 676 441

T 
1
LANG:1 5 Close
"main()
{
	PanelOff();
}
" 0
 E E E
1 211 13 "" 0
0
14 208
"dpname"
""
1 10 40 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
153 0 0 0 0 0
E E E
0
1
LANG:1 11 Device name

0
1
LANG:1 26 Arial,-1,13,5,40,0,0,0,0,0
0  8 38 676 64
2 "0s" 0 0 0 0 0 -1  E E E
0
LAYER, 1 
1
LANG:1 6 Layer2
0
LAYER, 2 
1
LANG:1 6 Layer3
0
LAYER, 3 
1
LANG:1 6 Layer4
0
LAYER, 4 
1
LANG:1 6 Layer5
0
LAYER, 5 
1
LANG:1 6 Layer6
0
LAYER, 6 
1
LANG:1 6 Layer7
0
LAYER, 7 
1
LANG:1 6 Layer8
0
3 13 "" -1
"objects\\fwGeneral\\fwHelpButton.pnl" 370 20 T 151 1 0 1 280 -18
1
"$sPanelRelativePath""fwDevice/fwDeviceAddress"
0
