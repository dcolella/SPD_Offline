V 10
1
LANG:1 8 (NoName)
PANEL,-1 -1 410 270 N "_3DFace" 2
"$sDpName"
"$sElmbLabels"
E E E E E 1 -1 -1 0  0 0
""0  1
E "
// ************************************************************************************
// Function:    modUpdateAvailableChannels
//
// Description: Updates combo box for first channel selection with available channels.
//
// Returns:     dyn_string if available channels
// ************************************************************************************
dyn_string modUpdateAvailableChannels(string argsDpName,
                                      string &argsChannel)
{
// Local Variables
// ---------------
  int i;
  int iPos = 0;
	
  dyn_string dsAvailableChannels;
  dyn_string dsExceptionInfo;

// Executable Code
// ---------------
  // Get list of available channels
  fwElmb_channelFilter(argsDpName,
                       ELMB_AI_SDO_TYPE_NAME,
                       dsAvailableChannels,
                       dsExceptionInfo);
				  		 	         
  // Check for errors
  if (dynlen(dsExceptionInfo) > 0) {
    fwExceptionHandling_display(dsExceptionInfo);
    return;
  }
	
  // Populate list of available channels
  cmbChannelSelector.items = dsAvailableChannels;
		
  // Initialise position of any given channel
  if (strlen(argsChannel) > 0)
    iPos = dynContains(dsAvailableChannels, argsChannel);
  if (iPos <= 0)
    iPos = 1;
  cmbChannelSelector.selectedPos = iPos;
  argsChannel = cmbChannelSelector.selectedText;

  // Return to calling routine
  return (dsAvailableChannels);
}

// ************************************************************************************
// Function:    modUpdateHowManyChannels
//
// Description: Updates the number of channels that can be created.
//
// Returns:     None
// ************************************************************************************
void modUpdateHowManyChannels(string argsChannel)
{
// Local Variables
// ---------------
  int i;
  int iChannelsPerSensor;
  int iPosSelectedChannel;
  int iMaxChannels = 1;
  int iMaxSensors;
  int iChannel;
  int iTemp;
	
  string sTemp;
	
  dyn_string dsAvailableChannels;
  dyn_string dsIds;

// Executable Code
// ---------------
  // Check if any channels available
  dsAvailableChannels = cmbChannelSelector.items;
  if (dynlen(dsAvailableChannels) == 1) {
    lblMessage.visible = false;
    lblChannelSelector.visible = true;
    spnHowMany.sbMinimum = 1;
    spnHowMany.text = \"1\";
    spnHowMany.enabled = false;
    return;
  } else if (dynlen(dsAvailableChannels) > 1) {
    lblMessage.visible = false;
    lblChannelSelector.visible = true;
    spnHowMany.sbMinimum = 1;
  } else {
    lblMessage.visible = true;
    lblChannelSelector.visible = false;
    spnHowMany.sbMinimum = 0;
    spnHowMany.text = \"0\";
    spnHowMany.enabled = false;
    return;
  }
	
  // Check if a channel has already been selected, and get the number
  if (argsChannel == \"\")
    iPosSelectedChannel = 1;
  else
    iPosSelectedChannel = dynContains(dsAvailableChannels, argsChannel);
  iChannel = dsAvailableChannels[iPosSelectedChannel];
	
  // If we've gotten this far, we know there are at least two channels available
  // (though these may not be consecutive channels)
  for (i = iPosSelectedChannel; i < dynlen(dsAvailableChannels); i++) {
    iTemp = dsAvailableChannels[i + 1];
    iChannel++;
    if (iTemp == iChannel)
      iMaxChannels++;
    else
      break;
  }
	
  // Check value displayed, and limit it if necessary
  if (iMaxChannels > 0) {
    sTemp = spnHowMany.text;
    sscanf(sTemp, \"%d\", iTemp);
    if (iTemp > iMaxChannels) {
      sprintf(sTemp, \"%d\", iMaxChannels);
      spnHowMany.text = sTemp;
    }

    // Set other controls
    spnHowMany.sbMaximum = iMaxChannels;
    spnHowMany.enabled = true;
  } else {
    spnHowMany.sbMinimum = 0;
    spnHowMany.text = \"0\";
    spnHowMany.enabled = false;
  }

  // Return to calling routine
  return;
}

void modGetDpNameElmb(int argiNElmb,
                      string &argsElmbDpName)
{
// Local Variables
// ---------------
  string sDpName;
	
  dyn_string dsExceptionInfo;
  dyn_string dsTemp = strsplit($sElmbLabels, \"|\");

// Executable Code
// ---------------
  // Check the datapoint type of the dollar parameter
  if (dpTypeName($sDpName) == ELMB_AI_CONFIG_TYPE_NAME) {
    fwDevice_getParent($sDpName, sDpName, dsExceptionInfo);
  } else if (dpTypeName($sDpName) == ELMB_TYPE_NAME) {
    sDpName = $sDpName;
  } else if (dpTypeName($sDpName) == ELMB_CAN_BUS_TYPE_NAME) {
    sDpName = $sDpName + fwDevice_HIERARCHY_SEPARATOR + dsTemp[argiNElmb];
  } else {
    fwExceptionHandling_display(makeDynString(\"Unknown parent DP type. Action aborted!!\"));
    return;
  }
	
  // Return only the datapoint name
  argsElmbDpName = dpSubStr(sDpName, DPSUB_DP);
	
  // Return to calling routine
  return;
}
" 0
 2
"CBRef" "1"
"EClose" E
""
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 6 Layer1
2 20
"txtMessage"
""
1 10 210 E E E 1 E 1 E N "_WindowText" E N "_Transparent" E E
 E E
20 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 12 212 398 238
0 2 0 "0s" 0 0 0 64 0 0  12 212 1
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
1
LANG:1 0 
2 21
"lblComment"
""
1 10 120 E E E 1 E 1 E N {0,0,0} E N "_Transparent" E E
 E E
20 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 12 122 68 139
0 2 0 "0s" 0 0 0 192 0 0  12 122 1
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
1
LANG:1 7 Comment
30 27
"frmOpcAddressing"
""
1 10 290 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
26 0 0 0 0 0
E E E
1
2
LANG:1 0 
LANG:5 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1 0 1 0 -70 0 E 10 240 400 290
2
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
LANG:5 107 -microsoft windows-Arial-normal-r-normal-*-*-120-100-100-*-*-iso8859-1|-13,0,0,0,400,0,0,0,0,3,2,1,34,Arial
0 ""
2
LANG:1 0 
LANG:5 0 
2 32
"txtTitle"
""
1 10 87 E E E 1 E 1 E N {0,0,0} E N "_Transparent" E E
 E E
30 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:5 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 12 12 340 37
0 2 0 "0s" 0 0 0 64 0 0  12 12 1
2
LANG:1 84 -*-Arial-*-r-normal-*-21-*-100-100-*-*-iso8859-1|-21,0,0,0,404,0,0,0,0,0,0,0,0,Arial
LANG:5 107 -microsoft windows-Arial-normal-r-normal-*-*-180-100-100-*-*-iso8859-1|-21,0,0,0,400,0,0,0,0,3,2,1,34,Arial
0 ""
2
LANG:1 32 Create SDO Analog Input Channels
LANG:5 16 Create Elmb Node
14 28
"txtComment"
""
1 10 140 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
27 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
 8 138 402 165
3 "0s" 0 0 0 0 0 -1  E E E
20 29
"chbOpcAddressing"
""
1 26 180 E E E 1 E 1 E N "_3DText" E N "_3DFace" E E
 E E
28 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:5 0 

0
2
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
LANG:5 107 -microsoft windows-Arial-normal-r-normal-*-*-120-100-100-*-*-iso8859-1|-13,0,0,0,400,0,0,0,0,3,2,1,34,Arial
0 ""
 24 178 221 212
1
T 
2
LANG:1 23 Default OPC addressing?
LANG:5 23 Default OPC addressing?

1 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
E"main(int button, bool state)
{
// Local Variables
// ---------------
  dyn_float df;
	
  dyn_string ds;
	
// Executable Code
// ---------------
  // Check the state of the check-box
  if (!state) {
    ChildPanelOnCentralReturn(\"vision/MessageInfo3\",
                              \"Question\",
                              makeDynString(\"Unselecting this option you'll have to address\\nyour devices by hand.\\nAre you sure you want to do it?\", \"Yes\", \"No\", \"Cancel\"),
                              df, ds);
    if (df[1] != 1)
      this.state(button) = true;
  }
	
  // Return to calling routine
  return;
}" 0
13 31
"cmdClose"
""
1 310 230 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
29 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
 308 228 402 262

T 
1
LANG:1 5 Close
"main()
{
	// Close the panel
	PanelOff();
}
" 0
 E E E
13 37
"cmdCreate"
""
1 210 230 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
33 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
 208 228 302 262

T 
1
LANG:1 6 Create
"main()
{
// Local Variables
// ---------------
  bool bIsRunning;
  bool bDefaultOPCAddressing;

  int iDriverNumber;
  int i, j;
  int iHowMany;
  int iFirstChannel;
  int iChannel;

  string sDpName;
  string sBus;
  string sElmb;
  string sComment;
  string sNextChannel;
	
  dyn_float df;

  dyn_string dsExceptionInfo;
  dyn_string dsElmbs = strsplit($sElmbLabels, \"|\");
  dyn_string dsAvailableChannels;
  dyn_string ds;
	
// Executable Code
// ---------------
// Check if the default driver is running
  bIsRunning = fwElmbUser_checkDefaultDriver(iDriverNumber, dsExceptionInfo);
  if (dynlen(dsExceptionInfo) > 0) {
    fwExceptionHandling_display(dsExceptionInfo);
    return;
  } else if (!bIsRunning) {
    ChildPanelOnCentralReturn(\"fwElmb/fwElmbDriverMessage.pnl\",
                              \"Warning\",
                              makeDynString(\"$drvNum:\" + iDriverNumber),
                              df, ds);
    return;
  }
	
  // Get required information from the panel
  iFirstChannel = cmbChannelSelector.text;
  bDefaultOPCAddressing = chbOpcAddressing.state(0);
  sComment = txtComment.text;
  iHowMany = spnHowMany.text;

  // Start progress bar to indicate system is busy
  fwOpenProgressBar(\"Create SDO Analog Input\", \"In progress. Please wait...\", 1);
	
  // For each ELMB, create the required analog output channels
  for (i = 1; i <= dynlen(dsElmbs); i++) {

    // Get current ELMB datapoint name	
    modGetDpNameElmb(i, sDpName);

    // Get the bus name, removing all framework path information (not strictly
    // necessary in this loop, but would allow for mutliple ELMBs to have analog
    // outputs created even if not on the same bus). Also get the ELMB name without
    // framework path information at the same time.
    ds = strsplit(sDpName, fwDevice_HIERARCHY_SEPARATOR);
    if (dynlen(ds) >= 3) {
      sBus = ds[2];
      sElmb = ds[3];
      iChannel = iFirstChannel;
      dsAvailableChannels = modUpdateAvailableChannels(sDpName, iFirstChannel);

      // Create all SDO analog inputs for this ELMB
      for (j = 1; j <= iHowMany; j++) {

        // Create the SDO analog input
        fwElmbUser_createAnalogInputSDO(sBus, sElmb,
                                        sComment,
                                        iChannel,
                                        bDefaultOPCAddressing,
                                        dsExceptionInfo);

        // Check for success or error
        if (dynlen(dsExceptionInfo) > 0) {
          break;
        } else {
          if (j != iHowMany) {
            // Calculate the next ELMB channel that will be used
            sNextChannel = dsAvailableChannels[dynContains(dsAvailableChannels, iChannel) + 1];
            dsAvailableChannels = modUpdateAvailableChannels(sDpName, sNextChannel);
            modUpdateHowManyChannels(sNextChannel);
            iChannel = sNextChannel;
          }
        }
      } // End of loop j (outputs)
    }
  } // End of loop i (ELMBs)
  
  sNextChannel = \"\";
  dsAvailableChannels = modUpdateAvailableChannels(sDpName, sNextChannel);
  modUpdateHowManyChannels(sNextChannel);

  if (dynlen(dsExceptionInfo) > 0) {
    fwCloseProgressBar(\"SDO Analog Input creation encountered errors\");
    fwExceptionHandling_display(dsExceptionInfo);
  } else {
    fwCloseProgressBar(\"SDO Analog Input creation completed successfully\");
  }	

  // Return to calling routine
  return;
}
" 0
 E E E
2 38
"lblChannelSelector"
""
1 19 97 E E E 1 E 1 E N {0,0,0} E N "_Transparent" E E
 E E
35 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 20 60 140 73
0 2 0 "0s" 0 0 0 64 0 0  20 60 1
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
1
LANG:1 19 First ELMB Channel:
22 39
"cmbChannelSelector"
""
1 150 60 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
37 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
 148 58 204 85
0

"main()
{
  string sDpName;
  string sChannel;
  
  dyn_string dsAvailableChannels;
  
  // Get all available channels and update combo box (within function)
  modGetDpNameElmb(1, sDpName);
  dsAvailableChannels = modUpdateAvailableChannels(sDpName, sChannel);

  // Update controls, checking the correct number of consecutive channels exist
  modUpdateHowManyChannels(sChannel);
}" 0

"main()
{
// Local Variables
// ---------------
  string sChannel = this.text;
	
// Executable Code
// ---------------
  // Update fields relevant to first channel selected
  modUpdateHowManyChannels(sChannel);
	
  // Return to calling routine
  return;
}" 0

E
 0 0
30 40
"frmChannelInformation"
""
1 25 440 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
39 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 0.730337078651685 0 0.845070422535211 -8.25842696629214 -270.985915492958 0 E 25 368 560 440
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
1
LANG:1 0 
21 41
"spnHowMany"
""
1 340 60 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
41 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
 338 58 382 85
0

E
E
E

N 1 64 1 1 1 1
2 42
"lblHowManyChannels"
""
1 208 110 E E E 1 E 1 E N {0,0,0} E N "_Transparent" E E
 E E
43 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 210 60 334 77
0 2 0 "0s" 0 0 0 64 0 0  210 60 1
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
1
LANG:1 18 How many channels?
2 43
"lblMessage"
""
1 18 58 E E E 1 E 1 E N {255,0,0} E N "_Transparent" E E
 E E
45 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 20 60 146 79
0 2 0 "0s" 0 0 0 64 0 0  20 60 1
1
LANG:1 84 -*-Arial-*-r-normal-*-13-*-100-100-*-*-iso8859-1|-13,0,0,0,404,0,0,0,0,0,0,0,0,Arial
0 ""
1
LANG:1 21 No channels available
0
LAYER, 1 
1
LANG:1 6 Layer2
0
LAYER, 2 
1
LANG:1 6 Layer3
0
LAYER, 3 
1
LANG:1 6 Layer4
0
LAYER, 4 
1
LANG:1 6 Layer5
0
LAYER, 5 
1
LANG:1 6 Layer6
0
LAYER, 6 
1
LANG:1 6 Layer7
0
LAYER, 7 
1
LANG:1 6 Layer8
0
0