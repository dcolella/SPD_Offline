class: ASS_AliDcsSafe_CU_CLASS/associated
!panel: AliDcsSafe_CU.pnl
    state: SUPERSAFE	!color: FwStateOKNotPhysics
        action: RESTORE_SAFE	!visible: 1
    state: SAFE	!color: FwStateOKPhysics
        action: GO_SUPERSAFE	!visible: 1
    state: MOVING_SAFE	!color: FwStateAttention1
    state: MOVING_SUPERSAFE	!color: FwStateAttention1
    state: NOT_SAFE	!color: FwStateAttention2
        action: GO_SAFE	!visible: 1
