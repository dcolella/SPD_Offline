class: ASS_Dcs_FSMTimer_CLASS/associated
!panel: Dcs_FSMTimer.pnl
    parameters: string USER_DATA = ""
    state: READY	!color: FwStateOKPhysics
        action: START(int DELAY = 0,string SPECIFIC = "None")	!visible: 1
    state: WAIT	!color: FwStateAttention1
        action: RESET	!visible: 1
        action: START(int DELAY = 0,string SPECIFIC = "None")	!visible: 1
    state: END	!color: FwStateAttention2
        action: ACKNOLEDGE	!visible: 1
        action: START(int DELAY = 0,string SPECIFIC = "None")	!visible: 1
