class: ASS_FwCaVLoop_CLASS/associated
!panel: aliDcsCavFSM/CaVFSM_loopOperation.pnl
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY	!visible: 1
        action: GO_ON	!visible: 1
        action: OPEN_LOOP(string loopList = "")	!visible: 0
        action: LOCK	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_ON	!visible: 1
        action: OPEN_LOOP(string loopList = "")	!visible: 0
        action: LOCK	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: ON	!color: FwStateOKPhysics
        action: GO_OFF	!visible: 1
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: LOCK	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: LOCKED	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: GO_ON	!visible: 1
        action: OPEN_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: MAINTENANCE	!color: FwStateOKNotPhysics
        action: EXIT_MAINTENANCE	!visible: 0
    state: PURGED	!color: FwStateOKNotPhysics
    state: ERROR	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
