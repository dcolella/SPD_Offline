class: ASS_AliDcs_runCU_CLASS/associated
!panel: aliDcsRunUnit/aliDcsRunCU.pnl
    parameters: string run_no = "uninitialized"
    state: RUN_OK	!color: FwStateOKPhysics
        action: SOR(string run_type = "",string run_no = "",string ddl_list = "",string sor_mode = "")	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "FULL")	!visible: 1
        action: INHIBIT_RUN	!visible: 1
        action: REQUEST_PAR	!visible: 1
    state: SOR_PROGRESSING	!color: FwStateAttention1
    state: EOR_PROGRESSING	!color: FwStateAttention1
    state: SOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: EOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: RUN_INHIBIT	!color: FwStateOKNotPhysics
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ALLOW_RUN	!visible: 1
    state: PAR_REQUEST	!color: FwStateAttention2
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "FULL")	!visible: 1
        action: RESET	!visible: 1
        action: RECOVER	!visible: 1
        action: ALLOW_RUN	!visible: 1
    state: PAR_PROGRESSING	!color: FwStateAttention1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "FULL")	!visible: 1
        action: RESET	!visible: 1
        action: ALLOW_RUN	!visible: 1
