class: ASS_SpdPower_CLASS/associated
!panel: SpdPower.pnl
    state: NOT_READY	!color: FwStateOKNotPhysics
        action: SWITCH_ON	!visible: 1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: KILL	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: ACKNOLEDGE	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ERROR	!color: FwStateAttention3
