class: ASS_SpdDCS_CLASS/associated
!panel: FSM/SpdDCS.pnl
    parameters: string RunMode = "DEFAULT", int VersionN = 0, string CalibMode = "ALL"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: INFRA_ONLY	!color: FwStateOKNotPhysics
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: STANDBY	!color: FwStateOKNotPhysics
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(string run_mode = "DEFAULT",int version = 0)	!visible: 1
        action: GO_READY	!visible: 1
        action: GO_BEAM_TUN	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
        action: SOR(string run_type = "",string run_no = "",string sor_mode = "FULL",string ddl_list = "")	!visible: 1
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: CONFIGURE(string run_mode = "DEFAULT",int version = 0)	!visible: 1
        action: GO_STBY_CONF	!visible: 1
        action: GO_READY	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: SOR(string run_type = "",string run_no = "",string sor_mode = "FULL",string ddl_list = "")	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
        action: REQUEST_PAR	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONF	!visible: 1
        action: CALIBRATE(string run_number = "0",string calib_mode = "ALL")	!visible: 1
        action: CONFIGURE	!visible: 1
        action: DAQ_EOR	!visible: 1
        action: GO_BEAM_TUN	!visible: 1
        action: SOR(string run_type = "",string run_no = "",string sor_mode = "FULL",string ddl_list = "")	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
        action: REQUEST_PAR	!visible: 1
    state: MIXED	!color: FwStateAttention2
        action: CALIBRATE(string run_number = "0",string calib_mode = "ALL")	!visible: 1
        action: CONFIGURE(string run_mode = "DEFAULT",int version = 0)	!visible: 1
        action: DAQ_EOR	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: DOWNLOADING	!color: FwStateAttention1
        action: STOP	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP	!visible: 1
        action: SOR(string run_type = "",string run_no = "",string ddl_list = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19",string sor_mode = "FULL")	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: BUSY	!color: FwStateAttention1
        action: STOP	!visible: 1
        action: RELEASE	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: MOVING_READY	!color: FwStateAttention1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: RELEASE	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
    state: PAR_REQUEST	!color: FwStateAttention2
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: RECOVER	!visible: 1
        action: ABORT_PAR	!visible: 1
    state: PAR_PROGRESSING	!color: FwStateAttention1
        action: GO_SAFE	!visible: 1
        action: GO_SUPERSAFE	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ACK_RUN_FAILURE	!visible: 1
        action: ABORT_PAR	!visible: 1
