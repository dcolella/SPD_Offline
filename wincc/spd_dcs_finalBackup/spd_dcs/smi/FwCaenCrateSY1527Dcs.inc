class: ASS_FwCaenCrateSY1527Dcs_CLASS/associated
!panel: FwCaenCrateSY1527|dcsCaen\dcsCaenCrateSY1527.pnl
    state: OFF	!color: _3DFace
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ON	!color: FwStateOKPhysics
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: PWS_FAULT	!color: FwStateAttention3
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL	!visible: 1
        action: RESET	!visible: 1
        action: ACKNOLEDGE	!visible: 1
