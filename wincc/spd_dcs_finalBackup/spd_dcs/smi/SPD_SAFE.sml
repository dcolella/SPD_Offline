class: TOP_AliDcsSafe_CU_CLASS
!panel: AliDcsSafe_CU.pnl
    state: SUPERSAFE	!color: FwStateOKNotPhysics
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state NOT_SAFE ) move_to NOT_SAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state MOVING_SAFE ) move_to MOVING_SAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state MOVING_SUPERSAFE )  move_to MOVING_SUPERSAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state SAFE ) move_to SAFE
        action: RESTORE_SAFE	!visible: 1
            do RESTORE_SAFE all_in ALIEMERGENCYBUTTON_DU_FWSETACTIONS
            if ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES not_in_state SAFE ) then
                move_to MOVING_SAFE
            endif
            move_to SAFE
    state: SAFE	!color: FwStateOKPhysics
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state MOVING_SAFE ) move_to MOVING_SAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state MOVING_SUPERSAFE )  move_to MOVING_SUPERSAFE
        when ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state SUPERSAFE ) move_to SUPERSAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state NOT_SAFE ) move_to NOT_SAFE
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIEMERGENCYBUTTON_DU_FWSETACTIONS
            if ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES not_in_state SUPERSAFE ) then
                move_to MOVING_SUPERSAFE
            endif
            move_to SUPERSAFE
    state: MOVING_SAFE	!color: FwStateAttention1
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state NOT_SAFE ) move_to NOT_SAFE
        when ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state SAFE ) move_to SAFE
        when ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state SUPERSAFE ) move_to SUPERSAFE
    state: MOVING_SUPERSAFE	!color: FwStateAttention1
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state NOT_SAFE ) move_to NOT_SAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state SAFE ) move_to SAFE
        when ( any_in ALIEMERGENCYBUTTON_DU_FWSETSTATES in_state SUPERSAFE ) move_to SUPERSAFE
    state: NOT_SAFE	!color: FwStateAttention2
        when ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES not_in_state NOT_SAFE ) move_to MOVING_SAFE
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIEMERGENCYBUTTON_DU_FWSETACTIONS
            if ( all_in ALIEMERGENCYBUTTON_DU_FWSETSTATES not_in_state SAFE ) then
                move_to MOVING_SAFE
            endif
            move_to SAFE

object: SPD_SAFE is_of_class TOP_AliDcsSafe_CU_CLASS

class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED )  ) move_to Complete
    state: IncompleteDead	!color: FwStateAttention3

object: SPD_SAFE_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded

object: SPD_SAFE_FWM is_of_class FwMode_CLASS

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
!panel: FwDevMode.pnl
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: SPD_SAFE_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES is_of_class VOID {SPD_SAFE_FWDM }
objectset: FWDEVMODE_FWSETACTIONS is_of_class VOID {SPD_SAFE_FWDM }

class: AliEmergencyButton_DU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from ALIEMERGENCYBUTTON_DU_FWSETSTATES
            remove &VAL_OF_Device from ALIEMERGENCYBUTTON_DU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in ALIEMERGENCYBUTTON_DU_FWSETSTATES
            insert &VAL_OF_Device in ALIEMERGENCYBUTTON_DU_FWSETACTIONS
            move_to READY

object: AliEmergencyButton_DU_FWDM is_of_class AliEmergencyButton_DU_FwDevMode_CLASS


class: AliEmergencyButton_DU_CLASS/associated
!panel: AliEmergencyButton|AliEmergencyButton_DU.pnl
    state: SUPERSAFE	!color: FwStateOKNotPhysics
        action: RESTORE_SAFE	!visible: 1
    state: SAFE	!color: FwStateOKPhysics
        action: GO_SUPERSAFE	!visible: 1
    state: MOVING_SAFE	!color: FwStateAttention1
    state: MOVING_SUPERSAFE	!color: FwStateAttention1
    state: NOT_SAFE	!color: FwStateAttention2
        action: GO_SAFE	!visible: 1

object: spdEmergencyButton is_of_class AliEmergencyButton_DU_CLASS

objectset: ALIEMERGENCYBUTTON_DU_FWSETSTATES is_of_class VOID {spdEmergencyButton }
objectset: ALIEMERGENCYBUTTON_DU_FWSETACTIONS is_of_class VOID {spdEmergencyButton }


objectset: FWCHILDREN_FWSETACTIONS union {ALIEMERGENCYBUTTON_DU_FWSETACTIONS } is_of_class VOID
objectset: FWCHILDREN_FWSETSTATES union {ALIEMERGENCYBUTTON_DU_FWSETSTATES } is_of_class VOID

