create table fw_installation_schema
             (idx number,
              schema_version varchar2(128) not null,
              valid_from date default sysdate,
              valid_until date,
              constraint idx_schema_pk primary key (idx));
              
create table fw_installation_pc
             (idx number,
              host varchar2(32) not null,
              ip varchar2(32),
              constraint idx_pc_pk primary key (idx));


create table fw_installation_project
             (idx number,
              project_name varchar2(128) not null,
              system_name varchar2(64) not null,
              system_number number(3) not null,
              project_dir varchar2(256) not null,
              pmon_port number(5) default 4999,
              pvss_version varchar2(20),
              pc_idx number,
              valid_from date default sysdate,
              valid_until date,
              constraint idx_project_pk primary key (idx));

create table fw_installation_component
             (idx number,
              component_name varchar2(128) not null,
              component_version varchar2(20),
              is_subcomponent number(1) not null,
              default_path varchar2(256),
              is_official number default 1,
              parent_component_idx number,
              constraint idx_component_pk primary key (idx));

create table fw_installation_configuration
             (idx number,
              component_idx number,
              project_idx number,
              source_dir varchar2(256),
              installation_dir varchar2(256),
              installed_by varchar2(64),
              installation_date date default sysdate,
              valid_until date,
              constraint idx_installed_component_pk primary key (idx));

create table fw_installation_request
             (idx number,
              action varchar2(32) not null,
              component_name varchar2(64) not null,
              component_version varchar2(32) not null,
              source_dir varchar2(256),
              project_name varchar2(256) not null,
              pc_name varchar2(256) not null,
              requested_by varchar2(64),
              request_date date default sysdate,
              scheduled_installation_date date,
              overwrite_files number default 0,
              force_required number default 1,
              is_silent number default 0,
              constraint idx_request_pk primary key (idx));

create table fw_installation_tool
             (idx number,
              tool_version varchar2(32) not null,
              project_idx number not null,
              valid_from date default sysdate,
              valid_until date,
              constraint idx_tool_project_pk primary key (idx));


alter table fw_installation_tool
	add constraint tool_project_idx_fk
	foreign key (project_idx)
	references fw_installation_project(idx);


alter table fw_installation_project
	add constraint project_pc_idx_fk
	foreign key (pc_idx)
	references fw_installation_pc(idx);


alter table fw_installation_configuration
	add constraint configuration_project_idx_fk
	foreign key (project_idx)
	references fw_installation_project(idx);


alter table fw_installation_configuration
	add constraint configuration_component_idx_fk
	foreign key (component_idx)
	references fw_installation_component(idx);


CREATE SEQUENCE fw_schema_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;
 

CREATE SEQUENCE fw_tool_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;


CREATE SEQUENCE fw_pc_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;


CREATE SEQUENCE fw_project_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;


CREATE SEQUENCE fw_component_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;


CREATE SEQUENCE fw_configuration_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;


CREATE SEQUENCE fw_request_seq
    MINVALUE 1
    MAXVALUE 9999999
    START WITH 1
    INCREMENT BY 1
    NOCACHE;


INSERT INTO fw_installation_schema VALUES (fw_schema_seq.nextval, '1.0.0', SYSDATE, NULL);

commit;


