This Archive contains log files that have been rotated out by WinCC.
This usually occurs when a log file reaches 10MB, and is created with the same name, and the suffix '.bak' added.
 - The exception is fwFSM logs, which are rotated every restart of the FSM, with the suffix '.bak-n' where n <= 5.

Each system archives the project .bak logs every 15 minutes, to this directory structure.
Each project has its own archive, sub-divided by year. 
Each log has the timestamp of when it was last modified injected into the filename. (This ensures the archive is complete.)

If you have any questions or concerns about this mechanism, please send an e-mail to the following address:

alice-dcs-support@cern.ch
