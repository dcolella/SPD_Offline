class: SPD_VMETOP_VME_CLASS
!panel: VME.pnl
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state CONTROL_OK ) and
       ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state OFF )  )  move_to NOT_READY
        when ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state PWS_FAILURE )  move_to ERROR
        when (  ( any_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state NO_CONTROL ) or
       ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state NO_CONTROL )  )  move_to NO_CONTROL
        action: SWITCH_OFF_ALL	!visible: 2
            do SWITCH_OFF all_in SPD_VMEFWWIENERCRATE_FWSETACTIONS
    state: NOT_READY	!color: FwStateOKNotPhysics
        when (  ( all_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state ON ) and
       ( all_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state CONTROL_OK )  )  move_to READY
        when ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state PWS_FAILURE )  move_to ERROR
        when (  ( any_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state NO_CONTROL ) or
       ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state NO_CONTROL )  )  move_to NO_CONTROL
        action: SWITCH_ON_ALL	!visible: 2
            do SWITCH_ON all_in SPD_VMEFWWIENERCRATE_FWSETACTIONS
    state: ERROR	!color: FwStateAttention3
        when (  ( all_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state ON ) and
       ( all_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state CONTROL_OK )  )  move_to READY
        when ( all_in SPD_VMEFWWIENERCRATE_FWSETSTATES not_in_state PWS_FAILURE )  move_to NOT_READY
        when ( any_in SPD_VMEFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL
        action: RESET_VME_bus	!visible: 2
            do RESET_VME_Bus all_in SPD_VMEFWWIENERCRATE_FWSETACTIONS
        action: SWITCH_ON_ALL	!visible: 2
            do SWITCH_ON all_in SPD_VMEFWWIENERCRATE_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( all_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state CONTROL_OK ) and
       ( all_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state ON )  )  move_to READY
        when (  ( all_in SPD_VMEFWWIENERCANBUS_FWSETSTATES in_state CONTROL_OK ) and
       ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state OFF )  )  move_to NOT_READY
        when (  ( any_in SPD_VMEFWWIENERCRATE_FWSETSTATES in_state PWS_FAILURE ) and
       ( all_in SPD_VMEFWCHILDREN_FWSETSTATES not_in_state NO_CONTROL )  )  move_to ERROR

object: SPD_VME is_of_class SPD_VMETOP_VME_CLASS

class: SPD_VMEFwWienerCanBus_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SPD_VMEFWWIENERCANBUS_FWSETSTATES
            remove &VAL_OF_Device from SPD_VMEFWWIENERCANBUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SPD_VMEFWWIENERCANBUS_FWSETSTATES
            insert &VAL_OF_Device in SPD_VMEFWWIENERCANBUS_FWSETACTIONS
            move_to READY

object: SPD_VMEFwWienerCanBus_FWDM is_of_class SPD_VMEFwWienerCanBus_FwDevMode_CLASS


class: SPD_VMEFwWienerCanBus_CLASS/associated
!panel: FwWienerCanBus.pnl
    state: CONTROL_OK	!color: FwStateOKPhysics
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1

object: Wiener:CAN0 is_of_class SPD_VMEFwWienerCanBus_CLASS

objectset: SPD_VMEFWWIENERCANBUS_FWSETSTATES is_of_class VOID {Wiener:CAN0 }
objectset: SPD_VMEFWWIENERCANBUS_FWSETACTIONS is_of_class VOID {Wiener:CAN0 }

class: SPD_VMEFwWienerCrate_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SPD_VMEFWWIENERCRATE_FWSETSTATES
            remove &VAL_OF_Device from SPD_VMEFWWIENERCRATE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SPD_VMEFWWIENERCRATE_FWSETSTATES
            insert &VAL_OF_Device in SPD_VMEFWWIENERCRATE_FWSETACTIONS
            move_to READY

object: SPD_VMEFwWienerCrate_FWDM is_of_class SPD_VMEFwWienerCrate_FwDevMode_CLASS


class: SPD_VMEFwWienerCrate_CLASS/associated
!panel: FwWienerCrate.pnl
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON	!visible: 2
    state: PWS_FAILURE	!color: FwStateAttention3
        action: SWITCH_ON	!visible: 2
        action: RESET_VME_Bus	!visible: 2
    state: NO_STATE	!color: FwStateAttention1

object: Wiener:CAN0:Crate1 is_of_class SPD_VMEFwWienerCrate_CLASS

object: Wiener:CAN0:Crate2 is_of_class SPD_VMEFwWienerCrate_CLASS

objectset: SPD_VMEFWWIENERCRATE_FWSETSTATES is_of_class VOID {Wiener:CAN0:Crate1,
	Wiener:CAN0:Crate2 }
objectset: SPD_VMEFWWIENERCRATE_FWSETACTIONS is_of_class VOID {Wiener:CAN0:Crate1,
	Wiener:CAN0:Crate2 }


objectset: SPD_VMEFWCHILDREN_FWSETACTIONS union {SPD_VMEFWWIENERCANBUS_FWSETACTIONS,
	SPD_VMEFWWIENERCRATE_FWSETACTIONS } is_of_class VOID
objectset: SPD_VMEFWCHILDREN_FWSETSTATES union {SPD_VMEFWWIENERCANBUS_FWSETSTATES,
	SPD_VMEFWWIENERCRATE_FWSETSTATES } is_of_class VOID

class: SpdPowerTOP_SpdPower_CLASS
!panel: SpdPower.pnl
    state: NOT_READY	!color: FwStateOKNotPhysics
        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {WA_REPAIR,ER_REPAIR,PWS_FAULT} )  move_to ERROR

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  move_to ERROR        

        when ( all_in SpdPowerFWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to NO_CONTROL

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state FAKE ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state FAKE )  )  move_to NO_CONTROL

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {INTERLOCK,INTERLOCK_WENT} )  move_to INTERLOCK

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  move_to READY

        action: SWITCH_ON	!visible: 1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {WA_REPAIR,ER_REPAIR,PWS_FAULT} )  move_to ERROR

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  move_to ERROR                

        when ( all_in SpdPowerFWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to NO_CONTROL        

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state FAKE ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state FAKE )  )  move_to NO_CONTROL

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {INTERLOCK,INTERLOCK_WENT} )  move_to INTERLOCK

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF )  )  move_to NOT_READY

        when ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF )  move_to NOT_READY

        action: KILL	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: ACKNOLEDGE	!visible: 1
            move_to NOT_READY   
    state: INTERLOCK	!color: FwStateAttention3
        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {WA_REPAIR,ER_REPAIR,PWS_FAULT} )  move_to ERROR

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  move_to ERROR                

        when ( all_in SpdPowerFWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to NO_CONTROL        

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state FAKE ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state FAKE )  )  move_to NO_CONTROL

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  move_to READY

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF )  )  move_to NOT_READY

        when ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF )  move_to NOT_READY

        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {WA_REPAIR,ER_REPAIR,PWS_FAULT} )  stay_in_state    

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  stay_in_state         

        when ( all_in SpdPowerFWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to NO_CONTROL

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state FAKE ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES not_in_state FAKE )  )  move_to NO_CONTROL

        when ( any_in SpdPowerFWCHILDREN_FWSETSTATES in_state {INTERLOCK,INTERLOCK_WENT} )  move_to INTERLOCK

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) and
( all_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state ON )  )  move_to READY

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state ON ) and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF )  )  move_to NOT_READY

        when (  ( all_in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES in_state OFF )  and
( any_in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES in_state OFF )  ) move_to NOT_READY


object: SpdPower is_of_class SpdPowerTOP_SpdPower_CLASS

class: SpdPowerFwCaenCrateSY1527Dcs_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES
            remove &VAL_OF_Device from SpdPowerFWCAENCRATESY1527DCS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES
            insert &VAL_OF_Device in SpdPowerFWCAENCRATESY1527DCS_FWSETACTIONS
            move_to READY

object: SpdPowerFwCaenCrateSY1527Dcs_FWDM is_of_class SpdPowerFwCaenCrateSY1527Dcs_FwDevMode_CLASS


class: SpdPowerFwCaenCrateSY1527Dcs_CLASS/associated
!panel: FwCaenCrateSY1527|dcsCaen\dcsCaenCrateSY1527.pnl
    state: OFF	!color: _3DFace
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ON	!color: FwStateOKPhysics
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: PWS_FAULT	!color: FwStateAttention3
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL	!visible: 1
        action: RESET	!visible: 1
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL	!visible: 1
        action: RESET	!visible: 1
        action: ACKNOLEDGE	!visible: 1

object: CAEN:SY1527 is_of_class SpdPowerFwCaenCrateSY1527Dcs_CLASS

objectset: SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES is_of_class VOID {CAEN:SY1527 }
objectset: SpdPowerFWCAENCRATESY1527DCS_FWSETACTIONS is_of_class VOID {CAEN:SY1527 }

class: SpdPowerFwCaenChannelDcsEasyPWS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES
            remove &VAL_OF_Device from SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES
            insert &VAL_OF_Device in SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETACTIONS
            move_to READY

object: SpdPowerFwCaenChannelDcsEasyPWS_FWDM is_of_class SpdPowerFwCaenChannelDcsEasyPWS_FwDevMode_CLASS


class: SpdPowerFwCaenChannelDcsEasyPWS_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenEasyPowerSupply.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON	!visible: 1
        action: RESET	!visible: 1
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: ER_REPAIR	!color: FwStateAttention2
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: PWS_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
        action: KILL	!visible: 1

object: CAEN:SY1527:branchController14:easyCrate4:powerConverter22:channel000 is_of_class SpdPowerFwCaenChannelDcsEasyPWS_CLASS

object: CAEN:SY1527:branchController14:easyCrate4:powerConverter22:channel001 is_of_class SpdPowerFwCaenChannelDcsEasyPWS_CLASS

object: CAEN:SY1527:branchController14:easyCrate5:powerConverter22:channel000 is_of_class SpdPowerFwCaenChannelDcsEasyPWS_CLASS

object: CAEN:SY1527:branchController14:easyCrate5:powerConverter22:channel001 is_of_class SpdPowerFwCaenChannelDcsEasyPWS_CLASS

objectset: SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES is_of_class VOID {CAEN:SY1527:branchController14:easyCrate4:powerConverter22:channel000,
	CAEN:SY1527:branchController14:easyCrate4:powerConverter22:channel001,
	CAEN:SY1527:branchController14:easyCrate5:powerConverter22:channel000,
	CAEN:SY1527:branchController14:easyCrate5:powerConverter22:channel001 }
objectset: SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETACTIONS is_of_class VOID {CAEN:SY1527:branchController14:easyCrate4:powerConverter22:channel000,
	CAEN:SY1527:branchController14:easyCrate4:powerConverter22:channel001,
	CAEN:SY1527:branchController14:easyCrate5:powerConverter22:channel000,
	CAEN:SY1527:branchController14:easyCrate5:powerConverter22:channel001 }

class: SpdPowerFwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdPowerFWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdPowerFWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdPowerFWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdPowerFWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdPowerFwDevMajority_FWDM is_of_class SpdPowerFwDevMajority_FwDevMode_CLASS


class: SpdPowerFwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SPD_DCS:SpdPower:FwCaenChannelDcsEasyPWS_FWMAJ is_of_class SpdPowerFwDevMajority_CLASS

objectset: SpdPowerFWDEVMAJORITY_FWSETSTATES is_of_class VOID {SPD_DCS:SpdPower:FwCaenChannelDcsEasyPWS_FWMAJ }
objectset: SpdPowerFWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SPD_DCS:SpdPower:FwCaenChannelDcsEasyPWS_FWMAJ }


objectset: SpdPowerFWCHILDREN_FWSETACTIONS union {SpdPowerFWCAENCRATESY1527DCS_FWSETACTIONS,
	SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETACTIONS,
	SpdPowerFWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdPowerFWCHILDREN_FWSETSTATES union {SpdPowerFWCAENCRATESY1527DCS_FWSETSTATES,
	SpdPowerFWCAENCHANNELDCSEASYPWS_FWSETSTATES,
	SpdPowerFWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdCoolingTOP_Ali_CaV_OT_CLASS
!panel: aliDcsCavFSM/CaVFSM_openBitmap.pnl
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state OFF )  move_to OFF

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state {RECOVERING,RUN} ) and
( any_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state {OFF,STANDBY,LOCKED} )  )  move_to MIXED

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state {OFF,STANDBY,RECOVERING,LOCKED} )  move_to NOT_READY

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES in_state MAINTENANCE )  move_to MAINTENANCE

        action: GO_STANDBY	!visible: 1
            do GO_STANDBY all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state STANDBY ) then
               move_to READY
            endif
            move_to NOT_READY
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state OFF ) then
               move_to READY
            endif
            move_to NOT_READY
        action: RECOVER	!visible: 2
            do LOCK CaV:SpdPlant:Loop10
            do LOCK CaV:SpdPlant:Loop09
            do LOCK CaV:SpdPlant:Loop08
            do LOCK CaV:SpdPlant:Loop05
            do LOCK CaV:SpdPlant:Loop04
            do LOCK CaV:SpdPlant:Loop03
            do LOCK CaV:SpdPlant:Loop02
            do LOCK CaV:SpdPlant:Loop01
            do LOCK CaV:SpdPlant:Loop07
            do LOCK CaV:SpdPlant:Loop06
            do GO_ON all_in SpdCoolingFWCAVLOOP_FWSETACTIONS
            do RECOVER all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state OFF ) then
               move_to READY
            endif
            move_to NOT_READY
        action: ALLOW_MAINTENANCE	!visible: 2
            do ALLOW_MAINTENANCE CaV:SpdPlant
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop10
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop09
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop08
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop05
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop04
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop03
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop02
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop01
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop07
            do ALLOW_MAINTENANCE CaV:SpdPlant:Loop06
            if ( any_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state MAINTENANCE )  then
               move_to READY
            endif
            move_to MAINTENANCE
        action: LOOPS_LOCK(string loopList = "all")	!visible: 1
            do LOCK_LOOP(loopList = loopList) all_in SpdCoolingFWCHILDREN_FWSETACTIONS
        action: LOOPS_OFF(string loopList = "all")	!visible: 1
            do CLOSE_LOOP(loopList = loopList)  all_in SpdCoolingFWCHILDREN_FWSETACTIONS
    state: NOT_READY	!color: FwStateOKNotPhysics
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state OFF )  move_to OFF

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state {RECOVERING,RUN} ) and
( any_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state {OFF,STANDBY,LOCKED} )  )  move_to MIXED

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state RUN ) and
( all_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state ON )  )  move_to READY

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES in_state MAINTENANCE )  move_to MAINTENANCE

        action: GO_READY	!visible: 2
            do LOCK CaV:SpdPlant:Loop10
            do LOCK CaV:SpdPlant:Loop09
            do LOCK CaV:SpdPlant:Loop08
            do LOCK CaV:SpdPlant:Loop05
            do LOCK CaV:SpdPlant:Loop04
            do LOCK CaV:SpdPlant:Loop03
            do LOCK CaV:SpdPlant:Loop02
            do LOCK CaV:SpdPlant:Loop01
            do LOCK CaV:SpdPlant:Loop07
            do LOCK CaV:SpdPlant:Loop06
            do GO_ON all_in SpdCoolingFWCAVLOOP_FWSETACTIONS
            do GO_RUN all_in SpdCoolingFWCAVPLANT_FWSETACTIONS
            if (  ( all_in SpdCoolingFWCAVLOOP_FWSETSTATES not_in_state ON ) or
                   ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES not_in_state RUN )  )  then
               move_to NOT_READY
            endif
            move_to READY
        action: RECOVER	!visible: 2
            do LOCK CaV:SpdPlant:Loop10
            do LOCK CaV:SpdPlant:Loop09
            do LOCK CaV:SpdPlant:Loop08
            do LOCK CaV:SpdPlant:Loop05
            do LOCK CaV:SpdPlant:Loop04
            do LOCK CaV:SpdPlant:Loop03
            do LOCK CaV:SpdPlant:Loop02
            do LOCK CaV:SpdPlant:Loop01
            do LOCK CaV:SpdPlant:Loop07
            do LOCK CaV:SpdPlant:Loop06
            do GO_ON all_in SpdCoolingFWCAVLOOP_FWSETACTIONS
            do RECOVER all_in SpdCoolingFWCAVPLANT_FWSETACTIONS
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state OFF ) then
               move_to READY
            endif
            move_to NOT_READY
        action: ALLOW_MAINTENANCE	!visible: 2
            do ALLOW_MAINTENANCE all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( any_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state MAINTENANCE )  then
               move_to NOT_READY
            endif
            move_to MAINTENANCE
        action: LOOPS_ON(string loopList = "all")	!visible: 1
            do OPEN_LOOP(loopList = loopList) all_in SpdCoolingFWCAVLOOP_FWSETACTIONS
        action: LOOPS_LOCK(string loopList = "all")	!visible: 1
            do LOCK_LOOP(loopList = loopList) all_in SpdCoolingFWCHILDREN_FWSETACTIONS
        action: LOOPS_OFF(string loopList = "all")	!visible: 1
            do CLOSE_LOOP(loopList = loopList) all_in SpdCoolingFWCHILDREN_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  stay_in_state

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state {RECOVERING,RUN} ) and
( any_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state {OFF,STANDBY,LOCKED} )  )  move_to MIXED

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES in_state RUN )  move_to READY

        when ( any_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state OFF )  move_to OFF

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state READY )  move_to NOT_READY

    state: MAINTENANCE	!color: FwStateOKNotPhysics
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR

        when ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state OFF )  move_to OFF

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state {RECOVERING,RUN} ) and
( any_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state {OFF,STANDBY,LOCKED} )  )  move_to MIXED

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state {OFF,STANDBY,RECOVERING,LOCKED} )  move_to NOT_READY

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state RUN ) and
( all_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state ON )  )  move_to READY

        action: EXIT_MAINTENANCE	!visible: 2
            do EXIT_MAINTENANCE CaV:SpdPlant
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop10
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop09
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop08
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop05
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop04
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop03
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop02
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop01
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop07
            do EXIT_MAINTENANCE CaV:SpdPlant:Loop06
            if ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state MAINTENANCE )  then
               move_to MAINTENANCE
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  stay_in_state

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state {RECOVERING,RUN} ) and
( any_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state {OFF,STANDBY,LOCKED} )  )  move_to MIXED

        when (  ( all_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state ON ) and
( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state RUN )  )  move_to READY

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES in_state MAINTENANCE )  move_to MAINTENANCE

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state ERROR )  move_to NOT_READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state OFF ) then
               move_to READY
            endif
            move_to NOT_READY
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state STANDBY )  move_to NOT_READY

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES in_state MAINTENANCE )  move_to MAINTENANCE

        action: GO_STANDBY	!visible: 1
            do GO_STANDBY all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state STANDBY ) then
               move_to READY
            endif
            move_to NOT_READY
        action: ALLOW_MAINTENANCE	!visible: 2
            do ALLOW_MAINTENANCE all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( any_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state MAINTENANCE )  then
               move_to READY
            endif
            move_to MAINTENANCE
    state: MIXED	!color: FwStateAttention1
        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR

        when (  ( all_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state {RECOVERING,RUN} ) and
( any_in SpdCoolingFWCAVLOOP_FWSETSTATES in_state {OFF,STANDBY,LOCKED} )  )  stay_in_state

        when ( all_in SpdCoolingFWCHILDREN_FWSETSTATES in_state RUN )  move_to READY

        when ( any_in SpdCoolingFWCAVPLANT_FWSETSTATES in_state OFF )  move_to OFF

        when ( any_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state READY )  move_to NOT_READY

        action: GO_STANDBY	!visible: 1
            do GO_STANDBY all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state STANDBY ) then
               move_to READY
            endif
            move_to NOT_READY
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdCoolingFWCHILDREN_FWSETACTIONS
            if ( all_in SpdCoolingFWCHILDREN_FWSETSTATES not_in_state OFF ) then
               move_to READY
            endif
            move_to NOT_READY
        action: LOOPS_ON(string loopList = "all")	!visible: 1
            do OPEN_LOOP(loopList = loopList) all_in SpdCoolingFWCAVLOOP_FWSETACTIONS
        action: LOOPS_OFF(string loopList = "all")	!visible: 1
            do CLOSE_LOOP(loopList = loopList)  all_in SpdCoolingFWCHILDREN_FWSETACTIONS

object: SpdCooling is_of_class SpdCoolingTOP_Ali_CaV_OT_CLASS

class: SpdCoolingFwCaVPlant_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdCoolingFWCAVPLANT_FWSETSTATES
            remove &VAL_OF_Device from SpdCoolingFWCAVPLANT_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdCoolingFWCAVPLANT_FWSETSTATES
            insert &VAL_OF_Device in SpdCoolingFWCAVPLANT_FWSETACTIONS
            move_to READY

object: SpdCoolingFwCaVPlant_FWDM is_of_class SpdCoolingFwCaVPlant_FwDevMode_CLASS


class: SpdCoolingFwCaVPlant_CLASS/associated
!panel: aliDcsCavFSM/CaVFSM_plantOperation.pnl
    state: ERROR	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY	!visible: 1
        action: GO_RUN	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_RUN	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 0
    state: RUN	!color: FwStateOKPhysics
        action: GO_OFF	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: ALLOW_MAINTENANCE	!visible: 0
    state: RECOVERING	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: GO_RUN	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 0
    state: MAINTENANCE	!color: FwStateOKNotPhysics
        action: EXIT_MAINTENANCE	!visible: 0
    state: NO_CONTROL	!color: FwStateAttention2

object: CaV:SpdPlant is_of_class SpdCoolingFwCaVPlant_CLASS

objectset: SpdCoolingFWCAVPLANT_FWSETSTATES is_of_class VOID {CaV:SpdPlant }
objectset: SpdCoolingFWCAVPLANT_FWSETACTIONS is_of_class VOID {CaV:SpdPlant }

class: SpdCoolingFwCaVLoop_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdCoolingFWCAVLOOP_FWSETSTATES
            remove &VAL_OF_Device from SpdCoolingFWCAVLOOP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdCoolingFWCAVLOOP_FWSETSTATES
            insert &VAL_OF_Device in SpdCoolingFWCAVLOOP_FWSETACTIONS
            move_to READY

object: SpdCoolingFwCaVLoop_FWDM is_of_class SpdCoolingFwCaVLoop_FwDevMode_CLASS


class: SpdCoolingFwCaVLoop_CLASS/associated
!panel: aliDcsCavFSM/CaVFSM_loopOperation.pnl
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY	!visible: 1
        action: GO_ON	!visible: 1
        action: OPEN_LOOP(string loopList = "")	!visible: 0
        action: LOCK	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_ON	!visible: 1
        action: OPEN_LOOP(string loopList = "")	!visible: 0
        action: LOCK	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: ON	!color: FwStateOKPhysics
        action: GO_OFF	!visible: 1
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: LOCK	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: LOCKED	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: GO_ON	!visible: 1
        action: OPEN_LOOP(string loopList = "")	!visible: 0
        action: ALLOW_MAINTENANCE	!visible: 0
    state: MAINTENANCE	!color: FwStateOKNotPhysics
        action: EXIT_MAINTENANCE	!visible: 0
    state: PURGED	!color: FwStateOKNotPhysics
    state: ERROR	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2

object: CaV:SpdPlant:Loop10 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop09 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop08 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop05 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop04 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop03 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop02 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop01 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop07 is_of_class SpdCoolingFwCaVLoop_CLASS

object: CaV:SpdPlant:Loop06 is_of_class SpdCoolingFwCaVLoop_CLASS

objectset: SpdCoolingFWCAVLOOP_FWSETSTATES is_of_class VOID {CaV:SpdPlant:Loop10,
	CaV:SpdPlant:Loop09,
	CaV:SpdPlant:Loop08,
	CaV:SpdPlant:Loop05,
	CaV:SpdPlant:Loop04,
	CaV:SpdPlant:Loop03,
	CaV:SpdPlant:Loop02,
	CaV:SpdPlant:Loop01,
	CaV:SpdPlant:Loop07,
	CaV:SpdPlant:Loop06 }
objectset: SpdCoolingFWCAVLOOP_FWSETACTIONS is_of_class VOID {CaV:SpdPlant:Loop10,
	CaV:SpdPlant:Loop09,
	CaV:SpdPlant:Loop08,
	CaV:SpdPlant:Loop05,
	CaV:SpdPlant:Loop04,
	CaV:SpdPlant:Loop03,
	CaV:SpdPlant:Loop02,
	CaV:SpdPlant:Loop01,
	CaV:SpdPlant:Loop07,
	CaV:SpdPlant:Loop06 }


objectset: SpdCoolingFWCHILDREN_FWSETACTIONS union {SpdCoolingFWCAVPLANT_FWSETACTIONS,
	SpdCoolingFWCAVLOOP_FWSETACTIONS } is_of_class VOID
objectset: SpdCoolingFWCHILDREN_FWSETSTATES union {SpdCoolingFWCAVPLANT_FWSETSTATES,
	SpdCoolingFWCAVLOOP_FWSETSTATES } is_of_class VOID

class: SpdServicesTOP_SpdServices_CLASS
!panel: SpdServices.pnl
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdServicesVME_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR  

        when (  ( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR                 

        when (  ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesALI_CAV_OT_FWSETSTATES not_in_state FAKE )  )move_to ERROR    

        when ( any_in SpdServicesSPDPOWER_FWSETSTATES in_state {NO_CONTROL,INTERLOCK,ERROR} )  move_to ERROR

        when ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {NO_CONTROL,ERROR} )  move_to ERROR

        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state END )  )  do ACKTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state READY )  )  do STARTTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do RESETTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do ACKTIMER
        when (  ( all_in SpdServicesVME_FWSETSTATES in_state READY )  and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state READY ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state  MIXED )  ) move_to MIXED

        when ( all_in SpdServicesFWCHILDREN_FWSETSTATES in_state READY )  move_to READY        

        when (  (  ( all_in SpdServicesVME_FWSETSTATES in_state READY )  or ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state READY )  ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {READY,NOT_READY,MINTENANCE,OFF,MIXED} )  ) move_to STANDBY        

        action: GO_STANDBY	!visible: 1
            move_to STANDBY
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
            do START(DELAY = 10) all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: RESETTIMER	!visible: 0
            do RESET all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: ACKTIMER	!visible: 0
            do ACKNOLEDGE all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS        
             move_to ERROR
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdServicesVME_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR  

        when (  ( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR                 

        when (  ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesALI_CAV_OT_FWSETSTATES not_in_state FAKE )  )move_to ERROR    

        when ( any_in SpdServicesSPDPOWER_FWSETSTATES in_state {NO_CONTROL,INTERLOCK,ERROR} )  move_to ERROR

        when ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {NO_CONTROL,ERROR} )  move_to ERROR

        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state END )  )  do ACKTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state READY )  )  do STARTTIMER

        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do RESETTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do ACKTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  stay_in_state  

        when ( any_in SpdServicesFWCHILDREN_FWSETSTATES not_in_state READY ) move_to STANDBY

        when (  ( all_in SpdServicesVME_FWSETSTATES in_state READY )  and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state READY ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state  MIXED )  ) move_to MIXED                

        when (  ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state OFF ) and
( all_in SpdServicesVME_FWSETSTATES in_state NOT_READY ) and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state NOT_READY )  )  move_to OFF

        action: SWITCH_OFF	!visible: 1
            move_to OFF
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
            do START(DELAY = 10) all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: RESETTIMER	!visible: 0
            do RESET all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: ACKTIMER	!visible: 0
            do ACKNOLEDGE all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS        
             move_to ERROR
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdServicesVME_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR  

        when (  ( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR                 

        when (  ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesALI_CAV_OT_FWSETSTATES not_in_state FAKE )  )move_to ERROR    

        when ( any_in SpdServicesSPDPOWER_FWSETSTATES in_state {NO_CONTROL,INTERLOCK,ERROR} )  move_to ERROR

        when ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {NO_CONTROL,ERROR} )  move_to ERROR

        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state END )  )  do ACKTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state READY )  )  do STARTTIMER

        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do RESETTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do ACKTIMER
        when (  ( all_in SpdServicesVME_FWSETSTATES in_state READY )  and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state READY ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state  MIXED )  ) move_to MIXED

        when ( all_in SpdServicesFWCHILDREN_FWSETSTATES in_state READY )  move_to READY        

        when (  ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state OFF ) and
( all_in SpdServicesVME_FWSETSTATES in_state NOT_READY ) and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state NOT_READY )  )  move_to OFF

        action: GO_READY	!visible: 1
            move_to READY
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
            do START(DELAY = 10) all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: RESETTIMER	!visible: 0
            do RESET all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: ACKTIMER	!visible: 0
            do ACKNOLEDGE all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS        
             move_to ERROR
    state: MIXED	!color: FwStateAttention1
        when (  ( all_in SpdServicesVME_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR  

        when (  ( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )move_to ERROR                 

        when (  ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesALI_CAV_OT_FWSETSTATES not_in_state FAKE )  )move_to ERROR    

        when ( any_in SpdServicesSPDPOWER_FWSETSTATES in_state {NO_CONTROL,INTERLOCK,ERROR} )  move_to ERROR

        when ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {NO_CONTROL,ERROR} )  move_to ERROR

        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state END )  )  do ACKTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state READY )  )  do STARTTIMER

        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do RESETTIMER
        when (  ( any_in SpdServicesVME_FWSETSTATES not_in_state {NO_CONTROL,ERROR} ) and
( all_in SpdServicesDCS_FSMTIMER_FWSETSTATES in_state WAIT )  )  do ACKTIMER
        when (  ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state OFF ) and
( all_in SpdServicesVME_FWSETSTATES in_state NOT_READY ) and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state NOT_READY )  )  move_to OFF

        when ( all_in SpdServicesFWCHILDREN_FWSETSTATES in_state READY )  move_to READY        

        when (  (  ( all_in SpdServicesVME_FWSETSTATES in_state NOT_READY ) or ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state NOT_READY )  ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {READY,NOT_READY,MINTENANCE,OFF,MIXED} )  ) move_to STANDBY        

        when (  (  ( all_in SpdServicesVME_FWSETSTATES in_state READY ) and ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state READY )  ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {NOT_READY,MINTENANCE,OFF} )  ) move_to STANDBY        

        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
            do START(DELAY = 10) all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: RESETTIMER	!visible: 0
            do RESET all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
        action: ACKTIMER	!visible: 0
            do ACKNOLEDGE all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS        
             move_to ERROR
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdServicesSPDPOWER_FWSETSTATES in_state {NO_CONTROL,INTERLOCK,ERROR} )  stay_in_state

        when ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state {NO_CONTROL,ERROR} )  stay_in_state

        when (  ( all_in SpdServicesVME_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  ) stay_in_state

        when (  ( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesSPDPOWER_FWSETSTATES not_in_state FAKE )  )stay_in_state                 

        when (  ( all_in SpdServicesSPDPOWER_FWSETSTATES in_state FAKE ) and 
( all_in SpdServicesALI_CAV_OT_FWSETSTATES not_in_state FAKE )  )stay_in_state    

        when (  ( all_in SpdServicesVME_FWSETSTATES in_state READY )  and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state READY ) and
( all_in SpdServicesALI_CAV_OT_FWSETSTATES in_state  MIXED )  ) move_to MIXED

        when ( all_in SpdServicesFWCHILDREN_FWSETSTATES in_state READY )  move_to READY        

        when (  ( any_in SpdServicesFWCHILDREN_FWSETSTATES not_in_state {READY,MIXED} ) and
( all_in SpdServicesFWCHILDREN_FWSETSTATES not_in_state {NO_CONTROL,INTERLOCK,ERROR} )  ) move_to STANDBY        

        when (  ( any_in SpdServicesALI_CAV_OT_FWSETSTATES in_state OFF ) and
( all_in SpdServicesVME_FWSETSTATES in_state NOT_READY ) and
( all_in SpdServicesSPDPOWER_FWSETSTATES in_state NOT_READY )  )  move_to OFF

        action: RECOVER	!visible: 1
            do ACKNOLEDGE all_in SpdServicesDCS_FSMTIMER_FWSETACTIONS
              move_to OFF

object: SpdServices is_of_class SpdServicesTOP_SpdServices_CLASS

class: SpdServicesVME_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdServicesVME_FWSETSTATES
            remove &VAL_OF_Device from SpdServicesVME_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdServicesVME_FWSETSTATES
            insert &VAL_OF_Device in SpdServicesVME_FWSETACTIONS
            move_to READY

object: SpdServicesVME_FWDM is_of_class SpdServicesVME_FwDevMode_CLASS


objectset: SpdServicesVME_FWSETSTATES is_of_class VOID {SPD_VME }
objectset: SpdServicesVME_FWSETACTIONS is_of_class VOID {SPD_VME }

class: SpdServicesSpdPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdServicesSPDPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdServicesSPDPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdServicesSPDPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdServicesSPDPOWER_FWSETACTIONS
            move_to READY

object: SpdServicesSpdPower_FWDM is_of_class SpdServicesSpdPower_FwDevMode_CLASS


objectset: SpdServicesSPDPOWER_FWSETSTATES is_of_class VOID {SpdPower }
objectset: SpdServicesSPDPOWER_FWSETACTIONS is_of_class VOID {SpdPower }

class: SpdServicesAli_CaV_OT_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdServicesALI_CAV_OT_FWSETSTATES
            remove &VAL_OF_Device from SpdServicesALI_CAV_OT_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdServicesALI_CAV_OT_FWSETSTATES
            insert &VAL_OF_Device in SpdServicesALI_CAV_OT_FWSETACTIONS
            move_to READY

object: SpdServicesAli_CaV_OT_FWDM is_of_class SpdServicesAli_CaV_OT_FwDevMode_CLASS


objectset: SpdServicesALI_CAV_OT_FWSETSTATES is_of_class VOID {SpdCooling }
objectset: SpdServicesALI_CAV_OT_FWSETACTIONS is_of_class VOID {SpdCooling }

class: SpdServicesDcs_FSMTimer_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdServicesDCS_FSMTIMER_FWSETSTATES
            remove &VAL_OF_Device from SpdServicesDCS_FSMTIMER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdServicesDCS_FSMTIMER_FWSETSTATES
            insert &VAL_OF_Device in SpdServicesDCS_FSMTIMER_FWSETACTIONS
            move_to READY

object: SpdServicesDcs_FSMTimer_FWDM is_of_class SpdServicesDcs_FSMTimer_FwDevMode_CLASS


class: SpdServicesDcs_FSMTimer_CLASS/associated
!panel: Dcs_FSMTimer.pnl
    parameters: string USER_DATA = ""
    state: READY	!color: FwStateOKPhysics
        action: START(int DELAY = 0,string SPECIFIC = "None")	!visible: 1
    state: WAIT	!color: FwStateAttention1
        action: RESET	!visible: 1
        action: START(int DELAY = 0,string SPECIFIC = "None")	!visible: 1
    state: END	!color: FwStateAttention2
        action: ACKNOLEDGE	!visible: 1
        action: START(int DELAY = 0,string SPECIFIC = "None")	!visible: 1

object: spdVmeTimer is_of_class SpdServicesDcs_FSMTimer_CLASS

objectset: SpdServicesDCS_FSMTIMER_FWSETSTATES is_of_class VOID {spdVmeTimer }
objectset: SpdServicesDCS_FSMTIMER_FWSETACTIONS is_of_class VOID {spdVmeTimer }


objectset: SpdServicesFWCHILDREN_FWSETACTIONS union {SpdServicesVME_FWSETACTIONS,
	SpdServicesSPDPOWER_FWSETACTIONS,
	SpdServicesALI_CAV_OT_FWSETACTIONS,
	SpdServicesDCS_FSMTIMER_FWSETACTIONS } is_of_class VOID
objectset: SpdServicesFWCHILDREN_FWSETSTATES union {SpdServicesVME_FWSETSTATES,
	SpdServicesSPDPOWER_FWSETSTATES,
	SpdServicesALI_CAV_OT_FWSETSTATES,
	SpdServicesDCS_FSMTIMER_FWSETSTATES } is_of_class VOID

class: TOP_SpdDCS_CLASS
!panel: FSM/SpdDCS.pnl
    parameters: string RunMode = "DEFAULT", int VersionN = 0, string CalibMode = "ALL"
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MIXED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) )  move_to MIXED

        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: INFRA_ONLY	!color: FwStateOKNotPhysics
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MIXED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state STANDBY ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state STANDBY ) )  move_to MIXED

        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                             
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: STANDBY	!color: FwStateOKNotPhysics
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MIXED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                        
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MIXED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: CONFIGURE(string run_mode = "DEFAULT",int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) then
                move_to STANDBY
            endif
        action: GO_BEAM_TUN	!visible: 1
            do GO_BEAM_TUNING all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) then
                move_to STANDBY
            endif
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                        
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MIXED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        when ( all_in ALIDCS_RUNCU_FWSETSTATES in_state PAR_REQUEST )  move_to PAR_REQUEST
        action: CONFIGURE(string run_mode = "DEFAULT",int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(version = VersionN, devices = "ALL") all_in SPDFEDCONTROL_FWSETACTIONS
        action: GO_STBY_CONF	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) then
                move_to BEAM_TUNING
            endif
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDSECTOR_FWSETACTIONS
            if ( all_in FWCHILDREN_FWSETSTATES not_in_state READY ) then
                move_to BEAM_TUNING
            endif
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS            
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: SOR(string run_type = "",string run_no = "",string sor_mode = "FULL",string ddl_list = "")	!visible: 1
            do SOR(run_type = run_type, run_no = run_no, ddl_list = ddl_list, sor_mode = sor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: REQUEST_PAR	!visible: 1
            do REQUEST_PAR all_in ALIDCS_RUNCU_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MIXED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( any_in SPDSECTOR_FWSETSTATES in_state ERROR ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        when ( all_in ALIDCS_RUNCU_FWSETSTATES in_state PAR_REQUEST )  move_to PAR_REQUEST
        action: GO_STBY_CONF	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) then
                move_to READY
            endif
            move_to STANDBY
        action: CALIBRATE(string run_number = "0",string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calibration_mode = calib_mode) all_in SPDFEDCONTROL_FWSETACTIONS
        action: CONFIGURE	!visible: 1
            do CONFIGURE all_in SPDFEDCONTROL_FWSETACTIONS
        action: DAQ_EOR	!visible: 1
            move_to BUSY
        action: GO_BEAM_TUN	!visible: 1
            do GO_BEAM_TUNING all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) then
                move_to READY
            endif
            move_to BEAM_TUNING
        action: SOR(string run_type = "",string run_no = "",string sor_mode = "FULL",string ddl_list = "")	!visible: 1
            do SOR(run_type = run_type, run_no = run_no, ddl_list = ddl_list, sor_mode = sor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                        
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_BEAM_TUNING all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) then
                move_to READY
            endif
            move_to BEAM_TUNING
        action: GO_SUPERSAFE	!visible: 1
            do GO_BEAM_TUNING all_in SPDSECTOR_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) then
                move_to READY
            endif
            move_to BEAM_TUNING
        action: REQUEST_PAR	!visible: 1
            do REQUEST_PAR all_in ALIDCS_RUNCU_FWSETACTIONS
    state: MIXED	!color: FwStateAttention2
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MIXED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) stay_in_state

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_REQUEST )
and ( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_PROGRESSING ) )  move_to BEAM_TUNING
        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and  
( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_REQUEST ) and 
( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_PROGRESSING ) and      
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING
        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and ( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_REQUEST )
and ( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_PROGRESSING ) )  move_to READY
        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and
( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_REQUEST ) and
( all_in ALIDCS_RUNCU_FWSETSTATES not_in_state PAR_PROGRESSING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY
        action: CALIBRATE(string run_number = "0",string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calibration_mode = calib_mode) all_in SPDFEDCONTROL_FWSETACTIONS
        action: CONFIGURE(string run_mode = "DEFAULT",int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(version = VersionN, devices = "ALL") all_in SPDFEDCONTROL_FWSETACTIONS
        action: DAQ_EOR	!visible: 1
            move_to BUSY
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                        
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: DOWNLOADING	!color: FwStateAttention1
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: STOP	!visible: 1
            do STOP_CONFIGURATION all_in SPDFEDCONTROL_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES in_state READY ) then
                move_to READY
            endif
            move_to MIXED
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                        
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: CALIBRATING	!color: FwStateAttention1
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( any_in FWCHILDREN_FWSETSTATES in_state DOWNLOADING ) or ( any_in FWCHILDREN_FWSETSTATES in_state CONFIGURING ) )  move_to DOWNLOADING

        when ( any_in FWCHILDREN_FWSETSTATES in_state CALIBRATING ) stay_in_state        

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_STBY_CONF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_BEAM_TUN

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) ) move_to MOVING_READY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: STOP	!visible: 1
            do STOP_CALIBRATION all_in SPDFEDCONTROL_FWSETACTIONS
            if ( all_in SPDSECTOR_FWSETSTATES in_state READY ) then
                move_to READY
            endif
            move_to MIXED
        action: SOR(string run_type = "",string run_no = "",string ddl_list = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19",string sor_mode = "FULL")	!visible: 1
            do SOR(run_type = run_type, run_no = run_no, ddl_list = ddl_list, sor_mode =  sor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS                            
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: BUSY	!color: FwStateAttention1
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        action: STOP	!visible: 1
            move_to READY
        action: RELEASE	!visible: 1
            move_to READY   
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                                 
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_BEAM_TUN ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) stay_in_state

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and 
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( all_in SPDSECTOR_FWSETSTATES not_in_state {MOVING_STBY_CONF,MOVING_BEAM_TUN,MOVING_READY} ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                    
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: MOVING_READY	!color: FwStateAttention1
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) stay_in_state

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and 
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( all_in SPDSECTOR_FWSETSTATES not_in_state {MOVING_STBY_CONF,MOVING_BEAM_TUN,MOVING_READY} ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS            
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) move_to ERROR

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) move_to ERROR

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state  MOVING_STBY_CONF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) stay_in_state

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and 
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state READY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MIXED )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( all_in SPDSECTOR_FWSETSTATES not_in_state {MOVING_STBY_CONF,MOVING_BEAM_TUN,MOVING_READY} ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to MIXED

        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                   
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: ERROR	!color: FwStateAttention3
        when ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) stay_in_state

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state ERROR ) and ( all_in SPDSERVICES_FWSETSTATES not_in_state {OFF,STANDBY} ) ) stay_in_state

        when (  ( all_in SPDSECTOR_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) stay_in_state

        when (  ( all_in SPDSERVICES_FWSETSTATES in_state FAKE ) and ( all_in FWCHILDREN_FWSETSTATES not_in_state FAKE )  ) stay_in_state

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {OFF,STANDBY} ) ) stay_in_state

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) ) move_to OFF

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) ) move_to INFRA_ONLY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state {STANDBY,READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to INFRA_ONLY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STANDBY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state MCM_ONLY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STANDBY

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to STBY_CONFIGURED

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED                

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) )  move_to BEAM_TUNING

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and 
( all_in SPDSECTOR_FWSETSTATES not_in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and ( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES in_state READY ) and 
( all_in SPDSERVICES_FWSETSTATES in_state {READY,MIXED} ) and  
( all_in SPDFEDCONTROL_FWSETSTATES in_state READY ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state OFF )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state MCM_ONLY )	and
( all_in SPDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDSECTOR_FWSETSTATES not_in_state BEAM_TUNING )	and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDSECTOR_FWSETSTATES not_in_state OFF ) and ( all_in SPDSERVICES_FWSETSTATES in_state OFF ) )  move_to MIXED

        action: RELEASE	!visible: 1
            do RELEASE all_in FWCHILDREN_FWSETACTIONS
            if ( all_in FWCHILDREN_FWSETSTATES not_in_state ERROR ) then
                move_to MIXED
            endif                    
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS            
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
    state: PAR_REQUEST	!color: FwStateAttention2
        when ( any_in FWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                             
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: RECOVER	!visible: 1
            do RECOVER all_in ALIDCS_RUNCU_FWSETACTIONS
            move_to PAR_PROGRESSING 
        action: ABORT_PAR	!visible: 1
            move_to MIXED
    state: PAR_PROGRESSING	!color: FwStateAttention1
        when ( any_in FWCHILDREN_FWSETSTATES in_state ERROR )  move_to ERROR
        when ( ( all_in SPDSECTOR_FWSETSTATES in_state READY ) and ( all_in ALIDCS_RUNCU_FWSETSTATES in_state RUN_OK ) )  move_to READY
        when ( ( all_in SPDSECTOR_FWSETSTATES in_state BEAM_TUNING ) and ( all_in ALIDCS_RUNCU_FWSETSTATES in_state RUN_OK ) )  move_to BEAM_TUNING
        action: GO_SAFE	!visible: 1
            do GO_SAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: GO_SUPERSAFE	!visible: 1
            do GO_SUPERSAFE all_in ALIDCSSAFE_CU_FWSETACTIONS
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
            do EOR(run_type = run_type, run_no = run_no, eor_mode = eor_mode) all_in ALIDCS_RUNCU_FWSETACTIONS            
        action: ACK_RUN_FAILURE	!visible: 1
            do RESET all_in ALIDCS_RUNCU_FWSETACTIONS                             
            do RESET all_in SPDFEDCONTROL_FWSETACTIONS    
        action: ABORT_PAR	!visible: 1
            move_to MIXED

object: SPD_DCS is_of_class TOP_SpdDCS_CLASS

class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDead ) move_to IncompleteDead
        when (  ( any_in FWCHILDRENMODE_FWSETSTATES in_state DEAD ) and ( any_in FWCHILDMODE_FWSETSTATES in_state MANUAL )  ) move_to IncompleteDead
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete )  move_to Incomplete
        when ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm,LockedOutPerm} )  move_to Incomplete
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDev )  move_to IncompleteDev
        when ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDead ) move_to IncompleteDead
        when (  ( any_in FWCHILDRENMODE_FWSETSTATES in_state DEAD ) and ( any_in FWCHILDMODE_FWSETSTATES in_state MANUAL )  ) move_to IncompleteDead
        when (  ( all_in FWCHILDMODE_FWSETSTATES in_state {Included,ExcludedPerm,LockedOutPerm} ) and
       ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state Incomplete )  )  move_to Complete
    state: IncompleteDev	!color: FwStateAttention1
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDead ) move_to IncompleteDead
        when (  ( any_in FWCHILDRENMODE_FWSETSTATES in_state DEAD ) and ( any_in FWCHILDMODE_FWSETSTATES in_state MANUAL )  ) move_to IncompleteDead
        when (  ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm,LockedOutPerm} ) or
       ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete )  )  move_to Incomplete
        when (  ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) and
       ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDev )  ) move_to Complete
    state: IncompleteDead	!color: FwStateAttention3
        when (  (  ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state DEAD ) or ( all_in FWCHILDMODE_FWSETSTATES not_in_state MANUAL )  ) and ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDead )  )  move_to Complete

object: SPD_DCS_FWCNM is_of_class FwChildrenMode_CLASS

class: ASS_FwChildrenMode_CLASS/associated
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
    state: IncompleteDead	!color: FwStateAttention3

object: SPD_DCS_RUN::SPD_DCS_RUN_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SPD_SAFE::SPD_SAFE_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_0::SpdSector_0_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_1::SpdSector_1_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_2::SpdSector_2_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_3::SpdSector_3_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_4::SpdSector_4_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_5::SpdSector_5_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_6::SpdSector_6_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_7::SpdSector_7_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_8::SpdSector_8_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdSector_9::SpdSector_9_FWCNM is_of_class ASS_FwChildrenMode_CLASS

objectset: FWCHILDRENMODE_FWSETSTATES is_of_class VOID

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            do Include(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: ReleaseAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded

object: SPD_DCS_FWM is_of_class FwMode_CLASS

class: ASS_FwMode_CLASS/associated
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Manual	!visible: 0
        action: Ignore	!visible: 0
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
        action: Manual(string OWNER = "")	!visible: 0
        action: Ignore(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
        action: ReleaseAll(string OWNER = "")	!visible: 1
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "")	!visible: 1
        action: Exclude(string OWNER = "")	!visible: 0
        action: Ignore	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
        action: SetInLocal	!visible: 0
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
        action: Exclude(string OWNER = "")	!visible: 0
        action: Manual	!visible: 0
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0

object: SPD_DCS_RUN::SPD_DCS_RUN_FWM is_of_class ASS_FwMode_CLASS

object: SPD_SAFE::SPD_SAFE_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_0::SpdSector_0_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_1::SpdSector_1_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_2::SpdSector_2_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_3::SpdSector_3_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_4::SpdSector_4_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_5::SpdSector_5_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_6::SpdSector_6_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_7::SpdSector_7_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_8::SpdSector_8_FWM is_of_class ASS_FwMode_CLASS

object: SpdSector_9::SpdSector_9_FWM is_of_class ASS_FwMode_CLASS

class: SPD_DCS_RUN_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
                insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Excluded )  do Exclude
        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Ignored )  move_to IGNORED

        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Manual )  move_to MANUAL

        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state Included ) then
                if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                        remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                        remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                        remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state Included ) then
                if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state Included ) then
                if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                        remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                        remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                        remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
              insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
              insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
              insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal SPD_DCS_RUN::SPD_DCS_RUN_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            !move_to Manual
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal SPD_DCS_RUN::SPD_DCS_RUN_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
              remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
              remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
              remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal SPD_DCS_RUN::SPD_DCS_RUN_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                 do SetInLocal SPD_DCS_RUN::SPD_DCS_RUN_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Included )  move_to INCLUDED

        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Excluded ) move_to EXCLUDED

        when ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
            insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state Included ) then
                if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
            remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                    remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                    remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETSTATES
                remove SPD_DCS_RUN::SPD_DCS_RUN from ALIDCS_RUNCU_FWSETACTIONS
                remove SPD_DCS_RUN::SPD_DCS_RUN_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
                insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
                insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_DCS_RUN::SPD_DCS_RUN_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_DCS_RUN::SPD_DCS_RUN_FWM
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETSTATES
                insert SPD_DCS_RUN::SPD_DCS_RUN in ALIDCS_RUNCU_FWSETACTIONS
                insert SPD_DCS_RUN::SPD_DCS_RUN_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SPD_DCS_RUN_FWM is_of_class SPD_DCS_RUN_FwChildMode_CLASS

class: SPD_SAFE_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
                insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
            remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SPD_SAFE::SPD_SAFE_FWM in_state Excluded )  do Exclude
        when ( SPD_SAFE::SPD_SAFE_FWM in_state Ignored )  move_to IGNORED

        when ( SPD_SAFE::SPD_SAFE_FWM in_state Manual )  move_to MANUAL

        when ( SPD_SAFE::SPD_SAFE_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state Included ) then
                if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                        remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                        remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                        remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
            insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state Included ) then
                if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state Included ) then
                if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SPD_SAFE::SPD_SAFE_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                        remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                        remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                        remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SPD_SAFE::SPD_SAFE_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
              insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
              insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
              insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SPD_SAFE::SPD_SAFE_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                 do SetInLocal SPD_SAFE::SPD_SAFE_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            !move_to Manual
            if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                 do SetInLocal SPD_SAFE::SPD_SAFE_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
              remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
              remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
              remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                 do SetInLocal SPD_SAFE::SPD_SAFE_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                 do SetInLocal SPD_SAFE::SPD_SAFE_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SPD_SAFE::SPD_SAFE_FWM in_state Included )  move_to INCLUDED

        when ( SPD_SAFE::SPD_SAFE_FWM in_state Excluded ) move_to EXCLUDED

        when ( SPD_SAFE::SPD_SAFE_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
            insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state Included ) then
                if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
            remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SPD_SAFE::SPD_SAFE_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                    remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                    remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SPD_SAFE::SPD_SAFE_FWM
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETSTATES
                remove SPD_SAFE::SPD_SAFE from ALIDCSSAFE_CU_FWSETACTIONS
                remove SPD_SAFE::SPD_SAFE_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
                insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
                insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SPD_SAFE::SPD_SAFE_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SPD_SAFE::SPD_SAFE_FWM
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETSTATES
                insert SPD_SAFE::SPD_SAFE in ALIDCSSAFE_CU_FWSETACTIONS
                insert SPD_SAFE::SPD_SAFE_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SPD_SAFE_FWM is_of_class SPD_SAFE_FwChildMode_CLASS

class: SpdSector_0_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_0::SpdSector_0_FWM in_state Excluded )  do Exclude
        when ( SpdSector_0::SpdSector_0_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_0::SpdSector_0_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_0::SpdSector_0_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state Included ) then
                if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                        remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_0::SpdSector_0_FWM not_in_state Included ) then
                if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state Included ) then
                if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_0::SpdSector_0_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                        remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_0::SpdSector_0_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_0::SpdSector_0_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
              insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
              insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_0::SpdSector_0_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                 do SetInLocal SpdSector_0::SpdSector_0_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            !move_to Manual
            if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                 do SetInLocal SpdSector_0::SpdSector_0_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
              remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
              remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                 do SetInLocal SpdSector_0::SpdSector_0_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                 do SetInLocal SpdSector_0::SpdSector_0_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_0::SpdSector_0_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_0::SpdSector_0_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_0::SpdSector_0_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state Included ) then
                if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
            remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_0::SpdSector_0_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_0::SpdSector_0_FWM
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETSTATES
                remove SpdSector_0::SpdSector_0 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_0::SpdSector_0_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_0::SpdSector_0_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_0::SpdSector_0_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_0::SpdSector_0_FWM
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETSTATES
                insert SpdSector_0::SpdSector_0 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_0::SpdSector_0_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_0_FWM is_of_class SpdSector_0_FwChildMode_CLASS

class: SpdSector_1_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_1::SpdSector_1_FWM in_state Excluded )  do Exclude
        when ( SpdSector_1::SpdSector_1_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_1::SpdSector_1_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_1::SpdSector_1_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state Included ) then
                if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                        remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_1::SpdSector_1_FWM not_in_state Included ) then
                if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state Included ) then
                if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_1::SpdSector_1_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                        remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_1::SpdSector_1_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_1::SpdSector_1_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
              insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
              insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_1::SpdSector_1_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                 do SetInLocal SpdSector_1::SpdSector_1_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            !move_to Manual
            if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                 do SetInLocal SpdSector_1::SpdSector_1_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
              remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
              remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                 do SetInLocal SpdSector_1::SpdSector_1_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                 do SetInLocal SpdSector_1::SpdSector_1_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_1::SpdSector_1_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_1::SpdSector_1_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_1::SpdSector_1_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state Included ) then
                if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
            remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_1::SpdSector_1_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_1::SpdSector_1_FWM
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETSTATES
                remove SpdSector_1::SpdSector_1 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_1::SpdSector_1_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_1::SpdSector_1_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_1::SpdSector_1_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_1::SpdSector_1_FWM
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETSTATES
                insert SpdSector_1::SpdSector_1 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_1::SpdSector_1_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_1_FWM is_of_class SpdSector_1_FwChildMode_CLASS

class: SpdSector_2_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_2::SpdSector_2_FWM in_state Excluded )  do Exclude
        when ( SpdSector_2::SpdSector_2_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_2::SpdSector_2_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_2::SpdSector_2_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state Included ) then
                if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                        remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_2::SpdSector_2_FWM not_in_state Included ) then
                if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state Included ) then
                if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_2::SpdSector_2_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                        remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_2::SpdSector_2_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_2::SpdSector_2_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
              insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
              insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_2::SpdSector_2_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                 do SetInLocal SpdSector_2::SpdSector_2_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            !move_to Manual
            if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                 do SetInLocal SpdSector_2::SpdSector_2_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
              remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
              remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                 do SetInLocal SpdSector_2::SpdSector_2_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                 do SetInLocal SpdSector_2::SpdSector_2_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_2::SpdSector_2_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_2::SpdSector_2_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_2::SpdSector_2_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state Included ) then
                if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
            remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_2::SpdSector_2_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_2::SpdSector_2_FWM
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETSTATES
                remove SpdSector_2::SpdSector_2 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_2::SpdSector_2_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_2::SpdSector_2_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_2::SpdSector_2_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_2::SpdSector_2_FWM
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETSTATES
                insert SpdSector_2::SpdSector_2 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_2::SpdSector_2_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_2_FWM is_of_class SpdSector_2_FwChildMode_CLASS

class: SpdSector_3_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_3::SpdSector_3_FWM in_state Excluded )  do Exclude
        when ( SpdSector_3::SpdSector_3_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_3::SpdSector_3_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_3::SpdSector_3_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state Included ) then
                if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                        remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_3::SpdSector_3_FWM not_in_state Included ) then
                if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state Included ) then
                if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_3::SpdSector_3_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                        remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_3::SpdSector_3_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_3::SpdSector_3_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
              insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
              insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_3::SpdSector_3_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                 do SetInLocal SpdSector_3::SpdSector_3_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            !move_to Manual
            if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                 do SetInLocal SpdSector_3::SpdSector_3_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
              remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
              remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                 do SetInLocal SpdSector_3::SpdSector_3_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                 do SetInLocal SpdSector_3::SpdSector_3_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_3::SpdSector_3_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_3::SpdSector_3_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_3::SpdSector_3_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state Included ) then
                if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
            remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_3::SpdSector_3_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_3::SpdSector_3_FWM
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETSTATES
                remove SpdSector_3::SpdSector_3 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_3::SpdSector_3_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_3::SpdSector_3_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_3::SpdSector_3_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_3::SpdSector_3_FWM
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETSTATES
                insert SpdSector_3::SpdSector_3 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_3::SpdSector_3_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_3_FWM is_of_class SpdSector_3_FwChildMode_CLASS

class: SpdSector_4_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_4::SpdSector_4_FWM in_state Excluded )  do Exclude
        when ( SpdSector_4::SpdSector_4_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_4::SpdSector_4_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_4::SpdSector_4_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state Included ) then
                if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                        remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_4::SpdSector_4_FWM not_in_state Included ) then
                if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state Included ) then
                if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_4::SpdSector_4_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                        remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_4::SpdSector_4_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_4::SpdSector_4_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
              insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
              insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_4::SpdSector_4_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                 do SetInLocal SpdSector_4::SpdSector_4_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            !move_to Manual
            if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                 do SetInLocal SpdSector_4::SpdSector_4_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
              remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
              remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                 do SetInLocal SpdSector_4::SpdSector_4_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                 do SetInLocal SpdSector_4::SpdSector_4_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_4::SpdSector_4_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_4::SpdSector_4_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_4::SpdSector_4_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state Included ) then
                if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
            remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_4::SpdSector_4_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_4::SpdSector_4_FWM
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETSTATES
                remove SpdSector_4::SpdSector_4 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_4::SpdSector_4_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_4::SpdSector_4_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_4::SpdSector_4_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_4::SpdSector_4_FWM
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETSTATES
                insert SpdSector_4::SpdSector_4 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_4::SpdSector_4_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_4_FWM is_of_class SpdSector_4_FwChildMode_CLASS

class: SpdSector_5_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_5::SpdSector_5_FWM in_state Excluded )  do Exclude
        when ( SpdSector_5::SpdSector_5_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_5::SpdSector_5_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_5::SpdSector_5_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state Included ) then
                if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                        remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_5::SpdSector_5_FWM not_in_state Included ) then
                if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state Included ) then
                if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_5::SpdSector_5_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                        remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_5::SpdSector_5_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_5::SpdSector_5_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
              insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
              insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_5::SpdSector_5_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                 do SetInLocal SpdSector_5::SpdSector_5_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            !move_to Manual
            if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                 do SetInLocal SpdSector_5::SpdSector_5_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
              remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
              remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                 do SetInLocal SpdSector_5::SpdSector_5_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                 do SetInLocal SpdSector_5::SpdSector_5_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_5::SpdSector_5_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_5::SpdSector_5_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_5::SpdSector_5_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state Included ) then
                if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
            remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_5::SpdSector_5_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_5::SpdSector_5_FWM
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETSTATES
                remove SpdSector_5::SpdSector_5 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_5::SpdSector_5_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_5::SpdSector_5_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_5::SpdSector_5_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_5::SpdSector_5_FWM
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETSTATES
                insert SpdSector_5::SpdSector_5 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_5::SpdSector_5_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_5_FWM is_of_class SpdSector_5_FwChildMode_CLASS

class: SpdSector_6_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_6::SpdSector_6_FWM in_state Excluded )  do Exclude
        when ( SpdSector_6::SpdSector_6_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_6::SpdSector_6_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_6::SpdSector_6_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state Included ) then
                if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                        remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_6::SpdSector_6_FWM not_in_state Included ) then
                if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state Included ) then
                if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_6::SpdSector_6_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                        remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_6::SpdSector_6_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_6::SpdSector_6_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
              insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
              insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_6::SpdSector_6_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                 do SetInLocal SpdSector_6::SpdSector_6_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            !move_to Manual
            if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                 do SetInLocal SpdSector_6::SpdSector_6_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
              remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
              remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                 do SetInLocal SpdSector_6::SpdSector_6_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                 do SetInLocal SpdSector_6::SpdSector_6_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_6::SpdSector_6_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_6::SpdSector_6_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_6::SpdSector_6_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state Included ) then
                if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
            remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_6::SpdSector_6_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_6::SpdSector_6_FWM
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETSTATES
                remove SpdSector_6::SpdSector_6 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_6::SpdSector_6_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_6::SpdSector_6_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_6::SpdSector_6_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_6::SpdSector_6_FWM
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETSTATES
                insert SpdSector_6::SpdSector_6 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_6::SpdSector_6_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_6_FWM is_of_class SpdSector_6_FwChildMode_CLASS

class: SpdSector_7_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_7::SpdSector_7_FWM in_state Excluded )  do Exclude
        when ( SpdSector_7::SpdSector_7_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_7::SpdSector_7_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_7::SpdSector_7_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state Included ) then
                if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                        remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_7::SpdSector_7_FWM not_in_state Included ) then
                if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state Included ) then
                if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_7::SpdSector_7_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                        remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_7::SpdSector_7_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_7::SpdSector_7_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
              insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
              insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_7::SpdSector_7_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                 do SetInLocal SpdSector_7::SpdSector_7_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            !move_to Manual
            if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                 do SetInLocal SpdSector_7::SpdSector_7_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
              remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
              remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                 do SetInLocal SpdSector_7::SpdSector_7_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                 do SetInLocal SpdSector_7::SpdSector_7_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_7::SpdSector_7_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_7::SpdSector_7_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_7::SpdSector_7_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state Included ) then
                if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
            remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_7::SpdSector_7_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_7::SpdSector_7_FWM
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETSTATES
                remove SpdSector_7::SpdSector_7 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_7::SpdSector_7_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_7::SpdSector_7_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_7::SpdSector_7_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_7::SpdSector_7_FWM
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETSTATES
                insert SpdSector_7::SpdSector_7 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_7::SpdSector_7_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_7_FWM is_of_class SpdSector_7_FwChildMode_CLASS

class: SpdSector_8_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_8::SpdSector_8_FWM in_state Excluded )  do Exclude
        when ( SpdSector_8::SpdSector_8_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_8::SpdSector_8_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_8::SpdSector_8_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state Included ) then
                if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                        remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_8::SpdSector_8_FWM not_in_state Included ) then
                if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state Included ) then
                if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_8::SpdSector_8_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                        remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_8::SpdSector_8_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_8::SpdSector_8_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
              insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
              insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_8::SpdSector_8_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                 do SetInLocal SpdSector_8::SpdSector_8_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            !move_to Manual
            if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                 do SetInLocal SpdSector_8::SpdSector_8_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
              remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
              remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                 do SetInLocal SpdSector_8::SpdSector_8_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                 do SetInLocal SpdSector_8::SpdSector_8_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_8::SpdSector_8_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_8::SpdSector_8_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_8::SpdSector_8_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state Included ) then
                if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
            remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_8::SpdSector_8_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_8::SpdSector_8_FWM
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETSTATES
                remove SpdSector_8::SpdSector_8 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_8::SpdSector_8_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_8::SpdSector_8_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_8::SpdSector_8_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_8::SpdSector_8_FWM
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETSTATES
                insert SpdSector_8::SpdSector_8 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_8::SpdSector_8_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_8_FWM is_of_class SpdSector_8_FwChildMode_CLASS

class: SpdSector_9_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
            remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdSector_9::SpdSector_9_FWM in_state Excluded )  do Exclude
        when ( SpdSector_9::SpdSector_9_FWM in_state Ignored )  move_to IGNORED

        when ( SpdSector_9::SpdSector_9_FWM in_state Manual )  move_to MANUAL

        when ( SpdSector_9::SpdSector_9_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state Included ) then
                if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                        remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdSector_9::SpdSector_9_FWM not_in_state Included ) then
                if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state Included ) then
                if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdSector_9::SpdSector_9_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                        remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                        remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                        remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdSector_9::SpdSector_9_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdSector_9::SpdSector_9_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
              insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
              insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
              insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdSector_9::SpdSector_9_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                 do SetInLocal SpdSector_9::SpdSector_9_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            !move_to Manual
            if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                 do SetInLocal SpdSector_9::SpdSector_9_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
              remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
              remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
              remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                 do SetInLocal SpdSector_9::SpdSector_9_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                 do SetInLocal SpdSector_9::SpdSector_9_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdSector_9::SpdSector_9_FWM in_state Included )  move_to INCLUDED

        when ( SpdSector_9::SpdSector_9_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdSector_9::SpdSector_9_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
            insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state Included ) then
                if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
            remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdSector_9::SpdSector_9_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                    remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                    remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdSector_9::SpdSector_9_FWM
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETSTATES
                remove SpdSector_9::SpdSector_9 from SPDSECTOR_FWSETACTIONS
                remove SpdSector_9::SpdSector_9_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdSector_9::SpdSector_9_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdSector_9::SpdSector_9_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdSector_9::SpdSector_9_FWM
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETSTATES
                insert SpdSector_9::SpdSector_9 in SPDSECTOR_FWSETACTIONS
                insert SpdSector_9::SpdSector_9_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdSector_9_FWM is_of_class SpdSector_9_FwChildMode_CLASS

objectset: FWCHILDMODE_FWSETSTATES is_of_class VOID {SPD_DCS_RUN_FWM,
	SPD_SAFE_FWM,
	SpdSector_0_FWM,
	SpdSector_1_FWM,
	SpdSector_2_FWM,
	SpdSector_3_FWM,
	SpdSector_4_FWM,
	SpdSector_5_FWM,
	SpdSector_6_FWM,
	SpdSector_7_FWM,
	SpdSector_8_FWM,
	SpdSector_9_FWM }
objectset: FWCHILDMODE_FWSETACTIONS is_of_class VOID {SPD_DCS_RUN_FWM,
	SPD_SAFE_FWM,
	SpdSector_0_FWM,
	SpdSector_1_FWM,
	SpdSector_2_FWM,
	SpdSector_3_FWM,
	SpdSector_4_FWM,
	SpdSector_5_FWM,
	SpdSector_6_FWM,
	SpdSector_7_FWM,
	SpdSector_8_FWM,
	SpdSector_9_FWM }

class: ASS_AliDcs_runCU_CLASS/associated
!panel: aliDcsRunUnit/aliDcsRunCU.pnl
    parameters: string run_no = "uninitialized"
    state: RUN_OK	!color: FwStateOKPhysics
        action: SOR(string run_type = "",string run_no = "",string ddl_list = "",string sor_mode = "")	!visible: 1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "FULL")	!visible: 1
        action: INHIBIT_RUN	!visible: 1
        action: REQUEST_PAR	!visible: 1
    state: SOR_PROGRESSING	!color: FwStateAttention1
    state: EOR_PROGRESSING	!color: FwStateAttention1
    state: SOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: EOR_FAILURE	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: RUN_INHIBIT	!color: FwStateOKNotPhysics
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "")	!visible: 1
        action: ALLOW_RUN	!visible: 1
    state: PAR_REQUEST	!color: FwStateAttention2
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "FULL")	!visible: 1
        action: RESET	!visible: 1
        action: RECOVER	!visible: 1
        action: ALLOW_RUN	!visible: 1
    state: PAR_PROGRESSING	!color: FwStateAttention1
        action: EOR(string run_type = "",string run_no = "",string eor_mode = "FULL")	!visible: 1
        action: RESET	!visible: 1
        action: ALLOW_RUN	!visible: 1

object: SPD_DCS_RUN::SPD_DCS_RUN is_of_class ASS_AliDcs_runCU_CLASS

objectset: ALIDCS_RUNCU_FWSETSTATES is_of_class VOID
objectset: ALIDCS_RUNCU_FWSETACTIONS is_of_class VOID

class: ASS_AliDcsSafe_CU_CLASS/associated
!panel: AliDcsSafe_CU.pnl
    state: SUPERSAFE	!color: FwStateOKNotPhysics
        action: RESTORE_SAFE	!visible: 1
    state: SAFE	!color: FwStateOKPhysics
        action: GO_SUPERSAFE	!visible: 1
    state: MOVING_SAFE	!color: FwStateAttention1
    state: MOVING_SUPERSAFE	!color: FwStateAttention1
    state: NOT_SAFE	!color: FwStateAttention2
        action: GO_SAFE	!visible: 1

object: SPD_SAFE::SPD_SAFE is_of_class ASS_AliDcsSafe_CU_CLASS

objectset: ALIDCSSAFE_CU_FWSETSTATES is_of_class VOID
objectset: ALIDCSSAFE_CU_FWSETACTIONS is_of_class VOID

class: ASS_SpdSector_CLASS/associated
!panel: FSM/SpdSector.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 1
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: GO_READY	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_READY	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: MIXED	!color: FwStateAttention1
    state: CONFIGURING	!color: FwStateAttention1
        action: STOP	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP	!visible: 1
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET	!visible: 1
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        action: RESET	!visible: 1
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: RELEASE	!visible: 1

object: SpdSector_0::SpdSector_0 is_of_class ASS_SpdSector_CLASS

object: SpdSector_1::SpdSector_1 is_of_class ASS_SpdSector_CLASS

object: SpdSector_2::SpdSector_2 is_of_class ASS_SpdSector_CLASS

object: SpdSector_3::SpdSector_3 is_of_class ASS_SpdSector_CLASS

object: SpdSector_4::SpdSector_4 is_of_class ASS_SpdSector_CLASS

object: SpdSector_5::SpdSector_5 is_of_class ASS_SpdSector_CLASS

object: SpdSector_6::SpdSector_6 is_of_class ASS_SpdSector_CLASS

object: SpdSector_7::SpdSector_7 is_of_class ASS_SpdSector_CLASS

object: SpdSector_8::SpdSector_8 is_of_class ASS_SpdSector_CLASS

object: SpdSector_9::SpdSector_9 is_of_class ASS_SpdSector_CLASS

objectset: SPDSECTOR_FWSETSTATES is_of_class VOID
objectset: SPDSECTOR_FWSETACTIONS is_of_class VOID

class: SpdServices_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SPDSERVICES_FWSETSTATES
            remove &VAL_OF_Device from SPDSERVICES_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SPDSERVICES_FWSETSTATES
            insert &VAL_OF_Device in SPDSERVICES_FWSETACTIONS
            move_to READY

object: SpdServices_FWDM is_of_class SpdServices_FwDevMode_CLASS


objectset: SPDSERVICES_FWSETSTATES is_of_class VOID {SpdServices }
objectset: SPDSERVICES_FWSETACTIONS is_of_class VOID {SpdServices }

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
!panel: FwDevMode.pnl
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: SPD_DCS:SPD_VME_FWDM is_of_class FwDevMode_CLASS

object: SPD_DCS:SpdCooling_FWDM is_of_class FwDevMode_CLASS

object: SPD_DCS:SpdPower_FWDM is_of_class FwDevMode_CLASS

object: SPD_DCS:SpdServices_FWDM is_of_class FwDevMode_CLASS

object: SPD_DCS_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES is_of_class VOID {SPD_DCS:SPD_VME_FWDM,
	SPD_DCS:SpdCooling_FWDM,
	SPD_DCS:SpdPower_FWDM,
	SPD_DCS:SpdServices_FWDM,
	SPD_DCS_FWDM }
objectset: FWDEVMODE_FWSETACTIONS is_of_class VOID {SPD_DCS:SPD_VME_FWDM,
	SPD_DCS:SpdCooling_FWDM,
	SPD_DCS:SpdPower_FWDM,
	SPD_DCS:SpdServices_FWDM,
	SPD_DCS_FWDM }

class: FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: FwDevMajority_FWDM is_of_class FwDevMajority_FwDevMode_CLASS


class: FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SPD_DCS:SpdSector_FWMAJ is_of_class FwDevMajority_CLASS

objectset: FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SPD_DCS:SpdSector_FWMAJ }
objectset: FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SPD_DCS:SpdSector_FWMAJ }

class: spdFEDControl_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SPDFEDCONTROL_FWSETSTATES
            remove &VAL_OF_Device from SPDFEDCONTROL_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SPDFEDCONTROL_FWSETSTATES
            insert &VAL_OF_Device in SPDFEDCONTROL_FWSETACTIONS
            move_to READY

object: spdFEDControl_FWDM is_of_class spdFEDControl_FwDevMode_CLASS


class: spdFEDControl_CLASS/associated
!panel: FSM/spdFEDControlDU.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_READY	!color: FwStateOKNotPhysics
        action: CONFIGURE	!visible: 1
        action: FORCE_TO_READY	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
        action: CONFIGURE	!visible: 1
        action: CALIBRATE(string calibration_mode = "ALL")	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
        action: RESET	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: STOP_CONFIGURATION	!visible: 1
        action: RESET	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: RECOVER	!visible: 1
        action: RESET	!visible: 1

object: spdFEElectonics is_of_class spdFEDControl_CLASS

objectset: SPDFEDCONTROL_FWSETSTATES is_of_class VOID {spdFEElectonics }
objectset: SPDFEDCONTROL_FWSETACTIONS is_of_class VOID {spdFEElectonics }


objectset: FWCHILDREN_FWSETACTIONS union {ALIDCS_RUNCU_FWSETACTIONS,
	ALIDCSSAFE_CU_FWSETACTIONS,
	SPDSECTOR_FWSETACTIONS,
	SPDSERVICES_FWSETACTIONS,
	FWDEVMAJORITY_FWSETACTIONS,
	SPDFEDCONTROL_FWSETACTIONS } is_of_class VOID
objectset: FWCHILDREN_FWSETSTATES union {ALIDCS_RUNCU_FWSETSTATES,
	ALIDCSSAFE_CU_FWSETSTATES,
	SPDSECTOR_FWSETSTATES,
	SPDSERVICES_FWSETSTATES,
	FWDEVMAJORITY_FWSETSTATES,
	SPDFEDCONTROL_FWSETSTATES } is_of_class VOID

