class: ASS_FwWienerCrate_CLASS/associated
!panel: FwWienerCrate.pnl
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON	!visible: 2
    state: PWS_FAILURE	!color: FwStateAttention3
        action: SWITCH_ON	!visible: 2
        action: RESET_VME_Bus	!visible: 2
    state: NO_STATE	!color: FwStateAttention1
