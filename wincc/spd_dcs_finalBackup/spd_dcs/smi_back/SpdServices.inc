class: ASS_SpdServices_CLASS/associated
!panel: SpdServices.pnl
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY	!visible: 1
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
        action: RESETTIMER	!visible: 0
        action: ACKTIMER	!visible: 0
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
        action: RESETTIMER	!visible: 0
        action: ACKTIMER	!visible: 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
        action: RESETTIMER	!visible: 0
        action: ACKTIMER	!visible: 0
    state: MIXED	!color: FwStateAttention1
        action: STARTTIMER(int DELAY = 5,string SPECIFIC = "NONE")	!visible: 0
        action: RESETTIMER	!visible: 0
        action: ACKTIMER	!visible: 0
    state: ERROR	!color: FwStateAttention3
        action: RECOVER	!visible: 1
