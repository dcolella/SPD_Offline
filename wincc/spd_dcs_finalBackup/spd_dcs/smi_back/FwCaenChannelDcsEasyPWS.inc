class: ASS_FwCaenChannelDcsEasyPWS_CLASS/associated
!panel: FwCaenChannel|dcsCaen\dcsCaenEasyPowerSupply.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON	!visible: 1
        action: RESET	!visible: 1
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: ER_REPAIR	!color: FwStateAttention2
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
    state: PWS_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: INTERLOCK	!color: FwStateAttention3
        action: SWITCH_ON	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: RESET	!visible: 1
        action: KILL	!visible: 1
