class: ASS_VME_CLASS/associated
!panel: VME.pnl
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF_ALL	!visible: 2
    state: NOT_READY	!color: FwStateOKNotPhysics
        action: SWITCH_ON_ALL	!visible: 2
    state: ERROR	!color: FwStateAttention3
        action: RESET_VME_bus	!visible: 2
        action: SWITCH_ON_ALL	!visible: 2
    state: NO_CONTROL	!color: FwStateAttention2
