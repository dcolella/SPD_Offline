class: ASS_FwCaVPlant_CLASS/associated
!panel: aliDcsCavFSM/CaVFSM_plantOperation.pnl
    state: ERROR	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY	!visible: 1
        action: GO_RUN	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_RUN	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 0
    state: RUN	!color: FwStateOKPhysics
        action: GO_OFF	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: ALLOW_MAINTENANCE	!visible: 0
    state: RECOVERING	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: LOCK_LOOP(string loopList = "")	!visible: 0
        action: CLOSE_LOOP(string loopList = "")	!visible: 0
        action: GO_STANDBY	!visible: 1
        action: GO_RUN	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 0
    state: MAINTENANCE	!color: FwStateOKNotPhysics
        action: EXIT_MAINTENANCE	!visible: 0
    state: NO_CONTROL	!color: FwStateAttention2
