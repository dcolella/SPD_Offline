class: SpdHSector_9_A_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Excluded )  do Exclude
        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Ignored )  move_to IGNORED

        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Manual )  move_to MANUAL

        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state Included ) then
                if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                        remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                        remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                        remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
            insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state Included ) then
                if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state Included ) then
                if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                        remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                        remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                        remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
              insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
              insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
              insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_A::SpdHSector_9_A_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            !move_to Manual
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_A::SpdHSector_9_A_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
              remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
              remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
              remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_A::SpdHSector_9_A_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_A::SpdHSector_9_A_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Included )  move_to INCLUDED

        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
            insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state Included ) then
                if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdHSector_9_A::SpdHSector_9_A_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdHSector_9_A::SpdHSector_9_A_FWM
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_A::SpdHSector_9_A from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_A::SpdHSector_9_A_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_A::SpdHSector_9_A_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_A::SpdHSector_9_A_FWM
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_A::SpdHSector_9_A in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_A::SpdHSector_9_A_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdHSector_9_A_FWM is_of_class SpdHSector_9_A_FwChildMode_CLASS

class: SpdHSector_9_C_FwChildMode_CLASS

!panel: FwChildMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state {Excluded, Manual} ) then
            !    else
                    move_to Excluded
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: Manual	!visible: 0
            do Manual SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore	!visible: 0
            do Ignore SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 1
            do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            move_to Excluded
        action: ExcludePerm(string OWNER = "")	!visible: 0
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 0
            move_to LockedOut
    state: Included	!color: FwStateOKPhysics
        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Excluded )  do Exclude
        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Ignored )  move_to IGNORED

        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Manual )  move_to MANUAL

        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Dead )  do Manual

        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state Included ) then
                if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                        do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                        remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                        remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                        remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !       else
            !            move_to Included
            !        endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 1
            do Manual(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 1
            do Ignore(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
            insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
        action: ExcludePerm(string OWNER = "")	!visible: 0
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state Included ) then
                if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to ExcludedPerm
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state Included ) then
                if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Dead ) then
                        do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                        remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                        remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                        remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                   else
                        move_to Included
                    endif
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to LockedOut
    state: Manual	!color: FwStateOKNotPhysics
        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Included )  move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Dead ) then
              move_to Manual
            endif
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
              insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
              insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
              insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
              if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Included ) then
                move_to Included
              endif
            move_to Manual
        action: Exclude(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_C::SpdHSector_9_C_FWM
            endif
            move_to Excluded
        action: Ignore	!visible: 0
            do Ignore SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            !move_to Manual
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_C::SpdHSector_9_C_FWM
            endif
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 1
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
              do ExcludeAll(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
              remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
              remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
              remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !endif
            !move_to Manual
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_C::SpdHSector_9_C_FWM
            endif
            move_to Excluded
        action: Manual	!visible: 0
            do Manual SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: Exclude&LockOut(string OWNER = "")	!visible: 1
                do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            !    move_to Excluded
            !endif
            !    else
            !    endif
            !else
            !endif
            !    move_to Excluded
            !endif
            !move_to Manual
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                 do SetInLocal SpdHSector_9_C::SpdHSector_9_C_FWM
            endif
            move_to LockedOut
    state: Ignored	!color: FwStateOKNotPhysics
        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Included )  move_to INCLUDED

        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Excluded ) move_to EXCLUDED

        when ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state Dead )  do Exclude

        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
            insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state Included ) then
                if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                endif
            else
                do Exclude(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Manual(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
            remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( SpdHSector_9_C::SpdHSector_9_C_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                    remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                    remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) SpdHSector_9_C::SpdHSector_9_C_FWM
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETSTATES
                remove SpdHSector_9_C::SpdHSector_9_C from SPDHALFSECTOR_FWSETACTIONS
                remove SpdHSector_9_C::SpdHSector_9_C_FWCNM from FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOut
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOutPerm	!visible: 0
            move_to LockedOutPerm
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            !    else
            !        move_to Excluded
            !    endif
            !else
            !endif
            !move_to Included
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state {Excluded, Manual} ) then
                move_to ExcludedPerm
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 1
            move_to LockedOut
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
    state: LockedOutPerm	!color: FwStateOKNotPhysics
        action: UnLockOut	!visible: 1
            move_to Excluded
        action: UnLockOut&Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            if ( SpdHSector_9_C::SpdHSector_9_C_FWM not_in_state Excluded ) then
            !    else
                    move_to LockedOutPerm
            !    endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) SpdHSector_9_C::SpdHSector_9_C_FWM
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETSTATES
                insert SpdHSector_9_C::SpdHSector_9_C in SPDHALFSECTOR_FWSETACTIONS
                insert SpdHSector_9_C::SpdHSector_9_C_FWCNM in FWCHILDRENMODE_FWSETSTATES
            endif
            move_to Included
        action: LockOut	!visible: 0
            move_to LockedOut

object: SpdHSector_9_C_FWM is_of_class SpdHSector_9_C_FwChildMode_CLASS

objectset: FWCHILDMODE_FWSETSTATES is_of_class VOID {SpdHSector_9_A_FWM,
	SpdHSector_9_C_FWM }
objectset: FWCHILDMODE_FWSETACTIONS is_of_class VOID {SpdHSector_9_A_FWM,
	SpdHSector_9_C_FWM }

class: ASS_SpdHalfSector_CLASS/associated
!panel: SpdHalfSector.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 1
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: GO_MCM_ONLY	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: GO_READY	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: GO_MCM_ONLY	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_READY	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: GO_MCM_ONLY	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: STOP	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP	!visible: 1
    state: MIXED	!color: FwStateAttention1
        action: GO_OFF	!visible: 1
        action: GO_MCM_ONLY	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: GO_READY	!visible: 1
        action: RESET	!visible: 1
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET	!visible: 1
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        action: RESET	!visible: 1
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: RELEASE	!visible: 1
        action: RESET	!visible: 1

object: SpdHSector_9_A::SpdHSector_9_A is_of_class ASS_SpdHalfSector_CLASS

object: SpdHSector_9_C::SpdHSector_9_C is_of_class ASS_SpdHalfSector_CLASS

objectset: SPDHALFSECTOR_FWSETSTATES is_of_class VOID
objectset: SPDHALFSECTOR_FWSETACTIONS is_of_class VOID

class: ASS_FwChildrenMode_CLASS/associated
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
    state: IncompleteDead	!color: FwStateAttention3

object: SpdHSector_9_A::SpdHSector_9_A_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: SpdHSector_9_C::SpdHSector_9_C_FWCNM is_of_class ASS_FwChildrenMode_CLASS

class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDead ) move_to IncompleteDead
        when (  ( any_in FWCHILDRENMODE_FWSETSTATES in_state DEAD ) and ( any_in FWCHILDMODE_FWSETSTATES in_state MANUAL )  ) move_to IncompleteDead
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete )  move_to Incomplete
        when ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm,LockedOutPerm} )  move_to Incomplete
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDev )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDead ) move_to IncompleteDead
        when (  ( any_in FWCHILDRENMODE_FWSETSTATES in_state DEAD ) and ( any_in FWCHILDMODE_FWSETSTATES in_state MANUAL )  ) move_to IncompleteDead
        when (  ( all_in FWCHILDMODE_FWSETSTATES in_state {Included,ExcludedPerm,LockedOutPerm} ) and
       ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state Incomplete )  )  move_to Complete
    state: IncompleteDev	!color: FwStateAttention1
        when ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDead ) move_to IncompleteDead
        when (  ( any_in FWCHILDRENMODE_FWSETSTATES in_state DEAD ) and ( any_in FWCHILDMODE_FWSETSTATES in_state MANUAL )  ) move_to IncompleteDead
        when (  ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm,LockedOutPerm} ) or
       ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete )  )  move_to Incomplete
        when (  ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDev )  ) move_to Complete
    state: IncompleteDead	!color: FwStateAttention3
        when (  (  ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state DEAD ) or ( all_in FWCHILDMODE_FWSETSTATES not_in_state MANUAL )  ) and ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDead )  )  move_to Complete

object: SpdSector_9_FWCNM is_of_class FwChildrenMode_CLASS

objectset: FWCHILDRENMODE_FWSETSTATES is_of_class VOID

class: ASS_FwMode_CLASS/associated
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Manual	!visible: 0
        action: Ignore	!visible: 0
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
        action: Manual(string OWNER = "")	!visible: 0
        action: Ignore(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
        action: ReleaseAll(string OWNER = "")	!visible: 1
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "")	!visible: 1
        action: Exclude(string OWNER = "")	!visible: 0
        action: Ignore	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
        action: SetInLocal	!visible: 0
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
        action: Exclude(string OWNER = "")	!visible: 0
        action: Manual	!visible: 0
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
        action: ExcludeAll(string OWNER = "")	!visible: 0

object: SpdHSector_9_A::SpdHSector_9_A_FWM is_of_class ASS_FwMode_CLASS

object: SpdHSector_9_C::SpdHSector_9_C_FWM is_of_class ASS_FwMode_CLASS

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            do Include(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: ReleaseAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Free(string OWNER = "")	!visible: 0
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded

object: SpdSector_9_FWM is_of_class FwMode_CLASS

class: TOP_SpdSector_CLASS
!panel: FSM/SpdSector.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state OFF )  ) move_to MIXED

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state MCM_ONLY )  then
               move_to MIXED
            endif
            move_to MCM_ONLY
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state MCM_ONLY )  ) move_to MIXED

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state OFF )  then
               move_to MIXED
            endif
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED            
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode= run_mode, version = version) all_in SPDHALFSECTOR_FWSETACTIONS
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING    

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED )  ) move_to MIXED

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state MCM_ONLY )  then
               move_to MIXED
            endif
            move_to MCM_ONLY         
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING   
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state READY )  then
               move_to MIXED
            endif
            move_to READY                        
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode = run_mode, version = version) all_in SPDHALFSECTOR_FWSETACTIONS
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calib_mode = calib_mode) all_in SPDHALFSECTOR_FWSETACTIONS
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state BEAM_TUNING )  ) move_to MIXED

        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED               
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state READY )  then
               move_to MIXED
            endif
            move_to READY 
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode = run_mode, version = version) all_in SPDHALFSECTOR_FWSETACTIONS
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calib_mode = calib_mode) all_in SPDHALFSECTOR_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING    

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state READY ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state READY )  ) move_to MIXED

        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED            
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING    
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode = run_mode, version = version) all_in SPDHALFSECTOR_FWSETACTIONS
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calib_mode = calib_mode) all_in SPDHALFSECTOR_FWSETACTIONS
    state: MIXED	!color: FwStateAttention1
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

    state: CONFIGURING	!color: FwStateAttention1
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING    

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to READY 

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state CONFIGURING )  ) move_to MIXED

        action: STOP	!visible: 1
            do STOP all_in SPDHALFSECTOR_FWSETACTIONS
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING    

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to READY

        when (  ( any_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) and
( any_in SPDHALFSECTOR_FWSETSTATES not_in_state CALIBRATING )  ) move_to MIXED

        action: STOP	!visible: 1
            do STOP all_in SPDHALFSECTOR_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES not_in_state {MOVING_STBY_CONF,MOVING_BEAM_TUN,MOVING_READY} ) move_to MIXED

        action: RESET	!visible: 1
            move_to OFF
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES not_in_state {MOVING_STBY_CONF,MOVING_BEAM_TUN,MOVING_READY} ) move_to MIXED

        action: RESET	!visible: 1
            move_to OFF            
    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR ) move_to ERROR

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MIXED ) move_to MIXED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY

        when ( all_in SPDHALFSECTOR_FWSETSTATES not_in_state {MOVING_STBY_CONF,MOVING_BEAM_TUN,MOVING_READY} ) move_to MIXED

        action: RESET	!visible: 1
            move_to OFF            
    state: ERROR	!color: FwStateAttention3
        when ( all_in SPDHALFSECTOR_FWSETSTATES not_in_state ERROR ) move_to MIXED

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSECTOR_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state MCM_ONLY ) move_to MCM_ONLY

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SPDHALFSECTOR_FWSETSTATES in_state READY ) move_to READY     

        action: RELEASE	!visible: 1
            do RELEASE all_in SPDHALFSECTOR_FWSETACTIONS
            if ( any_in SPDHALFSECTOR_FWSETSTATES in_state ERROR )  then
                move_to ERROR
            endif
            move_to MIXED

object: SpdSector_9 is_of_class TOP_SpdSector_CLASS


objectset: FWCHILDREN_FWSETACTIONS union {SPDHALFSECTOR_FWSETACTIONS } is_of_class VOID
objectset: FWCHILDREN_FWSETSTATES union {SPDHALFSECTOR_FWSETSTATES } is_of_class VOID

