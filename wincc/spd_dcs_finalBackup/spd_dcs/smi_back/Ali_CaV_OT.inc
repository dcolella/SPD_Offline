class: ASS_Ali_CaV_OT_CLASS/associated
!panel: aliDcsCavFSM/CaVFSM_openBitmap.pnl
    state: READY	!color: FwStateOKPhysics
        action: GO_STANDBY	!visible: 1
        action: GO_OFF	!visible: 1
        action: RECOVER	!visible: 2
        action: ALLOW_MAINTENANCE	!visible: 2
        action: LOOPS_LOCK(string loopList = "all")	!visible: 1
        action: LOOPS_OFF(string loopList = "all")	!visible: 1
    state: NOT_READY	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RECOVER	!visible: 2
        action: GO_OFF	!visible: 1
        action: ALLOW_MAINTENANCE	!visible: 2
        action: LOOPS_ON(string loopList = "all")	!visible: 1
        action: LOOPS_LOCK(string loopList = "all")	!visible: 1
        action: LOOPS_OFF(string loopList = "all")	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
    state: MAINTENANCE	!color: FwStateOKNotPhysics
        action: EXIT_MAINTENANCE	!visible: 2
    state: ERROR	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY	!visible: 1
        action: ALLOW_MAINTENANCE	!visible: 2
    state: MIXED	!color: FwStateAttention1
        action: GO_STANDBY	!visible: 1
        action: GO_OFF	!visible: 1
        action: LOOPS_ON(string loopList = "all")	!visible: 1
        action: LOOPS_OFF(string loopList = "all")	!visible: 1
