class: ASS_spdFEDControl_CLASS/associated
!panel: FSM/spdFEDControlDU.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_READY	!color: FwStateOKNotPhysics
        action: CONFIGURE	!visible: 1
        action: FORCE_TO_READY	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
        action: CONFIGURE	!visible: 1
        action: CALIBRATE(string calibration_mode = "ALL")	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
        action: RESET	!visible: 1
    state: CONFIGURING	!color: FwStateAttention1
        action: STOP_CONFIGURATION	!visible: 1
        action: RESET	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: RECOVER	!visible: 1
        action: RESET	!visible: 1
