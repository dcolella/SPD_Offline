Version: aliDcsScreenshot 1.2.0
Author:  ALICE DCS, 2014-07-22

How to install:
- run fwInstallation.pnl 
- in Advanced Options, select the PROJECT path (not the PROJECT_FW) and install

Configure:
- edit the XML file ./panels/aliDcsScreenshot_panels.xml
- list all the panels you want to export as screenshot
- respect the sample tags:
  - name: only used for debugging purposes
  - pngPath: path and filename of the PNG to be published on the web
             *** PLEASE prefix with DDD (your detector code) ***
  - panelPath: source panel name
  - keepOpenSeconds: if the panel shows trends or several data, allow enough time to load the data completely

Remote path:
  - use the following as remote destination for web publication
    \\alidcsfs001\Scratch\dcs\DcsMonitoring\Screenshots\Panels 

Activate:
- the component installation has inserted a new UI manager in the console, in "manual" mode
- after configuration, test the panel in the GEDI environment. When ready, run the it automatically using the provided manager in silentMode

