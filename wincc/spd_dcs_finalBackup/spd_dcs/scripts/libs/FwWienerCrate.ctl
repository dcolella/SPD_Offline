FwWienerCrate_initialize(string domain, string device)
{
}

FwWienerCrate_valueChanged( string domain, string device,
      bool General_dot_Status_dot_GetPowerOn,
      bool General_dot_Status_dot_SystemFailure,
      bool General_dot_Status_dot_UserBusFailure,
      bool General_dot_Status_dot_InputFailure,
      bool General_dot_Status_dot_OutputFailure,
      bool General_dot_Status_dot_SensorFailure, string &fwState )
{
	if (
	(General_dot_Status_dot_GetPowerOn == 1) &&
	(General_dot_Status_dot_SystemFailure == 0) &&
	(General_dot_Status_dot_UserBusFailure == 0) &&
	(General_dot_Status_dot_InputFailure == 0) &&
	(General_dot_Status_dot_OutputFailure == 0) &&
	(General_dot_Status_dot_SensorFailure == 0) )
	{
		fwState = "ON";
	}
	else if (
	(General_dot_Status_dot_GetPowerOn == 0) &&
	(General_dot_Status_dot_SystemFailure == 0) &&
	(General_dot_Status_dot_UserBusFailure == 0) &&
	(General_dot_Status_dot_OutputFailure == 0) &&
	(General_dot_Status_dot_InputFailure == 0) &&
	(General_dot_Status_dot_SensorFailure == 0) )
	{
		fwState = "OFF";
	}
	else if (
	(General_dot_Status_dot_OutputFailure == 1) ||
	(General_dot_Status_dot_InputFailure == 1) ||
	(General_dot_Status_dot_SystemFailure == 1) ||
	(General_dot_Status_dot_UserBusFailure == 1) ||
	(General_dot_Status_dot_SensorFailure == 1) )
	{
		fwState = "PWS_FAILURE";
	}
	else 
	{
		fwState = "NO_STATE";
	}
}


FwWienerCrate_doCommand(string domain, string device, string command)
{
  string currentState;
	if (command == "SWITCH_OFF")
	{
		dpSet(device+".General.Commands.OnOffCrate",0);
	}
	if (command == "SWITCH_ON")
	{
                fwDU_getState(domain, device, currentState);
                fwDU_setState(domain, device, currentState);
		dpSet(device+".General.Commands.OnOffCrate",1);
	}
	if (command == "RESET_VME_Bus")
	{
                fwDU_getState(domain, device, currentState);
                fwDU_setState(domain, device, currentState);
		dpSet(device+".General.Commands.VMESysreset",1);
	}
}


