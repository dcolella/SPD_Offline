#uses "SpdHSConfig.ctl"
#uses "SpdHSScan.ctl"
#uses "spdConfDataSourceManager.ctl"
#uses "spd_run/spdRunCU.ctl"
#uses "aliFeroCC/feroCC.ctc"

spdFEDControl_initialize(string domain, string device)
{
}




spdFEDControl_valueChanged( string domain, string device,
      int FEelect_dot_DetectorConfig,
      int FEelect_dot_RoutersConfig,
      int FEDServer_dot_OperationStatus, string &fwState )
{ 
  if (FEDServer_dot_OperationStatus ==3)
	{
		fwState = "CONFIGURING";
	} else if (FEDServer_dot_OperationStatus == 2)
	{
    	fwState = "CALIBRATING";
	} else if (
	(FEDServer_dot_OperationStatus == 1) &&
	(FEelect_dot_DetectorConfig >= 0) &&
	(FEelect_dot_RoutersConfig >= 0) )
	{
		fwState = "READY";
	} else if ((FEDServer_dot_OperationStatus == 1)&&
	((FEelect_dot_DetectorConfig < 0) ||
	(FEelect_dot_RoutersConfig < 0) ))
	{
		fwState = "NOT_READY";
	}
	else 
	{
		fwState = "ERROR";
	}
}





spdFEDControl_doCommand(string domain, string device, string command)
{
	if (command == "CONFIGURE")
	{
           int error,requiredVersion;
           dyn_string configParam, info, infoDetails;

           // the configuration needs to be tested!!!!            
           DebugTN("Starting Configuration");
           dpSet(device+".FEDServer.OperationStatus", 3); // 3 means that the FSM will be moved in configuring
           
           requiredVersion = spdRunCU_requiredVersion(info);  
           DebugTN("requiredVersion = "+requiredVersion);         
          //checks that the SPD is configured with the required version
          //spdRunCU_checkSPDVersion: 0 = SPD ok, 1= SPD to be configured, -1= problems detected from ACT side
           // or ACT DB connection, -2= problem in SPD DB connection
           int isConfigNeeded = spdRunCU_checkSPDVersion();
           
          //in case it is needed the full SPD will be configured with the required version
           if(isConfigNeeded == 1){
             //load right version and configure SPD
            spdRunCU_HardwareSynch();
            //loads the required version in the FED
             error = HSGetDbData(requiredVersion);
             //DebugTN("Get Version from DB. Error:", error);
             if(error) {
               configParam = makeDynString("error","SPD DB connection: HSGetDbData returned with an error");
               dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", configParam);
               dpSet(device+".FEDServer.OperationStatus", -1);
               return;
             }
             
            //full configuration: (CnfRout,ResetDetector,DPIcnf,APIcnf,PIXcnf)
             DebugTN("Ready to configure the SPD");
             error = SpdHSConfig_AutoConfigList(true,true,true,true,true);
             //DebugTN("SpdHSConfig_AutoConfigList returns error " , error);
             if(error) {
               configParam = makeDynString("error","error while configuring the SPD: SpdHSConfig_AutoConfigList", 
                                           "Try again or call an SPD expert in case of problems");
               dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", configParam);
               dpSet(device+".FEDServer.OperationStatus", -1);
               return;
             }
             DebugTN("Ready to mask noisy Pixels with the last version");
             error = spdRunCU_maskNoisyPixels();          
             if(error) {
               configParam = makeDynString("error","error while masking the SPD: spdRunCU_maskNoisyPixels", 
                                           "Try again or call an SPD expert in case of problems");
               dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", configParam);
               dpSet(device+".FEDServer.OperationStatus", -1); // this means that FSM will be moved in error
               }else {
                 // reads the required parameters and seve them in the applied: configuration done
                 dpGet( "dcs_rct:aliDcsRctSpd.requested_conf.config_params", configParam); 
                 dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", configParam);
                 dpSet(device+".FEDServer.OperationStatus", 1,
                     device+".FEelect.DetectorConfig", requiredVersion,
                     device+".FEelect.RoutersConfig", 1);
               }
           }
           else if(isConfigNeeded == 0) {
            delay(3);
            // reads the required parameters and seve them in the applied: configuration done
            dpGet( "dcs_rct:aliDcsRctSpd.requested_conf.config_params", configParam); 
            dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", configParam);             
            dpSet(device+".FEDServer.OperationStatus", 1,
                device+".FEelect.DetectorConfig", requiredVersion,
                device+".FEelect.RoutersConfig", 1);   
            } else if(isConfigNeeded == -1) {
             delay(2);
             dpGet( "dcs_rct:aliDcsRctSpd.requested_conf.config_params", configParam); 
             infoDetails = makeDynString("ignored",info, configParam);
             dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", infoDetails);
             dpSet(device+".FEDServer.OperationStatus", 1,
                     device+".FEelect.DetectorConfig", 1,
                     device+".FEelect.RoutersConfig", 1);
             //dpSet(device+".FEDServer.OperationStatus", -1); // this means that FSM will be moved in error
            } else if(isConfigNeeded == -2) {
             delay(2);
             dpGet( "dcs_rct:aliDcsRctSpd.requested_conf.config_params", configParam); 
             infoDetails = makeDynString("ignored","problem in SPD DB connection", "expert investigation is needed");
             dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", infoDetails);
             dpSet(device+".FEDServer.OperationStatus", 1,
                     device+".FEelect.DetectorConfig", -1,
                     device+".FEelect.RoutersConfig", -1);
             //dpSet(device+".FEDServer.OperationStatus", -1); // this means that FSM will be moved in error
            } else  {
             delay(2);
             dpGet( "dcs_rct:aliDcsRctSpd.requested_conf.config_params", configParam); 
             infoDetails = makeDynString("error","Error not recognized", "expert investigation is needed");
             dpSetWait( "dcs_rct:aliDcsRctSpd.applied_conf.config_params", infoDetails);
             dpSet(device+".FEDServer.OperationStatus", -1); // this means that FSM will be moved in error
          }

  }
	if (command == "RESET")
	{
           dpSet(device+".FEDServer.OperationStatus", 1,
                device+".FEelect.DetectorConfig", -1,
                device+".FEelect.RoutersConfig", -1);           
	}
	if (command == "CALIBRATE")
	{
            dpSet(device+".FEDServer.OperationStatus", 2);   
	
            string calib_mode;
	    fwDU_getCommandParameter(domain, device, "calibration_mode", calib_mode);
            
            if(calib_mode == "DAQ_MIN_TH_SCAN"){
              DebugTN("MinThScan\n"); 
             // MinTHScanAll();
            } else if(calib_mode == "DAQ_UNIFORMITY_SCAN") {
               DebugTN("UnifScan\n"); 
             //  UnifScanAll();
            } else if(calib_mode == "DAQ_DAC_PIX_SCAN"){
                DebugTN("DACScan\n"); 
             //  DACScanAll();
            }else if(calib_mode == "DAQ_DELAY_SCAN"){
                DebugTN("DelayScan\n"); 
               //DelayScanAll();
            }else if(calib_mode == "DAQ_MEAN_TH_SCAN"){
            }else if(calib_mode == "DAQ_NOISY_PIX_SCAN"){
            }
         }
	if (command == "STOP_CALIBRATION")
	{
           dpSet(device+".FEDServer.OperationStatus", 1,
                device+".FEelect.DetectorConfig", -1,
                device+".FEelect.RoutersConfig", -1); 
	}
	if (command == "STOP_CONFIGURATION")
	{
           dpSet(device+".FEDServer.OperationStatus", 1,
                device+".FEelect.DetectorConfig", -1,
                device+".FEelect.RoutersConfig", -1); 
	}
	if (command == "RECOVER")
	{
           dpSet(device+".FEDServer.OperationStatus", 1,
                device+".FEelect.DetectorConfig", -1,
                device+".FEelect.RoutersConfig", -1);           

  }
  if (command == "FORCE_TO_READY")
	{
           dpSet(device+".FEDServer.OperationStatus", 1,
                device+".FEelect.DetectorConfig", 1,
                device+".FEelect.RoutersConfig", 1);           

  }
}




