#uses "SPD_DCS$FwWienerCanBus$install"
#uses "SPD_DCS$FwWienerCrate$install"
#uses "SPD_DCS$FwCaenCrateSY1527Dcs$install"
#uses "SPD_DCS$FwCaenChannelDcsEasyPWS$install"
#uses "SPD_DCS$FwDevMajority$install"
#uses "SPD_DCS$FwCaVPlant$install"
#uses "SPD_DCS$FwCaVLoop$install"
#uses "SPD_DCS$Dcs_FSMTimer$install"
#uses "SPD_DCS$FwDevMode$install"
#uses "SPD_DCS$spdFEDControl$install"

startDomainDevices_SPD_DCS()
{
	fwFsm_startDomainDevicesNew("SPD_DCS");
}
