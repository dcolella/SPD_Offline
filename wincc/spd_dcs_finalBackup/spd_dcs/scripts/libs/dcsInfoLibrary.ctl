// $License: NOLICENSE
// ------------------------------------------
/* 					History

	27/02/2008 - Created

        
*/

// constant definition
const bool IS_DEBUGOUTPUT = true;
const string SYN_INFOSERVER = "dcs_ui:"; 

// Names of DPT Used
const string HMPDPT_INFOEXCHANGE = "AliDcs_InfoExchange";
const string DPN_INFOEXCHANGE = "aliDcs_Info";

    
// global variables definition
global string sysPrefix = "";
global dyn_string dsDpNames;

// ----------------------------------------------------------------------------
// __dcsInfo_createInfoDpt (internal use only)
/*
	This function create the  DpType. 
	
	F.A. ver 1.0   27/02/2008
	
	History
	
	  		
*/
int __dcsInfo_createInfoDpt()
{

int n;
dyn_dyn_string depes;
dyn_dyn_int depei;

	// Create the data type
	depes[1] = makeDynString (HMPDPT_INFOEXCHANGE,"","");
	depes[2] = makeDynString ("","tx","");
	depes[3] = makeDynString ("","","destination");
	depes[4] = makeDynString ("","","message");
	depes[5] = makeDynString ("","","user");
	depes[6] = makeDynString ("","","system");
	depes[7] = makeDynString ("","rx","");
	depes[8] = makeDynString ("","","destination");
	depes[9] = makeDynString ("","","message");
	depes[10] = makeDynString ("","","user");
	depes[11] = makeDynString ("","","system");


	depei[1] = makeDynInt (DPEL_STRUCT);
	depei[2] = makeDynInt (0,DPEL_STRUCT);
	depei[3] = makeDynInt (0,0,DPEL_STRING);
	depei[4] = makeDynInt (0,0,DPEL_STRING);
	depei[5] = makeDynInt (0,0,DPEL_STRING);
	depei[6] = makeDynInt (0,0,DPEL_STRING);
	depei[7] = makeDynInt (0,DPEL_STRUCT);
	depei[8] = makeDynInt (0,0,DPEL_STRING);
	depei[9] = makeDynInt (0,0,DPEL_STRING);
	depei[10] = makeDynInt (0,0,DPEL_STRING);
	depei[11] = makeDynInt (0,0,DPEL_STRING);
	// Create the datapoint type
	n = dpTypeCreate(depes,depei);
	DebugTN ("__dcsInfo_createInfoDpt : created INFO DPT : ",n);
	return(n);
}
// -------------------------
int dcsInfo_getDetectorsPrefix(dyn_string &dsDetPre )
{
  dynClear(dsDetPre);
  dsDetPre = makeDynString("BROADCAST","DCS","ACO","EMC","FMD","HMP","MCH","MTR",
                         "PHO","PMD","SDD","SPD","SSD","TOF","TPC","TRD",
                         "T00","V00","ZDC");
  return(dynlen(dsDetPre));
}

string dcsInfo_getRxExchangeDPE(string type = "MESSAGE")
{
  switch(type) {
    case "MESSAGE":
      return( getSystemName()+DPN_INFOEXCHANGE+".rx.message" );
      break;
    case "USER":
      return( getSystemName()+DPN_INFOEXCHANGE+".rx.user" );
      break;
    case "DESTINATION":
      return( getSystemName()+DPN_INFOEXCHANGE+".rx.destination" );
      break;
    case "SYSTEM":
      return( getSystemName()+DPN_INFOEXCHANGE+".rx.system" );
      break;
    default:
      return( getSystemName()+DPN_INFOEXCHANGE );
      break;
  }
}

string dcsInfo_getTxExchangeDPE(string type = "MESSAGE")
{
  switch(type) {
    case "MESSAGE":
      return( getSystemName()+DPN_INFOEXCHANGE+".tx.message" );
      break;
    case "USER":
      return( getSystemName()+DPN_INFOEXCHANGE+".tx.user" );
      break;
    case "DESTINATION":
      return( getSystemName()+DPN_INFOEXCHANGE+".tx.destination" );
      break;
    case "SYSTEM":
      return( getSystemName()+DPN_INFOEXCHANGE+".tx.system" );
      break;
    default:
      return( getSystemName()+DPN_INFOEXCHANGE );
      break;
  }
}

// ----------------------------------------------------------------------------
// dcsInfo_SendMsg (internal use only)
/*
	This function Send a message. 
	
	F.A. ver 1.0   27/02/2008
	
	History
	
	  		
*/

bool dcsInfo_sendMsg(string uname,string sname,string destination,string message)
{
  if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_sendMsg : set to "+getSystemName()+DPN_INFOEXCHANGE+" mes="+message);
  
  dpSetWait(getSystemName()+DPN_INFOEXCHANGE+".tx.message",message,
            getSystemName()+DPN_INFOEXCHANGE+".tx.user",uname,
            getSystemName()+DPN_INFOEXCHANGE+".tx.destination",destination,
            getSystemName()+DPN_INFOEXCHANGE+".tx.system",sname);    
  return(true); 
}


// ------------------------
bool dcsInfo_getMsg(string &uname,string &sname,string &destination,string &message)
{
  bool ret = false;
  
  if(dpExists(getSystemName()+DPN_INFOEXCHANGE)) {
    dpGet(getSystemName()+DPN_INFOEXCHANGE+".rx.message",message,
          getSystemName()+DPN_INFOEXCHANGE+".rx.user",uname,
          getSystemName()+DPN_INFOEXCHANGE+".rx.destination",destination,
          getSystemName()+DPN_INFOEXCHANGE+".rx.system",sname);
    ret = true;    
  } 
  if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_getMsg : get to "+getSystemName()+DPN_INFOEXCHANGE+" mes="+message);

  return(ret);
}



// ------------------------ 


bool dcsInfo_connectNet()
{
  dyn_string dsSystems;
  dyn_string dsAppo;
  dyn_uint duSysId;
  int i,j;
  string sPrefix;
  
  sPrefix = substr(getSystemName(),0,3);    
  sysPrefix = strtoupper(sPrefix);

  // first installation ------------------------  
  if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : try to init the info system on:" +getSystemName());
  
  // verify and create the dp
  i=0;
  if (!getUserPermission(5)) {
    DebugTN(" dcsInfo_connectNet : insufficient priviledges! (no level 5 for :"+getUserName() );
    return(false);
  }
  if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : level 5 for user="+getUserName());
  
  while(!dpExists(getSystemName()+DPN_INFOEXCHANGE)) {
    switch(i) {
      case 0:
      case 2:
        // try to create
        if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : Create DP level 5 for user="+getUserName()+" is:"+(getUserPermission(5)?"OK":"NOT"));        
        dpCreate(DPN_INFOEXCHANGE, HMPDPT_INFOEXCHANGE, getSystemId());
        delay(2);
        break;
      case 3:
        DebugN(" dcsInfo_connectNet : ERROR unable to create the DPs. run as root priviledges!");
        return(false);
        break;
      case 1:
        if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : Create DPT level 5 for user="+getUserName()+" is:"+(getUserPermission(5)?"OK":"NOT"));        
        __dcsInfo_createInfoDpt();
        delay(2);
        break;      
    }
    i++;
  }
  if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : good for work! (" +getSystemName()+")");
  // -------------------------

  // verify if this is the server (dcs machine)
  if(SYN_INFOSERVER != getSystemName()) {
    // nothing to do ...    
    if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : I'm not the server machine. do nothing");

  } else {
    // is the server !
    dpConnect("__dcsInfo_newSystemConnection", true, getSystemName()+"_DistManager.State.SystemNums:_original.._value");
    

  }
  return(true);
}

// This function reconnect DPs each time a new connection is done
void __dcsInfo_newSystemConnection(string dp, dyn_uint duSysNum)
{
    int i,j,k;
    dyn_string dsAppo;
    dyn_uint duSysId;
    dyn_string dsSystems;
    
    // get the list of Systems into the distributed..
    getSystemNames(dsSystems,duSysId);
    i = dynlen(dsSystems);
    while(i>0) {
      if(substr(dsSystems[i],0,3) == sysPrefix) dynRemove(dsSystems,i); // remove the dcs machines
      i--;
    }  
    if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : the list of Distributed machines:",dsSystems);
    
    // catch all the Interface DP
    for(i=1;i<=dynlen(dsSystems);i++) {
      dsAppo = dpNames(dsSystems[i]+":*",HMPDPT_INFOEXCHANGE);
      for(j=1;j<=dynlen(dsAppo);j++){
        if((k = dynContains(dsDpNames,dsAppo[j])) > 0) {
          dsDpNames[k] = "mantainIt_" + dsDpNames[k];
        } else {
          dynAppend(dsDpNames,"newIt_" + dsAppo[j]);
        }
      }
    }  
    if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : the list of exchange DPs:",dsDpNames);

    // then connect all the tx point of distributed sisyems
    for(i=dynlen(dsDpNames);i>=1;i--) {
      if(strpos(dsDpNames[i],"mantainIt_") >= 0) {
        strreplace(dsDpNames[i],"mantainIt_","");
      } else if(strpos(dsDpNames[i],"newIt_") >= 0) {
        strreplace(dsDpNames[i],"newIt_","");
        dpConnect("__dcsInfo_rxMesCB", false, dsDpNames[i]+".tx.destination:_original.._value");
      } else {
        dpDisconnect("__dcsInfo_rxMesCB", dsDpNames[i]+".tx.destination:_original.._value");
        dynRemove(dsDpNames,i);
      }
    }
    if( IS_DEBUGOUTPUT ) DebugN("dcsInfo_connectNet : dpConnect done !");
 
}


// This function dispatch messages

void __dcsInfo_rxMesCB(string dp,string value) {

  // read the message
  int i;
  string sDpName;
  string sUser, sMessage, sSystem;
  sDpName = dpSubStr(dp,DPSUB_SYS_DP);
  dpGet( sDpName+".tx.user",sUser, sDpName+".tx.system",sSystem, sDpName+".tx.message",sMessage);

  if( IS_DEBUGOUTPUT ) DebugN("__dcsInfo_rxMesCB : message received on server:"+dp+" to :"+value);
    
  if(value == sysPrefix) { // this messages come to the DCS
    // copy mess to the rx box
    dpSet( getSystemName()+DPN_INFOEXCHANGE+".rx.user",sUser, getSystemName()+DPN_INFOEXCHANGE+".rx.system",sSystem,
           getSystemName()+DPN_INFOEXCHANGE+".rx.message",sMessage, getSystemName()+DPN_INFOEXCHANGE+".rx.destination", value);
    if( IS_DEBUGOUTPUT ) DebugN("__dcsInfo_rxMesCB : message received to the server!");
    
  } else if(value == "BROADCAST") { // this will be for all detectors
    // copy to dcs machine
    dpSet( getSystemName()+DPN_INFOEXCHANGE+".rx.user",sUser, getSystemName()+DPN_INFOEXCHANGE+".rx.system",sSystem,
           getSystemName()+DPN_INFOEXCHANGE+".rx.message",sMessage, getSystemName()+DPN_INFOEXCHANGE+".rx.destination", value);
    // and all over the word ...

    for(i=1;i<dynlen(dsDpNames);i++){
      if(dp != dsDpNames[i]+".tx.destination:_original.._value") // except the sender
        dpSet(dsDpNames[i]+".rx.user",sUser, dsDpNames[i]+".rx.system",sSystem,
             dsDpNames[i]+".rx.message",sMessage, dsDpNames[i]+".rx.destination", value);
    }
    
    if( IS_DEBUGOUTPUT ) DebugN("__dcsInfo_rxMesCB : Broadcast message sended to all!",dsDpNames);    
    
  } else {
    // or propagate to a dcs machine...
    for(i=1;i<dynlen(dsDpNames);i++){
      if(strpos(strtoupper(dsDpNames[i]),value) >= 0) { // is the receiver ?
        dpSet(dsDpNames[i]+".rx.user",sUser, dsDpNames[i]+".rx.system",sSystem,
             dsDpNames[i]+".rx.message",sMessage, dsDpNames[i]+".rx.destination", value);
        
        if( IS_DEBUGOUTPUT ) DebugN("__dcsInfo_rxMesCB : message sended to :"+dsDpNames[i]);    
        
      }
    }
  }
  
  return;  
}

// =========================================== EOF ================================

