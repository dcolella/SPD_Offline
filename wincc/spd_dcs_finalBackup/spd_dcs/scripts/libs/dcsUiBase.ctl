// $License: NOLICENSE
/* ------------------------------------------------------------

	ALICE DCS - UI LIBRARY

This library collect all functions linked to the ALICE UI

---------------------------------------------------------------*/
//   Ver. 1.0 16/10/2008
//  
//   Add information for LHC
//   Add NetTestFunction
//   Add FsmConnectMode Wrapper
//   Add function to display Info Dialog
//   remove function to convert PMON Web pages in ASCII string  
//
//   3/10/2007 - Add functions for the E-Log access
//   18/10/2007 - Add function for store the FSM Hierarchy on File
//   27/03/2008 - Add function for store the FSM Hierarchy on Ordered File
//   8/9/2008 - Add Constants dyn dyn str for LHC
//   16/9/2008 - Add call for ScopeLib + Add Call for PopUpPanel
//   23/9/2008 - Add funtions for the color management
//   24/9/2008 - Add funtion for the Enable/Disable FSM configuration
//   16/10/2008 - Modify DispInfo functions in order to set visibility
//   17/06/2009 - Some fixing for the 3.0.9 ver
//   08/11/2009 - add routines for timed funcion use (refresh patch !)
//   13/11/2009 - fix bug in test net connection
//   03/11/2010 - Add the function that sets the Identity. in dcsUi_FsmInitialize()
//   01/12/2012 - Design the Light-Version
  
// ----------  Constant definition ----------------------------
const string DCSUI_VERSION = "3.2.0";
const int SPLASH_SCREEN_DELAY = 5; // Number of seconds

const bool DCSUI_INFO_BRIGHT = true; // Info window Hightligt
const bool DCSUI_INFO_DARK = true; // Info window Dark

// Coordinate In the AC Grant List
const int DCSUI_AC_OBSERVER = 1;
const int DCSUI_AC_OPERATOR = 2;
const int DCSUI_AC_EXPERT = 3;
const int DCSUI_AC_DEVELOPER = 4;

global dyn_dyn_string gdsLHCDescription = makeDynString();

const int DCSUI_FSM_MODAL = 1;
const int DCSUI_FSM_MODLESS = 2;

global string gsDcsUiEnvironmentShape = "";

/* ------------------------------------------------------------
	Funtion for Get the Ui Version
	F.A. ver 1.0  24/01/2011

	History 

		24/01/2011	- Creation
    
------------------------------------------------------------ */
string dcsUiBase_getVersion()
{
		return(DCSUI_VERSION);
}



/* ------------------------------------------------------------
	Funtion for Setting Global Variables

	F.A. ver 1.0  19/6/2006

	History 

		19/06/2006	- Creation
		03/08/2006 - Modify to the dcsUiBase_setMainTitle
		19/10/2006 - Add Configs entry for the Auziliary Monitoring Zone
		30/10/2006 - Add Configs entry for AC centralized Service 
		
------------------------------------------------------------ */
bool dcsUiBase_Init()
{
	// Verify if the init is done
	if( globalExists( "dcsUiACDomain" ) )	
	{
		DebugTN("dcsUiBase_Init(): ver."+DCSUI_VERSION+" Intialization phase done. Skipped !");
		return(false);
	}
	
	DebugTN("dcsUiBase_Init(): ver."+DCSUI_VERSION+" Intialization phase...");

	// Define the globals variables
	addGlobal("dcsUiRootNode", STRING_VAR);
	addGlobal("dcsUiRootObj", STRING_VAR);
	addGlobal("dcsUiAmzSubServList", STRING_VAR);
	addGlobal("dcsUiAmzTitle", STRING_VAR);
	addGlobal("dcsUiAmzRow", DYN_STRING_VAR);
	addGlobal("dcsUiACDomain", STRING_VAR);
	addGlobal("dcsUiACCentralized", BOOL_VAR);
	addGlobal("dcsUiACServerName", STRING_VAR);
	addGlobal("dcsUiACExcludePrivi", STRING_VAR);
  addGlobal("dcsUiACDisablePrivi", STRING_VAR); 
  addGlobal("dcsUiFsmUiShowHiddenChildren", BOOL_VAR);
 
	// Verify the presence of Config file
	if( getFileSize(getPath(CONFIG_REL_PATH) + "dcsUi.config") == -1)
	{
		DebugTN("dcsUiBase_Init(): ERROR Wrong dcsUi.config File !! Init Aborted.");
		return(false);
	}
	
	// Read the config file variables
	string fi = getPath(CONFIG_REL_PATH) + "dcsUi.config";
	int val = 0;
	string result= "";

	// read the Hierarchy	
	string sec = "Hierarchy";
	if(paCfgReadValue(fi,sec, "RootNode",result) != 0 ) 
	{
		DebugTN("dcsUiBase_Init(): ERROR accessing dcsUi.config file! ("+fi+":"+sec+":RootNode)" );
		return(false);
	}
	dcsUiRootNode = result;
	result = "";
	val += paCfgReadValue(fi,sec, "RootObject",result);
	dcsUiRootObj = result;

  // Hidden nodes
	if(paCfgReadValue(fi,sec, "HideNotVisibleNodes",result) != 0 ) 
	{
		DebugTN("dcsUiBase_Init(): ERROR accessing dcsUi.config file! ("+fi+":"+sec+":HideNotVisibleNodes)" );
   dcsUiFsmUiShowHiddenChildren = false; 
	 } else {
    if(strtoupper(result) == "YES" )
      dcsUiFsmUiShowHiddenChildren = false;
    else
      dcsUiFsmUiShowHiddenChildren = true;
  }  
  // ---------------------
 
	// read the AMZ 
	string sec = "AuxiliaryMonitoringZone";
	result = "";
	val += paCfgReadValue(fi,sec, "Title",result);
	dcsUiAmzTitle = result;
	result = "";
	val += paCfgReadValue(fi,sec, "Row1",result);
	dynAppend(dcsUiAmzRow,result);
	result = "";
	val += paCfgReadValue(fi,sec, "Row2",result);
	dynAppend(dcsUiAmzRow,result);
	result = "";
	val += paCfgReadValue(fi,sec, "Row3",result);
	dynAppend(dcsUiAmzRow,result);
	result = "";
	val += paCfgReadValue(fi,sec, "Row4",result);
	dynAppend(dcsUiAmzRow,result);
	result = "";
	val += paCfgReadValue(fi,sec, "Row5",result);
	dynAppend(dcsUiAmzRow,result);
//	result = "";
//	val += paCfgReadValue(fi,sec, "SubServicesItems",result);
	dcsUiAmzSubServList= "";


	// read the Access Control Def.
	string sec = "AccessControl";
	dyn_string dsAppo;

	result = "";
	val += paCfgReadValue(fi,sec, "UseCentralizedAC",result);
	dcsUiACCentralized = (strtoupper(result) == "YES") ? true : false;

	result = "";
	val += paCfgReadValue(fi,sec, "CentralizedACServer",result);
	dcsUiACServerName =result;

	result = "";
	val += paCfgReadValue(fi,sec, "ACDomain",result);
	dcsUiACDomain = result;

	result = "";
	val += paCfgReadValue(fi,sec, "FSMExcludeAC",result);
	dcsUiACExcludePrivi = result;

       	result = "";
	val += paCfgReadValue(fi,sec, "FSMDisableAC",result);
	dcsUiACDisablePrivi = result;

        
  gsDcsUiEnvironmentShape = myModuleName()+"."+myPanelName()+":";
        
	// Now it ends -----
	DebugTN("dcsUiBase_Init(): init DONE !");
	return( (val == 0) ? true : false );
}
// ------------------------------------------------------------


/* ------------------------------------------------------------
	Funtion for Get a Param from the Config File

	F.A. ver 1.0  19/6/2006

	History 

		19/06/2006	- Creation
------------------------------------------------------------ */
string dcsUiBase_getConfigParam(string section, string name)
{
	int val = 0;
	string result= "";
	string fi = getPath(CONFIG_REL_PATH) + "dcsUi.config";
	
	if(paCfgReadValue(fi, section, name ,result) != 0 ) 
	{
		DebugTN("dcsUiBase_getConfigParam(): ERROR accessing dcsUi.config file! ("+fi+":"+section+":"+name+")");
		return("");
	}
	return(result);
}


/* ------------------------------------------------------------
	Funtion for Get the AC grants defined into the config
	file Grants list

	F.A. ver 1.0  19/6/2006

	History 

		19/06/2006	- Creation
------------------------------------------------------------ */
void dcsUiBase_getACGrants(bool & isDeveloper,bool & isExpert,
				   bool & isOperator, bool & isObserver)
{
dyn_string exceptionInfo;	

		fwAccessControl_isGranted(dcsUiACDomain+":Monitor",isObserver, exceptionInfo);
		if (dynlen(exceptionInfo)) { return; }

		fwAccessControl_isGranted(dcsUiACDomain+":Control",isOperator, exceptionInfo);
		if (dynlen(exceptionInfo)) { return; }

		fwAccessControl_isGranted(dcsUiACDomain+":Debug",isExpert, exceptionInfo);
		if (dynlen(exceptionInfo)) { return; }

		fwAccessControl_isGranted(dcsUiACDomain+":Modify",isDeveloper, exceptionInfo);
		if (dynlen(exceptionInfo)) { return; }

}
// ------------------------------------------------------------


/* ------------------------------------------------------------
	Funtion for set the title into the Main DCS Window

	F.A. ver 1.0  19/6/2006

	History 

		19/06/2006	- Creation
		30/10/2006  - Add the Existence verify
                28/02/2007  - Add the mecchanism of arming...
                04/03/2007  - Remuve the Arm and replace with e fix constant for the name
------------------------------------------------------------ */
const string DCSUI_USERPANELNAME = "dcsUiUserPanelInFrame";

void dcsUiBase_setMainTitle(string title)
{
  if(substr(myPanelName(),0,strlen(DCSUI_USERPANELNAME)) == DCSUI_USERPANELNAME)
  {    
    if(!shapeExists(gsDcsUiEnvironmentShape+"txtTitlePanel")) return;
      if(strlen(title) > 50 && strpos(title,"\n") < 0)
      {
        setValue(gsDcsUiEnvironmentShape+"txtTitlePanel","text",substr(title,0,49)+"\n"+substr(title,50));
      }
      else
        setValue(gsDcsUiEnvironmentShape+"txtTitlePanel","text",title);
  }
}
void dcsUiBase_resetMainTitle(string title)
{
  if(strlen(title) > 50 && strpos(title,"\n") < 0)
    setValue(gsDcsUiEnvironmentShape+"txtTitlePanel","text",substr(title,0,49)+"\n"+substr(title,50));
  else
    setValue(gsDcsUiEnvironmentShape+"txtTitlePanel","text",title);
}
// ----------------------------------------------------------

/* ------------------------------------------------------------
	Funtion for display a text into Info Main DCS Window Dialog

	F.A. ver 1.0  11/2/2007

	History 

		11/02/2007	- Creation
		16/10/2008 - add visibility flag
		18/05/2011 - modify the scroll routine ...

------------------------------------------------------------ */
global int __dcsUiBase_busyMin = 0;
global int __dcsUiBase_busyMax = 0;
global int __dcsUiBase_busyPos = 0;
global int __dcsUiBase_busyHei = 0;
global int __dcsUiBase_busyDir = 1;
global int __dcsUiBase_busyThread = -1;
global int __dcsUiBase_scrollThread = -1;
global int __dcsUiBase_scrollPtr = 0;
global string __dcsUiBase_scrollTitle = "";
const int DCSUI_INFODISPLAYDIM = 41;

void dcsUiBase_setInfoText(string title, bool highlight = false)
{
  if(!shapeExists("txtDialogText")) return;
  bool bIs=false;getValue("txtDialogText","visible",bIs); if(!bIs) return;

  if(strlen(title) < DCSUI_INFODISPLAYDIM) {
    // if the scroll is on switch it
    if (__dcsUiBase_scrollThread != -1) {
        setMultiValue("bdPre","visible",false,"bdPos","visible",false);    
        __dcsUiBase_scrollThread = -1;
        delay(0,200);
    }
    setMultiValue("txtDialogText","text",title,
                  "txtDialogText","foreCol",highlight ? "dcsUiDisplayFore" : "dcsUiDisplayMidFore");
  } else {
    __dcsUiBase_scrollPtr = 0;
    __dcsUiBase_scrollTitle = title;
    if (__dcsUiBase_scrollThread == -1) {
      setMultiValue("txtDialogText","foreCol",highlight ? "dcsUiDisplayFore" : "dcsUiDisplayMidFore",
                    "bdPre","visible",true,"bdPos","visible",true);    
      __dcsUiBase_scrollThread = startThread("__dcsUiBase_thickScroll");
    }    
  }
  return;
}

void __dcsUiBase_thickScroll()
{
  if(!shapeExists("txtDialogText")) return;
  bool bIs=false; getValue("txtDialogText","visible",bIs); if(!bIs) return;

  int len = strlen(__dcsUiBase_scrollTitle)+5;
  while(__dcsUiBase_scrollThread != -1) {

    setValue("txtDialogText","text",
              substr(__dcsUiBase_scrollTitle+"�    ",
                     __dcsUiBase_scrollPtr,
                     ((__dcsUiBase_scrollPtr+DCSUI_INFODISPLAYDIM <= len) ? DCSUI_INFODISPLAYDIM : (len-__dcsUiBase_scrollPtr) )) +
              ((__dcsUiBase_scrollPtr+DCSUI_INFODISPLAYDIM<=len) ? 
                "" :
                substr(__dcsUiBase_scrollTitle+"�    ",
                       0, 
                       (DCSUI_INFODISPLAYDIM-(len-__dcsUiBase_scrollPtr)) ))
             );
    __dcsUiBase_scrollPtr = (__dcsUiBase_scrollPtr == (len-1)) ? 0 : __dcsUiBase_scrollPtr+1;
    delay(0,250);
  }
  return;
}
// ----------------------------------------------------------
void dcsUiBase_setProgresBar(float progres)
{
  if(!shapeExists("txtDialogText")) return;
  bool bIs=false;getValue("txtDialogText","visible",bIs); if(!bIs) return;

  float w,h;
  getValue("bdBarFrame","size",w,h);
  w = w/10.0;
  setMultiValue("txtPercentage","text",progres+"%","bdBarIndicator","scale",(float)((float)w/100.0)*progres,1);
  
}
void dcsUiBase_switchProgresBar(bool bOn)
{
  if(!shapeExists("txtDialogText")) return;
  bool bIs=false;getValue("txtDialogText","visible",bIs); if(!bIs) return;

  setMultiValue("txtPercentage","visible",bOn,"bdBarIndicator","visible",bOn,
                "bdBarFrame","visible",bOn,"bdBusy","visible",false);
  
  if (__dcsUiBase_busyThread != -1)
  {
    __dcsUiBase_busyThread = -1;
  }
    
}
void dcsUiBase_switchBusyBar(bool bOn)
{
  if(!shapeExists("txtDialogText")) return;
  bool bIs=false;getValue("txtDialogText","visible",bIs); if(!bIs) return;

  int a;
  setMultiValue("txtPercentage","visible",false,"bdBarIndicator","visible",false,
                "bdBarFrame","visible",bOn,"bdBusy","visible",bOn);

  getValue("bdBarFrame","size", __dcsUiBase_busyMax, a);
  getValue("bdBarFrame","position", __dcsUiBase_busyMin, __dcsUiBase_busyHei);
  __dcsUiBase_busyMax = __dcsUiBase_busyMax + __dcsUiBase_busyMin-50;
  __dcsUiBase_busyMin = __dcsUiBase_busyMin +13;
  __dcsUiBase_busyPos = __dcsUiBase_busyMin;
  __dcsUiBase_busyDir = 1;
  setValue("bdBusy","position",__dcsUiBase_busyPos,__dcsUiBase_busyHei-11);
  if(bOn)
  {
    __dcsUiBase_busyThread = 0;
    __dcsUiBase_busyThread = startThread("__dcsUiBase_thickBusyBar");
  }
  else
  {
    if (__dcsUiBase_busyThread != -1)
    {
        __dcsUiBase_busyThread = -1;
    }
  }
}
void __dcsUiBase_thickBusyBar()
{
  if(!shapeExists("txtDialogText")) return;
  bool bIs=false;getValue("txtDialogText","visible",bIs); if(!bIs) return;

  float w,h;
  while(__dcsUiBase_busyThread != -1)
  {
    __dcsUiBase_busyPos = __dcsUiBase_busyPos + (__dcsUiBase_busyDir*15);
    __dcsUiBase_busyDir = (__dcsUiBase_busyPos > __dcsUiBase_busyMax) ? -1 : (__dcsUiBase_busyPos < __dcsUiBase_busyMin) ? 1 : __dcsUiBase_busyDir ;
    setValue("bdBusy","position",__dcsUiBase_busyPos,__dcsUiBase_busyHei-11);
    delay(0,300);
  }
}


// ----------------------------------------------------------

/* ------------------------------------------------------------
	Funtion for set the Tree Viewer Node 

	F.A. ver 1.0  1/2/2008

	History 
              01/02/2008	- Creation
------------------------------------------------------------ */
void dcsUiBase_jumpToObject(string node, string obj)
{
  if(obj == "") return;
  if(node == "") node = obj;
  
  if(substr(myPanelName(),0,strlen(DCSUI_USERPANELNAME)) == DCSUI_USERPANELNAME) {    
    if(!shapeExists(gsDcsUiEnvironmentShape+"FSMTreeView.action")) return;
    setValue(gsDcsUiEnvironmentShape+"FSMTreeView.action","text",node+"::"+obj);
  } 
} 
    
// ----------------------------------------------------------


/* -------------------------------------------------------------
	Function for get the status of a One Manager

	F.A. ver 1.0   03/10/2006

	History

		03/10/2006	- Creation
-------------------------------------------------------------- */
bool dcsUiBase_getMgrState(string sSystemName, string sManagerName, string sManType, int iManNum = -1 )
{
	dyn_int diManNums;
	string sManTypeU = "";
	sManType = strtoupper(substr(sManType,0,1))+strtolower(substr(sManType,1));
	sManTypeU = strtoupper(sManType);

	if(iManNum == -1)
		dpGet(sSystemName+":_Connections."+sManType+".ManNums:_original.._value",diManNums);
	else
		dynAppend(diManNums,iManNum);

	return(dcsUiBase_getMgrsState(sSystemName, sManagerName, sManType, diManNums));
} 

/* -------------------------------------------------------------
	Function for get the status of a List of Managers

	F.A. ver 1.0   03/10/2006

	History

		03/10/2006	- Creation
-------------------------------------------------------------- */
bool dcsUiBase_getMgrsState(string sSystemName, string sManagerName, string sManType, dyn_int diManNums)
{

	dyn_string dsResults;
	dyn_string dsAppo;
	string sResult = "";
	int count =0;
	int j = 0;
	int i=0;
	string sDebDP = "";
	string sManTypeU = "";

	sManType = strtoupper(substr(sManType,0,1))+strtolower(substr(sManType,1));
	sManTypeU = strtoupper(sManType);

	for(i=1;i<=dynlen(diManNums);i++)
	{
		sDebDP = sSystemName+":_CtrlDebug_"+sManTypeU+"_"+(diManNums[i]);
		if (dpExists(sDebDP))
		{
			count = 0;
			dynClear(dsResults);
     	dpSetWait(sDebDP+".Command:_original.._value", "info scripts");
			while ( dynlen(dsResults) < 1 && count < 40)  
			{
		   	delay(0,50);
				dpGet(sDebDP+".Result:_online.._value",  dsResults);
				count ++;
			} 
			for(j=1; j <= dynlen(dsResults);j++)
			{
				dsAppo = strsplit(dsResults[j], ";");
				if( dsAppo[dynlen(dsAppo)] == sManagerName)
					return(true);			
			}
		}
	}
	return(false);
}
// ------------------------------------------------------------

/* -------------------------------------------------------------
	Function to open a PopUp windows inside the dcsUi

	F.A. ver 1.0   18/09/2008

	History


        
-------------------------------------------------------------- */
const int DCSUI_POPUPMODLESS = 1;
const int DCSUI_POPUPMODAL = 2;
 
//string dcsUiBase_openPopUpPanel( )
void dcsUiBase_openPopUpPanel(string sPanelFileName,
                                string sPanelName,
                                dyn_string dsParameters,
                                int iType = DCSUI_POPUPMODLESS )
{
  if(sPanelFileName == "" || sPanelName == "") return;

  // verify if the panel is open
  if( isPanelOpen(sPanelName, myModuleName() ) ) {
    // close the panel
    dyn_anytype da, daa;
    da[1]  = myModuleName();     
    da[2]  = sPanelName;
    daa[1] = 0.0; daa[2] = "FALSE"; // Return value optional  
    da[3] = daa;                    // dyn_anytype binding
    panelOff(da);
  }      
  
  // then open the pop-up  
  if(iType == DCSUI_POPUPMODLESS) {
    ChildPanelOnCentral(sPanelFileName, sPanelName, dsParameters);
  } else if(iType == DCSUI_POPUPMODAL) {
    ChildPanelOnCentralModal(sPanelFileName, sPanelName, dsParameters); 
  }
  
  return;
}

//string dcsUiBase_openPopUpPanel( )
void dcsUiBase_closePopUpPanel(string sPanelName)
{
  // verify if the panel is open
  if( isPanelOpen(sPanelName, myModuleName() ) ) {
    // close the panel
    dyn_anytype da, daa;
    da[1]  = myModuleName();     
    da[2]  = sPanelName;
    daa[1] = 0.0; daa[2] = "FALSE"; // Return value optional  
    da[3] = daa;                    // dyn_anytype binding
    panelOff(da);
  }      
  return;
}



/* -------------------------------------------------------------
	Function to loads LHC status description

	F.A. ver 1.0   8/9/2008

	History

  Upgraded 17/06/2009 -  From Document : LHC-OP-ES-0005 rev 0.1 
  Removed  - 01/01/2012
------------------------------------------------------------- */
/*
void dcsUiBase_LHCSet()
{
    dynAppend(gdsLHCDescription, makeDynString("NO BEAM",
                                               "CIRCULATE AND DUMP",
                                               "INJECT AND DUMP",
                                               "RECOVERY",
                                               "CYCLING",
                                               "RAMP DOWN",
                                               "BEAM DUMP",
                                               "BEAM DUMP WARNING",
                                               "UNSTABLE BEAMS",
                                               "STABLE BEAMS",
                                               "ADJUST",
                                               "SQUEEZE",
                                               "FLAT TOP",
                                               "RAMP",
                                               "PREPARE RAMP",
                                               "INJECTION NOMINAL",
                                               "INJECTION INTERMEDIATE",
                                               "INJECTION PILOT",
                                               "ABORT",
                                               "SETUP",
                                               "RECOVERY",
                                               "WARM-UP",
                                               "CALIBRATION",
                                               "MACHINE DEVELOPMENT",
                                               "TOTEM PHYSICS",
                                               "ION PHYSICS",
                                               "PROTON PHYSICS",
                                               "BEAM SETUP",
                                               "MACHINE TEST",
                                               "ACCESS",
                                               "MACHINE CHECKOUT",
                                               "COOLDOWN",
                                               "SHUTDOWN",
                                               "SECTOR DEPENDENT"));
    
    dynAppend(gdsLHCDescription, makeDynString("No Beam in the Machine",
                                               "Dump after a large number of turns following injection",
                                               "Dump after small number of turns following injection",
                                               "Following quench, emergency beam dump, post mortem, etc.",
                                               "Pre-cycle before injectionfollowing access, recovery, etc.",
                                               "Ramp down and cycling after programmed dump at end of physic fill",
                                               "Requested or emergency dump",
                                               "Before a requested beam dump at the end of stable beams",
                                               "Emergency mode entered from stable beams",
                                               "Stable conditions with collisions in the experiments",
                                               "Preparing for collisions or adjusting beams after the squeeze",
                                               "Preparing for the squeezing",
                                               "Ramp finished - pre-squeeze checks",
                                               "Ready to ramp or ramping or immediate post ramp",
                                               "Injection complete, preparing for ramp",
                                               "If either ring 1 or ring 2 will be injected with or have beam circulating at this intensity",
                                               "If either ring 1 or ring 2 will be injected with or have beam circulating at this intensity",
                                               "If either ring 1 or ring 2 will be injected with or have beam circulating at this intensity",
                                               "Recovery mode following beam permit drop",
                                               "Possibily beam in trasfer line dumps in",
                                               "Typically quench recovery or recovery from cryogenics plant disturbance",
                                               "One or more sector warming up for repair",
                                               "Power Converter calibration",
                                               "Beam based machine development",
                                               "Beam based operation aimed at TOTEM",
                                               "Beam based operation aimed at ion physics",
                                               "Beam based operation aimed at proton physics",
                                               "Machine setup with one or both beams",
                                               "Operations' test without beam",
                                               "Access or preparation for said",
                                               "Check the simultaneous functioning of the various LHC sub-systems in the final configuration",
                                               "Coming back from shutdown",
                                               "Usual winter status",
                                               "The Accelerator Mode tries to cope with the situation in which different sectors...")); 
                                               
    dynAppend(gdsLHCDescription, makeDynString("FwStateOKNotPhysics",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateAttention2",
                                               "FwStateAttention2",
                                               "FwStateAttention2",
                                               "FwStateOKPhysics",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateOKNotPhysics",
                                               "FwStateAttention1",
                                               "FwStateOKNotPhysics",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateAttention1",
                                               "FwStateAttention2",
                                               "FwStateAttention1",
                                               "FwStateAttention2",
                                               "FwStateAttention2",
                                               "FwStateAttention2",
                                               "FwStateOKNotPhysics",
                                               "FwStateOKPhysics",
                                               "FwStateOKPhysics",
                                               "FwStateOKPhysics",
                                               "FwStateAttention1",
                                               "FwStateOKNotPhysics",
                                               "FwStateAttention1",
                                               "FwStateAttention2",
                                               "FwStateAttention1",
                                               "FwStateOKNotPhysics",
                                               "FwStateAttention1")); 

  return;   
}
*/

/* -------------------------------------------------------------
	Function to convert the status of LHC into message

	F.A. ver 1.0   20/10/2006

	History

        8/9/2008  -  search into dds array
      1/1/2012     Removed        
-------------------------------------------------------------- */
/*
string dcsUiBase_LHCStatus2Mess(string sStatus, int iType=0)
{
  if(dynlen(gdsLHCDescription) == 0) dcsUiBase_LHCSet();
 
  int index=0;
  index = dynContains(gdsLHCDescription[1],sStatus); 
  if(index == 0) {
    DebugN("LHC conversion function :"+sStatus+" undefined !");
    return(sStatus);
  }    
  return(gdsLHCDescription[2][index]);
}
*/
/* -------------------------------------------------------------
	Function to convert the status of LHC into color 

	F.A. ver 1.0   20/10/2006

	History

        8/9/2008  -  search into dds array
        1/1/2012  -  removed
-------------------------------------------------------------- */
/*
string dcsUiBase_LHCStatus2Col(string sStatus)
{

  if(dynlen(gdsLHCDescription) == 0) dcsUiBase_LHCSet();
  
  int index=0;
  index = dynContains(gdsLHCDescription[1],sStatus); 
  if(index == 0) return("FwStateOKNotPhysics");
  return(gdsLHCDescription[3][index]);
}
*/

// ====================  ELOG INJECTION FUNCTIONS ==============
/* -------------------------------------------------------------
	Function to Init the E-Log System

	F.A. ver 1.0   03/10/2007

	History

-------------------------------------------------------------- */
bool dcsUiBase_ElogInit()
{
   if( globalExists( "dcsUiElogHostName" )){
	DebugTN("dcsUiBase_ElogInit(): ver."+DCSUI_VERSION+" Intialization phase done. Skipped !");
	return(false);
   }
	
   DebugTN("dcsUiBase_ElogInit(): ver."+DCSUI_VERSION+" Intialization phase...");

   // Define the globals variables
   addGlobal("dcsUiElogHostName", STRING_VAR);
   addGlobal("dcsUiElogPath", STRING_VAR);
   addGlobal("dcsUiElogDetector", STRING_VAR);
   addGlobal("dcsUiElogHostPort", INT_VAR);
   addGlobal("dcsUiElogWritePasswd", STRING_VAR);
   addGlobal("dcsUiElogUserName", STRING_VAR);
   addGlobal("dcsUiElogUserPasswd", STRING_VAR);
   addGlobal("dcsUiElogVerboseLog", BOOL_VAR);
       
   // Verify the presence of Config file
   if( getFileSize(getPath(CONFIG_REL_PATH) + "dcsUi.config") == -1){
	DebugTN("dcsUiBase_ElogInit(): ERROR Wrong dcsUi.config File !! Init Aborted.");
	return(false);
   }
	
   // Read the config file variables
   string fi = getPath(CONFIG_REL_PATH) + "dcsUi.config";
   int val = 0;
   string result= "";


   string sec = "ELogFacility";
   result = "";
   val = paCfgReadValue(fi,sec, "ELogHost",result);
   dcsUiElogHostName = (val==0) ? result : "aldcs101.cern.ch"; 
   result = "";
   val = paCfgReadValue(fi,sec, "ELogSubDirectory",result);
   dcsUiElogPath = (val==0) ? result : ""; 
   result = "";
   val = paCfgReadValue(fi,sec, "DetectorELogURL",result);
   dcsUiElogDetector = (val==0) ? result : ""; 
   result = "";
   val = paCfgReadValue(fi,sec, "ELogHostPort",result);
   dcsUiElogHostPort = (val==0) ? result : 8080; 
   result = "";
   val = paCfgReadValue(fi,sec, "ELogWritePasswd",result);
   dcsUiElogWritePasswd = (val==0) ? result : ""; 
   result = "";
   val = paCfgReadValue(fi,sec, "ELogUserName",result);
   dcsUiElogUserName = (val==0) ? result : ""; 
   result = "";
   val = paCfgReadValue(fi,sec, "ELogUserPasswd",result);
   dcsUiElogUserPasswd = (val==0) ? result : ""; 
   result = "";
   val = paCfgReadValue(fi,sec, "ELogVerboseLog",result);
   dcsUiElogVerboseLog = (strtoupper(result)=="YES" && val==0)?true:false; 

   // code the passwords
   if (dcsUiElogUserPasswd != "") 
     dcsUiElogUserPasswd = __dcsUiBase_base64_encode(dcsUiElogUserPasswd); // ?
   if (dcsUiElogWritePasswd != "") 
     dcsUiElogWritePasswd = __dcsUiBase_base64_encode(dcsUiElogWritePasswd);
   
   DebugTN("dcsUiBase_ElogInit(): Intialization DONE !");

   return(true);
}

/* -------------------------------------------------------------
	Function to Add an Entry into the E-Log System

	F.A. ver 1.0   03/10/2007

	History

-------------------------------------------------------------- */
bool dcsUiBase_ElogNewEntry(string sCategory = "General",
				    string sAuthor = "ALICE dcsUi",
				    string sSubject = "Test",
				    string Message = "Hello World !!",
                                    string Attached = "")
{

   string sLogTag = "[dcsUi E-Log interface ]:";
  
   string HostName; 
   int    iEncoding = 1; //        0:ELCode,1:plain,2:HTML
          
   // Standard Attribs   
   dyn_string attrib_name = makeDynString("Category","Author","Subject"); //     Attribute names
   dyn_string attrib = makeDynString(sCategory, sAuthor, sSubject); //          Attribute values
   dyn_string dsAttachedFiles;
   

   // verify if Elog is initalizied
   if( !globalExists( "dcsUiElogHostName" )){
     if( !dcsUiBase_ElogInit() )
	  return(false);
   }

   
   /* get local host name */
   HostName= getHostname();

   /* create socket */
   int tcpSocket;
   dyn_errClass err;

   tcpSocket = tcpOpen(dcsUiElogHostName, dcsUiElogHostPort);
   err=getLastError();
   if(dynlen(err)>0) {
     DebugTN(sLogTag+"Tcp socket:"+tcpSocket);
     DebugTN(sLogTag+"Error:"+err);  
     DebugTN(sLogTag+"Cannot Open TCP Connection with E-Log Severver:"+dcsUiElogHostName+"! (abort)");  
     return(false);
   }
     
   if(dcsUiElogVerboseLog) DebugTN(sLogTag+"Successfully connected to host:"+dcsUiElogHostName);

   /* compose content */
   string sContent;
   string sBoundary;
   string sAppo;
   srand(period(getCurrentTime()));
   sprintf(sBoundary, "---------------------------%04X%04X%04X", rand(), rand(), rand());
   sContent = sBoundary;
   sContent += "\r\nContent-Disposition: form-data; name=\"cmd\"\r\n\r\nSubmit\r\n";

   if(dcsUiElogUserName != "")
      sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"unm\"\r\n\r\n"+dcsUiElogUserName+"\r\n") ;

   if (dcsUiElogUserPasswd != "") 
      sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"upwd\"\r\n\r\n"+dcsUiElogUserPasswd+"\r\n");
   
   if (dcsUiElogDetector != "")
     sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"exp\"\r\n\r\n"+dcsUiElogDetector+"\r\n");

 
   switch(iEncoding){
     case 0:
       sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"Encoding\"\r\n\r\nELCode\r\n");
       break;
     case 1:
       sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"Encoding\"\r\n\r\nplain\r\n");
       break;
     case 2:
       sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"Encoding\"\r\n\r\nHTML\r\n");
       break;
   }

  // attributes
   int i;
   for (i = 1; i <= dynlen(attrib_name); i++) {
      sAppo = strtoupper(attrib_name[i]);
      sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\""+sAppo+"\"\r\n\r\n"+attrib[i]+"\r\n");
   }

  // message
  sContent += (sBoundary + "\r\nContent-Disposition: form-data; name=\"Text\"\r\n\r\n"+Message+"\r\n"+ sBoundary +"\r\n");

  // Then Attach files
  blob blBuf;
  file fi;
  int n;
  string Buffer;
  dsAttachedFiles = strsplit(Attached,",;");
  for(i=1;i<=dynlen(dsAttachedFiles);i++){
    fi = fopen(dsAttachedFiles[i],"rb");
    if(fi != 0) {
      sContent += "Content-Disposition: form-data; name=\"attfile"+i+"\"; filename=\""+dsAttachedFiles[i]+"\"\r\n\r\n";
      blobRead(blBuf,80000,fi);
      n = bloblen(blBuf);
      blobGetValue(blBuf, 0, Buffer, n);
      sContent += Buffer;
      sContent += "\r\n";
      sContent += sBoundary;
    }
    fclose(fi);   
   }
  
  int content_length = strlen(sContent);
  
  /* compose request */
  string request;
  request = "POST /";
  
  if(dcsUiElogPath != "") request +=  (dcsUiElogPath +"/");
  if(dcsUiElogDetector != "") request += (dcsUiElogDetector +"/");
  request += " HTTP/1.0\r\n";
  request += "Content-Type: multipart/form-data; boundary="+sBoundary+"\r\n";
  request += "Host: "+HostName+"\r\n";
  request += "User-Agent: ELOG\r\n";
  request += "Content-Length: "+content_length+"\r\n";

  if (dcsUiElogWritePasswd != "") 
    request += "Cookie: wpwd="+dcsUiElogWritePasswd+"\r\n";

  request += "\r\n";
  int header_length = strlen(request);

  
  /* send request */
  int write;
  write = tcpWrite(tcpSocket, request); 
  err=getLastError(); 
  if(dynlen(err)>0) {
     DebugTN(sLogTag+"Tcp Write:"+tcpSocket);
     DebugTN(sLogTag+"Error:"+err);  
     DebugTN(sLogTag+"Cannot Write the Request on TCP Connection with E-Log Severver:"+dcsUiElogHostName+"! (abort)");  
     tcpClose(tcpSocket);
     return(false);
  }
  
  if (dcsUiElogVerboseLog) {
     DebugTN(sLogTag+"Request sent to host:"+dcsUiElogHostName+"["+request+"]");  
   }

  /* send content */
  write = tcpWrite(tcpSocket, sContent); 
  err=getLastError(); 
  if(dynlen(err)>0) {
     DebugTN(sLogTag+"Tcp Write:"+tcpSocket);
     DebugTN(sLogTag+"Error:"+err);  
     DebugTN(sLogTag+"Cannot Write Content on TCP Connection with E-Log Severver:"+dcsUiElogHostName+"! (abort)");  
     tcpClose(tcpSocket);
     return(false);
  }
  
  if (dcsUiElogVerboseLog) {
     DebugTN(sLogTag+"Content sent to host:"+dcsUiElogHostName+"["+sContent+"]");  
  }

  /* receive response */
  int maxtime=100; 
  int read;
  string sBuf;
  read=tcpRead(tcpSocket, sBuf, maxtime); 
  err=getLastError(); 
  if(dynlen(err)>0) {
     DebugTN(sLogTag+"Tcp Read:"+tcpSocket);
     DebugTN(sLogTag+"Error:"+err);  
     DebugTN(sLogTag+"Cannot Read from TCP Connection with E-Log Severver:"+dcsUiElogHostName+"! (abort)");  
     tcpClose(tcpSocket);     
     return(false);
  }
    
  if (read < 0) {
     DebugTN(sLogTag+"Cannot receive response from TCP Connection with E-Log Severver:"+dcsUiElogHostName+"! (abort)");  
     tcpClose(tcpSocket);     
     return(false);
  }

  /* discard remainder of response */
  int n = read;
  while(read > 0) {
    read=tcpRead(tcpSocket, sAppo, maxtime); 
    err=getLastError(); 
    if(dynlen(err)>0) {
       DebugTN(sLogTag+"Tcp Read:"+tcpSocket);
       DebugTN(sLogTag+"Error:"+err);  
       DebugTN(sLogTag+"Cannot Read from TCP Connection with E-Log Severver:"+dcsUiElogHostName+"! (abort)");  
       tcpClose(tcpSocket);       
       return(false);
    }
    sBuf += sAppo;
  }

  tcpClose(tcpSocket);

  if (dcsUiElogVerboseLog) {
     DebugTN(sLogTag+"Response received:"+sBuf);  
  }

  /* check response status */
  if(strpos(sBuf, "302 Found") > 0) {
    if(strpos(sBuf, "Location:") > 0) {
      if(strpos(sBuf, "fail") > 0)
        DebugTN(sLogTag+"Error: Invalid user name or password\n");
      else
        DebugTN(sLogTag+"Message successfully transmitted");
    }else
      DebugTN(sLogTag+"Message successfully transmitted");
  }else
    if(strpos(sBuf, "Logbook Selection") > 0)
      DebugTN(sLogTag+"Error: No logbook specified");
    else
      if(strpos(sBuf, "enter password") > 0)
        DebugTN(sLogTag+"Error: Missing or invalid password");
      else
        if(strpos(sBuf, "form name=form1") > 0)
          DebugTN(sLogTag+"Error: Missing or invalid user name/password");
        else
          if(strpos(sBuf, "Error: Attribute") > 0) {
            DebugTN(sLogTag+"Error: Attribute");
          }else
            DebugTN(sLogTag+"Error transmitting message");

  return(true);

}


string __dcsUiBase_base64_encode(string Source)
{
  string map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  unsigned pad , t;
  pad = 3 - ( strlen(source) % 3);
  pad = (pad == 3) ? 0 : pad;
  string Destination = "";
  string a0,a1,a2,a3;
    
  int i=0;
  while(i<strlen(Source)) {
    t = (unsigned)((unsigned)(substr(Source,i++,1)) << 16);
    if(i<strlen(Source))
      t |= (unsigned)((unsigned)(substr(Source,i++,1)) << 8);
    if(i<strlen(Source))
      t |= (unsigned)((unsigned)(substr(Source,i++,1)) << 0);
    
    a3 = substr(map, (t & 63),1);
    t >>= 6;
    a2 = substr(map, (t & 63),1);
    t >>= 6;
    a1 = substr(map, (t & 63),1);
    t >>= 6;
    a0 = substr(map, (t & 63),1);
    
    Destination += (a0+a1+a2+a3);
  }
  
  switch(pad){
    case 1:
      Destination = substr(Destination,0,strlen(Destination)-1) + "=";
      break;
    case 2:
      Destination = substr(Destination,0,strlen(Destination)-2) + "==";
      break;
  }         
  return(Destination);
}
// =============================================================
//     DCS UI COLOR MANAGEMENT SECTION
// =============================================================

/* ------------------------------------------------

Name: 	dcsUiColor_val2Color()
Desc:   Convert a value into a colour string
        
inp :   temp := float    (value to convert)
	t_min := int      (minimum of the scale)
	t_max := int      (maximum of the scale)
	
out :   Colour := string  "{red,green,blu}"

Note : the range values varies from 0 to 130
       over range values are marcked with blue
       and red colours
       
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
string dcsUiColor_val2Color(float temp, int t_min = 0, int t_max = 130)
{
	int r,g,b;
	if(temp < t_min) return("{0,0,255}");
	if(temp > t_max) return("{255,0,0}");
	
	int t_delta = (t_max - t_min + 1);
	int s1 = t_min + floor(t_delta * 0.16);		
	int s2 = s1 + 1;
	int s11 = t_min + floor(t_delta * 0.33);		
	int s10 = s11 - 1;
	int s8 = s11 + 1;
	int s3 = t_min + floor(t_delta * 0.49);		
	int s7 = s3 - 1;
	int s12 = t_min + floor(t_delta * 0.70);	// .66	
	int s4 = s12 - 1;
	int s9 = s12 + 1;
	int s5 = t_min + floor(t_delta * 0.90);		// .82
	int s6 = s5 - 1;
	int step = floor( 240.0 / (s11-s1+1) );
	
	r = (temp < s3) ? 0 : ( (temp > s4 && temp < s5) ? 255 : (temp > s6) ? (255-(temp-s6)*step) : ((temp-s7)*step) );
	b = (temp > s1 && temp < s8) ? 255 : ( (temp > s3) ? 0 : (temp < s2) ? (60+(temp-t_min)*step) : (255-step*(temp-s8)) );
	g = (temp < s2 || temp > s6) ? 0 : ((temp < s9 && temp > s10) ? 255 : ((temp < s11 && temp > s1) ? ((temp-s1)*step) : (255-(step*(temp-s12))) ) );	

	return("{"+r+","+g+","+b+"}");
}


/* ------------------------------------------------

Name: 	dcsUiColor_convertColorName()
Desc:   Convert a colour name into a colour string
        
inp :  sSearchColor := string    (colour name)
	
out :   Colour := string  "{red,green,blu}"

       
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
string dcsUiColor_convertColorName( string sSearchColor )
{   
  file f;
  dyn_string dsNames;
  string sBuffer, sPath;
  int i,j,n, res, len;
  
 // DebugTN("Start search..." );
  
  for(i=1;i<= SEARCH_PATH_LEN ;i++){
    sPath = getPath(COLORDB_REL_PATH,"", 1,i);
    dsNames = getFileNames(sPath,"*");
    n = dynlen(dsNames);
    for(j=1;j<=n;j++){
      if(strpos(dsNames[j],".lock")>0) continue;
      f = fopen(sPath+dsNames[j],"r");
      while(feof(f) == 0){
        if( (len = fgets(sBuffer,1000,f)) > 0) 
          if( (res = strpos(sBuffer, sSearchColor)) > 0)
            if( (res = strpos(sBuffer, "[")) > 0) {fclose(f); return(substr(sBuffer,res,len-res-1));}
            else if( (res = strpos(sBuffer, "{")) > 0) {fclose(f); return(substr(sBuffer,res,len-res-1));}
            else if( (res = strpos(sBuffer, "<")) > 0) {fclose(f); return(substr(sBuffer,res,len-res-1));}
      } 
      fclose(f);
    }
  }
//  DebugTN("...end search!" );
  return("");
 } 

/* ------------------------------------------------

Name: 	dcsUiColor_string2RGB()
Desc:   Convert a colour string into the RGB components
        
inp :   color := string    (colour string)
	
out :   r:= int      (red component)
	g:= int      (green component)
	b:= int      (blu component)
       
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */ 
dyn_int dcsUiColor_string2RGB(string color, int &r, int &g, int &b)
{    
  dyn_string dsAp;
  color = substr(color,1,strlen(color)-1);
  dsAp = strsplit(color,",");
  r = dsAp[1];
  g = dsAp[2];  
  b = dsAp[3];
  return(makeDynInt(r,g,b));
}

/* ------------------------------------------------

Name: 	dcsUiColor_luminosity()
Desc:   Calculate the luminosity from RGB
        
inp :    r:= int      (red component)
	g:= int      (green component)
	b:= int      (blu component)

out :  luminosity:= float
       
Auth : A.Franco - INFN Bari

// from Hermann Fuchs 

Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */ 
float dcsUiColor_luminosity(int r, int g, int b)
{
  float normalizedR,normalizedG,normalizedB;
  float calcR,calcG,calcB;
  float luminosity;
  
  normalizedR = r/255.0;
  normalizedG = g/255.0;
  normalizedB = b/255.0;
  
  //calcR
      if (normalizedR<=0.03928) calcR=normalizedR/12.92;
      else  calcR= pow((normalizedR+0.055),2.4);
  //calcG    
      if (normalizedG<=0.03928) calcG=normalizedG/12.92;
      else  calcG= pow((normalizedG+0.055),2.4);
  //calcB
      if (normalizedB<=0.03928) calcB=normalizedB/12.92;
      else  calcB= pow((normalizedB+0.055),2.4);
 
  luminosity = 0.2126 * calcR + 0.7152 * calcG + 0.0722 * calcB;     
  return(luminosity);

}

/* ------------------------------------------------

Name: 	dcsUiColor_RGB2Croma()
Desc:   Convert RGB components into a HSV
        
inp :    red:= int      (red component)
	green:= int      (green component)
	blu:= int      (blu component)

out : 	h := float  (hue value)
	s := float  (saturation value)
	v := float  (intensity value)
 return hue value;
 
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */ 
float dcsUiColor_RGB2Croma(int red, int green, int blu, float &h, float &s, float &v)
{
  // -------------
  float mn=red;
  float mx=red;
  int maxVal=0;
 
  if (green > mx){ 
    mx=green;
    maxVal=1;
  }
  if (blu > mx){ 
    mx = blu;
    maxVal=2;
  } 
  if (green < mn)
    mn=green;
  if (blu < mn) 
    mn=blu; 

  float delta = mx - mn;

  v = mx; 
  if( mx != 0 )
    s = delta / mx; 
  else {
    s = 0;
    h = 0;
    return(0.0);
  }
  if (s == 0.0) {
    h=-1;
    return(h);
  } else { 
    switch (maxVal) {
      case 0:
        h = (float)( green - blu ) / delta;
        break;
      case 1:
        h = 2 + ( blu - red ) / delta;
        break;
      case 2:
        h = 4 + ( red - green ) / delta;
        break;
    }
  }
  if( h < 0.0 ) h = 5 + h;  
  if(h<=1) h *= 120.0;
  else if(h<=2) h = 60.0 + h * 60.0;
  else if(h<=3) h = 120.0 + h * 30.0;
  else if(h<=5) h = 30.0 + h * 60.0;
  else h = 180.0 + h * 30.0;
  
  return(h);
}

/* ------------------------------------------------

Name: 	dcsUiColor_croma2RGB()
Desc:   convert HSV to RGB
     
inp: 	h := float  (hue value)
	s := float  (saturation value)
	v := float  (intensity value)
	
out:    red:= int      (red component)
	green:= int      (green component)
	blu:= int      (blu component)


 return color string;
 
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */ 
string dcsUiColor_croma2RGB(float h, float s, float v, int &red, int &green, int &blu)
{
    int i;
    float f, p, q, t;
    float hTemp;
 
    if( s == 0.0 || h == -1.0) 
    {
      red = green = blu = v;
      return("{"+red+","+green+","+blu+"}");
    }

    if (h>=360.0) h -= 360.0;
    
    if(h<=120.0) hTemp = h / 120.0;
    else if(h<=180.0) hTemp = (h-120.0) / 60.0 + 1;
    else if(h<=210.0) hTemp = (h-180.0) / 30.0 + 2;
    else if(h<=270.0) hTemp = (h-210.0) / 60.0 + 3;
    else if(h<=330.0) hTemp = (h-270.0) / 60.0 + 4;
    else hTemp = (h-330.0) / 30.0 + 5;
    
    i = (int)floor( hTemp );    // which sector
    f = hTemp - i;              // how far through sector
    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );
 
    switch( i ) {
    case 0:{red = v;green = t;blu = p;break;}
    case 1:{red = q;green = v;blu = p;break;}
    case 2:{red = p;green = v;blu = t;break;}
    case 3:{red = p;green = q;blu = v;break;} 
    case 4:{red = t;green = p;blu = v;break;}
    case 5:{red = v;green = p;blu = q;break;}
    }
    return("{"+red+","+green+","+blu+"}");
}


/* ------------------------------------------------

Name: 	dcsUiColor_getComplementaryRGB()
Desc:   get the complementary RGB components
     
inp: 	red:= int      (red component)
	green:= int      (green component)
	blu:= int      (blu component)
	
	satTh:= float (Saturation threshold for the Contrast)
	lumTh:= int (Luminosity threshold for the Contrast,
			-1 = differenzial)
	
out:    red:= int      (red component)
	green:= int      (green component)
	blu:= int      (blu component)

 
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */ 
void dcsUiColor_getComplementaryRGB(int &red, int &green, int &blu, float satTh = 0.3, int lumTh = -1)
{   
  // calculate the luminosity
  float lum;
  lum = dcsUiColor_luminosity(red, green, blu); 
  
  // First calculate the Croma
  float croma,sat,val;
  dcsUiColor_RGB2Croma(red, green, blu, croma, sat, val);

  if(sat < satTh) {
    if(val < 128) { red = green = blu = 255 ;}
    else { red = green = blu = 0 ;}
    return;
  }      
  //  move the hue of 180 degrees 
  croma += 180.0;
  if(croma < 0.0) { croma += 360.0; }
  if(croma >= 360.0) { croma -= 360.0; }

  // correct the luminosity
  // -1 := differntial
  if(lumTh == -1) {
    val += 128.0;
    if(val > 255.0) { val -= 255.0; }
  } else {
    // flip flop
    if(lum >= lumTh) {
      val = 0.0;
    } else {
      val = 255.0;
    }
  }  

  // return back to RGB
  dcsUiColor_croma2RGB(croma, sat, val, red, green, blu);
  return; 
} 

/* ------------------------------------------------

Name: 	dcsUiColor_getComplementaryColor()
Desc:   return a color string that is the complementary color
     
inp: 	color:= string      (color string or static name)
	isBW:= bool (true = Black or White color return
			false = Color return)
	
out:    color string
	 
Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */ 
string dcsUiColor_getComplementaryColor(string color, bool isBW = true)
{ 
  int r,g,b;
  float l;
  
  if(strpos(color,"{") < 0)
	color = dcsUiColor_convertColorName( color );
	  
  dcsUiColor_string2RGB(color, r, g, b);
  
  if(isBW) {
    l = dcsUiColor_luminosity(r, g, b);
	if(l<0.7) r = 255; else r = 0;
	return("{"+r+","+r+","+r+"}");
  } else {
    dcsUiColor_getComplementaryRGB(r, g, b, 0.3, 120);
	return("{"+r+","+g+","+b+"}"); 
  }	
} 

/* ------------------------------------------------

Name: 	dcsUiColor_setColor()
Desc:   sets the foreCol and backCol properties of a shape
     
inp: 	shapename:= string      (name of shape)
	color:= string      (color string or color name)
	isBW:= bool (true = Black or White color return
			false = Color return)
	

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
void dcsUiColor_setColor(string shapename, string color, bool isBW = true)
{ 
  int r,g,b;
  if(strpos(color,"{") < 0)
	color = dcsUiColor_convertColorName( color );
	
  dcsUiColor_string2RGB(color, r, g, b);
  if(isBW) {
    l = dcsUiColor_luminosity(r, g, b);
	if(l<0.7) r = 255; else r = 0;
	g = r;
	b = r;
  } else {
	dcsUiColor_getComplementaryRGB(r, g, b, 0.3, 120);
  }
  setMultiValue(shapename, "backCol", color, 
                shapename, "foreCol", "{"+r+","+g+","+b+"}" );
  return; 
} 
// -------------------------------------------------------------



/* -------------------------------------------------------------
	Functions to Manage the Scope

	F.A. ver 1.0   16/09/2008

	History

-------------------------------------------------------------- */
void dcsUi_addScopePlot(string sLabel, string sDpName, string sTabName = "TEMP")
{
  if( !isModuleOpen("dcsUiScopeModule" ) ){
    ModuleOnWithPanel("dcsUiScopeModule", 75, 100, 0, 0, 1, 1, "None", 
                      "dcsScope/dcsUiScope.pnl", "dcsUiScope",
                      makeDynString("$PARAM:"+sDpName+";"+sLabel+";"+sTabName));    
  } else {
    if( !isPanelOpen("dcsUiScope", "dcsUiScopeModule" ) ){
      ModuleOnWithPanel("dcsUiScopeModule", 75, 100, 0, 0, 1, 1, "None", 
                        "dcsScope/dcsUiScope.pnl", "dcsUiScope",
                        makeDynString("$PARAM:"+sDpName+";"+sLabel+";"+sTabName));    
    } else {
      setValue("dcsUiScopeModule.dcsUiScope:carrier","text",
               sDpName+";"+sLabel+";"+sTabName);
    }  
  }    
  return;
}  

void dcsUi_closeScope()
{
  if( isModuleOpen("dcsUiScopeModule" ) ){
    ModuleOff( "dcsUiScopeModule");    
  } 
  return;
}  


/* -------------------------------------------------------------
	Function to test the status of Net Connection

	F.A. ver 1.0   19/12/2006

	History
  13/11/2009 -  Fixed bug related to multiple access to results...
  
-------------------------------------------------------------- */
bool dcsUiBase_TestNetworkConnection(string sHostName)
{

string sh_com;

	if(_UNIX)
	{
		sh_com = "ping -c 1 -w 1 "+sHostName;
		system(sh_com + " > " + getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt &");
		fileToString(getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt",sh_com);
		return( (strpos(sh_com,"0% packet loss") < 0) ? false : true);
	}
	else
	{
		sh_com = "ping -n 1 -w 1000 "+sHostName;
		system("cmd /MIN /c " + sh_com + " > " + getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt");
		fileToString(getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt",sh_com);
		return( (strpos(sh_com,"Lost = 0") < 0) ? false : true);
	}
}

/* -------------------------------------------------------------
	Function to use Timed Funcion timers...

	F.A. ver 1.0   07/11/2009

        
	History
          07/11/2009 - created
-------------------------------------------------------------- */
// constants
const string DCSTIMERDP_PREFIX = "dcsUi_";
const string DCSTIMERDP_TAIL = "_timer";
const string DCSTIMERDP_DEFAULT = "generic";

const string TIMEDFUNCTION_DPT = "_TimedFunc";
const int DCSTIMER_DEFAULTINTERVAL = 60;
 
// Set the timer variable
bool dcsUi_setTimerDP(string type, int interval = DCSTIMER_DEFAULTINTERVAL)
{		
  bool bResult;
  string sDpname = dcsUi_getTimerDPName(type);
  time t; 
 
  if(!dpExists(sDpname)) {
    if(dpCreate(sDpname, TIMEDFUNCTION_DPT) != 0) {
      bResult= false;
      DebugTN("dscUiBase_setTimedDP : error creating timer="+type+". Abort operation!");
      return(bResult);
    }
  } 

  if(interval < 1) interval = DCSTIMER_DEFAULTINTERVAL;
  
  if( dpSet(getSystemName() +sDpname+".validFrom:_original.._value",setPeriod(t, 0),  
        getSystemName() +sDpname+".validUntil:_original.._value",setPeriod(t, 0),  
        getSystemName() +sDpname+".time:_original.._value",makeDynInt(),  
        getSystemName() +sDpname+".interval:_original.._value",interval,
        getSystemName() +sDpname+".syncTime:_original.._value",-1) == 0) {  
  
    dyn_errClass err = getLastError();  
    if (dynlen(err) > 0) {
      errorDialog(err);  
      bResult= false;
    } else {
      bResult = true;
    }
  } else {
    bResult= false;
  }

  if(!bResult) {
    DebugTN("dscUiBase_setTimedDP : error setting timer="+type+". Abort operation!");
  }
  return(bResult);

}

// stop the timer
void dcsUi_stopTimer(string type)
{
  string sDpname = dcsUi_getTimerDPName(type);
  dpSet(sDpname+".validUntil", getCurrentTime() ); 
  return; 
}

// get the timer DP name
string dcsUi_getTimerDPName(string type) 
{
  if(type == "") type = DCSTIMERDP_DEFAULT;
  return(DCSTIMERDP_PREFIX+strtolower(type)+DCSTIMERDP_TAIL);
}
// remove the timer variable
void dcsUi_removeTimer(string type)
{
  string sDpname = dcsUi_getTimerDPName(type);
  
  if(dpExists(sDpname)) {
    dpDelete(sDpName);    
    DebugTN("dscUiBase_removeTimer : delete timer="+type+".");
  }
  return;
}
// ---------------------------------------------------------------------------

/* -------------------------------------------------------------
	Functions to manage apply the mode configuration file

	F.A. ver 1.0   24/09/2008

 
        return := true if it is done

	History
      1/1/2012   -   Defined Obsolete ! Out of Box
-------------------------------------------------------------- */
bool dcsUiFSMConfig_applyFSMModeConfiguration(string sFileName)
{
  int i,j,k;
  file f;
  int lockedOut, count;
  string sIdManager, sRootNode;
  dyn_string dsLine,dsTemp;
  bool flag;
  
  // then open the config file
  f = fopen(sFileName, "r");
  i=ferror(f);
  if (i!=0) {
    DebugTN("dcsUiFSMtool_applyFSMModeConfiguration: Error opening the file "+sFileName+" (Err no. "+i+")"); 
    fclose(f); 
    return(false);
  }

        
  // Read the top 
  flag = __dcsUiFSMConfig_ReadAndFormat(f, dsLine); // the first line is the Domain
  sRootNode = dsLine[2]; 

  // Take the entire Hierarchy and verify that it don't have a different
  // ownership
  fwUi_getOwnership(sRootNode, sIdManager);  
  if( sIdManager != fwUi_getUiId() && sIdManager != "" ) {
    DebugTN("dcsUiFSMtool_applyFSMModeConfiguration: Error Hierarchy is taken from other managers !");
    fclose(f); 
    return(false);
  }  
          
  dcsInfoLog_SendMsg("dcsUi User:"+getUserName()+" Mngr:"+myManNum()+" Source: dcsUiFSMtool_applyFSMModeConfiguration()",
                     "FSM Domain: apply the configuration file named:"+sFileName+". Starting...", "I", 1);
        
  // Take the Hierarcy          
  fwCU_takeTree(sRootNode, sIdManager);        
  delay(1,0);
  
  // Starts the reading loop            
  flag = __dcsUiFSMConfig_ReadAndFormat(f, dsLine);
  while(flag) {
    switch(dsLine[4]) {
      case "LU":
      case "DU": // This for DU and LU
        if(dsLine[1]== "ENABLED")
          fwCU_enableObj(dsLine[2],dsLine[3]);  // node,obj
        else
          fwCU_disableObj(dsLine[2],dsLine[3]); 
        break;
              
      case "CU":  // this is for CU
        fwUi_getOwnership(dsLine[2], sIdManager);  // node
        fwUi_getLockedOut(dsLine[2], dsLine[3]+"::"+dsLine[3], lockedOut);  // domain, node::obj              

        if(dsLine[1]== "UNLOCKEDOUT"){ // The command is to unlock
          if(lockedOut == 1) { // do command
            fwUi_unLockOutTree(dsLine[2], dsLine[3]+"::"+dsLine[3], sIdManager);
          }
        } else {  // The command is to LOCK
          if(lockedOut == 0) { // do command
            fwUi_excludeTree(dsLine[2], dsLine[3], sIdManager);
            fwUi_lockOutTree(dsLine[2], dsLine[3]+"::"+dsLine[3], sIdManager); 
          }
        }                                        
        break;
    }
    flag = __dcsUiFSMConfig_ReadAndFormat(f, dsLine);
  }
  // done !
  fwCU_releaseTree(sRootNode, sIdManager);        

  dcsInfoLog_SendMsg("dcsUi User:"+getUserName()+" Mngr:"+myManNum()+" Source: dcsUiFSMtool_applyFSMModeConfiguration()",
                     "FSM Domain: configuration file named:"+sFileName+". Applied!", "I", 1);
                
  fclose(f);
  return(true);

}

bool __dcsUiFSMConfig_ReadAndFormat(file f,dyn_string &line)
{
  string sBuf = "";
  int i;
  dyn_string dsAppo;
        
  i = fgets(sBuf,1000,f);
  if (i==0) return(false);
		
  sBuf = substr(sBuf,0,strlen(sBuf)-1);
	
  dsAppo = strsplit(sBuf,"=");
  if(dynlen(dsAppo) < 2) {
    line[4] = "";
    return(true);
  }
        
  line[1] = dsAppo[2];
        
  dsAppo = strsplit(dsAppo[1],":");
  if(dynlen(dsAppo) < 4) {
    line[4] = "";
    return(true);
  }
              
  line[2] = dsAppo[2];
  line[3] = dsAppo[4];
  line[4] = dsAppo[1];

  return(true);
}


/* -------------------------------------------------------------
	Functions to manage the FSM Hierarchy on a file

	F.A. ver 1.0   18/10/2007

 
        return := true if write is done

	History

-------------------------------------------------------------- */

const int FSM_CU = 1;
const int FSM_DU = 2;
const int FSM_LU = 0;
const string FSMHIERARCHYFILE = "dcsUiFSMHierarchy.csv";
const string FSMHIERARCHYFILEORDERED = "dcsUiFSMOrderedHierarchy.csv";

string dcsUiFSMtool_getHierarchyToFileName(string sRootName, bool withCreation = false, bool isOrdered = false)
{
  file f;
  string fileName;

  if(!isOrdered)
    fileName = getPath(CONFIG_REL_PATH) + FSMHIERARCHYFILE ;
  else
    fileName = getPath(CONFIG_REL_PATH) + FSMHIERARCHYFILEORDERED ;

  if(!withCreation) {
    return(fileName);
  } 
  if(withCreation) {
    if(!dcsUiFSMtool_isHierarchyFileUpdated(sRootName))
      dcsUiFSMtool_writeHierarchyToFile(sRootName);
    return(fileName);
  }  
  
} 

bool dcsUiFSMtool_isHierarchyFileUpdated(string sRootNode)
{
  string sFileName, sPath;
  time tSmi, tHier;
  int res,i;
  // First Catch the File date/time for the SMI
  for(i=1;i<= SEARCH_PATH_LEN ;i++){
    sPath = getPath(BIN_REL_PATH,"", 1,i);
    strreplace(sPath, "bin/", "");
    sFileName =  sPath +"smi/"+ sRootNode + ".sobj";
    if(isfile(sFileName)) {
      tSmi = getFileModificationTime(sFileName);
        
      // Catch the Hierarchy File
      sFileName = getPath(CONFIG_REL_PATH) + FSMHIERARCHYFILE ;
      if(!isfile(sFileName))
        return(false); // Not Exists -> Not updated
       
      tHier = getFileModificationTime(sFileName);
      if(tHier <= tSmi)
        return(false);
      else
        return(true);
    }
  }  
  return(false);
}

bool dcsUiFSMtool_writeHierarchyToFile(string sRootNode)
{
  
  file f;
  string fileName;

  // Open file for writing  
  fileName = getPath(CONFIG_REL_PATH) + FSMHIERARCHYFILE ;
  f = fopen(fileName,"w");
  if(ferror(f)) {
    DebugTN("dcsUiFSMtool_writeHierarchyToFile : Error to open file for Writing !");
    return(false);  
  }
  
  // Create the File Header
  fputs("FSM HIERARCHY DESCRIPTION  v.1.1\n", f);
  fputs("CREATION DATE:"+formatTime("%Y:%m:%d:%H:%M:%S",getCurrentTime())+"\n", f);
  fputs("SOD\n", f);
  
  // Create the Root
  string sLabel = ""; 
  string sRootDomain = sRootNode;
        
  dcsUi_getFsmLabel(sRootDomain ,sRootDomain , sLabel);
  fputs("CU,.,"+sLabel+",Root,"+sRootDomain+","+sRootDomain+",@\n", f);
  
  // Create Children
  int num;
  num = __dcsUiFSMtool_writeChildrenToFile(sRootDomain,sRootDomain,2,"Root",f)+1;

  // mark the eof
  fputs("===EOF===\n", f);
  
  fclose(f);
  
  // ============================================
  if(!__dcsUiFSMtool_createOrderedFile()) return(false);
  
  
  return(true);
}


bool __dcsUiFSMtool_createOrderedFile()
{  
  
  // first open the Hierarchy file
  file fs,fd;
  string fileName, fileNameOrd;

  // Open file for reading 
  fileName = getPath(CONFIG_REL_PATH) + FSMHIERARCHYFILE ;
  fs = fopen(fileName,"r");
  if(ferror(fs)) {
    DebugTN("dcsUiFSMtool_writeHierarchyToFile : Error to open file for Reading !");
    return(false);  
  }
  
  // Open ordered file for writing  
  fileNameOrd = getPath(CONFIG_REL_PATH) + FSMHIERARCHYFILEORDERED ;
  fd = fopen(fileNameOrd,"w");
  if(ferror(fd)) {
    DebugTN("dcsUiFSMtool_writeHierarchyToFile : Error to open file for Writing !");
    fclose(fs);
    return(false);  
  }

  // Create the File Header
  fputs("FSM ORDERED HIERARCHY DESCRIPTION  v.1.2\n", fd);
  fputs("CREATION DATE:"+formatTime("%Y:%m:%d:%H:%M:%S",getCurrentTime())+"\n", fd);
  fputs("SOD\n", fd);
  
  int ln;
  string Buffer;
  bool bContinue = true;
  bool bIsThereNextLevel = false;
  int iSerchLev = 0;
  int lev;
  dyn_string dsAp;
  
  while(bContinue){
    // move pointer to the  start and skip the header
    fseek(fs,0,SEEK_SET );
    ln = fgets(Buffer, 1200, fs);
    while(strpos(Buffer,"SOD") < 0) ln = fgets(Buffer, 1200, fs);
    
    // Set for search level
    bIsThereNextLevel = false;
    iSerchLev++;
     
    // loop into the file
    ln = fgets(Buffer, 1200, fs);
    while(strpos(Buffer,"===EOF===") < 0) {
      dsAp = strsplit(Buffer,",");
      lev = strlen(dsAp[2]);
      if(lev == iSerchLev) // is good for Ordered file
          fputs(Buffer, fd);
      if(lev == (iSerchLev+1)) bIsThereNextLevel = true;
      ln = fgets(Buffer, 1200, fs);      
    }    
    
    // nothing to do exit
    if( !bIsThereNextLevel ) bContinue = false;
  }

  // mark the eof
  fputs("===EOF===\n", fd);
  
  fclose(fd);
  fclose(fs);
  
  return(true);
}    


int __dcsUiFSMtool_writeChildrenToFile(string Node, string Domain,int level,string ParentKey, file f)
{
  dyn_string dsAppo,dsLabel;
  dyn_int diAppo;
  string sLabel, localDomain, sKey, sDomEx;
  int i , num , imageType, count;

  string ty,sp,dom;
  int j;
  sp = ""; for(j=1;j<=level;j++) sp=sp+".";   

  dsAppo = fwCU_getChildren(diAppo, Domain +"::"+ Node);
  dynClear(dsLabel);
  
  num = dynlen(dsAppo);
  for(i=1;i<=num;i++)
  {
    sDomEx = Domain;
    
    // - Added 05/06/07 -
    if(fwFsm_isAssociated(dsAppo[i])) {
		sDomEx = fwFsm_getAssociatedDomain(dsAppo[i]);
		if(sDomEx != Domain) {
			dsAppo[i] = fwFsm_getAssociatedObj(dsAppo[i]);
			if(fwFsm_isDU(sDomEx,dsAppo[i])) diAppo[i] = FSM_DU;
		}
    } else {
      sDomEx = Domain;
    }
    // - ---------------- -
    switch(diAppo[i])
    {
		case FSM_DU:  // DU:
			dcsUi_getFsmLabel(sDomEx, dsAppo[i], sLabel);
			dsLabel[i]=sLabel;
			localDomain = sDomEx;
			imageType = FSM_DU;
			break;
		case FSM_CU: //CU:
			dcsUi_getFsmLabel(dsAppo[i], dsAppo[i], sLabel);
			dsLabel[i]=sLabel;        
			localDomain = dsAppo[i];
			imageType = FSM_CU;
			break;
		case FSM_LU:  // LU OBJ:
			dcsUi_getFsmLabel(sDomEx, dsAppo[i], sLabel);
			dsLabel[i]=sLabel;
			imageType = FSM_LU;
			localDomain = sDomEx;
			break;
    }			
    sKey = ParentKey + "_" +i;
  }

  // this write the file and goes throw the hierarchy
  for(i=1;i<=num;i++)
  {
    sKey = ParentKey + "_" +i;
    switch(diAppo[i])
    {
		case FSM_DU:  // DU:
			dom = Domain ;
			localDomain = "";
			ty = "DU";
			break;
		case FSM_CU: //CU:
			localDomain = dsAppo[i];
			dom = Domain;//"";        
			ty = "CU";
			break;
		case FSM_LU:  // LU OBJ:
			localDomain = Domain;
			dom = localDomain ;        
			ty = "LU";        
			break;
    }
    fputs(ty+","+sp+","+dsLabel[i]+","+sKey+","+dom+","+dsAppo[i]+",@\n",f);
    
    if(localDomain != "")			
      count += __dcsUiFSMtool_writeChildrenToFile(dsAppo[i],localDomain,level+1,sKey,f);
    
  }

  count += num;
  return(count);
}
// -------------------------------------------------------------------------




/* ==============================================================
	FSM FUNCTIONS SECTION
================================================================ */
/* -------------------------------------------------------------
	Function to connect the FSM Status with the dpExists test
	F.A. ver 1.0   01/03/2007
        node  := DOMAIN::OBJECT
        return := true if connecton is done
	History
      16/04/2009 - Use the Dpconnect in order to display the
                   status every time
-------------------------------------------------------------- */
bool dcsUi_FsmConnectState(string CBFunction, string node, bool dispever=false)
{
  // Verify the existence of FSM Dp
  string sDpName;
  fwCU_getDp(node, sDpName, "STATE");
  if(!dpExists(sDpName)){
    DebugTN("dcsUi_FsmConnectState() : "+sDpName+" not Exists !");
    return(false);
  }
  // connect
  if(dispever)
    dpConnect(CBFunction,true,sDpName);
  else
    fwCU_connectState(CBFunction,node);
  return(true);
}

/* -------------------------------------------------------------
	Place an FSM Object with a CU type lock in the FSM main panel.
	F.A. ver 1.0   15/11/2006
	History
		Imported from Fsm 24.13

      @param node: The node name (CU) for this panel (available in $node).
      @param obj: The object name (a CU) (either $obj or the result of fwFsmUi_getXXXChildren()).
      @param x: The X coordinate for the Object.
      @param y: The Y coordinate for the Object.
      @return The X coordinate of the end of the object, if the user wants to place an adjacent widget

-------------------------------------------------------------- */
int dcsUi_addFsmObjWithLock(string node, string obj, int x, int y, string part = "", string mode = "MODAL", bool operable = true)
{
  int nextx;
  addSymbol(myModuleName(),myPanelName(),"dcsUi/dcsUiFsmObjWithLock.pnl",
  	obj, makeDynString("$domain:"+node,"$obj:"+obj,"$part:"+part,"$mode:"+mode,"$operable:"+operable),
  	x, y, 0,1,1);
  nextx = x+181+148+22;
  return nextx;
}

/* -------------------------------------------------------------
	Place an FSM Object with a LU/DU/Obj type lock (enable/disable widget) in the FSM main panel.
	F.A. ver 1.0   15/11/2006
	History
		Imported from Fsm 24.13

      @param node: The node name (CU) for this panel (available in $node).
      @param obj: The object name (a CU) (either $obj or the result of fwFsmUi_getXXXChildren()).
      @param x: The X coordinate for the Object.
      @param y: The Y coordinate for the Object.
      @return The X coordinate of the end of the object, if the user wants to place an adjacent widget

-------------------------------------------------------------- */
int dcsUi_addFsmObjWithDevLock(string node, string obj, int x, int y, string part = "",string mode = "MODAL" , bool operable = true)
{
  int nextx;
  addSymbol(myModuleName(),myPanelName(),"dcsUi/dcsUiFsmObjWithDevLock.pnl",
            obj,makeDynString("$domain:"+node,"$obj:"+obj,"$part:"+part,"$mode:"+mode,"$operable:"+operable),
	    x, y, 0,1,1);
  nextx = x+181+148+22;
  return nextx;
}

/* ==============================================================
	FSM FUNCTIONS WRAPPER SECTION
	This section is for Wrapping the FSM private function
================================================================ */

/* -------------------------------------------------------------
	Function to open the FSM Control Panel
	F.A. ver 1.0   15/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiMainPanel.pnl
//           panels\dcsUi\dcsUiFsmObjWithDevLock.pnl
//            panels\dcsUi\dcsUiFsmObjWithLock.pnl
void dcsUi_openFsmControlPanel(string node,string object, 
                          int mode=DCSUI_FSM_MODLESS, string parent = "")
{
  switch(mode){
    case DCSUI_FSM_MODAL:
      fwUi_setTopDomain(node, object, parent);
      ChildPanelOnModal("dcsUi/dcsUiFsmUi.pnl", "FSM Control :"+object,
                        makeDynString("$node:" + node, "$obj:" + object, "$mode:MODAL" ),
                        75, 100);		
      break;
    case DCSUI_FSM_MODLESS:
      fwUi_setTopDomain(node, object, parent);
      ModuleOnWithPanel ("FSM Control :"+object, 75, 100, 0, 0, 1, 1, "None", 
                         "dcsUi/dcsUiFsmUi.pnl", "FSM Control :"+object, 
                         makeDynString("$node:" + node, "$obj:" + object, "$mode:MODLESS"));
      break;
  }			
  return;
}

void dcsUi_closeFsmControlPanel()
{ 
  dyn_string dsMod;
  dsMod = getVisionNames();
  int i;
  for(i=1;i<=dynlen(dsMod);i++){
    if(strpos(dsMod[i],"FSM Control")>=0) {
      if(isModuleOpen(dsMod[i])){
        DebugTN("dcsUi_closeFsmControlPanel: "+dsMod[i]);    
        ModuleOff(dsMod[i]);
      }
    }      
  }
  return;
}


/* -------------------------------------------------------------
	Initialize the FSM.
	F.A. ver 1.0   25/11/2006
	History
       03/11/2010 - Add the function that sets the Identity...
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// Called in  panels\fwFSM\Root.pnl, fwFSM\FSMOperate.pnl
// used by : panels\dcsUi\dcsUiMainPanel.pnl
void dcsUi_FsmInitialize()
{
  string id;
  fwFsm_startShowFwObjects();
  fwFsm_initialize();
  id = fwUi_getGlobalUiId();
  fwUi_setManagerIdInfo(id);
  return;
}

/* -------------------------------------------------------------
	Init the FSM control ui panel.
	F.A. ver 1.0   25/1/2014
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiFsmUi.pnl

void dcsUi_fwFsmUi_init(string node, string obj)
{
  DebugTN("dcsUi_fwFsmUi_init() : +++ Opening Panel", node, obj);

	fwFsm_initialize(0);
//  fwUi_setPanelSize(node, obj);
	fwUi_getAccess(node);
	fwUi_getDisplayInfo(makeDynString(node));
	if(!globalExists("FwFSMUi_ModePanelBusy"))
		addGlobal("FwFSMUi_ModePanelBusy", INT_VAR);
  return;
}


/* -------------------------------------------------------------
	Get the user Panel associated with an FSM object.
	F.A. ver 1.0   25/11/2006
 History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiAuxiliaryWindow.pnl
//           panels\dcsUi\dcsUiMainPanel.pnl
void dcsUi_getFsmUserPanel(string domain, string object, string &PanelName)
{
  string sAp;
  fwUi_getUserPanel(domain, object, sAp);
  PanelName = sAp;
}

/* -------------------------------------------------------------
	Get the user Panel Path .
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiAuxiliaryWindow.pnl
string dcsUi_getFsmPanelPath(string Panel)
{
  return(fwUi_getPanelPath(Panel));
}

/* -------------------------------------------------------------
	Get the Label of a Fsm Node.
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : a lot of panels...
void dcsUi_getFsmLabel(string domain, string object, string &label)
{
  string sAp;
  fwUi_getLabel(domain, object, sAp);	
  label = sAp;
  return;
}

/* -------------------------------------------------------------
 Test if an object is a DU
 F.A. ver 1.0   25/11/2006
 History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// called by : fwFSMuser\fwUi.pnl
// used in : dcsUi\dcsUiFsmUi.pnl
//           dcsUi\dcsUiFSMtree.pnl
//           dcsUi\dcsUiAuxiliaryWindow.pnl
bool dcsUi_FsmIsDU(string domain, string object)
{
  return(fwFsm_isDU(domain,object));
}

/* -------------------------------------------------------------
	Test if an object is a CU
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// calledy by fwFSMuser\fwUi.pnl
// used in : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl
//          panels\dcsUi\dcsUiFSMObjStatusDisplay.pnl
//       panels\dcsUi\dcsUiFsmUi.pnl
bool dcsUi_FsmIsCU(string domain, string object)
{
  return(fwFsm_isCU(domain,object));
}


/* -------------------------------------------------------------
	Get the Device Physical Name
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
/*  -----------------------------------------------
  Replaced with fwCU_getObjDp()
  that is PUBLIC !
 
string dcsUi_getFsmPhysicalDeviceName(string domain, string object)
{
  string sAp, device;
  fwUi_getDomainSys(domain, sAp);
  device = fwDU_getPhysicalName(sAp+object);
  return(sAp+device);
}
------------------------------------------------ */

/* -------------------------------------------------------------
	Get the Node/Obj for associated
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiFsmObjWithDevLock.pnl,
//           panels\dcsUi\dcsUiFsmObjWithLock.pnl,
//           panels\dcsUi\dcsUiFsmUi.pnl
void dcsUi_translateFsmNodeObj(string &node, string &obj)
{
  if(fwFsm_isAssociated(obj)){
    node = fwFsm_getAssociatedDomain(obj);
    obj = fwFsm_getAssociatedObj(obj);	
  }else{
    node = node;
    obj = obj;
  }
  return;
}

/* -------------------------------------------------------------
	Show an FSM object
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
/*  -----------------------------------------------
  Replaced with fwFsmUi_view()
  that is PUBLIC !
 
void dcsUi_showFsmObject(string node, string obj, string sParent)
{
  fwUi_showFsmObject(node, obj, sParent);
}
-------------------------------------------------- */


/* -------------------------------------------------------------
	Simple FSM initialize
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// -----------  called in panels\fwFSM\FSMOperate.pnl and more...
// used by : panels\dcsUi\dcsUiFSMtree.pnl
void dcsUi_FsmSimpleInitialize()
{
  fwFsm_initialize(0);
}

/* -------------------------------------------------------------
	Get the FSM Hierarchy
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// -----------  called in panels\fwFSM\Root.pnl  ----------------
// used by : dcsUi\dcsUiFSMtree.pnl
string dcsUi_FsmGetDomains()
{
  return( fwFsm_getDomains() );
}

/* -------------------------------------------------------------
	Get the Mode Bits
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
/* ------ Not more used ! --------------------------------------
bit32 dcsUi_FsmGetCUModeBits(string SubDomain)
{
  // --- NOT Public FSM Function call ------------------------
  return(fwUi_getCUModeBits(SubDomain) );
  // ---------------------------------------------------------}
}
------------------------------------------------------- */

/* ------ Not more used ! --------------------------------------
bit32 dcsUi_FsmGetOwnModeBits()
{
  return( fwFsmUi_getOwnModeBits() );
}
------------------------------------------------------- */

/*  -----------------------------------------------
  Replaced with fwFsmUi_getModeBits()
  that is PUBLIC !
 
bit32 dcsUi_FsmGetDUModeBits(string Domain, string Object)
{
  // --- NOT Public FSM Function call ------------------------
  return(fwUi_getModeBits(Domain, Object));
  // ---------------------------------------------------------}
}
---------------------------------------------------- */


/* -------------------------------------------------------------
	Get the Ownership 
	F.A. ver 1.0   07/12/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmControllHierarchyWidget.pnl
string dcsUi_FsmGetOwnership( string node)
{
  string result;
  fwUi_getOwnership(node, result);
  return(result);
}
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl   
string dcsUi_FsmGetModeObj(string node, string object) {
  return(fwUi_getModeObj(node,object));
}
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl  
void dcsUi_FsmCheckOwnership(string cu, int &enabled, string &owner) {
  fwUi_checkOwnership(cu,enabled,owner);
  return;
}
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl   
string dcsUi_FsmGetManagerIdInfo(string id) {
  return(fwUi_getManagerIdInfo(id));
}

/* ------------------------------------------------------
	Verify that I'm the Owner 
	F.A. ver 1.0   27/05/2008
	History
------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiFsmUi.pnl   
bool dcsUi_FsmIsMine(string domain, string object)
{
  bit32 statusBits = fwFsmUi_getModeBits(domain,object);
  string state = fwUi_getCUMode(domain, object);
  string cu = dcsUi_FsmGetModeObj(domain, object);
  int enabled;
  string owner;
  fwUi_checkOwnership(cu, enabled, owner);        
  // FSM cotrol
  if( owner == getSystemName()+"Manager"+myManNum() && getBit(statusBits,FwOwnerBit) ) return(true);
  if( owner != "" && !getBit(statusBits,FwExclusiveBit) )  return(true);
  return(false);
}   



/* -------------------------------------------------------------
	Connect a CallBack Funct. to the Mode Bit etc..
	F.A. ver 1.0   12/01/2007
	History
-------------------------------------------------------------- */
/*  -----------------------------------------------
  Replaced with fwFsmUi_connectModeBits()
  that is PUBLIC !
  
int dcsUi_FsmConnectModeBits(string callback, string domain, string obj, string part = "")
{ 
  int ret;
  ret = fwUi_connectModeBits(callback, domain, obj, 1, part);
  return(ret);
}
------------------------------------------------------------ */

/*  -----------------------------------------------
  Replaced with fwFsmUi_disconnectModeBits()
  that is PUBLIC !
  
void dcsUi_FsmDisconnectModeBits(string domain, string obj)
{
  fwUi_disconnectModeBits(domain, obj);
  return;
}
------------------------------------------------------------ */

/*  -----------------------------------------------
  Replaced with fwFsmUi_getCUMode(string domain, string obj)
  that is PUBLIC !
  
string dcsUi_FsmGetCUMode(string domain, string obj)
{
  return( fwUi_getCUMode($node, $obj) );
}
-------------------------------------------------  */

/* -------------------------------------------------------------
	Start stop Tree on distributed systems
	F.A. ver 1.0   18/01/2007
	History
    1/1/2012 -  Redefined  and Add the domain
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// -----------  called in fwFsmTreeDisplay.ctl  ----------------
// used by : dcsFsmTools\dcsFsmObjNodeWidget.pnl
int dcsUi_FsmRestartTree(string domain, string sysName)
{ 
    fwFsm_restartTreeDomains(domain, sysName);
    delay(0,200);
    return;
}
void dcsUi_FsmStopTree(string domain, string sysName)
{
    fwFsm_stopTreeDomains(domain, sysName);
    delay(0,200);
    return;
}
// -------------------------------------------------------------

/* ------ Not more used ! --------------------------------------
void dcsUi_fwFsmRestartDomainDevices(string domain, string sysName)
{
    fwFsm_restartDomainDevices(domain, sysName);
    delay(0,200);
    return;
}
-------------------------------------------------------------- */

// *** NOT Public FSM Function call ***
// -----------  called in ui/fwFsmOperation.pnl ----------------
// used by : dcsFsmTools\dcsFsmControllHierarchyWidget.pnl
void dcsUi_FsmStopDomain(string sysName) {
  fwFsm_stopAllDomains(sysName);
  delay(0,200);
  return;
}
void dcsUi_FsmRestartDomain(string sysName) {
  fwFsm_restartAllDomains(sysName);
  delay(0,200);
  return;
}
// -------------------------------------------------------------

// ============================ EOF ===========================
