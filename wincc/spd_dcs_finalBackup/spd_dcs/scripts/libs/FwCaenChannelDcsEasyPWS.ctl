#uses "dcsCaen/dcsCaen.ctl"
 
FwCaenChannelDcsEasyPWS_initialize(string domain, string device)
{
 //    DebugN(">>-FwCaenChannelDcsEasyPWS_initialize->");   
}

FwCaenChannelDcsEasyPWS_valueChanged( string domain, string device,
      int actual_dot_status,
      bool actual_dot_remIlk,
      bool actual_dot_intFail, string &fwState )
{
    // get the invalid bit
    bool bInvalid = false;
    dpGet(device+".actual.status:_original.._aut_inv",bInvalid);
    if(bInvalid) {
      fwState = "NO_CONTROL";
    } else if (actual_dot_remIlk) 	{ 
	fwState = "INTERLOCK"; 
    } else if (actual_dot_intFail) { 
        fwState = "PWS_FAULT"; 
    } else  if (actual_dot_status & CAEN_TRIPMASK ) {      
	fwState = "WA_REPAIR";      
    } else if (actual_dot_status & CAEN_ERRORMASK ) {      
	fwState = "ER_REPAIR";      
    } else if (actual_dot_status & CAEN_NOCONTROLMASK ) {       
	fwState = "NO_CONTROL";       
    } else if (!(actual_dot_status ^ CAEN_OFFMASK))	{      
  	fwState = "OFF";      
    } else if (!(actual_dot_status ^ CAEN_ONMASK)) 	{ 
       fwState = "ON";   
    } else {      
//     DebugN(">>-FwCaenChannelDcsEasyPWS_valueChanged->UNDEFINED STATE ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);   
       fwState = "UNDEFINED";      
    }     
} 
 



FwCaenChannelDcsEasyPWS_doCommand(string domain, string device, string command)
{
	if (command == "SWITCH_ON")
	{
		dpSet(device+".settings.onOff",true);
	}
	if (command == "RESET")
	{
		dpSetWait(device+".settings.onOff",true);
		dpSetWait(device+".settings.onOff",false);
	}
	if (command == "SWITCH_OFF")
	{
		dpSet(device+".settings.onOff",false);
	}
	if (command == "KILL")
	{
		dpSet(device+".settings.onOff",false);
	}
}


