#uses "FED_CNF.ctl"
#uses "SECT_VIEW.ctl"
#uses "spdDefaultConfig.ctl"
#uses "spdOffDetElectronics.ctl"
#uses "spdFanInFanOut.ctl"
#uses "spdPIXDAC.ctl"


int spdRunCU_refreshHsList()
{  
dyn_bit32 HSGet;
int error=0;

	for(int i=0; i<120 ; i++) HSGet[i+1] = GetHSStatus(i);
	
	SetFedDP(120, "CNF_CHSTATUS_ALL", 0, HSGet);
        error = spdFed_WaitForStatus(120, "CNF_CHSTATUS_ALL");     
        
        return error;
}

int spdRunCU_resetDPI () //new version: loop done at the FED level
{

  int errorReturn = 0;
  string Command = "HSCNF_DPI_INT_DEF_ALL";
  SetFedDP( 0, Command,0,makeDynInt(0));
  errorReturn = spdFed_WaitForStatus(0, Command);

  SetFedDP( 60, Command,0,makeDynInt(0));
  errorReturn += spdFed_WaitForStatus(60, Command);


  return errorReturn;     
}        

int spdRunCU_dataReset () //new version: loop done at the FED level
{

 int errorReturn = 0;
 string Command = "HSCNF_DATA_RESET_ALL";
 SetFedDP( 0, Command,0,makeDynInt(0));
 errorReturn = spdFed_WaitForStatus(0, Command);

 SetFedDP(60, Command,0,makeDynInt(0));
 errorReturn += spdFed_WaitForStatus(60, Command);

 return errorReturn;     
}        


int spdRunCU_resetDPM (){
  
        int error =0;
        int errorReturn=0;
        int hsCh;
        string Command = "W_RT_FLUSH_DPM";
        
        // will loop through al lrouters
        for(int router=0; router < 20; router++){
                  
          hsCh = 6*router;
          // will reset the dpm for all
          SetFedDP( hsCh, Command,0,makeDynInt(0));
          error += spdFed_WaitForStatus(hsCh, Command);
          if(error) {
              DebugTN("Error number: " + error + " returned by FED with Command:" + Command + " in channel: " + hsCh); 
              errorReturn++;
          }     
        } 
          
        return errorReturn;     
}  


int spdRunCU_HardwareSynch()
{

  int error;
  string result, Status;
  bool Value1;
  bit32 DataOut;
  dyn_bit32 HSGet;

  dyn_int VectResults;

  //for router sincronization
  for (int routCh=0; routCh<=19; routCh++){   
    dyn_bool bitValue;  
    dyn_string RegisterName;  
    for (int ch=0; ch<=5; ch++){
      int  Hs=(routCh*6+ch);
      Status=IsHalfStaveOn(Hs);
      if (Status== "ON") Value1=false;
      else if (Status=="OFF")Value1=true;
      dynAppend(bitValue, Value1);
      dynAppend(RegisterName,"ChannelPresent"+ch);
    }
    spdRunCU_SetBitsControlRegToDP(routCh, RegisterName, bitValue); 
  }
  
  //for ch sincronization
  for (int Channel=0; Channel<=119; Channel++){
    result=IsHalfStaveOn(Channel);
    if (result=="ON"){
      Value1=1;
      SetHSStatus(Channel, "ON", 0 );
    }else {
      Value1=0;
      SetHSStatus(Channel, "OFF", 0 );
    }
    VectResults[Channel+1]=Value1;
    HSGet[Channel+1] = GetHSStatus(Channel);
  }
         
  SetFedDP(120, "CNF_CHSTATUS_ALL", 0, HSGet);
  return 0;
}




int spdRunCU_busyBoxReset()
{
  int error=0; 
  int controlReg = 0;
  
  error += resetFanInFanOut("A");
  error += setFanInFanOutRegister("A", "CONTROL" , controlReg);
  
  error += resetFanInFanOut("C");
  error += setFanInFanOutRegister("C", "CONTROL" , controlReg);
  
  return error;
   
} 




int spdRunCU_LrxFastorL1Delay()
{
  int error=0;
  dyn_int VectResults;
  int Value;

  string dp, dpsource;
  int fastorDelay;
  string command;
 
  if(patternMatch("PHYSICS", run_type)==1) dpsource = "PHYSICS";
  else if(patternMatch("STANDALONE*", run_type)==1) dpsource = "STANDALONE";
  else if (patternMatch("*", run_type)==1) return error = 1;

  for (int Channel=0; Channel<=119; Channel++){
        sprintf( dp, "spd_dcs:linkRxChannel%03d.settings.lrxFastorL1Delay."+dpsource, Channel);
        dpGet( dp, fastorDelay);
        SetFedDP(Channel, "W_LRX_FASTOR_L1_DELAY", 0, fastorDelay);
        error = spdFed_WaitForStatus(Channel, command);
        if(error) {
          DebugTN("Error number: " + error + " returned by FED with Command:W_LRX_FASTOR_L1_DELAY in channel: " + Channel); 
          return error;
        }    
  }
  return error;
} 

int spdRunCU_hsStatusConfig(string run_type)
{  
dyn_bit32 HSGet;
int error=0;
bit32 actualStatus, OnSatus ,TestSatus;
string Selection;

if(patternMatch("PHYSICS", run_type)==1) Selection = "ON";
else if(patternMatch("TECHNICAL*", run_type)==1) Selection = "ON";
else if(patternMatch("STANDALONE*", run_type)==1) Selection = "ON";
else if(patternMatch("DAQ_*", run_type)==1) Selection = "TEST";
else if (patternMatch("*", run_type)==1) return error = 1;

	for(int i=0; i<120 ; i++) {
           actualStatus = GetHSStatus(i);
           if (actualStatus == 0x3FF) HSGet[i+1] = actualStatus; 
           else HSGet[i+1] = SetHSStatus(i, Selection, FALSE);
	}  
        
	SetFedDP(120, "CNF_CHSTATUS_ALL", 0, HSGet);
        error = spdFed_WaitForStatus(120, "CNF_CHSTATUS_ALL");     
        
        return error;
}

int spdRunCU_hsDelaySetting(string run_type)
{
 
  int errorReturn = 0;
  int error = 0; 
  int miscControl, delayControl;  
  int delayDacPos = 42;   
  int miscDacPos = 43;
  dyn_string statusChannels;
  dyn_string errorList;
  string selectedStatus;
  
  DebugTN("run_type ", run_type);
  
  if(patternMatch("PHYSICS", run_type)==1){ 
    dpGet( "spd_dcs:physicSettings.miscControl", miscControl);
    dpGet( "spd_dcs:physicSettings.delayControl", delayControl);
    selectedStatus = "on";
    DebugTN("Delay setting ", selectedStatus, delayControl,miscControl);
  }
  else if(patternMatch("TECHNICAL*", run_type)==1){ 
    dpGet( "spd_dcs:physicSettings.miscControl", miscControl);
    dpGet( "spd_dcs:physicSettings.delayControl", delayControl);
    selectedStatus = "on";
    DebugTN("Delay setting ", selectedStatus, delayControl,miscControl);
  }  
  else if(patternMatch("STANDALONE*", run_type)==1){ 
    dpGet( "spd_dcs:standaloneSettings.miscControl", miscControl);
    dpGet( "spd_dcs:standaloneSettings.delayControl", delayControl);
    selectedStatus = "on";
    DebugTN("Delay setting ", selectedStatus, delayControl,miscControl);
  }  
  else if(patternMatch("DAQ_*", run_type)==1){ 
    dpGet( "spd_dcs:calibrationSettings.miscControl", miscControl);
    dpGet( "spd_dcs:calibrationSettings.delayControl", delayControl);
    selectedStatus = "test";    
    DebugTN("Delay setting ", selectedStatus, delayControl,miscControl);
  }
  else if (patternMatch("*", run_type)==1){

    return errorReturn = 1;
  }
  
  statusChannels = spdGetStatusChannels();
      
  for (int hs = 0 ; hs < dynlen (statusChannels); hs ++ ){
        
    if ( statusChannels[hs+1] == selectedStatus) {
      spdPixDacs_SetDacsToDP(hs,10 ,delayControl, "settings", delayDacPos);
      error = spdPixDacs_Write( hs, 10, delayDacPos);
      if (error ) dynAppend (errorList, "ERROR: Could not set delayControl in HS " + hs);
      spdPixDacs_SetDacsToDP(hs,10 ,miscControl, "settings", miscDacPos);
      error = spdPixDacs_Write( hs, 10, miscDacPos);
      if (error ) dynAppend (errorList, "ERROR: Could not set miscControl in HS " + hs);      
    }
  }
  
  DebugTN("error list: ", errorList);
  if(errorList == "") return errorReturn = 0;
  else return errorReturn = 1;
}        

int spdRunCU_routerHeaderSet(string run_type)
{
  int errorReturn =0;
  string error;
  bool routerHeaderState;
  bool isHsOnlyON = false;
   
  
  if(patternMatch("PHYSICS", run_type)==1) routerHeaderState = FALSE;
  else if(patternMatch("TECHNICAL*", run_type)==1) routerHeaderState = FALSE;  
  else if(patternMatch("STANDALONE*", run_type)==1) routerHeaderState = FALSE;  
  else if(patternMatch("DAQ_*", run_type)==1) routerHeaderState = TRUE;
  else if (patternMatch("*", run_type)==1) return errorReturn = 1;  
  
  for(int routCh = 0; routCh < 20; routCh++){ 
    if(isRouterReady(routCh, isHsOnlyON)){
      //error = SetBitInRouterControlRegToDP(routCh, "Enable router header", routerHeaderState);
      error = spdRunCU_SetBitInRouterControlRegToDP(routCh, "Enable router header", routerHeaderState);
    }  
  }
  
  if(error == "") return errorReturn = 0;
  else return errorReturn = 1;
}

int spdRunCU_startOfRun(int run_number)
{
   int errorReturn=0;
   string Command = "CNF_RUN_START";
   SetFedDP(120, Command,0,run_number);
   errorReturn = spdFed_WaitForStatus(120, Command);     
   //DebugTN("start of run with run number", run_number);
   return errorReturn;     
}
    
int spdRunCU_endOfRun(string run_type)
{
   int errorReturn=0;
   string Command = "CNF_RUN_STOP";
   SetFedDP(120, Command,0,makeDynInt(0));
   errorReturn = spdFed_WaitForStatus(120, Command);           
   if((patternMatch("PHYSICS", run_type)==1) || (patternMatch("STANDALONE*", run_type)==1)){
     dpSet( "spd_dcs:spdRunParameters.isCustomVersionUsed",false);
   }
   return errorReturn;  
}


// will configure all active routers
int spdRunCU_autoConfigRouter_all(bit32 options,string run_type)
{
  int error=0;
  bool isActive;
  
  
  for(int routCh = 0; routCh < 20; routCh++){
    
    isActive = isRouterReady(routCh);
    if(isActive){ 

      
      error = spdRunCU_autoConfigRouter( routCh, options, run_type);
      
      if(error){ 
       DebugTN("Error configuring router " + routCh );
       return error;
      }  
    } 
    else DebugTN("Router number " + routCh + " is not active for settings");  
      
    
  }
  return 0;
}

int spdRunCU_autoConfigRouter(int router, int options, string run_type){
  int errorReturn;
  unsigned ctrlReg; 
  unsigned l0l1Time;
  dyn_int lrxFoDelays;
  
  // gets the control reg from the datapoint 
  ctrlReg = getRoutRegisterFromDp(router, "rtControlReg", "settings");
  // gets the L0L1 time from the datapoint
  dpGet(L0L1_TIME_DP, l0l1Time);
  
  string dp;
  unsigned fastorDelay;
  int channel ;
  string rightDelay;
  DebugTN("run type ", run_type);
  if(patternMatch("PHYSICS", run_type)==1) rightDelay = "PHYSICS";
  else if(patternMatch("TECHNICAL*", run_type)==1) rightDelay = "PHYSICS";
  else if(patternMatch("STANDALONE*", run_type)==1) rightDelay = "STANDALONE";  
  else if(patternMatch("DAQ_*", run_type)==1) rightDelay = "STANDALONE";
  else if (patternMatch("*", run_type)==1) return errorReturn = 1;  

  
  // gets the lrx fastor delays
  for (unsigned hs = 0 ; hs < 6 ;hs ++){
    channel = 6*router + hs;
    sprintf( dp, "spd_dcs:linkRxChannel%03d.settings.lrxFastorL1Delay."+rightDelay, channel);
    dpGet( dp, fastorDelay);  
    
    dynAppend(lrxFoDelays, fastorDelay);
  }
  
  // will send the command to the router
  string Command = "CNF_AUTO_CONF_ROUTER";
  
  dyn_int settings = makeDynInt(options, ctrlReg, l0l1Time );
  
  dynAppend( settings ,lrxFoDelays);
  
  SetFedDP(6*router, Command,0, settings);
  
  errorReturn = spdFed_WaitForStatus(6*router, Command);
  return errorReturn;
  
}

 //-- Function that sets the value of a controlReg bit for a chosen router;

string spdRunCU_SetBitInRouterControlRegToDP(int routCh, string RegisterName, bool bitValue){
  dyn_int ControlReg; 
  unsigned regValues; 
  string error = ""; 
  unsigned bitNumber;
  

  regValues = getRoutRegisterFromDp(routCh, "rtControlReg", "settings");
  ControlReg = genFuncIntToBoolVect(regValues, 32);
  bitNumber=GetBitPositionInControlReg(RegisterName);
  //if (bitValue==true) ControlReg[bitNumber]=0;
  //else ControlReg[bitNumber]=1;
  ControlReg[bitNumber]= (int) bitValue;
  unsigned valueToSet = DynBoolToUint(ControlReg);
  setRouterRegisterToDp(routCh, "rtControlReg" ,"settings", valueToSet);

//    DebugTN("ControlReg..."+ControlReg);
   return "";
 
}


string spdRunCU_SetBitsControlRegToDP(int routCh, dyn_string RegisterName, dyn_bool bitValue)
{
  dyn_int ControlReg; 
  unsigned regValues; 
  string error = ""; 
  unsigned bitNumber;
  //error = readRouterRegister(routCh);
  //if (error == ""){ 
  
  regValues = getRoutRegisterFromDp(routCh, "rtControlReg", "settings");
  ControlReg = genFuncIntToBoolVect(regValues, 32);
    
  for(int i=0; i<=5; i++)
  {
    bitNumber=GetBitPositionInControlReg(RegisterName[i+1]);
    ControlReg[bitNumber]= bitValue[i+1];
   }
  unsigned valueToSet = DynBoolToUint(ControlReg);
  setRouterRegisterToDp(routCh, "rtControlReg" ,"settings", valueToSet);
//  error = setRouterRegister(routCh);
   
    //if (error != "") return "failed to set controlReg";
    //else return "";
 //}
 // else return "1";
}    

//The function returns the version loaded in the FED memory.
//in case of problems it returns -1
int spdRunCU_SPDVersionNumber()
{
   
  int error;
  int fedA = 0;
  dyn_int dataReturn;
  
  string Command = "HSCNF_DB_GET_GLOBAL_VERSION";
  SetFedDP(fedA , Command,0,0);
  error =spdFed_WaitForStatus(fedA , Command);
  if(!error) {
    GetFedDP(fedA,Command,dataReturn);
    return dataReturn[1];
  }
  else return -1;
  
}

//The function checks if any changes are commeted respect to the version loaded from the DB
//it returs true in case changes are detected otherwise it returns false
bool spdRunCU_isVersionUpdated()
{

  dyn_int diffSideA,diffSideC;
  int error;
  
  // gets the differences from side A
  int channel = 0;
  string Command = "HSCNF_DB_DIFF_COUNT";
  SetFedDP(channel , Command,0,0);
  error =spdFed_WaitForStatus(channel , Command);
  GetFedDP(channel,Command,diffSideA);
  
  /// gets the differences from side C
  channel = 60;
  SetFedDP(channel , Command,0,0);
  error =spdFed_WaitForStatus(channel , Command);
  GetFedDP(channel,Command,diffSideC);

  int totalA = diffSideA[1]+diffSideA[2]+diffSideA[3];
  int totalC = diffSideC[1]+diffSideC[2]+diffSideC[3];
  int isVersionChanged = totalA+totalC;
  if(isVersionChanged){
    DebugTN("Diffs side A:"+diffSideA[1]+":"+diffSideA[2]+":"+diffSideA[3]);
    DebugTN("Diffs side C:"+diffSideC[1]+":"+diffSideC[2]+":"+diffSideC[3]);
  }
  if(isVersionChanged) return true;
  else return false;
  
}


//This function checks if the required version is the same as the installed one.
//The function returns 0 if the versions are the same and no changes are done respect to the DB version
//it returns 1 if the SPD needs to be configured
//it returns -1 in case of errors
int spdRunCU_checkSPDVersion()
{

  dyn_string info;
  int spdVersionRequired = spdRunCU_requiredVersion(info);
  if(spdVersionRequired == -1) return -1;

  //what is loaded in the SPD
  int spdVersion = spdRunCU_SPDVersionNumber();
  if(spdVersion == -1) return -2;
  bool isVersionChanged = spdRunCU_isVersionUpdated();
  DebugTN("Current SPD version = "+spdVersion);

  if( (spdVersionRequired == spdVersion) && (!isVersionChanged) ) return 0;
  else return 1;

}

int spdRunCU_requiredVersion(dyn_string &exception)
{
  //version required 
  int  spdVersionRequired, status;
  string description, versionRequired;
  dyn_string configParam;  
  
  dpGet( "dcs_rct:aliDcsRctSpd.requested_conf.config_params", configParam);
  if(dynlen(configParam) < 2) return -1;
  status = getVersionFromFeroCC("SPD", "SPD",configParam[2],"",versionRequired,description,exception);
  spdVersionRequired = (int) versionRequired; 
  
  if(status != 0) return -1;
  else return spdVersionRequired;
  
}
//This function store in the dp the run Parameters: run number, version number, 
//noisy mask version numebr.
int spdRunCU_storeRunParameters(int run_number, string run_type)
{
  int spdVersion = spdRunCU_SPDVersionNumber();
  bool isVersionChanged = spdRunCU_isVersionUpdated();  
  int spdNoisyMaskVersion = spdRunCU_noisyMaskVersion();
  DebugTN("SPD version = "+spdVersion);
  //DebugTN("for debug ", run_number,run_type, spdVersion, isVersionChanged);
  dpSet( "spd_dcs:spdRunParameters.runNumber",run_number);
  dpSet( "spd_dcs:spdRunParameters.runType",run_type);
  dpSet( "spd_dcs:spdRunParameters.versionNumber",spdVersion);
  dpSet( "spd_dcs:spdRunParameters.noisyVersionNumber",spdNoisyMaskVersion);  
  if((patternMatch("PHYSICS", run_type)==1) || (patternMatch("STANDALONE*", run_type)==1)){
    dpSet( "spd_dcs:spdRunParameters.isCustomVersionUsed",isVersionChanged);
  }
  
}

int spdRunCU_LrxDelaySetting()
{
  int error=0;
  dyn_int VectResults;
  int Value;

  string clkDp, serialDp;
  int clkDelay, serialDelay;
  string clkCommand = "W_LRX_CLK_DELAY";
  string serialCommand = "W_LRX_DATA_DELAY";

  for (int Channel=0; Channel<=119; Channel++){
        sprintf( clkDp, "spd_dcs:linkRxChannel%03d.default.lrxClkDelay", Channel);
        dpGet( clkDp, clkDelay);
        sprintf( serialDp, "spd_dcs:linkRxChannel%03d.default.lrxDataDelay", Channel);
        dpGet( serialDp, serialDelay);        
        SetFedDP(Channel, clkCommand, 0, clkDelay);
        error = spdFed_WaitForStatus(Channel, clkCommand);
        if(error) {
          DebugTN("Error number: " + error + " returned by FED in channel: " + Channel); 
          return error;
        } 
        SetFedDP(Channel, serialCommand, 0, serialDelay);
        error = spdFed_WaitForStatus(Channel, serialCommand);
        if(error) {
          DebugTN("Error number: " + error + " returned by FED in channel: " + Channel); 
          return error;
        }            
  }
  return error;
} 

int spdRunCU_LrxForStrobeSetting () //new version: loop done at the FED level
{

  int forValue;
  int errorReturn = 0;
  string Command = "HSCNF_FO_STROBE_LENGTH_ALL";
  dpGet( "spd_dcs:spdLrxFoStrobeValue.setting", forValue);
  
  SetFedDP( 0, Command,0, forValue);
  errorReturn = spdFed_WaitForStatus(0, Command);

  SetFedDP( 60, Command,0, forValue);
  errorReturn += spdFed_WaitForStatus(60, Command);

  return errorReturn;     
}   


// int spdRunCU_LrxForStrobeSetting()
// {
//   int error=0;
//   dyn_int VectResults;
//   int Value;
// 
//   int forValue;
//   string command = "W_LRX_FO_STROBE_LENGTH";
// 
//   dpGet( "spd_dcs:spdLrxFoStrobeValue.setting", forValue);
// 
//   for (int Channel=0; Channel<=119; Channel++){
//     SetFedDP(Channel, command, 0, forValue);
//     error = spdFed_WaitForStatus(Channel, command);
//     if(error) {
//       DebugTN("Error number: " + error + " returned by FED in channel: " + Channel); 
//       return error;
//     } 
//   }
//   return error;
// } 

int spdRunCU_routerTimeoutReadyEvent () //loop done at the FED level
{

  int errorReturn = 0;
  int timeoutValue;
  string Command = "HSCNF_TIMEOUT_READY_EV_ALL";
  dpGet( "spd_dcs:routerTimeoutReadyEvent.setting", timeoutValue);
  
  SetFedDP( 0, Command,0, timeoutValue);
  errorReturn = spdFed_WaitForStatus(0, Command);

  SetFedDP( 60, Command,0, timeoutValue);
  errorReturn += spdFed_WaitForStatus(60, Command);

  return errorReturn;     
}   



int spdRunCU_noisyMaskVersion()
{
   
  int error;
  int fedA = 0;
  dyn_int dataReturn;
  
  string Command = "HSCNF_DB_GET_MASK_VERSION";
  SetFedDP(fedA , Command,0,0);
  error =spdFed_WaitForStatus(fedA , Command);
  if(!error) {
    GetFedDP(fedA,Command,dataReturn);
    return dataReturn[1];
  }
  else return -1;
  
}

int spdRunCU_maskNoisyPixels()
{
  string command = "HSCNF_DB_MASK";
  int version = -1;
  int error =0;
  int sideA=0;
  int sideC=60;
  int waitForReply = 1000;
  
  // will send the command to fed server A and C at the same time
  SetFedDP( sideA,command,0,version);
  SetFedDP( sideC,command,0,version);
  
  // waits till both feds have finished
  error = spdFed_WaitForStatus(sideA,command, waitForReply);
  error +=spdFed_WaitForStatus(sideC,command, waitForReply);
  
  return error;
  
}


// the list of channels received are set in Test, while all the others are in off
spdRunCU_channelsToAlign(dyn_int channel)
{
  
 dyn_bit32 HSSet;
 dyn_bit32 HSGet;
 int Value1;
 string hwStatus, result;
 dyn_int VectResults;
 int numberOfCh = dynlen(channel);
 
  for (int Channel=0; Channel<=119; Channel++){
    result=IsHalfStaveOn(Channel);
    if (result=="ON"){
      Value1=1;
      SetHSStatus(Channel, "ON", 0 );
    }else {
      Value1=0;
      SetHSStatus(Channel, "OFF", 0 );
    }
    VectResults[Channel+1]=Value1;
    HSGet[Channel+1] = GetHSStatus(Channel);
  }
 	for(int i=0; i<numberOfCh ; i++) {
    int chToSet = channel[i+1];
    hwStatus = IsHalfStaveOn(chToSet);

    if(hwStatus == "ON") 
      SetHSStatus(chToSet, "TEST" , 0);
      HSGet[chToSet+1] = GetHSStatus(chToSet);
      //DebugTN("HSSet[chToSet+1]", HSSet[chToSet+1]);
	}
  SetFedDP(120, "CNF_CHSTATUS_ALL", 0, HSGet);
 
  
}

//function to correct MEB alignement. 
// l1L2Select to select if the function have to use (1) L1 or (0) L2Reject
// correctAlign to select if the function have to (1) correct or (0) no correct the alignement when discovered
// return the list of (-1) not check, (0) aligned or (shiftNumber) disalignement chip in the format [hsNumb][chipNumb][value]
dyn_int spdRunCU_mebBufferAlign( int correctAlign, int l1L2Select)
{
  
  int error, chSideA, chSideC;
  dyn_int alignResult, resultToAdd;
  string Command = "SCN_MEB_START";
  dyn_int scanConfig = makeDynInt(correctAlign, l1L2Select);
  
  SetFedDP(0,Command , 0, scanConfig);
  error =spdFed_WaitForStatus(0, Command, 10000);
  if(error) return -100;
  else {      
    int error = GetFedDP(chSideA, Command, alignResult);
    //error += GetFedDP(chSideC, Command, resultToAdd);
    //dynAppend(alignResult, resultToAdd);
    //if(error) return -200;
    //else return alignResult;
    return alignResult;
  }
}

int spdRunCU_dataFormatOff(int channel)
{
  int error =0;
  //int channel=48;
  string command="HSCNF_DPI_SET";
  dyn_uint data = makeDynUInt(1,1,1023,0,2,1,0,0,1,0);

  SetFedDP( channel,command,0,data);
  error = spdFed_WaitForStatus(channel,command);
  
  return error;
  
}

int spdRunCU_dataFormatOn(int channel)
{
  int error =0;
  //int channel=48;
  string command="HSCNF_DPI_SET";
  dyn_uint data = makeDynUInt(1,1,1023,0,2,1,0,0,1,1);

  SetFedDP( channel,command,0,data);
  error = spdFed_WaitForStatus(channel,command);
  
  return error;
  
}

int spdRunCU_adjustL0L1delay(){

  int error=0;
  int channel;
  string command = "CNF_RT_WRITE_REGISTER";
  int L0L1m_register = 120;
  int L0L2m_register = 121;
  int L0L1m_value;// = 4259+20; //hardcoded specific values
  int L0L2m_value;// = 112259+20;
  dpGet(L0L1m_TIME_DP,L0L1m_value);
  dpGet(L0L2m_TIME_DP,L0L2m_value);
  
  for(int iRouter=0; iRouter<20; iRouter++){

    channel = iRouter*6;
    
    dyn_int data1 = makeDynInt(channel,L0L1m_register,0,L0L1m_value);
    SetFedDP(channel,command,0,data1);
    error = spdFed_WaitForStatus(channel,command);
    
    dyn_int data2 = makeDynInt(channel,L0L2m_register,0,L0L2m_value);
    SetFedDP(channel,command,0,data2);
    error = spdFed_WaitForStatus(channel,command);
  }
  return error;
}

