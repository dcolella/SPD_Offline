FwCaVPlant_initialize(string domain, string device)
{
}


FwCaVPlant_valueChanged( string domain, string device,
      int Actual_dot_status,
      bool Actual_dot_fault,
      bool Actual_dot_alarm, string &fwState )
{ 
    int ModbusDriver = 15;
    dyn_uint dui;
    int monitorOnMaintenance = 0;
    dpGet("_Connections.Driver.ManNums:_online.._value", dui);  

    if(dpExists("_aliDcsCavFsm.settings.monitorOnMaintenance"))
        dpGet("_aliDcsCavFsm.settings.monitorOnMaintenance",monitorOnMaintenance );
        

      if (Actual_dot_alarm) 
         fwState = "ERROR"; 
      else if( !isDriverRunning(dui, ModbusDriver))
              fwState = "NO_CONTROL";  
      else if(monitorOnMaintenance==0 && Actual_dot_status & 256) 
	   fwState = "MAINTENANCE"; 
//      else if(Actual_dot_fault) 
//	   fwState = "WARNING"; 
//      else if(Actual_dot_status & 32) 
//	   fwState = "LEAK_SEARCH";
      else  
         switch( Actual_dot_status & 15) 
         { 
             case 1: 
   	         fwState = "OFF"; 
               break; 
             case 2: 
   	         fwState = "STANDBY"; 
               break; 
             case 4: 
   	         fwState = "RUN"; 
               break; 
             case 8: 
   	         fwState = "RECOVERING"; 
               break; 
             default: 
   	         fwState = "ERROR"; 
               break; 
         } 
}



FwCaVPlant_doCommand(string domain, string device, string command)
{
  string timeout_state;
  int timeout_seconds;
  int current_command;
		anytype valueOf_loopList;
                int i_deviceNumber;
                dyn_int di_deviceNumbers;
                int i;
                int dev_control;
                bool deviceInList;
                  
  fwDU_getState(domain, device, timeout_state);
  
	if (command == "GO_OFF") 
	{ 
		dpSet(device+".Settings.control",1); 
	} 
	if (command == "GO_STANDBY")
	{
		dpSet(device+".Settings.control",2);
	}
	if (command == "GO_RUN") 
	{ 
		dpSet(device+".Settings.control",4); 
	} 
	if (command == "RECOVER")  
	{  
		dpSet(device+".Settings.control",8);  
	}         
	if (command == "ALLOW_MAINTENANCE")
	{
		dpGet(device+".Settings.control",current_command);
		dpSet(device+".Settings.control",current_command+256);
	}
	if (command == "EXIT_MAINTENANCE")  
	{  
		dpGet(device+".Actual.status",current_command);  
                if(current_command>256)
  		  dpSet(device+".Settings.control",current_command-256);
                else
  		  dpSet(device+".Settings.control",1);
	}  
	if (command == "LOCK_LOOP" || command == "CLOSE_LOOP")
	{

                
                //extract loop numbers from the list and compare with device loop number
		fwDU_getCommandParameter(domain, device, "loopList", valueOf_loopList);
            //DebugTN("LOOP LIST: " + valueOf_loopList);
            //DebugN("device: " + device);
            //DebugN("i_deviceNumber: " + i_deviceNumber);
            //DebugN("strlen(valueOf_loopList): " +strlen(valueOf_loopList));
                deviceInList = false;
                if(strtolower(valueOf_loopList)=="all")     
                {            
                    switch(command)
                    {
                       case "LOCK_LOOP":                       
		        dpGet(device+".Actual.status",current_command);  
         		 if(current_command != 2)
                           dpSet(device+".Settings.control",2);
                         else
                           fwDU_setState(domain, device, timeout_state);
                        break;
                        
                       case "CLOSE_LOOP":
		        dpGet(device+".Actual.status",current_command);  
         		 if(current_command != 2)
                           dpSet(device+".Settings.control",2);
                         else
                           fwDU_setState(domain, device, timeout_state);
                        break;
                    }
                }                         
                else //otherwise, don't do anything
                {
         		//dpGet(device+".Settings.control",dev_control);
        		//dpSet(device+".Settings.control",dev_control);
                        fwDU_setState(domain, device, timeout_state);
                }
	}
          
   dpGet(device+".Actual.timeout",timeout_seconds);       
   if (timeout_seconds<1) timeout_seconds=59;  
   //DebugTN(device + ": Waiting for Timeout (" + timeout_seconds + "s) before updating the command.");

   fwDU_startTimeout(timeout_seconds+5, domain, device, timeout_state);// timeout_state);   
   delay(timeout_seconds+7);
   dpGet(device+".Actual.status",current_command);  
   dpSet(device+".Actual.status",current_command);  
          
}



