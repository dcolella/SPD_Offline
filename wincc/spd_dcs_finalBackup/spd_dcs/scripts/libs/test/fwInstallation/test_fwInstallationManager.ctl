
#uses "fwUnitTestComponentAsserts.ctl" 
#uses "fwInstallationManager.ctl" 


void test_fwInstallationManager_isManagerPropertiesValid() 
{
	//Default set of parameters that should be valid
	string defManager = "WCCOArdb";
	string defStartMode = "manual";
	int defSecKill = 3;
	int defRestartCount = 3;
	int defResetMin = 3;
	
	bool result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, defRestartCount, defResetMin);
	assertTrue(result,"Default set of properties is invalid and should be corrected!");
	
	if(!result)
	{
		return;
	}
	
	//Managers' names testing
	dyn_string validManagersNames = makeDynString("WCCILdata", "WCCILdatabg", "WCCILdist", "WCCILevent",
										"WCCILpmon", "WCCILproxy", "WCCILredu", "WCCILsim",
										"WCCILsplit", "WCCILtoolGetHW", "WCCILtoolLicense", "WCCOAarchiv",
										"WCCOAascii", "WCCOAbacnet", "WCCOAchmgr", "WCCOActrl",
										/*"WCCOAdl", "WCCOAdnp3", "WCCOAeip",*/ "WCCOAiec", "WCCOAmod",
										/*"WCCOAoledb", "WCCOAopc", "WCCOAopcAE", "WCCOAopcsrv", "WCCOAopcsrvAE",*/
										"WCCOAopcua", "WCCOAopcuasrv", "WCCOApid", "WCCOArdb",/* "WCCOArk512",*/
										"WCCOAs7", /*"WCCOAsinaut", */"WCCOAsnmp", "WCCOAsnmpa", "WCCOAssi",
										"WCCOAtlp", "WCCOAtls", "WCCOAtoolBuildIdList", "WCCOAtoolConvertDb", "WCCOAtoolCreateDb",
										"WCCOAtoolCryptCtrl", "WCCOAtoolLogViewer", "WCCOAtoolMedia", "WCCOAtoolNameToId",
										"WCCOAtoolParse", "WCCOAtoolRepairDb", "WCCOAtoolRevise", "WCCOAtoolSyncTypes",
										"WCCOAtoolSysNames", "WCCOAtoolTrans", "WCCOAtoolTunnel", "WCCOAui", "WCCOAvalarch",
										"WCCOAvideo", "WCCOAvrmprx", /*"WCCOACMWClient", "WCCOACMWServer", 
										"WCCOAdip", "WCCOALaser", "WCCOAsmi", "WCCOAsmi3.11"*/
										"../bin/WCCILredu" /*actually it's a valid name!!!*/);
										
	int validManagersNamesLength = dynlen(validManagersNames);
	for(int i=1;i<=validManagersNamesLength;i++)
	{
		string manager = validManagersNames[i];
		result = fwInstallationManager_isManagerPropertiesValid(manager, defStartMode, defSecKill, defRestartCount, defResetMin);
		assertTrue(result, "Manager: " + manager + " is invalid!");
	}
	
	//Start modes testing
	dyn_string validStartModes = makeDynString("manual", "once", "always");
	
	int validStartModesLength = dynlen(validStartModes);
	for(int i=1;i<=validStartModesLength;i++)
	{
		string startMode = validStartModes[i];
		result = fwInstallationManager_isManagerPropertiesValid(defManager, startMode, defSecKill, defRestartCount, defResetMin);
		assertTrue(result, "Start mode: " + startMode + " is invalid!");
	}
	
	//secKill testing
	for(int i=FWINSTALLATION_MANAGER_MIN_SEC_KILL;i<=FWINSTALLATION_MANAGER_MAX_SEC_KILL;i++)
	{
		int secKill = i;
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, secKill, defRestartCount, defResetMin);
		assertTrue(result, "secKill = " + secKill + " is invalid!");
	}
	
	//restartCount testing
	for(int i=FWINSTALLATION_MANAGER_MIN_RESTART_COUNT;i<=FWINSTALLATION_MANAGER_MAX_RESTART_COUNT;i++)
	{
		int restartCount = i;
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, restartCount, defResetMin);
		assertTrue(result, "restartCount = " + restartCount + " is invalid!");
	}
	
	//resetMin testing
	for(int i=FWINSTALLATION_MANAGER_MIN_RESET_MIN;i<=FWINSTALLATION_MANAGER_MAX_RESET_MIN;i++)
	{
		int resetMin = i;
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, defRestartCount, resetMin);
		assertTrue(result, "resetMin = " + resetMin + " is invalid!");
	}
	
}

void test_fwInstallationManager_isManagerPropertiesInValid() //Managers' names are not tested currently!
{
	const int TEST_COUNTER = 100; //amount of tests for int variables (divided by 2)
	
	//Default set of parameters that should be valid
	string defManager = "WCCOArdb";
	string defStartMode = "manual";
	int defSecKill = 3;
	int defRestartCount = 3;
	int defResetMin = 3;
	
	bool result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, defRestartCount, defResetMin);
	assertTrue(result,"Default set of properties is invalid and should be corrected!");
	
	if(!result)
	{
		return;
	}
	
	//Managers' names testing
	dyn_string invalidManagersNames = makeDynString("WCCILdata.exe", "", " ", "abc",
										"123445435345"/*, "../bin/WCCILredu"*/ /*actually it's a valid name*/, "WCCILsim1",
										"0WCCOAdip", "_WCCOALaser", "WCCOAsmi_", "WCCOAsmi3.11.",
										"*COAdip", "WCCOAdip*", "WCCOA*", "WCCOAd*.exe");
										
	int invalidManagersNamesLength = dynlen(invalidManagersNames);
	for(int i=1;i<=invalidManagersNamesLength;i++)
	{
		string manager = invalidManagersNames[i];
		result = fwInstallationManager_isManagerPropertiesValid(manager, defStartMode, defSecKill, defRestartCount, defResetMin);
		//assertFalse(result, "Manager: " + manager + " is valid!"); //Has to be commented out.
     		//Currently, if manager name is invalid, only warning is displayed but function does not return false
	}
	
	//Start modes testing
	dyn_string invalidStartModes = makeDynString("Manual", "oNce", "alwayS", "", " ", ".", "invalid");
	
	int invalidStartModesLength = dynlen(invalidStartModes);
	for(int i=1;i<=invalidStartModesLength;i++)
	{
		string startMode = invalidStartModes[i];
		result = fwInstallationManager_isManagerPropertiesValid(defManager, startMode, defSecKill, defRestartCount, defResetMin);
		assertFalse(result, "Start mode: " + startMode + " is invalid!");
	}
	
	
	
	for(int i=0;i<TEST_COUNTER;i++)
	{
		//secKill testing
		int secKill1 = FWINSTALLATION_MANAGER_MIN_SEC_KILL-i-1;
		int secKill2 = FWINSTALLATION_MANAGER_MAX_SEC_KILL+i+1;
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, secKill1, defRestartCount, defResetMin);
		assertFalse(result, "secKill = " + secKill1 + " is valid!");
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, secKill2, defRestartCount, defResetMin);
		assertFalse(result, "secKill = " + secKill2 + " is valid!");
		
		//restartCount testing
		int restartCount1 = FWINSTALLATION_MANAGER_MIN_RESTART_COUNT-i-1;
		int restartCount2 = FWINSTALLATION_MANAGER_MAX_RESTART_COUNT+i+1;
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, restartCount1, defResetMin);
		assertFalse(result, "restartCount = " + restartCount1 + " is valid!");
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, restartCount2, defResetMin);
		assertFalse(result, "restartCount = " + restartCount2 + " is valid!");
		
		//resetMin testing
		int resetMin1 = FWINSTALLATION_MANAGER_MIN_RESET_MIN-i-1;
		int resetMin2 = FWINSTALLATION_MANAGER_MAX_RESET_MIN+i+1;
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, defRestartCount, resetMin1);
		assertFalse(result, "resetMin = " + resetMin1 + " is valid!");
		result = fwInstallationManager_isManagerPropertiesValid(defManager, defStartMode, defSecKill, defRestartCount, resetMin2);
		assertFalse(result, "resetMin = " + resetMin2 + " is valid!");
	}
}

void test_fwInstallationManager_remove() 
{
	//first have to add manager that can be removed
	assertTrue((fwInstallationManager_add("WCCOActrl", "manual", 3, 3, 3, "specialForTest") == 1), "Cannot add test manager for removal!");
	//now try to remove it
	assertTrue((fwInstallationManager_remove("WCCOActrl", "specialForTest") == 0), "Manager removal failed (1)!");
	
	//once again, working manager
	assertTrue((fwInstallationManager_add("WCCOAui", "always", 3, 3, 3, "-m vision") == 1), "Cannot add test manager for removal!");
	bool isExpired;
	fwInstallationManager_wait(true, "WCCOAui", "-m vision", 30, isExpired);
	assertTrue((fwInstallationManager_remove("WCCOAui", "-m vision") == 0), "Manager removal failed (2)!");
	
	//now negative case - manager not exist
	assertFalse((fwInstallationManager_remove("WCCOActrl", "specialForTest") == 0), "Manager removal failed (3)!");
	
	//another negative case - manager is followed by other working managers in the list
	assertTrue((fwInstallationManager_add("WCCOAui", "always", 3, 3, 3, "-m vision") == 1), "Cannot add test manager 1 for removal!");
	assertTrue((fwInstallationManager_add("WCCOAui", "always", 3, 3, 3, "-m para") == 1), "Cannot add test manager 2 for removal!");
	//wait until second manager starts
	fwInstallationManager_restart("WCCOAui", "-m vision");
	fwInstallationManager_restart("WCCOAui", "-m para");
	fwInstallationManager_wait(true, "WCCOAui", "-m para", 30, isExpired);
	//this manager cannot be removed because following manager running
	assertFalse((fwInstallationManager_remove("WCCOAui", "-m vision") == 0), "Manager removed when it should stay (4)!");
	//let's delete last manager from the list and replace it with non-working one
	assertTrue((fwInstallationManager_remove("WCCOAui", "-m para") == 0), "Manager removal failed (5)!");
	//add it once again
	assertTrue((fwInstallationManager_add("WCCOAui", "once", 3, 3, 3, "-m para") == 1), "Cannot add test manager 1 for removal!");
	fwInstallationManager_restart("WCCOAui", "-m para");
	fwInstallationManager_wait(true, "WCCOAui", "-m para", 30, isExpired);
	//now all managers could be deleted
	assertTrue((fwInstallationManager_remove("WCCOAui", "-m para") == 0), "Manager removal failed (6)!");
	assertTrue((fwInstallationManager_remove("WCCOAui", "-m vision") == 0), "Manager removal failed (7)!");
	
	
}
