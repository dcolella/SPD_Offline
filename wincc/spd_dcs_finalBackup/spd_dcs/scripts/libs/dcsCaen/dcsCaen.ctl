// ===================================================================
/*      
        ALICE DCS - CAEN CHANNEL COMPONENT LIBRARY
        
        File : dcsCaen.ctl
        Usage : Library
        
        auth : Cesar Torcato de Matos 
        date : 01/03/2007
        
  
------------------------------------------------------------------ */  
/* 		History

  8/03/2007  -  Creation
        
==================================================================== */

// GLOBAL CONST DEFINITION
const string DCSCAEN_HV_INTER_CHANNEL = "dcsHVint"; // HV channel with Intermediate
const string DCSCAEN_HV_BASE_CHANNEL = "dcsHV"; // HV Channel witout intermediate
const string DCSCAEN_LV_BASE_CHANNEL = "dcsLV"; // LV Channel

const string DCSCAEN_LV_INTER_TYPE = "FwCaenChannelDcsHVI";
const string DCSCAEN_LV_BASE_TYPE = "FwCaenChannelDcsLV"; // HV Channel witout intermediate
const string DCSCAEN_HV_BASE_TYPE = "FwCaenChannelDcsHV"; // LV Channel

const string DCSCAEN_DEFAULT_DPE = ".settings"; // Default values 
const string DCSCAEN_DEFAULT_DPTYPE = "dcsCaenDefaultSettings";

                                             // datapoint with the global definitions for the detector   
const string DCSCAEN_GLOBAL_DP ="dcsCaenGlbl";
const string DCSCAEN_GLOBAL_DEVICES_DPE =".FsmCaenDevices";
const string DCSCAEN_GLOBAL_PREFIX_DPE =".DetectorPrefix";
const string DCSCAEN_GLOBAL_TOPNODE_DPE =".TopNode";
const string DCSCAEN_GLOBAL_USE_DEFAULT =".UseDefaultSettings";
const string DCSCAEN_GLOBAL_TYPESHV =".TypesHV";
const string DCSCAEN_GLOBAL_TYPESHVI =".TypesHVI";
const string DCSCAEN_GLOBAL_TYPESLV =".TypesLV";


const string DCSCAEN_GLOBAL_AUTO_SETTINGS_DPE =".UseAutoSettings";


// Index definition into the array used as carrier
const int DCSCAEN_OUT_STRUCTSIZE = 11;

const int DCSCAEN_OUT_NAME = 1;
const int DCSCAEN_OUT_V0 = 2;
const int DCSCAEN_OUT_I0 = 3;
const int DCSCAEN_OUT_TRIPT = 4;
const int DCSCAEN_OUT_ONOFF = 5;
const int DCSCAEN_OUT_VSLIM = 6;
const int DCSCAEN_OUT_RDWN = 7;
const int DCSCAEN_OUT_RUP = 8;
const int DCSCAEN_OUT_V1 = 9;
const int DCSCAEN_OUT_I1 = 10;
const int DCSCAEN_OUT_USERDEF = 11;

    
// GLOBAL VARIABLE DEFINITION
// List of settings for Caen Channel
const int DCSCAEN_SETTINGS_ITEMS = 10;
global dyn_string DCSCAEN_SETTINGS_DPE;
global dyn_int DCSCAEN_SETTINGS_DPE_TYPE;
global dyn_string DCSCAEN_SETTINGS_DPE_DESCRIPTION;



// INTERNAL FUNCTIONS DEFINITION
dyn_string __dcsCaen_getFormatByType(string Type) {
  switch(Type) {
    case DCSCAEN_HV_INTER_CHANNEL:
    case DCSCAEN_HV_BASE_CHANNEL:
      return( makeDynString("%6.1f","%6.3f","%6.1f","%s","%5d","%4d","%4d","%6.1f","%6.3f","%s") );
      break;
    case DCSCAEN_LV_BASE_CHANNEL:
      return( makeDynString("%6.3f","%6.2f","%6.1f","%s","%5.3f","%s","%s","%6.3f","%6.2f","%s") );
      break;
    default:
      return( makeDynString() );
      break;
  }
}

dyn_int __dcsCaen_getTypesByType(string Type) {
  switch(Type) {
    case DCSCAEN_HV_INTER_CHANNEL:
    case DCSCAEN_HV_BASE_CHANNEL:
      return(makeDynInt(DPEL_FLOAT,DPEL_FLOAT,DPEL_FLOAT,DPEL_BOOL,DPEL_FLOAT,
                        DPEL_INT,DPEL_INT,DPEL_FLOAT,DPEL_FLOAT,DPEL_STRING));
      break;
    case DCSCAEN_LV_BASE_CHANNEL:
      return(makeDynInt(DPEL_FLOAT,DPEL_FLOAT,DPEL_FLOAT,DPEL_BOOL,DPEL_FLOAT,
                        DPEL_INT,DPEL_INT,DPEL_FLOAT,DPEL_FLOAT,DPEL_STRING));
      break;
    default:
      return( makeDynString() );
      break;
  }
}

dyn_string  __dcsCaen_getDescriptionByType(string Type) {
  switch(Type) {
    case DCSCAEN_HV_INTER_CHANNEL:
    case DCSCAEN_HV_BASE_CHANNEL:
      return(makeDynString("V0 set","I0 set","TRIP TIME","ON OFF","S V LIMIT","RAMP DW",
                           "RAMP UP","V1 set","I1 set","USER DEF."));
      break;
    case DCSCAEN_LV_BASE_CHANNEL:
      return(makeDynString("V0 set","I0 set","TRIP TIME","ON OFF","S V LIMIT","RAMP DW",
                           "RAMP UP","V1 set","I1 set","USER DEF."));
      break;
    default:
      return( makeDynString() );
      break;
  }
}

dyn_string __dcsCaen_getDpeNameByType(string Type) {
  switch(Type) {
    case DCSCAEN_HV_INTER_CHANNEL:
    case DCSCAEN_HV_BASE_CHANNEL:
      return(makeDynString(".settings.v0",".settings.i0",".settings.tripTime",".settings.onOff",
                           ".settings.vMaxSoftValue",".settings.rDwn",".settings.rUp",
                           ".settings.v1",".settings.i1",".userDefined"));
      break;
    case DCSCAEN_LV_BASE_CHANNEL:
      return(makeDynString(".settings.v0",".settings.i0",".settings.tripTime",".settings.onOff",
                           ".settings.vMaxSoftValue",".settings.rDwn",".settings.rUp",
                           ".settings.v1",".settings.i1",".userDefined"));
      break;
    default:
      return( makeDynString() );
      break;
  }
}

__initVariables()
{
if(dynlen(DCSCAEN_SETTINGS_DPE) == 0)
{  
  
  DCSCAEN_SETTINGS_DPE = makeDynString(".settings.v0",".settings.i0",
                                                      ".settings.tripTime",".settings.onOff",
                                                      ".settings.vMaxSoftValue",".settings.rDwn",
                                                      ".settings.rUp",".settings.v1",
                                                      ".settings.i1",".userDefined");

  DCSCAEN_SETTINGS_DPE_TYPE = makeDynInt(DPEL_FLOAT,DPEL_FLOAT,DPEL_FLOAT,DPEL_BOOL,DPEL_FLOAT,
                                                     DPEL_INT,DPEL_INT,DPEL_FLOAT,DPEL_FLOAT,DPEL_STRING);

  DCSCAEN_SETTINGS_DPE_DESCRIPTION = makeDynString("V0 set","I0 set",
                                                                  "TRIP TIME","ON OFF",
                                                                  "S V LIMIT","RAMP DW",
                                                                  "RAMP UP","V1 set",
                                                                  "I1 set","USER DEF.");
}
  
  fwConfigurationDB_allowResolveRemote = TRUE;  
  
return;
}

// PUBLIC FUNCTIONS DEFINITION

// ------------------------------------------------------------
// this just returns the available types, dcsHV or dcsLV
dyn_string dcsCaen_getFSMCaenTypes()
{
  return makeDynString (DCSCAEN_HV_INTER_CHANNEL,
                        DCSCAEN_HV_BASE_CHANNEL,
                        DCSCAEN_LV_BASE_CHANNEL);
}
// ------------------------------------------------------------





/* -----------------------------------------------------

   name : dcsCaen_getDeviceListOfDomain()
   param input : Domain Name (string)
                 Device List (dyn_string)
                 FSM type  (string)
   param output : Logical Names list (dyn_string)

   Description : returns the existing type of devices that are under this controll unit
                 if there is another controll unit bellow, does not loop over that

              History
              
   12/4/2007  -  Creation                  
   
------------------------------------------------------- */

dcsCaen_getDeviceListOfDomain( string node, dyn_string &deviceList, string type){
  dyn_string dsChildren;
  dyn_int diTypes;
  string childType;
  
  dsChildren = fwCU_getChildren(diTypes, node);
    
  for (int child = 1 ; child <= dynlen(dsChildren) ; child ++ ){
    switch(diTypes[child]) //  flags: 1 = CU, 2 = DU, 0 = Obj
    {
       case 2:
        fwCU_getType(dsChildren[child], childType);      
        
        if(type == childType){          
            dynAppend(deviceList,dsChildren[child]);
         }
         break;
         
        case 0:
          dcsCaen_getDeviceListOfDomain( dsChildren[child], deviceList, type);
          break;
        break;
    }
  }
  
}


/* -----------------------------------------------------

   name : dscCaen_DomainsWithTypeList()
   param input : Top Node (string)
                 Domain List (dyn_string)
                 FSM types  (dyn_string)
   param output : Logical Names list (dyn_string)

   Description : returns the domains wich have at least one of a certain type of device in a list
                 does not loop over other domains(CU's)

              History
              
   12/4/2007  -  Creation                  
   
------------------------------------------------------- */

dcsCaen_getDomainsWithTypeList(string topNode,dyn_string &domainList, dyn_string types, bool searchSubDomains=false){
  

  dyn_string dsChildren;
  dyn_int diTypes;
  string childType;
  dyn_string deviceList;
  
  dsChildren = fwCU_getChildren(diTypes, topNode);
  //DebugTN(topNode, dsChildren, diTypes);
                                            // searches first in the top node
  for (int i = 1 ; i <= dynlen(types) ; i ++){
    dcsCaen_getDeviceListOfDomain( topNode, deviceList, types[i]);
    //DebugTN("topNode Search",topNode,types[i], deviceList);
  
    if (dynlen(deviceList)){
      //DebugTN("Top Node with devices you bastards!!!");
      dynAppend(domainList, topNode);  
      break;
     } 
  }
  
  if (searchSubDomains){
   DebugTN("searching Sub Domains");
   for (int child = 1 ; child <= dynlen(dsChildren) ; child ++ ){
     switch(diTypes[child]) //  flags: 1 = CU, 2 = DU, 0 = Obj
     {
        case 1:
          deviceList = makeDynString();
          for (int i = 1 ; i <= dynlen(types) ; i ++){
            dcsCaen_getDeviceListOfDomain( dsChildren[child], deviceList, types[i]);
            //DebugTN(dsChildren[child], deviceList);
          
            if (dynlen(deviceList)){
              dynAppend(domainList, dsChildren[child]);
              break;
            }
          }
          
          dcsCaen_getDomainsWithTypeList( dsChildren[child], domainList, types);
          break;
          
        case 2:
          break;
         
         case 0:
           break;
         break;
     }
   }
 }
}



// ------------------------------------------------------------
// returns the list of settings from the caen in the same order 
// as in the function as dcsCaen_FSMGetCaenDataFromRecipe
dyn_string dcsCaen_getCaenRecipeSettings(string Type, dyn_string &dsDescription, 
                                                      dyn_int &diType, 
                                                      dyn_string &dsFormat)
{
  
  dsDescription = __dcsCaen_getDescriptionByType(Type);
  diType = __dcsCaen_getTypesByType(Type);
  dsFormat = __dcsCaen_getFormatByType(Type);
  
  return (__dcsCaen_getDpeNameByType(Type));	
}
// ------------------------------------------------------------

// ------------------------------------------------------------
//  returns the List of Domains with the ConfDB device
dyn_string dcsCaen_getFSMDomains(string rootDomain = "")
{
  dyn_string dsList;
    
  __dcsCaen_analizetree(rootDomain, rootDomain, dsList);
  dynUnique(dsList);

  return(dsList);  
  
}

__dcsCaen_analizetree(string node,string domain, dyn_string &dsList)
{
  dyn_string dsChildren;
  dyn_int diTypes;
  int i,n;
  string type;
  
  dsChildren = fwCU_getChildren(diTypes, node);
  n = dynlen(dsChildren);
  for(i=1;i<=n;i++)
  {
    switch(diTypes[i]) //  flags: 1 = CU, 2 = DU, 0 = Obj
    {
      case 0:
        __dcsCaen_analizetree(dsChildren[i],domain,dsList);
        break;        
      case 2:
        fwCU_getType(dsChildren[i], type);
        if(type == "FwFSMConfDBDUT"){
          dynAppend(dsList,domain);
          }
        break;
      case 1: // CU
        domain = dsChildren[i];
        __dcsCaen_analizetree(dsChildren[i],domain,dsList);
    }
  }
}

// ------------------------------------------------------------
//  returns the available states for dcsLV or dcsHV
dyn_string dcsCaen_getFSMCaenStates(string type)
{
  switch(type){
    case DCSCAEN_LV_BASE_CHANNEL:
      return makeDynString("OFF","READY","RAMP_UP_READY","RAMP_DW_OFF");
      break;
    case DCSCAEN_HV_INTER_CHANNEL:
      return makeDynString("READY","INTERMEDIATE","RAMP_UP_READY",
                           "RAMP_UP_INTER","RAMP_DW_OFF",
                           "RAMP_DW_INTER","OFF");
      break;
    case DCSCAEN_HV_BASE_CHANNEL:
      return makeDynString("READY","RAMP_UP_READY",
                           "RAMP_DW_OFF","OFF");
      break;
    default:
      return makeDynString("NULL");
  }
}

// ------------------------------------------------------------
// gets a recipe object for a domain (half a sector) device acording to the state
// the last argument selects if we are using logic names or hardware names,
// the default is logic names
dyn_dyn_mixed dcsCaen_getFSMRecipeFromCache(string domain,
                                            string type, 
                                            string state, 
                                            dyn_string &exceptionInfo, 
                                            string device = "", 
                                            bool useHardware = FALSE,
                                            string systemName="")
{
  dyn_dyn_mixed recipeObj;
  dyn_string deviceList;
		
  string logicName;
	
  if(strlen(device)>= 1){
    if(useHardware) logicName = dpGetAlias(device + "."); 									// gets the logic name for the device
    else logicName = device;
    deviceList[1] = logicName;																						// could get the data from the pvss hierarchy, but for the moment returns
  }

  if(patternMatch("*_A*", domain)) systemName="spd_a:";
  if(patternMatch("*_C*", domain)) systemName="spd_c:";  
  DebugTN("fwConfigurationDB_allowResolveRemote = "+fwConfigurationDB_allowResolveRemote);
  
  string recipeName =systemName + domain + "/" + type + "/" + state;									// for the moment the recipes are created acording to domain/type/state
																																			// gets the recipe from the cache only for this device	
  DebugTN("RECIPE NAME = "+recipeName);
  fwConfigurationDB_getRecipeFromCache( recipeName, deviceList, 
                                        fwDevice_LOGICAL, recipeObj, 
                                        exceptionInfo, systemName);
  if (dynlen(exceptionInfo)){DebugTN(exceptionInfo); return -1;}
  return recipeObj;
}
// ------------------------------------------------------------

// ------------------------------------------------------------
// function return values for one caen channel in a recipe
dyn_mixed dcsCaen_getDataChannel(string logicName, dyn_dyn_mixed recipeObj )
{
  dyn_mixed dataOut;
  dyn_mixed aux;
  int size = dynlen(recipeObj[1]);
  int n,i;
  __initVariables();
  
  dyn_string deviceElements;
  dyn_string exceptionInfo;

  dyn_string settings = __dcsCaen_getDpeNameByType(DCSCAEN_HV_INTER_CHANNEL);
  

  for (int index = 1; index <= dynlen(settings) ;index++ ){ 
    deviceElements[index]= logicName+ settings[index]+"*";
  }
  
  fwConfigurationDB_extractSettingFromRecipe( recipeObj, deviceElements, dataOut, exceptionInfo);  
  dynInsertAt(dataOut, logicName, 1);
  
  if (dynlen(exceptionInfo))DebugTN(exceptionInfo);
  //DebugTN(dataOut);
  
  return dataOut;
}

// ------------------------------------------------------------
// function to return all values for all diferent caen channels in a recipe
dyn_dyn_mixed dcsCaen_getDataFromRecipe(dyn_dyn_mixed recipeObj)
{
  dyn_mixed channels;
  channels = recipeObj[2];
  
  dynUnique (channels);	// gets all diferent channels
  dyn_dyn_mixed dataOut;
	
  //DebugTN (channels);
	
  int n = dynlen(channels);
  int i;
  for (i = 1; i <= n; i++){	
    dynAppend( dataOut, dcsCaen_getDataChannel(channels[i], recipeObj));
  }
	
  //DebugTN(dataOut);
  return dataOut;
}
// ------------------------------------------------------------


// ------------------------------------------------------------
// stores a recipe with the right naming convention for the FSM
string dcsCaen_storeFSMRecipeInCache(string domain,
                                   string type, 
                                   string state, 
                                   dyn_dyn_mixed recipeObj, 
                                   dyn_string exceptionInfo,
                                   string systemName=""){
  string recipeName = systemName+domain+"/"+type+"/"+state;
  fwConfigurationDB_storeRecipeInCache( recipeObj, recipeName, 
                                        fwDevice_LOGICAL, exceptionInfo);
  
  return recipeName;
}
// ------------------------------------------------------------

// ------------------------------------------------------------
// changes the settings of one caen channel in one recipe object
void dcsCaen_recipeChSettings(dyn_dyn_mixed &recipeObj, 
                              string logicName, 
                              dyn_mixed caenData){

  /* ------------- This is beatiful but don't works -------
  dyn_dyn_mixed dstRecipeObject;
  dyn_dyn_mixed srcRecipeObject = dcsCaen_createRecipeObj(logicName, caenData);
  dyn_string exceptionInfo;
  fwConfigurationDB_combineRecipes(dstRecipeObject,recipeObj,srcRecipeObject,exceptionInfo);
  recipeObj = dstRecipeObject ; 
  ------------------------------------ */

  dyn_dyn_mixed origRecipeObj;
  
  int nOfItems = dynlen(recipeObj[1]);
  int nOfProperties = fwConfigurationDB_RO_MAXIDX;
  
  int n,i,j;
  __initVariables();	
                       
  // Estract the elements for the logicName channel 
  for ( n = nOfItems ; n >= 1 ; n --){
    if( recipeObj[2][n] == logicName){
      for(j=1;j<=nOfProperties;j++) {
        dynRemove(recipeObj[j],n);
      }
    }
  }  
  
  // Now create the recipe with the new data
  dyn_dyn_mixed newRecipeObj = dcsCaen_createRecipeObj(logicName,caenData ); // addSystemName ,sysName 

  if(dynlen(newRecipeObj) == 0){  // This means no Items to add
    return;
  }
  
  // And Merge ...
  nOfItems = dynlen(newRecipeObj[1]);

  for ( n = 1 ; n <= nOfItems ; n++)
    for( j = 1; j <= nOfProperties; j++)
      dynAppend(recipeObj[j],newRecipeObj[j][n]);


}
// ------------------------------------------------------------

// ------------------------------------------------------------
// get the param value from User Defined string 
anytype dcsCaen_getUserDefinedParam(string recipe, string param){
																															// gets the recipe from the cache
  dyn_string dsAp1,dsAp2;
  int i;
  
  dsAp1 = strsplit(recipe,",");
  for(i=1;i<=dynlen(dsAp1);i++) {
    dsAp2 = strsplit(dsAp1[i],"=");
    if(strpos(strtolower(dsAp2[1]),strtolower(param))>=0) {
      DebugTN("dcsCaen_getUserDefinedParam executed, value = " + dsAp2[2]);
      return(dsAp2[2]);
    }
  }
  return("");
}
// ------------------------------------------------------------

// ------------------------------------------------------------
// set the param value from User Defined string 
string dcsCaen_setUserDefinedParam(string recipe, string param, anytype value){
																															// gets the recipe from the cache
  dyn_string dsAp1,dsAp2;
  int i;
  bool flag = false;
  
  dsAp1 = strsplit(recipe,",");
  for(i=1;i<=dynlen(dsAp1);i++) {
    dsAp2 = strsplit(dsAp1[i],"=");
    if(strpos(strtolower(dsAp2[1]),strtolower(param))>=0) {
      dsAp1[i]= strtolower(dsAp2[1])+"="+value;
      flag = true;
    }
  }
  if(flag) {
    recipe = "";
    for(i=1;i<=dynlen(dsAp1);i++) recipe += "," + dsAp1[i] ;
    return(substr(recipe,1));
  } else {
    if(recipe == "")
      return(strtolower(param)+"="+value);
    else
      return(recipe + "," + strtolower(param)+"="+value);
  }

}
// ------------------------------------------------------------

// ------------------------------------------------------------
// try to set the Default setting variable
dyn_string dcsCaen_getDefaultSettingsChannel(string sysn, string dp)
{
  int i;
  
  dyn_mixed caenDefault;
 
  dp =  sysn + dp + DCSCAEN_DEFAULT_DPE;
   
  if( dpExists(dp) )
  {
    dpGet(dp, caenDefault);
    for(i=dynlen(caenDefault)+1;i<DCSCAEN_OUT_STRUCTSIZE;i++) dynAppend(caenDefault,"");
  }
  else
  {
    DebugTN("dcsCaen library: Default settings for device:"+dp+" not found !");
    for(i=1;i<DCSCAEN_OUT_STRUCTSIZE;i++) dynAppend(caenDefault,"");
  }

  return caenDefault;
}

// ------------------------------------------------------------
// try to set the Default setting variable
void dcsCaen_setDefaultSettingsChannel(string sysn ,string dp,dyn_string caenDefault)
{
  int count = 0;
  bool bFlag = true;
  dyn_dyn_string elements;
  dyn_dyn_int types;

  while(bFlag)
  {
    if(dpExists(dp))
    {
      dpSet(sysn + dp + DCSCAEN_DEFAULT_DPE, caenDefault);
      bFlag = false;
    }
    else
    {
      // now try to create the dp
      if (dpCreate(dp, DCSCAEN_DPTYPEPRFIX + DCSCAEN_DEFAULT_DPTYPE, getSystemId(sysn) ) == -1)
      {
        // now try to create the dpType
        elements[1] = makeDynString (DCSCAEN_DPTYPEPRFIX + DCSCAEN_DEFAULT_DPTYPE, "");
        elements[2] = makeDynString ("", "settings");
        types[1] = makeDynInt(DPEL_STRUCT);
        types[2] = makeDynInt(0, DPEL_DYN_STRING);
        
        if( dpTypeCreate(elements,types) == -1)
        {
          dyn_errClass err;
          err = getLastError(); 
          if(dynlen(err) > 0)
          {
            errorDialog(err);
          }
          DebugTN("dcsCaen library: Error creating default settings dpType !");
          bFlag = false;
        }
        else
        {
          // Wait for system stabilization then retray
          if(++count > 3) bFlag = false;
          delay(0,250);
        }
      }
      else
      {
        // Wait for system stabilization then retray
        if(++count > 3) bFlag = false;
        delay(0,250);
      }            
    }
  }
  return;
}
// ------------------------------------------------------------
    
// ------------------------------------------------------------
// Returns a bidimentional array with the caen settings from a recipe
dyn_dyn_mixed dcsCaen_getFSMDataFromRecipe(string domain, 
                                           string type, 
                                           string state, 
                                           dyn_string &exceptionInfo, 
                                           string device = "", 
                                           bool useHardware = FALSE,
                                           string systemName ="")
{
  dyn_dyn_mixed recipeObj = dcsCaen_getFSMRecipeFromCache( domain, type, state, exceptionInfo, logicName, useHardware, systemName);
  dyn_dyn_mixed caenSettings = dcsCaen_getDataFromRecipe( recipeObj);	
  return caenSettings;
}
// ------------------------------------------------------------








/* -----------------------------------------------------

   name : dscCaen_DomainsWithType()
   param input : Top Node (string)
                 Domain List (dyn_string)
                 FSM type  (string)
   param output : Logical Names list (dyn_string)

   Description : returns the domains wich have at least one of a certain type of device
                 does not loop over other domains(CU's)

              History
              
   12/4/2007  -  Creation                  
   
------------------------------------------------------- */
dcsCaen_getDomainsWithType(string topNode,dyn_string &domainList, string type){
  

  dyn_string dsChildren;
  dyn_int diTypes;
  string childType;
  dyn_string deviceList;
  
  dsChildren = fwCU_getChildren(diTypes, topNode);
  //DebugTN(topNode, dsChildren, diTypes);
  
  for (int child = 1 ; child <= dynlen(dsChildren) ; child ++ ){
    switch(diTypes[child]) //  flags: 1 = CU, 2 = DU, 0 = Obj
    {
       case 1:
         deviceList = makeDynString();
         dcsCaen_getDeviceListOfDomain( dsChildren[child], deviceList, type);
         //DebugTN(dsChildren[child], deviceList);
         
         if (dynlen(deviceList)){
           dynAppend(domainList, dsChildren[child]);
         }
         
         dcsCaen_DomainsWithType( dsChildren[child], domainList, type);
         break;
         
       case 2:
         break;
        
        case 0:
          break;
        break;
    }
  }
}

/* -----------------------------------------------------

   name : dcsCaen_getFsmType()
   param input : Top Node (string)
                 Domain List (dyn_string)
                 FSM types  (dyn_string)
   param output : Logical Names list (dyn_string)

   Description : Returns the fsm type of device from our name 

              History
              
   12/4/2007  -  Creation                  
   
------------------------------------------------------- */

string dcsCaen_getFsmType(string type){
  switch(type){
   case DCSCAEN_HV_INTER_CHANNEL:
     return "FwCaenChannelDcsHVI";
    case DCSCAEN_LV_BASE_CHANNEL:	
      return "FwCaenChannelDcsLV";
    case DCSCAEN_HV_BASE_CHANNEL:
      return "FwCaenChannelDcsHV";
  }
}



// ------------------------------------------------------------
// function to append two recipe objects
void dcsCaen_appendRecipes(dyn_dyn_mixed &recipe1,dyn_dyn_mixed recipe2)
{
  int n, i;
  int size1;
	
  if(dynlen(recipe1)) 
    size1 = dynlen(recipe1[1]);
  else
    size1 = 0;
	
  //int size2 = dynlen(recipe2[1]);
  int nItems =  fwConfigurationDB_RO_MAXIDX;
  for( n = 1 ; n <= nItems ; n ++){
    for (i = 1 ; i <= dynlen(recipe2[n]) ; i ++){
      recipe1[n][size1+i] =  recipe2[n][i];
    }
  }
}
// ------------------------------------------------------------
	
// ------------------------------------------------------------
// function to return the value .v0 set from a recipe of a caen state
float dcsCaen_getFSMRecipeV0Value(string domain,
                                  string type,
                                  string state,
                                  dyn_string &exceptionInfo,
                                  string device,
                                  bool useHardware = FALSE,
                                  string systemName="")
{
  dyn_string deviceList;																
  float caenV0Value;
  
  
  
  dyn_mixed values;

  __initVariables();
  
  dyn_string deviceElements;
  dyn_string exceptionInfo;
  
            // loads the recipe from cache
  dyn_dyn_mixed recipeObj = dcsCaen_getFSMRecipeFromCache( domain, type, state, exceptionInfo, device, useHardware, systemName);
            // gets the logic name 
  
  string logicName;
  
  if (useHardware ){logicName= dpGetAlias(device+".");}
  else logicName = device;

  deviceElements[1]= logicName+"*";
  fwConfigurationDB_extractSettingFromRecipe( recipeObj, deviceElements, values, exceptionInfo);
  if (dynlen(exceptionInfo)){DebugTN(exceptionInfo); return -1;}
    

  for (int n = 1; n <= dynlen(deviceElements); n ++){
    if(deviceElements[n] == logicName + ".settings.v0"){
      //DebugTN("v0 value", logicName, values[n]); //commented out by SS on 06/05/2015 to remove the text output
      return values[n];
    }
  }
  
  return -1;
}
// ------------------------------------------------------------
	
// ------------------------------------------------------------
// function to apply one recipe of one device in the FSM from the cache,
// recipes in cache need to have the name : domain/command 
dyn_string dcsCaen_applyFSMRecipeFromCache(string domain,
                                           string type,
                                           string state,
                                           string device = "",
                                           bool useHardware = FALSE,
                                           string systemName="")
{
  dyn_string exceptionInfo;
  
  dyn_dyn_mixed recipeObj = dcsCaen_getFSMRecipeFromCache(domain,type, state, exceptionInfo, device, useHardware, systemName);
	
  if (dynlen(exceptionInfo)){DebugTN(exceptionInfo); return exceptionInfo;}
// DebugTN("Aplying Recipe", state, type, domain, device,recipeObj);

  // ---- Delay the application of recipes ---- 
  int ind = dynContains(recipeObj[4], ".userDefined");
  if(ind>0) {
    int DelayTime = dcsCaen_getUserDefinedParam(recipeObj[8][ind], "delay");
    if(DelayTime > 0) {
      // DebugTN(">Starting delay > :"+DelayTime+"  sec.");
      delay(DelayTime,0);
      // DebugTN(">End delay >");
    }
  }
  //----------------------
  
  DebugTN("RECIPE NAME = "+recipeObj);
  fwConfigurationDB_applyRecipe( recipeObj, fwDevice_LOGICAL, exceptionInfo, TRUE);										// applys the recipe
  if (dynlen(exceptionInfo)){DebugTN(exceptionInfo);}
   return exceptionInfo;
}    
// ------------------------------------------------------------

// ------------------------------------------------------------
// creates a recipe object for a single CAEN channel
dyn_dyn_mixed dcsCaen_createRecipeObj(string logicNode,
                                      dyn_mixed data, 
                                      bool addSystemName = true, string sysName = "")
{
  
  

  __initVariables();
  dyn_dyn_mixed objOut;

  int indexMax = fwConfigurationDB_RO_MAXIDX;

  int sizeRecipe = dynlen(DCSCAEN_SETTINGS_DPE);

  // format the system name  
  string sSysName = (addSystemName) ? ( (sysName == "") ? getSystemName(): sysName) : "" ;
  
  int i,j;
  int ind = 0;	
  mixed Value;
  dyn_string deviceElements;
  dyn_string exceptionInfo;
  
  for(i=1;i <= sizeRecipe ; i++){
    deviceElements[i]= logicNode+DCSCAEN_SETTINGS_DPE[i];
  
                       
  }
  
  
  fwConfigurationDB_makeRecipe( deviceElements, data, objOut, exceptionInfo);
  if(dynlen(exceptionInfo)){fwExceptionHandling_display(exceptionInfo);return;};
  //DebugTN("CreatObj", objOut);
  return objOut;
}
// ------------------------------------------------------------

// ----- Archive functions ------------------------------------------
/* ------------------------------------------------

Name: 	dcsCaen_isArchived()
Desc:   Test if some DPEs are archived
        
inp :   DpName  := string
inp :   Elements := dyn_string
out :   Result := bool

Auth : A.Franco - INFN Bari
Date : 24/01/2007
Ver. : 1.0

Hist : -

------------------------------------------------ */
bool dcsCaen_isArchived(string sDpName, dyn_string dsElements)
{
  int arcV,i;
  bool bArc = true;
  int num = dynlen(dsElements);

  for(i=1;i<=num;i++)
  {
    if (dpGet(sDpName + dsElements[i] + ":_archive.._type", arcV,
              sDpName + dsElements[i] + ":_archive.._archive", bArc) == -1) return(false);

    if(arcV != DPCONFIG_DB_ARCHIVEINFO || bArc == false)
	return(false);
  }
  
//DebugN(">-hmpBase_isArchived->>"+ sDpName + dsElements[1] + ":_archive.._type"+" := ",arcV,(bArc?"vero":"falso"));
  return(true);		
}
 
/* ---------------------------------------------------
Name: 	dcsCaen_getArchiveInfo()
Desc:   Get Archive Infos related to a DpE
        
inp :   DpName  := string
inp :   Element := string
out :   Result := dyn_dyn_string

Auth : A.Franco - INFN Bari
Date : 24/01/2007
Ver. : 1.0

Hist : -

------------------------------------------------ */
dyn_dyn_string dcsCaen_getArchiveInfo(string sDpName, string sElement)
{
  int iArc, iNr, iState;
  bool bArc;
  string sArcClass;
  string sSelectedArchiv;
  string sSystemName = substr(sDpName,0,strpos(sDpName,":")+1);

  if (dpGet(sDpName + sElement + ":_archive.._type", iArc,
            sDpName + sElement + ":_archive.._archive", bArc) == -1) return(false);
 
  dpGet(sDpName + sElement +":_archive.1._class", sArcClass);
  if (dpExists(sArcClass))
  {
    if (dpTypeName(dpSubStr(sArcClass, DPSUB_SYS_DP)) == "_RDBArchiveGroups" )
    {
      dpGet(dpSubStr(sArcClass, DPSUB_SYS_DP)+".groupName:_online.._value", sSelectedArchiv,
            dpSubStr(sArcClass, DPSUB_SYS_DP)+".managerNr:_online.._value", iNr);
      iNr += 2;
      sSelectedArchiv = "RDB-" + iNr + ") " + sSelectedArchiv;
    }
    else
      dpGet(dpSubStr(sArcClass, DPSUB_SYS_DP)+".general.arName:_online.._value", sSelectedArchiv,
            dpSubStr(sArcClass, DPSUB_SYS_DP)+".state:_original.._value", iState);
  }
  
  
  dyn_dyn_string ddsResults;
  dynAppend(ddsResults,makeDynString("DP Name",sDpName));
  dynAppend(ddsResults,makeDynString("DP Element Name",sElement));
  dynAppend(ddsResults,makeDynString("Archive Status",(bArc)?"TRUE":"FALSE"));
  dynAppend(ddsResults,makeDynString("Archive Type",(iArc == DPCONFIG_DB_ARCHIVEINFO) ? "ARCHIVE SETTINGS" : "NONE"));
  dynAppend(ddsResults,makeDynString("Archive System Name",sSystemName));
  dynAppend(ddsResults,makeDynString("Archive Class",sArcClass));
  dynAppend(ddsResults,makeDynString("Archive Name",sSelectedArchiv));
  dynAppend(ddsResults,makeDynString("Archive Manager Status",iState));     
  dynAppend(ddsResults,makeDynString("RDB Manager Number",iNr));     

  return(ddsResults);
    
}

/* ---------------------------------------------------
Name: 	dcsCaen_getArchiveStatusDpE()
Desc:   Get The Name of Archive Status DPE
        
inp :   DpName  := string
inp :   Element := string
out :   Result := string

Auth : A.Franco - INFN Bari
Date : 24/01/2007
Ver. : 1.0

Hist : -

------------------------------------------------ */
string dcsCaen_getArchiveStatusDpE(string sDpName, string sElement)
{
  string sArcClass;
     
  dpGet(sDpName + sElement +":_archive.1._class", sArcClass);
  if (dpExists(sArcClass))
  {
    if (dpTypeName(dpSubStr(sArcClass, DPSUB_SYS_DP)) == "_RDBArchiveGroups" )
    {
      return("RDB");
    }
    else
    {
      return( dpSubStr(sArcClass, DPSUB_SYS_DP)+".state");
    }
  }
}

// ===========================================================================
// CAEN library 
// Author : A.FRANCO - INFN BARI ITALY
// Ver 1.2  18/03/2007   - 
// ------------------------------------------
/*			History
  14/03/2007  - Imported from previous projects
			
  Functions List	
---------------------------------------------------------------------------------	             
  string dcsCaen_ChannelStatusDecode(int channelstatus,bool &error,bool &trip,
                                     bool &On,bool &Up,bool &Down)
  string dcsCaen_ChannelStatus2Color( int status)
  
  void 	 dcsCaen_CratesTimedControlCB(string dpe, int value)
  string dcsCaen_CrateControlStatus(string dp)
  ---------------------------------------------------------------------------------

*/

// ------------------------------------------
// Constants definition for Crate functions
const int NO_CONTROL_TRIGGER_TIME=20; // Seconds: timeout for the Crate connection Test

/* Status Bit Word for HV/LV Channels (Cfr. OPC Server v.2.X manual)
 0 : ON
 1 : Ramp Up
 2 : Ramp Down
 3 : Over Current
 4 : Over Voltage
 5 : Under Voltage
 6 : External Trip
 7 : Over HV Max
 8 : External Disabled
 9 : Internal Trip
10 : Calibration Error
11 : Unplugged
12 : Under Current 
13 : Over Voltage Protection
14 : Power Fail
15 : Temperature Error
------------------------------*/
// Constants for the Status detection masks
const int CAEN_TRIPMASK = 0x02280;   // Ovp | ITrip | HVMax  
const int CAEN_ERRORMASK = 0x08540;  // Cal | ETrip | EDis |  Terr
const int CAEN_NOCONTROLMASK = 0x04800;  //  Unp | PwFail  
const int CAEN_OFFMASK = 0x00000;     // This Mask Off,On,RampUp,RampDwn
const int CAEN_ONMASK = 0x00001;      // works with the XOR operation ...
const int CAEN_RAMPUPMASK = 0x00003;    
const int CAEN_RAMPDOWNMASK = 0x00005;    
const int CAEN_IGNOREDMASK = 0x01038;  //  OvC | UnV | OvV |  Unc    

// ============================================================================
//                     SECTION RELATED TO GENERAL CAEN USE FUNCTIONS
// ============================================================================

// ------------------------------------------
// dcsCaen_ChannelStatusDecode : Decode the status word
//
// Ver 1.0 25/8/2005
//
// this function return the string description of the channel status
// and sets some boolean vars
//
// History
// 20-10-2005 - Add Power Fail Error condition
//
string dcsCaen_ChannelStatusDecode(int channelstatus,bool &error,bool &trip,bool &On,bool &Up,bool &Down)
{

  bool 	OvC,OvV,UnV,ETrip,HVMax,EDis,ITrip,Cal,Unp,Unc,Ovp,Terr,Pwfail;
  string mes;

  On = getBit(channelstatus, 0 );
  Up = getBit(channelstatus, 1 );
  Down = getBit(channelstatus, 2 );
  OvC = getBit(channelstatus, 3 );
  OvV = getBit(channelstatus, 4 );
  UnV = getBit(channelstatus, 5 );
  ETrip = getBit(channelstatus, 6 );
  HVMax = getBit(channelstatus, 7 );
  EDis = getBit(channelstatus, 8 );
  ITrip = getBit(channelstatus, 9 );
  Cal = getBit(channelstatus, 10 );
  Unp = getBit(channelstatus, 11 );
  Unc = getBit(channelstatus, 12 );
  Ovp = getBit(channelstatus, 13 );
  Pwfail = getBit(channelstatus, 14 );
  Terr = getBit(channelstatus, 15 );

  // Trip condition
  trip = HVMax | ITrip | OvC | UnV | OvV | Ovp | Unc | Terr ;

  // Never goes in Error !
  error = false;

  // Made the message
  mes = (On)? "Channel On":"Channel Off";
  mes = (On && Up)?"Ramping Up":mes;
  mes = (On && Down)?"Ramping Down":mes;
  mes = (trip && Down)?"Tripping Down":mes;
  mes = (trip && !Down)?"Tripped":mes;
  mes += (OvC)?":Over Current":"";
  mes += (OvV)?":Over Voltage":"";
  mes += (Unc)?":Under Current":"";
  mes += (UnV)?":Under Voltage":"";
  mes += (Ovp)?":Over protection":"";
  mes += (Terr)?":Temperature":"";
  mes += (HVMax)?"HV Max":"";

  mes = (EDis)?"External Disabled":mes;
  mes = (ETrip)?"External Trip":mes;
  mes = (Unp)?"Channel Unplugged":mes;
  mes = (Cal)?"Calibration Error":mes;
  mes = (Pwfail)?"Power Fail Error":mes;

  return mes;
}


// ------------------------------------------
// hmpCAEN_ChannelStatus2Color: Decode the status word
//
// Ver 1.0 25/8/2005
//
// this function return the colour string of the channel status
//
// History
// 20-10-2005 - Change the color for ramping
// 21/12/2005 - Remap all colors for errors
// 20/02/2007 - Use CAEN Ch. Color scheme
//
// ------------------------------------------
string dcsCaen_ChannelStatus2Color(int status)
{
  switch(status)
  {
    case 0: // Chan OFF
      return("dcsCaenChannelOFF");
      break;
    case 1: // Chan ON
      return("dcsCaenChannelON");
      break;
    case 3:  // Ramping
      return("dcsCaenChannelRUMPUP");
      break;
    case 5:
      return("dcsCaenChannelRUMPDW");
      break;
  }

  // The unplugged condition colour
  if(getBit(status,8))
    return("dcsCaenChannelDISABLED");

  if(getBit(status,11))
    return("dcsCaenChannelUNPLUG");

  // The trip condition colour (OvC OvV UnV UnC)
  if(getBit(status,3) || getBit(status,4) || getBit(status,5) || getBit(status,12) )
    return("dcsCaenChannelOVC");

  // The trip condition colour (HVMax, ITRip, Ovp)
  if( getBit(status,7) || getBit(status,9) || getBit(status,13))
    return("dcsCaenChannelTRIP");

  // The Error condition colour (ETrip, PWf, Cal, Terr )
  if(getBit(status,10) || getBit(status,6) || getBit(status,15) || getBit(status,14))
    return("dcsCaenChannelERROR");

  return("FwDead");
}


// ============================================================================
//          SECTION RELATED TO THE SY1527 CAEN CRATE DEVICE
// ============================================================================

// ----------------------------------------------------------------------------
// dcsCaen_CratesTimedControlCB
/*
	This function performs the control of HV Crates every i seconds
        and compare the time stamp of the swRelease, this do the test 
        of connection
	
	F.A. ver 1.0   28/10/2004
	
	History
	
	28/10/2004 imported from HMPIDDCS3_3 proj.
		
-----------------------------------------------------------------*/
dcsCaen_CratesTimedControlCB(string dpe, int value)
{
  int i;
  time tLastChange, tNow;
  dyn_string dsCratesDP;
  bool bInvalid = false;
  string sState = "";	
  // get the current time 
  tNow = getCurrentTime();
  
  //Get all SY1527 crates
  dsCratesDP = dpNames("*","FwCaenCrateSY1527");

  // for each SY test the last change time of SwRelease
  for(i=1; i<= dynlen(dsCratesDP); i++)
  {
    dpGet( dsCratesDP[i]+".Information.SwRelease:_original.._value", sState,
           dsCratesDP[i]+".Information.SwRelease:_original.._stime", tLastChange,
           dsCratesDP[i]+".Information.SwRelease:_original.._aut_inv", bInvalid);
    if(sState == "LOST_CONTROL" && !bInvalid ) {
      dpSet(dsCratesDP[i]+".Information.SwRelease:_original.._aut_inv", true);
    } else if((period(getCurrentTime() - tLastChange) > NO_CONTROL_TRIGGER_TIME) && !bInvalid ) {
         dpSet(dsCratesDP[i]+".Information.SwRelease:_original.._value","LOST_CONTROL",
               dsCratesDP[i]+".Information.SwRelease:_original.._aut_inv", true);
    }
  }
  return;
}

// ----------------------------------------------------------------------------
// dcsCaen_CrateControlStatus
/*
	This function performs the control of CAEN Crates 
        verify the error/warning conditions
	
	F.A. ver 1.0   28/10/2004
	
	History
	
	28/10/2004 imported from HMPIDDCS3_3 proj.
		
----------------------------------------------------------------------- */
string dcsCaen_CrateControlStatus(string dp)
{
  int iPanIn, iPanOut,i,fault;
  string sFan,sPwS;
  dyn_string param;
  string resp = "READY";
	
  // Gets the global status indicators
  dpGet(dp+".FrontPanInP.FrontPanIn:_original.._value",iPanIn,
	dp+".PWstatus.HvPwSM:_original.._value",sPwS,	
	dp+".FanStatus.FanStat:_original.._value",sFan,
	dp+".FrontPanOutP.FrontPanOut:_original.._value",iPanOut);
				
  // Test conditions			
  if(getBit(iPanIn, 2 )) {
    // Kill Error severe
    // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " External Kill Signal received" +"|");
    resp = "EXTERNAL_KILL";
  }
  if(getBit(iPanIn, 3 )) {
    // Interlock Error severe
    // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " External Interlock Signal received" +"|");
    resp = "EXTERNAL_INTERLOCK";
  }

  // AC Severe Error
  param=strsplit(sPwS,":");
  if(param[1] == "-1") {
    // Generate the error message
    // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " SEVERE ERROR AC Failure !" +"|");

    // AC/DC Module stat
    fault = 0;
    for(i=2;i<6;i++) 
      if(param[i] == "-1") {
	  fault++;
	  // Generate the error message
          // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": ERROR AC/DC module n."+(i-1)+" fault !"  +"|");
      }
    if(fault > 1) {
      // error severe
      // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " SEVERE ERROR more than 1 AC/DC module failure. Kill All !" +"|");
      resp = "ERROR_AC_FAILURE";
    } else 
      if(fault == 1) {
	// error warning
	// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING 1 AC/DC module failure!" +"|");
	resp = "WARNING_AC_FAILURE";
      }
  }
  // Fan Status
  param=strsplit(sFan,":");
  // fan status stat
  for(i=1;i<13;i=i+2) 
    if(param[i] == "-1") {
      fault++;
      // Generate the error message
      // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": ERROR FAN n."+((i+1)/2)+" fault !"+"|");
    }
    if(fault > 2) {
      // error severe
      // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": SEVERE ERROR FAN module failure. Kill All !"+"|");
      resp = "ERROR_FAN_FAILURE";
    } 
    else
      if(fault > 0) {
        // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING 1 FAN module failure!"+"|");
	resp = "WARNING_FAN_FAILURE";
      }
  // Pan Out
  if(getBit(iPanOut, 9 )) {
    // Over temperature warning
    // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING at least une board in Over Temperature !"+"|");
    resp = "WARNING_TEMPERATURE";
  }
  if(getBit(iPanOut, 8 )) {
    // Fan Failure Severe error
    // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING Fan failure error !"+"|");
    resp = "WARNING_FAN_FAILURE";
  }
  return(resp);
}

