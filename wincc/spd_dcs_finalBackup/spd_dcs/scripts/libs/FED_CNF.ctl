
#uses "spdNamingCore.ctl"

const string SPDFED_ClientA = "dimClientA";
const string SPDFED_ClientC = "dimClientC";
const string SPDFED_OutChannelDp = ".ToServer.ChNumber";
const string SPDFED_OutOffsetDp = ".ToServer.Data.Offset";
const string SPDFED_OutDataDp =".ToServer.Data.DataOut";
const string SPDFED_OutCommandDp=".ToServer.Command";
const string SPDFED_InStatusDp=".FromServer.StatusCode";
const string SPDFED_InStatusRepDp=".FromServer.StatusReport";
const string SPDFED_InChannelDp=".FromServer.ChNumber";
const string SPDFED_InDataDp=".FromServer.DataIn";
const string SPDFED_InCommandRet=".FromServer.CommandRet";

const string SpdFedSystemName=SpdTopNodeSystemName; //defined in the spdNamingCore.ctl
const string SPDHS_ = "spdHS";

//_______________________________________________________________________________
SetFedDP(int ChNumber , string Cmd , int OffSet , dyn_int DataOut)
{
  dyn_int Data;
  
  dynClear (Data);
  Data[1] = dynlen(DataOut );
  dynInsertAt (Data ,DataOut ,2 );
  
  
  if(ChNumber < 60 || ChNumber > 119){
   
    dpSet(SpdFedSystemName+SPDFED_ClientA+".FromServer.StatusReport","");
    
    dpSet(SpdFedSystemName+SPDFED_ClientA+SPDFED_OutChannelDp , ChNumber,
    SpdFedSystemName+SPDFED_ClientA+SPDFED_OutOffsetDp , OffSet,
    SpdFedSystemName+SPDFED_ClientA+SPDFED_OutDataDp  , Data);
    dpSetWait(SpdFedSystemName+SPDFED_ClientA+SPDFED_OutCommandDp, Cmd);
    
  }
  if(ChNumber >= 60){
    
    dpSet(SpdFedSystemName+SPDFED_ClientC+".FromServer.StatusReport","");
    
    dpSet(SpdFedSystemName+SPDFED_ClientC+SPDFED_OutChannelDp , ChNumber,
    SpdFedSystemName+SPDFED_ClientC+SPDFED_OutOffsetDp , OffSet,
    SpdFedSystemName+SPDFED_ClientC+SPDFED_OutDataDp  , Data);
    dpSetWait(SpdFedSystemName+SPDFED_ClientC+SPDFED_OutCommandDp, Cmd);
  }
  
    

}


//_______________________________________________________________________________
int GetFedDP(int &ChNumber , string &CommandRet , dyn_int &DataIn)
{	
	int Info;

	string Error;

        
	//string systemName = SpdFedSystemName;
      string Client = SPDFED_ClientA;
      if(ChNumber >= 60) Client = SPDFED_ClientC;
          //"spd_dcs:dimClientC."
	dpGet(SpdFedSystemName+Client+SPDFED_InStatusDp,Info,
              SpdFedSystemName+Client+SPDFED_InStatusRepDp, Error);
	
	
	dpGet(SpdFedSystemName+Client+SPDFED_InChannelDp, ChNumber,
             SpdFedSystemName+Client+SPDFED_InDataDp, DataIn,
             SpdFedSystemName+Client+SPDFED_InCommandRet, CommandRet);
                
		
	
	return Info;
}

//_____________________________________________________________
// function to wait for the answer of the fed server 
// checks the return datapoint with a while loop and returns in case of a timeout
int spdFed_WaitForStatus( int channel, string command, int loopError=1000 ){
  

  string sStatus;
  int error=0;
  string Client = SPDFED_ClientA;			//constant defined in FED_CNF.ctl
  if(channel >= 60) Client = SPDFED_ClientC;	//constant defined in FED_CNF.ctl

  int loop = 0;

 
  while (1){
    
    dpGet(SpdFedSystemName+Client+".FromServer.StatusReport",sStatus);
    
    dpGet(SpdFedSystemName+Client+".FromServer.StatusCode",error);		
    
    if (patternMatch( "*"+command+"*", sStatus)){
      if (patternMatch( "INFO: Executed*", sStatus)) return 0;
      else return 1;
    }
    
    // if the loops runs for more than 500 times then the fed is lasting to loong to answer
    if (loop >= loopError){
      DebugTN("warning timeout on fed command", command, channel);
      return 1;
    }
    loop ++;
    delay(0,1);
  }
  
  
  return error;
}


spdFed_ErrorPanel(dyn_string errorList){
  ChildPanelOnCentralModal( "vision/MessageWarning", getCatStr( "e_para", "warning" ),errorList);
}
//_____________________________________________________________
// function to call a panel to display a error list
void spdErrorListDisplay( dyn_string errorList){
  
  ChildPanelOnCentralModal("objects/HsConfig/ErrorList.pnl", 
	                   "Errors",
                            makeDynString("$errors:" + errorList));
}
