//MAGNET SAFE procedures 2017

#uses "spd_safe/spdSafe_definitions.ctl"


//-------- moving DIPOLE SAFE -----------------------------------------------------//
int spdSafe_goDipoleSafe()
{
  //set moving to 1
  dpSet(s_dpMovingDipoleSafe,1);
  //
  //----- your code to go SAFE for the DIPOLE ramp ---------
  //
  delay(1);    //just an example.. remove it if you dont't need
  //if (problem) return -1; //example
  //
  //---------------- end of your code ---------
  return 0;
}

//-------- moving SOLENOID SAFE -----------------------------------------------------//
int spdSafe_goSolenoidSafe()
{
  //set moving to 1
  dpSet(s_dpMovingSolenoidSafe,1);
  //
  //----- your code to go SAFE for the SOLENOID ramp ---------
  //
  delay(1);    //just an example.. remove it if you dont't need
  //if (problem) return -1; //example
  //
  //---------------- end of your code ---------
  return 0;
}

//-------- restoring from DIPOLE SAFE -----------------------------------------------------//
int spdSafe_restoreFromDipoleSafe()
{
  //set moving to 1
  dpSet(s_dpRestoringFromDipoleSafe,1);
  //
  //----- your code to go back after the SOLENOID ramp ---------
  //
  delay(1);    //just an example.. remove it if you dont't need
  //if (problem) return -1; //example
  //
  //---------------- end of your code ---------
  return 0;
}

//-------- restoring from SOLENOID SAFE -----------------------------------------------------//
int spdSafe_restoreFromSolenoidSafe()
{
  //set moving to 1
  dpSet(s_dpRestoringFromSolenoidSafe,1);
  //
  //----- your code to go back after the SOLENOID ramp ---------
  //
  delay(1);    //just an example.. remove it if you dont't need
  //if (problem) return -1; //example
  //
  //---------------- end of your code ---------
  return 0;
}

//--------------------------------------------------------------------------------//
//  regular checks activated by timed_function                         -----------//
//--------------------------------------------------------------------------------//
int spdSafe_IsDipoleSafeActions()
{
  bool bSafe,bMoving,bRestoring;
  int rc;
  //YOUR CODE! check hardware status and define boolean bSafe
  bSafe=TRUE; // just an example, change with real check
  
  if(bSafe) {   
    dpGet(s_dpMovingDipoleSafe,bMoving);
    // if moving was set, now we can unset and declare completed
    if (bMoving) {
      DebugTN("timedFunction DipoleSafe: unset moving state");
      dpSetWait(s_dpMovingDipoleSafe,0);
      dpSetWait(s_dpCompletedGoDipoleSafe,TRUE);
    }
    dpSetWait(s_dpIsDipoleSafe,TRUE);
  }
  else { 
     dpSetWait(s_dpIsDipoleSafe,FALSE);
  }
    // if last action was "restoring", we can now unset and declare completed
    dpGet(s_dpRestoringFromDipoleSafe,bRestoring);
    if (bRestoring) {
      DebugTN("timedFunction RestoreFromDipoleSafe: unset restoring state");
      dpSetWait(s_dpRestoringFromDipoleSafe,0);
      dpSetWait(s_dpCompletedRestoreFromDipoleSafe,TRUE);
    }
  //}
  if (bSafe) return 1;
  else return 0;
}

int spdSafe_IsSolenoidSafeActions()
{
  bool bSafe,bMoving,bRestoring;
  int rc;
  //YOUR CODE! check hardware status and define boolean bSafe
  bSafe=TRUE; // just an example, change with real check
  
  if(bSafe) {   
    dpGet(s_dpMovingSolenoidSafe,bMoving);
    // if moving was set, now we can unset and declare completed
    if (bMoving) {
      DebugTN("timedFunction SolenoidSafe: unset moving state");
      dpSetWait(s_dpMovingSolenoidSafe,0);
      dpSetWait(s_dpCompletedGoSolenoidSafe,TRUE);
    }
    dpSetWait(s_dpIsSolenoidSafe,TRUE);
  }
  else { 
      dpSetWait(s_dpIsSolenoidSafe,FALSE);
  }
    // if last action was "restoring", we can now unset and declare completed
    dpGet(s_dpRestoringFromSolenoidSafe,bRestoring);
    if (bRestoring) {
      DebugTN("timedFunction RestoreFromSolenoidSafe: unset restoring state");
      dpSetWait(s_dpRestoringFromSolenoidSafe,0);
      dpSetWait(s_dpCompletedRestoreFromSolenoidSafe,TRUE);
    }
  //dpSetWait(s_dpIsSolenoidSafe,FALSE);
  //}
  if (bSafe) return 1;
  else return 0;
}
