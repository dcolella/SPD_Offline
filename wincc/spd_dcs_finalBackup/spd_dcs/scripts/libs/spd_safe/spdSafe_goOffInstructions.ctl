// EMERGENCY SHUTDOWN mechanism 2017

#uses "spd_safe/spdSafe_definitions.ctl"


//-------- go OFF -----------------------------------------------------//
int spdSafe_goOff()
{
  //set moving to 1
  dpSet(s_dpMovingOff,1);
  //
  //----- your code to SWITCH OFF ---------
  //
  //delay(0.5);    //just an example.. remove it if you dont't need
  // if (problem) return -1; //example
  //
  //---------------- end of your code ---------
  //done, set moving to 0

  dyn_string ListSector, ListHalfSector;
  dyn_int CUint;
  string hlStatus;

  ForceToBeamTuning();

  bool b_isSuperSafe = false;//use this variable to set superSafe/not superSafe
  
  while(!b_isSuperSafe){
    b_isSuperSafe = spdHandShakeSuperSafeCondition();  
    DebugTN("spdEmergencyButton.ctl: SPD is not SUPERSAFE yet");
    delay(10);
  }   
  
  CUint = makeDynInt(0);
  ListSector = fwCU_getIncludedChildren(CUint,"SPD_DCS","");      
  for(int i=1;i<=dynlen(ListSector);i++){
  
    if(patternMatch("*Sector*", ListSector[i]))  {
      ListHalfSector = fwCU_getIncludedChildren(CUint,ListSector[i],"");         
      for(int j=1;j<=dynlen(ListHalfSector);j++){
        fwCU_sendCommand(ListHalfSector[j], "GO_MCM_ONLY");       
      }  
    }
  } 
  
  
  return 0;
}


//--------------------------------------------------------------------------------//
//  regular checks activated by timed_function                         -----------//
//--------------------------------------------------------------------------------//

int spdSafe_IsOffActions()
{
  bool bOff,bMoving,bRestoring;
  int rc;
  string spdStatus;
  //real detector: check hardware status and define boolean bOff
  //bOff=FALSE; //just an example--> chanche with real check

  bOff = TRUE;
  
  dyn_string ListSector, ListHalfSector;
  dyn_int CUint;
  string hlStatus;
  
  CUint = makeDynInt(0);
  ListSector = fwCU_getIncludedChildren(CUint,"SPD_DCS","");      
  for(int i=1;i<=dynlen(ListSector);i++){
  
    if(patternMatch("*Sector*", ListSector[i]))  {
      ListHalfSector = fwCU_getIncludedChildren(CUint,ListSector[i],"");         
      for(int j=1;j<=dynlen(ListHalfSector);j++){
      fwCU_getState(ListHalfSector[j], hlStatus);     
      if(hlStatus != "MCM_ONLY")   bOff = FALSE;    
      }  
    }
  }  
  
  
  if(bOff) {   
    dpGet(s_dpMovingOff,bMoving);
    // if moving was set, now we can unset and declare completed
    if (bMoving) {
      DebugTN("timedFunction isOff: unset moving state");
      dpSetWait(s_dpMovingOff,0);
      dpSetWait(s_dpCompletedGoOff,TRUE);
    }
    dpSetWait(s_dpIsOff,TRUE);
  }
  // restoring from OFF is not foreseen at present...
  /*
  else { 
    // if last action was "restoring", we can now unset and declare completed
    dpGet(s_dpRestoringFromOff,bRestoring);
    if (bRestoring) {
      DebugTN("timedFunction RestoreFromOff: unset restoring state");
      dpSetWait(s_dpRestoringFromOff,0);
      dpSetWait(s_dpCompletedRestoreFromOff,TRUE);
    }
    dpSetWait(s_dpIsOff,FALSE);
  }
  */
  if (bOff) return 1;
  else return 0;
}
