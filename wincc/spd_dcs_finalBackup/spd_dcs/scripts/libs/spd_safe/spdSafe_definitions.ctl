string s_dpSafe =           "spd_dcs:spdEmergencyButton";	

string s_dpGoSafe =         s_dpSafe+".Actions.goSafe";	
string s_dpIsSafe =         s_dpSafe+".Actual.isSafe";	
string s_dpGotGoSafe =      s_dpSafe+".Actual.gotGoSafe";	
string s_dpHeartBeat =      s_dpSafe+".Actual.heartbeat";	

string s_dpGoSuperSafe =    s_dpSafe+".Actions.goSuperSafe";	
string s_dpIsSuperSafe =    s_dpSafe+".Actual.isSuperSafe";	
string s_dpGotGoSuperSafe = s_dpSafe+".Actual.gotGoSuperSafe";	

//added EYETS 2017
string s_dpRestoreSafe    =             s_dpSafe+".Actions.restoreSafe";
string s_dpGoDipoleSafe   =             s_dpSafe+".Actions.goDipoleSafe";
string s_dpGoSolenoidSafe =             s_dpSafe+".Actions.goSolenoidSafe";
string s_dpRestoreFromDipoleSafe =      s_dpSafe+".Actions.restoreFromDipoleSafe";
string s_dpRestoreFromSolenoidSafe =    s_dpSafe+".Actions.restoreFromSolenoidSafe";
string s_dpGoOff =                      s_dpSafe+".Actions.goOff";
string s_dpMovingSafe =                 s_dpSafe+".Actual.movingSafe";
string s_dpMovingSuperSafe =            s_dpSafe+".Actual.movingSuperSafe";
string s_dpGotRestoreSafe =             s_dpSafe+".Actual.gotRestoreSafe";
string s_dpRestoringSafe =              s_dpSafe+".Actual.restoringSafe";
string s_dpIsDipoleSafe =               s_dpSafe+".Actual.isDipoleSafe";
string s_dpIsSolenoidSafe =             s_dpSafe+".Actual.isSolenoidSafe";
string s_dpGotGoDipoleSafe =            s_dpSafe+".Actual.gotGoDipoleSafe";
string s_dpGotGoSolenoidSafe =          s_dpSafe+".Actual.gotGoSolenoidSafe";
string s_dpMovingDipoleSafe =           s_dpSafe+".Actual.movingDipoleSafe";
string s_dpMovingSolenoidSafe =         s_dpSafe+".Actual.movingSolenoidSafe";
string s_dpGotRestoreFromDipoleSafe =   s_dpSafe+".Actual.gotRestoreFromDipoleSafe";
string s_dpGotRestoreFromSolenoidSafe = s_dpSafe+".Actual.gotRestoreFromSolenoidSafe";
string s_dpRestoringFromDipoleSafe =    s_dpSafe+".Actual.restoringFromDipoleSafe";
string s_dpRestoringFromSolenoidSafe =  s_dpSafe+".Actual.restoringFromSolenoidSafe";
string s_dpGotGoOff =                   s_dpSafe+".Actual.gotGoOff";
string s_dpMovingOff =                  s_dpSafe+".Actual.movingOff";
string s_dpIsOff =                      s_dpSafe+".Actual.isOff";
string s_dpCompletedGoSafe =                  s_dpSafe+".Actual.completedGoSafe";
string s_dpCompletedGoSuperSafe =             s_dpSafe+".Actual.completedGoSuperSafe";
string s_dpCompletedRestoreSafe =             s_dpSafe+".Actual.completedRestoreSafe";
string s_dpCompletedGoDipoleSafe =            s_dpSafe+".Actual.completedGoDipoleSafe";
string s_dpCompletedGoSolenoidSafe =          s_dpSafe+".Actual.completedGoSolenoidSafe";
string s_dpCompletedRestoreFromDipoleSafe =   s_dpSafe+".Actual.completedRestoreFromDipoleSafe";
string s_dpCompletedRestoreFromSolenoidSafe = s_dpSafe+".Actual.completedRestoreFromSolenoidSafe";
string s_dpCompletedGoOff =                   s_dpSafe+".Actual.completedGoOff";
