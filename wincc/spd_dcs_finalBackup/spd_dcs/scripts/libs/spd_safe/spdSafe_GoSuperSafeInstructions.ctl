#uses "spd_safe/spdSafe_definitions.ctl"

bool spdSafe_IsSuperSafeActions()
{
  bool b_isSuperSafe;//use this variable to set superSafe/not superSafe
  bool bMoving;
////////////////////////////////////////////////////
//write here the code to check if your detector is in safe state.
//BEGIN OF YOUR CODE
//  b_isSuperSafe = false;//use this variable to set safe/not safe
  
   b_isSuperSafe = spdHandShakeSuperSafeCondition();  
  
  //DebugTN("spdEmergencyButton.ctl: IsSuperSafe condition to be implemented!!");
  //delay(2);//a delay just as example: remove it.

//END OF YOUR CODE
////////////////////////////////////////////////////

  if(b_isSuperSafe) {     
    // now unset moving state, if any   
    dpGet(s_dpMovingSuperSafe, bMoving);
    if (bMoving) {
      DebugTN("timedFunction GoSuperSafe: unset moving state");
      dpSetWait(s_dpMovingSuperSafe,0);
      dpSetWait(s_dpCompletedGoSuperSafe,TRUE); 
    }
  }   
   
//do not delete this line:
	return b_isSuperSafe;
}

int spdSafe_GoSuperSafeActions()
{
////////////////////////////////////////////////////
//write here the code to bring your detector to super safe state.
//BEGIN OF YOUR CODE
  
  DebugTN("spdEmergencyButton.ctl: SuperSafe Button Pressed!!!!!!!!!");
  dpSet("spd_dcs:spdEmergencyButton.Actual.movingSuperSafe",true);
  dpSetWait(s_dpMovingSuperSafe,1); 
  ForceToBeamTuning();
  
  bool b_isSafe=false;  
  
  while(!b_isSafe){
    b_isSafe = spdSafe_IsSafeActions();
    DebugTN("spdEmergencyButton.ctl: SPD is not SUPERSAFE yet");
    delay(5);
  } 
  DebugTN("spdEmergencyButton.ctl: SPD is SUPERSAFE now");
  dpSet("spd_dcs:spdEmergencyButton.Actual.movingSuperSafe",false);
  fwCU_sendCommand("SPD_DCS", "GO_BEAM_TUN");
//  delay(5);//a delay just as example: remove it.

//END OF YOUR CODE
////////////////////////////////////////////////////

//do not delete this line:
	return 0;
}
