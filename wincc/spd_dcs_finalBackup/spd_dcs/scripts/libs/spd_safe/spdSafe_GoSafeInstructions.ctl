#uses "spd_safe/spdSafe_definitions.ctl"
#uses "spdSafePower.ctl"
#uses "spdOffDetElectronics.ctl"

//do not modify the name of this function


void spdSafe_GoSafeActionsInit(string s_dpSafe)
{
////////////////////////////////////////////////////
//write here the code to bring your detector to safe state.
  DebugTN("Automatic Safe condition!!");
//END OF YOUR CODE
////////////////////////////////////////////////////
  
//do not delete this line:
  return;
}


int spdSafe_GoSafeActions()
{
////////////////////////////////////////////////////
//write here the code to bring your detector to safe state.
//BEGIN OF YOUR CODE
	DebugTN("spdEmergencyButton.ctl: Safety Button Pressed!!!!!!!!!");
  dpSet("spd_dcs:spdEmergencyButton.Actual.movingSafe",true);
  dpSetWait(s_dpMovingSafe,1); 
  ForceToBeamTuning();

  //delay(5);
  //check that all modules were ramped down
   
  bool b_isSafe=false;  
  
  while(!b_isSafe){
    b_isSafe = spdSafe_IsSafeActions();
    DebugTN("spdEmergencyButton.ctl: SPD is not SAFE yet");
    delay(5);
  } 
  DebugTN("spdEmergencyButton.ctl: SPD is SAFE now");
  dpSet("spd_dcs:spdEmergencyButton.Actual.movingSafe",false);
  fwCU_sendCommand("SPD_DCS", "GO_BEAM_TUN");
	//delay(1);//a delay just as example: remove it.

//END OF YOUR CODE
////////////////////////////////////////////////////

//do not delete this line:
	return 0;
}


bool spdSafe_IsSafeActions()
{
  bool b_isSafe;//use this variable to set safe/not safe
  bool bMoving, bRestoring, bSuperSafe;  
////////////////////////////////////////////////////
//write here the code to check if your detector is in safe state.
//BEGIN OF YOUR CODE
//  	b_isSafe = false;//use this variable to set safe/not safe  

  b_isSafe = spdHandShakeSafeCondition();
   
  if(b_isSafe) {   
    dpGet(s_dpMovingSafe, bMoving);
    // if moving was set, now we can unset and declare completed
    if (bMoving) {
      DebugTN("timedFunction GoSafe: unset moving state");
      dpSetWait(s_dpMovingSafe,0);
      dpSetWait(s_dpCompletedGoSafe,TRUE);
    }
    // if restoring was set, we can now unset and declare completed
    dpGet(s_dpRestoringSafe, bRestoring);
    if (bRestoring) {
      dpGet(s_dpIsSuperSafe, bSuperSafe);
      if (!bSuperSafe) {
        DebugTN("timedFunction GoSafe: unset restoring state");
        dpSetWait(s_dpRestoringSafe,0);
        dpSetWait(s_dpCompletedRestoreSafe,TRUE);
      }
    }
  }

//DebugTN("spdEmergencyButton.ctl: Safe condition " +  b_isSafe);
//delay(2);//a delay just as example: remove it.
//  b_isSafe = true; 
//END OF YOUR CODE
////////////////////////////////////////////////////

//do not delete this line:
	return b_isSafe;
}

