// SAFE procedure mechanism 2017

#uses "spd_safe/spdSafe_definitions.ctl"

//-------- moving from SUPERSAFE back to SAFE ---------------------------------------//
int spdSafe_restoreSafe()
{
  //set moving to 1
  dpSet(s_dpRestoringSafe,1);
  //
  //----your code to restore from SUPERSAFE to SAFE ---------
  //
  delay(1);    //just an example.. remove it if you dont't need
  // if (problem) return -1;
  //
  //----------------------------- end of your code -------
  return 0;
}
