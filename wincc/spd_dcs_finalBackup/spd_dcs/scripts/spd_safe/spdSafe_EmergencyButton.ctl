//Emergency Button
//ALICE spd - Marco Boccioli
//This script must be used to ramp down the HV 
//and to bring any other device to a safe state
//in case the Emergency button is pressed.

//NOTE: Do not modify the code

#uses "spd_safe/spdSafe_definitions.ctl"
#uses "spd_safe/spdSafe_GoSafeInstructions.ctl"
#uses "spd_safe/spdSafe_GoSuperSafeInstructions.ctl"

#uses "spd_safe/spdSafe_restoreSafeInstructions.ctl"
#uses "spd_safe/spdSafe_magnetSafeInstructions.ctl"
#uses "spd_safe/spdSafe_goOffInstructions.ctl"


main()
{
  int i_try =1;
  int i_maxTries =6;     
  spdSafe_GoSafeActionsInit(s_dpSafe);//this function can be found in spdSafe_goSafeInstructions.ctl
  while (i_try <= i_maxTries) {
    DebugTN("spdEmergencyButton.ctl: trying to connect to " + s_dpGoSafe + " (" + i_try + "/" + i_maxTries + ")");
 	  if (dpExists(s_dpGoSafe)) {       
       dpSet(s_dpGoSafe, false);//set it to false at startup of the script
       dpSet(s_dpGoSuperSafe, false);//set it to false at startup of the script
       dpSet(s_dpRestoreSafe, false);//set it to false at startup of the script
       dpSet(s_dpGoDipoleSafe, false);//set it to false at startup of the script
       dpSet(s_dpGoSolenoidSafe, false);//set it to false at startup of the script
       dpSet(s_dpRestoreFromDipoleSafe, false);//set it to false at startup of the script
       dpSet(s_dpRestoreFromSolenoidSafe, false);//set it to false at startup of the script
       dpSet(s_dpGoOff, false);//set it to false at startup of the script
       DebugTN("spdEmergencyButton script: connecting to the safe datapoint actions: " + s_dpGoSafe);    
       dpConnect("spdSafe_ConnectGoSafe",s_dpGoSafe);//check the status of the DP.            
       dpConnect("spdSafe_ConnectGoSuperSafe",s_dpGoSuperSafe);//check the status of the DP.   
       //new actions 2017
       dpConnect("spdSafe_ConnectRestoreSafe",s_dpRestoreSafe);  
       dpConnect("spdSafe_ConnectGoDipoleSafe",s_dpGoDipoleSafe);  
       dpConnect("spdSafe_ConnectGoSolenoidSafe",s_dpGoSolenoidSafe);  
       dpConnect("spdSafe_ConnectRestoreFromDipoleSafe",s_dpRestoreFromDipoleSafe);  
       dpConnect("spdSafe_ConnectRestoreFromSolenoidSafe",s_dpRestoreFromSolenoidSafe);  
       dpConnect("spdSafe_ConnectGoOff",s_dpGoOff);  
       break;
     }
     i_try++;                
     if (i_try > i_maxTries) DebugTN("spdEmergencyButton: ERROR: could not connect to " + s_dpGoSafe);    
     delay(10);
  }
  
	timedFunc("spdSafe_ConnectIsSafe", "_spdSafe_isSafeCheck");
  while (true) {
    bool b_heartBeat;
    dpGet(s_dpHeartBeat, b_heartBeat);
    dpSet(s_dpHeartBeat,! b_heartBeat);
    delay(2);
  }
}

void spdSafe_ConnectGoSafe(string dp, int goSafe)
{
	 spdSafe_GoSafe();
}
void spdSafe_ConnectGoSuperSafe(string dp, int goSafe)
{
	 spdSafe_GoSuperSafe();
}


//--------------------------------------------------------------------------------|
//---- spdSafe_IsSafeActions function is executed regularly by the timedFunction--|
//----   * calls spdSafe_IsSafeActions and similar from the detector library      |
//----   * keeps the corresponding datapoint uptodate                             |
//--------------------------------------------------------------------------------|
// add here the same mechanism for magnetSafe (and off?)
void spdSafe_ConnectIsSafe(string dpTF,time t1, time t2)
{
  dpSet(s_dpIsSafe,         spdSafe_IsSafeActions());         //this function can be found in spdSafe_goSafeInstructions.ctl
  dpSet(s_dpIsSuperSafe,    spdSafe_IsSuperSafeActions());    //this function can be found in spdSafe_goSuperSafeInstructions.ctl
  //
  dpSet(s_dpIsDipoleSafe,   spdSafe_IsDipoleSafeActions());   //function in spdSafe_magnetSafeInstructions.ctl
  dpSet(s_dpIsSolenoidSafe, spdSafe_IsSolenoidSafeActions()); //function in spdSafe_magnetSafeInstructions.ctl
  dpSet(s_dpIsOff,          spdSafe_IsOffActions());          //function in spdSafe_goOffInstructions.ctl
}


int spdSafe_GoSafe()
{
	 bool b_goSafe;	
	 int i_ret;
	 dpGet(s_dpGoSafe, b_goSafe);
	 if(!b_goSafe) return 1;
   DebugTN("spdEmergencyButton.ctl: got Go Safe command.");
	 dpSetWait(s_dpGotGoSafe,1);
   resetCompletedAction(s_dpCompletedGoSafe);
   resetCompletedAction(s_dpCompletedGoSuperSafe);   
   resetCompletedAction(s_dpCompletedRestoreSafe);
	 i_ret = spdSafe_GoSafeActions();//this function can be found in spdSafe_goSafeInstructions.ctl
   delay(1);
   if (i_ret == 0) {
     dpSet(s_dpCompletedGoSafe,1);
     dpSet(s_dpGotGoSafe,0);
   }
   else DebugTN("spdEmergencyButton.ctl: GoSafe command returned ERROR code "+irc);
	 return i_ret;
}


int spdSafe_GoSuperSafe()
{
	 bool b_goSuperSafe;	
	 int i_ret;
	 dpGet(s_dpGoSuperSafe, b_goSuperSafe);
	 if(!b_goSuperSafe) return 1;
   DebugTN("spdEmergencyButton.ctl: got Go SuperSafe command.");
	 dpSet(s_dpGotGoSuperSafe,1);
   resetCompletedAction(s_dpCompletedGoSafe);
   resetCompletedAction(s_dpCompletedGoSuperSafe);   
   resetCompletedAction(s_dpCompletedRestoreSafe);
	 i_ret = spdSafe_GoSuperSafeActions();//this function can be found in spdSafe_goSuperSafeInstructions.ctl
   delay(1);
   if (i_ret == 0) {
     dpSet(s_dpCompletedGoSuperSafe,1);
     dpSet(s_dpGotGoSuperSafe,0);
   }
   else DebugTN("spdEmergencyButton.ctl: GoSuperSafe command returned ERROR code "+irc);
   return i_ret;
}


//-- added EYETS 2017 -------------//
void  resetCompletedAction(string s)
{
  bool b;
  dpGet(s,b);
  if (b) dpSetWait(s,0);
}

//---------------------------------------------------------------------------------//
// all functions implement the acknowledge mechanism ("got" DPE)
// "moving" states are set by the libraries, unset by the timedFunction
// "completed" is set by the library or by the timedFunction, unset next time
//---------------------------------------------------------------------------------//

void spdSafe_ConnectRestoreSafe(string dp, bool b_dpRestoreSafe)
{
  if (b_dpRestoreSafe) {
    DebugTN("spdEmergencyButton.ctl: got restoreSafe command.");
	  dpSet(s_dpGotRestoreSafe,1);
    resetCompletedAction(s_dpCompletedGoSafe);
    resetCompletedAction(s_dpCompletedGoSuperSafe);   
    resetCompletedAction(s_dpCompletedRestoreSafe);
	  int irc = spdSafe_restoreSafe();//this function will be in spdSafe_SafeInstructions.ctl
	  delay(1);
    if (irc == 0) {
      dpSet(s_dpCompletedRestoreSafe,1);
      dpSet(s_dpGotRestoreSafe,0);
    }
    else DebugTN("spdEmergencyButton.ctl: restoreSafe command returned ERROR code "+irc);
  }
}
void spdSafe_ConnectGoDipoleSafe(string dp, bool b_dpGoDipoleSafe)
{
  if (b_dpGoDipoleSafe) {
    DebugTN("spdEmergencyButton.ctl: got goDipoleSafe command.");
	  dpSet(s_dpGotGoDipoleSafe,1);
    resetCompletedAction(s_dpCompletedGoDipoleSafe);
    resetCompletedAction(s_dpCompletedRestoreFromDipoleSafe);
    resetCompletedAction(s_dpCompletedGoSolenoidSafe);
    resetCompletedAction(s_dpCompletedRestoreFromSolenoidSafe);    
	  int irc = spdSafe_goDipoleSafe();//this function will be in spdSafe_SafeInstructions.ctl
	  delay(1);
    if (irc == 0) {
       dpSet(s_dpCompletedGoDipoleSafe,1); 
       dpSet(s_dpGotGoDipoleSafe,0);
    }
    else DebugTN("spdEmergencyButton.ctl: goDipoleSafe command returned ERROR code "+irc);
  }
}
void spdSafe_ConnectGoSolenoidSafe(string dp, bool b_dpGoSolenoidSafe)
{
  if (b_dpGoSolenoidSafe) {
    DebugTN("spdEmergencyButton.ctl: got goSolenoidSafe command.");
	  dpSet(s_dpGotGoSolenoidSafe,1);
    resetCompletedAction(s_dpCompletedGoDipoleSafe);
    resetCompletedAction(s_dpCompletedRestoreFromDipoleSafe);
    resetCompletedAction(s_dpCompletedGoSolenoidSafe);
    resetCompletedAction(s_dpCompletedRestoreFromSolenoidSafe);    
	  int irc = spdSafe_goSolenoidSafe();//this function will be in spdSafe_SafeInstructions.ctl
	  delay(1);
    if (irc == 0) {
       dpSet(s_dpCompletedGoSolenoidSafe,1); 
       dpSet(s_dpGotGoSolenoidSafe,0);
    }
    else DebugTN("spdEmergencyButton.ctl: goSolenoidSafe command returned ERROR code "+irc);
  }
} 
void spdSafe_ConnectRestoreFromDipoleSafe(string dp, bool b_dpRestoreFromDipoleSafe)
{
  if (b_dpRestoreFromDipoleSafe) {
    DebugTN("spdEmergencyButton.ctl: got restoreFromDipoleSafe command.");
	  dpSet(s_dpGotRestoreFromDipoleSafe,1);
    resetCompletedAction(s_dpCompletedGoDipoleSafe);
    resetCompletedAction(s_dpCompletedRestoreFromDipoleSafe);
    resetCompletedAction(s_dpCompletedGoSolenoidSafe);
    resetCompletedAction(s_dpCompletedRestoreFromSolenoidSafe);    
	  int irc = spdSafe_restoreFromDipoleSafe();//this function will be in spdSafe_SafeInstructions.ctl
	  delay(1);
    if (irc == 0) {
       dpSet(s_dpCompletedRestoreFromDipoleSafe,1); 
       dpSet(s_dpGotRestoreFromDipoleSafe,0);
    }
    else DebugTN("spdEmergencyButton.ctl: restoreFromDipoleSafe command returned ERROR code "+irc);
  }
}
void spdSafe_ConnectRestoreFromSolenoidSafe(string dp, bool b_dpRestoreFromSolenoidSafe)
{
  if (b_dpRestoreFromSolenoidSafe) {
    DebugTN("spdEmergencyButton.ctl: got restoreFromSolenoidSafe command.");
	  dpSet(s_dpGotRestoreFromSolenoidSafe,1);
    resetCompletedAction(s_dpCompletedGoDipoleSafe);
    resetCompletedAction(s_dpCompletedRestoreFromDipoleSafe);
    resetCompletedAction(s_dpCompletedGoSolenoidSafe);
    resetCompletedAction(s_dpCompletedRestoreFromSolenoidSafe);    
	  int irc = spdSafe_restoreFromSolenoidSafe();//this function will be in spdSafe_SafeInstructions.ctl
	  delay(1);
    if (irc == 0) {
       dpSet(s_dpCompletedRestoreFromSolenoidSafe,1); 
       dpSet(s_dpGotRestoreFromSolenoidSafe,0);
    }
    else DebugTN("spdEmergencyButton.ctl: restoreFromSolenoidSafe command returned ERROR code "+irc);
  }
}
void spdSafe_ConnectGoOff(string dp, bool b_dpGoOff)
{
  if (b_dpGoOff) {
    DebugTN("spdEmergencyButton.ctl: got goOff command.");
	  dpSet(s_dpGotGoOff,1);
    resetCompletedAction(s_dpCompletedGoOff);
	  int irc = spdSafe_goOff();//this function will be in spdSafe_SafeInstructions.ctl
	  //delay(1);
	  if (irc == 0) dpSet(s_dpGotGoOff,0);
    else DebugTN("spdEmergencyButton.ctl: goOff command returned ERROR code "+irc);
  }
}
