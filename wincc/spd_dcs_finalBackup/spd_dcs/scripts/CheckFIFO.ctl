#uses "FED_CNF.ctl"

main(){
  
  int RTchannel;
  int  error;
  int  RTreference = 0;

  dyn_int RTaddress,
        readRTarray,
        returnValue;
  
  int nErrors, output;

  string command = "CNF_RT_READ_REGISTER";
  DebugTN("starting FIFO check");
  while(1){
  for(int RouterNum=0; RouterNum<20; RouterNum++){
      
    RTchannel = RouterNum*6;
      
    RTaddress = 143;
    readRTarray=makeDynString(RTchannel,RTaddress,RTreference);
    SetFedDP(RTchannel,command,0,readRTarray);
    error = spdFed_WaitForStatus(RTchannel,command);
      
    if(error){
      DebugTN("Error while writing values to the channel "+RTchannel);
    }
    else{ 
      GetFedDP(RTchannel,command,returnValue);
      output = returnValue[1];
      //output += 65536;
      //DebugTN(output);
      if((output>>24)==21){
        nErrors = (output & 0xFF0000) >> 16;
        if(nErrors>0) {
          DebugTN("Output code = ",output);
          DebugTN("FIFO errors found in LRX0 of router "+RouterNum);
          if(getBit(nErrors,7)) DebugTN("CH1: FO ro FIFO underflow");
          if(getBit(nErrors,6)) DebugTN("CH1: FO ro FIFO overflow");  
          if(getBit(nErrors,5)) DebugTN("CH0: FO ro FIFO underflow");
          if(getBit(nErrors,4)) DebugTN("CH0: FO ro FIFO overflow"); 
          if(getBit(nErrors,3)) DebugTN("FO trigger FIFO underflow");
          if(getBit(nErrors,2)) DebugTN("FO trigger FIFO overflow");  
          if(getBit(nErrors,1)) DebugTN("Error code format problem. Bit 1 should be 0");
          if(getBit(nErrors,0)) DebugTN("Error code format problem. Bit 0 should be 0");  
        }
      }
    }
    
    RTaddress = 175;
    readRTarray=makeDynString(RTchannel,RTaddress,RTreference);
    SetFedDP(RTchannel,command,0,readRTarray);
    error = spdFed_WaitForStatus(RTchannel,command);
      
    if(error){
      DebugTN("Error while writing values to the channel "+RTchannel);
    }
    else{ 
      GetFedDP(RTchannel,command,returnValue);
      output = returnValue[1];
      //output += 65536;
      //DebugTN(output);
      if((output>>24)==21){
        nErrors = (output & 0xFF0000) >> 16;
        if(nErrors>0) {
          DebugTN("Output code = ",output);
          DebugTN("FIFO errors found in LRX1 of router "+RouterNum);
          if(getBit(nErrors,7)) DebugTN("CH1: FO ro FIFO underflow");
          if(getBit(nErrors,6)) DebugTN("CH1: FO ro FIFO overflow");  
          if(getBit(nErrors,5)) DebugTN("CH0: FO ro FIFO underflow");
          if(getBit(nErrors,4)) DebugTN("CH0: FO ro FIFO overflow"); 
          if(getBit(nErrors,3)) DebugTN("FO trigger FIFO underflow");
          if(getBit(nErrors,2)) DebugTN("FO trigger FIFO overflow");  
          if(getBit(nErrors,1)) DebugTN("Error code format problem. Bit 1 should be 0");
          if(getBit(nErrors,0)) DebugTN("Error code format problem. Bit 0 should be 0");  
        }
      }
    }
    
    RTaddress = 207;
    readRTarray=makeDynString(RTchannel,RTaddress,RTreference);
    SetFedDP(RTchannel,command,0,readRTarray);
    error = spdFed_WaitForStatus(RTchannel,command);
      
    if(error){
      DebugTN("Error while writing values to the channel "+RTchannel);
    }
    else{ 
      GetFedDP(RTchannel,command,returnValue);
      output = returnValue[1];
      //output += 65536;
      //DebugTN(output);
      if((output>>24)==21){
        nErrors = (output & 0xFF0000) >> 16;
        if(nErrors>0) {
          DebugTN("Output code = "+output);
          DebugTN("FIFO errors found in LRX2 of router "+RouterNum);
          if(getBit(nErrors,7)) DebugTN("CH1: FO ro FIFO underflow");
          if(getBit(nErrors,6)) DebugTN("CH1: FO ro FIFO overflow");  
          if(getBit(nErrors,5)) DebugTN("CH0: FO ro FIFO underflow");
          if(getBit(nErrors,4)) DebugTN("CH0: FO ro FIFO overflow"); 
          if(getBit(nErrors,3)) DebugTN("FO trigger FIFO underflow");
          if(getBit(nErrors,2)) DebugTN("FO trigger FIFO overflow");  
          if(getBit(nErrors,1)) DebugTN("Error code format problem. Bit 1 should be 0");
          if(getBit(nErrors,0)) DebugTN("Error code format problem. Bit 0 should be 0");  
        }
      }
    }    
  }
  //DebugTN("stopping");
  delay(10);
  }
}
