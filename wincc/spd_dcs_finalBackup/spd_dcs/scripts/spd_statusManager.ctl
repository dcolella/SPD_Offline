main(){
  
  DebugTN("spd_statusManager starting...");
  dpConnect("alignPITPhases_CB", FALSE, "dcs_globals:aliLhc_beamMode.value");
  
  
}

void alignPITPhases_CB(string dp, string lhcMode)
{
  
  string runState;  
  
  DebugTN("LHC Mode: "+lhcMode);
  if(lhcMode == "STABLE BEAMS"){
    
    dpGet("dcs_rct:aliDcsRctSpd.run_info.running", runState);

    if ((runState == "STARTING")||(runState == "STOPPING")||(runState == "RUNNING")) {      
      DebugTN("LHC Mode: STABLE BEAMS. SPD is RUNNING, TTCRx reset command not executed");      
    } else {        
      dpSet("spd_dcs:ltu.ToServer.Command", "ttcrxreset fee");
      DebugTN("LHC Mode: STABLE BEAMS. TTCRx reset command sent");    
    }
  }
 
  
}
