//script to test the VME communication by writing/reading some SPD router and link receiver registers
#uses "FED_CNF.ctl"

main(){
  DebugTN("Starting the tests");
  
  int        RTrouterNumber,
             error,
             RTchannel,
             RTreference;
  
  dyn_int    RTaddress,
             readRTarray,
             returnValue;
               
  string     commandReadRT="CNF_RT_READ_REGISTER";
  
  RTrouterNumber=4;
  RTaddress=1;  
  RTreference=0;
  
  RTchannel = RTrouterNumber*6;
  readRTarray=makeDynString(RTchannel,RTaddress,RTreference);
  
  int nTrials=200000;
  
  int prevValue=-1;
  bool testOK=true;
  
  DebugTN("Reading the router register #"+RTaddress+" "+nTrials+" times");
  for(int i=0; i<nTrials; i++){
    SetFedDP(RTchannel,commandReadRT,0,readRTarray); //rom  
    error = spdFed_WaitForStatus(RTchannel,commandReadRT);
    if(error){
      DebugTN("Error while writing values to the channel "+RTchannel);
      break;
    }
    else{ 
      GetFedDP(RTchannel,commandReadRT,returnValue);
      if(prevValue==-1){
        prevValue=returnValue[1];
      }
      else if(prevValue!=returnValue[1]){
        DebugTN("Read result changed. Old value:"+prevValue+", new value:"+returnValue[1]);
        testOK=false;
      }
    }
  }
  if(testOK){
    DebugTN("Router register read test passed");
  }
  else{
    DebugTN("Router register read test failed");
  }
  
  int        LRXchannel;
  
  uint       LRXaddress,
             LRXread,
             LRXwrite;
  
  dyn_uint readLRXarray,
           writeLRXarray;
  
  string commandReadLRX="CNF_LRX_READ_REGISTER";  
  string commandWriteLRX="CNF_LRX_WRITE_REGISTER";
  
  LRXchannel=24;
  
  LRXaddress=21;
  
  DebugTN("Reading the LRX register #"+LRXaddress+" "+nTrials+" times");
  
  prevValue=-1;  
  testOK=true;
  
  for(int i=0; i<nTrials; i++){
    readLRXarray[1]=LRXaddress; 
    SetFedDP(LRXchannel,commandReadLRX,0,readLRXarray);
    error = spdFed_WaitForStatus(LRXchannel,commandReadLRX);
    if(error){
      DebugTN("Error while writing values to the channel "+LRXchannel);
      break;
    }
    else{ 
      GetFedDP(LRXchannel,commandReadLRX,readLRXarray);
      if(prevValue==-1){
        prevValue=readLRXarray[1];
      }
      else if(prevValue!=readLRXarray[1]){
        DebugTN("Read result changed. Old value:"+prevValue+", new value:"+readLRXarray[1]);
        testOK=false;
      }
    }
  }
  if(testOK){
    DebugTN("LRX register read test passed");
  }
  else{
    DebugTN("LRX register read test failed");
  }
  
  LRXaddress=25; 
  
  LRXwrite=100;
  writeLRXarray[1]=LRXaddress;
  writeLRXarray[2]=LRXwrite;
  
  DebugTN("Writing and reading the LRX register #"+LRXaddress+" "+nTrials+" times");
  
  testOK=true;
  
  for(int i=0; i<nTrials; i++){
    SetFedDP(LRXchannel,commandWriteLRX,0,writeLRXarray);
    error = spdFed_WaitForStatus(LRXchannel,commandWriteLRX);    
    
    if(error){
      DebugTN("Error while writing values to the channel "+LRXchannel);
    }
    else{
      readLRXarray[1]=LRXaddress; 
      SetFedDP(LRXchannel,commandReadLRX,0,readLRXarray);
      error = spdFed_WaitForStatus(LRXchannel,commandReadLRX);
      if(error){
        DebugTN("Error while writing values to the channel "+LRXchannel);
        break;
      }
      else{ 
        GetFedDP(LRXchannel,commandReadLRX,readLRXarray);
        
        if(LRXwrite!=readLRXarray[1]){
          DebugTN("Error: written value:"+LRXwrite+", read value:"+readLRXarray[1]);
          testOK=false;
        }
      }
    }
  }
  if(testOK){
    DebugTN("LRX register write/read test passed");
  }
  else{
    DebugTN("LRX register write/read test failed");
  }
}
