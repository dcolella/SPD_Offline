#uses "spd_run/spdRunCU.ctl"
#uses "spdDefaultConfig.ctl"

AliDcsRun_initialize(string domain, string device)
{
}

int routerL1Limit()
{
  int error=0;
  int value = 1;
  string command;

  for (int Channel=0; Channel<=119; Channel++){
        SetFedDP(Channel, "W_RT_L1_IN_FIFO", 0, value);
        error = spdFed_WaitForStatus(Channel, command);
        if(error) return error = 1;
  }
  return error;
} 


checkL1L2(int channel)
{
  string command = "R_RT_STATUS_LINKRX3";
  
  SetFedDP(channel, command,0, 0);
  
  spdFed_WaitForStatus( channel, command);
  dyn_uint data;
      
  GetFedDP( channel, command, data);
  

  int l1Counter =  (data[1]/65536)%256;
  int l2Counter = (data[1]/16777216)%256;
  int difference = l1Counter-l2Counter;
  
  string dp;
  
  sprintf(dp,"spd_dcs:spdL1L2_HS%03d", channel);
  dpSet(dp+".L1Counter", l1Counter);
  dpSet(dp+".L2Counter", l2Counter);
  dpSet(dp+".L1L2Diff", difference);
  

}


AliDcsRun_valueChanged( string domain, string device,
      int state, string &fwState )
{
  if (state == 1) {
    fwState = "RUN_OK";
  } else if (state == 2) {
    fwState = "SOR_PROGRESSING";
  } else if (state == 3) {
    fwState = "EOR_PROGRESSING";
  } else if (state == 4) {
    fwState = "EOR_FAILURE";
  } else if (state == 5) {
    fwState = "SOR_FAILURE";
  } else if (state == 6) {
    fwState = "RUN_INHIBIT";
  } else if (state == 7) {
    fwState = "PAR_REQUEST";
  }
}



AliDcsRun_doCommand(string domain, string device, string command)
{
  int iResult;

  if (command == "SOR")	{
    string sActualState;
    anytype valueOf_run_no;
    anytype valueOf_run_type;
    anytype valueOf_ddl_list;
    anytype valueOf_sor_mode;
    
    dyn_string sorCommand = dpNames("*:*.*","spdSORConfiguration");
    dyn_bool isUsed;
    bool DummyBusyMaskHandling=FALSE;
    
    /*sorCommand = command defined in the DPtype "spdSORConfiguration"
    the list of command is:
    1 = LrxFastorL1Delay
    2 = autoConfigRouter
    3 = busyBoxReset
    4 = dataReset
    5 = hardwareSynch
    6 = hsDelaySetting
    7 = hsStatusConfig
    8 = resetDPI
    9 = routerHeaderSet
    
    */
    for (int i=1;i<=dynlen(sorCommand); i++) dpGet(sorCommand[i],isUsed[i]);
    DebugTN("Configuration list: ", sorCommand);
        
    fwDU_getCommandParameter(domain, device, "run_no", valueOf_run_no);
    dpSet(device+".runNo",valueOf_run_no);
    fwDU_getCommandParameter(domain, device, "run_type", valueOf_run_type);
    dpSet(device+".runType",valueOf_run_type);
    dpSet(device+".state",2); // ---- SOR_PROG
    fwDU_getCommandParameter(domain, device, "ddl_list", valueOf_ddl_list);
    DebugTN("List of DDLs : ", valueOf_ddl_list); 
    fwDU_getCommandParameter(domain, device, "sor_mode", valueOf_sor_mode);
    DebugTN("SOR mode = ", valueOf_sor_mode);
                
    ///////////////////////////////////////////////////////////////////////////
    //WRITE HERE YOUR CODE TO BE EXECUTED FOR BEGIN OF RUN (BOR)
    //YOU MAY USE valueOf_run_no AND valueOf_run_type FOR RUN INFORMATION
                
     bit32 options = 0;
   
      // reset router, write control reg and L0L1 time are all set if the autoconfigure option is set
     if (isUsed[2]){
       options = 7;
     }
     
     if(isUsed[1]){
       setBit( options, 3, true);
     }
     
     DebugTN("Options config = ",options);

     if( isUsed[2]){
       //iResult = spdRunCU_autoConfigRouter_all(3,valueOf_run_type); // reset and config Router in the list     
       iResult = spdRunCU_autoConfigRouter_all(11,valueOf_run_type); // reset and config Router in the list modified the option to write FastOR delays    
       DebugTN("1) spdRunCU_autoConfigRouter_all executed. Result = ",iResult);
     }
     else DebugTN("1) spdRunCU_autoConfigRouter_all skipped.");
     
    //sending FEE RESET
//     dpSet("spd_dcs:ltu.ToServer.Command","fee");    
//     DebugTN("FEE reset command sent");
    //int delayValue;
    //dpGet("spd_dcs:delayBeforeResetDPI.setting",delayValue);
    //DebugTN("Waiting "+delayValue+" seconds before next operation");
    //delay(delayValue); //delay to allow execution of the previous command.
//     bool PITphasesOK=false;
//     while(!PITphasesOK){
//       DebugTN("Checking PIT phases alignment");
//       dpGet("pit_dcs:pitPhasesAligned.status",PITphasesOK);
//       delay(0,200);
//     }
//     DebugTN("PIT phases are aligned. Continuing SOR.");
     
     if(!iResult & isUsed[6] & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_hsDelaySetting(valueOf_run_type);// the HS delay (misc/delay control) is changed accordingly with the run_type    
       DebugTN("2) spdRunCU_hsDelaySetting executed. Result = ",iResult);
     }
     else DebugTN("2) spdRunCU_hsDelaySetting skipped");
     /*
     for(int i=0; i<120; i++){
       JtagReset(i);
     }     
     */
     
     if(!iResult & isUsed[8]){
       iResult = spdRunCU_resetDPI(); // reset DPI for Hs in the list    
       DebugTN("3) spdRunCU_resetDPI executed. Result = ",iResult);
       if(iResult){
         DebugTN("3) spdRunCU_resetDPI failed once. Repeating.");
         iResult = spdRunCU_resetDPI();
         DebugTN("3) spdRunCU_resetDPI executed. Result = ",iResult);
       }
     }
     else DebugTN("3) spdRunCU_resetDPI skipped");
     /*
     iResult = spdRunCU_resetDPI();     
     DebugTN("3) spdRunCU_resetDPI executed. Result = ",iResult);
     
     iResult = spdRunCU_resetDPI();     
     DebugTN("3) spdRunCU_resetDPI executed. Result = ",iResult);
     */
     //DebugTN("iResult & isUsed[8]="+(iResult & isUsed[8]),iResult,isUsed[8]);    
     //DebugTN("!iResult & isUsed[8]="+(!iResult & isUsed[8]),!iResult,isUsed[8]);  
     
     if(!iResult & isUsed[4] & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_dataReset(); // Data reset for Hs in the list        
       DebugTN("4) spdRunCU_dataReset executed. Result = ",iResult);
     }
     else DebugTN("4) spdRunCU_dataReset skipped");
     
     if(!iResult & isUsed[3]){
       iResult = spdRunCU_busyBoxReset(); // reset fanInFanOut board          
       DebugTN("5) spdRunCU_busyBoxReset executed. Result = ",iResult);   
     }
     else DebugTN("5) spdRunCU_busyBoxReset skipped");
     
     if(isUsed[5] & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_HardwareSynch();  // sychronize the Hs list and router ch in the readout with the Hs powered          
       DebugTN("6) spdRunCU_HardwareSynch executed. Result = ",iResult);     
     }
     else DebugTN("6) spdRunCU_HardwareSynch  skipped");
     
     if(!iResult & isUsed[7] & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_hsStatusConfig(valueOf_run_type); //the HS status (on, test) is changed accordingly with the run_type     
       DebugTN("7) spdRunCU_hsStatusConfig executed. Result = ",iResult); 
     }
     else DebugTN("7) spdRunCU_hsStatusConfig skipped");
     
     if(!iResult & isUsed[9] & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_routerHeaderSet(valueOf_run_type);// the router header is changed accordingly with the run_type     
       DebugTN("8) spdRunCU_routerHeaderSet executed. Result = ",iResult);    
     }
     else DebugTN("8) spdRunCU_routerHeaderSet skipped");
     
     // here we set all the settings of the control reg, so if any of these options is chosen we need a last autoconfigureRouter
     //if(!iResult & (isUsed[1] || isUsed[2] || isUsed[5] || isUsed[9])) iResult = spdRunCU_autoConfigRouter_all(15,valueOf_run_type); // reset and config Router in the list
     //if(!iResult) spdRunCU_LrxDelaySetting(); // Set the default delay in the lrx for the clk and serial optical link
     
     if(!iResult & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_LrxForStrobeSetting(); // Set FOR strobe in the lrx. The default is retreived from the DP    
       DebugTN("9) spdRunCU_LrxForStrobeSetting executed. Result = ",iResult);
     }
     else DebugTN("9) spdRunCU_LrxForStrobeSetting skipped");
     
     if(!iResult & valueOf_sor_mode=="FULL"){
       iResult = spdRunCU_routerTimeoutReadyEvent(); // Set the timeout in the router. The default is retreived from the DP    
       DebugTN("10) spdRunCU_routerTimeoutReadyEvent executed. Result = ",iResult);
     }
     else DebugTN("10) spdRunCU_routerTimeoutReadyEvent skipped");
     
     if(!iResult){
       spdRunCU_startOfRun((int) valueOf_run_no); //Set the run number in the FED    
       DebugTN("11) spdRunCU_startOfRun executed. Result = ",iResult);
     }
     else DebugTN("11) spdRunCU_startOfRun skipped");
     
     if(!iResult){
       iResult = spdRunCU_adjustL0L1delay();
       DebugTN("12) spdRunCU_adjustL0L1delay executed. Result = ", iResult);
     }
     else DebugTN("12) spdRunCU_adjustL0L1delay skipped");
     
     spdRunCU_storeRunParameters((int) valueOf_run_no,valueOf_run_type); //Store the SPD run settings in the archived dp
     //string specialChannelStatus = spdGetStatusOfChannel(48);
     //if (specialChannelStatus != "off")  { 
        //DebugTN("Special setting ...............................", specialChannelStatus);
       // spdRunCU_dataFormatOn(48);// special setting for the 8A0
     //}
 
    //setting the busymask
    //two algorithms
    //1) using single mask for A and C sides
/*     
    bit32 BusyMask=0;
    dyn_anytype DDL_IDs;   
    dyn_anytype DDL_list_entries;  
    
    dpGet("spd_dcs:DDL_IDs.",DDL_IDs);
    DDL_list_entries = strsplit(valueOf_ddl_list,",");
    
    int NofDDLs = dynlen(DDL_IDs);
    int NofDDL_in_list = dynlen(DDL_list_entries);
    
    for(int j=0; j<NofDDL_in_list; j++){
      for(int i=0; i<NofDDLs; i++){
        if (DDL_IDs[i+1]==DDL_list_entries[j+1]){
          setBit(BusyMask,i,1);
        }
      }
    }
    
    DebugTN("BusyMask = ",BusyMask);
    
    SetFedDP(0,"CNF_DDL_LIST",0,BusyMask);   
    SetFedDP(60,"CNF_DDL_LIST",0,BusyMask>>10);  
    SetFedDP(0,"W_BSYCD_BUSYMASK",0,BusyMask);   
    SetFedDP(60,"W_BSYCD_BUSYMASK",0,BusyMask>>10);
*/    
    
    //2) using separate masks for A and C side
    bit32 BusyMaskA=0;
    bit32 BusyMaskC=0;
    
    dyn_anytype  DDL_IDs_A, DDL_IDs_C;  
    dyn_anytype DDL_list_entries; 
    
    dpGet("spd_dcs:DDL_IDs_A.",DDL_IDs_A);
    dpGet("spd_dcs:DDL_IDs_C.",DDL_IDs_C);
    
    int NofDDLs_A = dynlen(DDL_IDs_A);
    int NofDDLs_C = dynlen(DDL_IDs_C);
    
    DDL_list_entries = strsplit(valueOf_ddl_list,",");
    int NofDDL_in_list = dynlen(DDL_list_entries);
    
    bool found=0;
    
    for(int j=0; j<NofDDL_in_list; j++){
      found=0;
      for(int i=0; i<NofDDLs_A; i++){
        if (DDL_list_entries[j+1]==DDL_IDs_A[i+1]){
          setBit(BusyMaskA,i,1);
          found=1;
          break;
        }
      }
      if(found) continue;
      for(int i=0; i<NofDDLs_C; i++){
        if (DDL_list_entries[j+1]==DDL_IDs_C[i+1]){
          setBit(BusyMaskC,i,1);
          break;
        }
      }
    }
    
    DebugTN("BusyMask for A-side = ",BusyMaskA);
    DebugTN("BusyMask for C-side = ",BusyMaskC);
    
    if(!DummyBusyMaskHandling){
      SetFedDP(0,"CNF_DDL_LIST",0,BusyMaskA);   
      SetFedDP(60,"CNF_DDL_LIST",0,BusyMaskC);  
      SetFedDP(0,"W_BSYCD_BUSYMASK",0,BusyMaskA);   
      SetFedDP(60,"W_BSYCD_BUSYMASK",0,BusyMaskC);
    }
    else{
      DebugTN("BusyMasks aren't sent to equipment! Check DummyBusyMaskHandling setting");
    }
    
    //sending FEE RESET - commented out here as it's executed after the AutoConfig routers.
    dpSet("spd_dcs:ltu.ToServer.Command","fee");    
    DebugTN("FEE reset command sent");
       
    if(!iResult){ 
      DebugTN("Run control DU: Configuration done ! ");
      DebugTN("iResult ! ", iResult);
      dpSet(device+".state",1);  // ---- RUN_OK
    }
    else {
      DebugTN("Run control DU: error found while configuring ! ");
      DebugTN("iResult ! ", iResult);
      dpSet(device+".state",5);  // ---- Reset & config Failure
    }
    
    //END OF YOUR BOR CODE
    ///////////////////////////////////////////////////////////////////////////                
    delay(1); //wait for status update .... 
    //fwDU_getState(domain, device, sActualState);
    //if(sActualState != "SOR_FAILURE" && sActualState != "EOR_FAILURE")
    //  dpSet(device+".state",1);  // ---- RUN_OK              
  }
  if (command == "EOR")	{
    string sActualState;
    anytype valueOf_run_no;
    anytype valueOf_run_type;
    anytype valueOf_eor_mode;
    
    fwDU_getCommandParameter(domain, device, "run_no", valueOf_run_no);
    dpSet(device+".runNo",valueOf_run_no);
    fwDU_getCommandParameter(domain, device, "run_type", valueOf_run_type);
    dpSet(device+".runType",valueOf_run_type);
    dpSet(device+".state",3); // ---- EOR_PROG
    fwDU_getCommandParameter(domain, device, "eor_mode", valueOf_eor_mode);
    DebugTN("EOR mode = ", valueOf_eor_mode);
                
    ///////////////////////////////////////////////////////////////////////////
    //WRITE HERE YOUR CODE TO BE EXECUTED FOR END OF RUN (EOR)

    
    DebugTN("Run control DU: Execute the de-configuration for:"+valueOf_run_type); 
    //string specialChannelStatus = spdGetStatusOfChannel(48);    
    //if (specialChannelStatus != "off")  { 
    //  spdRunCU_dataFormatOff(48); // special setting for the 8A0 needed to establish the jtag access
    //}
    if(valueOf_eor_mode!="COMPLETE_FAST") spdRunCU_endOfRun(valueOf_run_type); 

    dyn_int channels,alignResult; 
    int alignedChip, notCheckedChip, toBeChecked;
    
    if(valueOf_eor_mode=="FULL" || valueOf_eor_mode=="COMPLETE_FAST"){
      //to see the Half-staves used in the scan
      channels = makeDynInt(30);
      //channels = makeDynInt(0);
      //channels = makeDynInt(0,42,55,56,103,114); tests done to understand the FastOR vs Hits mismatch.
      //2015.05.08 - SS - commented out to avoid the reinclusion of the HSs removed by the operator.
      //spdRunCU_channelsToAlign(channels);   //this should be enabled to have MEB alignment working
    }
    
    if(valueOf_eor_mode=="FULL" || valueOf_eor_mode=="COMPLETE_FAST") spdRunCU_autoConfigRouter_all(3,"STANDALONE");

     // function to align the MEB in the selected channels
     // param1 = 0 to check the alignment, 1 to check and correct
     // param2 = 0 to select L2Reject, 1 to select L1
     // results(1200) = -1 chip not checked, 0 chip aligned, (numebr) shift detected
     // if return -100 or -200, the function returns an error
    if(valueOf_eor_mode=="FULL" || valueOf_eor_mode=="COMPLETE_FAST") alignResult = spdRunCU_mebBufferAlign(1, 0);

    for(int chipNum =0; chipNum < dynlen(alignResult); chipNum++){
      if(alignResult[chipNum+1] > 0) {
        DebugTN("Chip non allineato :" + chipNum);
        toBeChecked++;
      }
    }
    // 2015.05.08 - SS - commented out to avoid the reinclusion of the HSs removed by the operator.
    //if(valueOf_eor_mode=="FULL" || valueOf_eor_mode=="COMPLETE_FAST") spdRunCU_HardwareSynch();    
    
    if(toBeChecked) {
      sActualState = "EOR_FAILURE";    
      fwDU_setState(domain, device,sActualState);
    }

                
    //END OF YOUR EOR CODE
    ///////////////////////////////////////////////////////////////////////////                
                
    fwDU_getState(domain, device, sActualState);
    if(sActualState != "SOR_FAILURE" && sActualState != "EOR_FAILURE")
     dpSet(device+".state",1);  // ---- RUN_OK                 
  }
  if (command == "RESET") {
    dpSet(device+".state",6); // ---- EOR_PROG
    
  }
  if (command == "INHIBIT_RUN") {
    dpSet(device+".state",6); //
    
  }
  if (command == "ALLOW_RUN") {
    dpSet(device+".state",1); //
    
  }

  if (command == "REQUEST_PAR")	{    
    dpSet(device+".state",7); //
    
  }    
    
  if (command == "RECOVER")	{ //PAR recovery procedure 

    int iResult;  
    int error;  
    string sActualState;
    anytype valueOf_run_no;
    anytype valueOf_run_type;
    anytype valueOf_ddl_list;
    anytype valueOf_sor_mode;  
  
    DebugTN("PAR recovery procedure starting...");
    dpGet("spd_dcs:spdRunParameters.runType", valueOf_run_type);   
 
//  ---> commented on 18.09.2017    
//     delay(1);
    // router reset (including LinkRx reset)
//     for(int routCh = 0; routCh < 20; routCh++){ 
//       if(isRouterReady(routCh)){    
//         resetRouter(routCh);
//         error = configRouter(routCh);   
//         DebugTN("resetRouter executed. Error = ", error);   
//       }
//     }

    delay(1);
    // reset and config Router in the list modified the option to write FastOR delays 
    iResult = spdRunCU_autoConfigRouter_all(11, valueOf_run_type);    
    DebugTN("spdRunCU_autoConfigRouter_all executed. Result = ", iResult);  
  
    if(iResult){ //spdRunCU_autoConfigRouter_all ended with error
      
      fwCU_sendCommand("SPD_DCS", "ABORT_PAR");
      delay(2);
      dpSet(device+".state",1);
      DebugTN("PAR recovery procedure finished: NOT OK");
    
    }else{
  
      delay(1);
      // reset DPI for Hs in the list
      iResult = spdRunCU_resetDPI();     
      DebugTN("spdRunCU_resetDPI executed. Result = ", iResult);
      if(iResult){
        DebugTN("spdRunCU_resetDPI failed once. Repeating.");
        iResult = spdRunCU_resetDPI();
        DebugTN("spdRunCU_resetDPI executed. Result = ", iResult);
      }  
  
      delay(1);
      // Data reset for Hs in the list
      iResult = spdRunCU_dataReset(); // Data reset for Hs in the list        
      DebugTN("spdRunCU_dataReset executed. Result = ", iResult);    
       
      //delay(1);
      //iResult = spdRunCU_autoConfigRouter_all(11,valueOf_run_type); // reset and config Router in the list modified the option to write FastOR delays    
      //DebugTN("spdRunCU_autoConfigRouter_all executed (2nd). Result = ",iResult);      
       
//  ---> commented on 18.09.2017          
//       delay(10);    
      //Mask noisy pixels
//       SetFedDP( 0, "HSCNF_DB_MASK", 0, -1, 10000);
//       SetFedDP( 60, "HSCNF_DB_MASK", 0, -1, 10000);
//       DebugTN("Noisy pixels masking done");         
      
      delay(5);    
      //FEE reset 
      dpSet("spd_dcs:ltu.ToServer.Command","fee");
      DebugTN("FEE reset command sent");
//       int delayValue;
//       dpGet("spd_dcs:delayBeforeResetDPI.setting",delayValue);
//       delay(delayValue); //delay to allow execution of the previous command.
//       bool PITphasesOK = false;
//       while(!PITphasesOK){
//         DebugTN("Checking PIT phases alignment");
//         dpGet("pit_dcs:pitPhasesAligned.status",PITphasesOK);
//         delay(0,200);
//       }    
    
      delay(2);      
      dpSet(device+".state",1); // ---- RUN_OK
      DebugTN("PAR recovery procedure finished: OK");
    }    
  }

  
}


