FwCaVLoop_initialize(string domain, string device)
{
}


FwCaVLoop_valueChanged( string domain, string device,
      int Actual_dot_status, string &fwState )
{
      int ModbusDriver = 15;
      dyn_uint dui;
      int monitorOnMaintenance = 0;
      dpGet("_Connections.Driver.ManNums:_online.._value", dui);  
    
      if(dpExists("_aliDcsCavFsm.settings.monitorOnMaintenance"))
          dpGet("_aliDcsCavFsm.settings.monitorOnMaintenance",monitorOnMaintenance );
    
      if(monitorOnMaintenance)
      {
        if(Actual_dot_status>=256)
          Actual_dot_status = Actual_dot_status - 256;
      }
      
      OpenLoopT(device);
    
      if(Actual_dot_status & 4096) 
	      fwState = "ERROR"; 
      else if( !isDriverRunning(dui, ModbusDriver))
              fwState = "NO_CONTROL";  
      else if(Actual_dot_status & 256) 
		fwState = "MAINTENANCE"; 
//      else if(Actual_dot_status & 512) 
//	      fwState = "WA_REPAIR"; 
//      else if(Actual_dot_status & 32) 
//		fwState = "LEAK_SEARCH"; 
      else 
       switch(Actual_dot_status) 
       { 
        case 1: 
		fwState = "OFF"; 
            break;        
        case 2: 
		fwState = "STANDBY"; 
            break;        
        case 4: 
		fwState = "ON"; 
            break;        
        case 8: 
		fwState = "LOCKED"; 
            break;        
        case 16: 
		fwState = "PURGED"; 
            break;        
        default: 
		fwState = "ERROR"; 
            break;        
       } 
}

void OpenLoopT(string device)
//open a loop with a delay depending on the loop number.
{
  string sDelayDp = dpSubStr(device,DPSUB_SYS)+"_aliDcsCavFsm.settings.loopOpenDelaySeconds";
  int iLoopNumber = 0;
  int iDelaySeconds = 0;
  DebugN(device);
  if(dpExists(sDelayDp))
  {
    dpGet(sDelayDp,iDelaySeconds);
    iLoopNumber = substr(device,strlen(device)-2);
    DebugN("device no. " + iLoopNumber + " opening will be delayed " +  iDelaySeconds*iLoopNumber + " s");        
  }
  delay(iDelaySeconds);
  //dpSet(device+".Settings.control",4);
}

FwCaVLoop_doCommand(string domain, string device, string command)
{
  string timeout_state;
  dyn_string loopDevices;
  int timeout_seconds;
  int current_command;
  int i_openDelaySeconds=0;
  int i_loopIndex;
 
		anytype valueOf_loopList;
                int i_deviceNumber;
                float f_deviceNumber1, f_deviceNumber2;
                dyn_float di_deviceNumbers;
                int i;
                int dev_control;
                bool deviceInList;
  
  fwDU_getState(domain, device, timeout_state);
 
        
	if (command == "GO_OFF") 
	{ 
		dpSet(device+".Settings.control",1); 
	} 
	if (command == "GO_STANDBY") 
	{ 
		dpSet(device+".Settings.control",2); 
	} 
	if (command == "GO_ON")
	{
		dpSet(device+".Settings.control",4);
	}
	if (command == "LOCK")
	{
		dpSet(device+".Settings.control",8);
	}
	if (command == "ALLOW_MAINTENANCE")
	{
		dpGet(device+".Settings.control",current_command);
		dpSet(device+".Settings.control",current_command+256);
	}
	if (command == "EXIT_MAINTENANCE")  
	{  
		dpGet(device+".Actual.status",current_command);  
                if(current_command>256)
  		  dpSet(device+".Settings.control",current_command-256);
                else
  		  dpSet(device+".Settings.control",1);
	}  
	if (command == "LOCK_LOOP" || command == "OPEN_LOOP" || command == "CLOSE_LOOP")
	{
                
                //extract loop numbers from the list and compare with device loop number
		fwDU_getCommandParameter(domain, device, "loopList", valueOf_loopList);
                deviceInList = false;
                if(strtolower(valueOf_loopList)!="all") 
                {                           
                  di_deviceNumbers = strsplit(valueOf_loopList,",");
                  f_deviceNumber1 = substr (device, strlen(device)-2);
                  i_deviceNumber = f_deviceNumber1;
                  i_loopIndex = dynContains(di_deviceNumbers, f_deviceNumber1);
                  if( i_loopIndex )
                  {
                      deviceInList = true;                
                  }
                }
                else// parameter is null, then all loops are by default selected
                {
                  deviceInList = true;
                  i_loopIndex = -1;
                }
                if(deviceInList) //if device number is in the list, st it LOCKED     
                {            
                    switch(command)
                    {
                       case "LOCK_LOOP":
         		dpSet(device+".Settings.control",8);
                        break;
                       case "OPEN_LOOP":
         		i_openDelaySeconds = OpenLoop(device,i_loopIndex);
                        break;
                       case "CLOSE_LOOP":
         		dpSet(device+".Settings.control",1);
                        break;
                    }
                }                         
                else //otherwise, don't do anything
                {
         		//dpGet(device+".Settings.control",dev_control);
        		//dpSet(device+".Settings.control",dev_control);
                        fwDU_setState(domain, device, timeout_state);
                }
	}
                      
        //load plant timeout
	loopDevices = dpNames("*", "FwCaVPlant");                        
	if(dynlen(loopDevices))
        {
          dpGet(loopDevices[1]+".Actual.timeout",timeout_seconds);
          if (timeout_seconds<1)
            {   
              timeout_seconds = 59;
            }
        }
        else
        {
          timeout_seconds = 61;
        }
        
  //DebugTN(device + ": Waiting for Timeout (" + timeout_seconds + "s) before updating the command. Otherwise go to " + timeout_state);        
  if(deviceInList)
    fwDU_startTimeout(timeout_seconds+5+i_openDelaySeconds, domain, device, "NO_CONTROL");// timeout_state);      
}

int OpenLoop(string device, int index)
//open a loop with a delay depending on the index. If index = -1, then the index is computed from the loop hw number.
{
  string sDelayDp = dpSubStr(device,DPSUB_SYS)+"_aliDcsCavFsm.settings.loopOpenDelaySeconds";;
  int iLoopNumber = 0;
  int iDelaySeconds = 0;
  if(dpExists(sDelayDp))
  {
    dpGet(sDelayDp,iDelaySeconds);
    if(index == -1)
    {
      DebugN("Loop Index is compued with device number");
      iLoopNumber = substr(device,strlen(device)-2);
      if(iLoopNumber==0) iLoopNumber = substr(device,strlen(device)-1);
      iLoopNumber--;//usually loop numbers start from 1.
    }
    else
    {
      DebugN("Loop Index is computed from device list");
      iLoopNumber = index-1;//dyn string index starts from 1.
    }
    iDelaySeconds=iDelaySeconds*iLoopNumber;
    DebugN("Loop Index: " +iDelaySeconds);
    if(iDelaySeconds<0) iDelaySeconds = 0;
    DebugN("device no. " + iLoopNumber + " opening will be delayed " +  iDelaySeconds + " s");        
  }
  delay(iDelaySeconds);
  dpSet(device+".Settings.control",4);
  return iDelaySeconds;
}

