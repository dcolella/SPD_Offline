AliEmergencyButton_DU_initialize(string domain, string device)
{
}

AliEmergencyButton_DU_valueChanged( string domain, string device,
      bool Actual_dot_isSuperSafe,
      bool Actual_dot_movingSuperSafe,
      bool Actual_dot_movingSafe,
      bool Actual_dot_isSafe, string &fwState )
{
	if (
	(Actual_dot_isSuperSafe == 1) &&
	(Actual_dot_movingSuperSafe == 0) &&
	(Actual_dot_movingSafe == 0) )
	{
		fwState = "SUPERSAFE";
	}
	else if (
	(Actual_dot_isSafe == 1) &&
	(Actual_dot_movingSafe == 0) &&
	(Actual_dot_movingSuperSafe == 0) )
	{
		fwState = "SAFE";
	}
	else if (Actual_dot_movingSafe == 1)
	{
		fwState = "MOVING_SAFE";
	}
	else if (Actual_dot_movingSuperSafe == 1)
	{
		fwState = "MOVING_SUPERSAFE";
	}
	else 
	{
		fwState = "NOT_SAFE";
	}
}


AliEmergencyButton_DU_doCommand(string domain, string device, string command)
{
	if (command == "RESTORE_SAFE")
	{
		dpSet(device+".Actions.restoreSafe",1);
	}
	if (command == "GO_SUPERSAFE")
	{
		dpSet(device+".Actions.goSuperSafe",1);
	}
	if (command == "GO_SAFE")
	{
		dpSet(device+".Actions.goSafe",1);
	}
}


