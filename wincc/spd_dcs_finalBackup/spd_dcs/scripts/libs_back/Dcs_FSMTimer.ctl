Dcs_FSMTimer_initialize(string domain, string device)
{
  string sDpName;
  
  sDpName = "dcsFsmTimer_"+device;
  
  // verify if the TimedDp exists
  if(!dpExists(sDpName)) {
    if(dpCreate(sDpName, "_TimedFunc") == -1){
      DebugTN("Dcs_FSMTimer_initialize() : Error creating Dp !("+sDpName+")");
      return;
    } 
  } 

  // Then Set the Timed DP
  dpSet(sDpName+".validFrom", makeTime(2030,1,1,1,1,1),
        sDpName+".validUntil", 0,
        sDpName+".time", makeDynInt(),        
        sDpName+".interval", 0,
        sDpName+".syncTime", -1);
  
  // Don't work :-(
  // fwDU_setParameter(domain, device, "TIMEDDP", sDpName);
  
  // Arm the function
  timedFunc("Dcs_FSMTimer_TimedFunction",sDpName); //timedFunc 

  // set the device
  dpSet(device+".status",0,  // Set READY state
        device+".preset",0,
        device+".timedDp",sDpName);

  DebugTN("Dcs_FSMTimer_initialize() : register TIMEDDP="+sDpName);
    
      
}


Dcs_FSMTimer_TimedFunction(string dp, time before, time now, bool call)
{
  string sDpName;
  sDpName = substr(dp,12);

  if(before == 0) return;  // this is the first call
  
  //DebugTN(">-ssss->",dp,sDpName);  
  
  // This terminate the count  
  dpSet(dp+".validFrom", makeTime(2030,1,1,1,1,1),
        sDpName+".status",2,    // Set END count state
        sDpName+".preset",0);
  
}


Dcs_FSMTimer_valueChanged( string domain, string device,
      int status, string &fwState )
{
  switch(status) {
    case 0:
      fwState = "READY";
      break;
    case 1:      
      fwState = "WAIT";
      break;
    case 2:      
      fwState = "END";
      break;
  }
}


Dcs_FSMTimer_doCommand(string domain, string device, string command)
{
  string value;
  string dpname;
  string userData;
  
  // don't work with the FSM param ... :-(
  // fwDU_getParameter(domain, device, "TIMEDDP", dpname);
  
  // Take the Timed DPName
  dpGet(device+".timedDp",dpname);
  
  switch(command) {
    case "START":  // Start the counter
      // the command accept the param DELAY (seconds)
      fwDU_getCommandParameter(domain, device, "DELAY", value);
      fwDU_getCommandParameter(domain, device, "SPECIFIC", userData);
      
      // Then set the Timed function
      dpSet(device+".status",1,   // Wait State
            device+".preset",(int)(value),
            device+".action",1,
            device+".userData",userData,
            dpname+".validFrom",0,  // start the timer
            dpname+".interval",(int)(value) );
      fwDU_setParameter(domain, device, "USER_DATA", userData);
      break;
    case "RESET": // Reset the counter
      dpSet(device+".status",0,  // READY state
            device+".preset",0,
            device+".action",9,
            dpname+".validFrom",makeTime(2030,1,1,1,1,1),  // stop the timer
            dpname+".interval",0);
      break;
    case "ACKNOLEDGE": // Acknoledge the end of Counting
      dpSet(device+".status",0,  // READY state
            device+".action",2,            
            device+".preset",0);
      break;
  }     
}

