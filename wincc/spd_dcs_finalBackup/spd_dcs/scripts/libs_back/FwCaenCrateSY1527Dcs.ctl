#uses "dcsCaen/dcsCaen.ctl" 

FwCaenCrateSY1527Dcs_initialize(string domain, string device)
{
const string PREFIX = "hmp";
const int TIMED_FUNCTION_TIME=2; // Seconds for the recall of function time
 
// Create and set the Timed Function dp   
// and link the timer variable to the CB function for a period of 5 second  
dyn_errClass err;  
  
if (!dpExists(PREFIX+"Caen"+device+"_timer"))   
{  
  dpCreate(PREFIX+"Caen"+device+"_timer", "_TimedFunc");  
  err = getLastError();  
  if (dynlen(err) > 0) errorDialog(err);  
}   
 
dpSetWait(getSystemName() +PREFIX+"Caen"+device+"_timer.validFrom:_original.._value",0,  
          getSystemName() +PREFIX+"Caen"+device+"_timer.validUntil:_original.._value",0,  
          getSystemName() +PREFIX+"Caen"+device+"_timer.time:_original.._value",makeDynInt(),  
          getSystemName() +PREFIX+"Caen"+device+"_timer.interval:_original.._value",TIMED_FUNCTION_TIME,  
	  getSystemName() +PREFIX+"Caen"+device+"_timer.syncTime:_original.._value",-1);  

//timedFunc("dcsCaen_CratesTimedControlCB",getSystemName()+PREFIX+"Caen"+device+"_timer");  
err = getLastError();  
if (dynlen(err) > 0) errorDialog(err);  
  
DebugTN("dcsCAEN FwCaenCrateSY1527Dcs_initialize :"+domain+" "+device+"  Timed Function installed");  
  
 
}

FwCaenCrateSY1527Dcs_valueChanged( string domain, string device,
      string Information_dot_SwRelease , string &fwState)
{
  string st;  
  bool bInvalid = false;
  string sPreviousState;
  
  dpGet(device+".Information.SwRelease:_original.._aut_inv", bInvalid);
  if (bInvalid) 
  {
    fwState = "NO_CONTROL";  
    st = fwState; 
  } else if(Information_dot_SwRelease == "LOST_CONTROL") 
  { 
    fwState = "NO_CONTROL";  
    st = fwState; 
  } else if(Information_dot_SwRelease == "KILL")  
  {  
    fwState = "WA_REPAIR";   
    st = fwState;  
  } else if(Information_dot_SwRelease == "CLEAR")  
  {  
    fwState = "WA_REPAIR";   
    st = fwState;  
  } else  
  {  
    fwDU_getState(domain,device,sPreviousState);
    if(sPreviousState == "INTERLOCK_WENT") {
      fwState = "INTERLOCK_WENT";  
      return;
    }    
    st = dcsCaen_CrateControlStatus(dpSubStr(device, DPSUB_SYS_DP));  
    if (sPreviousState != st)   
    {  
      switch(st)  {  
        case "READY":
          if( sPreviousState == "INTERLOCK")
            fwState = "INTERLOCK_WENT";  
          else
            fwState = "ON";  
          break;  
        case "EXTERNAL_INTERLOCK":  
        case "EXTERNAL_KILL":  
          fwState = "INTERLOCK";  
          break;  
        case "ERROR_AC_FAILURE":  
        case "ERROR_FAN_FAILURE":  
          fwState = "PWS_FAULT";  
          break;  
        case "WARNING_AC_FAILURE":  
        case "WARNING_FAN_FAILURE":  
          fwState = "ER_REPAIR";   
          break;   
        case "WARNING_TEMPERATURE":  
          fwState = "WA_REPAIR";  
          break;  
        default:  
          fwState = "ON";  
          break;  
      }   
    } 
  }  
}

FwCaenCrateSY1527Dcs_doCommand(string domain, string device, string command)
{ 
  device = dpSubStr(device, DPSUB_SYS_DP);  
  switch(command) {
    case "ACKNOLEDGE":
      fwDU_setState(domain,device,"ON");
      break;
    case "RESET":
      dpSetWait(device+".Commands.ClearAlarm:_original.._value",true); 
      dpSetWait(device+".Information.SwRelease:_original.._value","CLEAR");   
//    dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|I|H|"+"Clear Alarm command send to Crate:"+device+"|");  
      break;
    case "KILL":
      dpSetWait(device+".Commands.Kill:_original.._value",true);   
      dpSetWait(device+".Information.SwRelease:_original.._value","KILL");   
//    dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|I|H|"+"KILL ALL command send to Crate:"+device+"|");  
      break;  
  }
}

