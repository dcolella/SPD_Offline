main()
{
// edit the datapoint type name according to your needs!
  string sDpt = "AliEmergencyButton";
  change_dpt(sDpt);
}
//===================================================//
void change_dpt(string sdpt)
{
  dyn_dyn_string dptEmergency;
  dyn_dyn_int dptEmergencyStruct;
 
  //create data type
  dptEmergency[1] = makeDynString (sdpt,"","","");
  dptEmergency[2] = makeDynString ("","Actions","","","");
  dptEmergency[3] = makeDynString ("","","goSafe","","");
  dptEmergency[4] = makeDynString ("","","goSuperSafe","","");
  dptEmergency[5] = makeDynString ("","","_goSafeScript","");
  dptEmergency[6] = makeDynString ("","","restoreSafe","","");
  dptEmergency[7] = makeDynString ("","","goDipoleSafe","","");
  dptEmergency[8] = makeDynString ("","","goSolenoidSafe","","");
  dptEmergency[9] = makeDynString ("","","restoreFromDipoleSafe","","");
  dptEmergency[10] = makeDynString ("","","restoreFromSolenoidSafe","","");
  dptEmergency[11] = makeDynString ("","","goOff","","");
  dptEmergency[12] = makeDynString ("","Actual","","","");
  dptEmergency[13] = makeDynString ("","","isSafe","","");
  dptEmergency[14] = makeDynString ("","","isSuperSafe","","");
  dptEmergency[15] = makeDynString ("","","gotGoSafe","");
  dptEmergency[16] = makeDynString ("","","gotGoSuperSafe","","");
  dptEmergency[17] = makeDynString ("","","heartbeat","","");
  dptEmergency[18] = makeDynString ("","","movingSafe","");
  dptEmergency[19] = makeDynString ("","","movingSuperSafe","","");
  dptEmergency[20] = makeDynString ("","","gotRestoreSafe","","");
  dptEmergency[21] = makeDynString ("","","restoringSafe","","");
  dptEmergency[22] = makeDynString ("","","isDipoleSafe","","");
  dptEmergency[23] = makeDynString ("","","isSolenoidSafe","","");
  dptEmergency[24] = makeDynString ("","","gotGoDipoleSafe","","");
  dptEmergency[25] = makeDynString ("","","gotGoSolenoidSafe","","");
  dptEmergency[26] = makeDynString ("","","movingDipoleSafe","","");
  dptEmergency[27] = makeDynString ("","","movingSolenoidSafe","","");
  dptEmergency[28] = makeDynString ("","","gotRestoreFromDipoleSafe","","");
  dptEmergency[29] = makeDynString ("","","gotRestoreFromSolenoidSafe","","");
  dptEmergency[30] = makeDynString ("","","restoringFromDipoleSafe","","");
  dptEmergency[31] = makeDynString ("","","restoringFromSolenoidSafe","","");
  dptEmergency[32] = makeDynString ("","","gotGoOff","","");
  dptEmergency[33] = makeDynString ("","","movingOff","","");
  dptEmergency[34] = makeDynString ("","","isOff","","");
  dptEmergency[35] = makeDynString ("","","completedGoSafe","","");
  dptEmergency[36] = makeDynString ("","","completedGoSuperSafe","","");
  dptEmergency[37] = makeDynString ("","","completedRestoreSafe","","");
  dptEmergency[38] = makeDynString ("","","completedGoDipoleSafe","","");
  dptEmergency[39] = makeDynString ("","","completedGoSolenoidSafe","","");
  dptEmergency[40] = makeDynString ("","","completedRestoreFromDipoleSafe","","");
  dptEmergency[41] = makeDynString ("","","completedRestoreFromSolenoidSafe","","");
  dptEmergency[42] = makeDynString ("","","completedGoOff","","");
  
      
  dptEmergencyStruct[1] = makeDynInt (DPEL_STRUCT);
  dptEmergencyStruct[2] = makeDynInt (0,DPEL_STRUCT);
  dptEmergencyStruct[3] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[4] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[5] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[6] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[7] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[8] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[9] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[10] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[11] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[12] = makeDynInt (0,DPEL_STRUCT);
  dptEmergencyStruct[13] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[14] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[15] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[16] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[17] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[18] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[19] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[20] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[21] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[22] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[23] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[24] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[25] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[26] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[27] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[28] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[29] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[30] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[31] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[32] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[33] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[34] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[35] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[36] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[37] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[38] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[39] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[40] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[41] = makeDynInt (0,0,DPEL_BOOL);
  dptEmergencyStruct[42] = makeDynInt (0,0,DPEL_BOOL);
 
//create or change data point type
  dyn_string dsDpTypes = dpTypes("*");
  int rc;
  if (dynContains(dsDpTypes,sdpt)<0) {
    DebugN("creating datapoint type "+sdpt);
    rc = dpTypeCreate(dptEmergency,dptEmergencyStruct);
    DebugN(  "result = "+rc);
  }
  else {
    DebugN("datapoint type "+sdpt+" exists. Modifying ");
    rc = dpTypeChange(dptEmergency,dptEmergencyStruct);
    DebugN("  result = "+rc);
  }
}
