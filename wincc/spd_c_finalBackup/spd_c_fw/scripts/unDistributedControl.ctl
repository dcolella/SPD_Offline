/**@name SCRIPT: unDistributedControl.ctl

@author: Herve Milcent (LHC-IAS)

Creation Date: 12/07/2002

Modification History: 
  05/09/2011: Herve
  - IS-599: system name and component version added in the MessageText log and in the diagnostic  
  
	19/02/2008: Herve
		- check if unMessagetext function defined
		
	11/05/2006: Herve
		- optimization of dpSet
		
	06/07/2004: Herve
		- in unDistributedControl_init: 
			. remove the dist_read and replace it by unDistributedControl_getAllDeviceConfig
			. unDistributedControl DP are not created anymore, must be present
			. remove the redundancy
			. dpConnect to _Connection.Dist.ManNums and _DistManager.State.SystemNums
			

version 1.0

Purpose: 
This script implements the unDistributedControl component. The DistributedControl component checks if the 
remote PVSS systems defined are connected or not. The result of this check can be used to set 
the graphical characteristics of a component, send a message to the operator, send email, send an SMS, etc.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unDistributedControl_localDpName: the data point name for the local system: string
		. g_unDistributedControl_dsSysName: the list of the remote system names: dyn_string
		. g_unDistributedControl_dsRemoteDpName: the list of data point name for the remote systems: dyn_string
		. g_DistConnected: is the local WCCILdist manager connected
	. constant:
		. c_unDistributedControl_dpType: the DistributedControl component data point type
		. c_unDistributedControl_dpName: the beginning of the data point name of the DistributedControl component
		. c_unDistributedControl_dpElementName: the data point element name of the DistributedControl component
	. data point type needed: _UnDistributedControl
	. data point: the following data point is needed per system names
		. _unDistributedControl_XXX_n: of type _UnDistributedControl, XXX is the remote system name (without :)
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/

// global declaration
global string g_unDistributedControl_localDpName;
global dyn_string g_unDistributedControl_dsRemoteSysName;
global dyn_string g_unDistributedControl_dsRemoteDpName;
global dyn_bool g_unDistributedControl_bRemoteState;
global bool g_unDistributedControl_bLocalState;
global bool g_unDistributedControl_initialized;
global bool g_unMessagetextDefined;
global string g_DistributedControl_sSystemName;
// end global declaration

//@{

// main
/**
Purpose:
This is the main of the script.

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. no temporary data point, no graphical element
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
main()
{
	dyn_string exceptionInfo; // to hold any errors returned by the function
  int iRes, i, len;
  dyn_dyn_string ddsComponentsInfo;
  string sMessage;
	
	g_unMessagetextDefined = isFunctionDefined("unMessageText_sendException");
  g_DistributedControl_sSystemName = getSystemName();

// initialize the select/deselect mechanism
	unDistributedControl_init(exceptionInfo);

// handle any error, send message to the MessageText component
	if(dynlen(exceptionInfo) > 0) {
//		DebugN("MessageText", "-1;0", "DistributedControl initialisation failed", exceptionInfo);
		if(g_unMessagetextDefined)
			unMessageText_sendException("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", exceptionInfo);
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_DistributedControl_sSystemName+"DistributedControl", exceptionInfo);
		}
	}
	else {
//		DebugN("MessageText", "-1;0", "DistributedControl initialisation successfully done");
    iRes = fwInstallation_getInstalledComponents(ddsComponentsInfo);
    len = dynlen(ddsComponentsInfo);
    for(i=1;i<=len;i++)
    {
      if(ddsComponentsInfo[i][1] == "unDistributedControl")
      {
        sMessage = "unDistributedControl version "+ddsComponentsInfo[i][2]+" loaded";
        i = len+1;
      }
    }
    if(g_unMessagetextDefined)
    {
  			unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "INFO", 
  					getCatStr("unDistributedControl", "STARTED") , exceptionInfo);
  			unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "INFO", sMessage , exceptionInfo);
    }
    else
    {
  			DebugTN(g_DistributedControl_sSystemName+"DistributedControl: "+getCatStr("unDistributedControl", "STARTED"));
  			DebugTN(g_DistributedControl_sSystemName+"DistributedControl: "+ sMessage);
    }
// handle any error uin case the send message failed
		if(dynlen(exceptionInfo) > 0) {
			DebugTN(g_DistributedControl_sSystemName+"DistributedControl: "+getCatStr("unDistributedControl", "STARTED"), exceptionInfo);
		}
	}
}

// unDistributedControl_init
/**
Purpose:
This function does the initialisation the DistributedControl component. 

The list of remote PVSS system is read from the config file via the dist_readConfig function. A dpConnect is done to 
the internal data point in order to check if the remote systems are connected or not. Redundancy is not supported.

	@param exceptionInfo: dyn_string, output, Details of any exceptions are returned here

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. global variable: the following variables are global to the script
		. g_unDistributedControl_localDpName: the data point name for the local system: string
		. g_unDistributedControl_dsRemoteDpName: the list of data point name for the remote systems: dyn_string
		. g_unDistributedControl_dsRemoteSysName: the list of remote system name
		. g_unDistributedControl_localDpName: the local data point name
	. constant:
		. c_unDistributedControl_dpType: the DistributedControl component data point type
		. c_unDistributedControl_dpName: the beginning of the data point name of the DistributedControl component
		. c_unDistributedControl_dpElementName: the data point element name of the DistributedControl component
	. data point type needed: _UnDistributedControl
	. data point: the following data point is needed per system names, if it does not exist it is created at startup
		. _unDistributedControl_XXX_n: of type _UnDistributedControl, XXX is the remote system name (without :)
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
unDistributedControl_init(dyn_string &exceptionInfo)
{
	dyn_string dsSystemName, dsHost;
	dyn_int diPortNumber, diSystemId;
	int len, i, pos, iRes;
	string sDpName;
	
  //Make sure the CtrlDebug dp exists for the control manager:
  string debugDp = "_CtrlDebug_CTRL_" + myManNum();
  if(!dpExists(debugDp))
  {
    DebugTN(g_DistributedControl_sSystemName+"DistributedControl: Creating " + debugDp + " dp.");
    dpCreate(debugDp, "_CtrlDebug");
  }
// read all the config of type _UnDistributedControl
	unDistributedControl_getAllDeviceConfig(dsSystemName, diSystemId, dsHost, diPortNumber);
	g_unDistributedControl_initialized = false;
	
	len = dynlen(dsSystemName);	
	for(i=1;i<=len;i++) {
// fill the global variables
		if((dsSystemName[i]+":") != getSystemName()) {
			dynAppend(g_unDistributedControl_dsRemoteDpName, c_unDistributedControl_dpName+dsSystemName[i]);
			dynAppend(g_unDistributedControl_dsRemoteSysName, dsSystemName[i]+":");
			dynAppend(g_unDistributedControl_bRemoteState, false);
		}
	}	

// initialise the local dpname
	g_unDistributedControl_localDpName = c_unDistributedControl_dpName+ 
						substr(getSystemName(), 0, strpos(getSystemName(), ":"));
	g_unDistributedControl_bRemoteState = false;

// end of initialisation, connect to the dist dps.
// no redundancy, so just connect to dist dp
	iRes = dpConnect("_unDistributedControl_callback", "_Connections.Dist.ManNums", "_DistManager.State.SystemNums");
	if(iRes == -1) {
		fwException_raise(exceptionInfo, "ERROR", "unDistributedControl_init: the Dist DP is not existing","");
	}
}


// _unDistributedControl_callback
/**
Purpose:
This is the callback function called in the case of no redundant system. It sets the _unDistributedControl data point according to the state 
of the remote systems. 

localSystemIds contains the manager number of the local WCCILdist. No value means that the WCCILdist 
is not connected, therefore all the _unDistributedControl have to be set to false because there is no connection to the 
remote systems. 

remoteSystemIds contains the list of remote WCCILdist manager number connected and ready to the local WCCILdist, equivalent to 
remote system name because the WCCILdist manager number must be unique within the whole distributed system. 
getSystemId(remote systemName) returns the Id of the remote system, if the corresponding Id is in the remoteSystemIds 
then the _unDistributedControl is set to true, otherwise it is set to false.

	@param sConn: string, input, the _Connections.Dist.ManNums data point element
	@param localSystemIds: dyn_int, input, the value of _Connections.Dist.ManNums data point element
	@param sDistConnectionDp: string, input, the _DistManager.State.SystemNums data point element
	@param remoteSystemIds: dyn_int, input, the value of _DistConnections.Dist.ManNums data point element

Usage: Public

PVSS manager usage: CTRL (WCCOACTRL)

Constraints:
	. PVSS version: 3.0 
	. operating system: WXP, NT and Linux, but tested only under WXP and Linux.
	. distributed system: yes.
*/
_unDistributedControl_callback(string sConn, dyn_int localSystemIds, string sDistConnectionDp, dyn_int remoteSystemIds)
{
	int deviceSystemId, posId, i, len;
	string messageString;
	dyn_string exceptionInfo;

  //DebugN("_unDistributedControl_callback", localSystemIds, remoteSystemIds);
  string version = "N/A";
  bool notUnicos = !fwInstallation_isComponentInstalled("unCore", version);

	if(dynlen(localSystemIds) < 1) {
// local WCCILdist not connected  
// set the all the system state dps to false
		len = dynlen(g_unDistributedControl_dsRemoteDpName);
		for(i=1;i<=len;i++) {
			if(!g_unDistributedControl_initialized) {
				dpSetWait(g_unDistributedControl_dsRemoteDpName[i]+c_unDistributedControl_dpElementName, false);
				messageString = getCatStr("unDistributedControl", "REMNOTCON")+ ": "+g_unDistributedControl_dsRemoteSysName[i];
				if(g_unMessagetextDefined)
					unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
			}
			else if(g_unDistributedControl_bRemoteState[i])  {
				dpSetWait(g_unDistributedControl_dsRemoteDpName[i]+c_unDistributedControl_dpElementName, false);
				messageString = getCatStr("unDistributedControl", "REMNOTCON")+ ": "+g_unDistributedControl_dsRemoteSysName[i];
				if(g_unMessagetextDefined)
					unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
			}
			g_unDistributedControl_bRemoteState[i] = false;
		}
// set the local dp to false
		if(!g_unDistributedControl_initialized) {
			dpSetWait(g_unDistributedControl_localDpName+c_unDistributedControl_dpElementName, false);
			messageString = getCatStr("unDistributedControl", "LOCNOTCON")+ ": "+g_unDistributedControl_localDpName;
			if(g_unMessagetextDefined)
				unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
		}
		else if(g_unDistributedControl_bLocalState) {
			dpSetWait(g_unDistributedControl_localDpName+c_unDistributedControl_dpElementName, false);
			messageString = getCatStr("unDistributedControl", "LOCNOTCON")+ ": "+g_unDistributedControl_localDpName;
			if(g_unMessagetextDefined)
				unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
		}
		g_unDistributedControl_bLocalState = false;
	}
	else {
    
    if(notUnicos)
    {
      //If unCore is not installed, we will create a new dp for each of the connected WinCC OA systems    
      len = dynlen(remoteSystemIds);
      for(int i = 1; i <= len; i++)
      {
        string sys = getSystemName(remoteSystemIds[i]);
        strreplace(sys, ":", "");
        //DebugN("Checking if dp exists: " + sys , !dpExists(c_unDistributedControl_dpName + sys));
        if(sys != "" && !dpExists(c_unDistributedControl_dpName + sys))  
        {
          DebugTN(g_DistributedControl_sSystemName+"DistributedControl: Detected new connection to system " + sys + ". Creating internal datapoint as this is not a UNICOS project.");
          dpCreate(c_unDistributedControl_dpName + sys, "_UnDistributedControl");
          dpSet(c_unDistributedControl_dpName + sys + ".config", ";;" + remoteSystemIds[i],
                c_unDistributedControl_dpName + sys + ".connected", true);
          //Add the new system to the global variables:
     			dynAppend(g_unDistributedControl_dsRemoteDpName, c_unDistributedControl_dpName+sys);
    			dynAppend(g_unDistributedControl_dsRemoteSysName, sys+":");
    			dynAppend(g_unDistributedControl_bRemoteState, true);

        }
      }
    }
    
		len = dynlen(g_unDistributedControl_dsRemoteSysName);
// check which system among the defined one are connected, check in systemIds
		for(i=1;i<=len;i++) {
      // get the systemId
			deviceSystemId = getSystemId(g_unDistributedControl_dsRemoteSysName[i]);
// if the systemId is included, remote system connected and ready
			posId = dynContains(remoteSystemIds, deviceSystemId);
//DebugN(deviceSystemId, posId);
			if(posId>0) {
// set the dp to true.
				if(!g_unDistributedControl_initialized) {
					dpSetWait(g_unDistributedControl_dsRemoteDpName[i]+c_unDistributedControl_dpElementName, true);
					messageString = getCatStr("unDistributedControl", "REMCONOK")+ ": "+g_unDistributedControl_dsRemoteSysName[i];
					if(g_unMessagetextDefined)
						unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
				}
				else if(!g_unDistributedControl_bRemoteState[i]) {
					dpSetWait(g_unDistributedControl_dsRemoteDpName[i]+c_unDistributedControl_dpElementName, true);
					messageString = getCatStr("unDistributedControl", "REMCONOK")+ ": "+g_unDistributedControl_dsRemoteSysName[i];
					if(g_unMessagetextDefined)
						unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
				}
				g_unDistributedControl_bRemoteState[i] = true;

			}
			else {
// remote system not connected
// set the dp to false.
				if(!g_unDistributedControl_initialized) {
					dpSetWait(g_unDistributedControl_dsRemoteDpName[i]+c_unDistributedControl_dpElementName, false);
					messageString = getCatStr("unDistributedControl", "REMNOTCON")+ ": "+g_unDistributedControl_dsRemoteSysName[i];
					if(g_unMessagetextDefined)
						unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
				}
				else if(g_unDistributedControl_bRemoteState[i]) {
					dpSetWait(g_unDistributedControl_dsRemoteDpName[i]+c_unDistributedControl_dpElementName, false);
					messageString = getCatStr("unDistributedControl", "REMNOTCON")+ ": "+g_unDistributedControl_dsRemoteSysName[i];
					if(g_unMessagetextDefined)
						unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
				}
				g_unDistributedControl_bRemoteState[i] = false;
			}
		}
    
// local WCCILdist connected
    //make sure that the local system dp has not been deleted:
    if(!dpExists(g_unDistributedControl_localDpName))
    {
      dpCreate(g_unDistributedControl_localDpName, "_UnDistributedControl");
    }

    // set the local dp to true
		if(!g_unDistributedControl_initialized) {
			dpSetWait(g_unDistributedControl_localDpName+c_unDistributedControl_dpElementName, true);
			messageString = getCatStr("unDistributedControl", "LOCCONOK")+ ": "+g_unDistributedControl_localDpName;
			if(g_unMessagetextDefined)
				unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
		}
		else if(!g_unDistributedControl_bLocalState) {
			dpSetWait(g_unDistributedControl_localDpName+c_unDistributedControl_dpElementName, true);
			messageString = getCatStr("unDistributedControl", "LOCCONOK")+ ": "+g_unDistributedControl_localDpName;
			if(g_unMessagetextDefined)
				unMessageText_send("*", "*", g_DistributedControl_sSystemName+"DistributedControl", "user", "*", "ERROR", messageString, exceptionInfo);
		}
		g_unDistributedControl_bLocalState = true;
	}

	if(!g_unDistributedControl_initialized)
		g_unDistributedControl_initialized = true;
		
//DebugTN("end _unDistributedControl_callback", g_unDistributedControl_bLocalState, g_unDistributedControl_bRemoteState);

// handle any error in case the send message failed
	if(dynlen(exceptionInfo) > 0) {
		DebugTN(g_DistributedControl_sSystemName+"DistributedControl", messageString, exceptionInfo);
	}
}

//@}

