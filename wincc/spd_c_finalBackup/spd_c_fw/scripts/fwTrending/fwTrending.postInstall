#uses "fwTrending/fwTrendingTree.ctl"
#uses "fwInstallationManager.ctl"

main()
{
  bool bTemp;
  int iVersionNumber, iConfigType;
  string sUnCoreVersion, sIsUnicos, sFreshInstall;

  sIsUnicos = "TrendingConfiguration.UNICOS.isUNICOS:_original.._value";
  
  DebugN("fwTrending: post install file");

  if ( dpExists("FwTrendingPlotInfo.type") )
  {
	dpGet("FwTrendingPlotInfo.type:_general.._type", iConfigType);
    if ( DPCONFIG_GENERAL ==  iConfigType )
    {
      dpGet("FwTrendingPlotInfo.type:_general.._string_01", sFreshInstall);
      
      if ( "FreshInstall" == sFreshInstall )
      {
    	DebugN("fwTrending: Detected fresh installation of fwTrending. Importing DPs...");
        importFwTrendingFreshInstallDPs();
        dpSetWait("FwTrendingPlotInfo.type:_general.._type", DPCONFIG_NONE); /*delete the config holding the fresh install info*/
      }
    }
  }
  else
  {
    DebugTN("Error in fwTrending.postInstall -> FwTrendingPlotInfo.type doesn't exist."); 
  }


  DebugN("fwTrending: configuring default panels");

  if( (sIsUnicos != "") && (dpExists(sIsUnicos)) )
  {
    dpGet(sIsUnicos, bTemp);
  }
  else
  {
    DebugTN("Error in fwTrending.postInstall -> sIsUnicos DPE is empty or doesn't exist: " + sIsUnicos);
    return;
  }

  if( bTemp )
  {
    dpSetWait("TrendingConfiguration.PageSettings.operationPanels:_original.._value",     makeDynString("fwTrending/fwTrendingPage"),
              "TrendingConfiguration.PageSettings.configurationPanels:_original.._value", makeDynString("fwTrending/fwTrendingPlotsPage","fwTrending/fwTrendingManageChildren"),
              "TrendingConfiguration.PlotSettings.operationPanels:_original.._value",     makeDynString("fwTrending/fwTrendingPlot"),
              "TrendingConfiguration.PlotSettings.configurationPanels:_original.._value", makeDynString("fwTrending/fwTrendingPlotConfPanel","fwTrending/fwTrendingManageChildren"));
  } 
  else 
  {
    dpSetWait("TrendingConfiguration.PageSettings.operationPanels:_original.._value",     makeDynString("fwTrending/fwTrendingStandardViewNavigator"),
              "TrendingConfiguration.PageSettings.configurationPanels:_original.._value", makeDynString("fwTrending/fwTrendingStandardViewEditor","fwTrending/fwTrendingPlotsPage"),
              "TrendingConfiguration.PlotSettings.operationPanels:_original.._value",     makeDynString("fwTrending/fwTrendingStandardViewNavigator"),
              "TrendingConfiguration.PlotSettings.configurationPanels:_original.._value", makeDynString("fwTrending/fwTrendingStandardViewEditor","fwTrending/fwTrendingPlotConfPanel"));
  }

  DebugN("fwTrending: adding clipboard to trend tree");
  _fwTrendingTree_addClipboard();

  // Upgrades old trees to the new format of tree (new format as of fwTrending2.3)
  dpGet("TrendingConfiguration.versionNumber", iVersionNumber);
  if( iVersionNumber < 2300 )
    _fwTrendingTree_upgradeTree();

  dpSetWait("TrendingConfiguration.versionNumber", 2300,
            "fwTT_TrendTree.root",                 getSystemName());

  DebugN("fwTrending: updating device references");
  _fwTrendingTree_addSystemNameRecursive(fwTrendingTree_TREE_NAME);


  if( !fwInstallation_isComponentInstalled("unCore", sUnCoreVersion) )
  {
    fwInstallationManager_append(0, "FW", "WCCOAui", "manual", 5, 3, 5, "-p fwTrending/fwTrending.pnl -iconBar -menuBar");
    DebugN("fwTrending: added UI manager with fwTrending main panel");
  }

  DebugTN("fwTrending: finished post install file execution");
}

void importFwTrendingFreshInstallDPs()
{
  string componentName = "fwTrending";
  string sourceDir;
  string subPath = "/dplist/";
  dyn_string dynDplistFiles;
  bool updateTypes = false;
  dyn_string exceptionInfo;
  
  dynDplistFiles = makeDynString("fwTrendingFreshInstallDPs.dpl");

  string componentDP = fwInstallation_getComponentDp(componentName);
  
  if(!dpExists(componentDP))
  {
    fwException_raise(exceptionInfo, "ERROR", "fwInstallation component DP for fwTrending does not exist. Cannot import DPs for fresh install.", "");
    return ;
  }
    
  dpGet(componentDP+".installationDirectory",sourceDir);

  for ( int i=1; i <= dynlen(dynDplistFiles); i++ )
 {
    if(access(sourceDir + dynDplistFiles[i], R_OK) == 0) 
    {
       fwException_raise(dsExceptions, "ERROR", "Not importing " + dynDplistFiles[i] + " due to lack of access. Check that the file exists and that it is readable by the current user.", "");
    }
  
    if(fwInstallation_importComponentAsciiFiles(componentName, sourceDir, subPath, dynDplistFiles, updateTypes))
    {
         fwException_raise(dsExceptions, "ERROR", "Failed to import the DPL file " + dynDplistFiles[i] + ".", "");
    } 
  }
}
