/**
* This file contains OPCUA specific functions for fwPeriphAddress.
*/

/**
* Get a list of subscriptions on the system for the given server.
* 
* @param sys            The system.
* @param server         The server.
* @param &exceptionInfo Exceptions.
*
* @return dyn_string A list of subscriptions.
*/
public dyn_string fwPeriphAddressOPCUA_getSubscriptions(string sys, string server, dyn_string &exceptionInfo) {

  dyn_string dsSub;
  
  if (strlen(sys) == 0) {
     fwException_raise(exceptionInfo, "ERROR", "No system","");
     return makeDynString();
  }  
  
  string dpe = sys + "_" +  server + ".Config.Subscriptions:_original.._value";

  if (!dpExists(dpe)) {
    if (strlen(server) == 0) {
       fwException_raise(exceptionInfo, "ERROR", "No OPCUA server","");
       return dsSub;
    }
    fwException_raise(exceptionInfo, "ERROR", "Subscriptions DPE does not exist","");
    return dsSub;
  }  
  
  int retVal = dpGet(dpe, dsSub);
  
  if (retVal == -1) {
    fwException_raise(exceptionInfo, "ERROR", "Could not get subscriptions: dpGet(" + dpe + ") failed","");
    return dsSub;
  }  
  
  int len = dynlen(dsSub);
  
  for (int i = 1; i <= len; i++) {
    dsSub[i] = dpSubStr(dsSub[i], DPSUB_DP);
    dsSub[i] = substr(dsSub[i], 1, strlen(dsSub[i]) - 1);
  }
  
  dynSortAsc(dsSub);

  return dsSub;
}

/**
* Get the type for the given subscription.
*
* @param sys          The system.
* @param subscription The subscription.
*
* @return int The type or -1 on error.
*/
public int fwPeriphAddressOPCUA_getSubscriptionType(string sys, string subscription, dyn_string &exceptionInfo) {
  int type;

  if (strlen(sys) == 0) {
     fwException_raise(exceptionInfo, "ERROR", "No system","");
     return -1;
  }  
  
  string dpe = sys + "_" + subscription + ".Config.SubscriptionType";

  if (!dpExists(dpe)) {
    if (strlen(subscription) == 0) {
       fwException_raise(exceptionInfo, "ERROR", "No subscription","");
       return -1;
    }
    fwException_raise(exceptionInfo, "ERROR", "Subscription type DPE does not exist","");
    return -1;
  }      
  
  int retVal = dpGet(dpe, type);
  
  if (retVal == -1) {
    fwException_raise(exceptionInfo, "ERROR", "Could not get subscription type: dpGet(" + dpe + ") failed","");
    return -1;
  }  
  
  return type;
}

/**
* Get a list of servers on the given system.
*
* @return dyn_string List of servers.
*/
public dyn_string fwPeriphAddressOPCUA_getServers(string sys, dyn_string &exceptionInfo) {
  if (strlen(sys) == 0) {
     fwException_raise(exceptionInfo, "ERROR", "No system","");
     return makeDynString();
  }
  
  dyn_string dsEqu = dpNames(sys+"*", "_OPCUAServer");
  
  int len = dynlen(dsEqu);
  
  for (int i = len; i > 0; i--) {
    // don't display redundant datapoints
    if (isReduDp(dsEqu[i])) {
      dynRemove(dsEqu, i);
    }
  }
  len = dynlen(dsEqu);
  if (len > 0) {
    for (int i = 1; i <= len; i++) {
      dsEqu[i] = dpSubStr(dsEqu[i], DPSUB_DP);
      dsEqu[i] = substr(dsEqu[i], 1, strlen(dsEqu[i]) - 1);
    }
  }

  return dsEqu;
}

/* PANEL functions */

string dpe; /*!< the DPE(s) that we're working with */
bool   isMultiDpes; /*!< Is there multiple DPEs selected? */
global int fwPeriphAddressOPCUA_panelMode; /*!< panel mode - used in the the panel functions, so must be global */

public void _fwPeriphAddressOPCUA_initPanel(string inDpe, dyn_string &exceptionInfo, bool inMultiDpes = false, int inPanelMode = -1) {
  DebugFTN("FW_INFO", "_fwPeriphAddressOPCUA_initPanel(" + inDpe + ", " + inMultiDpes + ", " + inPanelMode + ")");
  dpe = inDpe;
  isMultiDpes = inMultiDpes;
  fwPeriphAddressOPCUA_panelMode = inPanelMode;
  
  if (inPanelMode != fwPeriphAddress_PANEL_MODE_OBJECT) {
    fwPeriphAddressOPCUA_panel_updateSubscriptions();
  }
  fwPeriphAddressOPCUA_panel_update();
}



/**
* Update the subscriptions.
* 
* @return void
*/
public void fwPeriphAddressOPCUA_panel_updateSubscriptions() {
  string sConn = cmbEquipment.text, sSub = cmbSubscription.text;

  fwPeriphAddressOPCUA_panel_updatePanelFillServers(dpe);
  fwPeriphAddressOPCUA_panel_updatePanelFillSubscriptions(dpe, cmbEquipment.text);
 
  dyn_string dsEqu, dsSub;

  getMultiValue("cmbEquipment", "items", dsEqu,
                "cmbSubscription", "items", dsSub);
  
  setMultiValue("cmbEquipment","selectedPos",dynContains(dsEqu,sConn),
                "cmbSubscription","selectedPos",dynContains(dsSub,sSub));
}


//the following functions are derived from opcuaDrvPara.ctl

/**
* Get the direction from the direction and receive mode GUI widgets.
*
* @return int The direction.
*/  
private int _getDirection() {
  int direction = einaus.number, receiveMode = modus.number;
  direction++;

  if (direction == 2) {
    if (receiveMode == 1) {
      direction = DPATTR_ADDR_MODE_INPUT_POLL;
    } else if (receiveMode == 2) {
      direction = DPATTR_ADDR_MODE_INPUT_SQUERY;
    } else if (receiveMode == 3) {
      direction = DPATTR_ADDR_MODE_IO_SQUERY;
    }
  } else if (direction == 3) {
    if (receiveMode == 0) {
      direction = DPATTR_ADDR_MODE_IO_SPONT;
    } else if (receiveMode == 1) {
      direction = DPATTR_ADDR_MODE_IO_POLL;
    } else if (receiveMode == 3) {
      direction = 9; //DPATTR_ADDR_MODE_AM_ALERT not defined in 3.11?
    }
  }
  
  return direction;
}

/**
* Fill the servers combo box.
*
* @return void
*/
public void fwPeriphAddressOPCUA_panel_updatePanelFillServers(string dpe) {
  dyn_string exceptionInfo;
  dyn_string servers = fwPeriphAddressOPCUA_getServers(dpSubStr(dpe, DPSUB_SYS), exceptionInfo);
  if (dynlen(exceptionInfo) > 0) {
    fwExceptionHandling_display(exceptionInfo);
  } else {              
    setValue("cmbEquipment", "items", servers);
  }
}

/**
* Fill the subscriptions combo box.
*
* @return void
*/
public void fwPeriphAddressOPCUA_panel_updatePanelFillSubscriptions(string dpe, string server) {
  dyn_string exceptionInfo;
  dyn_string dsSub = fwPeriphAddressOPCUA_getSubscriptions(dpSubStr(dpe, DPSUB_SYS), server, exceptionInfo);
  if (dynlen(exceptionInfo) > 0) {
    fwExceptionHandling_display(exceptionInfo);
  } else {
    dynInsertAt(dsSub, "", 1);
    setValue("cmbSubscription", "items", dsSub);
  }
}

/**
* Update the view by hiding/showing widgets.
* 
* @return void
*/
public void fwPeriphAddressOPCUA_panel_update() {
  DebugFTN("FW_INFO", "fwPeriphAddressOPCUA_panelMode: " + fwPeriphAddressOPCUA_panelMode);
  mainApplyButton.visible = true;
  
  txtPollGroup.visible = false;
  
  setOpcuaServer.visible = fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES;	
  setOpcuaDriver.visible = fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES;	
  setOpcuaVariant.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);
  setOpcuaDirection.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	
  setOpcuaMode.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	
  setOpcuaSubscription.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	
  setOpcuaTransformation.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	
  setOpcuaPollGroup.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	
  setOpcuaLowLevel.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	
  setOpcuaActive.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES);	  
  setOpcuaMode.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES) && modus.visible;
  setOpcuaPollGroup.visible = (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES) && cmbPollGroup.visible;
  lowlevel.visible = einaus.number != 0;
  
 	if(fwPeriphAddressOPCUA_panelMode != fwPeriphAddress_PANEL_MODE_MULTIPLE_DPES) {
    setOpcuaServer.state(0) = TRUE;
    setOpcuaDriver.state(0) = TRUE;
    setOpcuaVariant.state(0) = TRUE;
    setOpcuaSubscription.state(0) = TRUE;
    setOpcuaDirection.state(0) = TRUE;
    setOpcuaMode.state(0) = TRUE;
    setOpcuaTransformation.state(0) = TRUE;
    setOpcuaPollGroup.state(0) = TRUE;
    setOpcuaLowLevel.state(0) = TRUE;
    setOpcuaActive.state(0) = TRUE;
  }
 
  int iDirection = _getDirection();

  bool isNotObjectMode = fwPeriphAddressOPCUA_panelMode != fwPeriphAddress_PANEL_MODE_OBJECT;
  
  // IM 104259 mjeidler: iSub contains SubscriptionType of the chosen Subscription
  int iSub = radKind.number;

  string subscriptionText;

  if (fwPeriphAddressOPCUA_panelMode == fwPeriphAddress_PANEL_MODE_OBJECT) {
    subscriptionText = txtSubscription.text;
  } else {
    subscriptionText = cmbSubscription.text;    
  }
  
  setMultiValue(
    "tm", "visible", iDirection != DPATTR_ADDR_MODE_OUTPUT,   
    "border1", "visible", isNotObjectMode && iDirection != DPATTR_ADDR_MODE_OUTPUT,   
    "modus", "visible", iDirection != DPATTR_ADDR_MODE_OUTPUT,   
    "lowlevel", "visible", /*isNotObjectMode && */iDirection != DPATTR_ADDR_MODE_OUTPUT
  );

  // IM 104259 mjeidler: Direction, Receiving Mode, LowLevel will be shown only for the correct SubscriptionType  
  if (subscriptionText && ((iSub == 0) || (iSub == 1))) {
    if ((modus.number == 1) || (modus.number == 2) || (modus.number == 3)) {
       modus.number = 0;
    }
    
    modus.itemEnabled(0, true);
    modus.itemEnabled(1, false);
    modus.itemEnabled(2, false);
    modus.itemEnabled(3, false);
    einaus.itemEnabled(0, true);
    einaus.itemEnabled(1, true);
    lowlevel.visible = true;
  } else if (subscriptionText && (iSub == 2)) {
    if ((modus.number == 0) || (modus.number == 1) || (modus.number == 2)) {
      modus.number = 3;  
    }
    
    modus.itemEnabled(0, false);
    modus.itemEnabled(1, false);
    modus.itemEnabled(2, false);
    modus.itemEnabled(3, true);
    einaus.itemEnabled(0, false);
    einaus.itemEnabled(1, false);
    einaus.number = 2;
    lowlevel.visible = false;
    
    if (einaus.number == 2) {
      setMultiValue(
        "tm", "visible", true,
        "border1", "visible", true,
        "modus", "visible", true
      );
    }
  } else {
    if ((modus.number == 0) || (modus.number == 3)) {
      modus.number = 1;
    }
    
    modus.itemEnabled(0, false);
    modus.itemEnabled(1, true);
    modus.itemEnabled(2, true);
    modus.itemEnabled(3, false);
    einaus.itemEnabled(0, true);
    einaus.itemEnabled(1, true);
    lowlevel.visible = true;
  }
  
  // IM 104259 mjeidler: view PollGroup only, if Mode PollGroup is chosen
  if (shapeExists("frmPollGroup") && (einaus.number != 0) && (modus.number == 1)) {    
    lblPollGroup.visible = true;
    frmPollGroup.visible = true;
    if (isNotObjectMode) {
      cmbPollGroup.visible = true;
      cmdPollGroup.visible = true;
    } else {
      txtPollGroup.visible = true;
    }
  } else {
	   frmPollGroup.visible = false;
	   lblPollGroup.visible = false;
	   cmbPollGroup.visible = false;
	   cmdPollGroup.visible = false;
     txtPollGroup.visible = false;
  }
  
  setOpcuaMode.visible = isMultiDpes && modus.visible;
  setOpcuaPollGroup.visible = isMultiDpes && cmbPollGroup.visible;
  lowlevel.visible =/* isNotObjectMode &&*/ iDirection != DPATTR_ADDR_MODE_OUTPUT;  
  setOpcuaLowLevel.visible = isMultiDpes && lowlevel.visible;
  setOpcuaSubscription.visible = isMultiDpes;
  setOpcuaVariant.visible = isMultiDpes;                                 
    
  if (isMultiDpes) {
    txtItem.text = "Can't set OPC item in multiple data point element mode";
    txtItem.enabled = false;
    buGetItemId.enabled = false;
  }
    
  buGetItemId.visible = isNotObjectMode;
  lblDriverNumber.visible = isNotObjectMode;
  Treiber.visible = isNotObjectMode;
  radVariant.visible = isNotObjectMode;
  lblVariant.visible = isNotObjectMode;
  cmbEquipment.visible = isNotObjectMode;
  lblServer.visible = isNotObjectMode;
  cboAddressActive.visible = isNotObjectMode;
  
  if (!txtPollGroup.visible) {
    txtPollGroup.text = "";
  }
  
  //TODO is it allowed to change Kind for OPCUA (it's not editable in PARA)
  //lblKind.visible = !isNotObjectMode; 
  //radKind.visible = !isNotObjectMode;
  
  if (!isNotObjectMode) {
    lblVariant.visible = true;
    radVariant.visible = true;
  }
  
  cmbSubscription.visible = isNotObjectMode;
  cmdSubscription.visible = isNotObjectMode;
  txtSubscription.visible = !isNotObjectMode;  
}

