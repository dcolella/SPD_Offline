class: ASS_SpdHSTempLU_CLASS/associated
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED_PW	!color: FwStateOKPhysics
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: NO_TEMP	!color: FwStateAttention3
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
