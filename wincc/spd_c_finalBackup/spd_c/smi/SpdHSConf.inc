class: ASS_SpdHSConf_CLASS/associated
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
