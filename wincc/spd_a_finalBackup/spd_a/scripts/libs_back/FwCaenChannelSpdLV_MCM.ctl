#uses "dcsCaen/dcsCaen.ctl"
  
string typeLV = "";      
string stateTimeOutLV = "";      

const int UNLOCK_DELAYCYCLE_LV = 2; // seconds between 2 status checks, in order to prevent a double status change

FwCaenChannelSpdLV_MCM_initialize(string domain, string device)       
{       
  typeLV = DCSCAEN_LV_BASE_CHANNEL;
  stateTimeOutLV = "NO_CONTROL";
  
  // checks the new status       
  string logicNameLV = dpGetAlias(device + ".");       
  string actualStateLV = FwCaenChannelSpdLV_MCM_GetRealState(domain, device, logicNameLV);       
  fwDU_setState(domain, device, actualStateLV);        
  
}
FwCaenChannelSpdLV_MCM_valueChanged( string domain, string device,    
      int actual_dot_status, string &fwState )    
{    
  
  dyn_string exceptionInfo;        
  string lastState;
  fwDU_getState(domain,device,lastState);
  string logicNameLV = dpGetAlias(device + ".");

  string currState = FwCaenChannelSpdLV_MCM_GetRealState(domain, device, logicNameLV, actual_dot_status);        
	     
  switch(currState){        
    case "READY":        
      if (lastState != "READY"){ 																				// applies here the  recipe for the ready state        
	exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain,typeLV, currState,logicNameLV );        
      }       
      break;        
    case "OFF":        
      if (lastState != "OFF"){       
	exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain,typeLV, currState, logicNameLV );        
      }       
  }        
   
  // -----  User defined section example 
  /*  string UserDefined;
      dpGet(device +".userDefined", UserDefined);    
      int UserParam = dcsCaen_getUserDefinedParam(UserDefined, "param1");
       ... do some think ...
  */
  // --------------------------------------

  if(dynlen(exceptionInfo))DebugTN(exceptionInfo);        
 
  fwState = currState;    
  
}
FwCaenChannelSpdLV_MCM_doCommand(string domain, string device, string command)           
{             
        
  string state;        
  dyn_string exceptionInfo;        
  float V0 = 0.0;
  string logicNameLV = dpGetAlias(device + ".");
  string actualStateLV;
  int status;
  
//	fwDU_getState( domain, device, state); 							// gets the state        
// DebugTN("Command Received", command,"State", actualStateHVI , "Type",typeHVI);       

  switch (command){        
    case "SWITCH_OFF":  
      dpSet(device + ".settings.onOff", false);       
      break;        
      
    case "SWITCH_ON":  
      dpSet(device + ".settings.onOff", true);       
      break;        
      
    case "GO_OFF":  
      exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeLV, "RAMP_DW_OFF" , logicNameLV );
//      dpSet(device + ".settings.onOff", false);       
      break;        
			        
    case "GO_READY":        
      exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeLV, "RAMP_UP_READY",  logicNameLV );        
//      dpSet(device + ".settings.onOff", true);       
      break;        
			        
    case "RELOAD":
      fwDU_getState(domain,device,actualStateLV);
      exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeLV, actualStateLV , logicNameLV );	// Reloads the recipe for this state        
      break;        
			        
    case "RESET":        
//       dpGet(device + ".readBackSettings.v0", V0);       
//       dpSet(device + ".settings.onOff", true,device + ".settings.v0", 0.0);       
//       delay(0,500);
//       dpSet(device + ".settings.onOff", false, device + ".settings.v0", V0);       

      dpGet( device + ".actual.status", status);
      dpSet( device + ".actual.status", status);
      
      break;        
  }        

  // ----  User Defined Section
  string UserDefined;
  dpGet(device +".userDefined", UserDefined); 
  if(UserDefined != "") 
  {
    int Timeout = dcsCaen_getUserDefinedParam( UserDefined, "timeout");
    if(Timeout > 0) fwDU_startTimeout(Timeout, domain, device, stateTimeOutLV);       
  
    int s1, s2;
    int Unlock = dcsCaen_getUserDefinedParam( UserDefined, "unlock");
    if(Unlock > 0) // Force a status change
    {
      dpGet(device+".actual.status:_original.._value",s1);		
      delay(UNLOCK_DELAYCYCLE_LV ,0);
      dpGet(device+".actual.status:_original.._value",s2);		
      // DebugN(">-->unlock",s1,s2);
      if(s1 == s2) dpSetWait(device+".actual.status:_original.._value",s1);
    }
  }
  // -----------
  
  if(dynlen(exceptionInfo))DebugTN(exceptionInfo);
  
}        
        
string FwCaenChannelSpdLV_MCM_GetRealState(string domain, string device,
                                       string logicName, int status = -1){        

  string fwState;        
  int actual_dot_status;        
  dyn_string exceptionInfo;        
	        
  if (status == -1){        
    dpGet(device+".actual.status:_original.._value",actual_dot_status);             
  }        
  else
    actual_dot_status = status;        
	        
//	DebugTN("actual.status", actual_dot_status);        
	        
  //******************************* Error States *****************************        
  if (actual_dot_status & CAEN_TRIPMASK ){             
    fwState = "TRIPPED";             
  }            
  else if (actual_dot_status & CAEN_ERRORMASK ){             
    fwState = "CHAN_FAULT";             
  }           
  else if (actual_dot_status & CAEN_NOCONTROLMASK ){              
    fwState = "NO_CONTROL";              
  }              
  else if (actual_dot_status & CAEN_IGNOREDMASK){                
    fwDU_getState(domain, device, fwState);          
  }                
  //****************************** On/Off **********************************************        
  else if (!(actual_dot_status ^ CAEN_OFFMASK)){             
    fwState = "OFF";             
  }             
  else if (!(actual_dot_status ^ CAEN_ONMASK))	{          
    fwState = "READY";        
  }
  //********************************************* Ramp UP/DOWN **************************************        
  else if (!(actual_dot_status ^ CAEN_RAMPUPMASK )) {
    fwState = "RAMP_UP_READY";        
  }        
  else if (!(actual_dot_status ^ CAEN_RAMPDOWNMASK )){               
    fwState = "RAMP_DW_OFF";   
  }             
  else{             
    fwState = "UNDEFINED";             
  }            
	        
  if(dynlen(exceptionInfo))DebugTN(exceptionInfo);        
  
  return fwState;        
}  


 

