#uses "SpdHSector_5_A$SpdHSTemp$install"
#uses "SpdHSector_5_A$FwDevMajority$install"
#uses "SpdHSector_5_A$FwCaenChannelSpdLV_MCM$install"
#uses "SpdHSector_5_A$FwCaenChannelSpdLV_BUS$install"
#uses "SpdHSector_5_A$FwCaenChannelSpdHV$install"
#uses "SpdHSector_5_A$FwDevMode$install"

startDomainDevices_SpdHSector_5_A()
{
	fwFsm_startDomainDevicesNew("SpdHSector_5_A");
}
