#uses "dcsCaen/dcsCaen.ctl"
  
string typeHVI = "";      
string stateTimeOutHVI = "";  
//float band =1.0;    // standard setting
//float band =1.2;    // temporary solution
//float band = 2.0;    // 30/06/2017
float band = 5.0;    // 12/11/2017
int UndefineDelayHVI = 5;

const int UNLOCK_DELAYCYCLE_HVI = 2; // seconds between 2 status checks, in order to prevent a double status change

FwCaenChannelSpdHV_initialize(string domain, string device)       
{       
  typeHVI = DCSCAEN_HV_INTER_CHANNEL;
  stateTimeOutHVI = "NO_CONTROL";
  
  // checks the new status       
  string logicNameHVI = dpGetAlias(device + ".");
  string actualStateHVI = FwCaenChannelSpdHV_GetRealState(domain, device,logicNameHVI);       
  fwDU_setState(domain, device, actualStateHVI);        
  
}


FwCaenChannelSpdHV_valueChanged( string domain, string device,    
      int actual_dot_status, string &fwState )    
{    
  
  dyn_string exceptionInfo;        
  string lastState;
  fwDU_getState(domain,device,lastState);
  string logicNameHVI = dpGetAlias(device + ".");

  string currState = FwCaenChannelSpdHV_GetRealState(domain, device, logicNameHVI, actual_dot_status);        

	     
  switch(currState){        
    case "READY":        
      if (lastState != "READY"){ 																				// applies here the  recipe for the ready state        
	exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain,typeHVI, currState,logicNameHVI );        
      }       
      break;        
    case "INTERMEDIATE":        
      if (lastState != "INTERMEDIATE"){       
	exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain,typeHVI, currState, logicNameHVI );        
      }       
      break;        
    case "OFF":        
      if (lastState != "OFF"){       
	//exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain,typeHVI, currState, logicNameHVI );        
      }       
  }        
   
  // -----  User defined section example 
  /*  string UserDefined;
      dpGet(device +".userDefined", UserDefined);    
      int UserParam = dcsCaen_getUserDefinedParam( recipeObj[8][ind], "delay");
       ... do some think ...
  */
  // --------------------------------------
  if(dynlen(exceptionInfo))DebugTN(exceptionInfo);        
 
  fwState = currState;    
  
}


string FwCaenChannelSpdHV_GetRealState(string domain, string device,
                                        string logicName, int status = -1, int loop = 0){        

  string fwState;        
  int actual_dot_status;        
  dyn_string exceptionInfo;        
  float vMon;
  dpGet(device +".actual.vMon", vMon);

  float vReady = dcsCaen_getFSMRecipeV0Value(domain, typeHVI, "RAMP_UP_READY", exceptionInfo, logicName);
  float vInter = dcsCaen_getFSMRecipeV0Value(domain, typeHVI, "RAMP_UP_INTER", exceptionInfo, logicName);
  
	        
  if (status == -1){        
    dpGet(device+".actual.status:_original.._value",actual_dot_status);             
  }        
  else{
    actual_dot_status = status;        
  }
	        

	        
  //******************************* Error States *****************************        
  if (actual_dot_status & CAEN_TRIPMASK ){             
    fwState = "TRIPPED";             
  }            
  else if (actual_dot_status & CAEN_ERRORMASK ){             
    fwState = "CHAN_FAULT";             
  }           
  else if (actual_dot_status & CAEN_NOCONTROLMASK ){              
    fwState = "NO_CONTROL";              
  }              
  else if (actual_dot_status & CAEN_IGNOREDMASK){                
    fwDU_getState(domain, device, fwState);          
  }                
  //****************************** On/Off **********************************************        
  else if (!(actual_dot_status ^ CAEN_OFFMASK)){             
    fwState = "OFF";             
  }             
  else if (!(actual_dot_status ^ CAEN_ONMASK))	{          
    

                   // compares the V0 setting with the one in the Recipes      
    if ( (vMon > ( vReady - band)) &&  (vMon < ( vReady + band))){
      fwState = "READY";        
    }        
    else if ( (vMon > ( vInter - 0.5*band)) &&  (vMon < ( vInter + 0.5*band))){
      fwState = "INTERMEDIATE";	        
     }	    
    else if ( (vMon > ( 0.5*vReady - band)) &&  (vMon < ( 0.5*vReady + band))){
      fwState = "RAMP_UP_READY";	//added 27/09/2017 to handle two-step GO_READY        
     }	    
    else if ( loop <= 0){
      DebugTN("Delaying and waiting",vMon, status );
      fwState = spdHVIHandleUndefine( domain, device,logicName, loop +1);
      
    }
    else {
      fwState = "UNDEFINED";           
    }
  } 
               
  //********************************************* Ramp UP **************************************        
  else if (!(actual_dot_status ^ CAEN_RAMPUPMASK )) {
           
    if (vMon > vInter) {
      fwState = "RAMP_UP_READY";        
    }        
    else if (vMon < vInter) {        
      fwState = "RAMP_UP_INTER";           
    }
   else if ( loop <= 0){
       DebugTN("Delaying and waiting",vMon, status );
      fwState=spdHVIHandleUndefine( domain, device,logicName, loop +1);
    }    
   else{
      
      fwState = "UNDEFINED";
    }
  }  
   //********************************************* Ramp Down **************************************             
  else if (!(actual_dot_status ^ CAEN_RAMPDOWNMASK )){               
    bool onOff;           
    dpGet(device + ".settings.onOff", onOff); 
    
    if (onOff == FALSE){
      fwState = "RAMP_DW_OFF";   
    }
    else{         
      fwState = "RAMP_DW_INTER";
    }
  }
  else{
    fwState = "UNDEFINED";             
  }            
	        
  if(dynlen(exceptionInfo))DebugTN(exceptionInfo);        
  
  return fwState;        
}  

string spdHVIHandleUndefine(string domain, string device,
                    string logicName, int loop = 0){
  
      int status;
      delay( UndefineDelayHVI);                                           // so we create a delay and wait
      dpGet(device + ".actual.status", status);
          
                 // recalls this function increments loop to stop the recursivity
       return FwCaenChannelSpdHV_GetRealState( domain, device, logicName, status, loop +1);
  
}
FwCaenChannelSpdHV_doCommand(string domain, string device, string command)           
{             
  int status;  
  string state;        
  dyn_string exceptionInfo;    
  dyn_dyn_mixed recipeObj;
  dyn_mixed dataOut;
  float V0 = 0.0;
  string logicNameHVI = dpGetAlias(device + ".");
  string actualStateHVI;
  fwDU_getState(domain,device,actualStateHVI);
      
//	fwDU_getState( domain, device, state); 							// gets the state        
//DebugTN("Command Received", command,"State", actualStateHVI , "Type",typeHVI);       

  switch (command){        
    case "SWITCH_OFF":  
      dpSet(device + ".settings.onOff", false);       
      break;        
      
    case "SWITCH_ON":  
      dpSet(device + ".settings.onOff", true);       
      break;        
      
    case "GO_OFF":  
      exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeHVI, "RAMP_DW_OFF" , logicNameHVI );
//      dpSet(device + ".settings.onOff", false);       
      break;        
			        
    case "GO_READY":        
      //exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeHVI, "RAMP_UP_READY",  logicNameHVI );        
//    dpSet(device + ".settings.onOff", true);       
      recipeObj = dcsCaen_getFSMRecipeFromCache(domain, typeHVI, "RAMP_UP_READY", exceptionInfo, logicNameHVI);
      dataOut = dcsCaen_getDataChannel(logicNameHVI, recipeObj);
      dpSetWait(device+".settings.i0", dataOut[3]); //set i0
      float vinter = dataOut[2]/2.;
      dpSetWait(device+".settings.v0", vinter); //1st stage v = v0/2
      delay(50);
      float vready = dataOut[2];
      dpSetWait(device+".settings.v0", vready); //2nd stage v = v0  
      break;        
			        
    case "GO_INTERMEDIATE":        
      switch(actualStateHVI){        
	  case "READY":        
	    exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeHVI, "RAMP_DW_INTER" ,  logicNameHVI );	        
          break;        
	  case "OFF":        
	    exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeHVI, "RAMP_UP_INTER", logicNameHVI );        
//          dpSet(device + ".settings.onOff", true);       
          break;
        default:
          exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeHVI, "RAMP_UP_INTER", logicNameHVI );        
          break;
                  
      }        
      break;        
			        
    case "RELOAD":
      exceptionInfo = dcsCaen_applyFSMRecipeFromCache(domain, typeHVI, actualStateHVI , logicNameHVI );	// Reloads the recipe for this state        
      break;        
			        
    case "RESET":        
      //dpGet(device + ".readBackSettings.v0", V0);       
      //dpSet(device + ".settings.onOff", true,device + ".settings.v0", 0.0);       
      //delay(0,500);
      //dpSet(device + ".settings.onOff", false, device + ".settings.v0", V0);       
      
      dpGet( device + ".actual.status", status);
      dpSet( device + ".actual.status", status);
      
      break;        
  }        

  // ----  User Defined Section
  string UserDefined;
  dpGet(device +".userDefined", UserDefined); 
  if(UserDefined != "") 
  {
    int Timeout = dcsCaen_getUserDefinedParam( UserDefined, "timeout");
    fwDU_getState(domain,device,actualStateHVI);   
    DebugTN("Actual state = " + actualStateHVI);
    if(actualStateHVI == "RAMP_UP_READY") Timeout = Timeout + 30;
    if(Timeout > 0) fwDU_startTimeout(Timeout, domain, device, stateTimeOutHVI);       
  
    int s1, s2;
    int Unlock = dcsCaen_getUserDefinedParam( UserDefined, "unlock");
    if(Unlock > 0) // Force a status change
    {
      dpGet(device+".actual.status:_original.._value",s1);		
      delay(UNLOCK_DELAYCYCLE_HVI ,0);
      dpGet(device+".actual.status:_original.._value",s2);		
      // DebugN(">-->unlock",s1,s2);
      if(s1 == s2) dpSetWait(device+".actual.status:_original.._value",s1);
    }
  }
  // -----------
  	       
  if(dynlen(exceptionInfo))DebugTN(exceptionInfo);
  
}        
        



