class: ASS_SpdHalfStave_CLASS/associated
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 1
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 1
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 1
        action: GO_MCM_ONLY	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: GO_READY	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_READY	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: GO_MCM_ONLY	!visible: 1
        action: GO_STBY_CONFIGURED	!visible: 1
        action: GO_BEAM_TUNING	!visible: 1
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
    state: CONFIGURING	!color: 
        action: STOP	!visible: 1
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP	!visible: 1
    state: ERROR	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
        action: RELEASE	!visible: 1
        action: RESET	!visible: 1
    state: HOT	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
        action: RELEASE	!visible: 1
    state: ERROR_CONFIG	!color: FwStateAttention3
        action: GO_OFF	!visible: 1
        action: RELEASE	!visible: 1
        action: RESET	!visible: 1
    state: MOVING_STBY_CONF	!color: FwStateAttention1
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
    state: MOVING_READY	!color: FwStateAttention1
