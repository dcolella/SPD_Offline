class: ASS_FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1
