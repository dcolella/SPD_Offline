class: SpdHSTempBus_4_A_0TOP_SpdHSTempLU_CLASS
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
        when ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED_PW	!color: FwStateOKPhysics
        when ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED )  move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

    state: TOO_COOLED	!color: FwStateAttention2
        when ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: NO_TEMP	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: HOT	!color: FwStateAttention3
        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: UNDEFINED	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when (  ( all_in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW


object: SpdHSTempBus_4_A_0 is_of_class SpdHSTempBus_4_A_0TOP_SpdHSTempLU_CLASS

class: SpdHSTempBus_4_A_0SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_0SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_0SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_0SpdHSTemp_FWDM is_of_class SpdHSTempBus_4_A_0SpdHSTemp_FwDevMode_CLASS


class: SpdHSTempBus_4_A_0SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_0 is_of_class SpdHSTempBus_4_A_0SpdHSTemp_CLASS

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_0 is_of_class SpdHSTempBus_4_A_0SpdHSTemp_CLASS

objectset: SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_0,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_0 }
objectset: SpdHSTempBus_4_A_0SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_0,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_0 }

class: SpdHSTempBus_4_A_0FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_0FwDevMajority_FWDM is_of_class SpdHSTempBus_4_A_0FwDevMajority_FwDevMode_CLASS


class: SpdHSTempBus_4_A_0FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHSTempBus_4_A_0:SpdHSTemp_FWMAJ is_of_class SpdHSTempBus_4_A_0FwDevMajority_CLASS

objectset: SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_0:SpdHSTemp_FWMAJ }
objectset: SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_0:SpdHSTemp_FWMAJ }


objectset: SpdHSTempBus_4_A_0FWCHILDREN_FWSETACTIONS union {SpdHSTempBus_4_A_0SPDHSTEMP_FWSETACTIONS,
	SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdHSTempBus_4_A_0FWCHILDREN_FWSETSTATES union {SpdHSTempBus_4_A_0SPDHSTEMP_FWSETSTATES,
	SpdHSTempBus_4_A_0FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdHSPW_4_A_0TOP_SpdHSPower_CLASS
!panel: SpdHSPower.pnl
    parameters: int Executing = 0
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM        

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM    

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS  
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 1
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
    state: MCM_ON	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or 
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES not_in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BIAS_INTERMEDIATE

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS    
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED )  do RESET_CTKR

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do GO_BEAM_TUNING

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_ON

        when (  ( all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES not_in_state READY ) and
( all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BUS_OFF

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            move_to BEAM_TUNING
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS            
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BIAS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS   
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) or ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  move_to RUMP_DOWN_MCM                       

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS        
        action: GO_STBY_CONFIGURED	!visible: 1
            move_to STANDBY
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS        
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state READY )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_BEAM_TUN

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS                               
    state: RUMP_DOWN_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES in_state {MCM_ON,OFF} ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS         
    state: RUMP_UP_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_OFF	!visible: 0
            do SWITCH_OFFall_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BUS_OFF

        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        when (  ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BIAS_INTERMEDIATE

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED   
    state: CHAN_FAULT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: HOT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES not_in_state OFF )  do BUS_OFF

        when ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state {READY,RAMP_UP_READY,INTERMEDIATE} )  do BIAS_OFF

        when ( any_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES not_in_state STANDBY )  do RESET_CTKR

        when (  ( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and 
( any_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and        
( all_in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES in_state HOT )  ) do MCM_OFF

        action: GO_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
    state: TEMP_NO_CONTROL	!color: FwStateAttention2
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 1
            do RESET all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS

object: SpdHSPW_4_A_0 is_of_class SpdHSPW_4_A_0TOP_SpdHSPower_CLASS

class: SpdHSPW_4_A_0FwCaenChannelSpdLV_MCM_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_0FwCaenChannelSpdLV_MCM_FWDM is_of_class SpdHSPW_4_A_0FwCaenChannelSpdLV_MCM_FwDevMode_CLASS


class: SpdHSPW_4_A_0FwCaenChannelSpdLV_MCM_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdMCM_4_A_0 is_of_class SpdHSPW_4_A_0FwCaenChannelSpdLV_MCM_CLASS

objectset: SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_0 }
objectset: SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_0 }

class: SpdHSPW_4_A_0FwCaenChannelSpdLV_BUS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_0FwCaenChannelSpdLV_BUS_FWDM is_of_class SpdHSPW_4_A_0FwCaenChannelSpdLV_BUS_FwDevMode_CLASS


class: SpdHSPW_4_A_0FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdBUS_4_A_0 is_of_class SpdHSPW_4_A_0FwCaenChannelSpdLV_BUS_CLASS

objectset: SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_0 }
objectset: SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_0 }

class: SpdHSPW_4_A_0FwCaenChannelSpdHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_0FwCaenChannelSpdHV_FWDM is_of_class SpdHSPW_4_A_0FwCaenChannelSpdHV_FwDevMode_CLASS


class: SpdHSPW_4_A_0FwCaenChannelSpdHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenHVIChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF	!visible: 1
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdHV_4_A_0 is_of_class SpdHSPW_4_A_0FwCaenChannelSpdHV_CLASS

objectset: SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_0 }
objectset: SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_0 }

class: SpdHSPW_4_A_0SpdCMDTrack_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_0SpdCMDTrack_FWDM is_of_class SpdHSPW_4_A_0SpdCMDTrack_FwDevMode_CLASS


class: SpdHSPW_4_A_0SpdCMDTrack_CLASS
!panel: SpdCMDTrack.pnl
    parameters: int Executing = 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: MCM_ON	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED             
        action: RESET	!visible: 0
            move_to STANDBY            
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY

object: SpdCMDTrack_4_A_0 is_of_class SpdHSPW_4_A_0SpdCMDTrack_CLASS

objectset: SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES is_of_class VOID {SpdCMDTrack_4_A_0 }
objectset: SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS is_of_class VOID {SpdCMDTrack_4_A_0 }

class: SpdHSPW_4_A_0SpdHSTempLU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_0SPDHSTEMPLU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_0SPDHSTEMPLU_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_0SpdHSTempLU_FWDM is_of_class SpdHSPW_4_A_0SpdHSTempLU_FwDevMode_CLASS


objectset: SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES is_of_class VOID {SpdHSTempBus_4_A_0 }
objectset: SpdHSPW_4_A_0SPDHSTEMPLU_FWSETACTIONS is_of_class VOID {SpdHSTempBus_4_A_0 }

class: SpdHSPW_4_A_0SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_0SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_0SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_0SpdHSTemp_FWDM is_of_class SpdHSPW_4_A_0SpdHSTemp_FwDevMode_CLASS


class: SpdHSPW_4_A_0SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_0 is_of_class SpdHSPW_4_A_0SpdHSTemp_CLASS

objectset: SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_0 }
objectset: SpdHSPW_4_A_0SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_0 }


objectset: SpdHSPW_4_A_0FWCHILDREN_FWSETACTIONS union {SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETACTIONS,
	SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETACTIONS,
	SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETACTIONS,
	SpdHSPW_4_A_0SPDCMDTRACK_FWSETACTIONS,
	SpdHSPW_4_A_0SPDHSTEMPLU_FWSETACTIONS,
	SpdHSPW_4_A_0SPDHSTEMP_FWSETACTIONS } is_of_class VOID
objectset: SpdHSPW_4_A_0FWCHILDREN_FWSETSTATES union {SpdHSPW_4_A_0FWCAENCHANNELSPDLV_MCM_FWSETSTATES,
	SpdHSPW_4_A_0FWCAENCHANNELSPDLV_BUS_FWSETSTATES,
	SpdHSPW_4_A_0FWCAENCHANNELSPDHV_FWSETSTATES,
	SpdHSPW_4_A_0SPDCMDTRACK_FWSETSTATES,
	SpdHSPW_4_A_0SPDHSTEMPLU_FWSETSTATES,
	SpdHSPW_4_A_0SPDHSTEMP_FWSETSTATES } is_of_class VOID

class: SpdHStave_4_A_0TOP_SpdHalfStave_CLASS
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_UP_BEAM_TUN,RUMP_UP_READY} ) move_to RUMP_UP_STATE

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_DOWN_MCM,RUMP_DOWN_STBY,RUMP_DOWN_BEAM_TUN} ) move_to RUMP_DW_STATE

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to OFF
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do MCM_OFF all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY   
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY           
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to STBY_CONFIGURED
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
            move_to BEAM_TUNING
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to BEAM_TUNING
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to READY        
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to READY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
            move_to READY            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to READY            
    state: CONFIGURING	!color: 
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS            
    state: HOT	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
    state: ERROR_CONFIG	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY


object: SpdHStave_4_A_0 is_of_class SpdHStave_4_A_0TOP_SpdHalfStave_CLASS

class: SpdHStave_4_A_0SpdHSConf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_0SPDHSCONF_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_0SPDHSCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_0SPDHSCONF_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_0SPDHSCONF_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_0SpdHSConf_FWDM is_of_class SpdHStave_4_A_0SpdHSConf_FwDevMode_CLASS


class: SpdHStave_4_A_0SpdHSConf_CLASS
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
            move_to NOT_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED

object: SpdHSConf_4_A_0 is_of_class SpdHStave_4_A_0SpdHSConf_CLASS

objectset: SpdHStave_4_A_0SPDHSCONF_FWSETSTATES is_of_class VOID {SpdHSConf_4_A_0 }
objectset: SpdHStave_4_A_0SPDHSCONF_FWSETACTIONS is_of_class VOID {SpdHSConf_4_A_0 }

class: SpdHStave_4_A_0SpdHSPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_0SpdHSPower_FWDM is_of_class SpdHStave_4_A_0SpdHSPower_FwDevMode_CLASS


objectset: SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES is_of_class VOID {SpdHSPW_4_A_0 }
objectset: SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS is_of_class VOID {SpdHSPW_4_A_0 }


objectset: SpdHStave_4_A_0FWCHILDREN_FWSETACTIONS union {SpdHStave_4_A_0SPDHSCONF_FWSETACTIONS,
	SpdHStave_4_A_0SPDHSPOWER_FWSETACTIONS } is_of_class VOID
objectset: SpdHStave_4_A_0FWCHILDREN_FWSETSTATES union {SpdHStave_4_A_0SPDHSCONF_FWSETSTATES,
	SpdHStave_4_A_0SPDHSPOWER_FWSETSTATES } is_of_class VOID

class: SpdHSTempBus_4_A_1TOP_SpdHSTempLU_CLASS
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
        when ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED_PW	!color: FwStateOKPhysics
        when ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED )  move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

    state: TOO_COOLED	!color: FwStateAttention2
        when ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: NO_TEMP	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: HOT	!color: FwStateAttention3
        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: UNDEFINED	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when (  ( all_in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW


object: SpdHSTempBus_4_A_1 is_of_class SpdHSTempBus_4_A_1TOP_SpdHSTempLU_CLASS

class: SpdHSTempBus_4_A_1SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_1SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_1SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_1SpdHSTemp_FWDM is_of_class SpdHSTempBus_4_A_1SpdHSTemp_FwDevMode_CLASS


class: SpdHSTempBus_4_A_1SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_1 is_of_class SpdHSTempBus_4_A_1SpdHSTemp_CLASS

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_1 is_of_class SpdHSTempBus_4_A_1SpdHSTemp_CLASS

objectset: SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_1,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_1 }
objectset: SpdHSTempBus_4_A_1SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_1,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_1 }

class: SpdHSTempBus_4_A_1FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_1FwDevMajority_FWDM is_of_class SpdHSTempBus_4_A_1FwDevMajority_FwDevMode_CLASS


class: SpdHSTempBus_4_A_1FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHSTempBus_4_A_1:SpdHSTemp_FWMAJ is_of_class SpdHSTempBus_4_A_1FwDevMajority_CLASS

objectset: SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_1:SpdHSTemp_FWMAJ }
objectset: SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_1:SpdHSTemp_FWMAJ }


objectset: SpdHSTempBus_4_A_1FWCHILDREN_FWSETACTIONS union {SpdHSTempBus_4_A_1SPDHSTEMP_FWSETACTIONS,
	SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdHSTempBus_4_A_1FWCHILDREN_FWSETSTATES union {SpdHSTempBus_4_A_1SPDHSTEMP_FWSETSTATES,
	SpdHSTempBus_4_A_1FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdHSPW_4_A_1TOP_SpdHSPower_CLASS
!panel: SpdHSPower.pnl
    parameters: int Executing = 0
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM        

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM    

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS  
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 1
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
    state: MCM_ON	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or 
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES not_in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BIAS_INTERMEDIATE

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS    
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED )  do RESET_CTKR

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do GO_BEAM_TUNING

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_ON

        when (  ( all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES not_in_state READY ) and
( all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BUS_OFF

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            move_to BEAM_TUNING
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS            
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BIAS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS   
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) or ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  move_to RUMP_DOWN_MCM                       

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS        
        action: GO_STBY_CONFIGURED	!visible: 1
            move_to STANDBY
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS        
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state READY )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_BEAM_TUN

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS                               
    state: RUMP_DOWN_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES in_state {MCM_ON,OFF} ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS         
    state: RUMP_UP_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_OFF	!visible: 0
            do SWITCH_OFFall_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BUS_OFF

        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        when (  ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BIAS_INTERMEDIATE

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED   
    state: CHAN_FAULT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: HOT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES not_in_state OFF )  do BUS_OFF

        when ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state {READY,RAMP_UP_READY,INTERMEDIATE} )  do BIAS_OFF

        when ( any_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES not_in_state STANDBY )  do RESET_CTKR

        when (  ( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and 
( any_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and        
( all_in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES in_state HOT )  ) do MCM_OFF

        action: GO_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
    state: TEMP_NO_CONTROL	!color: FwStateAttention2
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 1
            do RESET all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS

object: SpdHSPW_4_A_1 is_of_class SpdHSPW_4_A_1TOP_SpdHSPower_CLASS

class: SpdHSPW_4_A_1FwCaenChannelSpdLV_MCM_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_1FwCaenChannelSpdLV_MCM_FWDM is_of_class SpdHSPW_4_A_1FwCaenChannelSpdLV_MCM_FwDevMode_CLASS


class: SpdHSPW_4_A_1FwCaenChannelSpdLV_MCM_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdMCM_4_A_1 is_of_class SpdHSPW_4_A_1FwCaenChannelSpdLV_MCM_CLASS

objectset: SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_1 }
objectset: SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_1 }

class: SpdHSPW_4_A_1FwCaenChannelSpdLV_BUS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_1FwCaenChannelSpdLV_BUS_FWDM is_of_class SpdHSPW_4_A_1FwCaenChannelSpdLV_BUS_FwDevMode_CLASS


class: SpdHSPW_4_A_1FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdBUS_4_A_1 is_of_class SpdHSPW_4_A_1FwCaenChannelSpdLV_BUS_CLASS

objectset: SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_1 }
objectset: SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_1 }

class: SpdHSPW_4_A_1FwCaenChannelSpdHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_1FwCaenChannelSpdHV_FWDM is_of_class SpdHSPW_4_A_1FwCaenChannelSpdHV_FwDevMode_CLASS


class: SpdHSPW_4_A_1FwCaenChannelSpdHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenHVIChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF	!visible: 1
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdHV_4_A_1 is_of_class SpdHSPW_4_A_1FwCaenChannelSpdHV_CLASS

objectset: SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_1 }
objectset: SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_1 }

class: SpdHSPW_4_A_1SpdCMDTrack_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_1SpdCMDTrack_FWDM is_of_class SpdHSPW_4_A_1SpdCMDTrack_FwDevMode_CLASS


class: SpdHSPW_4_A_1SpdCMDTrack_CLASS
!panel: SpdCMDTrack.pnl
    parameters: int Executing = 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: MCM_ON	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED             
        action: RESET	!visible: 0
            move_to STANDBY            
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY

object: SpdCMDTrack_4_A_1 is_of_class SpdHSPW_4_A_1SpdCMDTrack_CLASS

objectset: SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES is_of_class VOID {SpdCMDTrack_4_A_1 }
objectset: SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS is_of_class VOID {SpdCMDTrack_4_A_1 }

class: SpdHSPW_4_A_1SpdHSTempLU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_1SPDHSTEMPLU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_1SPDHSTEMPLU_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_1SpdHSTempLU_FWDM is_of_class SpdHSPW_4_A_1SpdHSTempLU_FwDevMode_CLASS


objectset: SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES is_of_class VOID {SpdHSTempBus_4_A_1 }
objectset: SpdHSPW_4_A_1SPDHSTEMPLU_FWSETACTIONS is_of_class VOID {SpdHSTempBus_4_A_1 }

class: SpdHSPW_4_A_1SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_1SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_1SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_1SpdHSTemp_FWDM is_of_class SpdHSPW_4_A_1SpdHSTemp_FwDevMode_CLASS


class: SpdHSPW_4_A_1SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_1 is_of_class SpdHSPW_4_A_1SpdHSTemp_CLASS

objectset: SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_1 }
objectset: SpdHSPW_4_A_1SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_1 }


objectset: SpdHSPW_4_A_1FWCHILDREN_FWSETACTIONS union {SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETACTIONS,
	SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETACTIONS,
	SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETACTIONS,
	SpdHSPW_4_A_1SPDCMDTRACK_FWSETACTIONS,
	SpdHSPW_4_A_1SPDHSTEMPLU_FWSETACTIONS,
	SpdHSPW_4_A_1SPDHSTEMP_FWSETACTIONS } is_of_class VOID
objectset: SpdHSPW_4_A_1FWCHILDREN_FWSETSTATES union {SpdHSPW_4_A_1FWCAENCHANNELSPDLV_MCM_FWSETSTATES,
	SpdHSPW_4_A_1FWCAENCHANNELSPDLV_BUS_FWSETSTATES,
	SpdHSPW_4_A_1FWCAENCHANNELSPDHV_FWSETSTATES,
	SpdHSPW_4_A_1SPDCMDTRACK_FWSETSTATES,
	SpdHSPW_4_A_1SPDHSTEMPLU_FWSETSTATES,
	SpdHSPW_4_A_1SPDHSTEMP_FWSETSTATES } is_of_class VOID

class: SpdHStave_4_A_1TOP_SpdHalfStave_CLASS
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_UP_BEAM_TUN,RUMP_UP_READY} ) move_to RUMP_UP_STATE

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_DOWN_MCM,RUMP_DOWN_STBY,RUMP_DOWN_BEAM_TUN} ) move_to RUMP_DW_STATE

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to OFF
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do MCM_OFF all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY   
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY           
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to STBY_CONFIGURED
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
            move_to BEAM_TUNING
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to BEAM_TUNING
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to READY        
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to READY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
            move_to READY            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to READY            
    state: CONFIGURING	!color: 
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS            
    state: HOT	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
    state: ERROR_CONFIG	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY


object: SpdHStave_4_A_1 is_of_class SpdHStave_4_A_1TOP_SpdHalfStave_CLASS

class: SpdHStave_4_A_1SpdHSConf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_1SPDHSCONF_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_1SPDHSCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_1SPDHSCONF_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_1SPDHSCONF_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_1SpdHSConf_FWDM is_of_class SpdHStave_4_A_1SpdHSConf_FwDevMode_CLASS


class: SpdHStave_4_A_1SpdHSConf_CLASS
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
            move_to NOT_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED

object: SpdHSConf_4_A_1 is_of_class SpdHStave_4_A_1SpdHSConf_CLASS

objectset: SpdHStave_4_A_1SPDHSCONF_FWSETSTATES is_of_class VOID {SpdHSConf_4_A_1 }
objectset: SpdHStave_4_A_1SPDHSCONF_FWSETACTIONS is_of_class VOID {SpdHSConf_4_A_1 }

class: SpdHStave_4_A_1SpdHSPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_1SpdHSPower_FWDM is_of_class SpdHStave_4_A_1SpdHSPower_FwDevMode_CLASS


objectset: SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES is_of_class VOID {SpdHSPW_4_A_1 }
objectset: SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS is_of_class VOID {SpdHSPW_4_A_1 }


objectset: SpdHStave_4_A_1FWCHILDREN_FWSETACTIONS union {SpdHStave_4_A_1SPDHSCONF_FWSETACTIONS,
	SpdHStave_4_A_1SPDHSPOWER_FWSETACTIONS } is_of_class VOID
objectset: SpdHStave_4_A_1FWCHILDREN_FWSETSTATES union {SpdHStave_4_A_1SPDHSCONF_FWSETSTATES,
	SpdHStave_4_A_1SPDHSPOWER_FWSETSTATES } is_of_class VOID

class: SpdHSTempBus_4_A_2TOP_SpdHSTempLU_CLASS
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
        when ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED_PW	!color: FwStateOKPhysics
        when ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED )  move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

    state: TOO_COOLED	!color: FwStateAttention2
        when ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: NO_TEMP	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: HOT	!color: FwStateAttention3
        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: UNDEFINED	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when (  ( all_in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW


object: SpdHSTempBus_4_A_2 is_of_class SpdHSTempBus_4_A_2TOP_SpdHSTempLU_CLASS

class: SpdHSTempBus_4_A_2SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_2SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_2SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_2SpdHSTemp_FWDM is_of_class SpdHSTempBus_4_A_2SpdHSTemp_FwDevMode_CLASS


class: SpdHSTempBus_4_A_2SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_2 is_of_class SpdHSTempBus_4_A_2SpdHSTemp_CLASS

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_2 is_of_class SpdHSTempBus_4_A_2SpdHSTemp_CLASS

objectset: SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_2,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_2 }
objectset: SpdHSTempBus_4_A_2SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_2,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_2 }

class: SpdHSTempBus_4_A_2FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_2FwDevMajority_FWDM is_of_class SpdHSTempBus_4_A_2FwDevMajority_FwDevMode_CLASS


class: SpdHSTempBus_4_A_2FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHSTempBus_4_A_2:SpdHSTemp_FWMAJ is_of_class SpdHSTempBus_4_A_2FwDevMajority_CLASS

objectset: SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_2:SpdHSTemp_FWMAJ }
objectset: SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_2:SpdHSTemp_FWMAJ }


objectset: SpdHSTempBus_4_A_2FWCHILDREN_FWSETACTIONS union {SpdHSTempBus_4_A_2SPDHSTEMP_FWSETACTIONS,
	SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdHSTempBus_4_A_2FWCHILDREN_FWSETSTATES union {SpdHSTempBus_4_A_2SPDHSTEMP_FWSETSTATES,
	SpdHSTempBus_4_A_2FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdHSPW_4_A_2TOP_SpdHSPower_CLASS
!panel: SpdHSPower.pnl
    parameters: int Executing = 0
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM        

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM    

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS  
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 1
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
    state: MCM_ON	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or 
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES not_in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BIAS_INTERMEDIATE

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS    
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED )  do RESET_CTKR

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do GO_BEAM_TUNING

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_ON

        when (  ( all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES not_in_state READY ) and
( all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BUS_OFF

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            move_to BEAM_TUNING
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS            
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BIAS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS   
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) or ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  move_to RUMP_DOWN_MCM                       

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS        
        action: GO_STBY_CONFIGURED	!visible: 1
            move_to STANDBY
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS        
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state READY )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_BEAM_TUN

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS                               
    state: RUMP_DOWN_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES in_state {MCM_ON,OFF} ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS         
    state: RUMP_UP_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_OFF	!visible: 0
            do SWITCH_OFFall_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BUS_OFF

        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        when (  ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BIAS_INTERMEDIATE

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED   
    state: CHAN_FAULT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: HOT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES not_in_state OFF )  do BUS_OFF

        when ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state {READY,RAMP_UP_READY,INTERMEDIATE} )  do BIAS_OFF

        when ( any_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES not_in_state STANDBY )  do RESET_CTKR

        when (  ( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and 
( any_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and        
( all_in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES in_state HOT )  ) do MCM_OFF

        action: GO_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
    state: TEMP_NO_CONTROL	!color: FwStateAttention2
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 1
            do RESET all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS

object: SpdHSPW_4_A_2 is_of_class SpdHSPW_4_A_2TOP_SpdHSPower_CLASS

class: SpdHSPW_4_A_2FwCaenChannelSpdLV_MCM_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_2FwCaenChannelSpdLV_MCM_FWDM is_of_class SpdHSPW_4_A_2FwCaenChannelSpdLV_MCM_FwDevMode_CLASS


class: SpdHSPW_4_A_2FwCaenChannelSpdLV_MCM_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdMCM_4_A_2 is_of_class SpdHSPW_4_A_2FwCaenChannelSpdLV_MCM_CLASS

objectset: SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_2 }
objectset: SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_2 }

class: SpdHSPW_4_A_2FwCaenChannelSpdLV_BUS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_2FwCaenChannelSpdLV_BUS_FWDM is_of_class SpdHSPW_4_A_2FwCaenChannelSpdLV_BUS_FwDevMode_CLASS


class: SpdHSPW_4_A_2FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdBUS_4_A_2 is_of_class SpdHSPW_4_A_2FwCaenChannelSpdLV_BUS_CLASS

objectset: SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_2 }
objectset: SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_2 }

class: SpdHSPW_4_A_2FwCaenChannelSpdHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_2FwCaenChannelSpdHV_FWDM is_of_class SpdHSPW_4_A_2FwCaenChannelSpdHV_FwDevMode_CLASS


class: SpdHSPW_4_A_2FwCaenChannelSpdHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenHVIChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF	!visible: 1
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdHV_4_A_2 is_of_class SpdHSPW_4_A_2FwCaenChannelSpdHV_CLASS

objectset: SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_2 }
objectset: SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_2 }

class: SpdHSPW_4_A_2SpdCMDTrack_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_2SpdCMDTrack_FWDM is_of_class SpdHSPW_4_A_2SpdCMDTrack_FwDevMode_CLASS


class: SpdHSPW_4_A_2SpdCMDTrack_CLASS
!panel: SpdCMDTrack.pnl
    parameters: int Executing = 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: MCM_ON	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED             
        action: RESET	!visible: 0
            move_to STANDBY            
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY

object: SpdCMDTrack_4_A_2 is_of_class SpdHSPW_4_A_2SpdCMDTrack_CLASS

objectset: SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES is_of_class VOID {SpdCMDTrack_4_A_2 }
objectset: SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS is_of_class VOID {SpdCMDTrack_4_A_2 }

class: SpdHSPW_4_A_2SpdHSTempLU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_2SPDHSTEMPLU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_2SPDHSTEMPLU_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_2SpdHSTempLU_FWDM is_of_class SpdHSPW_4_A_2SpdHSTempLU_FwDevMode_CLASS


objectset: SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES is_of_class VOID {SpdHSTempBus_4_A_2 }
objectset: SpdHSPW_4_A_2SPDHSTEMPLU_FWSETACTIONS is_of_class VOID {SpdHSTempBus_4_A_2 }

class: SpdHSPW_4_A_2SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_2SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_2SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_2SpdHSTemp_FWDM is_of_class SpdHSPW_4_A_2SpdHSTemp_FwDevMode_CLASS


class: SpdHSPW_4_A_2SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_2 is_of_class SpdHSPW_4_A_2SpdHSTemp_CLASS

objectset: SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_2 }
objectset: SpdHSPW_4_A_2SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_2 }


objectset: SpdHSPW_4_A_2FWCHILDREN_FWSETACTIONS union {SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETACTIONS,
	SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETACTIONS,
	SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETACTIONS,
	SpdHSPW_4_A_2SPDCMDTRACK_FWSETACTIONS,
	SpdHSPW_4_A_2SPDHSTEMPLU_FWSETACTIONS,
	SpdHSPW_4_A_2SPDHSTEMP_FWSETACTIONS } is_of_class VOID
objectset: SpdHSPW_4_A_2FWCHILDREN_FWSETSTATES union {SpdHSPW_4_A_2FWCAENCHANNELSPDLV_MCM_FWSETSTATES,
	SpdHSPW_4_A_2FWCAENCHANNELSPDLV_BUS_FWSETSTATES,
	SpdHSPW_4_A_2FWCAENCHANNELSPDHV_FWSETSTATES,
	SpdHSPW_4_A_2SPDCMDTRACK_FWSETSTATES,
	SpdHSPW_4_A_2SPDHSTEMPLU_FWSETSTATES,
	SpdHSPW_4_A_2SPDHSTEMP_FWSETSTATES } is_of_class VOID

class: SpdHStave_4_A_2TOP_SpdHalfStave_CLASS
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_UP_BEAM_TUN,RUMP_UP_READY} ) move_to RUMP_UP_STATE

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_DOWN_MCM,RUMP_DOWN_STBY,RUMP_DOWN_BEAM_TUN} ) move_to RUMP_DW_STATE

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to OFF
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do MCM_OFF all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY   
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY           
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to STBY_CONFIGURED
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
            move_to BEAM_TUNING
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to BEAM_TUNING
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to READY        
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to READY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
            move_to READY            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to READY            
    state: CONFIGURING	!color: 
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS            
    state: HOT	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
    state: ERROR_CONFIG	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY


object: SpdHStave_4_A_2 is_of_class SpdHStave_4_A_2TOP_SpdHalfStave_CLASS

class: SpdHStave_4_A_2SpdHSConf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_2SPDHSCONF_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_2SPDHSCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_2SPDHSCONF_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_2SPDHSCONF_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_2SpdHSConf_FWDM is_of_class SpdHStave_4_A_2SpdHSConf_FwDevMode_CLASS


class: SpdHStave_4_A_2SpdHSConf_CLASS
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
            move_to NOT_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED

object: SpdHSConf_4_A_2 is_of_class SpdHStave_4_A_2SpdHSConf_CLASS

objectset: SpdHStave_4_A_2SPDHSCONF_FWSETSTATES is_of_class VOID {SpdHSConf_4_A_2 }
objectset: SpdHStave_4_A_2SPDHSCONF_FWSETACTIONS is_of_class VOID {SpdHSConf_4_A_2 }

class: SpdHStave_4_A_2SpdHSPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_2SpdHSPower_FWDM is_of_class SpdHStave_4_A_2SpdHSPower_FwDevMode_CLASS


objectset: SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES is_of_class VOID {SpdHSPW_4_A_2 }
objectset: SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS is_of_class VOID {SpdHSPW_4_A_2 }


objectset: SpdHStave_4_A_2FWCHILDREN_FWSETACTIONS union {SpdHStave_4_A_2SPDHSCONF_FWSETACTIONS,
	SpdHStave_4_A_2SPDHSPOWER_FWSETACTIONS } is_of_class VOID
objectset: SpdHStave_4_A_2FWCHILDREN_FWSETSTATES union {SpdHStave_4_A_2SPDHSCONF_FWSETSTATES,
	SpdHStave_4_A_2SPDHSPOWER_FWSETSTATES } is_of_class VOID

class: SpdHSTempBus_4_A_3TOP_SpdHSTempLU_CLASS
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
        when ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED_PW	!color: FwStateOKPhysics
        when ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED )  move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

    state: TOO_COOLED	!color: FwStateAttention2
        when ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: NO_TEMP	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: HOT	!color: FwStateAttention3
        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: UNDEFINED	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when (  ( all_in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW


object: SpdHSTempBus_4_A_3 is_of_class SpdHSTempBus_4_A_3TOP_SpdHSTempLU_CLASS

class: SpdHSTempBus_4_A_3SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_3SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_3SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_3SpdHSTemp_FWDM is_of_class SpdHSTempBus_4_A_3SpdHSTemp_FwDevMode_CLASS


class: SpdHSTempBus_4_A_3SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_3 is_of_class SpdHSTempBus_4_A_3SpdHSTemp_CLASS

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_3 is_of_class SpdHSTempBus_4_A_3SpdHSTemp_CLASS

objectset: SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_3,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_3 }
objectset: SpdHSTempBus_4_A_3SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_3,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_3 }

class: SpdHSTempBus_4_A_3FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_3FwDevMajority_FWDM is_of_class SpdHSTempBus_4_A_3FwDevMajority_FwDevMode_CLASS


class: SpdHSTempBus_4_A_3FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHSTempBus_4_A_3:SpdHSTemp_FWMAJ is_of_class SpdHSTempBus_4_A_3FwDevMajority_CLASS

objectset: SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_3:SpdHSTemp_FWMAJ }
objectset: SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_3:SpdHSTemp_FWMAJ }


objectset: SpdHSTempBus_4_A_3FWCHILDREN_FWSETACTIONS union {SpdHSTempBus_4_A_3SPDHSTEMP_FWSETACTIONS,
	SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdHSTempBus_4_A_3FWCHILDREN_FWSETSTATES union {SpdHSTempBus_4_A_3SPDHSTEMP_FWSETSTATES,
	SpdHSTempBus_4_A_3FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdHSPW_4_A_3TOP_SpdHSPower_CLASS
!panel: SpdHSPower.pnl
    parameters: int Executing = 0
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM        

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM    

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS  
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 1
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
    state: MCM_ON	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or 
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES not_in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BIAS_INTERMEDIATE

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS    
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED )  do RESET_CTKR

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do GO_BEAM_TUNING

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_ON

        when (  ( all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES not_in_state READY ) and
( all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BUS_OFF

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            move_to BEAM_TUNING
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS            
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BIAS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS   
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) or ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  move_to RUMP_DOWN_MCM                       

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS        
        action: GO_STBY_CONFIGURED	!visible: 1
            move_to STANDBY
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS        
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state READY )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_BEAM_TUN

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS                               
    state: RUMP_DOWN_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES in_state {MCM_ON,OFF} ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS         
    state: RUMP_UP_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_OFF	!visible: 0
            do SWITCH_OFFall_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BUS_OFF

        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        when (  ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BIAS_INTERMEDIATE

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED   
    state: CHAN_FAULT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: HOT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES not_in_state OFF )  do BUS_OFF

        when ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state {READY,RAMP_UP_READY,INTERMEDIATE} )  do BIAS_OFF

        when ( any_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES not_in_state STANDBY )  do RESET_CTKR

        when (  ( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and 
( any_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and        
( all_in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES in_state HOT )  ) do MCM_OFF

        action: GO_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
    state: TEMP_NO_CONTROL	!color: FwStateAttention2
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 1
            do RESET all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS

object: SpdHSPW_4_A_3 is_of_class SpdHSPW_4_A_3TOP_SpdHSPower_CLASS

class: SpdHSPW_4_A_3FwCaenChannelSpdLV_MCM_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_3FwCaenChannelSpdLV_MCM_FWDM is_of_class SpdHSPW_4_A_3FwCaenChannelSpdLV_MCM_FwDevMode_CLASS


class: SpdHSPW_4_A_3FwCaenChannelSpdLV_MCM_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdMCM_4_A_3 is_of_class SpdHSPW_4_A_3FwCaenChannelSpdLV_MCM_CLASS

objectset: SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_3 }
objectset: SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_3 }

class: SpdHSPW_4_A_3FwCaenChannelSpdLV_BUS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_3FwCaenChannelSpdLV_BUS_FWDM is_of_class SpdHSPW_4_A_3FwCaenChannelSpdLV_BUS_FwDevMode_CLASS


class: SpdHSPW_4_A_3FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdBUS_4_A_3 is_of_class SpdHSPW_4_A_3FwCaenChannelSpdLV_BUS_CLASS

objectset: SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_3 }
objectset: SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_3 }

class: SpdHSPW_4_A_3FwCaenChannelSpdHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_3FwCaenChannelSpdHV_FWDM is_of_class SpdHSPW_4_A_3FwCaenChannelSpdHV_FwDevMode_CLASS


class: SpdHSPW_4_A_3FwCaenChannelSpdHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenHVIChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF	!visible: 1
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdHV_4_A_3 is_of_class SpdHSPW_4_A_3FwCaenChannelSpdHV_CLASS

objectset: SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_3 }
objectset: SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_3 }

class: SpdHSPW_4_A_3SpdCMDTrack_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_3SpdCMDTrack_FWDM is_of_class SpdHSPW_4_A_3SpdCMDTrack_FwDevMode_CLASS


class: SpdHSPW_4_A_3SpdCMDTrack_CLASS
!panel: SpdCMDTrack.pnl
    parameters: int Executing = 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: MCM_ON	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED             
        action: RESET	!visible: 0
            move_to STANDBY            
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY

object: SpdCMDTrack_4_A_3 is_of_class SpdHSPW_4_A_3SpdCMDTrack_CLASS

objectset: SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES is_of_class VOID {SpdCMDTrack_4_A_3 }
objectset: SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS is_of_class VOID {SpdCMDTrack_4_A_3 }

class: SpdHSPW_4_A_3SpdHSTempLU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_3SPDHSTEMPLU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_3SPDHSTEMPLU_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_3SpdHSTempLU_FWDM is_of_class SpdHSPW_4_A_3SpdHSTempLU_FwDevMode_CLASS


objectset: SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES is_of_class VOID {SpdHSTempBus_4_A_3 }
objectset: SpdHSPW_4_A_3SPDHSTEMPLU_FWSETACTIONS is_of_class VOID {SpdHSTempBus_4_A_3 }

class: SpdHSPW_4_A_3SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_3SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_3SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_3SpdHSTemp_FWDM is_of_class SpdHSPW_4_A_3SpdHSTemp_FwDevMode_CLASS


class: SpdHSPW_4_A_3SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_3 is_of_class SpdHSPW_4_A_3SpdHSTemp_CLASS

objectset: SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_3 }
objectset: SpdHSPW_4_A_3SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_3 }


objectset: SpdHSPW_4_A_3FWCHILDREN_FWSETACTIONS union {SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETACTIONS,
	SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETACTIONS,
	SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETACTIONS,
	SpdHSPW_4_A_3SPDCMDTRACK_FWSETACTIONS,
	SpdHSPW_4_A_3SPDHSTEMPLU_FWSETACTIONS,
	SpdHSPW_4_A_3SPDHSTEMP_FWSETACTIONS } is_of_class VOID
objectset: SpdHSPW_4_A_3FWCHILDREN_FWSETSTATES union {SpdHSPW_4_A_3FWCAENCHANNELSPDLV_MCM_FWSETSTATES,
	SpdHSPW_4_A_3FWCAENCHANNELSPDLV_BUS_FWSETSTATES,
	SpdHSPW_4_A_3FWCAENCHANNELSPDHV_FWSETSTATES,
	SpdHSPW_4_A_3SPDCMDTRACK_FWSETSTATES,
	SpdHSPW_4_A_3SPDHSTEMPLU_FWSETSTATES,
	SpdHSPW_4_A_3SPDHSTEMP_FWSETSTATES } is_of_class VOID

class: SpdHStave_4_A_3TOP_SpdHalfStave_CLASS
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_UP_BEAM_TUN,RUMP_UP_READY} ) move_to RUMP_UP_STATE

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_DOWN_MCM,RUMP_DOWN_STBY,RUMP_DOWN_BEAM_TUN} ) move_to RUMP_DW_STATE

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to OFF
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do MCM_OFF all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY   
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY           
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to STBY_CONFIGURED
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
            move_to BEAM_TUNING
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to BEAM_TUNING
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to READY        
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to READY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
            move_to READY            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to READY            
    state: CONFIGURING	!color: 
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS            
    state: HOT	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
    state: ERROR_CONFIG	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY


object: SpdHStave_4_A_3 is_of_class SpdHStave_4_A_3TOP_SpdHalfStave_CLASS

class: SpdHStave_4_A_3SpdHSConf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_3SPDHSCONF_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_3SPDHSCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_3SPDHSCONF_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_3SPDHSCONF_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_3SpdHSConf_FWDM is_of_class SpdHStave_4_A_3SpdHSConf_FwDevMode_CLASS


class: SpdHStave_4_A_3SpdHSConf_CLASS
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
            move_to NOT_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED

object: SpdHSConf_4_A_3 is_of_class SpdHStave_4_A_3SpdHSConf_CLASS

objectset: SpdHStave_4_A_3SPDHSCONF_FWSETSTATES is_of_class VOID {SpdHSConf_4_A_3 }
objectset: SpdHStave_4_A_3SPDHSCONF_FWSETACTIONS is_of_class VOID {SpdHSConf_4_A_3 }

class: SpdHStave_4_A_3SpdHSPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_3SpdHSPower_FWDM is_of_class SpdHStave_4_A_3SpdHSPower_FwDevMode_CLASS


objectset: SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES is_of_class VOID {SpdHSPW_4_A_3 }
objectset: SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS is_of_class VOID {SpdHSPW_4_A_3 }


objectset: SpdHStave_4_A_3FWCHILDREN_FWSETACTIONS union {SpdHStave_4_A_3SPDHSCONF_FWSETACTIONS,
	SpdHStave_4_A_3SPDHSPOWER_FWSETACTIONS } is_of_class VOID
objectset: SpdHStave_4_A_3FWCHILDREN_FWSETSTATES union {SpdHStave_4_A_3SPDHSCONF_FWSETSTATES,
	SpdHStave_4_A_3SPDHSPOWER_FWSETSTATES } is_of_class VOID

class: SpdHSTempBus_4_A_4TOP_SpdHSTempLU_CLASS
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
        when ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED_PW	!color: FwStateOKPhysics
        when ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED )  move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

    state: TOO_COOLED	!color: FwStateAttention2
        when ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: NO_TEMP	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: HOT	!color: FwStateAttention3
        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: UNDEFINED	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when (  ( all_in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW


object: SpdHSTempBus_4_A_4 is_of_class SpdHSTempBus_4_A_4TOP_SpdHSTempLU_CLASS

class: SpdHSTempBus_4_A_4SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_4SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_4SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_4SpdHSTemp_FWDM is_of_class SpdHSTempBus_4_A_4SpdHSTemp_FwDevMode_CLASS


class: SpdHSTempBus_4_A_4SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_4 is_of_class SpdHSTempBus_4_A_4SpdHSTemp_CLASS

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_4 is_of_class SpdHSTempBus_4_A_4SpdHSTemp_CLASS

objectset: SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_4,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_4 }
objectset: SpdHSTempBus_4_A_4SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_4,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_4 }

class: SpdHSTempBus_4_A_4FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_4FwDevMajority_FWDM is_of_class SpdHSTempBus_4_A_4FwDevMajority_FwDevMode_CLASS


class: SpdHSTempBus_4_A_4FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHSTempBus_4_A_4:SpdHSTemp_FWMAJ is_of_class SpdHSTempBus_4_A_4FwDevMajority_CLASS

objectset: SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_4:SpdHSTemp_FWMAJ }
objectset: SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_4:SpdHSTemp_FWMAJ }


objectset: SpdHSTempBus_4_A_4FWCHILDREN_FWSETACTIONS union {SpdHSTempBus_4_A_4SPDHSTEMP_FWSETACTIONS,
	SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdHSTempBus_4_A_4FWCHILDREN_FWSETSTATES union {SpdHSTempBus_4_A_4SPDHSTEMP_FWSETSTATES,
	SpdHSTempBus_4_A_4FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdHSPW_4_A_4TOP_SpdHSPower_CLASS
!panel: SpdHSPower.pnl
    parameters: int Executing = 0
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM        

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM    

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS  
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 1
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
    state: MCM_ON	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or 
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES not_in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BIAS_INTERMEDIATE

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS    
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED )  do RESET_CTKR

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do GO_BEAM_TUNING

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_ON

        when (  ( all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES not_in_state READY ) and
( all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BUS_OFF

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            move_to BEAM_TUNING
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS            
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BIAS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS   
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) or ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  move_to RUMP_DOWN_MCM                       

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS        
        action: GO_STBY_CONFIGURED	!visible: 1
            move_to STANDBY
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS        
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state READY )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_BEAM_TUN

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS                               
    state: RUMP_DOWN_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES in_state {MCM_ON,OFF} ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS         
    state: RUMP_UP_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_OFF	!visible: 0
            do SWITCH_OFFall_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BUS_OFF

        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        when (  ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BIAS_INTERMEDIATE

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED   
    state: CHAN_FAULT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: HOT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES not_in_state OFF )  do BUS_OFF

        when ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state {READY,RAMP_UP_READY,INTERMEDIATE} )  do BIAS_OFF

        when ( any_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES not_in_state STANDBY )  do RESET_CTKR

        when (  ( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and 
( any_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and        
( all_in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES in_state HOT )  ) do MCM_OFF

        action: GO_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
    state: TEMP_NO_CONTROL	!color: FwStateAttention2
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 1
            do RESET all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS

object: SpdHSPW_4_A_4 is_of_class SpdHSPW_4_A_4TOP_SpdHSPower_CLASS

class: SpdHSPW_4_A_4FwCaenChannelSpdLV_MCM_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_4FwCaenChannelSpdLV_MCM_FWDM is_of_class SpdHSPW_4_A_4FwCaenChannelSpdLV_MCM_FwDevMode_CLASS


class: SpdHSPW_4_A_4FwCaenChannelSpdLV_MCM_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdMCM_4_A_4 is_of_class SpdHSPW_4_A_4FwCaenChannelSpdLV_MCM_CLASS

objectset: SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_4 }
objectset: SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_4 }

class: SpdHSPW_4_A_4FwCaenChannelSpdLV_BUS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_4FwCaenChannelSpdLV_BUS_FWDM is_of_class SpdHSPW_4_A_4FwCaenChannelSpdLV_BUS_FwDevMode_CLASS


class: SpdHSPW_4_A_4FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdBUS_4_A_4 is_of_class SpdHSPW_4_A_4FwCaenChannelSpdLV_BUS_CLASS

objectset: SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_4 }
objectset: SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_4 }

class: SpdHSPW_4_A_4FwCaenChannelSpdHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_4FwCaenChannelSpdHV_FWDM is_of_class SpdHSPW_4_A_4FwCaenChannelSpdHV_FwDevMode_CLASS


class: SpdHSPW_4_A_4FwCaenChannelSpdHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenHVIChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF	!visible: 1
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdHV_4_A_4 is_of_class SpdHSPW_4_A_4FwCaenChannelSpdHV_CLASS

objectset: SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_4 }
objectset: SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_4 }

class: SpdHSPW_4_A_4SpdCMDTrack_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_4SpdCMDTrack_FWDM is_of_class SpdHSPW_4_A_4SpdCMDTrack_FwDevMode_CLASS


class: SpdHSPW_4_A_4SpdCMDTrack_CLASS
!panel: SpdCMDTrack.pnl
    parameters: int Executing = 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: MCM_ON	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED             
        action: RESET	!visible: 0
            move_to STANDBY            
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY

object: SpdCMDTrack_4_A_4 is_of_class SpdHSPW_4_A_4SpdCMDTrack_CLASS

objectset: SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES is_of_class VOID {SpdCMDTrack_4_A_4 }
objectset: SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS is_of_class VOID {SpdCMDTrack_4_A_4 }

class: SpdHSPW_4_A_4SpdHSTempLU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_4SPDHSTEMPLU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_4SPDHSTEMPLU_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_4SpdHSTempLU_FWDM is_of_class SpdHSPW_4_A_4SpdHSTempLU_FwDevMode_CLASS


objectset: SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES is_of_class VOID {SpdHSTempBus_4_A_4 }
objectset: SpdHSPW_4_A_4SPDHSTEMPLU_FWSETACTIONS is_of_class VOID {SpdHSTempBus_4_A_4 }

class: SpdHSPW_4_A_4SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_4SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_4SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_4SpdHSTemp_FWDM is_of_class SpdHSPW_4_A_4SpdHSTemp_FwDevMode_CLASS


class: SpdHSPW_4_A_4SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_4 is_of_class SpdHSPW_4_A_4SpdHSTemp_CLASS

objectset: SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_4 }
objectset: SpdHSPW_4_A_4SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_4 }


objectset: SpdHSPW_4_A_4FWCHILDREN_FWSETACTIONS union {SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETACTIONS,
	SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETACTIONS,
	SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETACTIONS,
	SpdHSPW_4_A_4SPDCMDTRACK_FWSETACTIONS,
	SpdHSPW_4_A_4SPDHSTEMPLU_FWSETACTIONS,
	SpdHSPW_4_A_4SPDHSTEMP_FWSETACTIONS } is_of_class VOID
objectset: SpdHSPW_4_A_4FWCHILDREN_FWSETSTATES union {SpdHSPW_4_A_4FWCAENCHANNELSPDLV_MCM_FWSETSTATES,
	SpdHSPW_4_A_4FWCAENCHANNELSPDLV_BUS_FWSETSTATES,
	SpdHSPW_4_A_4FWCAENCHANNELSPDHV_FWSETSTATES,
	SpdHSPW_4_A_4SPDCMDTRACK_FWSETSTATES,
	SpdHSPW_4_A_4SPDHSTEMPLU_FWSETSTATES,
	SpdHSPW_4_A_4SPDHSTEMP_FWSETSTATES } is_of_class VOID

class: SpdHStave_4_A_4TOP_SpdHalfStave_CLASS
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_UP_BEAM_TUN,RUMP_UP_READY} ) move_to RUMP_UP_STATE

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_DOWN_MCM,RUMP_DOWN_STBY,RUMP_DOWN_BEAM_TUN} ) move_to RUMP_DW_STATE

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to OFF
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do MCM_OFF all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY   
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY           
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to STBY_CONFIGURED
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
            move_to BEAM_TUNING
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to BEAM_TUNING
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to READY        
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to READY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
            move_to READY            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to READY            
    state: CONFIGURING	!color: 
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS            
    state: HOT	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
    state: ERROR_CONFIG	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY


object: SpdHStave_4_A_4 is_of_class SpdHStave_4_A_4TOP_SpdHalfStave_CLASS

class: SpdHStave_4_A_4SpdHSConf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_4SPDHSCONF_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_4SPDHSCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_4SPDHSCONF_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_4SPDHSCONF_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_4SpdHSConf_FWDM is_of_class SpdHStave_4_A_4SpdHSConf_FwDevMode_CLASS


class: SpdHStave_4_A_4SpdHSConf_CLASS
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
            move_to NOT_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED

object: SpdHSConf_4_A_4 is_of_class SpdHStave_4_A_4SpdHSConf_CLASS

objectset: SpdHStave_4_A_4SPDHSCONF_FWSETSTATES is_of_class VOID {SpdHSConf_4_A_4 }
objectset: SpdHStave_4_A_4SPDHSCONF_FWSETACTIONS is_of_class VOID {SpdHSConf_4_A_4 }

class: SpdHStave_4_A_4SpdHSPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_4SpdHSPower_FWDM is_of_class SpdHStave_4_A_4SpdHSPower_FwDevMode_CLASS


objectset: SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES is_of_class VOID {SpdHSPW_4_A_4 }
objectset: SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS is_of_class VOID {SpdHSPW_4_A_4 }


objectset: SpdHStave_4_A_4FWCHILDREN_FWSETACTIONS union {SpdHStave_4_A_4SPDHSCONF_FWSETACTIONS,
	SpdHStave_4_A_4SPDHSPOWER_FWSETACTIONS } is_of_class VOID
objectset: SpdHStave_4_A_4FWCHILDREN_FWSETSTATES union {SpdHStave_4_A_4SPDHSCONF_FWSETSTATES,
	SpdHStave_4_A_4SPDHSPOWER_FWSETSTATES } is_of_class VOID

class: SpdHSTempBus_4_A_5TOP_SpdHSTempLU_CLASS
!panel: SpdHSTemp.pnl
    parameters: int Executing = 0
    state: AMB_TEMP	!color: FwStateAttention1
        when ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED_PW	!color: FwStateOKPhysics
        when ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED )  move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

    state: TOO_COOLED	!color: FwStateAttention2
        when ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: NO_TEMP	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: HOT	!color: FwStateAttention3
        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state UNDEFINED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  ) move_to UNDEFINED

        when (  ( all_in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {TOO_COOLED,COOLED} )  )  ) move_to AMB_TEMP

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {COOLED} )  )  ) move_to TOO_COOLED

        when ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED ) move_to COOLED

    state: UNDEFINED	!color: FwStateAttention3
        when ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT ) move_to HOT

        when (  ( all_in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) or
( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state NO_TEMP )  )  move_to NO_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW )  ) move_to COOLED_PW

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state AMB_TEMP )  ) move_to AMB_TEMP

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state TOO_COOLED )  ) move_to TOO_COOLED

        when (  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {NO_TEMP,UNDEFINED} ) and 
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED )  ) move_to COOLED

        when ( ( all_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) or
(  ( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state COOLED_PW ) and
( any_in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES in_state {AMB_TEMP,TOO_COOLED,COOLED} )  )  ) move_to COOLED_PW


object: SpdHSTempBus_4_A_5 is_of_class SpdHSTempBus_4_A_5TOP_SpdHSTempLU_CLASS

class: SpdHSTempBus_4_A_5SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_5SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_5SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_5SpdHSTemp_FWDM is_of_class SpdHSTempBus_4_A_5SpdHSTemp_FwDevMode_CLASS


class: SpdHSTempBus_4_A_5SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_5 is_of_class SpdHSTempBus_4_A_5SpdHSTemp_CLASS

object: SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_5 is_of_class SpdHSTempBus_4_A_5SpdHSTemp_CLASS

objectset: SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_5,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_5 }
objectset: SpdHSTempBus_4_A_5SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempBusDSS_4_A_5,
	SPDTempMonitor:SpdHSector_4_A:SpdTempBusFEE_4_A_5 }

class: SpdHSTempBus_4_A_5FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: SpdHSTempBus_4_A_5FwDevMajority_FWDM is_of_class SpdHSTempBus_4_A_5FwDevMajority_FwDevMode_CLASS


class: SpdHSTempBus_4_A_5FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHSTempBus_4_A_5:SpdHSTemp_FWMAJ is_of_class SpdHSTempBus_4_A_5FwDevMajority_CLASS

objectset: SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_5:SpdHSTemp_FWMAJ }
objectset: SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSTempBus_4_A_5:SpdHSTemp_FWMAJ }


objectset: SpdHSTempBus_4_A_5FWCHILDREN_FWSETACTIONS union {SpdHSTempBus_4_A_5SPDHSTEMP_FWSETACTIONS,
	SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: SpdHSTempBus_4_A_5FWCHILDREN_FWSETSTATES union {SpdHSTempBus_4_A_5SPDHSTEMP_FWSETSTATES,
	SpdHSTempBus_4_A_5FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

class: SpdHSPW_4_A_5TOP_SpdHSPower_CLASS
!panel: SpdHSPower.pnl
    parameters: int Executing = 0
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM        

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state OFF )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state RAMP_UP_READY ) and
( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  )  move_to RUMP_UP_MCM    

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS  
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 1
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
    state: MCM_ON	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or 
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_STBY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  do MCM_OFF

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES not_in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BIAS_INTERMEDIATE

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS          
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS    
    state: STANDBY	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED )  do RESET_CTKR

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do GO_BEAM_TUNING

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_BEAM_TUN

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_ON

        when (  ( all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES not_in_state READY ) and
( all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES not_in_state STANDBY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BUS_OFF

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            move_to BEAM_TUNING
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS            
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BIAS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS   
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) or ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state READY ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_READY ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_UP_INTER ) or
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state RAMP_UP_READY ) )  )  move_to RUMP_UP_READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state MCM_ON )  move_to RUMP_DOWN_MCM                       

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS        
        action: GO_STBY_CONFIGURED	!visible: 1
            move_to STANDBY
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS        
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state READY )  do RESET_CTKR

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state {OFF,MCM_ON} ) and
( ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state {RAMP_DW_INTER,RAMP_DW_OFF} ) or 
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state {RAMP_DW_OFF,OFF} ) )  )  move_to RUMP_DOWN_MCM

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state STBY_CONFIGURED ) and
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_STBY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state BEAM_TUNING ) and
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state RAMP_DW_INTER )  )  move_to RUMP_DOWN_BEAM_TUN

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS                               
    state: RUMP_DOWN_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )   move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES in_state {MCM_ON,OFF} ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS         
    state: RUMP_UP_MCM	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_OFF	!visible: 0
            do SWITCH_OFFall_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE )  )  do BIAS_OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF )  )  do BUS_OFF

        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_STBY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        when (  ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY )  )  do BIAS_INTERMEDIATE

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_BEAM_TUN	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF )  )  do BUS_ON

        action: BUS_ON	!visible: 0
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: RUMP_DOWN_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_BEAM_TUNING	!visible: 0
            do GO_BEAM_TUNING all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: RUMP_UP_READY	!color: FwStateAttention1
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: GO_STBY_CONFIGURED	!visible: 0
            do GO_STBY_CONFIGURED all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED   
    state: CHAN_FAULT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state UNDEFINED ) or
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state UNDEFINED )   )  move_to UNDEFINED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  )  move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: UNDEFINED	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when (  ( any_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state HOT ) or 
( any_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  )move_to HOT

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state TRIPPED )  move_to TRIPPED

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state NO_CONTROL )  move_to NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES in_state CHAN_FAULT )  move_to CHAN_FAULT

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to OFF_NOT_COOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state OFF ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to OFF

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state COOLED ) or
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state TOO_COOLED ) )  )  move_to MCM_ON

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state OFF ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and
( ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state COOLED ) and ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state TOO_COOLED ) )  ) move_to MCM_ON_NCOOLED

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state INTERMEDIATE ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to STANDBY

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state READY ) and
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY )  )  move_to READY

        action: RESET	!visible: 1
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            do RESET all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
    state: HOT	!color: FwStateAttention3
        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state ERROR ) and 
( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES not_in_state FAKE )  )move_to NO_CONTROL

        when (  ( all_in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES in_state FAKE ) and 
( all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES not_in_state FAKE )  )move_to TEMP_NO_CONTROL

        when ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES not_in_state OFF )  do BUS_OFF

        when ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state {READY,RAMP_UP_READY,INTERMEDIATE} )  do BIAS_OFF

        when ( any_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES not_in_state STANDBY )  do RESET_CTKR

        when (  ( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES in_state READY ) and 
( any_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES in_state OFF ) and        
( all_in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES in_state HOT )  ) do MCM_OFF

        action: GO_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 0
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
    state: TEMP_NO_CONTROL	!color: FwStateAttention2
        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: BIAS_INTERMEDIATE	!visible: 0
            do GO_INTERMEDIATE all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: BUS_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
        action: BIAS_OFF	!visible: 0
            do GO_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
        action: MCM_OFF	!visible: 0
            do SWITCH_OFF all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
        action: RESET_CTKR	!visible: 1
            do RESET all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
        action: RELEASE	!visible: 1
            move_to OFF_NOT_COOLED
        action: GO_MCM_ONLY	!visible: 0
            do GO_MCM_ONLY all_in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            do GO_READY all_in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS

object: SpdHSPW_4_A_5 is_of_class SpdHSPW_4_A_5TOP_SpdHSPower_CLASS

class: SpdHSPW_4_A_5FwCaenChannelSpdLV_MCM_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_5FwCaenChannelSpdLV_MCM_FWDM is_of_class SpdHSPW_4_A_5FwCaenChannelSpdLV_MCM_FwDevMode_CLASS


class: SpdHSPW_4_A_5FwCaenChannelSpdLV_MCM_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdMCM_4_A_5 is_of_class SpdHSPW_4_A_5FwCaenChannelSpdLV_MCM_CLASS

objectset: SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_5 }
objectset: SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdMCM_4_A_5 }

class: SpdHSPW_4_A_5FwCaenChannelSpdLV_BUS_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_5FwCaenChannelSpdLV_BUS_FWDM is_of_class SpdHSPW_4_A_5FwCaenChannelSpdLV_BUS_FwDevMode_CLASS


class: SpdHSPW_4_A_5FwCaenChannelSpdLV_BUS_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenLVChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 2
        action: RELOAD	!visible: 2
        action: SWITCH_OFF	!visible: 2
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 2
        action: SWITCH_ON	!visible: 2
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 2
        action: GO_OFF	!visible: 2
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 2
        action: RELOAD	!visible: 2
        action: GO_OFF	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdBUS_4_A_5 is_of_class SpdHSPW_4_A_5FwCaenChannelSpdLV_BUS_CLASS

objectset: SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_5 }
objectset: SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdBUS_4_A_5 }

class: SpdHSPW_4_A_5FwCaenChannelSpdHV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_5FwCaenChannelSpdHV_FWDM is_of_class SpdHSPW_4_A_5FwCaenChannelSpdHV_FwDevMode_CLASS


class: SpdHSPW_4_A_5FwCaenChannelSpdHV_CLASS/associated
!panel: FwCaenChannel|dcsCaen/dcsCaenHVIChannel.pnl
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF	!visible: 1
        action: GO_READY	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: GO_READY	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: RAMP_UP_READY	!color: FwStateAttention1
        action: SWITCH_OFF	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: READY	!color: FwStateOKPhysics
        action: SWITCH_OFF	!visible: 1
        action: RELOAD	!visible: 1
        action: GO_OFF	!visible: 1
        action: GO_INTERMEDIATE	!visible: 1
    state: TRIPPED	!color: FwStateAttention2
        action: RESET	!visible: 1
        action: SWITCH_OFF	!visible: 1
        action: SWITCH_ON	!visible: 1
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET	!visible: 1
    state: CHAN_FAULT	!color: FwStateAttention3
        action: RESET	!visible: 1
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET	!visible: 1

object: SPDPower:SpdHSector_4_A:spdHV_4_A_5 is_of_class SpdHSPW_4_A_5FwCaenChannelSpdHV_CLASS

objectset: SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_5 }
objectset: SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS is_of_class VOID {SPDPower:SpdHSector_4_A:spdHV_4_A_5 }

class: SpdHSPW_4_A_5SpdCMDTrack_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_5SpdCMDTrack_FWDM is_of_class SpdHSPW_4_A_5SpdCMDTrack_FwDevMode_CLASS


class: SpdHSPW_4_A_5SpdCMDTrack_CLASS
!panel: SpdCMDTrack.pnl
    parameters: int Executing = 0
    state: STANDBY	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: MCM_ON	!color: FwStateOKNotPhysics
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: GO_READY	!visible: 0
            move_to READY
        action: RESET	!visible: 0
            move_to STANDBY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: GO_READY	!visible: 0
            move_to READY
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED             
        action: RESET	!visible: 0
            move_to STANDBY            
    state: READY	!color: FwStateOKPhysics
        action: GO_STBY_CONFIGURED	!visible: 0
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 0
            move_to MCM_ON
        action: GO_OFF	!visible: 0
            move_to OFF
        action: GO_BEAM_TUNING	!visible: 0
            move_to BEAM_TUNING             
        action: RESET	!visible: 0
            move_to STANDBY

object: SpdCMDTrack_4_A_5 is_of_class SpdHSPW_4_A_5SpdCMDTrack_CLASS

objectset: SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES is_of_class VOID {SpdCMDTrack_4_A_5 }
objectset: SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS is_of_class VOID {SpdCMDTrack_4_A_5 }

class: SpdHSPW_4_A_5SpdHSTempLU_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_5SPDHSTEMPLU_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_5SPDHSTEMPLU_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_5SpdHSTempLU_FWDM is_of_class SpdHSPW_4_A_5SpdHSTempLU_FwDevMode_CLASS


objectset: SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES is_of_class VOID {SpdHSTempBus_4_A_5 }
objectset: SpdHSPW_4_A_5SPDHSTEMPLU_FWSETACTIONS is_of_class VOID {SpdHSTempBus_4_A_5 }

class: SpdHSPW_4_A_5SpdHSTemp_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES
            remove &VAL_OF_Device from SpdHSPW_4_A_5SPDHSTEMP_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES
            insert &VAL_OF_Device in SpdHSPW_4_A_5SPDHSTEMP_FWSETACTIONS
            move_to READY

object: SpdHSPW_4_A_5SpdHSTemp_FWDM is_of_class SpdHSPW_4_A_5SpdHSTemp_FwDevMode_CLASS


class: SpdHSPW_4_A_5SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics

object: SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_5 is_of_class SpdHSPW_4_A_5SpdHSTemp_CLASS

objectset: SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_5 }
objectset: SpdHSPW_4_A_5SPDHSTEMP_FWSETACTIONS is_of_class VOID {SPDTempMonitor:SpdHSector_4_A:SpdTempMCMFEE_4_A_5 }


objectset: SpdHSPW_4_A_5FWCHILDREN_FWSETACTIONS union {SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETACTIONS,
	SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETACTIONS,
	SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETACTIONS,
	SpdHSPW_4_A_5SPDCMDTRACK_FWSETACTIONS,
	SpdHSPW_4_A_5SPDHSTEMPLU_FWSETACTIONS,
	SpdHSPW_4_A_5SPDHSTEMP_FWSETACTIONS } is_of_class VOID
objectset: SpdHSPW_4_A_5FWCHILDREN_FWSETSTATES union {SpdHSPW_4_A_5FWCAENCHANNELSPDLV_MCM_FWSETSTATES,
	SpdHSPW_4_A_5FWCAENCHANNELSPDLV_BUS_FWSETSTATES,
	SpdHSPW_4_A_5FWCAENCHANNELSPDHV_FWSETSTATES,
	SpdHSPW_4_A_5SPDCMDTRACK_FWSETSTATES,
	SpdHSPW_4_A_5SPDHSTEMPLU_FWSETSTATES,
	SpdHSPW_4_A_5SPDHSTEMP_FWSETSTATES } is_of_class VOID

class: SpdHStave_4_A_5TOP_SpdHalfStave_CLASS
!panel: FSM\SpdHalfStave.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF_NOT_COOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_UP_BEAM_TUN,RUMP_UP_READY} ) move_to RUMP_UP_STATE

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_DOWN_MCM,RUMP_DOWN_STBY,RUMP_DOWN_BEAM_TUN} ) move_to RUMP_DW_STATE

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to OFF
    state: MCM_ON_NCOOLED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do MCM_OFF all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY   
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to MCM_ONLY           
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to MCM_ONLY
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to STBY_CONFIGURED            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version 
            move_to STBY_CONFIGURED
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to BEAM_TUNING            
        action: GO_READY	!visible: 1
            do GO_READY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
            move_to BEAM_TUNING
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to BEAM_TUNING
    state: READY	!color: FwStateOKPhysics
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to READY        
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to READY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
            move_to READY            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            move_to CALIBRATING
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            move_to READY            
    state: CONFIGURING	!color: 
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: STOP	!visible: 1
            if ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ONLY )  then
               move_to MCM_ONLY
            endif
            if ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STBY_CONFIGURED )  then
               move_to STBY_CONFIGURED
            endif
            if ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY )  then
               move_to BEAM_TUNING
            endif
    state: ERROR	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
            move_to OFF_NOT_COOLED
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS            
    state: HOT	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
    state: ERROR_CONFIG	!color: FwStateAttention3
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS 
        action: RELEASE	!visible: 1
            do RELEASE all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
        action: RESET	!visible: 1
            do RESET all_in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_READY,RUMP_DOWN_READY} ) move_to MOVING_READY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY

    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state HOT ) move_to HOT

        when ( any_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {ERROR,TRIPPED,NO_CONTROL,UNDEFINED,CHAN_FAULT,TEMP_NO_CONTROL} ) move_to ERROR

        when ( any_in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES in_state ERROR ) move_to ERROR_CONFIG

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_MCM,RUMP_UP_STBY,RUMP_DOWN_MCM,RUMP_DOWN_STBY} ) move_to MOVING_STBY_CONF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state {RUMP_UP_BEAM_TUN,RUMP_DOWN_BEAM_TUN} ) move_to MOVING_BEAM_TUN

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF_NOT_COOLED ) move_to OFF_NOT_COOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state OFF ) move_to OFF

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON_NCOOLED ) move_to MCM_ON_NCOOLED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state MCM_ON ) move_to MCM_ONLY

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state STANDBY ) move_to STBY_CONFIGURED

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( all_in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES in_state READY ) move_to READY


object: SpdHStave_4_A_5 is_of_class SpdHStave_4_A_5TOP_SpdHalfStave_CLASS

class: SpdHStave_4_A_5SpdHSConf_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_5SPDHSCONF_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_5SPDHSCONF_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_5SPDHSCONF_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_5SPDHSCONF_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_5SpdHSConf_FWDM is_of_class SpdHStave_4_A_5SpdHSConf_FwDevMode_CLASS


class: SpdHStave_4_A_5SpdHSConf_CLASS
!panel: SpdHSConf.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: NOT_CONFIGURED	!color: FwStateOKNotPhysics
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: READY	!color: FwStateOKPhysics
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED
        action: CONFIGURE(int RunMonde = 0,int VersionN = 0,string Element = "ALL",int ChipSelect = 1024)	!visible: 1
            move_to READY
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            move_to CALIBRATING
    state: CALIBRATING	!color: FwStateAttention1
        action: STOP_CALIBRATION	!visible: 1
            move_to NOT_CONFIGURED
    state: ERROR	!color: FwStateAttention3
        action: RESET	!visible: 1
            move_to NOT_CONFIGURED

object: SpdHSConf_4_A_5 is_of_class SpdHStave_4_A_5SpdHSConf_CLASS

objectset: SpdHStave_4_A_5SPDHSCONF_FWSETSTATES is_of_class VOID {SpdHSConf_4_A_5 }
objectset: SpdHStave_4_A_5SPDHSCONF_FWSETACTIONS is_of_class VOID {SpdHSConf_4_A_5 }

class: SpdHStave_4_A_5SpdHSPower_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES
            remove &VAL_OF_Device from SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES
            insert &VAL_OF_Device in SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS
            move_to READY

object: SpdHStave_4_A_5SpdHSPower_FWDM is_of_class SpdHStave_4_A_5SpdHSPower_FwDevMode_CLASS


objectset: SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES is_of_class VOID {SpdHSPW_4_A_5 }
objectset: SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS is_of_class VOID {SpdHSPW_4_A_5 }


objectset: SpdHStave_4_A_5FWCHILDREN_FWSETACTIONS union {SpdHStave_4_A_5SPDHSCONF_FWSETACTIONS,
	SpdHStave_4_A_5SPDHSPOWER_FWSETACTIONS } is_of_class VOID
objectset: SpdHStave_4_A_5FWCHILDREN_FWSETSTATES union {SpdHStave_4_A_5SPDHSCONF_FWSETSTATES,
	SpdHStave_4_A_5SPDHSPOWER_FWSETSTATES } is_of_class VOID

class: TOP_SpdHalfSector_CLASS
!panel: FSM\SpdHalfSector.pnl
    parameters: int RunMode = 0, int VersionN = 0, string CalibMode = "ALL"
    state: OFF	!color: FwStateOKNotPhysics
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and       
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and 
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and    
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and         
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and            
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) move_to MIXED

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} )  then
               move_to MIXED
            endif
            move_to MCM_ONLY
    state: MCM_ONLY	!color: FwStateOKNotPhysics
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and    
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and            
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MIXED

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} )  then
               move_to MIXED
            endif
            move_to OFF
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING            
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode= run_mode, version = version) all_in SPDHALFSTAVE_FWSETACTIONS
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and     
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and           
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and    
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and            
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state  STBY_CONFIGURED ) move_to MIXED

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} )  then
               move_to MIXED
            endif
            move_to OFF
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} )  then
               move_to MIXED
            endif
            move_to MCM_ONLY
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING               
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state READY )  then
               move_to MIXED
            endif
            move_to READY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode = run_mode, version = version) all_in SPDHALFSTAVE_FWSETACTIONS            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calib_mode = calib_mode) all_in SPDHALFSTAVE_FWSETACTIONS
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and     
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and           
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and    
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and            
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  stay_in_state

        when ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state  BEAM_TUNING ) move_to MIXED

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} )  then
               move_to MIXED
            endif
            move_to OFF            
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} )  then
               move_to MIXED
            endif
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED             
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state READY )  then
               move_to MIXED
            endif
            move_to READY
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode = run_mode, version = version) all_in SPDHALFSTAVE_FWSETACTIONS            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calib_mode = calib_mode) all_in SPDHALFSTAVE_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and    
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( any_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} )  )  stay_in_state

        when ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state  READY ) move_to MIXED

        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} )  then
               move_to MIXED
            endif
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING            
        action: CALIBRATE(string calib_mode = "ALL")	!visible: 1
            set CalibMode = calib_mode
            do CALIBRATE(calib_mode = calib_mode) all_in SPDHALFSTAVE_FWSETACTIONS
        action: CONFIGURE(int run_mode = 0,int version = 0)	!visible: 1
            set RunMode = run_mode
            set VersionN = version
            do CONFIGURE(run_mode = run_mode, version = version) all_in SPDHALFSTAVE_FWSETACTIONS
    state: CONFIGURING	!color: FwStateAttention1
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and         
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and         
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and         
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and         
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        action: STOP	!visible: 1
            do STOP all_in SPDHALFSTAVE_FWSETACTIONS
    state: CALIBRATING	!color: FwStateAttention1
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        action: STOP	!visible: 1
            do STOP all_in SPDHALFSTAVE_FWSETACTIONS
    state: MIXED	!color: FwStateAttention1
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) move_to ERROR

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) move_to MOVING_STBY_CONF

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) move_to MOVING_BEAM_TUN

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and   
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and   
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                              
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        action: GO_OFF	!visible: 1
            do GO_OFF all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} )  then
               move_to MIXED
            endif
            move_to OFF
        action: GO_MCM_ONLY	!visible: 1
            do GO_MCM_ONLY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} )  then
               move_to MIXED
            endif
            move_to MCM_ONLY
        action: GO_STBY_CONFIGURED	!visible: 1
            do GO_STBY_CONFIGURED all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED )  then
               move_to MIXED
            endif
            move_to STBY_CONFIGURED
        action: GO_BEAM_TUNING	!visible: 1
            do GO_BEAM_TUNING all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING )  then
               move_to MIXED
            endif
            move_to BEAM_TUNING               
        action: GO_READY	!visible: 1
            do GO_READY all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES not_in_state READY )  then
                move_to MIXED
            endif
            move_to READY
        action: RESET	!visible: 1
            move_to OFF
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) stay_in_state

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {RUMP_UP_STATE,RUMP_DW_STATE} ) move_to MIXED

        action: RESET	!visible: 1
            move_to OFF
    state: MOVING_BEAM_TUN	!color: FwStateAttention1
        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) stay_in_state

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {RUMP_UP_STATE,RUMP_DW_STATE} ) move_to MIXED

        action: RESET	!visible: 1
            move_to OFF
    state: MOVING_READY	!color: FwStateAttention1
        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) move_to CALIBRATING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) move_to CONFIGURING

        when ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) stay_in_state

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) move_to ERROR

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {RUMP_UP_STATE,RUMP_DW_STATE} ) move_to MIXED

        action: RESET	!visible: 1
            move_to OFF
    state: ERROR	!color: FwStateAttention3
        when ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR )  stay_in_state

        when (  ( all_in FWDEVMAJORITY_FWSETSTATES in_state FAKE ) and ( all_in SPDHALFSTAVE_FWSETSTATES not_in_state FAKE )  ) stay_in_state

        when (  ( all_in SPDHALFSTAVE_FWSETSTATES in_state FAKE ) and ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state FAKE )  ) stay_in_state

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {HOT,ERROR} ) stay_in_state

        when (  ( any_in SPDHALFSTAVE_FWSETSTATES in_state CALIBRATING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR )  )  move_to CALIBRATING

        when (  ( any_in SPDHALFSTAVE_FWSETSTATES in_state CONFIGURING ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR )  )  move_to CONFIGURING

        when (  ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_STBY_CONF ) and
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR )  )move_to MOVING_STBY_CONF

        when (  ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_BEAM_TUN ) and
     ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR )  )move_to MOVING_BEAM_TUN

        when (  ( any_in SPDHALFSTAVE_FWSETSTATES in_state MOVING_READY ) and
     ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR )  )move_to MOVING_READY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) move_to OFF

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to OFF

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) move_to MCM_ONLY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                              
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to MCM_ONLY

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) move_to STBY_CONFIGURED

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state STBY_CONFIGURED ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and   
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                             
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to STBY_CONFIGURED

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) move_to BEAM_TUNING

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state BEAM_TUNING ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and      
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state READY ) and          
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to BEAM_TUNING        

        when ( all_in SPDHALFSTAVE_FWSETSTATES in_state READY ) move_to READY

        when ( ( any_in SPDHALFSTAVE_FWSETSTATES in_state READY ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {OFF,OFF_NOT_COOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state {MCM_ONLY,MCM_ON_NCOOLED} ) and
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state STBY_CONFIGURED ) and  
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state BEAM_TUNING ) and        
( all_in SPDHALFSTAVE_FWSETSTATES not_in_state MIXED ) and                              
( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_OK ) )  move_to READY

        when ( any_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR )  move_to MIXED

        action: RELEASE	!visible: 1
            do RELEASE all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES in_state ERROR )  then
                move_to ERROR
            endif
            move_to MIXED
        action: RESET	!visible: 1
            do RESET all_in SPDHALFSTAVE_FWSETACTIONS
            if ( any_in SPDHALFSTAVE_FWSETSTATES in_state ERROR )  then
                move_to ERROR
            endif
            move_to MIXED            

object: SpdHSector_4_A is_of_class TOP_SpdHalfSector_CLASS

class: FwChildrenMode_CLASS
!panel: FwChildrenMode.pnl
    state: Complete	!color: _3DFace
        when ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED )  ) move_to Complete
    state: IncompleteDead	!color: FwStateAttention3

object: SpdHSector_4_A_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
!panel: FwMode.pnl
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Manual	!visible: 0
            move_to Manual
        action: Ignore	!visible: 0
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual(string OWNER = "")	!visible: 0
            move_to Manual
        action: Ignore(string OWNER = "")	!visible: 0
            move_to Ignored
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Excluded
        action: ReleaseAll(string OWNER = "")	!visible: 1
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 1
            move_to InLocal
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
            move_to Included
        action: Take(string OWNER = "")	!visible: 1
            move_to InManual
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Ignore	!visible: 0
            move_to Ignored
        action: Free(string OWNER = "")	!visible: 0
            move_to Excluded
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")	!visible: 1
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: ReleaseAll(string OWNER = "")	!visible: 0
            move_to Excluded
        action: SetInLocal	!visible: 0
            move_to InLocal
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include	!visible: 0
            move_to Included
        action: Exclude(string OWNER = "")	!visible: 0
            move_to Excluded
        action: Manual	!visible: 0
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")	!visible: 0
        action: Free(string OWNER = "")	!visible: 0
            move_to Included
        action: ExcludeAll(string OWNER = "")	!visible: 0
            move_to Excluded

object: SpdHSector_4_A_FWM is_of_class FwMode_CLASS

class: SpdHalfStave_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from SPDHALFSTAVE_FWSETSTATES
            remove &VAL_OF_Device from SPDHALFSTAVE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in SPDHALFSTAVE_FWSETSTATES
            insert &VAL_OF_Device in SPDHALFSTAVE_FWSETACTIONS
            move_to READY

object: SpdHalfStave_FWDM is_of_class SpdHalfStave_FwDevMode_CLASS


objectset: SPDHALFSTAVE_FWSETSTATES is_of_class VOID {SpdHStave_4_A_0,
	SpdHStave_4_A_1,
	SpdHStave_4_A_2,
	SpdHStave_4_A_3,
	SpdHStave_4_A_4,
	SpdHStave_4_A_5 }
objectset: SPDHALFSTAVE_FWSETACTIONS is_of_class VOID {SpdHStave_4_A_0,
	SpdHStave_4_A_1,
	SpdHStave_4_A_2,
	SpdHStave_4_A_3,
	SpdHStave_4_A_4,
	SpdHStave_4_A_5 }

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
!panel: FwDevMode.pnl
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: SpdHSector_4_A:SpdHSPW_4_A_0_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSPW_4_A_1_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSPW_4_A_2_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSPW_4_A_3_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSPW_4_A_4_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSPW_4_A_5_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSTempBus_4_A_0_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSTempBus_4_A_1_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSTempBus_4_A_2_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSTempBus_4_A_3_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSTempBus_4_A_4_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHSTempBus_4_A_5_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHStave_4_A_0_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHStave_4_A_1_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHStave_4_A_2_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHStave_4_A_3_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHStave_4_A_4_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A:SpdHStave_4_A_5_FWDM is_of_class FwDevMode_CLASS

object: SpdHSector_4_A_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHSPW_4_A_0_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_1_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_2_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_3_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_4_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_5_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_0_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_1_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_2_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_3_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_4_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_5_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_0_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_1_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_2_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_3_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_4_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_5_FWDM,
	SpdHSector_4_A_FWDM }
objectset: FWDEVMODE_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHSPW_4_A_0_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_1_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_2_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_3_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_4_FWDM,
	SpdHSector_4_A:SpdHSPW_4_A_5_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_0_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_1_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_2_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_3_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_4_FWDM,
	SpdHSector_4_A:SpdHSTempBus_4_A_5_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_0_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_1_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_2_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_3_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_4_FWDM,
	SpdHSector_4_A:SpdHStave_4_A_5_FWDM,
	SpdHSector_4_A_FWDM }

class: FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: FwDevMajority_FWDM is_of_class FwDevMajority_FwDevMode_CLASS


class: FwDevMajority_CLASS/associated
!panel: FwDevMajority.pnl
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: SpdHSector_4_A:SpdHalfStave_FWMAJ is_of_class FwDevMajority_CLASS

objectset: FWDEVMAJORITY_FWSETSTATES is_of_class VOID {SpdHSector_4_A:SpdHalfStave_FWMAJ }
objectset: FWDEVMAJORITY_FWSETACTIONS is_of_class VOID {SpdHSector_4_A:SpdHalfStave_FWMAJ }


objectset: FWCHILDREN_FWSETACTIONS union {SPDHALFSTAVE_FWSETACTIONS,
	FWDEVMAJORITY_FWSETACTIONS } is_of_class VOID
objectset: FWCHILDREN_FWSETSTATES union {SPDHALFSTAVE_FWSETSTATES,
	FWDEVMAJORITY_FWSETSTATES } is_of_class VOID

