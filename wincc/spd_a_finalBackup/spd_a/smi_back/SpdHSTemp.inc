class: ASS_SpdHSTemp_CLASS/associated
!panel: spd/FSM/HSTemp.pnl
    parameters: int Executing = 0
    state: NO_TEMP	!color: FwStateAttention3
    state: AMB_TEMP	!color: FwStateAttention1
    state: COOLED	!color: FwStateOKNotPhysics
    state: TOO_COOLED	!color: FwStateAttention2
    state: HOT	!color: FwStateAttention3
    state: UNDEFINED	!color: FwStateAttention3
    state: COOLED_PW	!color: FwStateOKPhysics
