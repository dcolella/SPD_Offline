include(WinCC-API.pri)

TEMPLATE = app

### TO MAKE IT BUILD CORRECTLY WE NEED QT BUG FIX
### https://bugreports.qt.io/browse/QTBUG-51782
### the fix is in the mkspecs/features/resolve_target.prf
### which we copy also here

TARGET = WCCOAdip

CONFIG -= qt
CONFIG += \
    warn_off \
    thread
	
INCLUDEPATH += $(DIP_DISTRIBUTION_DIR)/include
	
SOURCES += ../Sources/ReduMan.cxx

#SOURCES += ../Sources/addVerInfo.cxx
SOURCES += ../Sources/AnswerCallBackHandler.cpp
SOURCES += ../Sources/ApiMain.cxx
SOURCES += ../Sources/Browser.cpp
SOURCES += ../Sources/CdipClientDataObject.cpp
SOURCES += ../Sources/CdipDataMapping.cpp
SOURCES += ../Sources/CdipSubscription.cpp
SOURCES += ../Sources/CdpeWrapper.cpp
SOURCES += ../Sources/CmdLine.cpp
SOURCES += ../Sources/DipApiManager.cxx
SOURCES += ../Sources/DipApiResources.cxx
SOURCES += ../Sources/DIPClientManager.cpp
SOURCES += ../Sources/dipManager.cpp
SOURCES += ../Sources/dipPubGroup.cpp
SOURCES += ../Sources/dipPublicationManager.cpp
SOURCES += ../Sources/dpeMapping.cpp
SOURCES += ../Sources/dpeMappingTuple.cpp
SOURCES += ../Sources/mapping.cpp
SOURCES += ../Sources/pubDpeWrapper.cpp
SOURCES += ../Sources/PVSS_DIP_TypeMap.cpp
SOURCES += ../Sources/simpleDPEWrapper.cpp

LIBS += -L$(DIP_DISTRIBUTION_DIR)/lib64 -ldip -llog4cplus

DEFINES += PLATFORMDEPENDANT_STATIC

win32 {
    CONFIG += console
    LIBS += -lwsock32
    LIBS += -lnetapi32
    LIBS += -lodbc32
    LIBS += $(DIP_DISTRIBUTION_DIR)/lib64/PlatformDependant.lib
    INCLUDEPATH += "."
    INCLUDEPATH += "../Sources"
    DEFINES += WIN32

    QMAKE_LFLAGS += /nodefaultlib:libc
    QMAKE_LFLAGS += /nodefaultlib:libcp

    # This is to have PDB files also on Release target
    QMAKE_CXXFLAGS+=/Zi
    QMAKE_LFLAGS+= /INCREMENTAL:NO /Debug
        
    QMAKE_CXXFLAGS -= -Zc:strictStrings
    QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings

}

unix {
    LIBS += -lpthread
    LIBS += $(DIP_DISTRIBUTION_DIR)/lib64/PlatformDependant.a

    # This is to separate debug info
    CONFIG += separate_debug_info

    QMAKE_LFLAGS += '-Wl,--exclude-libs,ALL -Wl,-rpath,\'\$$ORIGIN\''
    QMAKE_RPATHDIR += /opt/WinCC_OA/3.15/bin

    QMAKE_CLEAN += ${TARGET} ${TARGET}.debug

}
