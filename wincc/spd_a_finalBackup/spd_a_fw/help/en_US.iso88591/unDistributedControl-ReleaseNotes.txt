unDistributedControl version 5.2.1 release notes: 
-------------------------------------------------

Please note that PVSS 3.8-SP2 and the latest PVSS patch must be installed to use unDistributedControl-v5.2.1

This is a major release of the unDistributedControl package:
+ all functionalities of previous unDistributedControl version
+ only with PVSS 3.8-SP2

Release Notes - UNICOS - Version unDistributedControl-5.2.1

** New Feature
    * [IS-599] - system name and component version added in the MessageText log and in the diagnostic 
    * [IS-637] - interface to restart the PVSS00dist




