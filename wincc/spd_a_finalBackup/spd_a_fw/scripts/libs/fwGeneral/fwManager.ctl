/**@file

This library contains functions related to WinCC OA Managers

@par Creation Date
        18/02/2015


@author Piotr Golonka (CERN EN/ICE)
*/


#uses "fwConfigs/fwConfigs.ctl"
#uses "fwConfigs/fwDPELock.ctl"

/** Returns the list of Ctrl scripts executed by a given Ctrl Manager.

    @par systemName (input) the name of the system to be queried
    @par ctrlManNum (input) the manager number to be queried
    @par exceptionInfo (output) the standard exception-handling variable

    @returns the list of names of scripts executed by given Ctrl manager,

    If not existing, the necessaery _CtrlDebug datapoint is created.
*/
synchronized dyn_string fwManager_getScriptsRunByCtrlMan(string systemName, unsigned ctrlManNum, dyn_string &exceptionInfo)
{
    dyn_string listOfScripts;
    string ctrlDbgDP =  "_CtrlDebug_CTRL_"+ctrlManNum;

    if (systemName=="") systemName=getSystemName();
    int systemNumber=getSystemId(systemName);
    if (systemNumber<0) {
	fwException_raise(exceptionInfo,"ERROR","Cannot retrieve list of scripts for Ctrl "+
                      ctrlManNum+" on "+systemName+": system not connected or does not exist","");
        return listOfScripts; 
    }

    if(!dpExists(ctrlDbgDP)) {
	DebugTN("WARNING: fwManager_getScriptsRunByCtrlMan: trying to create missing datapoint "+ctrlDbgDP+" on system "+systemName);
	int rc=dpCreate(ctrlDbgDP,"_CtrlDebug",systemNumber);

	if (rc<0) {
	    dyn_errClass err=getLastError();
	    fwException_raise(exceptionInfo,"ERROR","Cannot retrieve list of scripts for Ctrl "+
                    		ctrlManNum+" on "+systemName+": Failed to create _CtrlDebug DP "+ctrlDbgDP+";"+getErrorText(err),"");
	    return listOfScripts;
	}
	delay(0,100); // make sure that the DP already exists
    }


    ctrlDbgDP =  systemName+ctrlDbgDP;

    // lock, then trigger the command on the Debug datapoint
    fwDPELock_tryLock(ctrlDbgDP+".Command", exceptionInfo,7,10);
    if (dynlen(exceptionInfo)) {
	DebugTN("PROBLEM at fwManager_getScriptsRunByCtrlMan() for "+systemName+" CTRL#"+(int)ctrlManNum,exceptionInfo[2]);
	dynClear(exceptionInfo);
	return listOfScripts;
    }

    dpSet(ctrlDbgDP+".Command:_original.._value", "info scripts");

    bool hasResult=false;
    dyn_string result;

    bool timerExpired;
    dyn_anytype retValues;
    const int maxTimeout=5;
    dpWaitForValue(ctrlDbgDP+".Result:_original.._value",makeDynMixed(),
	makeDynString(ctrlDbgDP+".Result:_original.._stime", 
		      ctrlDbgDP+".Result:_original.._value"),
	retValues, maxTimeout, timerExpired);


    // unlock the Debug datapoint
    fwDPELock_unlock(ctrlDbgDP+".Command", exceptionInfo);
    if (dynlen(exceptionInfo)) {
	DebugTN("WARNING: fwManager_getScriptsRunByCtrlMan() exception in unlocking CTRL Manager "+systemName+" #"+(int)ctrlManNum, exceptionInfo[2]);
	dynClear(exceptionInfo);
    }


    if (!timerExpired) {
	  hasResult=true;
	  result=retValues[2];
	  if (dynlen(result) && result[1]=="all breakpoints deleted") {
	    //the value was already discarded by the debugger...
	    DebugTN("WARNING: fwManager_getScriptsRunByCtrlMan: CTRL Manager "+systemName+" #"+(int)ctrlManNum + " cannot check (CtrlDbg discarded the value)");
	    hasResult=false;
	    return listOfScripts;
	  }
    }

    if (!hasResult) {
	// note: sometimes the manager is not running (even though indicated in _Connections DP.
	// in this case, the CtrlDebug does not respond. Report it in the log, yet do not fail.
	DebugTN("WARNING: fwManager_getScriptsRunByCtrlMan: CTRL Manager "+systemName+" #"+(int)ctrlManNum + " does not respond");
    }

    // the result is a list of strings in form such as
    // ScriptId: 0;unSelectDeselect.ctl
    // ScriptId: 1;unDistributedControl.ctl
    // ScriptId: 2; current thread: 2;unSystemIntegrity.ctl
    // ScriptId: 3;unSendMail.ctl
    // ScriptId: 4;scheduler.ctc
    // ScriptId: 5;unDeviceSet.ctl
    for (int i=1;i<=dynlen(result);i++) {
      dyn_string ds=strsplit(result[i],";");
      if (dynlen(ds)>1) {
        // we take the last token (see example of scripId:2 above!
        string scriptName=ds[dynlen(ds)];
        dynAppend(listOfScripts, scriptName);
      }
    }


   return listOfScripts;
}
