<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta http-equiv="generator" content="WebSVN 2.3.3" /> <!-- leave this for stats -->
  <link rel="shortcut icon" type="image/x-icon" href="/websvn/templates/calm/images/favicon.ico" />
  <link type="text/css" href="/websvn/templates/calm/styles.css" rel="stylesheet" media="screen" />
  <link rel='alternate' type='application/rss+xml' title='WebSVN RSS' href='/cern/wsvn/spdFed/JamPlayer/CAENVMEoslib.h?op=rss&amp;' />
  <!--[if gte IE 5.5000]>
  <script type="text/javascript" src="/websvn/templates/calm/png.js"></script>
  <style type="text/css" media="screen">
  tbody tr td { padding:1px 0 }
  #wrap h2 { padding:10px 5px 0 5px; margin-bottom:-8px }
  </style>
  <![endif]-->
  <title>
       WebSVN
          - spdFed
               - Revisione 24
            - /JamPlayer/CAENVMEoslib.h
  </title>
  <script type="text/javascript">
  //<![CDATA[
       function getPath()
       {
         return '/websvn';
       }
       
       function checkCB(chBox)
       {
          count = 0
          first = null
          f = chBox.form
          for (i = 0 ; i < f.elements.length ; i++)
          if (f.elements[i].type == 'checkbox' && f.elements[i].checked)
          {
             if (first == null && f.elements[i] != chBox)
                first = f.elements[i]
             count += 1
          }
          
          if (count > 2) 
          {
             first.checked = false
             count -= 1
          }
       }
  //]]>
  </script>
</head>
<body id="file">
<div id="container">
	<div id="select">
		<form method="get" action="" id="project"><input type="hidden" name="op" value="rep" /><select name="repname" onchange="javascript:this.form.submit();"><option value="spdFed" selected="selected">spdFed</option><option value="spdFed" selected="selected">spdFed</option></select><noscript><input type="submit" value="Vai" /></noscript></form>
		<form method="get" action="" id="template"><select name="template" onchange="javascript:this.form.submit();"><option value="BlueGrey">BlueGrey</option><option value="calm" selected="selected">calm</option><option value="Elegant">Elegant</option></select><noscript><input type="submit" value="Vai" /></noscript></form>
		<form method="get" action="" id="language"><select name="language" onchange="javascript:this.form.submit();"><option value="ca">Catal&agrave;-Valenci&agrave; - Catalan</option><option value="zh-CN">&#20013;&#25991; - Chinese (Simplified)</option><option value="zh-TW">&#20013;&#25991; - Chinese (Traditional)</option><option value="cs">&#268;esky - Czech</option><option value="da">Dansk - Danish</option><option value="nl">Nederlands - Dutch</option><option value="en">English - English</option><option value="fi">Suomi - Finnish</option><option value="fr">Fran&ccedil;ais - French</option><option value="de">Deutsch - German</option><option value="he-IL">&#1506;&#1489;&#1512;&#1497;&#1514; - Hebrew</option><option value="hin">&#2361;&#2367;&#2306;&#2342;&#2368; - Hindi</option><option value="hu">Magyar - Hungarian</option><option value="id">Bahasa Indonesia - Indonesian</option><option value="it" selected="selected">Italiano - Italian</option><option value="ja">&#26085;&#26412;&#35486; - Japanese</option><option value="ko">&#54620;&#44397;&#50612; - Korean</option><option value="mk">&#1052;&#1072;&#1082;&#1077;&#1076;&#1086;&#1085;&#1089;&#1082;&#1080; - Macedonian</option><option value="mr">&#2350;&#2352;&#2366;&#2336;&#2368; - Marathi</option><option value="no">Norsk - Norwegian</option><option value="pl">Polski - Polish</option><option value="pt">Portugu&ecirc;s - Portuguese</option><option value="pt-BR">Portugu&ecirc;s - Portuguese (Brazil)</option><option value="ru">&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081; - Russian</option><option value="sk">Sloven&#269;ina - Slovak</option><option value="sl">Sloven&#353;&#269;ina - Slovenian</option><option value="es">Espa&ntilde;ol - Spanish</option><option value="sv">Svenska - Swedish</option><option value="tr">T&uuml;rk&ccedil;e - Turkish</option><option value="uk">&#1059;&#1082;&#1088;&#1072;&#1111;&#1085;&#1089;&#1100;&#1082;&#1072; - Ukrainian</option><option value="uz">O&euml;zbekcha - Uzbek</option></select><noscript><input type="submit" value="Vai" /></noscript></form>
	</div>
	<h1><a href="/cern/wsvn/?" title="Subversion Repositories">Subversion Repositories</a>
		<span><a href="/cern/wsvn/spdFed?">spdFed</a></span>
	</h1>
  <h2 id="pathlinks"><a href="/cern/wsvn/spdFed/?" class="root"><span>(root)</span></a>/<a href="/cern/wsvn/spdFed/JamPlayer/?#a4875ae1cc99be50623326df2b2d5c027">JamPlayer</a>/<span class="file">CAENVMEoslib.h</span> - Revisione 24</h2>
  <div id="revjump"><form method="get" action="" id="revision">Revisione <input type="text" size="5" name="rev" placeholder="HEAD" /><span class="submit"><input type="submit" value="Vai" /></span></form></div>
  <p>
    <span class="blame"><a href="/cern/wsvn/spdFed/JamPlayer/CAENVMEoslib.h?op=blame&amp;rev=24">Responsabilità</a></span> &#124;
    <span class="changes"><a href="/cern/wsvn/spdFed/JamPlayer/CAENVMEoslib.h?op=revision&amp;rev=24">Ultima modifica</a></span> &#124;
    <span class="log"><a href="/cern/wsvn/spdFed/JamPlayer/CAENVMEoslib.h?op=log&amp;rev=24">Visualizza Log</a></span>
    &#124; <span class="feed"><a href="/cern/wsvn/spdFed/JamPlayer/CAENVMEoslib.h?op=rss&amp;">feed RSS</a></span>
  </p>
  <div class="listing">
<ol class="cpp geshi" style="font-family:monospace;"><li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">/*</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; -----------------------------------------------------------------------------</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; --- CAEN SpA - Computing Systems Division ---</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; -----------------------------------------------------------------------------</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; CAENVMEoslib.h</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; -----------------------------------------------------------------------------</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; Author: &nbsp;Stefano Coluccini (s.coluccini@caen.it)</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; Created: January 2004</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">&nbsp; &nbsp; &nbsp; &nbsp; -----------------------------------------------------------------------------</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #ff0000; font-style: italic;">*/</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#ifndef __CAENVMEOSLIB_H</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#define __CAENVMEOSLIB_H</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#ifdef WIN32</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#include &lt;windows.h&gt;</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#include &lt;winioctl.h&gt;</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#define CAENVME_API CVErrorCodes __stdcall</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#else &nbsp; // WIN32</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#define CAENVME_API CVErrorCodes</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#endif &nbsp;// WIN32</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;"><span style="color: #339900;">#endif &nbsp;// __CAENVMEOSLIB_H</span></div></li>
<li style="font-weight: normal; vertical-align:top;"><div style="font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;">&nbsp;</div></li>
</ol>  </div>
</div>
<div id="footer">
  <p style="padding:0; margin:0"><small>Powered by <a href="http://www.websvn.info/">WebSVN</a> 2.3.3 e <a href="http://subversion.tigris.org">Subversion</a> 1.7.14 &nbsp; &nbsp; &#x2713; <a href="http://validator.w3.org/check?uri=https://svnweb.cern.ch/cern/wsvn.php?template=%26language=it">XHTML</a> &amp; <a href="http://jigsaw.w3.org/css-validator/validator?uri=https://svnweb.cern.ch/cern/wsvn.php?template=%26language=it">CSS</a></small></p>
</div>
</body>
</html>
