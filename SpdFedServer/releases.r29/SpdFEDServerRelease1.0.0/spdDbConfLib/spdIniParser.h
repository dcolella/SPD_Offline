#pragma once
#include <map>
#include <vector>


using namespace std;

namespace spdCalib{

	
	

	// an entry with a value and a name
	struct Entry
	{
		string name;
		string value;
	};

	typedef  map<string, string> EntryMap;
	// struct to keep tha category data
	// each category  as a name and a vector of entries
	struct Category
	{
		string name;
		EntryMap entries;
		//std::vector< Entry> entries;
	};


	typedef  map<string, Category> CategoryMap;



	class IniParser
	{
		
		string currCategory;

		void insertCategory( string category);
		void insertEntry( Entry newEntry);
		void insertText(string text);

		map<string, Category>  categoryList;
			
			// methods to see if a text line as a category or a entry
			// if nothing is found they return an empty string or object
		Entry getEntryFromLine(const string &line);
		string getCategoryFromLine(const string &line);

			// method to remove comments from a line
		string removeComments(const string &line);


	public:

				// opens a file and parses it
		void openFile(const char filename[]);

					// returns the available categories
		std::vector<string> categories(void);
					// returns a category
		Category category(string cat)const;
		
		Category &operator[](const string &cat){return categoryList[cat];};

				// looks like there is no easy way of making this method return a copy 
		//Category operator[](const string &cat)const{return categoryList[cat];};

		void parseLine( string &line);

		
				//constructor that initializes imediatly one line
		IniParser( const char filename[]);
		IniParser(void);

		~IniParser(void);
	};
}