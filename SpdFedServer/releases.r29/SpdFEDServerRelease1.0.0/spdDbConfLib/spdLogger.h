#ifndef SPDLOGGER_H
#define SPDLOGGER_H


#include "stdAfx.h"
#include <dis.hxx>

//using namespace std;

class spdLogger
{

protected:
	spdLogger(void);

private:
	std::string logFile;
	unsigned int logOptions;

	void formatTime(char *time);

	DimService *logService;
	char logMsg[2000];

public:
	static spdLogger &getInstance();

	void setLogFile( std::string filename){ logFile = filename;};
	std::string getLogFile(){return logFile;};
	int log( std::string msg);

	int logToFile(std::string msg);

	int log (const char * format, ...);
	int logToScr(const char * format, ...);
	int logToDim(const char * format, ...);
	int logToFile(const char * format, ...);

	void deleteLogFile();

	void setDimServiceName(const char * serviceName);
	
	~spdLogger(void);
};
#endif