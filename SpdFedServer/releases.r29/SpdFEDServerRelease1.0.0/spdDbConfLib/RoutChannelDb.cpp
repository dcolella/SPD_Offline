#include "stdafx.h"
#include ".\routchanneldb.h"

RoutChannelDb::RoutChannelDb(void)
{

				// intializes the ver table settings
	routChannelVer.setTableSettings("ROUTCHANNEL_VER", 
									"ROUTCHANNEL_VER",
									getRouterChannelFields());
	delay_clk=0;
	delay_glink=0;
	temp_tresh_bus=0;
	temp_tresh_mcm=0;

}

RoutChannelDb::~RoutChannelDb(void)
{
}

std::vector <std::string> RoutChannelDb::getRouterChannelFields(){
	std::vector<std::string> fields(4);

	fields[0]="DELAY_CLK";
	fields[1]="DELAY_GLINK";
	fields[2]="TEMP_TRESH_BUS";
	fields[3]="TEMP_TRESH_MCM";

	return fields;
}

long RoutChannelDb::update(){
	std::vector<Uint32> values(4,0);
	
	values[0]=(Uint32)getDelayClk();
	values[1]=(Uint32)getDelayGlink();
	values[2]=(Uint32)getTreshTempBus();
	values[3]=(Uint32)getTreshTempMcm();

	return routChannelVer.update( values);

}

int RoutChannelDb::getDataFromDB(){
	std::vector<Uint32> values(4,0);
	routChannelVer.getValues( values);

	setDelayClk((Uint8) values[0]);
	setDelayGlink((Uint8) values[1]);
	setTreshTempBus((Uint8) values[2]);
	setTreshTempMcm((Uint8) values[3]);
	return 0;
}