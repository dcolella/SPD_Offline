#include ".\spdreadoutconfdb.h"
#include "stdafx.h"
#include "spdLogger.h"

spdReadOutConfDB::spdReadOutConfDB(void)
{
	readOutTable.setTableSettings(	"READELECTRONICS_VER", 
									"READELECTRONICS_VER",
									getReadOutElecVerFields());
}

spdReadOutConfDB::~spdReadOutConfDB(void)
{
}


void spdReadOutConfDB::setVersionNumber( long version){

	vector<long> values(20,0);
	readOutTable.setVersionNumber(version);
	readOutTable.getValues(values);

		// sets the version numbers to all routers
	for (int n=0 ; n < NRouters ; n ++){
		routersConf[n].setVersionNumber(values[n]);
	}

}


int spdReadOutConfDB::getDataFromDB(){	

	for (int n=0 ; n < NRouters ; n ++){
		routersConf[n].getDataFromDB();
	}
	return 0;
}


long spdReadOutConfDB::update(){

	vector<long> versions(20,0);
	for (int n=0 ; n < NRouters ; n ++){
		versions[n]= routersConf[n].update();
	}

	return readOutTable.update( versions);
}
vector<string> spdReadOutConfDB::getReadOutElecVerFields(){
	vector<string> fields(20);

	fields[0]="ROUTERVER0";
	fields[1]="ROUTERVER1";
	fields[2]="ROUTERVER2";
	fields[3]="ROUTERVER3";
	fields[4]="ROUTERVER4";
	fields[5]="ROUTERVER5";
	fields[6]="ROUTERVER6";
	fields[7]="ROUTERVER7";
	fields[8]="ROUTERVER8";
	fields[9]="ROUTERVER9";
	fields[10]="ROUTERVER10";
	fields[11]="ROUTERVER11";
	fields[12]="ROUTERVER12";
	fields[13]="ROUTERVER13";
	fields[14]="ROUTERVER14";
	fields[15]="ROUTERVER15";
	fields[16]="ROUTERVER16";
	fields[17]="ROUTERVER17";
	fields[18]="ROUTERVER18";
	fields[19]="ROUTERVER19";

	return fields;
}

RouterConfDB & spdReadOutConfDB::operator [](int router){

	if ( (router >= 0) && (router < NRouters) ){
		return routersConf[router];
	}
	else{
		spdLogger *logger = spdLogger::subscribe();
		string err="out of memory: there are only 20 routers in the system";
		logger->log(err);

			// throws an exception
		throw err;
			// returns the zero by default
		return routersConf[0];
	}
	
}