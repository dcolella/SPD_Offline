#ifndef PIXELCONFDB_H
#define PIXELCONFDB_H

#include "PixelConfFile.h"
#include "spdVerTable.h"
#include "stdafx.h"



//using namespace oracle::occi; 
//using namespace std;


/*
	pixel configuration container, with connectivity with the data base.
	Uses spdVertable objects to manage the versionning.

	in the db there are 4 tables 
	set version will set 

*/


class PixelConfDb :
	public PixelConfFile
{
private:
	
	SpdVerTable hsTable;

	SpdVerTable dacTable[10];
	SpdVerTable noisyTable;
	SpdVerTable mcmTable;

	std::vector <std::string> getHsVersionFields();
	std::vector <std::string> getMcmVerFields();
	std::vector <std::string> getPixDacVerFields();

	std::vector <std::string> getNoisyVerFields();

				// needs this because noisy ver its the only table
				//which as strings and integers together

	std::vector< spdDbTypes > getNoisyVerTypes();

				// functions to update the internal version tables
	long int updateMcmVer();
	long int updatePixDacVer(int pixel);
	long int updateNoisyPixVer();
				// functions to get the values from the data base for 
				// the internal versioning tables
	int getMcmDbValues();
	int getPixDacDbValues(int pixel);
	int getNoisyPixDbValues();
				// to transform the noisy pixels to string
	std::string noisyToStr( int pixel);
				// and the other way around
	std::vector <PixelCoordinate> strToNoisy(const std::string &strValue);
	

public:

	

			// update to syncronize with the database
	long int update();
			// gets data from the database
	int getDataFromDB();

	void setVersionNumber(long int version);
	long int getVersionNumber(){return hsTable.getVersionNumber();}

	

	PixelConfDb(void);
	~PixelConfDb(void);
};

#endif //PixelConfDb_H