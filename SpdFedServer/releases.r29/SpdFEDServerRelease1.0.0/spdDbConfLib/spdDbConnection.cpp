#include "stdafx.h"
#include ".\spddbconnection.h"

//
spdDbConnection::spdDbConnection(void)
{
//	pinstance=NULL;
	env=NULL;
	conn=NULL;
}

// destructor just disconnects and deletes the instance
spdDbConnection::~spdDbConnection(void){

	disconnect();

	//if (pinstance != NULL) delete this->pinstance ;
	//this->pinstance =NULL;

}

			// if the connection is null then its not connected 
bool spdDbConnection::isConnected(){
	return (conn!=NULL);
}


// gets an oracle statement to do something in the database
Statement * spdDbConnection::createStatement(){
	try{
		Statement * stmt;
		// if it is not connected connects automatically
		if (! this->isConnected() )this->connect();
		stmt = conn->createStatement();
		return stmt;
	}
	catch(exception &str) {
        spdLogger::getInstance().log(str.what());

		return NULL;
    }
}

// transaction commit
int spdDbConnection::commit(){

	conn->commit();

	return 0;

}
// transaction rollback, just call the oracle class method
int spdDbConnection::rollback(){

	conn->rollback();
	return 0;
}

// connects to the database definning the connections variables 
int spdDbConnection::connect( const char *machine, const char * username, const char * password){

	conString = machine;
	user=username;
	passwd=password;

	return this->connect();
}

// connects to database using the settings stored inside
int spdDbConnection::connect(){

	try {
			
		env = Environment::createEnvironment(Environment::DEFAULT);
		conn = env->createConnection( user, passwd, conString);
		//conn->setStmtCacheSize(120);
	}
	catch(exception &str) {
        spdLogger::getInstance().log(str.what());

		disconnect();
		return -1;
    }
	
	return 0;
	
}

// sets the db variables
void spdDbConnection::setDbSettings( const char *machine, const char * username, const char * password){
	conString = machine;
	user = username;
	passwd = password;
}

// loops over an ini file category getting the database connection settings
void spdDbConnection::setDbSettings( spdCalib::Category & settings){

	conString=settings.entries["conString"];
	user = settings.entries["user"];
	passwd = settings.entries["passwd"];
}

// gets the only instance of this object
// the commented code is because this was making a link error tha  Icould not understand;
spdDbConnection *spdDbConnection::subscribe(){

	static spdDbConnection pinstance;

	//if (pinstance == NULL){
	//	pinstance = new spdDbConnection();
	//}

	return &pinstance;
}

// terminates on oracle statement
void spdDbConnection::terminateStatement( Statement * stmt){
	conn->terminateStatement (stmt);
}

// just sends one sql command does not return an resultset
int spdDbConnection::sendSqlCommand( std::string command){
	Statement * stmt;
	try {
	
		stmt = conn->createStatement();
		ResultSet *rs =  stmt->executeQuery( command);
		
		stmt->closeResultSet (rs);
		conn->terminateStatement (stmt);
		return 0;
	}
	catch(exception &error) {
		
		conn->terminateStatement (stmt);

		spdLogger::getInstance().log("ERROR: following query to the DB failed:\n%s", command.c_str());
        spdLogger::getInstance().log(error.what());

		return -1;
    }
	catch(string &error){
		spdLogger::getInstance().log("ERROR: following query to the DB failed:\n%s", command.c_str());
        spdLogger::getInstance().log(error.c_str());
		return -1;
	}
	catch(...){
		conn->terminateStatement (stmt);
		spdLogger::getInstance().log("ERROR: following query to the DB failed:\n%s", command.c_str());
		return -1;
	}

	return 0;
}
/*
	Closes the database connection
*/
int spdDbConnection::disconnect(){
	try{
		if (env != NULL){
			if (conn != NULL){ 
				env->terminateConnection (conn);
				conn = NULL;
			}
			Environment::terminateEnvironment (env);
			env = NULL;
		}
		return 0;
	}
	catch(exception &str) {
        spdLogger::getInstance().log(str.what());
		return -1;
    }
	catch(string &str) {
		spdLogger::getInstance().log(str.c_str());
		return -1;
    }
	

}