#pragma once

class RouterConf
{
public:
						//channel present 10 bits register
	virtual int getChannelPresent()=0;
	virtual void setChannelPresent(int item)=0;
						// jtag to use 
	virtual int getJtagToUse()=0;
	virtual void setJtagToUse(int item)=0;
					
						// bits taken from the PVSS interface
	virtual bool getDpmSampleMode()=0;
	virtual void setDpmSampleMode(bool item)=0;

	virtual bool getNoDataToDaq()=0;
	virtual void setNoDataToDaq(bool item)=0;

	virtual bool getBusy()=0;
	virtual void setBusy(bool item)=0;

	virtual bool getEnTpinlo()=0;
	virtual void setEnTpinlo(bool item)=0;

	virtual bool getEnRouterHeader()=0;
	virtual void setEnRouterHeader(bool item)=0;

	virtual bool getShortTdiTdo()=0;
	virtual void setShortTdiTdo(bool item)=0;

	virtual bool getFasterJtag()=0;
	virtual void setFasterJtag(bool item)=0;

	virtual bool getEnOrbInternal()=0;
	virtual void setEnOrbInternal(bool item)=0;

	virtual bool getDisTtcTriggers()=0;
	virtual void setDisTtcTriggers(bool item)=0;

	virtual bool getEnFastorCoincidence()=0;
	virtual void setEnFastorCoincidence(bool item)=0;

	RouterConf(void);
	virtual ~RouterConf(void);
};
