#pragma once
#include "stdafx.h"

class LinkRxConf
{
public:
					//2 32 bit controll registers 
	virtual Uint32 getCntl(int pos)=0;
	virtual void setCntl(int pos, Uint32 item)=0;

					// 2 32 bit Channel colum mask
	virtual Uint32 getChanColumnMask( int pos)=0;
	virtual void setChanColumnMask(int pos,Uint32 item)=0;

	virtual Uint32 getChanTempLimit(int channel)=0;
	virtual void setChanTempLimit(int channel, Uint32 item)=0;

					// 2 32 bit Busy control registers 
	virtual Uint32 getBusyControlReg( int pos)=0;
	virtual void setBusyControlReg(int pos, Uint32 item)=0;

	virtual Uint32 getShiftSelector()=0;
	virtual void setShiftSelector(Uint32 item)=0;

	virtual Uint8 getEnableMoveBit()=0;
	virtual void setEnableMoveBit(Uint8 item)=0;

					// 2 32 bit busy mask registers 
	virtual Uint32 getBusyMask(int pos)=0;
	virtual void setBusyMask(int pos ,Uint32 item)=0;

			
	virtual Uint8 getDisFastorcounter()=0;
	virtual void setDisFastorcounter(Uint8 item)=0;

	virtual Uint8 getDisFastor()=0;
	virtual void setDisFastor(Uint8 item)=0;

	LinkRxConf(void);
	~LinkRxConf(void);
};
