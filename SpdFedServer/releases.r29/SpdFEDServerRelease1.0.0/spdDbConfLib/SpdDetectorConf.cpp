#include "stdafx.h"
#include ".\spddetectorconf.h"


SpdDetectorConf::SpdDetectorConf(void)
{

							// itializes the values of the tables
	detectorTable.setTableSettings( "DET_SIDE_VER2", "DET_SIDE_VER", getDetectorVerFields());

							// does the same thing for the sector versions 
	for (int sector = 0 ; sector < NSectors ; sector ++){
		sectorTables[sector].setTableSettings( "HSECTOR_VER2", "HSECTOR_VER", getSectorVerFields());
	}
}


SpdDetectorConf::~SpdDetectorConf(void)
{
}


// updates one sector versio
long int SpdDetectorConf::updateSector( int sector){
	
	std::vector<long int> hsVersions(NHalfstaves ,0);

					// loops over the channels updates all checking the new version
	for (int hs = 0 ; hs < NHalfstaves ; hs ++){
						
		int chNumber = getChannelNumber( sector, hs);
		hsVersions[hs] = hsChannels[chNumber].update();

	}
	
							// updates the sector version with the versions returned by the children
	return sectorTables[sector].update(hsVersions);

}
long int SpdDetectorConf::update(){
	
	std::vector<long int> sectorVersions(NSectors ,0);

	int detectorVersion=0;
	
						// updates all sector versions 
	for (int sector = 0 ; sector < NSectors; sector ++){
		
		sectorVersions[sector] =updateSector( sector);

	}
	
					// updates the values with the new sector versions
	detectorVersion = detectorTable.update(sectorVersions);
	return detectorVersion;
}


// gets the data for all channels 
SpdDetectorConf::getDataFromDB(){

		
	for (int channel=0; channel < NChannels ; channel ++ ){
		hsChannels[channel].getDataFromDB();
	}

	return 0;
}

// sets the version of one sector
int SpdDetectorConf::setSectorVersion( int sector, long int version){
	
	std::vector<long int> channelsVersions(NHalfstaves,0);
	sectorTables[sector].setVersionNumber( version);
				
				// gets the version numbers for the 12 channels 
	sectorTables[sector].getValues( channelsVersions);


						// sets the versions of the individual channels 
	for (int hs = 0 ; hs < NHalfstaves ; hs ++){

						// gets the Ivan's channel number 
		int chNumber = getChannelNumber( sector, hs);
		hsChannels[chNumber].setVersionNumber( channelsVersions[hs]);

	}


	return 0;

}

// sets a version for all 
void SpdDetectorConf::setVersionNumber(long int version){

	std::vector<long int> detectorVersions(NSectors,0);

			
	if (version >= 0){
		detectorTable.setVersionNumber( version);
	}
	else {
					// if the version is small than zero then it will get 
					// the latest available version number 
		int maxVer= detectorTable.getMaxVersionNumber();
		detectorTable.setVersionNumber(maxVer);
	}
			
					// gets the versions of all sectors
	detectorTable.getValues( detectorVersions);

				// sets the versions of all sectors
	for (int sector = 0 ; sector < NSectors ; sector ++){
		setSectorVersion( sector, detectorVersions[sector]);
	}


}

std::vector<std::string> SpdDetectorConf::getSectorVerFields(){
	std::vector<std::string> fields(6);

	fields[0] = "HS_VER0";
	fields[1] = "HS_VER1";
	fields[2] = "HS_VER2";
	fields[3] = "HS_VER3";
	fields[4] = "HS_VER4";
	fields[5] = "HS_VER5";


	return fields;
}

std::vector <std::string> SpdDetectorConf::getDetectorVerFields(){
	std::vector<std::string> fields(10);

	fields[0]="HSECTOR_VER0";
	fields[1]="HSECTOR_VER1";
	fields[2]="HSECTOR_VER2";
	fields[3]="HSECTOR_VER3";
	fields[4]="HSECTOR_VER4";
	fields[5]="HSECTOR_VER5";
	fields[6]="HSECTOR_VER6";
	fields[7]="HSECTOR_VER7";
	fields[8]="HSECTOR_VER8";
	fields[9]="HSECTOR_VER9";

	return fields;
}