#ifndef ROUTERCONFDB_H
#define ROUTERCONFDB_H


#include "routerconf.h"
#include "LinkRxconfDB.h"
#include "RoutChannelDb.h"
#include "spdLogger.h"

const int NLrx=3;
const int NRoutCh=6;

// small class just to encapsulate the linkRx array
class RoutLrxGroup{
private:
	LinkRxConfDB lrx[NLrx];
public:
	LinkRxConfDB & operator[](unsigned int channel);
	int size(){return NLrx;};

};

//small  class just to encapsulate the router channels settings
class RoutChanGroup{
private:
	RoutChannelDb channels[NRoutCh];
public:
	RoutChannelDb & operator[](unsigned int channel);
	int size(){return NRoutCh;};
};

class RouterConfDB //:
	//public RouterConf
{

	Uint32 channelpresent;
	Uint32 jtagtouse;
	Uint8 dpmsamplemode;
	Uint8 nodatatodaq;
	Uint8 busy;
	Uint8 en_tpinlo;
	Uint8 en_routerheader;
	Uint8 shorttditdo;
	Uint8 fasterjtag;
	Uint8 en_orbinternal;
	Uint8 dis_ttctriggers;
	Uint8 en_fastorcoincidence;
	
	SpdVerTable routVerTable;
	SpdVerTable routConfTable;
	
	std::vector<std::string> getRoutConfFields();
	std::vector<std::string> getRoutVerFields();

	long updateRoutConf();
	void getRoutConfValues();

public:
	Uint32 getChannelPresent(){return 0x3ff & channelpresent;};
	void setChannelPresent( Uint32 value){channelpresent = value;}

	Uint32 getJtagToUse(){return 0x3ff & jtagtouse;};
	void setJtagToUse( Uint32 value){jtagtouse = value;};
	
	Uint8 getDpmSampleMode(){return 0x1 & dpmsamplemode;};
	void setDpmSampleMode( Uint8 value){dpmsamplemode = value;};

	Uint8 getNoDataToDaq(){return 0x1 & nodatatodaq;};
	void setNoDataToDaq(Uint8 value){nodatatodaq=value;};

	Uint8 getBusy(){return 0x1 & busy;};
	void setBusy( Uint8 value){busy = value;};

	Uint8 getEnTpInLo(){return 0x1 & en_tpinlo;};
	void setEnTpInLo( Uint8 value){en_tpinlo = value;};

	Uint8 getEnRoutheader(){return 0x1 & en_routerheader;};
	void setEnRoutheader( Uint8 value){en_routerheader = value;};

	
	Uint8 getShortDtiTdo(){return 0x1 & shorttditdo;};
	void setShortDtiTdo( Uint8 value){shorttditdo = value;};

	Uint8 getFasterJtag(){return 0x1 & fasterjtag;};
	void setFasterJtag( Uint8 value){fasterjtag = value;};

	Uint8 getEnOrbInternal(){return 0x1 & en_orbinternal;};
	void setEnOrbInternal( Uint8 value){en_orbinternal = value;};

	Uint8 getDisTtcTriggers(){return 0x1 & dis_ttctriggers;};
	void setDisTtcTriggers(Uint8 value){dis_ttctriggers=value;};

	Uint8 getEnFastorCoincidence(){return 0x1 & en_fastorcoincidence;};
	void setEnFastorCoincidence(Uint8 value){en_fastorcoincidence=value;};

			// update to syncronize with the database
	long int update();
			// gets data from the database
	int getDataFromDB();

	void setVersionNumber(long int version);
	long int getVersionNumber(){return routVerTable.getVersionNumber();}

	RoutLrxGroup lrx;
	RoutChanGroup channel;	


	RouterConfDB(void);
	~RouterConfDB(void);
};

#endif
