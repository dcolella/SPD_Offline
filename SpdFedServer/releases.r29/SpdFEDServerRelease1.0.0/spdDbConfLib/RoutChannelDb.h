#ifndef ROUTCHANNELDB_H
#define ROUTCHANNELDB_H


#include "stdafx.h"
#include "spdVerTable.h"

//using namespace std;

class RoutChannelDb
{
	Uint8 delay_clk;
	Uint8 delay_glink;
	Uint8 temp_tresh_bus;
	Uint8 temp_tresh_mcm;

	SpdVerTable routChannelVer;

	std::vector<std::string> getRouterChannelFields();

public:
	Uint8 getDelayClk(){return delay_clk;};
	void setDelayClk(Uint8 value){delay_clk=value;};

	Uint8 getDelayGlink(){return delay_glink;};
	void setDelayGlink(Uint8 value){delay_glink=value;};

	Uint8 getTreshTempBus(){return temp_tresh_bus;};
	void setTreshTempBus(Uint8 value){temp_tresh_bus=value;};

	Uint8 getTreshTempMcm(){return temp_tresh_mcm;};
	void setTreshTempMcm(Uint8 value){temp_tresh_mcm=value;};


			// update to syncronize with the database
	long int update();
			// gets data from the database
	int getDataFromDB();

	void setVersionNumber(long int version){routChannelVer.setVersionNumber(version);};
	long int getVersionNumber(){return routChannelVer.getVersionNumber();}


	RoutChannelDb(void);
	~RoutChannelDb(void);
};

#endif
