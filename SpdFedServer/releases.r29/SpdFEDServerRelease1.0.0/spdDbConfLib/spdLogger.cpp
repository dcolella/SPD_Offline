#include "stdafx.h"
#include ".\spdLogger.h"
#include <ctime>
#include <stdio.h>
#include <stdarg.h>
#include <windows.h>


using namespace std;

spdLogger::spdLogger(void)
{
	logFile = "./spd.log";
	logOptions= 0;
	logService = NULL;
	
}

spdLogger::~spdLogger(void)
{
}



void spdLogger::deleteLogFile(){

	char command[200];
	sprintf( command , "del %s \n", logFile.c_str());
	system ( command);
}

// logs one message, adds date 
int spdLogger::log(std::string msg){

	
	char time[50];
	formatTime( time);

	sprintf(logMsg, "%s::%s\n", time, msg.c_str());
	cout << logMsg;

	ofstream outfile;

	outfile.open( logFile.c_str(), std::ios::app);

	if ( !outfile.is_open()){

		cout << "unable to open the file " << logFile << endl;
		return -1;
	}

	outfile << logMsg;

	outfile.close();

	if (this->logService != NULL){
		this->logService->updateService(logMsg);
	}

	return 0;
}

int spdLogger::logToFile(std::string msg){

	
	char time[50];
	formatTime( time);

	sprintf(logMsg, "%s::%s\n", time, msg.c_str());
	cout << logMsg;

	ofstream outfile;

	outfile.open( logFile.c_str(), std::ios::app);

	if ( !outfile.is_open()){

		cout << "unable to open the file " << logFile << endl;
		return -1;
	}

	outfile << logMsg;

	outfile.close();

	return 0;
}

void spdLogger::formatTime(char * time){

	SYSTEMTIME  lt;
    GetLocalTime(&lt);

	sprintf(time, "%d/%d/%d %d:%d:%d,%03d", lt.wDay,
											lt.wMonth,
											lt.wYear,
											lt.wHour,
											lt.wMinute,
											lt.wSecond,
											lt.wMilliseconds);

    

}

// logs one message, adds date 
int spdLogger::log(const char * format, ...){

	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf(msg, format, va);

	sprintf(logMsg, "%s::%s\n", time, msg);
	cout<< logMsg;

	ofstream outfile;

	outfile.open( logFile.c_str(), std::ios::app);
	
	if ( !outfile.is_open()){

		cout << "unable to open the file " << logFile;
		return -1;
	}

	outfile << logMsg;

	outfile.close();

	if(this->logService != NULL){
		this->logService->updateService(logMsg);
	}

	return 0;
}


// logs one message, adds date 
int spdLogger::logToScr(const char * format, ...){
	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf(msg, format, va);

	sprintf(logMsg, "%s::%s\n", time, msg);
	cout<< logMsg;


	return 0;
}


int spdLogger::logToDim(const char * format, ...){

	if (this->logService == NULL){
		return 1;
	}

	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf(msg, format, va);

	sprintf(logMsg, "%s::%s\n", time, msg);
	
	
	cout<< logMsg;

	this->logService->updateService(logMsg);
	
	return 0;


}
int spdLogger::logToFile(const char * format, ...){

	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf(msg, format, va);

	sprintf(logMsg, "%s::%s\n", time, msg);
	cout<< logMsg;

	ofstream outfile;

	outfile.open( logFile.c_str(), std::ios::app);
	
	if ( !outfile.is_open()){

		cout << "unable to open the file " << logFile;
		return -1;
	}

	outfile << logMsg;

	outfile.close();

	return 0;
}

spdLogger & spdLogger::getInstance(){

	static spdLogger pinstance;
	return pinstance;

}

void spdLogger::setDimServiceName( const char * serviceName){
	if (logService != NULL){
		delete logService;
	}

	logService = new DimService( serviceName, "log service started");

	
}