#include ".\pixelconftest.h"

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include "stdafx.h"

using namespace std;


PixelConfTest::PixelConfTest(void)
{
	sizeNoisyPixels=0;
	sizeDeadPixels = 0;

	noisyPixelsVector = NULL;	//ask ivan
	deadPixelsVector = NULL;

	pixDacVector = NULL;
	apiVector = NULL;
	dipVector = NULL;
	golVector = NULL;	
	analogConfVector = NULL;

}

PixelConfTest::~PixelConfTest(void)
{
}

/*
	template to delete a vector, checks it is not null first
*/
template <class T>
void deleteVector(T * vector){
	if (vector != NULL){
		delete [] vector;
	}
}

void PixelConfTest::deleteVectors(void){

	deleteVector( apiVector);
	deleteVector( dipVector);
	deleteVector( golVector);
	deleteVector( analogConfVector);
	deleteVector( pixDacVector);
}


/*
	template to alocate one numeric vector and intialize it all to '0'
*/
template <class Number>
void initializeVector(Number *vector, int size, Number iniValue){


	vector = new Number[size];
	for (int n =0; n < size ; n ++){
		vector[n]=iniValue;
	}
}


/*
	property to initialize internal vectors
	calls delete vectors to be sure that the memory is freed

	intializes the memory to '0'
	
*/

void PixelConfTest::iniVectors(void)
{

	deleteVectors();

	initializeVector( apiVector, SIZE_APIVECTOR, (char) 0);
	initializeVector( dipVector, SIZE_DIPVECTOR, (char) 0);
	initializeVector( golVector, SIZE_GOLVECTOR, (char) 0);
	initializeVector( analogConfVector, SIZE_ANALOGCONFVECTOR, (float) 0);
	//initializeVector2D( pixDacVector, N_PIXELS, N_PIXELDACS, (char) 0 );

	pixDacVector = new char[N_PIXELS][N_PIXELDACS];

	for (int n =0; n < N_PIXELS ; n ++){
		for( int i = 0 ; i < N_PIXELDACS ; i ++){ 
			pixDacVector[n][i]=0;
		}
	}
	

}
