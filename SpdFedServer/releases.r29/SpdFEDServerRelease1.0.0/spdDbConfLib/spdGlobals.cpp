#include "stdafx.h"

/*********************************************************
Spd Global functions added to this namespace
***********************************************************/


#include "spdGlobals.h"

//using namespace std::cout;
//using namespace std::ios;

using namespace std;

namespace spdGlobals{

	const char sqlDebug[] = "./debug/sqlDebug.sql";

	std::string intToStr( int number){
		char buffer[20];

		std::string str = itoa( number, buffer, 10); 

		return str;
	}


	template <class T>
	std::string toString( T value )
	{
	std::ostringstream oss;

	oss << value;

	return oss.str();
	}


	////////////////////////////////////////////////////////////////////////////
	//
	// string conversion from string to typename T 
	// e.g: int d = FromString<int>( s );
	////////////////////////////////////////////////////////////////////////////
	//
	template<typename T>
	T fromString( const std::string& s){
		std::istringstream is(s);
		T t;
		is >> t;
		return t;
	}


	void deleteSqldebug(void){

		char command[200];
		sprintf( command , "del %s \n", sqlDebug);
		system ( command);
	};

	//function to split a string 
	std::vector<std::string> splitStr(const std::string& text, const std::string& delimeter){
	std::size_t pos = 0;
	std::size_t oldpos = 0;
	std::size_t delimlen = delimeter.length();

	std::vector<std::string> result;
	while(pos != std::string::npos)
	{
		pos = text.find(delimeter, oldpos);
		result.push_back(text.substr(oldpos, pos - oldpos));
		oldpos = pos + delimlen;
	}

	return result;
	   
	}

	std::vector<std::string> getMcmVerFields(){
		
		std::vector<std::string> fields(20);

		fields[0]="DPI_WaitBefRow";
		fields[1]="DPI_SebMeb";
		fields[2]="DPI_MaskChip";
		fields[3]="DPI_EventNumber";
		fields[4]="DPI_StrobeLenght";
		fields[5]="DPI_HoldRow";
		fields[6]="DPI_SkipMode";
		fields[7]="DPI_Tdo8Tdo9";
		fields[8]="DPI_EnableCeSeq";
		fields[9]="DPI_DataFormat";
		fields[10]="API_DacRefHi";
		fields[11]="API_DacRefMid";
		fields[12]="API_GtlRefA";
		fields[13]="API_GtlRefD";
		fields[14]="API_AnTestHi";
		fields[15]="API_AnTestLow";
		fields[16]="GOL_Config0";
		fields[17]="GOL_Config1";
		fields[18]="GOL_Config2";
		fields[19]="GOL_Config3";

		return fields;
	}


	std::vector <std::string> getNoisyVerFields(){
		std::vector<std::string> fields(20);

		fields[0]="NOISYCOUNT0";
		fields[1]="NOISYVECT0";
		fields[2]="NOISYCOUNT1";
		fields[3]="NOISYVECT1";
		fields[4]="NOISYCOUNT2";
		fields[5]="NOISYVECT2";
		fields[6]="NOISYCOUNT3";
		fields[7]="NOISYVECT3";
		fields[8]="NOISYCOUNT4";
		fields[9]="NOISYVECT4";
		fields[10]="NOISYCOUNT5";
		fields[11]="NOISYVECT5";
		fields[12]="NOISYCOUNT6";
		fields[13]="NOISYVECT6";
		fields[14]="NOISYCOUNT7";
		fields[15]="NOISYVECT7";
		fields[16]="NOISYCOUNT8";
		fields[17]="NOISYVECT8";
		fields[18]="NOISYCOUNT9";
		fields[19]="NOISYVECT9";

		return fields;


	}

	const char * getDacName( int index){

		switch( index){
			case 0: return "DIS_BIASTH"; 
			case 1: return "DIS_VCASD4"; 
			case 2: return "DIS_VCASD21"; 
			case 3: return "DIS_VIBCOMP"; 
			case 4: return "DIS_VIBIASCARD"; 
			case 5: return "DIS_VIDISC"; 
			case 6: return "DIS_VREF2DIS"; 
			case 7: return "EU_VBIAS"; 
			case 8: return "EU_VBN"; 
			case 9: return "EU_VBNBUFFER"; 
			case 10: return "EU_VBNBUSLATCH"; 
			case 11: return "EU_VBNG"; 
			case 12: return "EU_VBNLHCB"; 
			case 13: return "EU_VBPPULLDOWN"; 
			case 14: return "FAST_CGPOL"; 
			case 15: return "FAST_CGPOLFM"; 
			case 16: return "FAST_COMPREF"; 
			case 17: return "FAST_CONVPOL"; 
			case 18: return "FAST_CONVPOLFM"; 
			case 19: return "FAST_FMPOL"; 
			case 20: return "FAST_FOPOL"; 
			case 21: return "KEN_EOCVBN"; 
			case 22: return "KEN_VBN"; 
			case 23: return "KEN_VBNM"; 
			case 24: return "KEN_VBNS"; 
			case 25: return "KEN_VBUFFVBN"; 
			case 26: return "PRE_VI1"; 
			case 27: return "PRE_VI2"; 
			case 28: return "PRE_VI3"; 
			case 29: return "PRE_VI4"; 
			case 30: return "PRE_VI5"; 
			case 31: return "PRE_VIFB"; 
			case 32: return "PRE_VIPREAMP"; 
			case 33: return "PRE_VREF1"; 
			case 34: return "PRE_VREF2"; 
			case 35: return "PRE_VREF3"; 
			case 36: return "PRE_VREF4"; 
			case 37: return "PRE_VREF5"; 
			case 38: return "PRE_VREF6"; 
			case 39: return "PRE_VTH"; 
			case 40: return "VAL_BUFFIN"; 
			case 41: return "VAL_BUFFOUT"; 
			case 42: return "DELAY_CONTROL"; 
			case 43: return "MISC_CONTROL"; 
		}
		
		return "";
	};

	int getDacNumber( const char *dacName){
		if(!strcmp(dacName,"DIS_BIASTH")) return 0; 
		if(!strcmp(dacName,"DIS_VCASD4")) return 1; 
		if(!strcmp(dacName,"DIS_VCASD21")) return 2; 
		if(!strcmp(dacName,"DIS_VIBCOMP")) return 3; 
		if(!strcmp(dacName,"DIS_VIBIASCARD")) return 4; 
		if(!strcmp(dacName,"DIS_VIDISC")) return 5; 
		if(!strcmp(dacName,"DIS_VREF2DIS")) return 6; 
		if(!strcmp(dacName,"EU_VBIAS")) return 7; 
		if(!strcmp(dacName,"EU_VBN")) return 8; 
		if(!strcmp(dacName,"EU_VBNBUFFER")) return 9; 
		if(!strcmp(dacName,"EU_VBNBUSLATCH")) return 10; 
		if(!strcmp(dacName,"EU_VBNG")) return 11; 
		if(!strcmp(dacName,"EU_VBNLHCB")) return 12; 
		if(!strcmp(dacName,"EU_VBPPULLDOWN")) return 13; 
		if(!strcmp(dacName,"FAST_CGPOL")) return 14; 
		if(!strcmp(dacName,"FAST_CGPOLFM")) return 15; 
		if(!strcmp(dacName,"FAST_COMPREF")) return 16; 
		if(!strcmp(dacName,"FAST_CONVPOL")) return 17; 
		if(!strcmp(dacName,"FAST_CONVPOLFM")) return 18; 
		if(!strcmp(dacName,"FAST_FMPOL")) return 19; 
		if(!strcmp(dacName,"FAST_FOPOL")) return 20; 
		if(!strcmp(dacName,"KEN_EOCVBN")) return 21; 
		if(!strcmp(dacName,"KEN_VBN")) return 22; 
		if(!strcmp(dacName,"KEN_VBNM")) return 23; 
		if(!strcmp(dacName,"KEN_VBNS")) return 24; 
		if(!strcmp(dacName,"KEN_VBUFFVBN")) return 25; 
		if(!strcmp(dacName,"PRE_VI1")) return 26; 
		if(!strcmp(dacName,"PRE_VI2")) return 27; 
		if(!strcmp(dacName,"PRE_VI3")) return 28; 
		if(!strcmp(dacName,"PRE_VI4")) return 29; 
		if(!strcmp(dacName,"PRE_VI5")) return 30; 
		if(!strcmp(dacName,"PRE_VIFB")) return 31; 
		if(!strcmp(dacName,"PRE_VIPREAMP")) return 32; 
		if(!strcmp(dacName,"PRE_VREF1")) return 33; 
		if(!strcmp(dacName,"PRE_VREF2")) return 34; 
		if(!strcmp(dacName,"PRE_VREF3")) return 35; 
		if(!strcmp(dacName,"PRE_VREF4")) return 36; 
		if(!strcmp(dacName,"PRE_VREF5")) return 37; 
		if(!strcmp(dacName,"PRE_VREF6")) return 38; 
		if(!strcmp(dacName,"PRE_VTH")) return 39; 
		if(!strcmp(dacName,"VAL_BUFFIN")) return 40; 
		if(!strcmp(dacName,"VAL_BUFFOUT")) return 41; 
		if(!strcmp(dacName,"DELAY_CONTROL")) return 42; 
		if(!strcmp(dacName,"MISC_CONTROL")) return 43; 


		return -1;
	};

	void saveQuerytoFile( std::string coment, std::string query){
		ofstream outfile;



		outfile.open( sqlDebug, ios::app);
		
		if ( !outfile.is_open()){

			cout << "unable to open the file " << sqlDebug;
			return;
		}

		outfile << "--" << coment << "\n";
		outfile << query << "\n";

		outfile.close();


	};


	Uint32  extractFromInt32( Uint32  input, int offset, int bits){
			
		long int mask = ~(0xffffffff - ((1<< bits) - 1));
		long int aux =  input >> offset;
		long int out = aux & mask;

		//printf("input %x , offset %d, bits %d == out %x\n", input, offset, bits, out);
		return out;
	};

	//void insertIntoInt32( Uint32 input, int offset, int nbits){

	//}


	void insertIntoInt32( Uint32 &input, Uint32 value, int offset, int bits ){

		
					// creates the mask, needsd to be done in these steps
					// the mask_part1 is something like 1111100000
					// the mask part2 is something like 0000000011
		long int mask_part1 = (0xffffffff - ((1<< bits) - 1)) << offset;
		long int mask_part_2 = ~(0xffffffff - ((1<< offset) - 1));
		long int mask = mask_part1 | mask_part_2;

		long int shiftedValue =  (value << offset) & (~mask);
		long int cleanedInput = input & mask;
		input = cleanedInput | shiftedValue;

	}


	std::vector<spdDbTypes> getNoisyVerTypes(){
		std::vector<spdDbTypes> types(20);

		types[0]=spdNumeric;
		types[1]=spdSTRING;
		types[2]=spdNumeric;
		types[3]=spdSTRING;
		types[4]=spdNumeric;
		types[5]=spdSTRING;
		types[6]=spdNumeric;
		types[7]=spdSTRING;
		types[8]=spdNumeric;
		types[9]=spdSTRING;
		types[10]=spdNumeric;
		types[11]=spdSTRING;
		types[12]=spdNumeric;
		types[13]=spdSTRING;
		types[14]=spdNumeric;
		types[15]=spdSTRING;
		types[16]=spdNumeric;
		types[17]=spdSTRING;
		types[18]=spdNumeric;
		types[19]=spdSTRING;

		return types;

	}


	void insertBitsIntoArray( Uint32 *array, Uint32 data, unsigned offset, Uint32 nBits ){
		
		unsigned pos = offset /32;
		unsigned offsetInReg = offset%32;

		if (offsetInReg + nBits < 32){

			insertIntoInt32( array[pos], data, offsetInReg, nBits);
		}
		else{
			Uint32 offset1=offsetInReg;
			Uint32 offset2 =0;

			Uint32 nBits1=32-offset1;
			Uint32 nBits2=nBits-nBits1;
			Uint32 data2= data >> nBits1;

			insertIntoInt32( array[pos], data, offset1, nBits1);
			insertIntoInt32( array[pos+1], data2, offset2, nBits2);

		}
	    
	}


	const char * getApiDacName(unsigned apiDac){
	
		switch (apiDac){
			case 0 : return "APIDacRefHigh";
			case 1 : return "APIDacRefMid";
			case 2 : return "APIGTLRefA";
			case 3 : return "APIGTLRefD";
			case 4 : return "APITestHigh";
			case 5 : return "APITestLow";
			default : return "unknown";
		}
	}
}

