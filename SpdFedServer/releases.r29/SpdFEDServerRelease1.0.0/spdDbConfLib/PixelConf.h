#pragma once

#include "stdafx.h"



class PixelConf{
private:


public:
	

	virtual Uint8 getDpiWaitBefRow() const=0;
	virtual void setDpiWaitBefRow(Uint8 item)=0;

	virtual Uint8 getDpiSebMeb() const=0;
	virtual void setDpiSebMeb(Uint8 item)=0;

	virtual bool getDpiMaskChip() const=0;
	virtual void setDpiMaskChip(bool item)=0;

	virtual int getDpiEventNumber() const=0;
	virtual void setDpiEventNumber(int item)=0;

	virtual Uint8 getDpiStrobeLenght() const=0;
	virtual void setDpiStrobeLenght(Uint8 item)=0;

	virtual bool getDpiHoldRow() const=0;
	virtual void setDpiHoldRow(bool item)=0;

	virtual Uint8 getDpiSkipMode() const=0;
	virtual void setDpiSkipMode(Uint8 item)=0;

	virtual bool getDpiTdo8Tdo9() const=0;
	virtual void setDpiTdo8Tdo9(bool item)=0;

	virtual bool getDpiEnableCeSeq() const=0;
	virtual void setDpiEnableCeSeq(bool item)=0;

	virtual bool getDpiDataFormat() const=0;
	virtual void setDpiDataFormat(bool item)=0;

	virtual Uint8 getApiDacRefHi() const=0;
	virtual void setApiDacRefHi(Uint8 item)=0;

	virtual Uint8 getApiDacRefMid() const=0;
	virtual void setApiDacRefMid(Uint8 item)=0;

	virtual Uint8 getApiGtlRefA() const=0;
	virtual void setApiGtlRefA(Uint8 item)=0;

	virtual Uint8 getApiGtlRefD() const=0;
	virtual void setApiGtlRefD(Uint8 item)=0;

	virtual Uint8 getApiAnTestHi() const=0;
	virtual void setApiAnTestHi(Uint8 item)=0;

	virtual Uint8 getApiAnTestLow() const=0;
	virtual void setApiAnTestLow(Uint8 item)=0;

	virtual Uint32 getGolConfig() const=0;
	virtual Uint8 getGolConfig(int pos) const=0;
	virtual void setGolConfig(int pos, Uint8 item)=0;
	virtual void setGolConfig( Uint32 intem)=0;

	virtual Uint8 getPixelDac(int pixel, int dac) const=0;
	virtual void setPixelDac(int pixel, int dac, Uint8 item)=0;

	virtual float getAnalogConvValue(int pos) const=0;
	virtual void setAnalogConvValue(int pos, float item)=0;

	virtual int getNoisyPixelCount(int pixelChip)const=0;
	virtual void insertNoisyPixel(int pixelChip, int column, int row)=0;
	virtual void getNoisyPixel( int pixelChip, int pos, int &column, int &row)const=0;
	//virtual PixelCoordinate getNoisyPixel( int pixel, int pos)const=0;
	virtual void removeNoisyPixel( int pixelChip, int pos)=0;
	//virtual void insertNoisyPixel(int pixel, PixelCoordinate noisy)=0;

	virtual int getDeadPixelCount(int pixelChip)const=0;
	virtual void getDeadPixel(int pixelChip, int pos, int &column, int &row)const=0;
	//virtual PixelCoordinate getDeadPixel( int pixel, int pos)const=0;
	virtual void insertDeadPixel( int pixelChip,  int column, int row)=0;
	//virtual void insertDeadPixel(int pixel, PixelCoordinate deadPix)=0;

	virtual void removeDeadPixel( int pixelChip, int pos)=0;





	virtual ~PixelConf();

	PixelConf();
};
