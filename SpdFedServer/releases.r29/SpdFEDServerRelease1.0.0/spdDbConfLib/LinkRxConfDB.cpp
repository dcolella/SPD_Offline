#include "stdafx.h"
#include ".\linkrxconfdb.h"

LinkRxConfDB::LinkRxConfDB(void)
{

					//initializes the table settings 
	lrxVertable.setTableSettings( "LRXVERSION", "LRXVERSION", getLrxTableFields());

			// set all properties to zero
	setCntl(0,0);
	setCntl(1,0);
	setChanColumnMask(0,0);
	setChanColumnMask(1,0);
	setChanTempLimit(0,0);
	setChanTempLimit(1,0);
	setBusyControlReg(0,0);
	setBusyControlReg(1,0);
	setShiftSelector(0);
	setEnableMoveBit(0);
	setBusyMask(0,0);
	setBusyMask(1,0);
	setDisFastorcounter(0);
	setDisFastor(0);


}

LinkRxConfDB::~LinkRxConfDB(void)
{
}


std::vector <std::string> LinkRxConfDB::getLrxTableFields(){
	std::vector<std::string> fields(14);

	fields[0]="CNTL0";
	fields[1]="CNTL1";
	fields[2]="CHAN0_COLUMN_MASK";
	fields[3]="CHAN1_COLUMN_MASK";
	fields[4]="CHAN0_TEMP_LIMIT";
	fields[5]="CHAN1_TEMP_LIMIT";
	fields[6]="BUSY_CONTROL_REG0";
	fields[7]="BUSY_CONTROL_REG1";
	fields[8]="SHIFT_SELECTOR";
	fields[9]="ENABLE_MOVE_BIT";
	fields[10]="BUSY_MASK0";
	fields[11]="BUSY_MASK1";
	fields[12]="DIS_FASTORCOUNTER";
	fields[13]="DIS_FASTOR";

	return fields;


}

int LinkRxConfDB::getDataFromDB(){
	std::vector<Uint32> values(14,0);

			// gets the values from the database
	lrxVertable.getValues( values);

	setCntl(0,values[0]);
	setCntl(1,values[1]);
	setChanColumnMask(0,values[2]);
	setChanColumnMask(1,values[3]);
	setChanTempLimit(0,values[4]);
	setChanTempLimit(1,values[5]);
	setBusyControlReg(0,values[6]);
	setBusyControlReg(1,values[7]);
	setShiftSelector( values[8]);
	setEnableMoveBit( (Uint8) values[9]);
	setBusyMask(0,values[10]);
	setBusyMask(1,values[11]);
	setDisFastorcounter( (Uint8) values[12]);
	setDisFastor( (Uint8) values[13]);

	return 0;

}
long int LinkRxConfDB::update(){
	std::vector<Uint32> values(14,0);
	

	values[0]=getCntl(0);
	values[1]=getCntl(1);
	values[2]=getChanColumnMask(0);
	values[3]=getChanColumnMask(1);
	values[4]=getChanTempLimit(0);
	values[5]=getChanTempLimit(1);
	values[6]=getBusyControlReg(0);
	values[7]=getBusyControlReg(1);
	values[8]=getShiftSelector();
	values[9]=getEnableMoveBit();
	values[10]=getBusyMask(0);
	values[11]=getBusyMask(1);
	values[12]=getDisFastorcounter();
	values[13]=getDisFastor();
	
	return lrxVertable.update( values);
}