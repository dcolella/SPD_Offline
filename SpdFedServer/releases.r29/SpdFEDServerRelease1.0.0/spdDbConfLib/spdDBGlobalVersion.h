#ifndef SPDDBGLOBALVERSION_H
#define SPDDBGLOBALVERSION_H

#include "spdVerTable.h"
#include "stdafx.h"

class spdDBGlobalVersion
{

	
			// gets the rows names for this table in the database
			// its is used in the initialization of the topTable object (spdVerTable)
	std::vector <std::string> getSpdVerFields();
	std::vector <std::string> getRunTypeFields();

			// object to manage the top level versioned table
	SpdVerTable topTable;
	SpdVerTable runTypeTable;
	
			// internal members for the versions of side A and C
			// router and detector configuration + run type
	long int detSideA_Ver;
	long int detSideC_Ver;
	long int routSideA_Ver;
	long int routSideC_Ver;
	long int runType;

	std::string runTypeName ;

public:
	
			// update method to generate a new version table
			// inserting all the versions
	long int update( long int hsSideA, 
				  long int hsSideC, 
				  long int routerSideA,
				  long int routerSideC,
				  long int typeOfRun);
			
			// updates the database version with the internal members
	long int update();

			// setters for the internal members	
	void setHsVerSideA(long int ver){detSideA_Ver =  ver;};
	void setHsVerSideC(long int ver){detSideC_Ver =  ver;};
	void setRouterVerSideA(long int ver){routSideA_Ver =  ver;};
	void setRouterVerSideC(long int ver){routSideC_Ver =  ver;};
	void setRunType(long int ver){runType =  ver;};

		// getters for the same internal members
	long int getHsVerSideA(){return detSideA_Ver;};
	long int getHsVerSideC(){return detSideC_Ver;};
	long int getRouterVerSideA(){return routSideA_Ver;};
	long int getRouterVerSideC(){return routSideC_Ver;};
	long int getRunType(){return runType;};

	std::string getRunTypeName(){return runTypeName;};

		// gets the current version
	long int getVersionNumber(){return topTable.getVersionNumber(); };
		// set the version number updating the internal members with the values in the database
	void setVersionNumber(long int version);

	spdDBGlobalVersion(void);
	~spdDBGlobalVersion(void);
};

#endif