#ifndef SPDDETECTORCONF_H
#define SPDDETECTORCONF_H

#include ".\pixelconfdb.h"
//using namespace std;

const int NSectors = 10;
const int NChannels = 60;
const int NHalfstaves = 6;

// class to mange the detector configuration 
class SpdDetectorConf
{
			// configuration for 60 HS's
	PixelConfDb hsChannels[NChannels];

			// obects to manage the versioning
			// 10 sector versions plus its own detector version
	SpdVerTable sectorTables[NSectors];
	SpdVerTable detectorTable;
			
	std::vector <std::string> getSectorVerFields();
	std::vector <std::string> getDetectorVerFields();
			
			// sets the sector version
	int setSectorVersion( int sector, long int version);
			// updates one sector 
	long int updateSector( int sector);
			// gets channels number from sector and hs number
	int getChannelNumber( int sector, int hs){
		return (hs + 6*sector );
	};

public:
			// gets the last version 
	long int getLastVersion();
			// updates data with db
	long int update();
			// gets data with db
	int getDataFromDB();
			// to get channel configuration			
	PixelConfDb & getChannel(int channel){return hsChannels[channel];};
	PixelConfDb & operator[](unsigned int channel){return hsChannels[channel];}


			
	void setVersionNumber(long int version);
	long int getVersionNumber(){ return detectorTable.getVersionNumber();}

				
	SpdDetectorConf(void);
	~SpdDetectorConf(void);
};
#endif