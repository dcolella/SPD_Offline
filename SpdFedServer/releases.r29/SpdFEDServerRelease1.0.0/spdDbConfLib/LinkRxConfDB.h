#ifndef LINKRXCONFDB_H
#define LINKRXCONFDB_H

#include "linkrxconf.h"
#include "spdvertable.h"
//using namespace std;

class LinkRxConfDB //: public LinkRxConf
{
	Uint32 cntl[2];
	Uint32 chanColumnMask[2];
	Uint32 chanTempLimit[2];
	Uint32 busyCntlReg[2];
	Uint32 shiftSelector;
	Uint8  enableMoveBit;
	Uint32  busyMask[2];
	Uint8  disFastorcounter;
	Uint8  disFastor;
	
	SpdVerTable lrxVertable;
	std::vector<std::string> getLrxTableFields();
public:

	Uint32 getCntl(int pos){return cntl[pos];};
	 void setCntl(int pos, Uint32 item){cntl[pos]=item;};
  
					// 2 32 bit Channel colum mask
	 Uint32 getChanColumnMask(int pos){return chanColumnMask[pos];};
	 void setChanColumnMask(int pos, Uint32 item){chanColumnMask[pos]=item;};

	 Uint32 getChanTempLimit(int channel){return chanTempLimit[channel];};
	 void setChanTempLimit(int channel, Uint32 temp){chanTempLimit[channel]=temp;};

					// 2 32 bit Busy control registers 
	 Uint32 getBusyControlReg( int pos){return busyCntlReg[pos];};
	 void setBusyControlReg(int pos, int item){busyCntlReg[pos]=item;};

	 Uint32 getShiftSelector(){return shiftSelector;};
	 void setShiftSelector(Uint32 item){shiftSelector=item;};

	 Uint8 getEnableMoveBit(){return 0x7 & enableMoveBit;};
	 void setEnableMoveBit(Uint8 item){enableMoveBit=item;};

					// 2 32 bit busy mask registers 
	 Uint32 getBusyMask(int pos){return busyMask[pos];};
	 void setBusyMask(int pos ,Uint32 item){busyMask[pos]=item;};

			
	 Uint8 getDisFastorcounter(){return disFastorcounter; };
	 void setDisFastorcounter(Uint8 item){disFastorcounter=item;};

	 Uint8 getDisFastor(){return 0x7 & disFastor ;};
	 void setDisFastor(Uint8 item){disFastor=item;};

	 
			// update to syncronize with the database
	long int update();
			// gets data from the database
	int getDataFromDB();

	void setVersionNumber(long int version){lrxVertable.setVersionNumber(version);};
	long getVersionNumber(){return lrxVertable.getVersionNumber();};


	LinkRxConfDB(void);
	~LinkRxConfDB(void);
};

#endif // LinkRxConfDB_H