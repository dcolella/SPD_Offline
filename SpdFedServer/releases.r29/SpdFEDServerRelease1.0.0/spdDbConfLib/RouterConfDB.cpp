#include "stdafx.h"
#include ".\routerconfdb.h"


RouterConfDB::RouterConfDB(void){
				// sets the internal tables settings
	routVerTable.setTableSettings("ROUTERVER","ROUTERVER",getRoutVerFields());
	routConfTable.setTableSettings("ROUTREG_CONFVER","ROUTREG_CONFVER",getRoutConfFields());

	en_fastorcoincidence=0;
	dis_ttctriggers=0;
	en_orbinternal=0;
	fasterjtag=0;
	shorttditdo=0;
	en_routerheader=0;
	en_tpinlo=0;
	busy=0;
	nodatatodaq=0;
	dpmsamplemode=0;
	jtagtouse = 0x1;
	channelpresent=0x3f;


}



// to encapsulate the channGroup array
RoutChannelDb  & RoutChanGroup::operator [](unsigned int channel){
	if ( channel >=0 && channel < 6) 
		return channels[channel];
	else{
		spdLogger &logger = spdLogger::getInstance();
		std::string err="out of memory: there are only 6 channels in one router";
		logger.log(err);

			// throws an exception
		throw err;
			// returns the zero by default
		return channels[0];
	}
}
// to encapsulate the lrxGroup array
LinkRxConfDB & RoutLrxGroup::operator [](unsigned int channel){
	if ( channel >=0 && channel < 3) 
		return lrx[channel];
	else{
		spdLogger &logger = spdLogger::getInstance();
		std::string err="out of memory: there are only 3 linkrx in one router";
		logger.log(err);

			// throws an exception
		throw err;
			// returns the zero by default
		return lrx[0];
	}
}



RouterConfDB::~RouterConfDB(void)
{
}


std::vector<std::string> RouterConfDB::getRoutVerFields(){
	std::vector<std::string> fields(10);

	fields[0]="ROUTREG_CONFVER";
	fields[1]="LRXVERSION0";
	fields[2]="LRXVERSION1";
	fields[3]="LRXVERSION2";
	fields[4]="ROUTCHANNEL0";
	fields[5]="ROUTCHANNEL1";
	fields[6]="ROUTCHANNEL2";
	fields[7]="ROUTCHANNEL3";
	fields[8]="ROUTCHANNEL4";
	fields[9]="ROUTCHANNEL5";

	return fields;
}

void RouterConfDB::setVersionNumber(long version){
	std::vector<long> values(10,0);

	routVerTable.setVersionNumber(version);
			// gets the versions stored in the table
	routVerTable.getValues(values);


			// sets the versions to all the devices
	routConfTable.setVersionNumber(values[0]);

	int pos = 1;
	for (int n = 0; n < lrx.size(); n ++ ){
		lrx[n].setVersionNumber(values[pos+n]);
	}

	pos = lrx.size() +1;
	for (int n = 0 ; n < channel.size(); n++ ){
		channel[n].setVersionNumber( values[pos+n]);
	}
}



long RouterConfDB::update(){
	std::vector<long> values(10,0);
	
			// gets the update values from all the children tables
	values[0]=updateRoutConf();

	int pos = 1;
	for (int n = 0; n < lrx.size(); n ++ ){
		values[pos+n]=lrx[n].update();
	}

	pos = lrx.size() +1;
	for (int n = 0 ; n < channel.size(); n++ ){
		values[pos+n]=channel[n].update();
	}
	
	return routVerTable.update( values);
}
std::vector<std::string> RouterConfDB::getRoutConfFields(){
	std::vector<std::string> fields(12);

	fields[0]="CHANNELPRESENT";
	fields[1]="JTAGTOUSE";
	fields[2]="DPMSAMPLEMODE";
	fields[3]="NODATATODAQ";
	fields[4]="BUSY";
	fields[5]="EN_TPINLO";
	fields[6]="EN_ROUTERHEADER";
	fields[7]="SHORTTDITDO";
	fields[8]="FASTERJTAG";
	fields[9]="EN_ORBINTERNAL";
	fields[10]="DIS_TTCTRIGGERS";
	fields[11]="EN_FASTORCOINCIDENCE";

	return fields;

}

long RouterConfDB::updateRoutConf(){
	std::vector<Uint32> values(12,0);

	values[0]=(Uint32)getChannelPresent();
	values[1]=(Uint32)getJtagToUse();
	values[2]=(Uint32)getDpmSampleMode();
	values[3]=(Uint32)getNoDataToDaq();
	values[4]=(Uint32)getBusy();
	values[5]=(Uint32)getEnTpInLo();
	values[6]=(Uint32)getEnRoutheader();
	values[7]=(Uint32)getShortDtiTdo();
	values[8]=(Uint32)getFasterJtag();
	values[9]=(Uint32)getEnOrbInternal();
	values[10]=(Uint32)getDisTtcTriggers ();
	values[11]=(Uint32)getEnFastorCoincidence();

	return routConfTable.update(values);

}

int RouterConfDB::getDataFromDB(){
				// gets the data for all the children tables
	getRoutConfValues();

	int pos = 1;
	for (int n = 0; n < lrx.size(); n ++ ){
		lrx[n].getDataFromDB();
	}

	pos = lrx.size() +1;
	for (int n = 0 ; n < channel.size(); n++ ){
		channel[n].getDataFromDB();
	}
	
	return 0;
}

void RouterConfDB::getRoutConfValues(){
	std::vector<Uint32> values(12,0);
	
		// gets the values
	routConfTable.getValues(values);
		// sets all the values from the database
	setChannelPresent(values[0]);
	setJtagToUse(values[1]);
	setDpmSampleMode((Uint8)values[2]);
	setNoDataToDaq((Uint8)values[3]);
	setBusy((Uint8)values[4]);
	setEnTpInLo((Uint8)values[5]);
	setEnRoutheader((Uint8)values[6]);
	setShortDtiTdo((Uint8)values[7]);
	setFasterJtag((Uint8)values[8]);
	setEnOrbInternal((Uint8)values[9]);
	setDisTtcTriggers ((Uint8)values[10]);
	setEnFastorCoincidence((Uint8)values[11]);
}

