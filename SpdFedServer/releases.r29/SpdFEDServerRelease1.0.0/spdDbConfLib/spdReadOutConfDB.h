#pragma once
#include "RouterConfDB.h"
#include "spdvertable.h"


using namespace std;
const int NRouters = 20;

class spdReadOutConfDB
{

	SpdVerTable readOutTable;
	RouterConfDB routersConf[NRouters];
	vector <string> getReadOutElecVerFields();

public:

	long int update();
			// gets data from the database
	int getDataFromDB();

	void setVersionNumber(long int version);
	long int getVersionNumber(){return readOutTable.getVersionNumber();}

	RouterConfDB & operator[](int router);

	spdReadOutConfDB(void);
	~spdReadOutConfDB(void);
};
