#pragma once
#include "pixelconf.h"

const int SIZE_APIVECTOR=6;
const int SIZE_DIPVECTOR=8;
const int SIZE_GOLVECTOR=4;
const int SIZE_ANALOGCONFVECTOR=400;
const int N_PIXELS = 10;
const int N_PIXELDACS = 44;


class PixelConfTest :
	public PixelConf
{
private:
	char (*pixDacVector)[44];	//10x44
	char *apiVector;	//6
	char *dipVector;	//8
	char *golVector;		//4
	float *analogConfVector;	//1600 chars, 400 floats
	short *noisyPixelsVector;	//ask ivan
	short *deadPixelsVector;

	int sizeNoisyPixels;
	int sizeDeadPixels;
	char Channel;


	void iniVectors(void);
	void deleteVectors(void);

public:

	PixelConfTest(void);
	~PixelConfTest(void);


};
