#pragma once

#ifndef BitsManage_h
#define BitsManage_h



class BitsManage{
 private:	
	UInt32 NewTerm;
	UInt32 Position;      //bit address
	UInt32 Nbit;
	UInt32 NShift;
	UInt32 CleanerWord;

	UInt8 BMOperation;   //0 = change content, 1 = shift, 2 = all

  public:
	BitsManage(void);
	~BitsManage(void);
	
	void ShiftNewTerm(void){NewTerm <<= Position;}; 
	void SetClWord(void){CleanerWord = _lrotl((0xffffffff - ((1<< Nbit) - 1)), Position);};
	void SetClWord(UInt32 ClWord){CleanerWord = ClWord;};
	void ModifyUInt32(UInt32 &OldUInt32);    //ok
	void ModifyUInt32(UInt32 &OldUInt32, UInt32 NewEle, UInt8 position, UInt8 bitNumber);
	UInt32 ExtractFromUInt32(UInt32 Input, UInt8 position, UInt8 bitNumber);

    
	UInt32 UInt8ToUInt32(UInt8 * DataIn);
	void UInt32ToUInt8(UInt8 * DataOut, UInt32 DataIn);
	friend class LinkRxIntAddr;
	friend class RoutIntAddr;
	friend class BusyCardIntAddr;
};
#endif