#pragma once


struct DACParameters {
	unsigned int maximum;		// maximum value of the DAC for this scan
	unsigned int minimum;		// minimum value of the DAC for this scan
	unsigned int step;			// step of the DAC value for this scan
	unsigned int current;		// current value of the DAC in the scan loop
	UInt8 index;
};

//! pure virtual class to define as base class for spd scans
class SpdScan
{
	bool active;
public:

				//! Method to start the calibration of the Fast Or
	virtual unsigned start(){return 0;};

		//! Method to restart the calibration of the Fast Or
	virtual unsigned restart(void){active = true;return 0;};

		//! Method to stop the calibration of the Fast Or
	virtual void stop(){active = false;};
	
		//! Method to lead the calibration of the Fast Or
	virtual void scan(){};

	virtual bool isActive(){return active;};

	SpdScan(void);
	virtual ~SpdScan(void);
};
