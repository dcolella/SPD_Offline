#include "StdAfx.h"
#include ".\spdfastorcalibration.h"
#include "SPDConfig.h"

spdFastorCalibration::spdFastorCalibration(SPDConfig *spdConfig){

	log = &spdLogger::getInstance();

	active = false;

	parent = spdConfig;

	if (this->parent->side == 'A'){
		pitCmdStatus = new DimUpdatedInfo( "PIT/CMD_STATUS_FED_A", "", this);
		getFastorCounters = new DimUpdatedInfo( "PIT/FO_COUNTERS_SIDE_A",-1, this);
	}
	else if (this->parent->side == 'C'){
		pitCmdStatus = new DimUpdatedInfo( "PIT/CMD_STATUS_FED_C", "", this);
		getFastorCounters = new DimUpdatedInfo( "PIT/FO_COUNTERS_SIDE_C",-1, this);
	}
	else{
		log->log("ERROR: side not assigned properly quiting");
		exit(1);
	}
	
		// now we will need some kind of state machine to keep track of the status
		// because of the flush problems in the network
	this->stepPhase = 0;
		
		// settings that will always be here 
	testPulse = 0;
	fopol.index = 20;
	convpol.index = 17;
	compref.index = 16;
	prevth.index = 39;
	cgpol.index = 14;
	
	stepCounter = 0;
	//
	this->pitRequestTimeout= 10000;
}

spdFastorCalibration::~spdFastorCalibration(void) {

	delete getFastorCounters;
	delete pitCmdStatus;

}
		//! Method to restart the calibration of the Fast Or
unsigned spdFastorCalibration::restart (void) {
	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}
	this->active = true;
	this->foStep();
	return 0;
}

		//! Method to stop the calibration of the Fast Or
void spdFastorCalibration::stop() {
	active = false;
	//stepPhase = 0;
}

// debug method only 
UInt32 spdFastorCalibration::testFastorCalibration(){
	timerScan.restart("starting fastor calibration test scan");
	active = true;

	fopol.minimum = 128;
	fopol.maximum = 128;
	fopol.step = 1;
	fopol.current = fopol.minimum;
	this->setDacEnabledHalfStaves(fopol.index, fopol.current);

	convpol.minimum = 128;
	convpol.maximum = 128;
	convpol.step = 1;
	convpol.current = convpol.minimum;
	this->setDacEnabledHalfStaves(convpol.index, convpol.current);

	compref.minimum = 128;
	compref.maximum = 128;
	compref.step = 1;
	compref.current = compref.minimum;
	this->setDacEnabledHalfStaves(compref.index, compref.current);

	prevth.minimum = 128;
	prevth.maximum = 128;
	prevth.step = 1;
	prevth.current = prevth.minimum;
	this->setDacEnabledHalfStaves(prevth.index, prevth.current);

	triggerNumber = 100;
				
		// will set the corresponding test pulse to all enabled channels
	testPulse = 0;
	this->setTpMatrixAllEnabled(testPulse);

	return 0;
}
	// Method to start the calibration of the Fast Or
unsigned spdFastorCalibration::start (unsigned int fopolMinimum,
									unsigned int fopolMaximum,
									unsigned int fopolStep,
									unsigned int convpolMinimum,
									unsigned int convpolMaximum,
									unsigned int convpolStep,
									unsigned int comprefMinimum,
									unsigned int comprefMaximum,
									unsigned int comprefStep,
									unsigned int cgpolMinimum,
									unsigned int cgpolMaximum,
									unsigned int cgpolStep,
									unsigned int prevthMinimum,
									unsigned int prevthMaximum,
									unsigned int prevthStep,
									UInt32 triggerNum,
									UInt32 testPulseMatrices) {

	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}


	triggerNumber = triggerNum;
	testPulseActive = testPulseMatrices;

	testPulse = 0;
	// checks the first testpulse to be set and applies it to the hardware
	testPulse = checkNextTestPulse( testPulse);

	if (testPulse < 0){
		log->log("ERROR: there are no active test pulses scan did not start");
		this->active=false;
		return 1;
	}

	timerScan.restart("starting fastor calibration scan");
	
		// sets all values in the internal variables and all dacs to start the scan

	fopol.minimum = fopolMinimum;
	fopol.maximum = fopolMaximum;
	fopol.step = fopolStep;
	fopol.current = fopol.minimum;
	this->setDacEnabledHalfStaves(fopol.index, fopol.current);

	convpol.minimum = convpolMinimum;
	convpol.maximum = convpolMaximum;
	convpol.step = convpolStep;
	convpol.current = convpol.minimum;
	this->setDacEnabledHalfStaves(convpol.index, convpol.current);

	compref.minimum = comprefMinimum;
	compref.maximum = comprefMaximum;
	compref.step = comprefStep;
	compref.current = compref.minimum;
	this->setDacEnabledHalfStaves(compref.index, compref.current);

	prevth.minimum = prevthMinimum;
	prevth.maximum = prevthMaximum;
	prevth.step = prevthStep;
	prevth.current = prevth.minimum;
	this->setDacEnabledHalfStaves(prevth.index, prevth.current);
				
	cgpol.minimum = cgpolMinimum;
	cgpol.maximum = cgpolMaximum;
	cgpol.step = cgpolStep;
	cgpol.current = cgpol.minimum;
	this->setDacEnabledHalfStaves(cgpol.index, cgpol.current);
	
	

	active = true;
	stepCounter = 0;

	setTpMatrixAllEnabled( testPulse);

		// will start the scan
	this->foStep();
	this->timePitRequest= GetTickCount();
	return 0;
}

void spdFastorCalibration::scan(){

	if(this->active == false) return;

	if ( (GetTickCount() - this->timePitRequest) > this->pitRequestTimeout ){
		log->log("ERROR: PIT fed failed to answer in %d ms, is the PIT FED runnig? the scan will be stopped",this->pitRequestTimeout);
		this->active = false;
	}

}

// infoHandler callback method from DIM,
// on update of a service in the DIM server (PIT FED) this method will be called automativally 
// all services updates will be handled here
void spdFastorCalibration::infoHandler(){


	log->logToScr("service '%s' updated", this->getInfo()->getName());

	if (this->getInfo() == pitCmdStatus ){
		//log->logToScr("its our private command status: '%s'",pitCmdStatus->getString());

		if (strcmp(pitCmdStatus->getString(), "EXECUTING") == 0){
			return;
		}
	}
	else if (this->getInfo() == getFastorCounters){
		//log->logToScr("fo counters counters updated ");
		return;
	}
	else{
		return;
	}

	switch (stepPhase){
		case 0: // idle
			break;
		case 1: 
			
				// send triggers to all halfstaves
			this->parent->sendTriggerToEnabledRouters(triggerNumber);
			
			if (this->parent->side == 'A'){
				// stopping the conters and getting the data
				sendPitCommand("stop_focounters_for_fed A");
			}
			else {
				sendPitCommand("stop_focounters_for_fed C");
			}
			stepPhase = 2;
			break;
		case 2:	
			
				// will write the header as soon as we have the data back
				// and will increment and apply the next dac values for the next measurement
			finalizeStep();

				// if the scan did not reach the end we will restart the cycle
			if (active == true){
				this->foStep();
			}
			else {stepPhase = 0;}
			break;
		default:
			log->logToFile("(spdFastorCalibration::infoHandler) ERROR: unkown step phase %d", stepPhase);
			break;
	}

}


// Start function of the fastor calibration scan,
// first it will just start the counters and then wait till the PIT fed server answer
// when it answers the code will jump to the infoHandler method
// and there we have the sequence of operations in the middle 
unsigned spdFastorCalibration::foStep() {
	
	stepPhase = 1;
	int error;

	//log->log("sending start counters command for fed %c", this->parent->side);
	// first starts all counters 
	if (this->parent->side == 'A'){
		error = this->sendPitCommand("start_focounters_for_fed A");
	}
	else{
		error = this->sendPitCommand("start_focounters_for_fed C");
	}
	this->timePitRequest= GetTickCount();
	return 0;
}

// last step in the fastor calibration scan
// it writes the data to the routers, increments the dac values for the next step
// checks if it is the end of the scan and writes the new test pulse if needed 
int spdFastorCalibration::finalizeStep(){

	UInt32 * foData = (UInt32 *) getFastorCounters->getData();

	this->sendHeadersAllEnabled(foData);

	//cout << "Finished step " << dec << stepCounter<< endl;
	//cout << " fopol = "		<< (int)fopol.current	<< " / " << (int)fopol.maximum << endl;
	//cout << " convpol = "	<< (int)convpol.current	<< " / " << (int)convpol.maximum << endl; 
	//cout << " compref = "	<< (int)compref.current	<< " / " << (int)compref.maximum << endl; 
	//cout << " cgpol = "	<< (int)cgpol.current	<< " / " << (int)cgpol.maximum << endl;
	//cout << " prevth = "	<< (int)prevth.current	<< " / " << (int)prevth.maximum << endl; 
	//cout << "Matrix number = " << testPulse << endl;


	stepCounter ++;
		// Increments the DACs until they reach their maximum value, starting from fopol.
		// The logic is like a counter, when fopol reaches its max value, then it's set back to its minimum
		// and convpol is increased... and so on for the other DACs
	fopol.current += fopol.step;

	if (fopol.current <= fopol.maximum){
		// will set the dac value for this scan
		this->setDacEnabledHalfStaves( fopol.index, fopol.current);
		return 0;
	}
	
	fopol.current = fopol.minimum;
	this->setDacEnabledHalfStaves( fopol.index, fopol.current);

	convpol.current += convpol.step;

	if (convpol.current <= convpol.maximum){
		// will set the dac value for this scan
		this->setDacEnabledHalfStaves( convpol.index, convpol.current);
		return 0;
	}

	convpol.current = convpol.minimum;
	this->setDacEnabledHalfStaves( convpol.index, convpol.current);

	log->logToScr( "fastor scan : finished compref %d", compref.current);
	compref.current += compref.step;

	if (compref.current <= compref.maximum){
		// will set the dac value for this scan
		this->setDacEnabledHalfStaves( compref.index, compref.current);
		return 0;
	}

	compref.current = compref.minimum;
	this->setDacEnabledHalfStaves( compref.index, compref.current);

	cgpol.current += cgpol.step;

	if (cgpol.current <= cgpol.maximum){
		// will set the dac value for this scan
		this->setDacEnabledHalfStaves( cgpol.index, cgpol.current);
		return 0;
	}


	cgpol.current = cgpol.minimum;
	this->setDacEnabledHalfStaves( cgpol.index, cgpol.current);

	prevth.current += prevth.step;

	if (prevth.current <= prevth.maximum){
		// will set the dac value for this scan
		this->setDacEnabledHalfStaves( prevth.index, prevth.current);
		return 0;
	}

	log->logToFile( "fastor scan : finished test matrix %d", testPulse);
	testPulse++;
	testPulse= this->checkNextTestPulse(testPulse);

	if (testPulse < 0){
		timerScan.stop("Finished fastor calibration scan");
		
		log->log( "fastor scan finished: number of steps = %d, time elapsed = %f", stepCounter, timerScan.elapsed_time());
		this->active = false;

		return 0;
	}

	prevth.current = prevth.minimum;
	this->setDacEnabledHalfStaves( prevth.index, prevth.current);

	this->setTpMatrixAllEnabled(testPulse);

	return 0;
}

// gets the next test pulse matrix number to use in the test 
int spdFastorCalibration::checkNextTestPulse( int currentTestPulse){

	int bit;
	for (bit = currentTestPulse; bit < numberOftestPulses ; bit ++){
		if (((testPulseActive >> bit) & 1) != 0){
			return bit;
		}
	}

	return -1;
}


// sets the matrix number un the hardware and returns the number of the next testpulse
// if 7 or bigger means that there are no more enabled test pulses 
UInt32 spdFastorCalibration::setTpMatrixAllEnabled(int testPulse){
		// enables the pixel mask and not the test pulse
	bool mask = true;
	UInt32 testPulseMatrix[256];
	for (unsigned int n = 0; n<256; n++) testPulseMatrix[n] = 0;


	this->row = 256;
	this->column = 32;

	unsigned numberOfPixelsInChip = 8192;
	unsigned pixelStep = 257;

	switch (testPulse) {
		case 0:		// Empty matrix
			break;

		case 1:		// (128,16)
			this->row = 128;
			this->column = 16;

			this->setPixelInMatrix(testPulseMatrix, row, column);
			break;

		case 2:		// (0,0)
			
			this->row = 0;
			this->column = 0;
			this->setPixelInMatrix(testPulseMatrix, row, column);
			break;
			

		case 3:		// (0,31)
			
			this->row = 0;
			this->column = 31;
			this->setPixelInMatrix(testPulseMatrix, row, column);
			break;
		

		case 4:		// (255,31)
			
			this->row = 255;
			this->column = 31;
			this->setPixelInMatrix(testPulseMatrix, row, column);
			break;
		

		case 5:		// (255,0)
			
			this->row = 255;
			this->column = 0;
			this->setPixelInMatrix(testPulseMatrix, row, column);
	
			break;

		case 6:		// High occupancy
		
			pixelStep = 50;
			for (unsigned int pixel = 0; pixel < numberOfPixelsInChip; pixel += pixelStep) {
				this->column = pixel/256;
				this->row = pixel - (column*256);

				setPixelInMatrix(testPulseMatrix, row, column);
				
			}
			break;
			
		default:
			log->log("(spdFastorCalibration::setTpMatrixAllEnabled) ERROR: unknown test pulse: %d", testPulse);
			return -1;
		
	}

	// here sets the test pulse in all enabled channels
	for (int channelNum = 0; channelNum < NHalfStaves; channelNum ++) {
		// Checks if the channel is in test mode
		if (parent->halfStaves[channelNum]->getChActStatus() == 2) {
			UInt32 error = parent->halfStaves[channelNum]->LoadHSPixelMatrix (1023, testPulseMatrix, true, mask);

			if (error) return error;
		}
	}
	return 0;
}


// writes the header to all enabled routers (routers that have at least one hs ennabled)
UInt32 spdFastorCalibration::sendHeadersAllEnabled( UInt32 * foData ){
	
	unsigned fastOrCounters[300];
	unsigned chipsEnabled[6];

	//Array containing the header data (test pulse, pixel coordinates, DAC values and FO counters)
	const int sizeOfHeader = 47;
	UInt32 header[sizeOfHeader];

			// Creates the array with the Fast-Or data
	for (int i = 0; i < 300; i++){
		UInt32 counter1 = foData [ i*2];
		UInt32 counter2 = foData [ i*2 + 1];

				// Inserts into a UInt32 two counter values
		spdGlobals::insertIntoInt32( counter2, counter1, 16, 16);		// 32 bits -> [counter1][counter2]
		
		fastOrCounters[i] = counter2;									// 300 elements -> [1-2][3-4][5-6]...
	}

	int routerNumber = -1;
				// Writes the header for the fast or calibration
	for (int channelNumber = 0; channelNumber < NHalfStaves; channelNumber ++) {
		if ((parent->halfStaves[channelNumber]->getChActStatus() == 2) && (routerNumber != channelNumber/6)) {

			routerNumber = channelNumber/6;

				// Creates the array with the chips activated for the calibration 
			for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter ++) {
				unsigned int HSnumber = routerNumber*6 + HSinRouter;

				if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
					chipsEnabled[HSinRouter] = parent->halfStaves[HSnumber]->GetDPI()->GetDPIConfMaskChip();
				}
				else chipsEnabled[HSinRouter] = 0;
			}	

			
			header[0] = routerNumber;
			header[1] = scanType;
			header[2] = triggerNumber;
			header[3] = chipsEnabled[0] + (chipsEnabled[1] << 10) + (chipsEnabled[2] << 20);
			header[4] = chipsEnabled[3] + (chipsEnabled[4] << 10) + (chipsEnabled[5] << 20);
			header[5] = parent->HSData.getGlobalDBVersionNumber();
			header[6] = testPulse + (row << 10) + (column << 20);
			header[7] = fopol.index;
			header[8] = fopol.current;
			header[9] = convpol.index;
			header[10] = convpol.current;
			header[11] = compref.index;
			header[12] = compref.current;
			header[13] = cgpol.index;
			header[14] = cgpol.current;
			header[15] = prevth.index;
			header[16] = prevth.current;

			
			const int numberOfSettings = 17;
			 
						// Fills the remaining elements of the header with the FO counters
			const int NumberOfFastorInRouter = 30;
		
			for (int i = 0; i < NumberOfFastorInRouter; i++){

				unsigned index = routerNumber*NumberOfFastorInRouter + i;
				header[i+numberOfSettings] = fastOrCounters[index];

			}

			saveFoDataToLog( routerNumber, foData);
			
			

			unsigned error = parent->routers[routerNumber]->WriteHeader(header, sizeOfHeader);
			if (error) return error;
			error = parent->routers[routerNumber]->SendTrigger(1);
			if (error) return error;
		}
	}
	return 0;
}

// writes a dac value to all enabled HS
UInt32 spdFastorCalibration::setDacEnabledHalfStaves( UInt8 DACn, UInt8 DACValue){
	
	for (int channelNumber = 0; channelNumber < NHalfStaves; channelNumber ++) {

		if (parent->halfStaves[channelNumber]->getChActStatus() == 2) {
			UInt32 error=parent->halfStaves[channelNumber]->LoadHSPixelDAC (1023, DACn, DACValue);
			if (error) return error;
		}	
	}
	return 0;
}


// sends a command to the pit fed
int spdFastorCalibration::sendPitCommand( const char *command){
						// Stops the Fast Or counters in the pixeltrigger
	return DimClient::sendCommand( "PIT/COMMAND", command);
}

// saves the fastor data to the log file, 
// nice to generate all of these ruby graphs 
int spdFastorCalibration::saveFoDataToLog( int router, UInt32 *foData){

	stringstream line(" foData: \t");

	line<< dec << router << "\t";
	line<< dec << triggerNumber << "\t";
	line<< dec << testPulse << "\t";
	line<< dec << fopol.current << "\t";
	line<< dec << convpol.current << "\t";
	line<< dec << compref.current << "\t";
	line<< dec << cgpol.current << "\t";
	line<< dec << prevth.current << "\t";
	
	const int nCountersRouter=60;


	for(int fastor = 0 ; fastor < nCountersRouter ; fastor ++){
		line << dec << foData[ router*nCountersRouter+fastor]<<"\t";
	}

	log->logToFile( line.str());
	
	return 0;
}