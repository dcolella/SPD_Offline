#include "StdAfx.h"
#include ".\digitalpilot.h"

DigitalPilot::DigitalPilot(void){
	//setting DPI Conf Default after Reset
	//DPIConf[0] = 0x11003FF9;
    //DPIConf[1] = 0x0001FFFD;
	DPIConf[0] = 0x0;
    DPIConf[1] = 0x0;
	this->SetDPIConfWaitBefRow(1);
	this->SetDPIConfSebMeb(1);
	this->SetDPIConfMaskChip(0x3FF);
	this->SetDPIConfEventNumb(0);
	this->SetDPIConfStrobeLength(0);
	this->SetDPIConfHoldRow(1);
	this->SetDPIConfTDO8TDO9(0);
	this->SetDPIConfSkipMode(0);
	this->SetDPIConfEnableCESeq(1);
	this->SetDPIConfDataFormat(1);
    
	
	//setting DPI Internal reg Default after Reset
	DPIInternal[0] = 0x0;
	DPIInternal[1] = 0x0;
	DPIInternal[2] = 0x0;

    //scan_strobe_busy
	this->SetDPIInternalStrobe_i(1);
	this->SetDPIInternalStrobeCycCnt(1);
	this->SetDPIInternalMebVal(0);
	this->SetDPIInternalBusy(0);
	this->SetDPIInternalBusyViolation(0);
	this->SetDPIInternalIdleViolation(0);

	//scan_queue
	this->SetDPIInternalL2WrPtr(0);
	this->SetDPIInternalL2yFIFO(0);
	this->SetDPIInternalL2nFIFO(0);
	this->SetDPIInternalId3(0);
	this->SetDPIInternalIdleCnt(0);
	this->SetDPIInternalL2RdPtr(0);
	this->SetDPIInternalStartRow(0);
	this->SetDPIInternalClear(0);

    //scan_pilot_sm2003
	this->SetDPIInternalState(0);
	this->SetDPIInternalWaitBeforeRow(0);
	this->SetDPIInternalRowAdd(0);
	this->SetDPIInternalEventNumb(0);
	this->SetDPIInternalRemainingChips(1);
	this->SetDPIInternalFirstEv(0);
}



DigitalPilot::~DigitalPilot(void){

}

void DigitalPilot::SetDPIConf(UInt32 * Vector){
	DPIConf[0] = Vector[0];
	DPIConf[1] = Vector[1];
}

UInt32 * DigitalPilot::GetDPIConfElements(){
	UInt32 * DPIElements = new UInt32[15];
	DPIElements[0] = this->GetDPIConfWaitBefRow();
	DPIElements[1] = this->GetDPIConfSebMeb();
	DPIElements[2] = this->GetDPIConfMaskChip();
	DPIElements[3] = this->GetDPIConfEventNumb();
	DPIElements[4] = this->GetDPIConfStrobeLength();
	DPIElements[5] = this->GetDPIConfHoldRow();
	DPIElements[6] = this->GetDPIConfSkipMode();
	DPIElements[7] = this->GetDPIConfTDO8TDO9();
	DPIElements[8] = this->GetDPIConfEnableCESeq();
	DPIElements[9] = this->GetDPIConfDataFormat();
	DPIElements[10] = this->GetDPIConfMebValIn_RO(); 
	DPIElements[11] = this->GetDPIConfL2nFifoIn_RO(); 
	DPIElements[12] = this->GetDPIConfL2yFifoIn_RO(); 
	DPIElements[13] = this->GetDPIConfL2WrPtr_RO(); 
	DPIElements[14] = this->GetDPIConfL2RdPtr_RO();
	//for(int i = 0 ; i < 15; i ++) cout << " Position " << i << " Value " << (int)DPIElements[i] << endl; 
	return DPIElements;
}

void DigitalPilot::SetDPIConfElements(UInt32 * VectorInput){
	this->SetDPIConfWaitBefRow((UInt8)VectorInput[0]);
	this->SetDPIConfSebMeb((UInt8)VectorInput[1]);
	this->SetDPIConfMaskChip((UInt16)VectorInput[2]);
	this->SetDPIConfEventNumb((UInt16)VectorInput[3]);
	this->SetDPIConfStrobeLength((UInt16)VectorInput[4]);
	this->SetDPIConfHoldRow(VectorInput[5]?true:false);
	this->SetDPIConfSkipMode((UInt8)VectorInput[6]);
	this->SetDPIConfTDO8TDO9(VectorInput[7]?true:false);
	this->SetDPIConfEnableCESeq(VectorInput[8]?true:false);
	this->SetDPIConfDataFormat(VectorInput[9]?true:false);
}


void DigitalPilot::SetDPIInternal(UInt32 * Vector){
	DPIInternal[0] = Vector[0];
	DPIInternal[1] = Vector[1];
	DPIInternal[2] = Vector[2];
}

UInt32 * DigitalPilot::GetDPIInternalElements(){
	UInt32 * DPIElements = new UInt32[20];
	//scan_strobe_busy
    DPIElements[0] = this->GetDPIInternalStrobe_i();
	DPIElements[1] = this->GetDPIInternalStrobeCycCnt();
	DPIElements[2] = this->GetDPIInternalMebVal();
	DPIElements[3] = this->GetDPIInternalBusy();
	DPIElements[4] = this->GetDPIInternalBusyViolation();
	DPIElements[5] = this->GetDPIInternalIdleViolation();

	//scan_queue
	DPIElements[6] = this->GetDPIInternalL2WrPtr();
	DPIElements[7] = this->GetDPIInternalL2yFIFO();
	DPIElements[8] = this->GetDPIInternalL2nFIFO();
	DPIElements[9] = this->GetDPIInternalId3();
	DPIElements[10] = this->GetDPIInternalIdleCnt(); 
	DPIElements[11] = this->GetDPIInternalL2RdPtr(); 
	DPIElements[12] = this->GetDPIInternalStartRow(); 
	DPIElements[13] = this->GetDPIInternalClear(); 
	
	//scan_pilot_sm2003
	DPIElements[14] = this->GetDPIInternalState();
    DPIElements[15] = this->GetDPIInternalWaitBeforeRow();
	DPIElements[16] = this->GetDPIInternalRowAdd();
	DPIElements[17] = this->GetDPIInternalEventNumb(); 
	DPIElements[18] = this->GetDPIInternalRemainingChips(); 
	DPIElements[19] = this->GetDPIInternalFirstEv(); 
 
	//for(int i = 0 ; i < 20; i ++) cout << " Position " << i << " Value " << (int)DPIElements[i] << endl; 
	return DPIElements;
}

void DigitalPilot::SetDPIInternalElements(UInt32 * VectorInput){


	//scan_strobe_busy
	this->SetDPIInternalStrobe_i(VectorInput[0]?true:false);
	this->SetDPIInternalStrobeCycCnt((UInt8)VectorInput[1]);
	this->SetDPIInternalMebVal((UInt8)VectorInput[2]);
	this->SetDPIInternalBusy(VectorInput[3]?true:false);
	this->SetDPIInternalBusyViolation(VectorInput[4]?true:false);
	this->SetDPIInternalIdleViolation(VectorInput[5]?true:false);

	//scan_queue
	this->SetDPIInternalL2WrPtr((UInt8)VectorInput[6]);
	this->SetDPIInternalL2yFIFO((UInt8)VectorInput[7]);
	this->SetDPIInternalL2nFIFO((UInt8)VectorInput[8]);
	this->SetDPIInternalId3(VectorInput[9]?true:false);
	this->SetDPIInternalIdleCnt((UInt8)VectorInput[10]);
	this->SetDPIInternalL2RdPtr((UInt8)VectorInput[11]);
	this->SetDPIInternalStartRow(VectorInput[12]?true:false);
	this->SetDPIInternalClear(VectorInput[13]?true:false);

    //scan_pilot_sm2003
	this->SetDPIInternalState((UInt8)VectorInput[14]);
	this->SetDPIInternalWaitBeforeRow((UInt8)VectorInput[15]);
	this->SetDPIInternalRowAdd((UInt8)VectorInput[16]);
	this->SetDPIInternalEventNumb((UInt16)VectorInput[17]);
	this->SetDPIInternalRemainingChips((UInt16)VectorInput[18]);
	this->SetDPIInternalFirstEv(VectorInput[19]?true:false);

	
}



