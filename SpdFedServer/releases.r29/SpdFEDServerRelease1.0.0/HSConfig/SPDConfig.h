#pragma once
#include <dis.hxx>

#ifndef SPDConfig_h
#define SPDConfig_h

#include "stdafx.h"
#include "..\VMEAcc\VMEAccess.h"
#include ".\hsconfiguration.h"
#include ".\hsjtagstreams.h"
#include ".\HSConfData.h"
//#include ".\dbcfg.h"
#include "..\ALICE_SPD_DIMServer\DataBuffer.h"

#include "SpdHalfStave.h"
#include "SpdRouter.h"

#include "../spdDbConfLib/timer.h"
#include "spdFastorCalibration.h"

#include "spdDacScan.h"
#include "spdMeanTHScan.h"
#include "SpdDelayScan.h"
#include "SpdUniMatrixScan.h"
#include "SpdMinThresScan.h"


#pragma warning( once : 4006 )
//Operation to JTAG scan required
#define DAC_SET 0
#define DAC_SET_SENSE 1
#define SET_TP 2
#define SET_MASK 3

//const int numberOfDacs = 44;
//const int NHalfStaves = 60;
const unsigned int HSInRouter = 6;
const unsigned int NRouters = 10;

//! Class to perform global operations in the detectors and calibration scans 
/*! this class contains instances of halfstaves routers and classes
to perform calibration scans and global operations in the detector 
 at the moment I've made all these calibration scans methods friends of this class
most probably a huge crime in encapsulation  but, for historical reasons, 
this was the easiest way of doing it */
class SPDConfig{
protected:
	char side;

private:

	spdLogger *log;

				//! this will contain the HalfStave classes, 60 per side
    SpdHalfStave * halfStaves[NHalfStaves];
		//! contains the routers per side 
	SpdRouter * routers[NRouters];
		//! instance to perform mean threshold scans
	spdMeanTHScan *meanThScan;

		//! object instance in the class to perform dac scans
	SpdDacScan *dacScan;
		//! object instance in the class to perform delay scans
	SpdDelayScan *delayScan;
		//! object instance in the class to perform uniformity matrix scans
	SpdUniMatrixScan *uniMatrixScan;
		//! object instance in the class to perform minimun threshold scans
	SpdMinThresScan *minThreshScan;
		//! object instance of the class to perform fastor calibration scans
	spdFastorCalibration *foCalibration;


  public:	

	SPDConfig(char side, VMEAccess * vme);
	~SPDConfig(void);
    

	SpdDelayScan &getDelayScan(){ return * this->delayScan;};
	SpdDacScan &getDacScan(){ return * this->dacScan;};
	spdFastorCalibration &getFoScan(){return * this->foCalibration;};
	SpdUniMatrixScan &getUniMatrixScan(){return * this->uniMatrixScan;};
	SpdMinThresScan &getMinThresScan(){return * this->minThreshScan;};
	spdMeanTHScan &getMeanThScan(){return * this->meanThScan;};


		//! method that returns if any of the scans is active
	bool isScanActive();


		//! Instance to manage database configuration of the halfstaves
    HSConfData HSData;

		//! methos to return a reference of one halfstave 
		/*there is bound protections inside, even though not done in the best way*/
	SpdHalfStave &getHS( unsigned chNumber);
		//! methos to return a reference of one router 
		/*there is bound protections inside, even though not done in the best way*/
	SpdRouter &getRouter(unsigned routerNumber);


	//!getter for the side 
	char getSide(){return side;};

	//! starts a noisy scan
	unsigned StartNoiseScan(UInt8 ChNumber, UInt32 triggern, bool InternTrig);

	//! sets the MCM stimuly for one link receiver channel only
	/*! \param ChN chanel number (halfstave) to set
		\param Enable enable or disable flag for the mcm stimuly
		\param ColumnToSet defines the column to set in the mcm stimuly*/
	void setMCMStimuli(UInt8 ChN, UInt32 Enable, UInt32 ColumnToSet = 1); 

	//! sets the MCM stimuly to all enable channels  
	/*! \param Enable enable or disable flag for the mcm stimuly
		\param ColumnToSet defines the column to set in the mcm stimuly*/
	void setMCMStimuliToAll(UInt32 Enable, UInt32 ColumnToSet = 1); 

	
	//void SetScansHeader(UInt8 DataType, UInt8 ScanType, unsigned RoutN, UInt8 DACIdentifier,UInt8 ActualRow ,UInt8 StartRow ,UInt8 EndRow);
    //!Method to write the scan header in the router for the noise scan only
	void SetScansHeader(UInt8 routerNumber, UInt8 scanType,UInt32 triggern );


	//! checks "loops" over the scans calling their scan method
	void scans();
	

	//! unmasks all pixel chips
	unsigned int UnMaskAllPixels(void);

	//!refreshes the temperatures in all enabled halfstaves
	unsigned int refreshAllTemperatures();
	

	//! Method to set the internal digital pilot with the default values in all enabled halfstaves
	unsigned int resetInternalDPI_All(void);

	//! performs a data reset in all half-staves
	unsigned int DataReset_All(void);


		/*! function to write the differences to the database temporary table
			will loop through all halfstaves and send a command to compare the default and actual values 
			writing them in the database in a temporary table
		*/
	int writeConfigDiffTable();

		/*! function to get the number of differences between the default and the actual data
			will loop through all internal containers comparing the defaul and actual values 
			and counting the number of differente settings in mcm values, dac values and noisy pixel values
			@param mcmDiff: argument by reference it will be incremented inside the funcion to contain the number of mcm differences
			@param dacDiff: argument by reference it will be incremented inside the funcion to contain the number of pixel dac differences
			@param noisyDiff: argument by reference it will be incremented inside the funcion to contain the number of noisy pixels differences
		*/
	void countConfigDiff(unsigned &mcmDiff, unsigned &dacDiff, unsigned &noisyDiff);

	/*! Method to send triggers to all enabled routers 
		you can define the number of triggers to send and the rest of the varaiables enable the type of triggers to send
		\param triggerN number of triggers to send
		\param delay have to ask what this means to michele 
		\param l2y enables the level 2 yes trigger
		\param l1 enables the level 1 trigger
		\param tp  enables the test pulse trigger
	*/
	int sendTriggerToEnabledRouters(UInt32 triggerN, bool delay = true, bool l2y= true, bool l1=true, bool tp=true);



	/*@Configures the all halfstaves with the default values 
	@param the inputs define which configurations will be performed
	writes in the report the status of the 60 hs*/
	void configureSPDDefault(UInt32 * report,bool routerReset=true, bool digitalPilot=true, bool analogPilot=true, bool pixdacs=true);

	/*!returns if a router as at least one acitve channel or not
	CAUTION it uses the local router number, so if side is C the router  is still from 0-9*/
	bool isRouterActive(unsigned routerNumber);

	
	// at the moment I've made all these calibration scans methods friends of this class
	// most probably a huge crime in encapsulation 
	//but for historical reasons this was the easiest way of doing it
	friend class spdFastorCalibration;
	friend class spdMeanTHScan;
	friend class SpdDacScan;
	friend class SpdDelayScan;
	friend class SpdUniMatrixScan;
	friend class SpdMinThresScan;
};


#endif