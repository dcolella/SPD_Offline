#pragma once

#ifndef hsconfiguration_h
#define hsconfiguration_h


#include ".\hsconfiguration.h"

const UInt32 DPI_CONF_RW = 0x09;
const UInt32 DPI_CONF_R  = 0x0a;
const UInt32 DPI_INT_RW = 0x0b;
const UInt32 API_CONF_RW = 0x0b;
const UInt32 API_CONF_R_DAC = 0x0c;
const UInt32 API_CONF_R_ADC = 0x0a;
const UInt32 API_START_CONV = 0x09;
const UInt32 GOL_CONF_RW = 0x09;
const UInt32 GOL_CONF_R  = 0x0a;

const UInt32 IDCODE = 0x1;
const UInt32 BYPASS = 0x1f;
const UInt32 BYPASSPix = 0xf;
const UInt32 SelENBL = 0x1;
const UInt32 SelGLOBAL = 0x4;
const UInt32 SelTM = 0x3;
const UInt32 RstPixel = 0xd;



#define DAC_SET 0
#define DAC_SET_SENSE 1
#define SET_TP 2
#define SET_MASK 3

//IRIstrucVect[i][j]: [i] select the environment and the operation to do:
     // 0 is used for default load HS default. first step where you select the global register.
     // 1 is used for write in the proper selected register. second step for load register. 
     // 2 for mask or set TP
     // 3 ID code
     // 4 Bypass
	 // 5 user define vector
	 // 6 read vector

#define IRSelEnable 0
#define IRSelGlobal 1
#define	IRSelTM 2
#define	IRIdCode 3
#define	IRbypass 4
#define	IRfree 5
#define	IRRead 6
#define IRStartConv 7
#define JTAG_RESET 8


const int NInstructions = 9;

//these codes are used by the decoder to address the right element
#define	GOL 0
#define API_DAC 1
#define API_ADC 2
#define	PIX_DAC 3
#define	PIX_COLUMN 4
#define	DPIConf 5
#define	DPIInternal 6
#define	ID_CODE 7


 
class HSJTAGStreams{
	
	UInt32 IRIstrucVect[NInstructions][13];

    
	//UInt8 ChNumber;
	UInt32 bitNumber;
	UInt32 ModeSel;
	UInt32 * ScanVector;
	UInt8 JTOperation;            //0 = DAC operation
								  //1 = DAC with sense on
								  //2 = set TP
								  //3 = setMask
	UInt8 SkipMode;

    UInt32 GOLConfTemp;
	UInt8  * APIConfTemp;
	UInt32 * DPIConfTemp;

	UInt32 GetInstruction(UInt8 position);
	UInt32 AppendData(UInt32 * Stream, UInt32 bitNum, UInt32 * Data);
   UInt32 * ExtractData(UInt32 * Stream, UInt32 StartPosition, UInt32 Type);
    
public:
	HSJTAGStreams(void);
	~HSJTAGStreams(void);
    

    

	UInt32 GetModeSel(void){return ModeSel;};
	void SetModeSel(UInt32 Mode){ModeSel = Mode;};

	UInt8 GetJTOperation(void){return JTOperation;};
	void SetJTOperation(UInt8 OperTemp){JTOperation = OperTemp;};

	UInt8 GetSkipMode(void){return SkipMode;};
	void SetSkipMode(UInt8 Mode){SkipMode = Mode;};

	void SetScanVector(UInt8 SelVector){ScanVector = IRIstrucVect[SelVector];};
    
	UInt32 * IRGenerator(void);
	UInt32 * DRGenerator(UInt32 *DataIn, bool ConstValue);


	UInt32** DecodeJTStream(UInt32 * DRVect);

		//! function to generate the jtag vectors to set one pixel dac
		/*!
			@return 2 dimension array with jtag ir, dr,ir,dr vectors to set the dacs
			@param ChN - channel number
			@param DACn - dac number to chane
			@param DACValues - array with the different values for every chip in chain
			@param ChipSelect- 10 bit number saying which chips we want to change in this scan 
				(00000001) means we will put all chips except number 0 in bypass
			@param All - specifies if we are setting all chips with the same value (DACValues[0])
			@param pixInChain - parameter defining the chips in chain (if any chip is being skiped in the chain)
		*/
	UInt32** SetJTSPixelDAC(  UInt8 * DACn, UInt8 * DACValues, UInt32 ChipSelect, bool All, UInt32 pixInChain = 1023);

		//!Function to set all pixel 44*10 dacs in one halfstave
		/*! 
			@return 2 dimension array with 90 arrays 0,1 ir vectors for IRSelEnable and IRSelGlobal
			@param ChN - channel number
			@param ChipSelect- 10 bit number saying which chips we want to change in this scan 
				(00000001) means we will put all chips except number 0 in bypass
			@param DACVect - array with the different values  44*10 dac values
			@param pixInChain - parameter defining the chips in chain (if any chip is being skiped in the chain)

		*/
	UInt32** SetJTSPixelAllDAC(UInt32 ChipSelect, UInt8 * DACVect,UInt32 pixInChain=1023);

		
	UInt32** SetJTSPixelMatrix( UInt32 ChipSelect, UInt32 * Matrix, bool All, bool TP, UInt32 pixInChain = 1023);
    	
	UInt32** SetJTSApiDAC( UInt8 * ApiDac, UInt32 pixInChain=1023);
	UInt32** SetJTSApiADCStartConv( UInt32 pixInChain=1023);
	UInt32** SetJTSDpiConfReg(UInt32 * DpiReg, UInt32 pixInChain=1023);
	UInt32** SetJTSDpiInternalReg( UInt32 * DpiReg, UInt32 pixInChain=1023);

	UInt32** GetJTSApiDAC( UInt32 pixInChain=1023);
	UInt32** GetJTSApiADC( UInt32 pixInChain = 1023);
	UInt32** GetJTSDpiConfReg( UInt32 pixInChain=1023);
    UInt32** GetJTSDpiInternalReg( UInt32 pixInChain=1023);

	UInt32* GetJTSResetPixel( UInt32 ChipSelect, UInt32 pixInChain=1023);

};

#endif