#pragma once

#include "../addressgenerator/routintaddr.h"
#include "..\VMEAcc\VMEAccess.h"
#include "../spdDbConfLib/spdLogger.h"
#include <dis.hxx>

/*! Constant defining the maximum number of errors that will be read by the fed server 
	for every router, like this we can avoid locking up the FED server*/
const unsigned maxNumberOfRouterErrors = 100;

class SpdRouter
{

		/*!Dim service displaying if there are router errors in the memory or not*/
	DimService *serviceRouterError;

		/*!Internal member to keep the status of the router error, connected with serviceRouterError*/
	int routerError;

		/*!Logging instance for the spd router */
	spdLogger *log;

	//! intenal member storing the router number
	unsigned int routerNumber;

	//! internal member class to store the router addresses
	RoutIntAddr routerAddresses;
	
	//! vme access class, will act as the lower level driver
	VMEAccess * vmeDriver;

public:

	struct RouterError{
		unsigned bunchCrossing;
		unsigned errorOrder;
		unsigned errorClass;
		unsigned errorID;
		UInt32 details1;
		UInt32 details2;

	};


	
	/*! Performs a router reset and the reset of all lrx in this router*/
	void reset(void);

	/*!writes the LoL1 time i nthe router */
	void writeL0L1Time(UInt32 value);

	/*!Sets the memc stimuly, will stop router state machine and write in the correct link receiver the settings
	@param channelInRouter : chanel in this router to enable the mcm stimuly (0-5)
	@param enable: 1 -> enable mcm stimly, 0 -> disable it
	@param column to set (parameter to set in the mcm stimuly register see documentation of the router/lrx to be sure)*/
	ViStatus setMcmStimuly(UInt8 channelInRouter, UInt32 enable,  UInt32 ColumnToSet = 1);


	/*!constructor of the class
		@param routNumber: number of the router 
		@param vmeDriver: VMEAcess class to get the vme access
	*/
	SpdRouter( unsigned routNumber, VMEAccess * vmeDriver);

	~SpdRouter(void);
		/*! method to send triggers to one router, by default all trigger types are true
		   @param triggerN: number of triggers to send
		   @param delay: delay option
		   @param l2y: level 2 yes trigger option
		   @param l1: level 1 trigger
		   @param tp: test pulse trigger */
	ViStatus SendTrigger(UInt32 triggerN, bool delay = true, bool l2y= true, bool l1=true, bool tp=true);

	/*! Method to write a raw data in the calibration header in one router 
		@param data: array of 32 bit words to write
		@param size: number of words to write
	*/
	ViStatus WriteHeader( UInt32 data[], UInt32 size);

	
	/*! method to set the busy flag
		reads the control register of the router and writes 
		the same value just setting the flag
	*/
	ViStatus setBusyFlag(bool busy);

	/*! method to return if a router is idle or not
		reads the control register 2 and makes an and with 0x4
	*/
	bool getIdleFlag();

	/*!Stets the stop state machine bit in the control register*/
	ViStatus setStopSM(bool stopSM);

	/*! method to get if a router has the stop machine stopped or not
		reads the control register 2 and makes an and with 0x400
	*/
	bool getStopSM();
	//! getter for the router number
	unsigned getRouterNumber(){return routerNumber;};

	ViStatus dataResetHS();


	/*!Method to read the error list from the router memory
		It will read the router memory and parse each error (4 32 bit registers)
		word0 = Errors_counting [23:12], bc_id  [11:0]
		word1 = Errors Class [31:10] , 	Error Name [9:0]
		word2 = Detail 1[31:0]
		word3 = Detail 2[31:0]
	*/
	vector<RouterError> readErrorList();

	/*!Method to read the error mask register of one router
		this is address 0xf0, the format of this register bits its the following:
			0	Enable / Disable Error Handling
			1	Mask ' TTCRX and QPLL link error
			2	Mask ' Trigger Errors from TSM
			3	Mask ' Timeout BC Reset error
			4	Mask ' Trigger error from Router FSM 
			5	Mask ' Fatal error DAQ from Router DAQ FSM
			6	Mask ' Error Optical link (RxReady & RxError)
			7	Mask ' Rx Error (HS optical link)
			8	Mask ' Error Format (HS error format optical communication) 
			9	Mask ' Error Data Transfer (HS error optical data transfer not coherent)
			10	Mask ' Error Control Int (command not properly recognized from the MCM)
			11	Mask ' Error Event Number (Error in MCM Event Number)
			12	Mask ' HS_0 Global Error (Idle, Busy violation, link RX fatal errors, etc)
			13	Mask ' HS_1 Global Errors (     //    )
			14	Mask ' HS_2 Global Errors (     //    )
			15	Mask ' HS_3 Global Errors (     //    )
			16	Mask ' HS_4 Global Errors (     //    )
			17	Mask ' HS_5 Global Errors (     //    )
			18	Mask ' HSs Timeout during data acquisitions 
			19	Mask ' Error Data Format ( from Router Data Format Check)
			20	Mask ' Error FastOR in Data Stream ( Fast OR not coherent in the Data Stream)
			21 .. 32	XXXX don't care
		@return the value of the mask register
	*/
	UInt32 readErrorMask();

	/*!Writes the a maks value in the error mask register*/
	void writeErrorMask(UInt32 mask);

	/*! will send a router reset only to one halfstave
		it writes in the router control register masking all other channels,
		sends the router reset and then rewrites the original router control 
	*/
	void resetHalfStave(unsigned halfstave);

	/*! writes the value in the control register of the router 
		control reg information:
		bit 0 :		DPM Sample mode
		bit 1 :		no data to DAQ
		bits 2-7 :	HS 0-5 mask (0 -> hs ennabled, 1 -> hs masked)
		bit 8:		Busy flag
		bit 9 :		enable TP in L0
		bit 10:		stop all state machine
		bit 19 :	enable router header
		bit 20:		enable orbit counter
		bit 21:		exclude ttc		

	*/
	void writeControlReg(UInt32 regValue);

	/*!reads the contends of the control register in ther router*/
	UInt32 readControlReg();

	/*! Will write the router halfstave mask i nthw control reg*/
	void writeHalfStaveMask(unsigned mask);

	/*!Will all enabled hs in the router */
	void resetDetector();
	

	/*!Function to write the fastor L1 delay in one link receiver
	@param lrx: link receiver number 0-2
	@param fastorDelay - the L1 fastor delay */
	void writeLrxFastorL1Delay(unsigned lrx, UInt32 fastorDelay);

	/*!Function to configure a router 
	 It peforms a router and lrx resets 
	 write the control register, sets L0L1 time 
	 and the fastorL1 time for all the lrx 
	 @param options : bit0 enable router reset, bit1 configure router, bit2 L0L1 time, bit3 lrx fastor L1 time
	 @param ctlrReg: router control reg
	 @param L0L1 time
	 @param lrxFoDelays fastor L1 delay for al link receivers (0-2)
	 */
	void configureRouter(UInt32 options,
							UInt32 ctlrReg, 
							UInt32 L0L1, 
							UInt32 lrxFoDelays[]);

	/*!Function read the router busy times
	 it receives as input an array that is filled inside
	 @param values array to be filled inside: 
		0 = busy daq, 
		1= busy router, 
		2 = busy HS, 
		3 = busy trigger 
	 @return error condition
	 */
	UInt32 readRouterBusyTimes(UInt32 values[]);

};
