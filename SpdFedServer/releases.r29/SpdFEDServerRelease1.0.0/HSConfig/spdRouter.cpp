#include "StdAfx.h"
#include ".\spdrouter.h"

#include "..\spdDbConfLib\spdGlobals.h"

using spdGlobals::extractFromInt32;

SpdRouter::SpdRouter( unsigned routNumber, VMEAccess * vme)
{

	log = &(spdLogger::getInstance());


	this->vmeDriver = vme;
	this->routerNumber = routNumber;
	
		// this will do the shift of the data 
	this->routerAddresses.ShiftAddresses(16);

		// this will initialize all internal addresses 
	this->routerAddresses.SetRoutBoardAddr( routNumber +1);

	// will initialize the router error services
	this->routerError = 0;
	stringstream serviceName;

	if (this->routerNumber < 10){
		serviceName << "spd_feDimServerA/ROUTER_ERROR"<< routerNumber;
	}
	else{
		serviceName << "spd_feDimServerC/ROUTER_ERROR"<< routerNumber;
	}

	this->serviceRouterError = new DimService(serviceName.str().c_str(),routerError);

}

SpdRouter::~SpdRouter(void)
{
	delete this->serviceRouterError;
}


ViStatus SpdRouter::setMcmStimuly(UInt8 channelInRouter, UInt32 enable,  UInt32 ColumnToSet){
	bool isRouterStopped = this->getStopSM();

	// if the router is not stopped already we will stop the state machine (required to set or read lrx registers)
	if(isRouterStopped == false){
		this->setStopSM(true);
	}

	
	UInt8 channel = 6*this->routerNumber + channelInRouter;
	
	
	UInt32 addr = vmeDriver->AG->getLRxAddr(channel).GetEnableMCMStimuliDataAddr();


	ViStatus status = 0;


	log->log("setting mcm stimuly enable: channel %d  address = 0x%x, value = 0x%x",channel, addr, enable );
	status = vmeDriver->VMERegisterAcc( addr, &enable, 1, 'W');   //enable MCM stimuly data out
	

	// now will write the column in the register
	addr = vmeDriver->AG->getLRxAddr(channel).GetMCMStimuliDataAddr();

	// this is particualr weird why this register value has something to do with the 
	// router number
	//UInt32 column = 0xffffffff - ((2^ColumnToSet - 1) << this->routerNumber);
	
	// will insert a zero in the correct column to set 
	UInt32 column = 0xffffffff;
	insertIntoInt32(  column, 0,this->routerNumber%10, ColumnToSet);

	//log->log("setting mcm stimuly column: channel %d  address = 0x%x, value = 0x%x",channel, addr, column );
	
	status +=vmeDriver->VMERegisterAcc( addr, &column, 1, 'W');   //set the column to send out

	addr = vmeDriver->AG->getLRxAddr(channel).GetTestRegAddr();
	//log->log("column:  channel %d  address = 0x%x, value = 0x%x",channel, addr, column );
	
	status +=vmeDriver->VMERegisterAcc( addr, &enable, 1, 'W');   //start MCM Stimuly
	
			
	if(isRouterStopped == false){
		this->setStopSM(false);
	}
	
	return status;
	
}


// sends triggers to the router
ViStatus SpdRouter::SendTrigger( UInt32 TriggerN, bool Delay, bool l2y, bool l1, bool tp){ //Trigger Number = 0..26 
	
	UInt32 Addr = this->routerAddresses.GetSendTriggSeqAddr();

	UInt32 Data = 0;
	//UInt32 Data = (  (  ((UInt32)(!tp) << 4) + ( (UInt32)(!l1) << 3) +  
	//				   ((UInt32)(!l2y) << 1) + (UInt32)(!Delay)  )       << 27) + TriggerN; 


	insertIntoInt32( Data,!Delay,27,1);

	// for the moment the l2n is always disactivated (the logic is inverted)
	insertIntoInt32( Data,1,28,1);

	insertIntoInt32( Data,!l2y,29,1);
	insertIntoInt32( Data,!l1,30,1);
	insertIntoInt32( Data,!tp,31,1);
	
	Data +=TriggerN;

	//log->log("writting data word  addr %x, data %x ",Addr, Data);

	return vmeDriver->VMEWriteRegister( Addr, Data);

}
// writes the calibration header in the router
ViStatus SpdRouter::WriteHeader( UInt32 header[], UInt32 headerLenght){
	
	UInt32 Addr = routerAddresses.GetResetExtraHeaderFifoAddr();
	vmeDriver->VMEWriteRegister( Addr, header[0] ); 

	Addr = routerAddresses.GetExtraHeaderFifoAddr();
	return vmeDriver->VMERegisterAcc( Addr, header , headerLenght, 'W'); 
}

//writes the busy flag
ViStatus SpdRouter::setBusyFlag( bool busy){
	
	UInt32 Addr = routerAddresses.GetCntrRegAddr();   
	UInt32 cntrlRegister;

	vmeDriver->VMEReadRegister( Addr, &cntrlRegister);  

			// inserts the new busy bit in the old control register
	spdGlobals::insertIntoInt32( cntrlRegister, busy, 8, 1);

    return vmeDriver->VMEWriteRegister( Addr, cntrlRegister); 
}


bool SpdRouter::getIdleFlag(){
	UInt32 RoutAddr = routerAddresses.GetStatusReg2Addr(); 
	UInt32 RouterStatus ;
	
	vmeDriver->VMEReadRegister(RoutAddr, &RouterStatus);

	return (RouterStatus & 0x4)? true:false;

}
// stops the state machine of the router 
bool SpdRouter::getStopSM(){
	UInt32 RouterCtlOrig;
	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEReadRegister(RoutAddr, &RouterCtlOrig);

			// will return true if the bit is not zero
	bool stopSM = ((RouterCtlOrig & 0x400) != 0) ;
	return stopSM;
}


// resets all enabled halfstaves in one router
ViStatus SpdRouter::dataResetHS(){
	UInt32 resetAddress = routerAddresses.GetResetPixelAddr();

	// here I am writting 2 times because of an unknown problem while 
	//performing this data reset
	ViStatus status = vmeDriver->VMEWriteRegister(resetAddress, 0);
	status += vmeDriver->VMEWriteRegister(resetAddress, 1);

	log->log("INFO: Performed data reset router %d, adress %x",this->routerNumber, resetAddress );

	return status;
}


// sets the stop state machine in the router control register (bit 10)
ViStatus SpdRouter::setStopSM( bool stopSM){

	UInt32 RouterCtlOrig;
	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEReadRegister(RoutAddr, &RouterCtlOrig);

	UInt32 RouterCtl;

	if (stopSM){
		RouterCtl = RouterCtlOrig | 0x400;
	}
	else {
		RouterCtl = RouterCtlOrig & 0xfffffffffffffbff;// 0x400;
	}

	return vmeDriver-> VMEWriteRegister( RoutAddr, RouterCtl);   //setting stopFSM mode
	
}


// will read all errors from a router and return them in a nice vector
vector<SpdRouter::RouterError>  SpdRouter::readErrorList(){

	// starts reading from the spm base address
	UInt32 spmPointer = routerAddresses.GetSPMBaseAddr();
	UInt32 spmRegister;
	

	vector<RouterError> out;


	unsigned const numberOferrorRegisters = 4;

	while (1){
		UInt32 errorRegister[numberOferrorRegisters];

		// the reading two times is because of a problem in the router 
		// michelle is trying to fix it 
		vmeDriver->VMEReadRegister(spmPointer, &spmRegister);
		vmeDriver->VMEReadRegister(spmPointer, &spmRegister);

	
		spmPointer +=4;

		unsigned byteCheck = extractFromInt32(spmRegister,24,8 );

		if (byteCheck != 0xaa){
			//log->logToScr("ERROR: no valid error found in the router %d, register = %x  ", routerNumber,spmRegister );
			break;
		}
		if ( (spmRegister & 0x000fffff)== 0x00055555){
			break;
		}
		if (spmRegister == 0xFFFFFFFF){

			//log->logToScr("ERROR: format error in the SPM register (0x%x), is router %d plugged? ",spmRegister, routerNumber );
			
			break;
		}

		errorRegister[0]= spmRegister;
		
		
		for (unsigned index =1 ; index < numberOferrorRegisters; index ++){

			vmeDriver->VMEReadRegister(spmPointer, &spmRegister);
			vmeDriver->VMEReadRegister(spmPointer, &spmRegister);

			spmPointer +=4;
			
			// if the 0x55555555 marks the end of data in the router error
			errorRegister[index]= spmRegister;

		}

		// now it will parse the error
		RouterError error;

		// it will 
		error.bunchCrossing = extractFromInt32( errorRegister[0], 0 , 12);
		error.errorOrder = extractFromInt32(errorRegister[0], 12, 12);
		error.errorID =  extractFromInt32(errorRegister[1], 0, 10);
		error.errorClass = extractFromInt32(errorRegister[1], 10, 22);
		error.details1 = errorRegister[2];
		error.details2 = errorRegister[3];

		// inserts the error in the list
		out.push_back(error);

		// if we reach the limit of the number of errors we will stop here
		// TODO: clean the router memory if maximum number of errors is reached
		// TODO: add an errors class and error type in the router database so to add this error directly in the router error table
		if (out.size() >= maxNumberOfRouterErrors){
			log->log("ERROR: reached nmaximum number of errors in router %d, the remaining errors will be ignored", this->routerNumber);

			// this is the maximum value limited in the verilog code for router errors 
			const UInt32 maxAddress =  routerAddresses.GetSPMBaseAddr()+ 0x3fffff;
			vmeDriver->VMEReadRegister( maxAddress, &spmRegister);
			
			break;
		}

	}

	if (out.size() > 0){
		
		routerError =(int) out.size();
		log->log("ERROR: router %d with %d errors during last pooling", this->routerNumber, routerError);
		// if there are errors it will send the DIM service
		this->serviceRouterError->updateService(routerError);
	}

	// returns the list of errors
	return out;
}


// reads the error mask register
UInt32 SpdRouter::readErrorMask(){
	UInt32 errorMaskAddress = routerAddresses.GetErrorMask();
	UInt32 maskRegister;

	vmeDriver->VMEReadRegister(errorMaskAddress, &maskRegister);

	return maskRegister;
}


UInt32 SpdRouter::readRouterBusyTimes(UInt32 values[]){

	UInt32 address, routerRegister;

	address = routerAddresses.GetTimeBusyDaqAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[0]=routerRegister;

	address = routerAddresses.GetTimeBusyRouterAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[1]=routerRegister;

	address = routerAddresses.GetTimeBusyHsAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[2]=routerRegister;

	address = routerAddresses.GetTimeBusyTriggersL1FifoAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[3]=routerRegister;

	//address = routerAddresses.GetL0CounterAddr();
	//vmeDriver->VMEReadRegister(address, &routerRegister);

	//values[4]=routerRegister;

	return 0;
}


// will write the error mask register
void SpdRouter::writeErrorMask(UInt32 mask){
	UInt32 errorMaskAddress = routerAddresses.GetErrorMask();
	vmeDriver->VMEWriteRegister(errorMaskAddress, mask);

}

// will read the control register of the router
UInt32 SpdRouter::readControlReg(){
	UInt32 controlRegister;
	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEReadRegister(RoutAddr, &controlRegister);

	return controlRegister;
}




//writes the control register of one router
void SpdRouter::writeControlReg(UInt32 regValue){

	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEWriteRegister( RoutAddr, regValue);

}


// performs a router reset and 3 link receivers resets
void SpdRouter::reset(){

	
	// first we stop the error handling 
	UInt32 originalMask = this->readErrorMask();
	//log->logToScr("stopping error mask original value = %x", originalMask);
	this->writeErrorMask(0);
	

	// performes a rotuer reset and a flush dpm command
	UInt32 RoutAddr = routerAddresses.GetRtVMEResetAddr();
	vmeDriver->VMEWriteRegister( RoutAddr, 0x1);

	RoutAddr = routerAddresses.GetFlushDPMAddr();
	vmeDriver->VMEWriteRegister( RoutAddr, 0x1);

	// this performs a link receiver reset
	UInt32 lrxResetAddr =routerAddresses.GetResetLinkRxAddr();
	vmeDriver->VMEWriteRegister(lrxResetAddr, 0x1);


	//log->logToScr("restarting error mask with value = %x", originalMask);
	// rewrites the error handling
	this->writeErrorMask(originalMask);
	
}

// writes the L1 link receiver fastor delay 
void SpdRouter::writeLrxFastorL1Delay(unsigned lrx, UInt32 fastorDelay){

	

	bool isRouterStopped = this->getStopSM();
	if (isRouterStopped == false){
		this->setStopSM( true);
	}
	unsigned chNumb = 6*routerNumber + lrx;
	log->logToScr( "writing lrx fastor delay  channel %d, value %d", chNumb, fastorDelay);
	UInt32 lrxAddr = vmeDriver->AG->getLRxAddr( chNumb).GetLRXFastorL1DelayAddr();

	log->logToScr("Writting lrx fo delay addres %x, value" , lrxAddr, fastorDelay);
	vmeDriver->VMEWriteRegister(lrxAddr, fastorDelay);

	this->setStopSM(isRouterStopped);

}
// writes the L1 time in one router
void SpdRouter::writeL0L1Time(UInt32 value){

	UInt32 RoutAddr = routerAddresses.GetTimeL0L1();
	vmeDriver->VMEWriteRegister( RoutAddr, value);
}

// configures the router 
// performs the reset of both the router and link receivers
// writes the L0-L1 time, writes the L1 fastor delay in the lrx's
void SpdRouter::configureRouter(UInt32 options,
								UInt32 ctlrReg, 
								UInt32 L0L1Time, 
								UInt32 lrxFoDelays[]){

									
		//options : bit0 enable router reset, bit1 configure router, bit2 L0L1 time, bit3 lrx fastor L1 time
	bool routerReset =		(extractFromInt32( options,0,1)== 1) ? true: false;
	bool routerConfigure =	(extractFromInt32( options,1,1)== 1) ? true: false;
	bool setL0L1Time =		(extractFromInt32( options,2,1)== 1) ? true: false;
	bool setL1FastorDelay = (extractFromInt32( options,3,1)== 1) ? true: false;

	if (routerReset){ 
		this->reset();
	}

	if (routerConfigure){ 
		this->writeControlReg(ctlrReg);
	}

	if ( setL0L1Time){
		this->writeL0L1Time(L0L1Time);
	}

	if (setL1FastorDelay){
		unsigned const nHsInRouter = 6;
		UInt32 lrxAddr;

		// we have to stop the state machine to set a register in the lrx
		bool isRouterStopped = this->getStopSM();

		if (isRouterStopped == false){this->setStopSM( true);}

		for (unsigned lrx =0; lrx < nHsInRouter ; lrx ++){
			
			unsigned chNumb = 6*routerNumber + lrx;
			
			lrxAddr = vmeDriver->AG->getLRxAddr( chNumb).GetLRXFastorL1DelayAddr();

			log->logToScr("Writting lrx fo delay addres %x, value" , lrxAddr, lrxFoDelays[lrx]);
			vmeDriver->VMEWriteRegister(lrxAddr, lrxFoDelays[lrx]);

		}
		
		if (isRouterStopped == false){this->setStopSM( false);}

	}

	
}

// resets only one halfstave for the moment its not working, problem in the router
void SpdRouter::resetHalfStave(unsigned halfstave){


	if (halfstave > 5){
		log->log("(SpdRouter::resetHalfStave) halfstave as to be between 0-5 found %d operation ignored", halfstave);
		return;
	}

	UInt32 RouterCtlOrig = this->readControlReg();
	UInt32 mask = 1 << halfstave;

	UInt32 ctlMasked = RouterCtlOrig;

	// inserts the mask in the control register
	insertIntoInt32( ctlMasked, mask,2,6);
	
	this->writeControlReg(ctlMasked);

	// then sends the command to reset the detector with only one channel enabled
	this->resetDetector();

	//rewrites the original mask in the router 
	this->writeControlReg(RouterCtlOrig);

}
// resets all 6 HS in one router at the same time (the reset detector in the labview)
void  SpdRouter::resetDetector(){
	UInt32 addr = routerAddresses.GetResetDetectorAddr();
	// writting anything to this register will start the router reset of the channels 
	vmeDriver->VMEWriteRegister( addr, 0x1);
}

// writes the halfstave mask in the router 
void SpdRouter::writeHalfStaveMask(unsigned mask){

	UInt32 ctrlRegister = this->readControlReg();

	// inserts the mask in the control register
	insertIntoInt32( ctrlRegister, mask,2,6);
	
	// and writes the mask
	this->writeControlReg(ctrlRegister);
	
}