#pragma once
#include "spdscan.h"


#include "stdafx.h"
#include "..\VMEAcc\VMEAccess.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"

class SPDConfig;

const int MinMiscControl = 192;
const int MaxMiscControl = 128;

/*! Class to manage a delay scan
	two dacs will be looped: delay_control and misc_control 
	for each step of delay_control misc_control will have the two possible values 192 and 128
	so the total number of steps of the scan will be 2*(steps of delay_control)
*/
class SpdDelayScan :
	public SpdScan
{
	//! stores if the scan is active or not
	bool active;
	//! stores the parameters of a dac scan
	DACParameters dac;
	//! logger of the class
	spdLogger *log;
	//! stores if we use internal trigger or not
	bool internalTrigger;
	//! stores the number of triggers
    UInt32 triggerN;
	//! keeps the chip selection for this scan (10 bits)
	UInt32 chipSelect;

	//! waiting time between steps
	UInt32 waitTime;
	//! this is a constant and should be 2 for a dac scan
    UInt8 scanType;

	//! parent SPDConfig to handle the hardware
	SPDConfig *parent;

	//! stores the misc_control index
	UInt8 misControlIndex;
	//! stores the delayControlIndex
	UInt8 delayControlIndex;

	void delayStep(UInt8 miscValue);
		// will write the router header for this scan
	void SetScansHeader(UInt8 routerNumber);

		//! timer for the scan
	timer timerScan;

public:

	/*! method to initiate a delay scan
		\param triggerNumber number of triggers to send (if internal trigger)
		\param dacStart delay control value to start
		\param dacEnd delay control value to finish
		\param newStep step of delay control to increment
		\param InternTrig flag enabling internal trigger 
		\param waitTime time to wait beetween delay steps
	*/
	unsigned start( UInt32 triggerNumber, 
				UInt8 dacStart, 
				UInt8 dacEnd, 
				UInt8 newStep,  
				bool InternTrig, 
				UInt32 waitTime);

			//! Method to restart the dac scan
	unsigned restart(void);

		//! Method to stop the dac scan
	void stop(){this->active = false;};
	
		//! Method to lead the dac scan
	void scan();
		//! method to get if the scan is active or not 
	bool isActive(){return this->active;};


	SpdDelayScan(SPDConfig * spdConf);
	~SpdDelayScan(void);
};
