#pragma once

#include ".\HSConfDefault.h"
#include "..\BitManager\BitsManage.h"
#include "..\spdDbConfLib\spdGlobals.h"
#include ".\DigitalPilot.h"

using namespace spdGlobals;

class HSConfiguration : public HSConfDefault{

 private:
   	UInt8 PxDAC[10][44];               //Pixels Chip DACs
    UInt32 PxTP[10][256];               //Pixels TP        //for tp and mask the structure is: 
    UInt32 PxMASK[10][256];             //Pixels Mask      //first 8 UInt32 are colum 0, the second 8 UInt32 column 1..
                                        //                 //bit 0 (right one) of first UInt32 is {0,0} (column, row) 
      
    //MCM Parameters
    DigitalPilot DPI;
	UInt32 GOLConf;
    UInt8 APIConf[6];                 //API Configuration
                                      //APIConf[0] = DAC_REF_HI
                                      //APIConf[1] = DAC_REF_MID
                                      //APIConf[2] = GTL_REF_A
                                      //APIConf[3] = GTL_REF_D
                                      //APIConf[4] = AN_TEST_HI
                                      //APIConf[5] = AN_TEST_LOW
	UInt32 PixChipInChain;      //bit 0 at 1 = chip 0 in chain, bit 1 at 1 = chip 1 .... 
    
	
	// std::vector <PixelCoordinate> noisyPixels[NPixels];
     
    public:
	HSConfiguration(void);
	~HSConfiguration(void);

    UInt32 LoadClassDefaults(void);
    
	
	UInt32 SetClassToDb(PixelConfDb &dbData);

	UInt8 GetPxDAC(UInt8 ChipN, UInt8 DACn){return PxDAC[ChipN][DACn];};               
	void SetPxDAC(UInt8 val, UInt8 ChipN, UInt8 DACn){PxDAC[ChipN][DACn] = val;};               
	UInt8 * GerPxDACVect(UInt8 ChipN){return PxDAC[ChipN];};
    void SetPxDACVector(UInt8 * inputStream,UInt8 ChipN);

	UInt32 * GetPxTP(UInt8 ChipN){return PxTP[ChipN];};
	void SetPxTP(UInt32 * Input, UInt8 ChipN);
	UInt32 * GetPxMASK(UInt8 ChipN){return PxMASK[ChipN];};
    void SetPxMASK(UInt32 * Input, UInt8 ChipN);

	std::vector <PixelCoordinate> getNoisyPixelVector(UInt8 ChipN);
      
	DigitalPilot * GetDPI(void){return  &this->DPI;};
	UInt32 GetGOLConf(void){return GOLConf;};
	void SetGOLConf(UInt32 val){GOLConf = val;};
	
	UInt8 * GetAPIConf(void){return APIConf;};
    void SetAPIConf(UInt8 * Input);

	UInt32 GetPixChipInChain(void){return PixChipInChain;};
	void SetPixChipInChain(UInt32 pix){PixChipInChain = pix;};
    
	UInt8 GetDACRefHiAPIConf(void){return APIConf[0];};
	void SetDACRefHiAPIConf(UInt8 Value){APIConf[0] = Value;};
    UInt8 GetDACRefMidAPIConf(void){return APIConf[1];};
	void SetDACRefMidAPIConf(UInt8 Value){APIConf[1] = Value;};
	UInt8 GetDACGtlRefAPIConf(void){return APIConf[2];};
	void SetDACGtlRefAAPIConf(UInt8 Value){APIConf[2] = Value;};
	UInt8 GetDACGtlRefDAPIConf(void){return APIConf[3];};
	void SetDACGtlRefDAPIConf(UInt8 Value){APIConf[3] = Value;};
	UInt8 GetDACTestHiAPIConf(void){return APIConf[4];};
	void SetDACTestHiAPIConf(UInt8 Value){APIConf[4] = Value;};
	UInt8 GetDACTestLowAPIConf(void){return APIConf[5];};
	void SetDACTestLowAPIConf(UInt8 Value){APIConf[5] = Value;};
	
    
    void SetGOLWaitTime(UInt8 Val){insertIntoInt32(GOLConf,Val, 0, 5);};          //ConfREG0
    UInt8 GetGOLWaitTime(void){return (UInt8)extractFromInt32(GOLConf, 0, 5);}; 

	void SetGOLLossOfLock(UInt8 Val){insertIntoInt32(GOLConf,Val, 5, 3);}; 
    UInt8 GetGOLLossOfLock(void){return (UInt8)extractFromInt32(GOLConf, 5, 3);}; 

	void SetGOLPLLlockTime(UInt8 Val){insertIntoInt32(GOLConf,Val, 8, 4);};        //ConfREG1
    UInt8 GetGOLPLLlockTime(void){return (UInt8)extractFromInt32(GOLConf, 8, 4);};

	void SetGOLEnSoftLossLock(bool Val){insertIntoInt32(GOLConf,Val, 12, 1);};
    bool GetGOLEnSoftLossLock(void){return extractFromInt32(GOLConf, 12, 1)?true:false;}; 

	void SetGOLEnLossLockCount(bool Val){insertIntoInt32(GOLConf,Val, 13, 1);};
	bool GetGOLEnLossLockCount(void){return extractFromInt32(GOLConf, 13, 1)?true:false;};

    void SetGOLEnForceLock(bool Val){insertIntoInt32(GOLConf,Val, 14, 1);};
    bool GetGOLEnForceLock(void){return extractFromInt32(GOLConf, 14, 1)?true:false;}; 
	
	void SetGOLEnSelfTest(bool Val){insertIntoInt32(GOLConf,Val, 15, 1);}; 
    bool GetGOLEnSelfTest(void){return extractFromInt32(GOLConf, 15, 1)?true:false;}; 
	
	void SetGOLPLLCurrent(UInt8 Val){insertIntoInt32(GOLConf,Val, 16, 5);};        //ConfREG2
	UInt8 GetGOLPLLCurrent(void){return (UInt8)extractFromInt32(GOLConf, 16, 5);}; 

    void SetGOLTestSel(UInt8 Val){insertIntoInt32(GOLConf,Val, 21, 2);}; 
	UInt8 GetGOLTestSel(void){return (UInt8)extractFromInt32(GOLConf, 21, 2);}; 

    void SetGOLEnFlag(bool Val){insertIntoInt32(GOLConf,Val, 23, 1);}; 
	bool GetGOLEnFlag(void){return extractFromInt32(GOLConf, 23, 1)?true:false;}; 
    
	void SetGOLLDCurrent(UInt8 Val){insertIntoInt32(GOLConf,Val, 24, 7);};         //ConfREG0
	UInt8 GetGOLLDCurrent(void){return (UInt8)extractFromInt32(GOLConf, 24, 7);};
    
	void SetGOLUseConfReg(bool Val){insertIntoInt32(GOLConf,Val, 31, 1);}; 
	bool GetGOLUseConfReg(void){return extractFromInt32(GOLConf, 31, 1)?true:false;};


	static double convertTempBUS( UInt32 digitalValue);
	static double convertTempMCM( UInt32 digitalValue);

		//! wil set the internal noisy pixel vector using as input the matrix like the one received by pvss
	void SetNoisyPixelVect(UInt32 Input[10][256]);

		//! wil set the internal dead pixel vector using as input the matrix like the one for a jtag scan
	void SetNoisyPixelsVectJT(UInt32 chipSel, Uint32 * Matrix,  bool repeatFirstMatrix = false);

	
	




};


/*
200 spare location
16 TestCondDef
40 LadderIV
440 DACVect
6 apiVect
8 DPIVect
4 GOLVect
1 Channel
16000AnalConv
2 NoisyPixlengh
n NoisipixVect 1stbyte chip, 2nd bute row 3th byte column
2 DeadPixLengh
n DeadPixelVect
2 NoteLenght
n Note
120 THVect


In API conv file.

4      cells          IPt
4                     VddMCM                
4                     GNDdiff
4                     TemperatureTest
5120                  DAC conv        HI, Mid, GTL, TestHi, TestLow
5                     Delta ref Hi   
5                     Delta ref Mid
5                     Delta gtl  Ref
5                     Delta test Hi
5                     Delta test Low
4096               Sense V
4096               Sense I2
1600               Pixel Vdd/2
-------
14953
*/