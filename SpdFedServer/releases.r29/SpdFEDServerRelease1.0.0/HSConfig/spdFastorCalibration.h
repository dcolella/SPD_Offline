#pragma once

#ifndef spdFastorCalibration_h

#define spdFastorCalibration_h

#include <dic.hxx>
//#include "SPDConfig.h"
#include "..\VMEAcc\VMEAccess.h"
#include "stdafx.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"
#include "spdScan.h"

//const char * pitCommandName = "PIT/SPD_COMMAND";
#define pitCommandName "PIT/COMMAND"
#define pitCmdStatusService "PIT/CMD_STATUS"
//const char * pitCommandName = "PIT/COMMAND";
//const char * pitCmdStatusService = "PIT/CMD_STATUS";

class SPDConfig;

const UInt8 scanType = 6;
const int numberOftestPulses = 7;

/*! fastor calibration scan
	1: set a test pulse matrix
	2: loop over fopol, convpol, compref, cgpol and prevth
		sending triggers and asking the PIT FED for the fastor counters
	3:write header to router and send extra trigger with the counter data 
	(the actual hits in the detector are not important)
*/
class spdFastorCalibration :private DimClient{
	//! counts the current step
	unsigned stepCounter;

	spdLogger *log;
	
	//! Stores when  the last command to the pit was sent
	DWORD timePitRequest;
	//! stores the pit timeout time, it is initialized to 10000 ms
	DWORD pitRequestTimeout;

	DACParameters fopol, convpol, compref, cgpol ,prevth;

	timer timerScan;
	
	//! keeps the current row of the test matrix being used
	unsigned int row;
	//! keeps the current column of the tet matrix being used
	unsigned int column;
    
		//! Dim info connected with the services in the PIT fed to get all the fo counters
	DimUpdatedInfo *getFastorCounters;

		//! Dim Info getting the return value of the pit fed commands
	DimUpdatedInfo *cmdReturn;

		//! Dim Info getting the ID of the command sent to the pit fed
	DimUpdatedInfo *cmdId;
		//! Dim command 
	DimUpdatedInfo * pitCmdStatus;

		//! Connection to the SPD Config class to access all configuration methods
	SPDConfig *parent;

		//! internal member which defines if there is a scan active or not
	bool active;
		
		//! internal member which defines the number of triggers used in each step
	UInt32 triggerNumber;

		//! internal member which defines the matrices to set for the test pulse
		/*	bit 0 = empty matrix
				1 = test pulse in (128,16)
				2 = test pulse in (0,0)
				3 = test pulse in (0,31)
				4 = test pulse in (255,31)
				5 = test pulse in (255,0)
				6 = high occupancy (7% of pixels)
		*/
	UInt32 testPulseActive;			

		//! variable needed to keep track of the scan phase 
		/*! This is needed because the DIM does not support the send receiving of the commands in the same loop,
			flush problem using DIM, I've sent a support request without any success
			so a solution is to leave the cycle and keep track of the step phase like this the flush will be done
		*/
	int stepPhase;

		//! variable to keep track of the current row for the test pulse
	int testPulse;	

		//! info handler method that overloads the dim DimClient one
		/*! This method is a callback function which will be called every time the
			services connect to pit FED are refreshed
		*/
	void infoHandler(void);

		/*! Sends a command to the pixel trigger
			For the moment is just calling the DimClient::SendCommand  function
			but the main objective is to encapsulate the interface with the pit commands
		*/
	inline int sendPitCommand(const char * command);

		/*! Method to finalize the step of the scan
			Writes the header in the enabled routers,
			checks all DAC values that need to be incremented and the new test pulse to be set
			this method is needed because of the DIM flush problem 
			so it had to be separated in several functions
			@return error state 0 = everything was ok (all dacs where set properly)
		*/
	int finalizeStep();

		/*! function to set all test pulse matrices in all enabled channels 
			@param currentTestPulse- the number of the current test pulse to set [0-5]
			@return the error state of the operation (0=ok not 0 means one problem with one of the channels)
		*/
	UInt32 setTpMatrixAllEnabled( int currentTestPulse);
	//! method to return the next selected test-pulse
	int checkNextTestPulse (int currentTestPulse );

		/*! Method to set a dac value in all enabled channels
			it sets all 10 pixel chips of all the enabled chanels with the same dac value
			@param DACn- number identifying the dac
			@param DACValue- value to set in the dacs
		*/
	UInt32 setDacEnabledHalfStaves( UInt8 DACn, UInt8 DACValue);
    
		/*! function to set the header in all enabled routers 
			@return the error state of the operation (0=ok not 0 means one problem with one of the channels)
		*/
	UInt32 sendHeadersAllEnabled( UInt32  foData[]);

		/*! method to save the fastor data to log
		*/
	int saveFoDataToLog(int router, UInt32 * foData);
	
	void setPixelInMatrix( UInt32 testPulseMatrix[], unsigned row, unsigned column){
		spdGlobals::insertIntoInt32 (testPulseMatrix[(8*column) + row/32], 1, row%32, 1);
	};

		//! Method to perform one step of the fastor calibration
	unsigned foStep();
public:
	
	
	spdFastorCalibration(SPDConfig * spdConf );
	~spdFastorCalibration(void);


		//! Method to start the calibration of the Fast Or
	unsigned start(	unsigned int fopolMinimum,
					unsigned int fopolMaximum,
					unsigned int fopolStep,

					unsigned int convpolMinimum,
					unsigned int convpolMaximum,
					unsigned int convpolStep,

					unsigned int comprefMinimum,
					unsigned int comprefMaximum,
					unsigned int comprefStep,

					unsigned int cgpolMinimum,
					unsigned int cgpolMaximum,
					unsigned int cgpolStep,

					unsigned int prevthMinimum,
					unsigned int prevthMaximum,
					unsigned int prevthStep,

					UInt32 triggerNum,
					UInt32 testPulseMatrices);

		//! Method to restart the calibration of the Fast Or
	unsigned restart(void);

		//! Method to stop the calibration of the Fast Or
	void stop();
	
 
		//! Method to perform the scan
	/*Because of the callback method of DIM (serviceHandler) 
	here we only need to check if the PIT FED server  answers the requests in time
	Other checks can be added here*/
	void scan();

		//! method to test the fastor calibration, will perform only one step of the scan
	UInt32 testFastorCalibration();
		//returns if the scan is active or not
	bool isActive(){return this->active;};
};

#endif
