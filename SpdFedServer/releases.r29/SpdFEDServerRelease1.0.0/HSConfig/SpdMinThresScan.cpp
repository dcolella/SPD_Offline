#include "StdAfx.h"
#include ".\spdminthresscan.h"
#include "SPDConfig.h"


SpdMinThresScan::SpdMinThresScan(SPDConfig * spdConf)
{

	this->parent = spdConf;
	this->log = &spdLogger::getInstance();

	this->dacScan = new SpdDacScan(spdConf);
	this->dacScan->scanType = 0;
	this->prevth_index = 39;
	this->active=false;

}

SpdMinThresScan::~SpdMinThresScan(void)
{
	delete this->dacScan;
}


unsigned SpdMinThresScan::start( UInt32 triggerNumber, UInt8 dacMin, UInt8 dacMax, UInt8 step, bool internTrig, UInt32 waitTime){

	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}
	// only first chip to start
	dacScan->setChipSelected(0x001);
	this->dacScan->start(triggerNumber, dacMin,dacMax, step,prevth_index, internTrig, waitTime);
	this->active = true;
	scanPhase = 0;
	log->log("MinTh chip %d/10", scanPhase+1);
	return 0;
}

unsigned SpdMinThresScan::restart(void){
	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}
	this->active=true;
	return 0;
}

void SpdMinThresScan::scan(){
	if(this->active == false) return;
		
	if (this->dacScan->isActive() == FALSE){

			// will set all pre_vth to the minimum
		for (int hs = 0 ; hs < NHalfStaves ; hs ++) {
			if (parent->halfStaves[hs]->getChActStatus() == 2){

				parent->halfStaves[hs]->LoadHSPixelDAC( 1023, this->prevth_index ,this->dacScan->dac.minimum);  
			}
		}
		

		//end of the scan
		if (this->scanPhase == 9) {
			this->active = false;
			log->log("Min TH Scan finished");
			
			return;
		}
		else {
			// shifts the enabled chip to the left 
			Uint32 chipSelect = dacScan->getChipSelected();
			dacScan->setChipSelected(chipSelect <<= 1);

					// like this restarts the least scan
			this->dacScan->dac.current = this->dacScan->dac.minimum;

			++scanPhase;
			log->log("MinTh chip %d/10", scanPhase+1);
			
			
			this->dacScan->active=true;
		} 
	

	}

	// will perform the dac scan here with the correct pixel chip
	this->dacScan->scan();

	return;
}
