#pragma once
#include "StdAfx.h"
#include ".\vmeregaccess.h"

VMERegAccess::VMERegAccess(){
    VRAOperation = 3;
}

VMERegAccess::~VMERegAccess(void)
{
}

ViStatus VMERegAccess::VMERegisterAcc(UInt32  RegAddr, UInt32 * Value, UInt32 dataSize, char  mode){
	
	// if in debug mode returns
    if(this->isInDebugMode()){
		//cout << "|";
		return 0;
	}

	//if(VRAOperation && 1) SessMappedNumb = MapAddr(*RegAddr, dataSize, &MappedRetNumb);
	if(dataSize ==0) dataSize =1;

	if(dataSize == 1){
		if (mode == 'R'){
			viIn32(VisSesObjGen(0), VI_A32_SPACE, RegAddr,  Value); 
		} 
		else if (mode == 'W'){
            viOut32(VisSesObjGen(0), VI_A32_SPACE, RegAddr,  *Value); 
		}
	}else if (dataSize > 1){ 		
		if (mode == 'R'){
			viMoveIn32(VisSesObjGen(0), VI_A32_SPACE, RegAddr,  dataSize, Value); 
		} 
		else if (mode == 'W'){
            viMoveOut32(VisSesObjGen(0), VI_A32_SPACE, RegAddr,  dataSize,  Value); 
		}
	}
	
	return 0;
}
