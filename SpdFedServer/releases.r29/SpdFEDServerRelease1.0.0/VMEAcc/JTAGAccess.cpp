#pragma once
#include "StdAfx.h"
#include ".\jtagaccess.h"

JTAGAccess::JTAGAccess(){
	_ScanType = 0 ;    
	_BitNumber = 0;   
	_ChNumb = 0;       
	_CLKSpeed = 0;     
	_JTAGHeader = 0xffffffff;
    _VectorOut = new UInt32;
    BuildContrWords(0);
}

JTAGAccess::~JTAGAccess(void){
	if (_VectorOut != NULL) delete _VectorOut;
}

//The JTAGWriter functions are used to 
ViStatus JTAGAccess::JTAGWriter(UInt8  ChN, UInt32 * VectIn, UInt32  ScType, UInt32 BtNumb){
	this->SetJTChannel(ChN);
	_VectorIn = VectIn+1;
	_ScanType = ScType;
	_BitNumber = BtNumb;
	return this->JTAGScanReg();
}

ViStatus JTAGAccess::JTAGWriter(UInt8 ChN, UInt32 * IRVect, UInt32 * DRVect){
	this->SetJTChannel(ChN);
    
	_BitNumber = IRVect[0];
	_ScanType = IR;
	_VectorIn = IRVect + 1;
	status = JTAGScanReg();
	if(status != 0) return status;
	
    _BitNumber = DRVect[0];
	_ScanType = DR;
	_VectorIn = DRVect + 1;
	status = JTAGScanReg();
	return status;
}


ViStatus JTAGAccess::JTAGWriter(UInt8 ChN, UInt32 * IRVect0, UInt32 * DRVect0, UInt32 * IRVect1, UInt32 * DRVect1){
	status = this->JTAGWriter(ChN, IRVect0, DRVect0);
	if(status != 0) return status;
	status = this->JTAGWriter(ChN, IRVect1, DRVect1);
	return status; 
}


void JTAGAccess::SetJTClkSpeed(UInt32 CLKSp){
	if(CLKSp >=0 && CLKSp < 5){
		_CLKSpeed = CLKSp;
		std::cout << "INFO: JTAG clock speed set at " << (UInt32)(2^_CLKSpeed) <<" MHz\n";
	}else{
		std::cout << "ERROR: The JTAG clock speed specified is out of range 0..4\n" ;
		std::cout << "0 = 1 MHz, 1 = 2 MHz, 2 = 4 MHz, 3 = 8 MHz\n";
	}
}


	
	




//Scan the JTAG register: IstrType tell if scan IR (0) or DR (1) or RESET (2)   
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
ViStatus JTAGAccess::JTAGScanReg(void){
           
	if(isInDebugMode()){
		//cout << "|";
		return 0;
	}
    BuildContrWords(2);
	
	this->SetJTChannel(_ChNumb);
	if(!this->IsControllerIdle(TRUE)) {
		this->ResetJTAGContr();
	}
	this->BuildContrWords(1);
	    		

    if(_ScanType == 2){      //RESET istruction
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _ContrWord1); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _ExStartAddr,  0); 

		
	}else{
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _ContrWord1); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _ContrWord2); 
		
		for(UInt32 i=0; i < _WordsNumber; i++){
			viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _VectorIn[i]); 
		}
    
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
        viOut32(VisSesObjGen(0), VI_A32_SPACE, _ExStartAddr, 0);
    
        UInt32 startTime;
		startTime =	GetTickCount();  

        while(!this->IsControllerIdle(FALSE)){  				
			if((GetTickCount() - startTime) > 500){
				printf("JTAG Timeout\n");
				this->ResetJTAGContr();
				return 1;
			}
		}
			
                
		if(_VectorOut != NULL) delete[] _VectorOut;
		_VectorOut = new UInt32 [_WordsNumber + 1];
			
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
			
        for(UInt32 i=0; i < _WordsNumber; i++){
			viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + i);
		}
            
		_VectorOut[_WordsNumber - 1] >>=  (32 - _LastWordBits);

		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
			
		if(!this->IsControllerIdle(TRUE)) this->ResetJTAGContr();
	}
    
	return 0;
}

bool JTAGAccess::IsControllerIdle(bool reset){

	

	if(isInDebugMode()){
		//cout << "|";
		return 0;
	}

	UInt32 StatusContr;
	ViStatus status = viIn32(VisSesObjGen(0), VI_A32_SPACE, _StatusRegAddr, &StatusContr);

	if(status != NULL){
		cout << "ERROR Accessing JTAG" << endl;
		return FALSE;
	}

	if((StatusContr == 0xCC018001 && reset) || ((StatusContr & 0x1) && !reset)) return TRUE;

	
	return FALSE;
}


ViStatus JTAGAccess::ResetJTAGContr(){  

	if(isInDebugMode()){
		//cout << "|";
		return 0;
	}
    ViStatus status = 1;
	
	status = viOut32(VisSesObjGen(0), VI_A32_SPACE,  _ResetControllerSM, 0);
    
	return status;
}		

void JTAGAccess::SetJTChannel(UInt8 ChN){
	 _ChNumb = ChN;
	 _RdWrDataAddr = AG->getRoutAddr(_ChNumb).GetJTRdWrDataAddr(_ChNumb);
	 _ExStartAddr = AG->getRoutAddr(_ChNumb).GetJTExStartAddr(_ChNumb);
	 _StatusRegAddr = AG->getRoutAddr(_ChNumb).GetJTStatusRegAddr(_ChNumb);
	 _ResetControllerSM = AG->getRoutAddr(_ChNumb).GetJTResetStateMacAddr(_ChNumb);
}


void JTAGAccess::BuildContrWords(UInt8 Mode){
	   
	if(Mode == 0 || Mode == 1){
	   _ContrWord1 = (_CLKSpeed << 12) + (_ChNumb << 7) + _ScanType; 
	}
	if(Mode == 0 || Mode == 2){
       _WordsNumber = (_BitNumber / 32);
       _LastWordBits = _BitNumber % 32;
	   if (_LastWordBits == 0){ 
		   _LastWordBits = 32;
	   } else {
		   _WordsNumber++;
	   }
       _ContrWord2 = _WordsNumber + (_LastWordBits - 1) * 0x4000000;
	}
}
