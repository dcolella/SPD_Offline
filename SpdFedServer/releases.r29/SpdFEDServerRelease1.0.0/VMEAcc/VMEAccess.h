#pragma once

#include "JTAGAccess.h"
#include "../addressgenerator/addressgenerator.h"
#include "../HSConfig/hsconfiguration.h"


class VMEAccess: public JTAGAccess{
	//HSConfiguration * HS;

	UInt8 DataType;      /*Data Type
						  0 = normal data
						  1 = DAC Scan 
						  2 = TP Scan
						  3 = Matrix
						  */
public:
	VMEAccess(AddressGenerator *);
	~VMEAccess(void);

	   
	UInt32 * GetEvent(int ChNumb);    //ok

	UInt8 GetDataType(void){return DataType;};
	void SetDataType(UInt8 val){DataType = val;};

	ViStatus SendTrig10Chip(UInt8 ChN, UInt32 TriggerN ){return SendTrigger(ChN, TriggerN, 1,1,1,1);};
    ViStatus SendTrigger(UInt8 ChN,UInt32 TriggerN, bool Delay, bool l2y, bool l1, bool tp);




};
