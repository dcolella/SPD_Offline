#pragma once

#include "StdAfx.h"
#include "ErrorHandler.h"




class VisaSessControl : public ErrorHandler{
  private:

      UInt8 MaxOpenSess;
      char  DevLogicAddr[50]; 

	  ViSession ResourceMen;
      ViSession * VisaSess;
	  bool * Mapped;
	  

	  UInt8 NVisSessOpened;

	  bool debugMode;
	  

  public:
	VisaSessControl(void);					//ok
	~VisaSessControl(void);					//ok
		
	ViSession VisSesObj(int SesNumb){return VisaSess[SesNumb];};                 //ok
	ViSession VisSesObjGen(int SesNumb);                 //ok
	UInt8 GetUnmappedSessNumb(void);                //ok
	ViStatus RunResorceMenager(void);         //ok
	ViStatus CloseSess(int SesNumb);          //ok
	ViStatus CloseUnmappedSess(void);          //ok
	void SetDevLogicAddr(char &LogAddr){strcpy(DevLogicAddr, &LogAddr);};  //ok
	
	UInt8 NbVISAOpened(void){return NVisSessOpened;};  //ok
	bool SessionMapped(int SesNumb){return Mapped[SesNumb];}; //ok
	void SetMapSession(int SesNumb){Mapped[SesNumb] = 1;};    //ok
	void SetUnMapSession(int SesNumb){Mapped[SesNumb] = 0;};    //ok

	UInt8 MapAddr(UInt32 Offset, UInt32 CellsNumber, ViAddr * RetAddr); //ok
	void UnMapAddr(UInt8 VirtSesNumb);             //ok

   	ViSession GetResMen(void){return ResourceMen;};

	bool isInDebugMode(){return debugMode;};
	void setDebugMode(bool value){debugMode=value;};

};
