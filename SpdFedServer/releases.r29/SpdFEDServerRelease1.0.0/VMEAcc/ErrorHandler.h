#pragma once

#ifndef ErrorHandler_h

class ErrorHandler{
  public:
    ViStatus status;
    char OutMsgText[200];
	
	ErrorHandler(void);
	~ErrorHandler(void);
	void CheckStatus(void);
	void CheckStatus(ViStatus);

	//cesar 27/05/2008: changed this declaraion so to be more meaningful
	//char * CheckStatus(char * , ViStatus,  unsigned int );
	char * CheckStatus(char * command , ViStatus error,  unsigned int channel);
	
};

#define ErrorHandler_h
#endif
