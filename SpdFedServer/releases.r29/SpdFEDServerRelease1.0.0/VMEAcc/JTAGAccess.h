#pragma once

#include ".\vmeregaccess.h"
//#include "VisaSessContr.h"
#include "errorhandler.h"


#define  JTAGHeader  0xffffffff


#define DR 0
#define IR 1
#define RESET 2


class JTAGAccess : public VMERegAccess{
  private:
	 UInt32 * _VectorIn;
	 UInt32 * _VectorOut;
     
	 UInt32 _RdWrDataAddr;
	 UInt32 _ExStartAddr;
	 UInt32 _StatusRegAddr;
	 UInt32 _ResetControllerSM;

	 UInt32 _ScanType;     //IstrType tell if scan IR (1) or DR (0) or RESET (2)
	 UInt32 _BitNumber;    
	 UInt8  _ChNumb; 
	 UInt32 _CLKSpeed;     //clkSpeed  0 = 1MHz, 1 = 2 MHz, 2 = 4MHz, 3= 8MHz      


     UInt32 _ContrWord1;   //ControlWord1 = 15..12 clKspeed, 10..8 Channel, 7..0 Istruction   
     UInt32 _ContrWord2;	  //ControlWord2 =  31..26 BIt last word, 25..0 Word Number 
	 UInt32 _WordsNumber;  
	 UInt32 _LastWordBits;
	
	 UInt32 _JTAGHeader;  

	 void BuildContrWords(UInt8 Mode);								 
	 
	 bool IsControllerIdle(bool reset);								
	 ViStatus ResetJTAGContr(void);						
	 void SetJTChannel(UInt8 ChN);	

	 ViStatus JTAGScanReg(void);
 public:  
	 JTAGAccess();                          
	 ~JTAGAccess(void);
	 	 	
	 UInt32 * RdJTVectOut(){return _VectorOut;};		
	 	 
	 void SetJTClkSpeed(UInt32 CLKSp); 
	 
	 UInt32 GetWordsNumber(){return _WordsNumber;};             
	 UInt32 GetBitsNumber(){ return _BitNumber;};

	 ViStatus JTAGWriter(UInt8 ChN, UInt32 * VectIn, UInt32 ScType, UInt32 BtNumb); //To implement 6 ch togather 
     ViStatus JTAGWriter(UInt8 ChN, UInt32 * IRVect0, UInt32 * DRVect0);
	 ViStatus JTAGWriter(UInt8 ChN, UInt32 * IRVect0, UInt32 * DRVect0, UInt32 * IRVect1, UInt32 * DRVect1);
};

