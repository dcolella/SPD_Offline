#pragma once
#include "StdAfx.h"
#include "errorhandler.h"
#include "..\spdDbConfLib\spdLogger.h"


ErrorHandler::ErrorHandler(void){
	status = NULL;
}

ErrorHandler::~ErrorHandler(void){

}

void ErrorHandler::CheckStatus(ViStatus InStatus){ 	

	if (InStatus != VI_SUCCESS){
       cerr << "Error" << InStatus << endl;
       exit(1);
    }
}


void ErrorHandler::CheckStatus(void){
	CheckStatus(status);
}

char * ErrorHandler::CheckStatus(char * MsgText, ViStatus InStatus , unsigned int  channel){
	if (InStatus != VI_SUCCESS){
		sprintf(OutMsgText,  "ERROR: Operation %s not succeeded, channel %d\n", MsgText, channel);
	} else{
		sprintf(OutMsgText,  "INFO: Executed %s, channel %d\n", MsgText, channel);
	}
	
	return  OutMsgText;
}


