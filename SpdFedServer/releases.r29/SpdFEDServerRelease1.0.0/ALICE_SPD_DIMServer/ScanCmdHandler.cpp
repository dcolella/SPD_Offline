#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"

int DimServerCommandHandler::ScanCmdHandler(void){

    if(strncmp(CommString, "SCN_", 4) != 0) return 2;
	
	status = 0;
    // Uniformity scan
	if(strcmp(CommString, "SCN_UNIFORMITY_START" ) == 0){

		const int numberOfInputs = 5;

		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}

		stringstream msg( "received Uni Matrix: ");
		
		for (int n = 0 ; n < numberOfInputs ; n ++){
			msg << dec << DataIn[n]<<  " ,";
		}
		
		logger->logToFile(msg.str().c_str());

		status = SPD->getUniMatrixScan().start((UInt32)DataIn[1],		//trigger number
												(UInt8)DataIn[2],		// start row
												(UInt8)DataIn[3],		// end row
												DataIn[4]?true:false);	// mask no active pixels

	} 
	// fastor calibration scan
	else if (strcmp(CommString, "SCN_FO_SCAN_START") ==0){	

		const int numberOfInputs = 17;
		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}

		stringstream msg("Fastor scan data received: ");
		
		for (int input = 0 ; input < numberOfInputs; ++input){
			msg << dec << DataIn[input] << " ,";
		}

		logger->logToFile(msg.str().c_str());
		// will have to stop all poolings
		//this->POOL->stopHSTempPool();
		//this->POOL->stopRouterErrorPooling();

		SPD->getFoScan().start( DataIn[0], // fopolMinimum
								DataIn[1], // fopolMaximum
								DataIn[2], // fopolStep

								DataIn[3], // convpolMinimum
								DataIn[4], // convpolMaximum
								DataIn[5], // convpolStep

								DataIn[6], // comprefMinimum
								DataIn[7], // comprefMaximum
								DataIn[8], // comprefStep

								DataIn[9], // cgpolMinimum
								DataIn[10], //cgpolMaximum
								DataIn[11], // cgpolStep

								DataIn[12], // prevthMinimum
								DataIn[13], //prevthMaximum
								DataIn[14], // prevthStep

								DataIn[15], // triggerNum
								DataIn[16]		// Matrices
								); 

		return 0;
	}
	else if (strcmp(CommString, "SCN_FO_SCAN_STOP") ==0){
		SPD->getFoScan().stop();		// command to stop the fastor calibration
	}
	else if(strcmp(CommString, "SCN_FO_SCAN_TEST") ==0){

		SPD->getFoScan().testFastorCalibration();
		
	} 
	else if(strcmp(CommString, "SCN_UNIFORMITY_STOP") ==0){
		SPD->getUniMatrixScan().stop();
	} 
	else if(strcmp(CommString, "SCN_UNIFORMITY_RESTART") ==0){
		status = SPD->getUniMatrixScan().restart();
	
	}
	else if(strcmp(CommString, "SCN_MEANTH_START") ==0){

		const int numberOfInputs = 8;

		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}

		stringstream msg("received Uni Matrix: ");

		for (int input = 0 ; input < numberOfInputs; ++input){
			msg << dec << DataIn[input] << ", ";
		}

		logger->logToFile(msg.str().c_str());

		status = SPD->getMeanThScan().start( (UInt8)DataIn[4], // dac min
										  (UInt8)DataIn[5], // dac max
										  (UInt8)DataIn[6],//step
										  (UInt8)DataIn[2], // start row
										  (UInt8)DataIn[3], //end row
										  1,				//row step
										  (UInt32)DataIn[1] //trigger number
									   );// DCSAct??? what is that?!
	} 
	else if(strcmp(CommString, "SCN_MEANTH_STOP") ==0){
		SPD->getMeanThScan().stop();
	} 
	else if(strcmp(CommString, "SCN_MEANTH_RESTART") ==0){
		status = SPD->getMeanThScan().restart();
	
	} 

	else if(strcmp(CommString, "SCN_DAC_START") ==0){

		const int numberOfInputs = 9;

		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}

		stringstream msg("dac scan inputs : ");
		for (int input = 0 ; input < numberOfInputs; ++input){
			msg << dec << DataIn[input] << ", ";
		}

		logger->logToFile(msg.str().c_str());

		status = SPD->getDacScan().start( (UInt32)DataIn[1],		//trigger number
									(UInt8)DataIn[2],		// dac min
									(UInt8)DataIn[3],		//dac max
									(UInt8)DataIn[4],		//step
									(UInt8)DataIn[5],		//dac number
									DataIn[6]?true:false,	//insternal trigger
									//DataIn[7]?true:false,	// DCSAct??? what is that?!
									(UInt32)DataIn[8]);		// wait time


	} else if(strcmp(CommString, "SCN_DAC_STOP") ==0){
		SPD->getDacScan().stop();

	} else if(strcmp(CommString, "SCN_DAC_RESTART") ==0){
		status = SPD->getDacScan().restart();
	} 
	else if(strcmp(CommString, "SCN_DELAY_START") ==0){

		
		const int numberOfInputs = 8;

		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}

		stringstream msg("delay scan inputs : ");

		for (int input = 0 ; input < numberOfInputs; ++input){
			msg << dec << DataIn[input] << ", ";
		}
		
		logger->logToFile(msg.str().c_str());

		status = SPD->getDelayScan().start((UInt32)DataIn[1],		// trigger number
									(UInt8)DataIn[2],		// dac min
									(UInt8)DataIn[3],		// dac max
									(UInt8)DataIn[4],		// step
									DataIn[5]?true:false,	// internal trigger
									//DataIn[6]?true:false,	// flag saying if the dcs is active or not 
									(UInt32)DataIn[7]);		// wait time


	} else if(strcmp(CommString, "SCN_DELAY_STOP") ==0){
		SPD->getDelayScan().stop();

	} else if(strcmp(CommString, "SCN_DELAY_RESTART") ==0){
		status = SPD->getDelayScan().restart();

	} 
	else if(strcmp(CommString, "SCN_MINTH_START") ==0){


		const int numberOfInputs = 8;

		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}
		stringstream msg;
		for (int input = 0 ; input < numberOfInputs; ++input){
			msg << dec << DataIn[input] << ", ";
		}
		logger->logToFile(msg.str().c_str());

		status = SPD->getMinThresScan().start(
									(UInt32)DataIn[1],		//trigger number
									(UInt8)DataIn[2],		// dac min
									(UInt8)DataIn[3],		// dac max
									(UInt8)DataIn[4],		// step
									DataIn[5]?true:false,	// internal trigger
									(UInt32)DataIn[7]);		// wait time
	} 
	else if(strcmp(CommString, "SCN_MINTH_STOP") ==0){
		SPD->getMinThresScan().stop();
		
	} 
	else if(strcmp(CommString, "SCN_MINTH_RESTART") ==0){
		status = SPD->getMinThresScan().restart();
		
	} 
	else if(strcmp(CommString, "SCN_NOISE" ) == 0){

		const int numberOfInputs = 3;

		if (this->dataSize < numberOfInputs){
			this->argumentError();
			return 1;
		}

		stringstream msg;

		for (int input = 0 ; input < numberOfInputs; ++input){
			msg << dec << DataIn[input] << " ";
		}
		logger->logToFile(msg.str().c_str());

		status = SPD->StartNoiseScan((UInt8)DataIn[0],		// channel 
								(UInt32)DataIn[1],		// trigger number
								DataIn[2]?true:false);	// internal trigger or not 


    }
	else if (strcmp(CommString, "SCN_NOISE_STOP" ) == 0){
		//status = SPD->stop


	}
	else{
		sprintf(OutMsgText, "ERROR: CONF command %s not ricognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}
 
	// will update the status of the command here 
	//SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), status);
	return 0; 
}