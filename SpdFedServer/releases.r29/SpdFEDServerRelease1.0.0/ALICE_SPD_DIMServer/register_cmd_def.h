/*! Definition file for commands associated with read-write register commands
	
*/

//************************************ busy card register commands *******************************


#define BSYCD_RESET				"BSYCD_RESET"			//!< 
#define BSYCD_VERSION			"BSYCD_VERSION"			//!<
#define BSYCD_DRVREC			"BSYCD_DRVREC"			//!<
#define BSYCD_L0DELAY			"BSYCD_L0DELAY"			//!<
#define BSYCD_CONTROL			"BSYCD_CONTROL"			//!<
#define BSYCD_BUSYMASK			"BSYCD_BUSYMASK"		//!<
#define BSYCD_L0COUNTER			"BSYCD_L0COUNTER"		//!<
#define BSYCD_INPUTS			"BSYCD_INPUTS"			//!<
#define BSYCD_CNCOUNTERS		"BSYCD_CNCOUNTERS"		//!<

//***************************************** link receiver register commands ***********************
#define LRX_DPM						"LRX_DPM"							//!<
#define LRX_LRX_VERSION				"LRX_LRX_VERSION"					//!< read only register with the version number of the firmware for this link receiver
#define LRX_CONTROL					"LRX_CONTROL"						//!<
#define LRX_STATUS					"LRX_STATUS"						//!<
#define LRX_TEMPERATURE				"LRX_TEMPERATURE"					//!<
#define LRX_CLK_DELAY				"LRX_CLK_DELAY"						//!< write only register, its used to set the delay in the clock line for this channel, 8 bits value
#define LRX_DATA_DELAY				"LRX_DATA_DELAY"					//!< write only register, its used to set the delay in the data line for this channel, 8 bits value
#define LRX_DPM_ADDRESS_COUNTER		"LRX_DPM_ADDRESS_COUNTER"			//!<
#define LRX_MASK_COLUMN				"LRX_MASK_COLUMN"					//!<
#define LRX_RESET					"LRX_RESET"							//!<
#define LRX_SOFT_RESET				"LRX_SOFT_RESET"					//!<
#define LRX_FLUSH					"LRX_FLUSH"							//!<
#define LRX_TEST_REG				"LRX_TEST_REG"						//!<
#define LRX_MCM_STIMULI				"LRX_MCM_STIMULI"					//!<
#define LRX_BUSY_MASK				"LRX_BUSY_MASK"						//!<
#define LRX_HISTOGRAM				"LRX_HISTOGRAM"						//!<
#define LRX_ERROCOUNTER				"LRX_ERROCOUNTER"					//!<
#define LRX_L1_MEM_POINTER			"LRX_L1_MEM_POINTER"				//!<
#define LRX_L1_ROUT_COUNT			"LRX_L1_ROUT_COUNT"					//!<
#define LRX_BUSY_CONTROL			"LRX_BUSY_CONTROL"					//!<
#define LRX_SHIFT_SEL				"LRX_SHIFT_SEL"						//!<
#define LRX_EN_MOVEBIT				"LRX_EN_MOVEBIT"					//!<
#define LRX_EV_START				"LRX_EV_START"						//!<
#define LRX_EV_END					"LRX_EV_END"						//!<
#define LRX_MEM_START				"LRX_MEM_START"						//!<
#define LRX_MEM_END					"LRX_MEM_END"						//!<
#define LRX_FORMAT_ERROR			"LRX_FORMAT_ERROR"					//!<
#define LRX_INPUT_ST_STATUS0		"LRX_INPUT_ST_STATUS0"				//!<
#define LRX_INPUT_ST_STATUS1		"LRX_INPUT_ST_STATUS1"				//!<
#define LRX_EV_DESCRAM_STATUS		"LRX_EV_DESCRAM_STATUS"				//!<
#define LRX_FIFO_PIX_DATA_STATUS	"LRX_FIFO_PIX_DATA_STATUS"			//!<
#define LRX_FIFO_EV_DATA_STATUS		"LRX_FIFO_EV_DATA_STATUS"			//!<
#define LRX_DATA_ENC_STATUS0		"LRX_DATA_ENC_STATUS0"				//!<
#define LRX_DATA_ENC_STATUS1		"LRX_DATA_ENC_STATUS1"				//!<
#define LRX_DATA_ENC_STATUS2		"LRX_DATA_ENC_STATUS2"				//!<
#define LRX_DATA_ENC_STATUS3		"LRX_DATA_ENC_STATUS3"				//!<
#define LRX_DATA_ENC_STATUS4		"LRX_DATA_ENC_STATUS4"				//!<
#define LRX_DATA_ENC_STATUS5		"LRX_DATA_ENC_STATUS5"				//!<
#define LRX_DATA_ENC_STATUS6		"LRX_DATA_ENC_STATUS6"				//!<
#define LRX_DATA_ENC_STATUS7		"LRX_DATA_ENC_STATUS7"				//!<


//************************************************** Router Register Commands **********************************
#define RT_ERROR_MASK					"RT_ERROR_MASK"							//!< Command to access the router error mask register
#define RT_REGISTER						"RT_REGISTER"							//!< function to access a register inside the router
#define RT_DPM							"RT_DPM"								//!<
#define RT_VERSION_NUMBER				"RT_VERSION"							//!< 'RT_VERSION' is already defined in winUser.h file, an old windwos file that we don't want to mess for sure
#define RT_SPM							"RT_SPM"								//!<
#define RT_LRX_MEM_BASEADDR				"RT_LRX_MEM_BASEADDR"					//!<
#define RT_CONTROL						"RT_CONTROL"							//!<
#define RT_STATUS1						"RT_STATUS1"							//!<
#define RT_STATUS2						"RT_STATUS2"							//!<
#define RT_STATUS3						"RT_STATUS3"							//!<
#define RT_STATUS_LINKRX0				"RT_STATUS_LINKRX0"						//!<
#define RT_STATUS_LINKRX1				"RT_STATUS_LINKRX1"						//!<
#define RT_STATUS_LINKRX2				"RT_STATUS_LINKRX2"						//!<
#define RT_STATUS_LINKRX3				"RT_STATUS_LINKRX3"						//!<
#define RT_STATUS_LINKRX4				"RT_STATUS_LINKRX4"						//!<
#define RT_STATUS_LINKRX5				"RT_STATUS_LINKRX5"						//!<
#define RT_STATUS_LINKRX6				"RT_STATUS_LINKRX6"						//!<
#define RT_STATUS_LINKRX7				"RT_STATUS_LINKRX7"						//!<
#define RT_STATUS_LINKRX8				"RT_STATUS_LINKRX8"						//!<
#define RT_STATUS_LINKRX9				"RT_STATUS_LINKRX9"						//!<
#define RT_STATUS_LINKRX10				"RT_STATUS_LINKRX10"					//!<
#define RT_STATUS_LINKRX11				"RT_STATUS_LINKRX11"					//!<
#define RT_STATUS_LINKRX12				"RT_STATUS_LINKRX12"					//!<
#define RT_STATUS_LINKRX13				"RT_STATUS_LINKRX13"					//!<
#define RT_STATUS_LINKRX14				"RT_STATUS_LINKRX14"					//!<
#define RT_STATUS_LINKRX15				"RT_STATUS_LINKRX15"					//!<
#define RT_WRHISTO						"RT_WRHISTO"							//!<
#define RT_RDHISTO						"RT_RDHISTO"							//!<
#define RT_JPLAYER						"RT_JPLAYER"							//!<
#define RT_L0ID							"RT_L0ID"								//!<
#define RT_L1ID							"RT_L1ID"								//!<
#define RT_L2ID							"RT_L2ID"								//!<
#define RT_RSTBNC						"RT_RSTBNC"								//!<
#define RT_IRQPUSHB						"RT_IRQPUSHB"							//!<
#define RT_RST_TEMPLIM					"RT_RST_TEMPLIM"						//!<
#define RT_FO_FROMVME					"RT_FO_FROMVME"							//!<
#define RT_VMERESET						"RT_VMERESET"							//!<
#define RT_DATA_RESET					"RT_DATA_RESET"							//!<
#define RT_STATUS_JTAG_SELECT			"RT_STATUS_JTAG_SELECT"					//!<
#define RT_DATA_SELECT					"RT_DATA_SELECT"						//!<
#define RT_SELECT						"RT_SELECT"								//!<
#define RT_DDL_STATUS					"RT_DDL_STATUS"							//!<
#define RT_RESET_DETECTOR				"RT_RESET_DETECTOR"						//!<
#define RT_RD_EV_START					"RT_RD_EV_START"						//!<
#define RT_EV_LENGTH_OF_BLOCK			"RT_EV_LENGTH_OF_BLOCK"					//!<
#define RT_RESET_TTC					"RT_RESET_TTC"							//!<
#define RT_RESET_LRX					"RT_RESET_LRX"							//!<
#define RT_RESET_HS						"RT_RESET_HS"							//!<
#define RT_SEND_TRIGGER					"RT_SEND_TRIGGER"						//!<
#define RT_FIFO_STARTADDR				"RT_FIFO_STARTADDR"						//!<
#define RT_FIFO_ENDADDR					"RT_FIFO_ENDADDR"						//!<
#define RT_FLUSH_DPM					"RT_FLUSH_DPM"							//!<
#define RT_FO_NUMBER					"RT_FO_NUMBER"							//!<
#define RT_TEMPERATURE					"RT_TEMPERATURE"						//!<
#define RT_JT_RESET_STMACHINE			"RT_JT_RESET_STMACHINE"					//!<
#define RT_JT_RESET_FIFO				"RT_JT_RESET_FIFO"						//!<
#define RT_JT_RDWR_DATA					"RT_JT_RDWR_DATA"						//!<
#define RT_JT_EXEC_START				"RT_JT_EXEC_START"						//!<
#define RT_JT_STATUS					"RT_JT_STATUS"							//!<
#define RT_JT_RESET_CH					"RT_JT_RESET_CH"						//!<
#define RT_JT_RDEN_FIFOIN				"RT_JT_RDEN_FIFOIN"						//!<
#define RT_JT_RDNUMB_FIFOIN				"RT_JT_RDNUMB_FIFOIN"					//!<
#define RT_TEMPERATURE_LIMIT_MCM		"RT_TEMPERATURE_LIMIT_MCM"				//!<
#define RT_TEMPERATURE_LIMIT_BUS		"RT_TEMPERATURE_LIMIT_BUS"				//!<
#define RT_FO_GLOBAL_COUNTER			"RT_FO_GLOBAL_COUNTER"					//!<
#define RT_FO_COINCIDENCE_COUNTER		"RT_FO_COINCIDENCE_COUNTER"				//!<
#define RT_FO_TIME_COUNTER				"RT_FO_TIME_COUNTER"					//!<
#define RT_FO_LINKRX_COUNTER			"RT_FO_LINKRX_COUNTER"					//!<
#define RT_SCOPE_SELECTOR0				"RT_SCOPE_SELECTOR0"					//!<
#define RT_SCOPE_SELECTOR1				"RT_SCOPE_SELECTOR1"					//!<
#define RT_SCOPE_SELECTOR2				"RT_SCOPE_SELECTOR2"					//!<
#define RT_RESET_PIXEL					"RT_RESET_PIXEL"						//!<
#define RT_TIME_L0L1					"RT_TIME_L0L1"							//!<
#define RT_RXREADY								"RT_RXREADY"							//!< new michele register
#define RT_RESET_BUSYRESOLVER					"RT_RESET_BUSYRESOLVER"					//!< new michele register
#define RT_TIME_BUSY_DAQ						"RT_TIME_BUSY_DAQ"						//!< new michele register
#define RT_TIME_BUSY_ROUTER						"RT_TIME_BUSY_ROUTER"					//!< new michele register
#define RT_TIME_BUSY_HS							"RT_TIME_BUSY_HS"						//!< new michele register
#define RT_TIME_BUSY_TRIGGERS_L1_FIFO			"RT_TIME_BUSY_TRIGGERS_L1_FIFO"			//!< new michele register
#define RT_NUM_TRANS_BUSY_DAQ					"RT_NUM_TRANS_BUSY_DAQ"					//!< new michele register
#define RT_NUM_TRANS_BUSY_ROUTER				"RT_NUM_TRANS_BUSY_ROUTER"				//!< new michele register
#define RT_NUM_TRANS_BUSY_HS					"RT_NUM_TRANS_BUSY_HS"					//!< new michele register
#define RT_NUM_TRANS_BUSY_TRIGGERS_L1_FIFO		"RT_NUM_TRANS_BUSY_TRIGGERS_L1_FIFO"	//!< new michele register
#define RT_MEM_COUNTERS							"RT_MEM_COUNTERS"						//!< new michele register
#define RT_ADDRESS_MEM_COUNTERS_SELECTED		"RT_ADDRESS_MEM_COUNTERS_SELECTED"		//!< new michele register
#define RT_L0_COUNTER							"RT_L0_COUNTER"							//!< new michele register

                                 		                     				  		