#pragma once
#include "StdAfx.h"
#include <dis.hxx>
#include "..\spdDbConfLib\spdLogger.h"

const int ChannelsPerSide=60;

class DimServerServiceHandler : public DimService{
	private:

		spdLogger *logger;
        DimService * SendChNumber;
		DimService * SendCommandName;
		DimService * SendErrorReportCode;
		DimService * SendErrorReport;
		DimService * SendRetData;
		DimService * SendDataStream;
		DimService * Hearthbit;

    
		char side;
		int ChannelNumber;

		char * CommandName;
        unsigned int sizeCommandName;
        
		int ErrorCode;
		char * ErrorName;
		unsigned int sizeErrorName;
		
		unsigned int * DataToSend;
        unsigned int sizeDataToSend;  

		unsigned int * DataStream;
        unsigned int sizeDataStream;

		int  heartBitTime;
		
        		 
	public:
		DimServerServiceHandler(char);
		~DimServerServiceHandler(void);
	    
		void SetChNumber(int ChNumber){ChannelNumber = ChNumber;}; //ok
		void SetCommandName(char *);									//ok					
		void SetErrorName(char *, int);                                         //ok
		void SetDataToSend(unsigned int * dttoSnd){DataToSend = dttoSnd;}; //ok
		void SetDataToSendSize(unsigned int szDtToSend){sizeDataToSend = szDtToSend;}; //ok
	    
		
		int UpdateServices();										//ok	
		int UpdateServices(int ChNumber, char * cmdName, int errCode, char * errName, unsigned int * dttoSnd, unsigned int szDtToSend); //ok

		void SendHearthbit();
		int UpdateChNumber();										//OK
		int UpdateChNumber(int );								//OK	
		int UpdateCommandName();									//ok	
		int UpdateCommandName(char *);								//ok			
		int UpdateErrorName();										//ok
		int UpdateErrorName(char *, int);								//ok	
		int UpdateDataToSend();										//ok		
		int UpdateDataToSend(unsigned int*, unsigned int);			//ok
		int UpdateDataStream();										//ok	
		int UpdateDataStream(unsigned int* dttoSnd, unsigned int szDtToSend);  //ok
		void UpdateTemperature(const double * busTemp, const double * mcmTemp);
};