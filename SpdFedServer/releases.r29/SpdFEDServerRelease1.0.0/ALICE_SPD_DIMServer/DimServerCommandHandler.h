#pragma once
#include "StdAfx.h"
#include ".\PoolingOperationControl.h"
#include "..\BitManager\BitsManage.h"
#include ".\DimServerServiceHandler.h"
#include "../HSConfig/SPDConfig.h"
#include "../HSConfig/DigitalPilot.h"


class DimServerCommandHandler : public DimServer, public ErrorHandler{
 protected:

	 //!integer dim command for the analisys tool channel number (this can be removed)
    DimCommand * cmdSetRootChNumber;
	//!integer dim command to receive the channel number for PVSS operation
	DimCommand * cmdSetChNumber;
	//!string dim commad to handle the commands received from PVSS
	DimCommand * cmdRxCommand;
	//!integer array (32 bit) dim command to receive the data for the commands 
	DimCommand * cmdDataIn;
	//!integer (32 bit) dim command for offset (not really used either)
	DimCommand * cmdSetOffset;
	
	/*! The internal member storing the jam player executable filename
	The fed server will call this file as an external executable on the program router command*/
	string jamPlayerExecutable;

	//! keeps the detector side 0 = A 1 = C
    UInt32  DetSide;
	//! class for the VME access
	VMEAccess * VME;
	DimServerServiceHandler * SRV;
    PoolingOperationControl * POOL;
    SPDConfig * SPD;
	
    UInt32 commNumb;
    Int8 RootChNumber; 
    Int8 ChNumber;
    
	UInt32 Offset;
	UInt32 dataSize;
	char RdWrSelector;

	UInt32 * DataIn;
	UInt32 * DataOut;
	UInt32 * DataStream;
	char * CommString;
	char * CommStringOriginal;	 
	
	void commandHandler();
	
	spdLogger *logger;

	/*!Variable to store the run number of a command*/
	unsigned runNumber;
	
 public: 
    DimServerCommandHandler();
	DimServerCommandHandler(VMEAccess *,  SPDConfig *, char );
	~DimServerCommandHandler(void);
    
	//UInt32 GetDataSize(void){return dataSize;};
	//!comand handler for register access in the router
	int RegisterCmdHandlerRt(void);
	//!command handler for router access in the link receiver 
	int RegisterCmdHandlerLRx(void);
	//! command handler for register access in the busy card
	int RegisterCmdHandlerBusy(void);
	//! command handler for initializing things, is this needed?!
	int InitCmdHandler(void);
	//! command handler for jtag access in the halfstaves
	int JTAGCmdHandler(void);
	//! command handler for configuyration issues (this is used for global router access also=
	int ConfCmdHandler(void);
	//! command handler for the SPD analisys tool
	int ROOTCmdHandler(void);
	//! command handler for halfstave configuration commands 
	int HSConfCmdHandler(void);
	//! scan command handler
	int ScanCmdHandler(void);

	/*!New command decoder function that will implement a new way of parsing strings
		this function will expect to receive a white space separated string
		first the command and then all the arguments ex: "command arg1 arg2 ... argn"
		the revix for this command is "STR_"
	*/
	int StringCmdHandler(void);

	/*! Setter for the jamplayer external executable filename*/
	void setJamPlayerFilename(string filename);

	int PoolingFunction(){return POOL->PoolingFunction() ;};
	void SendHearthbit(){ SRV->SendHearthbit() ;};

	/*@will get the VME adress of a router register from the command received
	@param command 
	@return the corresponding VME address returns -1 (0xffffffff) if the command is unkown*/
	UInt32 getRouterAddressFromCommand(const char * command);
	UInt32 getLrxAdressFromCommand(const char * command);
	UInt32 getBusyAddressFromCommand(const char * command);

	/*! method to be used when the number of arguments is not correct
	it will update all the DIM services informing the PVSS side of the error*/
	void argumentError();


};
