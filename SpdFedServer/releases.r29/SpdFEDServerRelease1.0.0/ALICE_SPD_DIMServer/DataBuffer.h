#pragma once

struct EventStruct{
	UInt32 * Data; 
    EventStruct * NextEvent;
};




class DataBuffer{
	EventStruct * FirstEvent;
	EventStruct * ActualEvent;
	EventStruct * LastEvent;

    UInt32 EventInBuffer;
	bool AutoDelete;
	//bool AppendEvents;
	UInt32 MaxEventsToAppend;
	
public:
	DataBuffer(void);
	~DataBuffer(void);

	UInt32 AppendEvent(UInt32 *);
	UInt32 DeleteEvent(void);
    UInt32 DeleteEvent(UInt32 evNumber);          
	UInt32 ClearRedEvent(void);
	UInt32 ClearBuffer(void);
    UInt32* GetNextEvent(void);

	void SetAutoDelete(bool val){AutoDelete = val;};
	bool GetAutoDelete(void){return AutoDelete;};

	//void SetAppendEvents(bool val){AppendEvents = val;};
	//bool GetAppendEvents(void){return AppendEvents;};
	
	void SetMaxEventsToAppend(UInt32 val){MaxEventsToAppend = val;};
	UInt32 GetMaxEventsToAppend(void){return MaxEventsToAppend;};

	UInt32 GetEventInBuffer(void){return EventInBuffer;};
};
