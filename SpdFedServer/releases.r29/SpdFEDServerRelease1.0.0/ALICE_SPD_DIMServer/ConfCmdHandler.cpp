#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"
#include ".\conf_cmd_def.h"

int DimServerCommandHandler::ConfCmdHandler(void){
    if(strncmp(CommString, "CNF_", 4) != 0) return 2;

// this will make by default to update the dim service for data out to send 0 bytes 
		// and the the operation successed 
	int dataOutSize = 0;
    status = 0;
	   	
	// updates the activation status of all halfstaves
	if(strcmp(CommString, CNF_CHSTATUS_ALL) == 0){
      
		const unsigned numberOfImputs=120;

		if (this->dataSize < numberOfImputs){
			this->argumentError();
			return 1;
		}

		for(unsigned i=0; i < numberOfImputs; i++){
			if((DetSide == 0 && i < 60) || (DetSide == 1 && i >= 60) ){
				SPD->getHS(i).setChActStatus((UInt8) ((DataIn[i] >> 10) & 0x7));
			}
		}


	}
	// updates the activation status of only one halfstave
	else if(strcmp(CommString, CNF_CHSTATUS_CH) ==0){

		SPD->getHS(ChNumber).setChActStatus( (UInt8)((DataIn[0] >> 10) & 0x7) );

	}
	// starts the temperature pooling
	else if(strcmp(CommString, CNF_PL_TEMPERATURE_ON) == 0){
		POOL->startHSTempPool(DataIn[0]);
	} 
	//stops the temperature pooling
	else if(strcmp(CommString, CNF_PL_TEMPERATURE_OFF) == 0){ 
		POOL->stopHSTempPool();		
	} 
	// starts the pooling of router errors
	else if(strcmp(CommString, CNF_PL_RT_ERRORS_START) == 0){ 
		
		logger->log("starting router error pooling, refresh rate = %d (ms)", DataIn[0]);
		POOL->startRouterErrorPooling( DataIn[0]);
	}
	// stops the pooling of router errors
	else if(strcmp(CommString, CNF_PL_RT_ERRORS_STOP) == 0){ 
		logger->log("stopping router error pooling", DataIn[0]);
		POOL->stopRouterErrorPooling();
	}
	// gets the start of run information
	else if(strcmp(CommString, CNF_RUN_START) == 0){
		logger->log("start of run received for run %d ",  DataIn[0]);
		this->POOL->setRunNumber(DataIn[0]);
	} 
	// gets the stop of run information
	else if(strcmp(CommString, CNF_RUN_STOP) == 0){ 
		
		logger->log("stop of run received for run %d ",  POOL->getRunNumber());
		this->POOL->setRunNumber(0);		

	} 
	//reads the router errors only in one router
	else if (strcmp(CommString, CNF_READ_ROUTER_ERRORS) == 0){ 
		vector<SpdRouter::RouterError> errorList;

		spdDbConnection *conn= spdDbConnection::subscribe();
		conn->connect();

		unsigned routerNumber = ChNumber/6;

	
		errorList = SPD->getRouter( routerNumber).readErrorList();

		for (unsigned error = 0 ;  error < errorList.size() ; error++){
			this->POOL->insertRouterErrorInTheDB(errorList[error], routerNumber );
		}
		
		conn->commit();
		conn->disconnect();
	}
	// sets the mcm stimuly only in one channel
	else if(strcmp(CommString, CNF_MCMSTIMULI_ON) == 0){
		
		SPD->setMCMStimuli(ChNumber, 1, DataIn[0]);
	}
	//resets the mcm stimutli only in one channel
	else if(strcmp(CommString, CNF_MCMSTIMULI_OFF) == 0){
		SPD->setMCMStimuli(ChNumber, 0, DataIn[0]);
	
	}
	// sets the mcm stimuli in all enabled channels
	else if(strcmp(CommString, CNF_MCMSTIMULI_ON_ALL) == 0){
		SPD->setMCMStimuliToAll(1, DataIn[0]);
	}
	// resets the mcmc stimuli in all channels
	else if(strcmp(CommString, CNF_MCMSTIMULI_OFF_ALL) == 0){
		SPD->setMCMStimuliToAll(0, DataIn[0]);
	}
	// configures one router (control reg. L0L1 times and L1 FO delays)
	else if(strcmp(CommString, CNF_AUTO_CONF_ROUTER) == 0){

		const unsigned numberOfImputs=9;
		if (this->dataSize < numberOfImputs){
			this->argumentError();
			return 1;
		}

		SPD->getRouter(ChNumber/6).configureRouter(DataIn[0],	// options
													DataIn[1],	// control reg
													DataIn[2],	//L0L1 time
													&DataIn[3]); // L1 fastor delays array

	}
	// reads all busy information in one router
	else if(strcmp(CommString, CNF_READ_BUSY_ROUTER) == 0){
		
		dataOutSize = 4;
		delete[] DataOut;
		DataOut = new UInt32 [dataOutSize];

		SPD->getRouter(ChNumber/6).readRouterBusyTimes( DataOut);
	}
	else{
		sprintf(OutMsgText, "ERROR: CONF command %s not ricognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}

	// here all services are updated for PVSS for any command
	SRV->UpdateServices(ChNumber, 
						CommStringOriginal, 
						status, 
						CheckStatus(CommStringOriginal,status, ChNumber),
						(unsigned int*)DataOut, 
						dataOutSize);    
	
	return 0; 
}