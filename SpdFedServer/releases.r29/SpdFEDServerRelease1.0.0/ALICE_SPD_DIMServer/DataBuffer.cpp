#include "StdAfx.h"
#include ".\databuffer.h"

DataBuffer::DataBuffer(void){
	FirstEvent = NULL;
	ActualEvent = NULL;
	LastEvent = NULL;
	AutoDelete = true;
//	AppendEvents = FALSE;
	MaxEventsToAppend = 1;
	EventInBuffer = 0;
	
}

DataBuffer::~DataBuffer(void){
}

UInt32 DataBuffer::AppendEvent(UInt32 * Event){
	EventStruct* newstruct = new EventStruct; 
	if(newstruct == NULL) return 1;
	if(FirstEvent == NULL){
		FirstEvent = newstruct;
		ActualEvent = FirstEvent;
	} else{
		LastEvent->NextEvent = newstruct;	
	}
	if(ActualEvent == NULL){
		ActualEvent = newstruct;
	}

	newstruct->NextEvent = NULL;
    newstruct->Data = Event;
	LastEvent  = newstruct;
	EventInBuffer++;
	return 0;
}

UInt32* DataBuffer::GetNextEvent(void){
	if(FirstEvent == NULL || ActualEvent == NULL) return NULL;
	UInt32 * data;
	data = ActualEvent->Data;
    ActualEvent = ActualEvent->NextEvent;
	return data;
}


UInt32 DataBuffer::DeleteEvent(void){

	if(FirstEvent == NULL) return 1;
	if(FirstEvent == ActualEvent) return 2;

	EventStruct * tempAddr;
    tempAddr = FirstEvent;
	FirstEvent = FirstEvent->NextEvent;

	if (tempAddr->Data !=NULL) delete [] tempAddr->Data;
	if (tempAddr != NULL) delete [] tempAddr;

	if(EventInBuffer > 0) EventInBuffer--;
	return 0;
}

UInt32 DataBuffer::DeleteEvent(UInt32 evNumber){
	if(FirstEvent ==NULL) return 1;
	if(evNumber == 0) return DeleteEvent();

	EventStruct * tempAddr, * tempAddrNext, * tempAddrLast;
	tempAddrLast = FirstEvent;
	for(UInt32 i =0; i< evNumber-1; i++){
		tempAddrLast = tempAddrLast->NextEvent;		
		if(tempAddrLast == NULL) return 2;
	}

	tempAddr = tempAddrLast->NextEvent;
	tempAddrNext = tempAddr->NextEvent; 
	tempAddrLast->NextEvent = tempAddrNext;
	
	if (tempAddr->Data  != NULL) delete [] tempAddr->Data;
	if (tempAddr != NULL) delete [] tempAddr;
	

    if(EventInBuffer > 0) EventInBuffer--;
	return 0;
}

UInt32 DataBuffer::ClearRedEvent(void){
	while(DeleteEvent() != 2){}
	return 0;
}


UInt32 DataBuffer::ClearBuffer(void){
	ActualEvent =NULL;
	while(FirstEvent != NULL){
		DeleteEvent();
	}
	EventInBuffer = 0;
	return 0;
}