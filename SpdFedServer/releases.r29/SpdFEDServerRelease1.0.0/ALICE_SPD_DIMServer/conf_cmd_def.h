#define CNF_CHSTATUS_ALL			"CNF_CHSTATUS_ALL"			//!< command to refresh the status of all half staves @param array with 120 values
#define CNF_CHSTATUS_CH				"CNF_CHSTATUS_CH"			//!< command to refresh the status of one half stave	 @param value

#define CNF_PL_TEMPERATURE_ON		"CNF_PL_TEMPERATURE_ON"		//!< command to start pooling the temperature @param refresh time (ms)
#define CNF_PL_TEMPERATURE_OFF		"CNF_PL_TEMPERATURE_OFF"	//!< command to stop the ooling of the temperature


#define CNF_MCMSTIMULI_ON			"CNF_MCMSTIMULI_ON"			//!< command to set the mcm stimuli on in one channel  @param channelNumber, columnToSet
#define CNF_MCMSTIMULI_OFF			"CNF_MCMSTIMULI_OFF"		//!< command to clear the mcm stimuli in one channel  @param channelNumber, columnToSet
#define CNF_MCMSTIMULI_ON_ALL		"CNF_MCMSTIMULI_ON_ALL"		//!< command to set the mcm stimuli on  in all enabled channels @param columnToSet
#define CNF_MCMSTIMULI_OFF_ALL		"CNF_MCMSTIMULI_OFF_ALL"	//!< command to clear the mcm stimuli in all enabled channels @param  columnToSet


#define CNF_PL_RT_ERRORS_START		"CNF_PL_RT_ERRORS_START"    //!< Starts pooling the router memory for errors 
#define CNF_PL_RT_ERRORS_STOP		"CNF_PL_RT_ERRORS_STOP"     //!< Stops pooling the router memory for errors 

#define CNF_RUN_START				"CNF_RUN_START"				//!< start of run commmand to initialize the run number variable
#define CNF_RUN_STOP				"CNF_RUN_STOP"				//!< Stop of run variable to clean the run number variable
#define CNF_READ_ROUTER_ERRORS		"CNF_READ_ROUTER_ERRORS"     //!< Reads the error list in one router
#define CNF_AUTO_CONF_ROUTER		"CNF_AUTO_CONF_ROUTER"		//!< Auto configures the router @param options, controlReg, L0L1Time, LrxL1FastorDelay0, LrxL1FastorDelay1, LrxL1FastorDelay2

#define CNF_READ_BUSY_ROUTER		"CNF_READ_BUSY_ROUTER"		//!< reads the router busy times and sends them all to PVSS, 0 = busy daq, 1= busy router, 2 = busy HS, 3 = busy trigger 
