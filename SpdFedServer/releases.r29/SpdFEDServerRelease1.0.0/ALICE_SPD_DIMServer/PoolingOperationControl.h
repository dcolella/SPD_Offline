#pragma once
#include ".\DataBuffer.h"
#include "../HSConfig/SPDConfig.h"
#include ".\DimServerServiceHandler.h"


#include "../spdDbConfLib/spdLogger.h"


class PoolingOperationControl{	
	DimServerServiceHandler * SRV;
	SPDConfig  * SPD;

	spdLogger *log;
    
	unsigned TempRefreshTime;     //number of ms


	bool TemperaturePoolActive;
	bool routerErrorPoolActive;
    
	UInt32 TempStartTime;

	//!variable storing the time when the last time there was a router error read
	UInt32 RouterErrorTime;

	//! variable storing the refresh time, the time in ms that we want to wait between erading of the router errors 
	UInt32 RouterRefreshTime;


	unsigned runNumber;
	
public:


	//! function to read the router control register 
	UInt32 readControlReg();

	//! method to read all routers get errors and store them in the database
	void readRouterErrors();

	void setRunNumber(unsigned value){runNumber = value;};
	unsigned getRunNumber(){return runNumber;};


	PoolingOperationControl(SPDConfig *, DimServerServiceHandler *);
	~PoolingOperationControl(void);
    

    void SetTempRefreshTime(UInt32 value){TempRefreshTime = value;};
	
	//! sets the refresh time for the router error pooling
	void setRouterErrorRefreshTime(UInt32 value){RouterRefreshTime = value;};


	/*!Starts the router error pooling*/
	void startRouterErrorPooling(unsigned refreshTime = 10000);

	/*!Stops the router error pooloing*/
	void stopRouterErrorPooling(){routerErrorPoolActive = false;};

	//! inserts one router error in the database
	void insertRouterErrorInTheDB(SpdRouter::RouterError error, unsigned  routerNumber);


	void startHSTempPool(unsigned refreshTime = 1000){
		TempRefreshTime=refreshTime;
		TemperaturePoolActive = true;
		TempStartTime = GetTickCount();
	};

	void stopHSTempPool(){TemperaturePoolActive=false;};
    bool isTempPoolActive(void){return TemperaturePoolActive;};


	UInt32 PoolingFunction(void);


};
