#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"


int DimServerCommandHandler::RegisterCmdHandler(void){ 
	
	if(!(strncmp(CommString, "R_", 2)== 0) && !(strncmp(CommString, "W_", 2)==0)){
		return 2;
	}
	UInt32 Aux =0;
	RdWrSelector = CommString[0];
	CommString += 2;
		
	//LinkRx Commands
	//-------------------------------------------------------------
	if(strncmp(CommString, "LRX_", 4)==0){
        CommString += 4; 
		if(strcmp(CommString, "DPM") == 0){
			BaseAddr = VME->AG->GetLRxDPMBaseAddr(ChNumber);	              	
		}else if(strcmp(CommString, "CONTROL") == 0){
			BaseAddr = VME->AG->GetLRxCntrRegAddr(ChNumber);
		}else if(strcmp(CommString, "STATUS") == 0){
            BaseAddr = VME->AG->GetLRxStatusRegAddr(ChNumber);	
		}else if(strcmp(CommString, "TEMPERATURE") == 0){
			BaseAddr = VME->AG->GetLRxTempAddr(ChNumber);
		}else if(strcmp(CommString, "CLK_DELAY") == 0){
			BaseAddr = VME->AG->GetLRxClkDelayAddr(ChNumber);	
		}else if(strcmp(CommString, "DATA_DELAY") == 0){
			BaseAddr = VME->AG->GetLRxDataDelayAddr(ChNumber);
		}else if(strcmp(CommString, "DPM_ADDRESS_COUNTER") == 0){
			BaseAddr = VME->AG->GetLRxDPMAddrCountAddr(ChNumber);
		}else if(strcmp(CommString, "MASK_COLUMN") == 0){
			BaseAddr = VME->AG->GetLRxColumMaskAddr(ChNumber);
		}else if(strcmp(CommString, "RESET") == 0){
			BaseAddr = VME->AG->GetLRxResetAddr(ChNumber);
		}else if(strcmp(CommString, "SOFT_RESET") == 0){
			BaseAddr = VME->AG->GetLRxResetSoftAddr(ChNumber);
		}else if(strcmp(CommString, "FLUSH") == 0){
			BaseAddr = VME->AG->GetLRxFlushAddr(ChNumber);
		}else if(strcmp(CommString, "TEST_REG") == 0){
			BaseAddr = VME->AG->GetLRxTestRegAddr(ChNumber);
		}else if(strcmp(CommString, "MCM_STIMULI") == 0){
			BaseAddr = VME->AG->GetLRxTestStimAddr(ChNumber);
		}else if(strcmp(CommString, "BUSY_MASK") == 0){
			BaseAddr = VME->AG->GetLRxBusyMaskAddr(ChNumber);
		}else if(strcmp(CommString, "HISTOGRAM") == 0){
			BaseAddr = VME->AG->GetLRxHistAddr(ChNumber);
		}else if(strcmp(CommString, "ERROR_COUNTER") == 0){
			BaseAddr = VME->AG->GetLRxRxErrorCount(ChNumber);
		}else if(strcmp(CommString, "L1_MEM_POINTER") == 0){
			BaseAddr = VME->AG->GetLRxL1MemPointAddr(ChNumber);
		}else if(strcmp(CommString, "L1_ROUT_COUNT") == 0){
			BaseAddr = VME->AG->GetLRxL1RoutCountAddr(ChNumber);
		}else if(strcmp(CommString, "BUSY_CONTROL") == 0){
			BaseAddr = VME->AG->GetLRxBusyControlAddr(ChNumber);
		}else if(strcmp(CommString, "SHIFT_SEL") == 0){
			BaseAddr = VME->AG->GetLRxShiftSelAddr(ChNumber);
		}else if(strcmp(CommString, "EN_MOVEBIT") == 0){
			BaseAddr = VME->AG->GetLRxEnMoveBitAddr(ChNumber);
		}else if(strcmp(CommString, "EV_START") == 0){
			BaseAddr = VME->AG->GetLRxEventStartAddr(ChNumber);
		}else if(strcmp(CommString, "EV_END") == 0){
			BaseAddr = VME->AG->GetLRxEventEndAddr(ChNumber);
		}else if(strcmp(CommString, "MEM_START") == 0){
			BaseAddr = VME->AG->GetLRxL1MemStartAddr(ChNumber);
		}else if(strcmp(CommString, "MEM_END") == 0){
			BaseAddr = VME->AG->GetLRxL1MemEndAddr(ChNumber);
		}else if(strcmp(CommString, "FORMAT_ERROR") == 0){
			BaseAddr = VME->AG->GetLRxErrorFormatAddr(ChNumber);
		}else if(strcmp(CommString, "INPUT_ST_STATUS0") == 0){
			BaseAddr = VME->AG->GetLRxInputStageStatus0Addr(ChNumber);
		}else if(strcmp(CommString, "INPUT_ST_STATUS1") == 0){
			BaseAddr = VME->AG->GetLRxInputStageStatus1Addr(ChNumber);
		}else if(strcmp(CommString, "EV_DESCR_RAM_STATUS") == 0){
			BaseAddr = VME->AG->GetLRxEvDescrRamStatusAddr(ChNumber);
		}else if(strcmp(CommString, "FIFO_PIX_DATA_STATUS") == 0){
			BaseAddr = VME->AG->GetLRxFIFOPixDataStatusAddr(ChNumber);
		}else if(strcmp(CommString, "FIFO_EV_DATA_STATUS") == 0){
			BaseAddr = VME->AG->GetLRxFIFOEvDataStatusAddr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS0") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus0Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS1") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus1Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS2") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus2Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS3") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus3Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS4") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus4Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS5") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus5Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS6") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus6Addr(ChNumber);
		}else if(strcmp(CommString, "DATA_ENC_STATUS7") == 0){
			BaseAddr = VME->AG->GetLRxDataEncoderStatus7Addr(ChNumber);
		}else{
			sprintf(OutMsgText, "ERROR: LinkRx command %s not ricognized\n", CommStringOriginal);
			printf(OutMsgText);
			SRV->UpdateErrorName(OutMsgText, 1);
			return 1;
		}
	}
    	
    
	//Router Commands
	//-------------------------------------------------------------
	else if(strncmp(CommString, "RT_", 3)==0){
		CommString += 3; 
		if(strcmp(CommString, "DPM") == 0){
			BaseAddr = VME->AG->GetRoutDPMBaseAddr(ChNumber);          	
		}else if(strcmp(CommString, "SPM") == 0){
			BaseAddr = VME->AG->GetRoutSPMBaseAddr(ChNumber);
		}else if(strcmp(CommString, "LRX_MEM_BASEADDR") == 0){
			BaseAddr = VME->AG->GetRoutLinkRxBaseAddr(ChNumber);
		}else if(strcmp(CommString, "CONTROL") == 0){
			BaseAddr = VME->AG->GetCntrRegAddr(ChNumber);
		}else if(strcmp(CommString, "STATUS1") == 0){
			BaseAddr = VME->AG->GetStatusReg1Addr(ChNumber);
		}else if(strcmp(CommString, "STATUS2") == 0){
			BaseAddr = VME->AG->GetStatusReg2Addr(ChNumber);
		}else if(strcmp(CommString, "STATUS3") == 0){
			BaseAddr = VME->AG->GetStatusReg3Addr(ChNumber);
        }else if(strcmp(CommString, "STATUS_LINKRX0") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 0);
		}else if(strcmp(CommString, "STATUS_LINKRX1") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 1);
		}else if(strcmp(CommString, "STATUS_LINKRX2") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 2);			
		}else if(strcmp(CommString, "STATUS_LINKRX3") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 3);			
		}else if(strcmp(CommString, "STATUS_LINKRX4") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 4);			
		}else if(strcmp(CommString, "STATUS_LINKRX5") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 5);			
		}else if(strcmp(CommString, "STATUS_LINKRX6") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 6);
		}else if(strcmp(CommString, "STATUS_LINKRX7") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 7);
		}else if(strcmp(CommString, "STATUS_LINKRX8") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 8);
		}else if(strcmp(CommString, "STATUS_LINKRX9") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 9);
		}else if(strcmp(CommString, "STATUS_LINKRX10") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 10);
		}else if(strcmp(CommString, "STATUS_LINKRX11") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 11);
		}else if(strcmp(CommString, "STATUS_LINKRX12") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 12);
		}else if(strcmp(CommString, "STATUS_LINKRX13") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 13);
		}else if(strcmp(CommString, "STATUS_LINKRX14") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 14);
		}else if(strcmp(CommString, "STATUS_LINKRX15") == 0){
			BaseAddr = VME->AG->GetStatusRegLinkRxAddr(ChNumber, 15);
		}else if(strcmp(CommString, "WRHISTO") == 0){
			BaseAddr = VME->AG->GetWtHistogramAddr(ChNumber);
        }else if(strcmp(CommString, "RDHISTO") == 0){
			BaseAddr = VME->AG->GetRdHistogramAddr(ChNumber);
		}else if(strcmp(CommString, "JPLAYER") == 0){
			BaseAddr = VME->AG->GetJPlayerAddr(ChNumber);
		}else if(strcmp(CommString, "L0ID") == 0){
			BaseAddr = VME->AG->GetRdL0IdAddr(ChNumber);
		}else if(strcmp(CommString, "L2ID") == 0){
			BaseAddr = VME->AG->GetRdL2aIdAddr(ChNumber);
		}else if(strcmp(CommString, "RSTBNC") == 0){
			BaseAddr = VME->AG->GetResetBcntAddr(ChNumber);
		}else if(strcmp(CommString, "IRQPUSHB") == 0){
			BaseAddr = VME->AG->GetIrqPushButtonAddr(ChNumber);
		}else if(strcmp(CommString, "RST_TEMPLIM") == 0){
		    BaseAddr = VME->AG->GetResetTempLimitAddr(ChNumber);
		}else if(strcmp(CommString, "FO_FROMVME") == 0){
			BaseAddr = VME->AG->GetFOFromVMEAddr(ChNumber);
		}else if(strcmp(CommString, "VMERESET") == 0){
			BaseAddr = VME->AG->GetRtVMEResetAddr(ChNumber);
		}else if(strcmp(CommString, "STATUS_JTAG_SELECT") == 0){
			BaseAddr = VME->AG->GetStatusRegJTAGSelAddr(ChNumber);
		}else if(strcmp(CommString, "DATA_SELECT") == 0){
			BaseAddr = VME->AG->GetDataRegSelAddr(ChNumber);
		}else if(strcmp(CommString, "SELECT") == 0){
			BaseAddr = VME->AG->GetRegSelAddr(ChNumber);
		}else if(strcmp(CommString, "DDL_STATUS") == 0){
			BaseAddr = VME->AG->GetFeDDLStatusWordAddr(ChNumber);
		}else if(strcmp(CommString, "RESET_DETECTOR") == 0){
			BaseAddr = VME->AG->GetResetDetectorAddr(ChNumber);
		}else if(strcmp(CommString, "RD_EV_START") == 0){
			BaseAddr = VME->AG->GetRdStartAddr(ChNumber);
		}else if(strcmp(CommString, "EV_LENGTH_OF_BLOCK") == 0){
			BaseAddr = VME->AG->GetRdLEnghtOfBlockAddr(ChNumber);
		}else if(strcmp(CommString, "RESET_TTC") == 0){
			BaseAddr = VME->AG->GetResetTTCrxAddr(ChNumber);
		}else if(strcmp(CommString, "RESET_LRX") == 0){
			BaseAddr = VME->AG->GetResetLinkRxAddr(ChNumber);
		}else if(strcmp(CommString, "RESET_HS") == 0){
			BaseAddr = VME->AG->GetResetHalfStaveAddr(ChNumber);
		}else if(strcmp(CommString, "SEND_TRIGGER") == 0){
			BaseAddr = VME->AG->GetSendTriggSeqAddr(ChNumber);
		}else if(strcmp(CommString, "FIFO_STARTADDR") == 0){
			BaseAddr = VME->AG->GetDPMfifoStartAddr(ChNumber);
		}else if(strcmp(CommString, "FIFO_ENDADDR") == 0){
			BaseAddr = VME->AG->GetDPMfifoEndAddr(ChNumber);
		}else if(strcmp(CommString, "FLUSH_DPM") == 0){
			BaseAddr = VME->AG->GetFlushDPMAddr(ChNumber);
		}else if(strcmp(CommString, "L1_ID") == 0){
			BaseAddr = VME->AG->GetRdL1IdAddr(ChNumber);
		}else if(strcmp(CommString, "FO_NUMBER") == 0){
			BaseAddr = VME->AG->GetRdFONumbAddr(ChNumber);
		}else if(strcmp(CommString, "TEMPERATURE") == 0){
			BaseAddr = VME->AG->GetRdTempChAddr(ChNumber);
		}else if(strcmp(CommString, "JT_RESET_STMACHINE") == 0){
	        BaseAddr = VME->AG->GetJTResetStateMacAddr(ChNumber);      	
		}else if(strcmp(CommString, "JT_RESET_FIFO") == 0){
			BaseAddr = VME->AG->GetJTResetFIFOsAddr(ChNumber);
		}else if(strcmp(CommString, "JT_RDWR_DATA") == 0){
			BaseAddr = VME->AG->GetJTRdWrDataAddr(ChNumber);
		}else if(strcmp(CommString, "JT_EXEC_START") == 0){
			BaseAddr = VME->AG->GetJTExStartAddr(ChNumber);
		}else if(strcmp(CommString, "JT_STATUS") == 0){
			BaseAddr = VME->AG->GetJTStatusRegAddr(ChNumber);
		}else if(strcmp(CommString, "JT_RESET_CH") == 0){
			BaseAddr = VME->AG->GetJTResetChAddr(ChNumber);
		}else if(strcmp(CommString, "JT_RDEN_FIFOIN") == 0){
			BaseAddr = VME->AG->GetJTRdEnFIFOin(ChNumber);
		}else if(strcmp(CommString, "JT_RDNUMB_FIFOIN") == 0){ 
			BaseAddr = VME->AG->GetJTRdNumbFIFOin(ChNumber);
		}else if(strcmp(CommString, "TEMPERATURE_LIMIT_MCM") == 0){
			BaseAddr = VME->AG->GetTempLimitMcmAddr(ChNumber);
		}else if(strcmp(CommString, "TEMPERATURE_LIMIT_BUS") == 0){
			BaseAddr = VME->AG->GetTempLimitBusAddr(ChNumber);
		}else if(strcmp(CommString, "FO_GLOBAL_COUNTER") == 0){
			BaseAddr = VME->AG->GetFOGlobalCountAddr(ChNumber);
		}else if(strcmp(CommString, "FO_COINCIDENCE_COUNTER") == 0){
			BaseAddr = VME->AG->GetFOCoincedenceCountAddr(ChNumber);
        }else if(strcmp(CommString, "FO_TIME_COUNTER") == 0){
			BaseAddr = VME->AG->GetFOTimeCountAddr(ChNumber);
		}else if(strcmp(CommString, "FO_LINKRX_COUNTER") == 0){
			BaseAddr = VME->AG->GetFOLinkRxCountAddr(ChNumber);
		}else if(strcmp(CommString, "SCOPE_SELECTOR0") == 0){
			BaseAddr = VME->AG->GetScopeSelectorAddr(ChNumber, 0);
		}else if(strcmp(CommString, "SCOPE_SELECTOR1") == 0){
			BaseAddr = VME->AG->GetScopeSelectorAddr(ChNumber, 1);
		}else if(strcmp(CommString, "SCOPE_SELECTOR2") == 0){
			BaseAddr = VME->AG->GetScopeSelectorAddr(ChNumber, 2);
		}else{	
			sprintf(OutMsgText, "ERROR: Router command %s not ricognized\n", CommStringOriginal);
			printf(OutMsgText);
			SRV->UpdateErrorName(OutMsgText, 1);
			return 1;
		}
	}


	//BUSY CARDS Commands
	//-------------------------------------------------------------
	else if(strncmp(CommString, "BSYCD_", 6)==0){
        CommString += 6; 
		if(strcmp(CommString, "RESET") == 0){
			BaseAddr = VME->AG->GetBsyVMEResetAddr(ChNumber);	              	
		}else if(strcmp(CommString, "VERSION") == 0){
			BaseAddr = VME->AG->GetBsyVersionNumberAddr(ChNumber);
		}else if(strcmp(CommString, "DRVREC") == 0){
			BaseAddr = VME->AG->GetBsyEnDriverOrReceiverAddr(ChNumber);	
		}else if(strcmp(CommString, "L0DELAY") == 0){
			BaseAddr = VME->AG->GetBsyL0DelayAddr(ChNumber, DataIn[0]);
            DataIn++;
			dataSize--;
		}else if(strcmp(CommString, "CONTROL") == 0){
			BaseAddr = VME->AG->GetBsyControlRegAddr(ChNumber);	
		}else if(strcmp(CommString, "BUSYMASK") == 0){
			BaseAddr = VME->AG->GetBsyBusyMaskAddr(ChNumber);
		}else if(strcmp(CommString, "L0COUNTER") == 0){
			BaseAddr = VME->AG->GetBsyL0CounterAddr(ChNumber);
		}else if(strcmp(CommString, "INPUTS") == 0){
			BaseAddr = VME->AG->GetBsyReadInputAddr(ChNumber);
		}else if(strcmp(CommString, "CNCOUNTERS") == 0){
			BaseAddr = VME->AG->GetBsyCnCounterAddr(ChNumber, DataIn[0]);
			DataIn++;
			dataSize--;
		}else{
			sprintf(OutMsgText, "ERROR: BusyCard command %s not ricognized\n", CommStringOriginal);
			printf(OutMsgText);
			SRV->UpdateErrorName(OutMsgText, 1);
			return 1;
		}
	}
	else{
		sprintf(OutMsgText, "ERROR: %s is not a valid VME access command\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}
    
	BaseAddr += Offset;                //add protection: if it is register Offsett must be 0
	
	if(RdWrSelector == 'R'){
		dataSize = 1;
		delete[] DataOut;
		DataOut = new UInt32 [dataSize];
		status = VME->VMERegisterAcc(&BaseAddr, DataOut, dataSize, &RdWrSelector);
		SRV->UpdateServices(ChNumber, CommStringOriginal, (int)status, CheckStatus(CommStringOriginal, status),(unsigned int*)DataOut, dataSize);
		cout << "Address " << hex << BaseAddr << " DataOut " << hex << DataOut[0] << endl;
    }else{ 
		status = VME->VMERegisterAcc(&BaseAddr, DataIn, dataSize, &RdWrSelector);
		SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status), (int)status);
	}
	
	return 0; 
}