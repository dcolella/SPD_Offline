#define HSCNF_DB_DIFF_COUNT			"HSCNF_DB_DIFF_COUNT"	//!< command to count the diferences between the data in the default containers and actual containers in the fed @return McmDifferences, DacDifferences, NoisyDifferences
#define HSCNF_DB_COMPARE			"HSCNF_DB_COMPARE"		//!< command to compare the data in the default containers and actual containers in the fed and save it to the temporary table in the database 
#define HSCNF_CONFIGURE_DEF			"HSCNF_CONFIGURE_DEF"	//!< command to configure an halfstave with the default values, DPI, API and DACS @return 1 if it fails setting the digital pilot, 2 if it fails the analog pilot setting, 3 if it fails the pixel dacs settings
#define HSCNF_CONFIGURE_DEF_ALL		"HSCNF_CONFIGURE_DEF_ALL"	//!< command to configure all of the halfstaves with the default values @return an array with 60 elements each one with the status of the configuration (1- dpi problem, 2 api problem, 3 pix dac problem, -1 if the channel was skipped)


#define HSCNF_JTAG_RESET			"HSCNF_JTAG_RESET"		//!< performs a jtag reset in the halfstave, its a IR scan only with IR code 0xb for the pixel chips
#define HSCNF_API__SETDAC			"HSCNF_API__SETDAC"		//!< command to set the dacs in the analog pilot 
#define HSCNF_API__DACDEF			"HSCNF_API__DACDEF"		//!< command to set the dacs in the analog pilot with the default values  
#define HSCNF_API__READ				"HSCNF_API__READ"		//!< command to read the dacs in the analog pilot 
#define HSCNF_API__READADC			"HSCNF_API__READADC"	//!< command to read the adcs in the analog pilot 
#define HSCNF_API__SET_ONE_DAC		"HSCNF_API__SET_ONE_DAC"	//!< command to set only one dac of the analog pilot @param dac number (follows the same order as in the jtag scan) @param dac value

#define HSCNF_DPI_SET				"HSCNF_DPI_SET"			//!< command to set the digital pilot settings
#define HSCNF_DPI_DEF				"HSCNF_DPI_DEF"			//!< command to set the digital pilot settings with the default values
#define HSCNF_DPI_READ				"HSCNF_DPI_READ"		//!< command to read the digital pilot settings
#define HSCNF_DPI_INT_SET			"HSCNF_DPI_INT_SET"		//!< command to set the digital pilot internal settings
#define HSCNF_DPI_INT_DEF			"HSCNF_DPI_INT_DEF"		//!< command to set the digital pilot internal settings with the default values
#define HSCNF_DPI_INT_DEF_ALL		"HSCNF_DPI_INT_DEF_ALL" //!< command to set the digital pilot internal settings with the default values in all enabled halfstaves

#define HSCNF_DATA_RESET_ALL		"HSCNF_DATA_RESET_ALL" //!< command to set the digital pilot internal settings with the default values in all enabled halfstaves

#define HSCNF_PIXMTX_SET			"HSCNF_PIXMTX_SET"			//!< command to set the matrix settings
#define HSCNF_PIXMASK_DEF			"HSCNF_PIXMASK_DEF"			//!< command to set the matrix settings with the values from the database
#define HSCNF_PIX_UNMASK_ALL		"HSCNF_PIX_UNMASK_ALL"		//!< command to unmask all pixel chips


#define HSCNF_PXDAC_DEFAULT			"HSCNF_PXDAC_DEFAULT"	//!< command to set the pixel dacs with the defualt values
#define HSCNF_PXDAC_ALL				"HSCNF_PXDAC_ALL"		//!< command to set all pixel dacs in one hs
#define HSCNF_PXDAC_CH				"HSCNF_PXDAC_CH"		//!< command to set only one pixel dac in one hs
#define HSCNF_READ_PXDAC			"HSCNF_READ_PXDAC"		//! command to read the one pixel dac for one all 10 pixel chips of one channel @param 

#define HSCNF_DB_GET				"HSCNF_DB_GET"					//!< command to get the current configuration of the detector from the database
#define HSCNF_DB_SET				"HSCNF_DB_SET"					//!< command to save the current configuration of the detector in the database
#define HSCNF_DB_LINK				"HSCNF_DB_LINK"					//!< command to link the two side versions of each fed in the global version in the database
#define HSCNF_DB_GET_SIDE_VERSION	"HSCNF_DB_GET_SIDE_VERSION"		//!< command to return the current database side version assigned to this fed server
#define HSCNF_DB_GET_GLOBAL_VERSION	"HSCNF_DB_GET_GLOBAL_VERSION"	//!< command to return the current database global version loaded in the FED
#define HSCNF_DB_MASK				"HSCNF_DB_MASK"					//!< command to mask the full detector with the data from the database \param versionNumber
#define HSCNF_DB_GET_MASK_VERSION	"HSCNF_DB_GET_MASK_VERSION"		//!< command to return the current mask (noisy pixels) version assigned to this fed server


#define HSCNF_ACTUALS_TO_FILE	"HSCNF_ACTUALS_TO_FILE"				//!< command to dump all actual settings to file, it will generate 60 files in the folder '\DumpConf' with the name actualHS001.txt, actualHS002 etc
#define HSCNF_DEFAULTS_TO_FILE	"HSCNF_DEFAULTS_TO_FILE"			//!< command to dump all default settings to file, it will generate 60 files in the folder '\DumpConf' with the name defaultHS001.txt, actualHS002 etc


#define HSCNF_FILE_GET				"HSCNF_FILE_GET"				//!< command the read a file for a certain channel
#define HSCNF_FILE_GETNAME			"HSCNF_FILE_GETNAME"			//!< command to return the file name assigned to a certain channel
#define HSCNF_FILE_REFRESH			"HSCNF_FILE_REFRESH"			//!< command to reload a file for a certain channel
#define HSCNF_FILE_LOAD				"HSCNF_FILE_LOAD"				//!< command to load a file to memory !? I think it is connected with the fact that PVSS cannot read binary files (should it be there!?) 



#define HSCNF_PIXEL_DAC				"HSCNF_PIXEL_DAC"				//!< gets the current default configuration from the fed server for the pixel dacs
#define HSCNF_APIDAC				"HSCNF_APIDAC"					//!< gets the current default configuration from the fed server for the analog pilot dacs
#define HSCNF_DPICONF				"HSCNF_DPICONF"					//!< gets the current default configuration from the fed server for the digital pilot 
#define HSCNF_GOLCONF				"HSCNF_GOLCONF"					//!< gets the current configuration for the gol from the fed server

#define HSCNF_NOISYPIX				"HSCNF_NOISYPIX"				//!< gets the default configuration in the fed memory (from configuration file only)
	