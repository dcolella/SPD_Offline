// spdDbtest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../HSConfig/HSConfData.h"
#include "../spdDbConfLib/spdDBGlobalVersion.h"
const char iniFileName[] = "spdFedIni.ini";

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{

	
	

	//******************** gets the values from the ini file*******************************************

						// parses the inifile
	spdCalib::IniParser inifile(iniFileName);

	char Version[] = "2.2.15";
	char Side = '#';
	
	spdCalib::Category genSettings = inifile["General"];

	for (unsigned int n =0 ; n < genSettings.entries.size() ; ++n ){
		string name = genSettings.entries[n].name;
		string value = genSettings.entries[n].value;

		if ( name== "side"){
			Side = *value.c_str();
		}
		
	}


	//cout << "Welcome to SPD FED Server version " << Version << " Side  " << Side << endl;

	//spdCalib::Category dbSettings = inifile["DataBase"];

	//					// puts the settings in the connection object
	//spdDbConnection *conn = spdDbConnection::subscribe();
	//conn->setDbSettings( dbSettings);
	//	
	//	// connects to the db server
	////conn->connect();


	//	// initializes the fed server object
	//HSConfData hsData;
	//HSConfiguration *hsArray;
	//hsArray = new HSConfiguration[120];
	//hsData.SetHSFilePointer( hsArray);

	//hsData.GetFromDB('A', -1);

	////spdDBGlobalVersion topVersion;

	////topVersion.setVersionNumber( -1);

	////cout << "global version settings are:\n";
	////cout << "global version number : "<< topVersion.getVersionNumber()<< endl;
	////cout << "version side A: " << topVersion.getHsVerSideA() << endl;
	////cout << "version side C: " << topVersion.getHsVerSideC() << endl;
	////cout << "run type for this configuration: " ;
	////cout << topVersion.getRunType()<<", ";
	////cout << topVersion.getRunTypeName().c_str() << endl;
	////

	//
	//		
	//

	////hsArray[0].GetDPIDefault()->SetDPIEventNumb( 123);
	//
	////hsData.SaveInDB( 'A', true);

	//hsData.linkDbVersions( 'A',1,0,0,0);
	//

	//delete[] hsArray;
	//

	////topVersion.update( 1, 2, 0,0,0);
	//

	//conn->disconnect();

	//cout << "press any key to continue\n";
	//getchar();

	//			// this will update a global version with
	//			// side a -> version 1
	//			// side c with version 2
	//			// version 0 routers side a and c
	//			// run type 0				
	//

	return 0;
}

