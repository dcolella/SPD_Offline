#pragma once

#include <vector>
#include "../spdDbConfLib/spdIniParser.h"
#include "../spdDbConfLib/SpdDetectorConf.h"

namespace spdCalib{
	

	//struct Coord{
	//	int column;
	//	int row;
	//};

	struct NoisyList{
		int router;
		int hs;
		int pixel;
		std::vector<PixelCoordinate> coordinates;
	};

	struct DacValue{
		int router;
		int hs;
		int pixel;
		int dacNumber;
		unsigned char value;
	};

	enum OperMode{

	};

	enum headerVariables{
		hdRunNumber = 0,
		hdType,
		hdRouter,
		hdActDetecConf

	};
	class Parser
	{


		long int RunNumber;
		int Type;
		int Router;
		long DbOperationMode;
		int DbConfig;
		int DbType;

		
		std::vector<NoisyList> NoisyPixels;
		std::vector<DacValue> DacValues;

		void dacValuesToConf(SpdDetectorConf &dbConfA, SpdDetectorConf &dbConfC);
		void noisyToConf( SpdDetectorConf &dbConfA, SpdDetectorConf &dbConfC);

		void parseHeader( const Category &header);
		void parseDacValues( const Category & dacs);
		void parseNoisy( const Category & noisy);

	public:


		
		DacValue getDacValue(int index){return  DacValues[index];};
		int getDacValueCount(){return (int)DacValues.size();};

		NoisyList getNoisyList( int index){return NoisyPixels[index];};
		int getNoisyListCount(){return (int)NoisyPixels.size();};

		long getRunNumber(){return RunNumber;};
		void setRunNumber(long run){RunNumber=run;};

		int getType(){return Type;};
		
		int getDbOperationMode(){return DbOperationMode;};
		void setDbOperationMode( int mode){DbOperationMode=mode;};

		long getDbVersion(){return DbConfig;};
		void setDbVersion(long version){DbConfig=version;};

		int getDbRunType(){return DbType;};
		void setDbRunType(int type){DbType= type;};

		int getRouter(){return Router;};

		void openFile( const char filename[]);

		int openFileList(std::vector<string> fileList);
		int openFileList(const char filename[]);

		int updateDB(void);

		Parser(void);
		~Parser(void);
	};

};