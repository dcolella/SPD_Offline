#include "StdAfx.h"
#include "chtoaddrdecoder.h"

ChToAddrDecoder::ChToAddrDecoder(void){
	int sect, stave, side;

	for (int i = 0; i < 120; i++){
		//ChActivationStatus[i] = 0;
		
		if (i >= 60){
			sect = (i - 60)/6;
			stave = (i - 60) % 6;
			side = 1;
		} else {
			sect = i / 6;
			stave = i % 6;
			side = 0;
		}
		
		PositionToChannel[sect][stave][side] = i; 
	}

	
	for (UInt32 j =0; j < 20; j++){
		RouterBoardAddresses[j] = j+1; 
	}
	//RouterBoardAddresses[19] = 22; //pay attention, it is the same of faninout 0
	BusyBoardsAddresses[0] = 22;
	BusyBoardsAddresses[1] = 23;
}


ChToAddrDecoder::~ChToAddrDecoder(void){
}

/*
void ChToAddrDecoder::SetVectRoutBoardAddr(UInt8 BoardNumb, UInt32 Addr){
	RouterBoardAddresses[BoardNumb] =(Addr << 27);
}

void ChToAddrDecoder::SetVectBusyBoardAddr(UInt8 BoardNumb, UInt32 Addr){
	BusyBoardsAddresses[BoardNumb] =(Addr << 19);
}
*/

UInt8  ChToAddrDecoder::GetChannNumbFromPosition(UInt8 SectorNumb, UInt8 StaveNumb, UInt8 Side){
	return PositionToChannel[SectorNumb][StaveNumb][Side];
}


void  ChToAddrDecoder::SetChannNumbFromPosition(UInt8 SectorNumb, UInt8 StaveNumb, UInt8 Side, UInt8 Channel){
	PositionToChannel[SectorNumb][StaveNumb][Side] = Channel;
}


UInt32 ChToAddrDecoder::GetBoardAddr(UInt8 ChNumb){
	return RouterBoardAddresses[ChNumb / 6];
}


bool ChToAddrDecoder::GetRTActStatus(UInt8 RTNumb){

	// this will return 1 for the moment while I sort things out
	return 1;

	//RTNumb *= 6;
	//for(int i=0; i < 6; i++){
	//	if(ChActivationStatus[RTNumb + i] > 0 ) return 1;
	//}

	//return 0;
}
