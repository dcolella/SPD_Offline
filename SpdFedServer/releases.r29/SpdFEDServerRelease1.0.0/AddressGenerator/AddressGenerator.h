#pragma once
#ifndef AddressGenerator_h
#define AddressGenerator_h

#include "stdafx.h"
#include "ChToAddrDecoder.h"
#include "LinkRxIntAddr.h"
#include "RoutIntAddr.h"
#include "BusyCardIntAddr.h"
#include "../spdDbConfLib/spdLogger.h"

const int nHalfStaves= 120;
const int nRouters = 20;
const int nBusyCards = 2;

const int nHsInSide=60;
const int nHsInRouter = 6;

class AddressGenerator{
  private:
	
    LinkRxIntAddr LRxAddr[nHalfStaves];
    RoutIntAddr RoutAddr[nRouters];
	BusyCardIntAddr BusyCardAddr[nBusyCards];

	spdLogger *log;
	
  public:
	ChToAddrDecoder * CHD;
	 AddressGenerator(ChToAddrDecoder *);                 //ok
	~AddressGenerator(void);
	
    void ReDefineAddr(void);                //ok
    
	LinkRxIntAddr & getLRxAddr(unsigned ChNumb);
	RoutIntAddr & getRoutAddr(unsigned ChNumb);
	BusyCardIntAddr & getBusyCardAddr(unsigned ChNumb);	


};


#endif