#pragma once
#include "../BitManager/bitsmanage.h"

class ChToAddrDecoder : public BitsManage {
  private:	
	//UInt8 ChActivationStatus[120];      // = 0 off = 1 on  = 2scan
	
	UInt32 RouterBoardAddresses[20];
    UInt32 BusyBoardsAddresses[2];
	UInt8 PositionToChannel[10][6][2]; 
    
  public:
	ChToAddrDecoder(void);                                            //ok
	~ChToAddrDecoder(void); 
	
	void SetVectRoutBoardAddr(UInt8 BoardNumb, UInt32 Addr);               //ok
	UInt32 GetVectRoutBoardAddr(UInt8 BoardNumb){return RouterBoardAddresses[BoardNumb];};                          //ok  
    void SetVectBusyBoardAddr(UInt8 BoardNumb, UInt32 Addr);               //ok
	UInt32 GetVectBusyBoardAddr(UInt8 BoardNumb){return BusyBoardsAddresses[BoardNumb];};;

	UInt8 GetChannNumbFromPosition(UInt8 SectorNumb, UInt8 StaveNumb, UInt8 Side); //ok
    void  SetChannNumbFromPosition(UInt8 SectorNumb, UInt8 StaveNumb, UInt8 Side, UInt8 Channel); //ok
	
	                        //ok  
	UInt32 GetBoardAddr(UInt8 ChNumb);                        //ok  
	UInt32 GetRouterCh(UInt8 ChNumb){return ChNumb % 6;};     //ok
    
	//void SetChActStatus(UInt8 ChNumb, UInt8 ActStatus){ChActivationStatus[ChNumb] = ActStatus;}; //ok

	//UInt8 GetChActStatus(UInt8 ChNumb) {return ChActivationStatus[ChNumb];};					//ok
	bool GetRTActStatus(UInt8 RTNumb);                        //ok
	bool GetLRxActStatus(UInt8 LRxNumb);
};
