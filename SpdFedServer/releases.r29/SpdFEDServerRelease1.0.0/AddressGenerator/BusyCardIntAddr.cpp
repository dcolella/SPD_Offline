#pragma once
#include "StdAfx.h"
#include ".\busycardintaddr.h"

BusyCardIntAddr::BusyCardIntAddr(void){
    VMEResetAddr = 0x10;
	VersionNumberAddr = 0x20;
	EnDriverOrReceiverAddr = 0x30;
    ControlRegAddr = 0x1c0;
	BusyMaskAddr = 0x1d0;
    L0CounterAddr = 0x1e0;
    ReadInputAddr = 0x1f0;
	
	for (UInt32 i=0; i < 24; ++i){
		L0DelayAddr[i] = (i << 4) + 0x40;
	}
    
	for (UInt32 i=0; i < 4; ++i){
		CnCounterAddr[i] = (i << 4) + 0x200;
	}

}

BusyCardIntAddr::~BusyCardIntAddr(void)
{
}



void BusyCardIntAddr::ModifyBusyCardAddresses(void){
	ModifyUInt32(VMEResetAddr);
    ModifyUInt32(VersionNumberAddr);    
	ModifyUInt32(EnDriverOrReceiverAddr);

	ModifyUInt32(ControlRegAddr);
	ModifyUInt32(BusyMaskAddr);
	ModifyUInt32(L0CounterAddr);
	ModifyUInt32(ReadInputAddr);

	for (int i=0; i < 24; ++i){
		ModifyUInt32(L0DelayAddr[i]);
	}

    for (int i=0; i < 4; ++i){
		ModifyUInt32(CnCounterAddr[i]);
	}

}
void BusyCardIntAddr::SetBusyCardAddr(UInt32 BrdAdd){
	NewTerm = BrdAdd;
	Nbit= 5;
	Position = 27;  
	BMOperation = 0;	
    SetClWord();

	BoardAddr = BrdAdd << Position;

	ModifyBusyCardAddresses();
}



void BusyCardIntAddr::ShiftAddresses(int Nshift){
	if (Nshift != 0){
		NShift = Nshift;
		BMOperation = 1;

		ModifyBusyCardAddresses();
	}

}



