#pragma once
#include "../BitManager/bitsmanage.h"


class LinkRxIntAddr : public BitsManage{

 private:
    UInt32 LRxDPMBaseAddr;
	UInt32 LRxVersionAddr;

	UInt32 LRxCntrRegAddr;
	UInt32 LRxStatusRegAddr;
	UInt32 LRxTempAddr;
	UInt32 LRxClkDelayAddr;
	UInt32 LRxDataDelayAddr;
	UInt32 LRxDPMAddrCountAddr;
	UInt32 LRxColumMaskAddr;
	UInt32 LRxResetAddr;
	UInt32 LRxResetSoftAddr;
	UInt32 LRxFlushAddr;
	UInt32 LRxTestRegAddr;
	UInt32 LRxEnableMCMStimuliData;
	UInt32 LRxMCMStimuliData;
	UInt32 LRxTestStimAddr;
	UInt32 LRxBusyMaskAddr;
	UInt32 LRxHistAddr;
	UInt32 LRxRxErrorCount;
	UInt32 LRxL1MemPointAddr;
	UInt32 LRxL1RoutCountAddr;
	UInt32 LRxBusyControlAddr;
	UInt32 LRxShiftSelAddr;
	UInt32 LRxEnMoveBitAddr;
	UInt32 LRxEventStartAddr;
	UInt32 LRxEventEndAddr;
	UInt32 LRxL1MemStartAddr;
	UInt32 LRxL1MemEndAddr;
//	UInt32 LRxErrorFormatAddr;     
    UInt32 LRxInputStageStatus0Addr; 
	UInt32 LRxInputStageStatus1Addr; 
	UInt32 LRxEvDescrRamStatusAddr; 
    UInt32 LRxFIFOPixDataStatusAddr; 
	UInt32 LRxFIFOEvDataStatusAddr; 
    UInt32 LRxDataEncoderStatus0Addr; 
	UInt32 LRxDataEncoderStatus1Addr; 
	UInt32 LRxDataEncoderStatus2Addr; 
	UInt32 LRxDataEncoderStatus3Addr; 
	UInt32 LRxDataEncoderStatus4Addr; 
	UInt32 LRxDataEncoderStatus5Addr; 
	UInt32 LRxDataEncoderStatus6Addr; 
	UInt32 LRxDataEncoderStatus7Addr;

	UInt32 LRXFastorL1DelayAddr;


 public:
	LinkRxIntAddr(void);                        //ok
	~LinkRxIntAddr(void);
    
	void SetLinkRxChannel(UInt32 RTChannel);  //ok
    void ShiftAddresses(int Nshift);             //ok  
    void AddRoutAddresses(UInt32 RoutLinkRxBaseAddr);   //ok
	void ModifyLinkRxAddresses(void);                 //ok
	
		
	UInt32 GetDPMBaseAddr(){return LRxDPMBaseAddr;};
	UInt32 GetVersionAddr(){return LRxVersionAddr;};
	
	UInt32 GetCntrRegAddr(){return LRxCntrRegAddr;};
	UInt32 GetStatusRegAddr(){return LRxStatusRegAddr;};
	UInt32 GetTempAddr(){return LRxTempAddr;};
	UInt32 GetClkDelayAddr(){return LRxClkDelayAddr;};
	UInt32 GetDataDelayAddr(){return LRxDataDelayAddr;};
	UInt32 GetDPMAddrCountAddr(){return LRxDPMAddrCountAddr;};
	UInt32 GetColumMaskAddr(){return LRxColumMaskAddr;};
	UInt32 GetResetAddr(){return LRxResetAddr;};
	UInt32 GetResetSoftAddr(){return LRxResetSoftAddr;};
	UInt32 GetFlushAddr(){return LRxFlushAddr;};
	UInt32 GetTestRegAddr(){return LRxTestRegAddr;};
	UInt32 GetEnableMCMStimuliDataAddr(){return LRxEnableMCMStimuliData;};
	UInt32 GetMCMStimuliDataAddr(){return LRxMCMStimuliData;};
	UInt32 GetTestStimAddr(){return LRxTestStimAddr;};
	UInt32 GetBusyMaskAddr(){return LRxBusyMaskAddr;};
	UInt32 GetHistAddr(){return LRxHistAddr;};
	UInt32 GetRxErrorCount(){return LRxRxErrorCount;};
	UInt32 GetL1MemPointAddr(){return LRxL1MemPointAddr;};
	UInt32 GetL1RoutCountAddr(){return LRxL1RoutCountAddr;};
	UInt32 GetBusyControlAddr(){return LRxBusyControlAddr;};
	UInt32 GetShiftSelAddr(){return LRxShiftSelAddr;};
	UInt32 GetEnMoveBitAddr(){return LRxEnMoveBitAddr;};
	UInt32 GetEventStartAddr(){return LRxEventStartAddr;};
	UInt32 GetEventEndAddr(){return LRxEventEndAddr;};
	UInt32 GetL1MemStartAddr(){return LRxL1MemStartAddr;};
	UInt32 GetL1MemEndAddr(){return LRxL1MemEndAddr;};

	UInt32 GetLRXFastorL1DelayAddr(){return LRXFastorL1DelayAddr;};

	//UInt32 GetErrorFormatAddr(){return LRxErrorFormatAddr;};
    UInt32 GetInputStageStatus0Addr(){return LRxInputStageStatus0Addr;}; 
	UInt32 GetInputStageStatus1Addr(){return LRxInputStageStatus1Addr;}; 
	UInt32 GetEvDescrRamStatusAddr(){return LRxEvDescrRamStatusAddr;}; 
    UInt32 GetFIFOPixDataStatusAddr(){return LRxFIFOPixDataStatusAddr;}; 
	UInt32 GetFIFOEvDataStatusAddr(){return LRxFIFOEvDataStatusAddr;}; 
    UInt32 GetDataEncoderStatus0Addr(){return LRxDataEncoderStatus0Addr;}; 
	UInt32 GetDataEncoderStatus1Addr(){return LRxDataEncoderStatus1Addr;}; 
	UInt32 GetDataEncoderStatus2Addr(){return LRxDataEncoderStatus2Addr;}; 
	UInt32 GetDataEncoderStatus3Addr(){return LRxDataEncoderStatus3Addr;}; 
	UInt32 GetDataEncoderStatus4Addr(){return LRxDataEncoderStatus4Addr;}; 
	UInt32 GetDataEncoderStatus5Addr(){return LRxDataEncoderStatus5Addr;}; 
	UInt32 GetDataEncoderStatus6Addr(){return LRxDataEncoderStatus6Addr;}; 
	UInt32 GetDataEncoderStatus7Addr(){return LRxDataEncoderStatus7Addr;};
	

};
