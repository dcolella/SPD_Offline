#pragma once
#include "StdAfx.h"
#include "addressgenerator.h"

AddressGenerator::AddressGenerator(ChToAddrDecoder * ch){
	CHD = ch;
	ReDefineAddr();

	this->log = & spdLogger::getInstance();
}

AddressGenerator::~AddressGenerator(){
}

void AddressGenerator::ReDefineAddr(void){
	for(int i =0; i < 20; i++){
		RoutAddr[i].ShiftAddresses(16);
		RoutAddr[i].SetRoutBoardAddr(CHD->GetVectRoutBoardAddr(i));
		
	}

	for(int i =0; i < 120; i++){
		LRxAddr[i].SetLinkRxChannel(i % 6);
		LRxAddr[i].ShiftAddresses(2);
		LRxAddr[i].AddRoutAddresses(RoutAddr[i / 6].GetLinkRxBaseAddr());  
	}
	
	for(int i =0; i < 2; i++){
		BusyCardAddr[i].SetBusyCardAddr(CHD->GetVectBusyBoardAddr(i));
		BusyCardAddr[i].ShiftAddresses(0);
	}
}

LinkRxIntAddr & AddressGenerator::getLRxAddr(unsigned ChNumb){
	
	if (ChNumb >= nHalfStaves){
		log->log("ERROR:(AddressGenerator::getLRxAddr) channel %d out of range ", ChNumb);
		ChNumb =ChNumb%nHalfStaves;
	}
	return LRxAddr[ChNumb];
}

RoutIntAddr & AddressGenerator::getRoutAddr(unsigned ChNumb){
	if (ChNumb >= nHalfStaves){
		log->log("ERROR:(AddressGenerator::getRoutAddr) channel %d out of range ", ChNumb);
		ChNumb =ChNumb%nHalfStaves;
	}
	return RoutAddr[ChNumb / nHsInRouter];
}

BusyCardIntAddr & AddressGenerator::getBusyCardAddr(unsigned ChNumb){ 
	if (ChNumb >= nHalfStaves){
		log->log("ERROR:(AddressGenerator::getBusyCardAddr) channel %d out of range ", ChNumb);
		ChNumb =ChNumb%nHalfStaves;
	}
	return BusyCardAddr[ChNumb / nHsInSide];
}