#pragma once
#include "StdAfx.h"
#include "linkrxintaddr.h"


LinkRxIntAddr::LinkRxIntAddr(void){

    LRxDPMBaseAddr = 0x0;

	LRxCntrRegAddr = 0x400000;
	LRxStatusRegAddr = 0x400001;
	LRxTempAddr = 0x400002;
	LRxClkDelayAddr = 0x400003;
	LRxDataDelayAddr = 0x400004;
	LRxDPMAddrCountAddr = 0x40000b;
	LRxColumMaskAddr = 0x40000c;
	LRxResetAddr = 0x40000d;
	LRxResetSoftAddr = 0x40000e;
	LRxFlushAddr = 0x40000f;
	LRxTestRegAddr = 0x400010;
	LRxTestStimAddr = 0x400011;
	LRxBusyMaskAddr= 0x400012;
	LRxHistAddr = 0x400013;
	LRxRxErrorCount = 0x400014;
	LRxVersionAddr = 0x400015;
	LRxEnableMCMStimuliData = 0x400016;
	LRxMCMStimuliData = 0x400017;

	LRXFastorL1DelayAddr = 0x400019;
	//LRxErrorFormatAddr = 0x400019;

	LRxL1MemPointAddr = 0x400030;
	LRxL1RoutCountAddr = 0x400031;
	LRxBusyControlAddr = 0x400032;
	LRxShiftSelAddr = 0x400033;
	LRxEnMoveBitAddr = 0x400034;
	LRxEventStartAddr = 0x400040;
	LRxEventEndAddr = 0x40005f;
	LRxL1MemStartAddr = 0x400060;
	LRxL1MemEndAddr = 0x40007f;

	
    LRxInputStageStatus0Addr = 0x400081;
    LRxInputStageStatus1Addr = 0x400082; 
	LRxEvDescrRamStatusAddr = 0x400083; 
    LRxFIFOPixDataStatusAddr = 0x400084; 
	LRxFIFOEvDataStatusAddr = 0x400085; 
    LRxDataEncoderStatus0Addr = 0x400086; 
	LRxDataEncoderStatus1Addr = 0x400087; 
	LRxDataEncoderStatus2Addr = 0x400088; 
	LRxDataEncoderStatus3Addr = 0x400089; 
	LRxDataEncoderStatus4Addr = 0x40008A; 
	LRxDataEncoderStatus5Addr = 0x40008B; 
	LRxDataEncoderStatus6Addr = 0x40008C; 
	LRxDataEncoderStatus7Addr = 0x40008D; 
    
	
}

LinkRxIntAddr::~LinkRxIntAddr(void){
}

void LinkRxIntAddr::ModifyLinkRxAddresses(void){
    
	ModifyUInt32(LRxDPMBaseAddr);
	ModifyUInt32(LRxVersionAddr);
    
	ModifyUInt32(LRxCntrRegAddr);
	ModifyUInt32(LRxStatusRegAddr);
	ModifyUInt32(LRxTempAddr);
	ModifyUInt32(LRxClkDelayAddr);
	ModifyUInt32(LRxDataDelayAddr);
	ModifyUInt32(LRxDPMAddrCountAddr);
	ModifyUInt32(LRxColumMaskAddr);
	ModifyUInt32(LRxResetAddr);
	ModifyUInt32(LRxResetSoftAddr);
	ModifyUInt32(LRxFlushAddr);
	ModifyUInt32(LRxTestRegAddr);
	ModifyUInt32(LRxEnableMCMStimuliData);
	ModifyUInt32(LRxMCMStimuliData);
	ModifyUInt32(LRxTestStimAddr);
	ModifyUInt32(LRxBusyMaskAddr);
	ModifyUInt32(LRxHistAddr);
	ModifyUInt32(LRxRxErrorCount);
	ModifyUInt32(LRxL1MemPointAddr);
	ModifyUInt32(LRxL1RoutCountAddr);
	ModifyUInt32(LRxBusyControlAddr);
	ModifyUInt32(LRxShiftSelAddr);
	ModifyUInt32(LRxEnMoveBitAddr);
	ModifyUInt32(LRxEventStartAddr);
	ModifyUInt32(LRxEventEndAddr);
	ModifyUInt32(LRxL1MemStartAddr);
	ModifyUInt32(LRxL1MemEndAddr);

	//ModifyUInt32(LRxErrorFormatAddr);
	ModifyUInt32(LRXFastorL1DelayAddr);

	ModifyUInt32(LRxInputStageStatus0Addr);
	ModifyUInt32(LRxInputStageStatus1Addr); 
	ModifyUInt32(LRxEvDescrRamStatusAddr); 
    ModifyUInt32(LRxFIFOPixDataStatusAddr); 
	ModifyUInt32(LRxFIFOEvDataStatusAddr); 
    ModifyUInt32(LRxDataEncoderStatus0Addr); 
	ModifyUInt32(LRxDataEncoderStatus1Addr); 
	ModifyUInt32(LRxDataEncoderStatus2Addr); 
	ModifyUInt32(LRxDataEncoderStatus3Addr); 
	ModifyUInt32(LRxDataEncoderStatus4Addr); 
	ModifyUInt32(LRxDataEncoderStatus5Addr); 
	ModifyUInt32(LRxDataEncoderStatus6Addr); 
	ModifyUInt32(LRxDataEncoderStatus7Addr); 

}

void LinkRxIntAddr::SetLinkRxChannel(UInt32 RTChannel){
	NewTerm = RTChannel;
	Nbit= 3;
	Position = 19;  
	BMOperation = 0;	
    
	SetClWord();

	ModifyLinkRxAddresses();
	
}

void LinkRxIntAddr::AddRoutAddresses(UInt32 RoutLinkRxBaseAddr){
	NewTerm = RoutLinkRxBaseAddr >> 26 ;
	Nbit= 6;
	Position = 26;  
	BMOperation = 0;	
    SetClWord();

	ModifyLinkRxAddresses();
}



void LinkRxIntAddr::ShiftAddresses(int Nshift){
    if (Nshift != 0){
		NShift = Nshift;
		BMOperation = 1;

		ModifyLinkRxAddresses();
	}
}


