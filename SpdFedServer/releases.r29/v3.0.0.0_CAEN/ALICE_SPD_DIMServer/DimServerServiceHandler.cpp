#include "StdAfx.h"
#include ".\dimserverservicehandler.h"

#include "dis.hxx" 
#include "../AddressGenerator/ChToAddrDecoder.h"
#include "../AddressGenerator/addressgenerator.h"
#include "../VMEAcc/errorhandler.h"
#include "../VMEAcc/VMEAccess.h"
#include ".\DimServerCommandHandler.h"
#include ".\DimServerServiceHandler.h"

#include "../HSConfig/SPDConfig.h"


DimServerServiceHandler::DimServerServiceHandler(char Side){

  	
  side = Side;
  logger = &spdLogger::getInstance();
  ReturnCode = 0;

  this->heartBitTime = GetTickCount();

  if( Side == 'A'){
	ChNumberService = new DimService("spd_feDimServerA/CH_NUMBER", ChNumber);
	CommandNameService = new DimService("spd_feDimServerA/CMD_NAME", "no command");
	ReturnCodeService = new DimService("spd_feDimServerA/ERROR_CODE", ReturnCode);  //SS 14.04.2014
	ReturnTextService = new DimService("spd_feDimServerA/ERRORS_REPORT", "INFO: fed started");
	DataOutService = new DimService("spd_feDimServerA/RET_DATA", "I", this);
	SendDataStream = new DimService("spd_feDimServerA/DATA_STREAM", "I", this);
	HearthbitService = new DimService("spd_feDimServerA/HEATHBIT", heartBitTime);
		// will set the logging dim service also
	logger->setDimServiceName("spd_feDimServerA/LOG");

	

	
  } else {
	ChNumberService = new DimService("spd_feDimServerC/CH_NUMBER",ChNumber);
	CommandNameService = new DimService("spd_feDimServerC/CMD_NAME",  "no command");
	ReturnCodeService = new DimService("spd_feDimServerC/ERROR_CODE", ReturnCode); //SS 14.04.2014
	ReturnTextService = new DimService("spd_feDimServerC/ERRORS_REPORT",  "INFO: fed started");
	DataOutService = new DimService("spd_feDimServerC/RET_DATA", "I", this);
	SendDataStream = new DimService("spd_feDimServerC/DATA_STREAM", "I", this);
	HearthbitService = new DimService("spd_feDimServerC/HEATHBIT", heartBitTime);
	//will set the logging dim service also
	logger->setDimServiceName("spd_feDimServerC/LOG");
	
	
  }
  
  logger->logToFile("dim log service started");


}

DimServerServiceHandler::~DimServerServiceHandler(void){


	// deletes all services
	delete ChNumberService ;
	delete CommandNameService;
	delete ReturnCodeService;
	delete ReturnTextService ;
	delete DataOutService; 
	delete SendDataStream;
	delete HearthbitService;


}





void DimServerServiceHandler::SetCommandName(char * cmdName){
		CommandName = cmdName; 
		sizeCommandName = strlen(CommandName);
}


void DimServerServiceHandler::SetErrorName(char * errName, int errCode){
		ReturnCode = abs(errCode);
	    ReturnText = errName; 
		sizeReturnText = strlen(ReturnText);
}

void DimServerServiceHandler::SendHearthbit(void){
	heartBitTime = GetTickCount();
	HearthbitService->updateService(heartBitTime);
}

int DimServerServiceHandler::UpdateServices(void){

	ChNumberService->updateService(ChNumber);
	DataOutService->updateService(DataOut, (sizeDataOut * 4));
	CommandNameService->updateService(CommandName);
	ReturnCodeService->updateService(ReturnCode);
	ReturnTextService->updateService(ReturnText);
	logger->log(ReturnText);

	return 0;
} 


int DimServerServiceHandler::UpdateServices(int chNumber, char * cmdName, int returnCode, char * returnText, unsigned int * dttoSnd, unsigned int szDtToSend){
	
	ChNumber = chNumber;

	CommandName = cmdName; 
	sizeCommandName = strlen(CommandName);
    
	ReturnCode = abs(returnCode);
	ReturnText = returnText; 
	sizeReturnText = strlen(ReturnText);
	
    DataOut = dttoSnd;
    sizeDataOut = szDtToSend;
    return UpdateServices();
}

/*
int DimServerServiceHandler::UpdateServicesPool(char * errName){
	ChNumberService->updateService(0);
	//SendRetData->updateService(0);
	SendCommandName->updateService("POOL_ANAPIL");

	//SendErrorReportCode->updateService(0);
	SendErrorReport->updateService(errName);
	
	return 0;
}
*/


int DimServerServiceHandler::UpdateChNumber(){
	return ChNumberService->updateService(ChNumber);
	
}

int DimServerServiceHandler::UpdateChNumber(int chNumber){
	ChNumber = chNumber;
	return UpdateChNumber();
	
}


int DimServerServiceHandler::UpdateCommandName(){
	return CommandNameService->updateService(CommandName);
}

int DimServerServiceHandler::UpdateCommandName(char * cmdName ){
    CommandName = cmdName; 
	sizeCommandName = strlen(CommandName);
	return UpdateCommandName();
}


int DimServerServiceHandler::UpdateErrorName(){
	ReturnCodeService->updateService(ReturnCode);
	ReturnTextService->updateService(ReturnText);

	logger->log(ReturnText);
	return 0;
}

int DimServerServiceHandler::UpdateErrorName(char * errName, int errCode ){
	ReturnCode = abs(errCode);
	ReturnText = errName; 
	sizeReturnText = strlen(ReturnText);
	return UpdateErrorName();
}

int DimServerServiceHandler::UpdateDataToSend(){
	return DataOutService->updateService(DataOut, sizeDataOut * 4);
}


int DimServerServiceHandler::UpdateDataToSend(unsigned int* dttoSnd, unsigned int szDtToSend){
    DataOut = dttoSnd;
    sizeDataOut = szDtToSend;
	return UpdateDataToSend();
}

int DimServerServiceHandler::UpdateDataStream(){
	return SendDataStream->updateService(DataStream, sizeDataStream * 4);
}


int DimServerServiceHandler::UpdateDataStream(unsigned int* dttoSnd, unsigned int szDtToSend){
    DataStream = dttoSnd;
    sizeDataStream = szDtToSend;
	return UpdateDataStream();
}