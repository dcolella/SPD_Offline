/*! Definition file for commands associated with read-write register commands
	
*/

//************************************ busy card register commands *******************************


#define BSYCD_RESET				"BSYCD_RESET"			//!< busy card reset
#define BSYCD_VERSION			"BSYCD_VERSION"			//!< busy card FPGA version register
#define BSYCD_DRVREC			"BSYCD_DRVREC"			//!<
#define BSYCD_L0DELAY			"BSYCD_L0DELAY"			//!<
#define BSYCD_CONTROL			"BSYCD_CONTROL"			//!< writes the busy card control
#define BSYCD_BUSYMASK			"BFSYCD_BUSYMASK"		//!< writes the busy card mask register
#define BSYCD_L0COUNTER			"BSYCD_L0COUNTER"		//!< 
#define BSYCD_INPUTS			"BSYCD_INPUTS"			//!<
#define BSYCD_CNCOUNTERS		"BSYCD_CNCOUNTERS"		//!<

//***************************************** link receiver register commands ***********************
#define LRX_DPM						"LRX_DPM"							//!<	
#define LRX_LRX_VERSION				"LRX_LRX_VERSION"					//!< read only register with the version number of the firmware for this link receiver
#define LRX_CONTROL					"LRX_CONTROL"						//!<
#define LRX_STATUS					"LRX_STATUS"						//!<
#define LRX_TEMPERATURE				"LRX_TEMPERATURE"					//!<
#define LRX_CLK_DELAY				"LRX_CLK_DELAY"						//!< write only register, its used to set the delay in the clock line for this channel, 8 bits value
#define LRX_DATA_DELAY				"LRX_DATA_DELAY"					//!< write only register, its used to set the delay in the data line for this channel, 8 bits value
#define LRX_DPM_ADDRESS_COUNTER		"LRX_DPM_ADDRESS_COUNTER"			//!<
#define LRX_MASK_COLUMN				"LRX_MASK_COLUMN"					//!<
#define LRX_RESET					"LRX_RESET"							//!<
#define LRX_SOFT_RESET				"LRX_SOFT_RESET"					//!<
#define LRX_FLUSH					"LRX_FLUSH"							//!<
#define LRX_TEST_REG				"LRX_TEST_REG"						//!<
#define LRX_MCM_STIMULI				"LRX_MCM_STIMULI"					//!<
#define LRX_BUSY_MASK				"LRX_BUSY_MASK"						//!<
#define LRX_HISTOGRAM				"LRX_HISTOGRAM"						//!<
#define LRX_ERROCOUNTER				"LRX_ERROCOUNTER"					//!<
#define LRX_L1_MEM_POINTER			"LRX_L1_MEM_POINTER"				//!<
#define LRX_L1_ROUT_COUNT			"LRX_L1_ROUT_COUNT"					//!<
#define LRX_BUSY_CONTROL			"LRX_BUSY_CONTROL"					//!<
#define LRX_SHIFT_SEL				"LRX_SHIFT_SEL"						//!<
#define LRX_EN_MOVEBIT				"LRX_EN_MOVEBIT"					//!<
#define LRX_EV_START				"LRX_EV_START"						//!<
#define LRX_EV_END					"LRX_EV_END"						//!<
#define LRX_MEM_START				"LRX_MEM_START"						//!<
#define LRX_MEM_END					"LRX_MEM_END"						//!<
#define LRX_FORMAT_ERROR			"LRX_FORMAT_ERROR"					//!<
#define LRX_INPUT_ST_STATUS0		"LRX_INPUT_ST_STATUS0"				//!<
#define LRX_INPUT_ST_STATUS1		"LRX_INPUT_ST_STATUS1"				//!<
#define LRX_EV_DESCRAM_STATUS		"LRX_EV_DESCRAM_STATUS"				//!<
#define LRX_FIFO_PIX_DATA_STATUS	"LRX_FIFO_PIX_DATA_STATUS"			//!<
#define LRX_FIFO_EV_DATA_STATUS		"LRX_FIFO_EV_DATA_STATUS"			//!<
#define LRX_DATA_ENC_STATUS0		"LRX_DATA_ENC_STATUS0"				//!<
#define LRX_DATA_ENC_STATUS1		"LRX_DATA_ENC_STATUS1"				//!<
#define LRX_DATA_ENC_STATUS2		"LRX_DATA_ENC_STATUS2"				//!<
#define LRX_DATA_ENC_STATUS3		"LRX_DATA_ENC_STATUS3"				//!<
#define LRX_DATA_ENC_STATUS4		"LRX_DATA_ENC_STATUS4"				//!<
#define LRX_DATA_ENC_STATUS5		"LRX_DATA_ENC_STATUS5"				//!<
#define LRX_DATA_ENC_STATUS6		"LRX_DATA_ENC_STATUS6"				//!<
#define LRX_DATA_ENC_STATUS7		"LRX_DATA_ENC_STATUS7"				//!<
//#define LRX_FASTOR_L1_DELAY			"LRX_FASTOR_L1_DELAY"				//!< register to define the delay between fast-or and L1
#define LRX_FO_STROBE_LENGTH		"LRX_FO_STROBE_LENGTH"				//!< register to define the strobe length for the fast-or in the link receiver


//************************************************** Router Register Commands **********************************
#define RT_ERROR_MASK					"RT_ERROR_MASK"							//!< Command to access the router error mask register
#define RT_REGISTER						"RT_REGISTER"							//!< function to access a register inside the router
#define RT_DPM							"RT_DPM"								//!< Base address of the dual port memory (read only)
#define RT_VERSION_NUMBER				"RT_VERSION"							//!< 'RT_VERSION' is already defined in winUser.h file, an old windwos file that we don't want to mess for sure
#define RT_SPM							"RT_SPM"								//!< Base address of the single port memory (read only)
#define RT_LRX_MEM_BASEADDR				"RT_LRX_MEM_BASEADDR"					//!< Base address of the dual port memory in the link receiver : Must be debugged, never used and there are 6 memories per router(2 per lrx) but the software only returns one (we have a clear bug here)
#define RT_CONTROL						"RT_CONTROL"							//!< Base address of the router control register (this one is used every time)  
#define RT_STATUS1						"RT_STATUS1"							//!<
#define RT_STATUS2						"RT_STATUS2"							//!<
#define RT_STATUS3						"RT_STATUS3"							//!<
#define RT_STATUS_LINKRX0				"RT_STATUS_LINKRX0"						//!<
#define RT_STATUS_LINKRX1				"RT_STATUS_LINKRX1"						//!<
#define RT_STATUS_LINKRX2				"RT_STATUS_LINKRX2"						//!<
#define RT_STATUS_LINKRX3				"RT_STATUS_LINKRX3"						//!<
#define RT_STATUS_LINKRX4				"RT_STATUS_LINKRX4"						//!<
#define RT_STATUS_LINKRX5				"RT_STATUS_LINKRX5"						//!<
#define RT_STATUS_LINKRX6				"RT_STATUS_LINKRX6"						//!<
#define RT_STATUS_LINKRX7				"RT_STATUS_LINKRX7"						//!<
#define RT_STATUS_LINKRX8				"RT_STATUS_LINKRX8"						//!<
#define RT_STATUS_LINKRX9				"RT_STATUS_LINKRX9"						//!<
#define RT_STATUS_LINKRX10				"RT_STATUS_LINKRX10"					//!<
#define RT_STATUS_LINKRX11				"RT_STATUS_LINKRX11"					//!<
#define RT_STATUS_LINKRX12				"RT_STATUS_LINKRX12"					//!<
#define RT_STATUS_LINKRX13				"RT_STATUS_LINKRX13"					//!<
#define RT_STATUS_LINKRX14				"RT_STATUS_LINKRX14"					//!<
#define RT_STATUS_LINKRX15				"RT_STATUS_LINKRX15"					//!<
#define RT_WRHISTO						"RT_WRHISTO"							//!<
#define RT_RDHISTO						"RT_RDHISTO"							//!< For the moment this feature is disabled, the link receiver does not have histogram mode anymore
#define RT_JPLAYER						"RT_JPLAYER"							//!<
#define RT_L0ID							"RT_L0ID"								//!<
#define RT_L1ID							"RT_L1ID"								//!<
#define RT_L2ID							"RT_L2ID"								//!<
#define RT_RSTBNC						"RT_RSTBNC"								//!<
#define RT_IRQPUSHB						"RT_IRQPUSHB"							//!<
#define RT_RST_TEMPLIM					"RT_RST_TEMPLIM"						//!<
#define RT_FO_FROMVME					"RT_FO_FROMVME"							//!<
#define RT_VMERESET						"RT_VMERESET"							//!<
#define RT_DATA_RESET					"RT_DATA_RESET"							//!<
#define RT_STATUS_JTAG_SELECT			"RT_STATUS_JTAG_SELECT"					//!<
#define RT_DATA_SELECT					"RT_DATA_SELECT"						//!<
#define RT_SELECT						"RT_SELECT"								//!<
#define RT_DDL_STATUS					"RT_DDL_STATUS"							//!<
#define RT_RESET_DETECTOR				"RT_RESET_DETECTOR"						//!<
#define RT_RD_EV_START					"RT_RD_EV_START"						//!<
#define RT_EV_LENGTH_OF_BLOCK			"RT_EV_LENGTH_OF_BLOCK"					//!<
#define RT_RESET_TTC					"RT_RESET_TTC"							//!<
#define RT_RESET_LRX					"RT_RESET_LRX"							//!<
#define RT_RESET_HS						"RT_RESET_HS"							//!<
#define RT_SEND_TRIGGER					"RT_SEND_TRIGGER"						//!<
#define RT_FIFO_STARTADDR				"RT_FIFO_STARTADDR"						//!<
#define RT_FIFO_ENDADDR					"RT_FIFO_ENDADDR"						//!<
#define RT_FLUSH_DPM					"RT_FLUSH_DPM"							//!<
#define RT_FO_NUMBER					"RT_FO_NUMBER"							//!<
#define RT_TEMPERATURE					"RT_TEMPERATURE"						//!<
#define RT_JT_RESET_STMACHINE			"RT_JT_RESET_STMACHINE"					//!<
#define RT_JT_RESET_FIFO				"RT_JT_RESET_FIFO"						//!<
#define RT_JT_RDWR_DATA					"RT_JT_RDWR_DATA"						//!<
#define RT_JT_EXEC_START				"RT_JT_EXEC_START"						//!<
#define RT_JT_STATUS					"RT_JT_STATUS"							//!<
#define RT_JT_RESET_CH					"RT_JT_RESET_CH"						//!<
#define RT_JT_RDEN_FIFOIN				"RT_JT_RDEN_FIFOIN"						//!<
#define RT_JT_RDNUMB_FIFOIN				"RT_JT_RDNUMB_FIFOIN"					//!<
#define RT_TEMPERATURE_LIMIT_MCM		"RT_TEMPERATURE_LIMIT_MCM"				//!<
#define RT_TEMPERATURE_LIMIT_BUS		"RT_TEMPERATURE_LIMIT_BUS"				//!<
#define RT_FO_GLOBAL_COUNTER			"RT_FO_GLOBAL_COUNTER"					//!<
#define RT_FO_COINCIDENCE_COUNTER		"RT_FO_COINCIDENCE_COUNTER"				//!<
#define RT_FO_TIME_COUNTER				"RT_FO_TIME_COUNTER"					//!<
#define RT_FO_LINKRX_COUNTER			"RT_FO_LINKRX_COUNTER"					//!<
#define RT_SCOPE_SELECTOR0				"RT_SCOPE_SELECTOR0"					//!<
#define RT_SCOPE_SELECTOR1				"RT_SCOPE_SELECTOR1"					//!<
#define RT_SCOPE_SELECTOR2				"RT_SCOPE_SELECTOR2"					//!<
#define RT_RESET_PIXEL					"RT_RESET_PIXEL"						//!<
#define RT_TIME_L0L1					"RT_TIME_L0L1"							//!<
#define RT_RXREADY								"RT_RXREADY"							//!< new michele register
#define RT_RESET_BUSYRESOLVER					"RT_RESET_BUSYRESOLVER"					//!< new michele register
#define RT_TIME_BUSY_DAQ						"RT_TIME_BUSY_DAQ"						//!< new michele register
#define RT_TIME_BUSY_ROUTER						"RT_TIME_BUSY_ROUTER"					//!< new michele register
#define RT_TIME_BUSY_HS							"RT_TIME_BUSY_HS"						//!< new michele register
#define RT_TIME_BUSY_TRIGGERS_L1_FIFO			"RT_TIME_BUSY_TRIGGERS_L1_FIFO"			//!< new michele register
#define RT_NUM_TRANS_BUSY_DAQ					"RT_NUM_TRANS_BUSY_DAQ"					//!< new michele register
#define RT_NUM_TRANS_BUSY_ROUTER				"RT_NUM_TRANS_BUSY_ROUTER"				//!< new michele register
#define RT_NUM_TRANS_BUSY_HS					"RT_NUM_TRANS_BUSY_HS"					//!< new michele register
#define RT_NUM_TRANS_BUSY_TRIGGERS_L1_FIFO		"RT_NUM_TRANS_BUSY_TRIGGERS_L1_FIFO"	//!< new michele register
#define RT_MEM_COUNTERS							"RT_MEM_COUNTERS"						//!< new michele register
#define RT_ADDRESS_MEM_COUNTERS_SELECTED		"RT_ADDRESS_MEM_COUNTERS_SELECTED"		//!< new michele register
#define RT_L0_COUNTER							"RT_L0_COUNTER"							//!< new michele register

#define RT_INTERLOCK_STATUS						"RT_INTERLOCK_STATUS"					//!<Read_interlock_Status_selected          = (areg =='hf4) --> only read, you received  7 bits with the following format: Router_Interlock  Interlock on HS_5	Interlock on HS_4 	Interlock on HS_3 	Interlock on HS_2 	Interlock on HS_1 	Interlock on HS_0 
#define RT_BC_CLOCK_LOCK						"RT_BC_CLOCK_LOCK"						//!< Clock Fine Delay, makes the router lock to one of the bunch crossing orbits, write and read register with 2 bits with the follow format: Clock_phase_fine_delay[0],	Clock_phase_fine_delay[1]
#define RT_ERROR_MANAGER_RESET					"RT_ERROR_MANAGER_RESET"				//!< Error handler Global RESET (for all FSMs and SPM) 
#define RT_TP_L1_DELAY							"RT_TP_L1_DELAY"						//!< Configurable  Test pulse L1 delay, like this calibration scans and data taking scans can have the same delay setting in the pixels
#define RT_L1_IN_FIFO							"RT_L1_IN_FIFO"							//!< Defines the maximum number of L1s in the link receiver FIFO for the mcm multi event buffer 

#define RT_SEB_MEB_SET							"RT_SEB_MEB_SET"						//!< register to select multi event buffer or single event buffer. Address 0xf6 can only have 2 values -> 20090606 and 20070526 
#define RT_THRESHOLD_BUSY_TIME					"RT_THRESHOLD_BUSY_TIME"				//!< register to define the time that the router waits for the transition busy->dead
#define RT_L1_FINE_DELAY						"RT_L1_FINE_DELAY"						//!< register to define the fine delay applied to L1 in terms of BC
#define RT_TIMEOUT_READY_EVENT					"RT_TIMEOUT_READY_EVENT"				//!< register to define the timeot between the arrival of one L2 and the message of ready event from linkRx