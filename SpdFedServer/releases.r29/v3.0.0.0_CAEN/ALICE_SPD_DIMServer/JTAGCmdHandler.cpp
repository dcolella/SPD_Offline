#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"


int DimServerCommandHandler::JTAGCmdHandler(void){
	if(strncmp(CommString, "JT_", 3) != 0) return 2;
	CommString += 3;
    VMEAccess & VME = VMEAccess::getInstance();
	UInt32 ScanType;
	if(strcmp(CommString, "IR") == 0){
		ScanType = 1;
	}else if (strcmp(CommString, "DR") == 0){
		ScanType = 0;
	}else if (strcmp(CommString, "SETCLKSPEED") == 0){
		VME.SetJTClkSpeed(DataIn[0]);
		SRV->UpdateErrorName(CheckStatus(CommStringOriginal, cvSuccess, ChNumber), 0);
		return 0;
	}else{
		sprintf_s(OutMsgText, "ERROR: JTAG command %s not recognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}
    
	status = VME.JTAGWriter(ChNumber, &DataIn[1], ScanType,DataIn[0]);
	if(status == cvSuccess){
		dataSize = VME.GetWordsNumber();
		delete [] DataOut;
		DataOut = new UInt32 [dataSize];
		for(unsigned i=0; i < dataSize; ++i) DataOut[i]	= VME.RdJTVectOut()[i];
		SRV->UpdateServices(ChNumber, CommStringOriginal, status, CheckStatus(CommStringOriginal, status, ChNumber),(unsigned int*)DataOut, dataSize);
	} else SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), (int)status);
	return 0;
}

