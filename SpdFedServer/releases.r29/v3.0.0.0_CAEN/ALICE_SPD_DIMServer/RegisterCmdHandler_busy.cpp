#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"


int DimServerCommandHandler::RegisterCmdHandlerBusy(void){ 
	UInt32 BaseAddr; 
	AddressGenerator & addresses = AddressGenerator::getInstance();

	if(!(strncmp(CommString, "R_BSYCD_", 8)== 0) && !(strncmp(CommString, "W_BSYCD_", 8)==0)){
		return 8;
	}
	UInt32 Aux =0;
	char RdWrSelector = CommString[0];
	CommString += 8;

	VMEAccess & VME = VMEAccess::getInstance();
	//BUSY CARDS Commands
	//-------------------------------------------------------------
		if(strcmp(CommString, "RESET") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetVMEResetAddr(ChNumber);	              	
		}else if(strcmp(CommString, "VERSION") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetVersionNumberAddr(ChNumber);
		}else if(strcmp(CommString, "DRVREC") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetEnDriverOrReceiverAddr(ChNumber);	
		}else if(strcmp(CommString, "L0DELAY") == 0){
			BaseAddr = addresses.getBusyCardAddr( (UInt8) ChNumber).GetL0DelayAddr((UInt8)ChNumber,(UInt8) DataIn[0]);
            DataIn++;
			dataSize--;
		}else if(strcmp(CommString, "CONTROL") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetControlRegAddr(ChNumber);	
		}else if(strcmp(CommString, "BUSYMASK") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetBusyMaskAddr(ChNumber);
		}else if(strcmp(CommString, "L0COUNTER") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetL0CounterAddr(ChNumber);
		}else if(strcmp(CommString, "INPUTS") == 0){
			BaseAddr = addresses.getBusyCardAddr(ChNumber).GetReadInputAddr(ChNumber);
		}else if(strcmp(CommString, "CNCOUNTERS") == 0){
			BaseAddr = addresses.getBusyCardAddr((UInt8) ChNumber).GetCnCounterAddr((UInt8)ChNumber,(UInt8) DataIn[0]);
			DataIn++;
			dataSize--;
		}else{
			sprintf_s(OutMsgText, "ERROR: BusyCard command %s not recognized\n", CommStringOriginal);
			printf(OutMsgText);
			SRV->UpdateErrorName(OutMsgText, 1);
			return 1;
		}
	    
	BaseAddr += Offset;                //add protection: if it is register Offsett must be 0
	
	if(RdWrSelector == 'R'){
		dataSize = 1;
		delete[] DataOut;
		DataOut = new UInt32 [dataSize];
		status = VME.VMERegisterAcc( BaseAddr, DataOut, dataSize, RdWrSelector);

		SRV->UpdateServices(ChNumber, CommStringOriginal, (int)status, CheckStatus(CommStringOriginal, status, ChNumber),(unsigned int*)DataOut, dataSize);
		logger->logToScr(" read address in busy card Address %x, data out: %x ", BaseAddr, DataOut[0], ChNumber/6);
		
    }else{ 

		logger->logToScr(" writting address in busy card Address %x, data out: %x ", BaseAddr, DataIn[0], ChNumber/6);
		status = VME.VMERegisterAcc( BaseAddr, DataIn, dataSize, RdWrSelector);
		SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), (int)status);

	}
	
	return 0; 
}