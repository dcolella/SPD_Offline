#include "StdAfx.h"
#include ".\poolingoperationcontrol.h"


PoolingOperationControl::PoolingOperationControl(SPDConfig * spd, DimServerServiceHandler * srv){
    
	SPD = spd;
	SRV = srv;
	log = &(spdLogger::getInstance());

	runNumber = 0;

	this->routerErrorPoolActive = false;

		// for the moment the default is 10 seconds
	this->RouterRefreshTime = 10000;

	this->TemperaturePoolActive = true;
	this->TempRefreshTime = 1000; //ms

/*
	anapilDACPoolActive = false;
	this->anapilDACRefreshTime = 5000; //ms
	this->anapilDACStop = true;

	anapilADCPoolActive = false;
	this->anapilADCRefreshTime = 5000; //ms
	this->anapilADCStop = true;
*/
}

PoolingOperationControl::~PoolingOperationControl(void){

}


// starts the error handling
void PoolingOperationControl::startRouterErrorPooling( unsigned refreshTime){
	this->RouterRefreshTime = refreshTime;
	this->routerErrorPoolActive = true;
}

// reads errors in one router
void PoolingOperationControl::readRouterErrors(){
	vector <SpdRouter::RouterError> errorsInRouter;

	unsigned sideOffset= 0 ;
	this->log->logToScr("checking router errors");
	if (this->SPD->getSide() == 'C') sideOffset = 10;

	spdDbConnection * conn =spdDbConnection::subscribe();
	conn->connect();

	for (unsigned router = 0 ; router < 10 ; router ++){

		// if side c then its router 10 to 19
		unsigned routerNumber = router + sideOffset;
		errorsInRouter =  this->SPD->getRouter(routerNumber).readErrorList();
		if (errorsInRouter.size() > 0 )log->logToFile("------ error bunch router %d -------", router);
		
		for (unsigned error = 0 ;  error < errorsInRouter.size() ; error++){
			insertRouterErrorInTheDB( errorsInRouter[error], routerNumber);

			// here we are logging the router errors, completely redundant
			log->logToFile("router error: router = %d, runnumber = %d, class =0x%x, id=0x%x,bunch crossing =%d,order = %d, details1 = 0x%x, details2 = 0x%x  ", 
							router, runNumber, 
							errorsInRouter[error].errorClass, errorsInRouter[error].errorID, 
							errorsInRouter[error].bunchCrossing, errorsInRouter[error].errorOrder , 
							errorsInRouter[error].details1, errorsInRouter[error].details2 );
		}
	}

	// commits all errors in the database and disconnects
	conn->commit();
	conn->disconnect();
}

void PoolingOperationControl::insertRouterErrorInTheDB(SpdRouter::RouterError error, unsigned routerNumber){
	try{

		spdDbConnection *conn= spdDbConnection::subscribe();
		std::stringstream command;

		command << "insert into ErrorHandlerTable ";
		command << "(router_number, run_number, bunch_crossing, error_class, error_number, order_number, details_1, details_2)";
		command << " values (";
		command << routerNumber << ",";
		command << this->getRunNumber() << ",";
		command << error.bunchCrossing << ",";
		command << error.errorClass << ",";
		command << error.errorID << ",";
		command << error.errorOrder << ",";
		command << error.details1 << ",";
		command << error.details2 << ")";
		conn->sendSqlCommand( command.str());

	}
		
	catch(exception &error ){

		log->log("ERROR:: Exception caught writing error in the DB for router %d : %s",routerNumber, error.what());
	}
	catch(...){
		log->log("ERROR:: Unknown exception writing router error to the DB for router %d",routerNumber );
	}
}

UInt32 PoolingOperationControl::PoolingFunction(void){
	SPD->scans();

	// will refresh all temperatures
	if( ( TemperaturePoolActive && (GetTickCount() - TempStartTime) > TempRefreshTime) ){
		SPD->refreshAllTemperatures();
		TempStartTime = GetTickCount();
	}

/*
		//checking the side, in case of side C, add 60 to the hs value
	for (unsigned hs = 0; hs < numberOfHS ; hs++){
		if (this->SPD->getSide() == 'C'){
			hsSide = hs + 60;
		}else{
			hsSide = hs;
		}
		//if the jtag is used by another command, stop the pooling
		if ((this->SPD->getHS(hsSide).VME->JtagPause == true) || (anapilDACStop == true)) {
			anapilDACPoolActive = false;
		} else {
			anapilDACPoolActive = true;
		}
	}

	// refresh all Anapil DAC values
	if( ( anapilDACPoolActive && (GetTickCount() - anapilDACStartTime) > anapilDACRefreshTime) ){
		SRV->UpdateServicesPool(" ");
		SPD->refreshDACAnapilAll();
		anapilDACStartTime = GetTickCount();
		SRV->UpdateServicesPool("INFO: Executed pool");
	}

			//checking the side, in case of side C, add 60 to the hs value
	for (unsigned hs = 0; hs < numberOfHS ; hs++){
		if (this->SPD->getSide() == 'C'){
			hsSide = hs + 60;
		}else{
			hsSide = hs;
		}
		//if the jtag is used by another command, stop the pooling
		if ((this->SPD->getHS(hsSide).VME->JtagPause == true) || (anapilADCStop == true)){
			anapilADCPoolActive = false;
		} else {
			anapilADCPoolActive = true;
		}
	}

	// refresh all Anapil ADC values
	if( ( anapilADCPoolActive && (GetTickCount() - anapilADCStartTime) > anapilADCRefreshTime) ){
		SRV->UpdateServicesPool(" ");
		SPD->refreshADCAnapilAll();
		anapilADCStartTime = GetTickCount();
		SRV->UpdateServicesPool("INFO: Executed pool");
	}
*/
	
	// will read the router errors
	if( ( routerErrorPoolActive && (GetTickCount() - RouterErrorTime) > RouterRefreshTime) ){
		readRouterErrors();	
		RouterErrorTime = GetTickCount();
	}
	
	return 0;
}

/*
//! Method to start the anapil DAC pooling
void PoolingOperationControl::startDACAnapilPool(unsigned refreshTime){
	anapilDACRefreshTime = refreshTime;
	anapilDACPoolActive = true;
	anapilDACStop = false;
	anapilDACStartTime = GetTickCount(); 
}

//! Method to start the anapil ADC polling
void PoolingOperationControl::startADCAnapilPool(unsigned refreshTime){
	anapilADCRefreshTime = refreshTime;
	anapilADCPoolActive = true;
	anapilADCStop = false;
	anapilADCStartTime = GetTickCount(); 
}
*/
