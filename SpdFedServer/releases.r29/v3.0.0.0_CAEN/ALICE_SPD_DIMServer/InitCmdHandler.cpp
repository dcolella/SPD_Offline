#pragma once

#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"

int DimServerCommandHandler::InitCmdHandler(void){
    if(strncmp(CommString, "INIT_", 5) != 0) return 2;
	
    VMEAccess & VME = VMEAccess::getInstance();
	if(strcmp(CommString, "INIT_VME") == 0){
		//status = VME.RunResorceMenager();
		sprintf_s(OutMsgText, "INFO: INIT_VME command is not needed\n");
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 0);
	}

	else{
		sprintf_s(OutMsgText, "ERROR: INIT command %s not recognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}

	return 0; 
}

