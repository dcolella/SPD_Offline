// DIMServer.cpp : Defines the entry point for the console application.

#include "stdafx.h"
using namespace std;

#include "dis.hxx" 
#include "../AddressGenerator/ChToAddrDecoder.h"
#include "../AddressGenerator/addressgenerator.h"
#include "../VMEAcc/errorhandler.h"
#include "../VMEAcc/VMEAccess.h"
#include ".\DimServerCommandHandler.h"
#include ".\DimServerServiceHandler.h"

#include "../HSConfig/SPDConfig.h"

const char iniFileName[] = "spdFedIni.ini";
VMEAccess &VMEAcc = VMEAccess::getInstance();

BOOL CtrlHandler( DWORD fdwCtrlType ) //function is used to catch external calls closing the application to correctly close the USB connection to the CAEN controller
{ 
  spdLogger &log = spdLogger::getInstance();
  CVErrorCodes rc;

  switch( fdwCtrlType ) 
  { 
    case CTRL_C_EVENT:
      log.log("Terminating application");
	  rc=VMEAcc.EndSession();
	  if(rc!=cvSuccess) log.log("Error closing USB connection");
	  else log.log("INFO: Connection to CAEN VME-USB bridge is closed");
      return FALSE; 
 
    case CTRL_CLOSE_EVENT: 
      log.log("Terminating application");
	  rc=VMEAcc.EndSession();
	  if(rc!=cvSuccess) log.log("Error closing USB connection");
	  else log.log("INFO: Connection to CAEN VME-USB bridge is closed");
      return FALSE; 
 
    case CTRL_BREAK_EVENT:
      log.log("Ctrl-Break is received. Terminating application");
	  rc=VMEAcc.EndSession();
	  if(rc!=cvSuccess) log.log("Error closing USB connection");
	  else log.log("INFO: Connection to CAEN VME-USB bridge is closed");
      return FALSE;  
 
    case CTRL_LOGOFF_EVENT: 
      log.log("User logoff. Terminating application");
	  rc=VMEAcc.EndSession();
	  if(rc!=cvSuccess) log.log("Error closing USB connection");
	  else log.log("INFO: Connection to CAEN VME-USB bridge is closed");
      return FALSE;  
 
    case CTRL_SHUTDOWN_EVENT: 
      log.log("Machine is going to shutdown. Terminating application");
	  rc=VMEAcc.EndSession();
	  if(rc!=cvSuccess) log.log("Error closing USB connection");
	  else log.log("INFO: Connection to CAEN VME-USB bridge is closed");
      return FALSE;  
 
    default: 
      return FALSE; 
  } 
}

int main(){
    
	// remember to add the \0 for the string termination
	char Version[] = "3.0.0 using CAEN USB-VME interface\0";
	char Side = 'A';
	bool deBugMode=false;
  	string jamFilename;
    spdLogger &log = spdLogger::getInstance();

	log.log("********************** starting spd fed software **********************");
	log.log("parsing ini file 'spdFedIni.ini'");

//******************** gets the values from the ini file*******************************************
	
						// parses the inifile
	spdCalib::IniParser inifile(iniFileName);
						// gets the general settings
	spdCalib::Category genSettings = inifile["General"];

	Side = genSettings.entries["side"][0];

	// checks the side
	switch(Side){
		case 'a':
		case 'A':
			Side = 'A';
			break;
		case 'c':
		case 'C':
			Side='C';
			break;
		default:
			
			log.log("ERROR side should be 'A' or 'C' found %c, please check the %s file", Side,iniFileName);
			throw std::out_of_range("side as to be 'A' or 'C'");
	}
	
	if (genSettings.entries["debugMode"]=="true"){
		deBugMode=true;
		log.log("**********************************************************");
		log.log("***        Fed server in Debug mode no VME acess       ***");
		log.log("**********************************************************");
		deBugMode= true;
	}
	else{
		deBugMode=false;
	}

	// gets the db settings from the ini file
	spdCalib::Category dbSettings = inifile["DataBase"];

						// puts the settings in the connection object
	spdDbConnection *conn = spdDbConnection::subscribe();
	conn->setDbSettings( dbSettings);

	spdCalib::Category jamSettings = inifile["jam player"];
	jamFilename = jamSettings.entries["executable"];


//************************************************************************

	//VMEAccess &VMEAcc = VMEAccess::getInstance();
			// will set here the debug Mode from the ini file
	VMEAcc.setDebugMode(deBugMode);
	VMEAcc.InitSession();
		
	//VMEAcc.RunResorceMenager();
	//log.log("automatic INIT_VME");

	DimServerCommandHandler Commands(  Side);
	Commands.setJamPlayerFilename( jamFilename);

	log.log("starting spdFED server for side %c version = %s", Side, Version);

    if( Side == 'A'){
	  DimServer::start("spd_feDimServerA");
	} else {
      DimServer::start("spd_feDimServerC");
	}
	
	//correct handling of the exit signals
	if(!deBugMode) SetConsoleCtrlHandler( (PHANDLER_ROUTINE) CtrlHandler, TRUE );

	// will be sending the heartbit
	long tickcount=0;
	while (1){
		Sleep(1);
		Commands.PoolingFunction();
		tickcount = GetTickCount();
		//log.log("tick counts %d", tickcount);
		if( (tickcount % 3000) < 15){
			//log.log("tick counts %d", tickcount);
			Commands.SendHearthbit(); 	
		}
	}
	
	return 0;
}
