#include "StdAfx.h"
#include ".\HSConfDefault.h"



HSConfDefault::HSConfDefault(void){
	//NoisyPixMatrix = NULL;
	//DeadPixMatrix = NULL;
	//NoteInFile = NULL;
	FileName = "";
	FileSize = 0;

}

HSConfDefault::~HSConfDefault(void){
}

void HSConfDefault::SetPixDACVectDefault(UInt8 * Input){
	for(int i=0; i< 440; i++){
		PixDACVect[i] = Input[i];
	}	
}


void HSConfDefault::SetAPIdacVectDefault(UInt8 * Input){ //to remove
	for(int i=0; i< 6; i++){
		APIdacVect[i] = Input[i];
	}
}


UInt16 HSConfDefault::SetNoisyPixelVectDefault(UInt8 * Input){

	 int NoisyPixelNumber = (Input[0]* 256 + Input[1]) /3;
     Input +=2;

	

		// it will clear all noisy pixel vectors
	 for (int chip = 0 ; chip < numberOfChips; chip ++){
		 this->noisyPixelsDefault[chip].clear();
	 }
 	 
	for(unsigned i = 0; i < (unsigned)NoisyPixelNumber ; i++){
			// for each pixel 1st byte = chip number, 2nd byte = row number, 3th byte = column
		int chip = Input[3*i];
			// will initialize a pixel coordinate column = 3rd byte, row = 2nd byte
		PixelCoordinate noisyPix={(int)Input[3*i+2],(int)Input[3*i+1]} ;
				// will insert the noisy pixel
		this->noisyPixelsDefault[chip].push_back( noisyPix);
	
	}

	return NoisyPixelNumber * 3 + 2;
	 
}



// wil set the internal noisy pixel vector using as input the matrix like the one received by pvss
void HSConfDefault::SetNoisyPixelVectDefault(UInt32 Input[numberOfChips][256]){
	
	
	for(int chip =0; chip < numberOfChips; ++chip){
		noisyPixelsDefault[chip].clear();
		for(int i=0; i < 256; ++i){
			for(int  j=0; j < 32; ++j){
				if((Input[chip][i] >> j) & 1){
					PixelCoordinate noisyPix;
					noisyPix.row = (UInt8) j + 32 * (i % 8); //row
					noisyPix.column= (UInt8) i / 8; //column
					noisyPixelsDefault[chip].push_back(noisyPix);
				}
			}
		}
	}
}


// function to get the noisy pixels with the format of the jtag command, 
// matrix eill be returned in the argument 'output'
void HSConfDefault::GetNoisyPixelVectDefault(unsigned chip,  UInt32 output[256]){
	
	
	for (unsigned n = 0 ; n < numberOfRows ; ++n) output[n]=0;

	for (unsigned noisy = 0 ; noisy < this->noisyPixelsDefault[chip].size(); ++noisy ){
		unsigned  row = noisyPixelsDefault[chip][noisy].row;
		unsigned  column = noisyPixelsDefault[chip][noisy].column;

		// will set the correspondent bit
		spdGlobals::insertIntoInt32( output[8*column + (row/32) ] ,1, row%32, 1 ) ;
		cout<< dec<< "default masking chip "<< chip << ", row " <<  row << ", col " <<  column<<endl;

		
	}
	
}

void HSConfDefault::GetNoisyPixelVectDefault(  UInt32 output[numberOfChips*numberOfRows]){
	
	
	for (unsigned chip = 0 ; chip < numberOfChips ; ++chip){
		// will put all data in the big vector in a row
		GetNoisyPixelVectDefault( chip, output + numberOfRows*chip);
	}
}

unsigned HSConfDefault::GetNoisyPixelNumberDefault(void){
	int out = 0;
	for (int chip = 0 ; chip < numberOfChips ; chip ++){
		out += this->GetNoisyPixelNumberDefault(chip);
	}

	return out;
}



void HSConfDefault::SetFileName(const char * File){
	
	FileName= File;
	
	
}


//************************************************************
// Function to move data from this class to the database
//  exists for debugging only
UInt32 HSConfDefault::SetClassDefaultsDb( PixelConfDb & dbData){

	Uint32 * dpiConfig = this->GetDPIConfVectDefault();

	using namespace spdGlobals;

	//************************* Digital Pilot Values ************************
	dbData.setDpiWaitBefRow((Uint8)DPI.GetDPIConfWaitBefRow());
	dbData.setDpiSebMeb((Uint8)DPI.GetDPIConfSebMeb());
	dbData.setDpiMaskChip(DPI.GetDPIConfMaskChip());
	dbData.setDpiEventNumber(DPI.GetDPIConfEventNumb());

	dbData.setDpiStrobeLenght((Uint8)DPI.GetDPIConfStrobeLength());
	dbData.setDpiHoldRow( DPI.GetDPIConfHoldRow());
	dbData.setDpiSkipMode((Uint8)DPI.GetDPIConfSkipMode());
	dbData.setDpiTdo8Tdo9(DPI.GetDPIConfTDO8TDO9());
	dbData.setDpiEnableCeSeq(DPI.GetDPIConfEnableCESeq());
	dbData.setDpiDataFormat(DPI.GetDPIConfDataFormat());

	UInt8 *apiDacVect = this->GetAPIdacVectDefault();

	//************************* Analog Pilot values *********************
	//		API DAC Values 0 = REF HI, REF MID, GTL REFA, GTL REFD, TEST HI, TEST LOW
	dbData.setApiDacRefHi( apiDacVect[0]);
	dbData.setApiDacRefMid( apiDacVect[1]);
	dbData.setApiGtlRefA( apiDacVect[2]);
	dbData.setApiGtlRefD( apiDacVect[3]);
	dbData.setApiAnTestHi( apiDacVect[4]);
	dbData.setApiAnTestLow( apiDacVect[5]);

	//*********************Gol Config******************************
	dbData.setGolConfig( this->GetGOLConfVectDefault());
	
	//************** Dac Values *************************************
	UInt8 * pixelDac = this->GetPixelDACVectDefault();
	
	for (int pixel = 0 ; pixel < numberOfChips ; ++pixel){
		for (int dac = 0 ; dac < NPixelDacs ; ++dac){
			int pos = (pixel*NPixelDacs) + dac;
			dbData.setPixelDac( pixel,dac, pixelDac[ pos]);
			
		}

		dbData.setNoisyPixelsVect(pixel, this->GetNoisyPixVectDefault(pixel));
	}



	return 0;
}

//********************************************************************
//	Function to move the data from the database class to this class 
UInt32 HSConfDefault::LoadClassDefaultsDb(const PixelConfDb & dbData){
	
	UInt32 dpiConfig[2];
	
	dpiConfig[0]=0;
	dpiConfig[1]=0;

	using namespace spdGlobals;

	//************************* Digital Pilot Values ************************
	this->DPI.SetDPIConfWaitBefRow((UInt8)dbData.getDpiWaitBefRow());
	this->DPI.SetDPIConfSebMeb(dbData.getDpiSebMeb());
	this->DPI.SetDPIConfMaskChip((UInt16)dbData.getDpiMaskChip());
	this->DPI.SetDPIConfEventNumb(dbData.getDpiEventNumber());
	this->DPI.SetDPIConfStrobeLength((UInt16) dbData.getDpiStrobeLenght());
	this->DPI.SetDPIConfHoldRow(dbData.getDpiHoldRow());
	this->DPI.SetDPIConfSkipMode(dbData.getDpiSkipMode());
	this->DPI.SetDPIConfTDO8TDO9(dbData.getDpiTdo8Tdo9());
	this->DPI.SetDPIConfEnableCESeq(dbData.getDpiEnableCeSeq());
	this->DPI.SetDPIConfDataFormat(dbData.getDpiDataFormat());
	

	
	//*************** Gol Config **************************************
	this->SetGOLConfVectDefault( dbData.getGolConfig());
	
	//************************* Analog Pilot values *********************
	//API DAC Values 0 = REF HI, REF MID, GTL REFA, GTL REFD, TEST HI, TEST LOW
	UInt8 apiDacVect[6];
	apiDacVect[0]= dbData.getApiDacRefHi();
	apiDacVect[1]= dbData.getApiDacRefMid();
	apiDacVect[2]= dbData.getApiGtlRefA();
	apiDacVect[3]= dbData.getApiGtlRefD();
	apiDacVect[4]= dbData.getApiAnTestHi();
	apiDacVect[5]= dbData.getApiAnTestLow();
	
	this->SetAPIdacVectDefault( apiDacVect);

	//************** Dac Values *************************************

	UInt8 * pixelDac = this->GetPixelDACVectDefault();
	
	for (unsigned pixel = 0 ; pixel < numberOfChips ; ++pixel){
		for (int dac = 0 ; dac < NPixelDacs ; ++dac){
			int pos = (pixel*NPixelDacs) + dac;
			pixelDac[ pos] = dbData.getPixelDac(pixel,dac);
			
		}

		//this->noisyPixelsDefault[pixel]= dbData.getNoisyPixelsVect(pixel);
		
	}

	//need to know how the noisy pixel vector works to do this 
	// I will ask Ivan after

	return 0;
}

int HSConfDefault::LoadDefaultsFromFile(const char * filename){

	char * fileContent;
	unsigned int size;
	fstream file;

	file.open(filename, ios::in|ios::binary|ios::ate);   


	if(file.is_open()){
		file.seekg (0, ios::end);
		size = file.tellg();
		fileContent = new char [size];
		file.seekg (0, ios::beg);
		file.read (fileContent , size);
		file.close();


	}else{
	return 1;
	}


		// I don't know why Ivan is doing this 
		//he keeps the internal properties in memory and does not delete the file
		// looks like he uses this todo some other sutff later,
		// but the memory will never be freed, which does not look very good
	this->SetFileName(filename);
	this->SetFileSize(size);
	this->LoadToMemory((UInt8 *) fileContent);

	delete [] fileContent;
	return 0;

}
void HSConfDefault::LoadToMemory(UInt8 * FileContent){
	 UInt32 i;
	 FileContent += 200;
	 
	 //Exteranl parameter during test
	 //TestCondVddMCM = (float)UInt8ToUInt32(FileContent);
	 FileContent += 4;
     //TestCondVddBus = (float)UInt8ToUInt32(FileContent);
	 FileContent += 4;
	 //TestCondTempBus = (float)UInt8ToUInt32(FileContent);
	 FileContent += 4;
	 //TestCondTempMCM = (float)UInt8ToUInt32(FileContent);
     FileContent += 4;

	 //Ladder IV: inside the vector I(nA) at 10, 20, 30, 40, 50 V
	 for(i=0; i < 5; i++){
		//Ladder0IV[i] = UInt8ToUInt32(FileContent);
		//Ladder1IV[i] = UInt8ToUInt32(FileContent+20);
		FileContent += 4;
	 }
     FileContent += 20;

	 //Pixel DAC Values 0..43 chip 0, 44..87 chip 1, ...
	 for(i=0; i < 440; i++){
		PixDACVect[i] = FileContent[0] ;
        FileContent++;
	 }

	 //API DAC Values 0 = REF HI, REF MID, GTL REF, TEST HI, TEST LOW
	 for(i=0; i < 6; i++){
		APIdacVect[i] = FileContent[0] ;
        FileContent++;
	 }

     
	 //DPI configuration values
	 UInt32 DPIConfVect[2];
     DPIConfVect[0] =0;
	 DPIConfVect[1] =0;
	 GOLConfVect =0;
	 for(i = 0; i< 4; i++){
		DPIConfVect[0] += (UInt32)FileContent[i] << (i*8);
		DPIConfVect[1] += (UInt32)FileContent[i+4] << (i*8);
        GOLConfVect += (UInt32)FileContent[i+8] << (i*8);
	 }
	 this->DPI.SetDPIConf0(DPIConfVect[0]);
	 this->DPI.SetDPIConf1(DPIConfVect[1]);
	 //DPIConfVect[0] = (UInt32)UInt8ToUInt32(FileContent);
     FileContent += 4;
	 //DPIConfVect[1] = (UInt32)UInt8ToUInt32(FileContent);
     FileContent += 4;
	 
	 //GOL configuration values
     //GOLConfVect = (UInt32)UInt8ToUInt32(FileContent);
     FileContent += 4;
     
	 FileContent++;            //in the file there was the ChNumber here
	 //API conversion values for voltages and current: Dig to analog 
	 LoadConversionParam(FileContent);
	 FileContent += 16000;
 
    
     //Noisy and dead pixel  
	 FileContent += SetNoisyPixelVectDefault(FileContent);
    // FileContent += SetDeadPixelVectDefault(FileContent);
    

	 //Note
	 //NoteLengh = FileContent[0]* 256 + FileContent[1];
	 FileContent +=2;
	// if(NoteInFile != NULL) delete NoteInFile;

	 //if(NoteLengh != 0){
		//NoteInFile = new char [NoteLengh];
		//for(i = 0; i < NoteLengh; i++){
		//	NoteInFile[i] = FileContent[i];
		//}
		//FileContent += NoteLengh;
	 //}

	 //TH vector
	 for(i = 0; i < 10; i++){
		//MinTHVect[i] = (UInt8)UInt8ToUInt32(FileContent);
		//MeanTHVect[i] = (float)UInt8ToUInt32(FileContent + 40);
		//RMSNoiseVect[i] = (float)UInt8ToUInt32(FileContent + 80);
		FileContent += 4;
	 }
	 //FileContent += 80;
}



/*
Configuration file content (byte)
200 spare location
16 TestCondDef
40 LadderIV
440 PixDACVect
6 apiVect
8 DPIVect
4 GOLVect
1 Channel
16000AnalConv
2 NoisyPixlengh
n NoisipixVect 1stbyte chip, 2nd bute row 3th byte column
2 DeadPixLengh
n DeadPixelVect
2 NoteLenght
n Note
120 THVect
*/