#include "StdAfx.h"
#include ".\spdMeanTHScan.h"
#include "SPDConfig.h"

spdMeanTHScan::spdMeanTHScan(SPDConfig *spdConfig){
	this->log = & spdLogger::getInstance();
	this->active = false;
	this->parent = spdConfig; 

	this->tpLowIndex = 105;		//identifiers 100 .. 105 for API DACs

	this->triggerNum = 0;
	this->activeRows = 0;
	this->rowCurrent = 0;
	this->rowStep = 0;
	this->rowEnd = 0;
	this->rowStart =0;

	this->tpLowCurrent = 0;
	this->tpLowStep = 0;
	this->tpLowMaximum = 0;
	this->tpLowMinimum = 0;
}

spdMeanTHScan::~spdMeanTHScan(void){

}


	//! Method to restart the mean threshold scan
unsigned spdMeanTHScan::restart(){
	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	active = true;
	scan();
	return 0;
}


	//! Method to start the mean threshold scan
unsigned spdMeanTHScan::start(	unsigned int LowMinimum,
										unsigned int LowMaximum,
										unsigned int LowStep,
										unsigned int startRow,
										unsigned int endRow,
										unsigned int stepRow,
										unsigned int triggerNumber){
	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	timerScan.start("starting mean threshold scan");

	tpLowMinimum = LowMinimum;
	tpLowMaximum = LowMaximum;
	tpLowStep =	LowStep;
	tpLowCurrent = LowMaximum;	// to have the test pulse amplitude increasing from minimum to maximum values

	rowStart = startRow;
	rowEnd = endRow;
	rowStep = stepRow;
	rowCurrent = startRow;

	triggerNum = triggerNumber;
	
		// saves the test pulse low values from the half-staves to set it back at the end of the scan
	for (unsigned hs = 0; hs < NHalfStaves; hs++){
		if (parent->halfStaves[hs]->getChActStatus() == 2){
			UInt8 * APIConf = parent->halfStaves[hs]->GetAPIConf();
			this->tpFromHalfstave[hs] = APIConf[tpLowNum];
		}
	}

	active = true;
	return 0;
}


UInt32 spdMeanTHScan::SetMatrixToAllEnabled(UInt8 rowStart, UInt8 nRows){

	UInt32 matrix[256];

	// clears the matrix
	for (int row = 0 ; row < 256 ; row ++){
		matrix[row]= 0 ;
	}

	if ( (rowStart + nRows) > 255){
		log->log("Error trying to set a row bigger than the number of row");
		return 1;
	}

	// will insert the number of '1s' defined by nRows 
	// starting from the row position (each 8 32 bit = 255 bits)
	for(UInt8 i =0; i < 32; i++){
		spdGlobals::insertBitsIntoArray( &matrix[i*8], 0xffffffff, rowStart, nRows);
	}

			// loading the matrix with active rows in all the half-staves in test state
	for (unsigned int channel = 0; channel < NHalfStaves ; channel++){

		if (parent->halfStaves[channel]->getChActStatus()==2){
			UInt32 error = parent->halfStaves[channel]->LoadHSPixelMatrix(1023, matrix, true, true);
			if (error) return error;
		}
	}

	return 0;
}


UInt32 spdMeanTHScan::SetTestLowToAllEnabled(int testLowValue){

	for (unsigned int channel = 0; channel < NHalfStaves; channel++){
		if (parent->halfStaves[channel]->getChActStatus() == 2){
			parent->halfStaves[channel]->setAnalogPilotDAC(tpLowNum, testLowValue);
		}
	}
	
	return 0;
}


UInt32 spdMeanTHScan::testLowStep(int currentValue, unsigned sleepTime){

	tpLowCurrent= currentValue;
	log->log("setting test low = %d / %d",tpLowCurrent,  tpLowMinimum);

	int error = this->SetTestLowToAllEnabled(tpLowCurrent);
	if (error){
		log->log("ERROR: error trying to set test pulse low during mean th scan");
		return error;
	}

	// will sleep a certain amount of time to let the capacitor in the test pulse low charge
	Sleep(sleepTime);	

	this->setMeanTHScanHeader();

		// sending triggers to all the routers enabled
	error = parent->sendTriggerToEnabledRouters(triggerNum);
	if (error){
		log->log("ERROR: while sending trigger during mean th scan");
		return error;
	}

	return 0;
}

	//! Method for the mean threshold scan
void spdMeanTHScan::scan(){	

	if (active == false) return;
	
	bool TP = true;		// enables the test pulse
		
		// setting 2 rows at the same time and checking the number of missing rows
	unsigned int maxActiveRows = 2;
	unsigned int missingRows = rowEnd - rowCurrent + 1;		// +1 to modify if maxActiveRows changes!!

	if (missingRows > maxActiveRows){
		activeRows = maxActiveRows;
	}
	else{
		activeRows = missingRows;
	}

	log->log("starting  row = %d , activerows = %d ", rowCurrent, activeRows);

	// will set the rows the test pulse to all enabled channels 
	this->SetMatrixToAllEnabled(rowCurrent, activeRows);	
	
	/******************************************************
	first step with 100 milli seconds wait 
	 because this is the biggest change so we need a bigger time to let the capacitor charge
	*/
	int error = this->testLowStep( tpLowMaximum, 100);
	if (error){	 
		return;
	}
	/***********************************************************/

		// looping through the values of test pulse low, increasing the test pulse amplitude
	for (tpLowCurrent = tpLowMaximum -tpLowStep; tpLowCurrent >= tpLowMinimum; tpLowCurrent -= tpLowStep){
			// this is here because apparently we have some sort of multi threading
			// and the stop is changed even in the middle of this
		if (this->active == false)return;
		// 10 millisenconds should be enough
		error = this->testLowStep(tpLowCurrent, 10);
		
	}

	log->log("Finished rows %d - %d", rowCurrent, rowCurrent+activeRows-1);

		// stopping condition for the scan
	if (rowCurrent + activeRows > rowEnd){

			// sets the previous value of test pulse low read from hardware before the scan
		for (unsigned hs = 0; hs < NHalfStaves; hs++){
			if (parent->halfStaves[hs]->getChActStatus() == 2){
				parent->halfStaves[hs]->setAnalogPilotDAC(tpLowNum, tpFromHalfstave[hs]);
			}
		}

		active = false;
		timerScan.stop();

		stringstream msg;
		msg << "Mean threshold scan finished: time elapsed "  << timerScan;
		log->log(msg.str());
	}

	rowCurrent += activeRows;

}


UInt32 spdMeanTHScan::setMeanTHScanHeader(void){
	const unsigned sizeOfHeader = 17;
	unsigned sideOffsetRouter = 0;
	UInt32 scanHeader[sizeOfHeader];
	unsigned int enabledChips[6];	// enabled chips of the 6 half-staves
	//UInt8 DACvector[6];				// array for the 6 API DACs of one half-stave
									// 0=DacRefHigh, 1=DacRefMid, 2=GTLRefA, 3=GTLRefD, 4=TestHigh, 5=TestLow
	unsigned int tpHighDAC[6];		// value of test pulse high DAC of the 6 half-staves
	unsigned int tpLowDAC[6];		// value of test pulse low DAC of the 6 half-staves
	float tpAmplitude[6];			// test pulse amplitudes in mV of the 6 half-staves


	int routerNum = -1;
			// writes the header for the fast or calibration
	for (int channel = 0; channel < NHalfStaves; channel ++) {
		if ((parent->halfStaves[channel]->getChActStatus()==2) && (routerNum != channel/6)){
			routerNum = channel/6;

				// creates the array with the chips activated for the calibration
				// and reads the analog pilot settings
			for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter ++) {
				unsigned int HSnumber = routerNum*6 + HSinRouter;


				if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
					enabledChips[HSinRouter] = parent->halfStaves[HSnumber]->GetDPI()->GetDPIConfMaskChip();

					//parent->halfStaves[HSnumber]->ReadHSApiDAC(DACvector);
					//
					//tpHighDAC[HSinRouter] = DACvector[tpHighNum];
					//tpLowDAC[HSinRouter] = DACvector[tpLowNum];

					UInt8 *memorizedDacVector=parent->halfStaves[HSnumber]->GetAPIConf();

					tpHighDAC[HSinRouter] = memorizedDacVector[tpHighNum];
					tpLowDAC[HSinRouter] = memorizedDacVector[tpLowNum];

				}
				else {
					enabledChips[HSinRouter] = 0;
					tpHighDAC[HSinRouter] = 0;
					tpLowDAC[HSinRouter] = 0;
				}
			}

				// gets the test pulse amplitudes in mV
			this->getTPAmplitude(routerNum, tpAmplitude);

				// if we are on side C, the routerNum will go from 10 to 19
			if (this->parent->side == 'C'){
				sideOffsetRouter = 10;
			}

			scanHeader[0] = routerNum + sideOffsetRouter;
			scanHeader[1] = typeOfScan;
			scanHeader[2] = triggerNum;
			scanHeader[3] = enabledChips[0] + (enabledChips[1] << 10) + (enabledChips[2] << 20);
			scanHeader[4] = enabledChips[3] + (enabledChips[4] << 10) + (enabledChips[5] << 20);
			scanHeader[5] = (tpLowMinimum << 24) + (tpLowMaximum << 16) + (tpLowStep << 8) + tpLowIndex;
			scanHeader[6] = (rowStart << 24) + (rowEnd << 16) + (rowCurrent << 8) + activeRows;
			scanHeader[7] = (tpHighDAC[0] << 24) + (tpLowDAC[0] << 16) + (tpHighDAC[1] << 8) + tpLowDAC[1];
			scanHeader[8] = (tpHighDAC[2] << 24) + (tpLowDAC[2] << 16) + (tpHighDAC[3] << 8) + tpLowDAC[3];
			scanHeader[9] = (tpHighDAC[4] << 24) + (tpLowDAC[4] << 16) + (tpHighDAC[5] << 8) + tpLowDAC[5];
			scanHeader[10] = (UInt32)tpAmplitude[0]*100;
			scanHeader[11] = (UInt32)tpAmplitude[1]*100;
			scanHeader[12] = (UInt32)tpAmplitude[2]*100;
			scanHeader[13] = (UInt32)tpAmplitude[3]*100;
			scanHeader[14] = (UInt32)tpAmplitude[4]*100;
			scanHeader[15] = (UInt32)tpAmplitude[5]*100;
			scanHeader[16] = this->parent->HSData.getGlobalDBVersionNumber();

			this->parent->routers[routerNum]->WriteHeader(scanHeader,sizeOfHeader);

	
		}
	}

	return 0;
}

UInt32 spdMeanTHScan::getTPAmplitude(unsigned int routerNum, float *tpAmplitude){
	UInt32 ADCvector[17];		// array for the 6 API ADCs of one half-stave
	float tpHighADC[6];			// value of test pulse high ADC of the 6 HS
	float tpLowADC[6];			// value of test pulse low ADC of the 6 HS

	const unsigned tpLowAdcPos= 7;
	const unsigned tpHighAdcPos= 6;

	for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter++){
		unsigned int HSnumber = routerNum*6 + HSinRouter;

		if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
            parent->halfStaves[HSnumber]->ReadHSApiADC(ADCvector);

			tpHighADC[HSinRouter] = convertAdcValue(ADCvector[tpHighAdcPos]);
			tpLowADC[HSinRouter] = convertAdcValue (ADCvector[tpLowAdcPos]);
			
			log->logToFile("channel %d : tp High = %f , tp Low = %f , tp Amplitude = %f ", 
				HSinRouter,	
				tpHighADC[HSinRouter] ,
				tpLowADC[HSinRouter], 
				tpHighADC[HSinRouter] - tpLowADC[HSinRouter]);
		}
		else {
			tpHighADC[HSinRouter]=0;
			tpLowADC[HSinRouter]=0;
		}

		tpAmplitude[HSinRouter] = tpHighADC[HSinRouter] - tpLowADC[HSinRouter];
	}


	stringstream msg;

	for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter++){
		msg << tpAmplitude[HSinRouter] << "\t" ;
	}
	
	log->logToFile("tp amplitudes = %s", msg.str().c_str());
	return 0;
}

float spdMeanTHScan::convertAdcValue(unsigned adcCount){
	return (float)(1.379*adcCount + 513.0);
}