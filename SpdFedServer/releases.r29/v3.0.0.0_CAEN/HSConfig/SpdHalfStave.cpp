#include "StdAfx.h"
#include ".\SpdHalfStave.h"


	//! jtag object to manage the ID-DR vectors
	// the compiler does not let me put this as a static variable don't know why
	// so it stays a global variable in this module, which is roughly the same this
HSJTAGStreams HSJTAGFunct;	

//Template function to compare two vectors 
template <class T>
bool compareVectors(T *v1,T *v2,unsigned int size){

	for(unsigned int index = 0 ; index < size ; index ++){
		if (v1[index]!= v2[index])	return false;
	}
	return true;
}


SpdHalfStave::SpdHalfStave(unsigned halfstave,   VMEAccess * vmeAcc){

	logger = &spdLogger::getInstance();
		// will set the internal members 
	this->chNumber= halfstave;

	this->VME = vmeAcc;
	//this->HSJTAGFunct = jtagFunctions;


	char side = (chNumber < 60)? 'A':'C';
			// strings for the service name, I know I could have assigned the right size here but I am lazy
	char serviceNameBus[100];
	char serviceNameMcm[100];

	if (side=='A'){
		sprintf_s( serviceNameBus, "spd_feDimServerA/TEMP_BUS%03d", chNumber);
		sprintf_s( serviceNameMcm, "spd_feDimServerA/TEMP_MCM%03d", chNumber);

		serviceTempBus = new DimService( serviceNameBus, tempBus);
		serviceTempMcm = new DimService( serviceNameMcm, tempMcm);
	}
	else{
		sprintf_s( serviceNameBus, "spd_feDimServerC/TEMP_BUS%03d", chNumber );
		sprintf_s( serviceNameMcm, "spd_feDimServerC/TEMP_MCM%03d", chNumber );

		serviceTempBus = new DimService( serviceNameBus, tempBus);
		serviceTempMcm = new DimService( serviceNameMcm, tempMcm);
	}

	this->activationStatus = 0;

	isConfigured = false;	
}

SpdHalfStave::~SpdHalfStave(void){

	delete serviceTempBus;
	delete serviceTempMcm;
}


// performs a jtag reset to the halfstave
CVErrorCodes SpdHalfStave::jtagReset(){	
	if (this->getChActStatus() == 0 ){
		this->logger->log("ERROR: Channel %d is not active",this->getChannelNumber());
		return spdChannelNotActive;
	}
	Uint32 * ScanVector = HSJTAGFunct.GetJTSResetPixel(1023, this->GetPixChipInChain());
				// to debug the input vectors 
	int sizeStConv = ScanVector[0]/32;
	if ( (ScanVector[0]%32) != 0)sizeStConv++;
	
	            // makes the jtag scan
	CVErrorCodes status = VME->JTAGWriter( chNumber, ScanVector, IR, ScanVector[0],false);
	return status;
}


// executes JTAG validation cycles
CVErrorCodes SpdHalfStave::jtagValidation(UInt32 cycles, UInt32* errorsJtag){
	CVErrorCodes status;
	*errorsJtag = 0;

	if (this->getChActStatus() == 0 ){
		this->logger->log("ERROR: Channel %d is not active",this->getChannelNumber());
		return spdChannelNotActive;
	}
	
	UInt8 skipMode = HSJTAGFunct.GetSkipMode();
	UInt32 * ScanVector = HSJTAGFunct.GetJTSResetPixel(1023, this->GetPixChipInChain());
	
	            // makes the jtag scan
	for (UInt32 loop = 0; loop < cycles; loop++) {
		status = VME->JTAGWriter(chNumber, ScanVector, IR, ScanVector[0], false);
		if (status != 0){
			(*errorsJtag)++;
		}
	}
	
	if ( (*errorsJtag) != 0) {
		this->logger->log("ERROR: errors validating JTAG %d", *errorsJtag);
	}

	delete [] ScanVector;
	return status;
}

// API functions
//------------------------------------------------------------------
//------------------------------------------------------------------

CVErrorCodes SpdHalfStave::LoadHSApiDAC(UInt8* DACVect){

	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();

	if(SkMd > 1) return spdGeneralError;
    HSJTAGFunct.SetSkipMode(SkMd);

    if(DACVect == NULL) DACVect = this->GetAPIdacVectDefault();    //the values are from the default class

    UInt32 ** ScanVector = HSJTAGFunct.SetJTSApiDAC(DACVect, this->GetPixChipInChain());
	
	CVErrorCodes status = VME->JTAGWriter(this->chNumber, ScanVector[0], ScanVector[1],false);
	
	if (status != cvSuccess)logger->log("ERROR: (SpdHalfStave::LoadHSApiDAC) error writing JTAG");

	delete [] ScanVector[0];
	delete [] ScanVector[1];
	delete [] ScanVector;
	return status;

}

// reads the analog pilot dacs
CVErrorCodes SpdHalfStave::ReadHSApiDAC( UInt8* DACVect){

	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

    UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();
    if(SkMd > 1) return spdGeneralError;
	HSJTAGFunct.SetSkipMode(SkMd);

    if(DACVect == NULL){
        return spdGeneralError;
    }
                    // gets the jtag vector to shift in
    UInt32 ** ScanVector = HSJTAGFunct.GetJTSApiDAC(this->GetPixChipInChain());
                    // makes the jtag scan

    CVErrorCodes status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[1]);

	if(status != 0){
		logger->log( "ERROR: (SpdHalfStave::ReadHSApiDAC) error writing to jtag");
		delete [] ScanVector[0];
		delete [] ScanVector[1];
		delete [] ScanVector;
        return status;
	}
                    // gets the returning jtag vector that shifted out
    UInt32  *vectorOut =VME->RdJTVectOut();

    UInt32 ** DecodedVect;

    DecodedVect = HSJTAGFunct.DecodeJTStream(vectorOut); 

	for (int dac = 0 ; dac < 6 ;dac ++){
		DACVect[dac] =(UInt8) DecodedVect[2][dac];
	}
			
	for (int n = 0 ; n < 14 ; n++){
		if (DecodedVect[n]!=NULL) delete [] DecodedVect[n];
	}

	delete [] DecodedVect ;

	delete [] ScanVector[0];
	delete [] ScanVector[1];
	delete [] ScanVector;
	return status;
}


// sets the analog pilot dacs
CVErrorCodes SpdHalfStave::setAnalogPilotDAC(unsigned dacNumber, UInt8 value){

	if (dacNumber >= numberOfApiDacs) {
		logger->log("Error: dac number %d is out of range (0-5)", dacNumber);
		return spdGeneralError;
	}

	UInt8 *currentValues= this->GetAPIConf();

	currentValues[dacNumber] = value;

	return this->LoadHSApiDACCompare( currentValues);
}

//*********************************************************
// writes api dacs reads back and compares
CVErrorCodes SpdHalfStave::LoadHSApiDACCompare(UInt8* DACVect){
	
	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

    if(DACVect == NULL) DACVect = this->GetAPIdacVectDefault();    //the values are from the default class
   
		// writes the values
	CVErrorCodes status = LoadHSApiDAC(DACVect);

	if (status != cvSuccess){
		logger->log("ERROR: while writing analog pilot dacs to the hardware channel %d", this->chNumber);
		return status;
	}
	
	// now we will read the api dacs 
	const unsigned int apiDacVectorSize= 6;
	UInt8 readVector[apiDacVectorSize];
	status = ReadHSApiDAC(readVector);

	if(status != 0) {
		logger->log(" ERROR: whiler reading back analog pilot dacs to values channel %d", this->chNumber);
		return status;
	}

			// sets the "actual values" here
	this->SetAPIConf(DACVect);

	// if it is in debug mode it will not compare the values
	if (VME->isInDebugMode()){return cvSuccess;}

		// compares the vectors 
	if ( !compareVectors(DACVect, readVector, apiDacVectorSize)){
		logger->log(" ERROR: error comparing vectors after reading back the values, channel %d", this->chNumber);

		status = spdGeneralError;
	}
	
	return status;
}

// reads the analog pilot ADC's 
CVErrorCodes SpdHalfStave::ReadHSApiADC( UInt32* ADCVect){

	if (this->getChActStatus() == 0 ){
		logger->log("ERROR: channel %d is not active", this->chNumber);
		return spdChannelNotActive;
	}

    UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();
    if(SkMd > 1) return spdGeneralError;
	HSJTAGFunct.SetSkipMode(SkMd);

    if(ADCVect  == NULL){
		return spdGeneralError;
    }
    // gets the start conversion jtag vector to shift in
    UInt32 ** ScanVector = HSJTAGFunct.SetJTSApiADCStartConv(  this->GetPixChipInChain());
	
    // makes the jtag scan, IR only scan
	CVErrorCodes status = VME->JTAGWriter( chNumber, ScanVector[0], IR, ScanVector[0][0],false);
    
	delete [] ScanVector[0];
    if(status != cvSuccess){
		logger->log("ERROR: (SpdHalfStave::ReadHSApiADC) ERROR writing JTAG channel %d", this->chNumber);
        return status;
	}
	
	// waiting time for the conversion 
	Sleep(1);
	
	// gets the adcs values with a second IR-DR scan
	ScanVector = HSJTAGFunct.GetJTSApiADC( this->GetPixChipInChain() );

	status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[1]);

	if(status != 0){
		logger->log("ERROR:(SpdHalfStave::ReadHSApiADC) error writing JTAG, channel %d", this->chNumber);

		delete [] ScanVector[0];
		delete [] ScanVector[1];
		delete [] ScanVector;
        return status;
	}

       // gets the returning jtag vector that shifted out
    UInt32  *vectorOut =VME->RdJTVectOut();
	
	int sizeVect = VME->GetWordsNumber();

       // here does the decoding
    UInt32 ** DecodedVect;

    DecodedVect = HSJTAGFunct.DecodeJTStream(vectorOut);           
	
	// protection in case the vector is not decoded for some reason
	if (ADCVect[2] != NULL){
		for (int adc = 0 ; adc < 17 ;adc ++){
			ADCVect[adc] = DecodedVect[2][adc];
	
		}
	}
	else{
		logger->log("ERROR: error in the decoding, decoded vector is NULL, channel %d", this->chNumber);
	}
		
	for (int n = 0 ; n < 14 ; n++){
		if (!DecodedVect[n]) delete [] DecodedVect[n];
	}

	delete [] DecodedVect ;
    delete [] ScanVector[0];
    delete [] ScanVector[1];
    delete [] ScanVector;
    return status;
}


// DPI functions
//------------------------------------------------------------------
//------------------------------------------------------------------
//Writes the digital pilot settings to the hardware 
CVErrorCodes SpdHalfStave::LoadHSDpiConfReg( UInt32* DPIVect){

	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	HSJTAGFunct.SetSkipMode((UInt8)this->GetDPI()->GetDPIConfSkipMode());
	
		// this is the defaul configuration part
		// here we put all 10 values in the two registers
		// used by the jtag functions
	if(DPIVect == NULL) DPIVect = this->GetDPIConfVectDefault();     //the values are from the default class
   		
    UInt32 ** ScanVector = HSJTAGFunct.SetJTSDpiConfReg( DPIVect, this->GetPixChipInChain());
	
	CVErrorCodes status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[1],false);
	if(status != cvSuccess)	logger->log("ERROR: error writing to jtag");
	
				// sets the "actual value" here
	//this->GetDPI()->SetDPIConf(DPIVect);

	delete [] ScanVector[0];
	delete [] ScanVector[1];
	delete [] ScanVector;
	
	return status;
}


// writes the internal digital pilot registers to the hardware
CVErrorCodes SpdHalfStave::LoadHSDpiInternalReg( UInt32* RegOutVect, UInt32* DPIVect){

	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	HSJTAGFunct.SetSkipMode((UInt8)this->GetDPI()->GetDPIConfSkipMode());
	
		// this is the defaul configuration part
		// here we put all 10 values in the two registers
		// used by the jtag functions
	if(DPIVect == NULL) DPIVect = this->GetDPIInternalVectDefault();     //the values are from the default class
   		
    UInt32 ** ScanVector = HSJTAGFunct.SetJTSDpiInternalReg( DPIVect, this->GetPixChipInChain());

	CVErrorCodes status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[1]);
	if(status != cvSuccess){
		logger->log("ERROR: error writing to jtag");
    	delete [] ScanVector[0];
	    delete [] ScanVector[1];
	    delete [] ScanVector;

	    return status;
	}

	UInt32  *vectorOut =VME->RdJTVectOut(); 

	// decoding function here
	UInt32 ** DecodedVect = HSJTAGFunct.DecodeJTStream(vectorOut);      

	// copies the registers to the output array
	RegOutVect[0]= DecodedVect[13][0];
	RegOutVect[1]= DecodedVect[13][1];
	RegOutVect[2]= DecodedVect[13][2];

	// delallocates all memory 
	delete [] ScanVector[0];
	delete [] ScanVector[1];
	delete [] ScanVector;

	for (int n = 0 ; n < 14 ; n++){
		if (DecodedVect[n]!=NULL) delete [] DecodedVect[n];
	}
	delete [] DecodedVect ;
			
	return status;
}

// reads the digital pilot registers from the hardware
CVErrorCodes SpdHalfStave::ReadHSDpiConfReg( UInt32 * DpiVector){ 

	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	HSJTAGFunct.SetSkipMode((UInt8)this->GetDPI()->GetDPIConfSkipMode());
			//gets the vector
	UInt32 ** ScanVector = HSJTAGFunct.GetJTSDpiConfReg( this->GetPixChipInChain());
			// makes the jtag scan
	CVErrorCodes status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[1]);

	if(status != cvSuccess){ 
		printf("ERROR: error writing JTAG\n");
		delete [] ScanVector[0];
		delete [] ScanVector[1];
		delete [] ScanVector;
        return status;
	}

	UInt32  *vectorOut =VME->RdJTVectOut(); 
	
	// decoding functio here
	UInt32 ** DecodedVect = HSJTAGFunct.DecodeJTStream(vectorOut);      

	// copies the registers to the output array
	DpiVector[0]= DecodedVect[13][0];
	DpiVector[1]= DecodedVect[13][1];

	// delallocates all memory 
	delete [] ScanVector[0];
	delete [] ScanVector[1];
	delete [] ScanVector;

	for (int n = 0 ; n < 14 ; n++){
		if (DecodedVect[n]!=NULL) delete [] DecodedVect[n];
	}
	delete [] DecodedVect ;
			
	return status;
}

// writes the digital pilot settings, reads backs and compares
CVErrorCodes SpdHalfStave::LoadHSDpiConfRegCompare( UInt32* DPIVect){

	if (this->getChActStatus() == 0 ){
		this->logger->log(" ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	if(DPIVect == NULL) DPIVect = this->GetDPIConfVectDefault();     //the values are from the default class

		// will write the values
	CVErrorCodes status = LoadHSDpiConfReg( DPIVect);
	if (status != cvSuccess ){
		logger->log ("ERROR: error writing digital pilot settings to the hardware");
		return status;
	}

		// reads the values back
	const unsigned int dpiVectorSize= 2;
	UInt32 DPIVectRead[dpiVectorSize];
	status = ReadHSDpiConfReg(  DPIVectRead);

	if (status != cvSuccess ){
		logger->log("ERROR: error while reading digital pilot settings from the hardware");
		
		return status;
	}

	
		// sets the values in the "actual" container
	this->GetDPI()->SetDPIConf(DPIVect);


			// if it is in debug mode it will skip the checking of the data 
	if (VME->isInDebugMode()){return cvSuccess;}

		// will clean the read only values in both vectors 
	DPIVectRead[1] &= 0x03;
	DPIVect[1] &= 0x03;

		// compares the vectors 
	if ( !compareVectors(DPIVect, DPIVectRead, dpiVectorSize)){
		logger->log("ERROR: error comparing vectors after reading back the values in digital pilot settings, channel %d", this->chNumber);
		
		status = spdGeneralError;
	}

	return status;
}


// PixelChips DAC functions
//------------------------------------------------------------------
//------------------------------------------------------------------
// writes one the pixel dacs to the hardware (10 chips)
CVErrorCodes SpdHalfStave::LoadHSPixelDAC( UInt32 ChipSel, UInt8 DACn, UInt8 *DACValues,  bool all){

	UInt32 chipsInChain = this->GetPixChipInChain();

	//count the bits set to 1 in chipsInChain
	UInt32 sel = 0x1;
	UInt32 chipCount = 0;
	for (int i = 0; i < numberOfChips; i ++){
		if(chipsInChain & sel<<i){
			chipCount+=1;
		}
	}
//	logger->log("chips in jtag chain %d", chipCount);

	if (this->getChActStatus() == 0 ){
		logger->log("ERROR: channel %d is not active", this->getChannelNumber());
	
		return spdChannelNotActive;
	}

	UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();
	if(SkMd == 1 || SkMd == 3) return spdGeneralError;
    HSJTAGFunct.SetSkipMode(SkMd);

	UInt32 ** ScanVector = HSJTAGFunct.SetJTSPixelDAC(  &DACn, DACValues, ChipSel, all, chipsInChain);

	CVErrorCodes status = VME->JTAGWriter(this->chNumber,ScanVector[0], ScanVector[1], ScanVector[2], ScanVector[3],false);

	for(UInt8 i=0; i < 4; i++) delete [] ScanVector[i];
	delete [] ScanVector;

	if(status != cvSuccess){
		logger->log("ERROR: error writing to jtag");
		return  status;

	}

	// will set the "actual value" inside the class for all 10 pixel chips
	for (int chip = 0 ; chip < numberOfChips ; chip ++){
		if (spdGlobals::extractFromInt32(ChipSel, chip,1)){

			if (all == false) {
				this->SetPxDAC(DACValues[chip], chip, DACn);
			}
			else{
				this->SetPxDAC(DACValues[0], chip, DACn);
			}
		}
	}

			// if it is in debug mode it will skip the checking of the data 
	if (VME->isInDebugMode()){return cvSuccess;}

    // data to compare with the readout values
	// it will have the least significant bit always set to 1
	UInt32 dataToCompare[numberOfChips];
	for (UInt32 chip = 0 ; chip < chipCount-1 ; chip ++){
		dataToCompare[chip] = this->GetPxDAC(chip, DACn) | 0x1;
	}

	// for the last chip in the chain the values are set correctly
	dataToCompare[chipCount-1] = this->GetPxDAC(chipCount-1, DACn);

	UInt32 dataOut[numberOfChips];

	ReadHSPixelDAC(DACn, dataOut);

	// compares the vectors considering chipCount number of bits
	if (compareVectors(dataToCompare, dataOut, chipCount) == false){
		logger->log("ERROR: error comparing vectors after reading back the values in pixel dac settings, %d", this->chNumber);
		status = spdGeneralError;
	}
	return status;
}

// reads the one pixel dac from the hardware (10 chips)
CVErrorCodes SpdHalfStave::ReadHSPixelDAC(UInt8 DACn, UInt32 * DACOutVect){

	if (this->getChActStatus() == 0 ){
		logger->log("Channel %d in not active",this->getChannelNumber()); 
		return spdChannelNotActive;
	}

	UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();
	if(SkMd == 1 || SkMd == 3) return spdGeneralError;
    HSJTAGFunct.SetSkipMode(SkMd);

	UInt8 DACValue[numberOfChips];

	for (int chip = 0 ; chip < numberOfChips ;chip ++){
		DACValue[chip] = this->GetPxDAC( chip, DACn);

	}

	UInt32 ** ScanVector = HSJTAGFunct.SetJTSPixelDAC( &DACn, DACValue, 0x3ff, false, this->GetPixChipInChain());

	CVErrorCodes status = VME->JTAGWriter(this->chNumber,ScanVector[0], ScanVector[1], ScanVector[2], ScanVector[3]);

	for(UInt8 i=0; i < 4; i++) delete [] ScanVector[i];
	delete [] ScanVector;

	if(status != cvSuccess){
		logger->log("ERROR: error writing JTAG while reading values, halfstave=%d", this->chNumber);
		return status;
	}

	UInt32  *vectorOut =VME->RdJTVectOut(); 

	// decoding function here
	UInt32 ** DecodedVect = HSJTAGFunct.DecodeJTStream(vectorOut);      

	// copies the registers to the output array
	for(UInt8 i =0; i < 10; ++i){
		if(DecodedVect[0][0] & (0x1 << (i+2))) DACOutVect[i]= DecodedVect[i+3][0];
	}

	for (int n = 0 ; n < 14 ; n++){
		if (DecodedVect[n]!=NULL) delete [] DecodedVect[n];
	}

	delete [] DecodedVect ;

	return status;

}

// writes all pixel dacs to the hardware 
CVErrorCodes SpdHalfStave::LoadHSPixelAllDAC( UInt8 * DACVect){
	CVErrorCodes status;
	Uint8 DacValues[numberOfChips];

	if (this->getChActStatus() == 0 ){
		logger->log("ERROR: channel %d is not active", this->getChannelNumber());

		return spdChannelNotActive;
	}

	if(DACVect == NULL) DACVect = this->GetPixelDACVectDefault();

	for (int dac = 0 ; dac < numberOfDacs ; dac ++){
		for (int chip = 0 ; chip < numberOfChips ; chip++){
			DacValues[chip] = DACVect[NPixelDacs*chip + dac];
		}

		status = LoadHSPixelDAC(  0x3ff, dac, DacValues);

		if(status != cvSuccess){
			logger->log("ERROR: ERROR writing dac %d, channel %d rest of dac settings skipped ",dac, this->chNumber );
			break;
		}
	}

	return status;
}


// PixelChips Matrix functions
//------------------------------------------------------------------
//------------------------------------------------------------------
// writes the pixel matrix to the hardware. The argument TP defiens if it is a test pulse or a mask
CVErrorCodes SpdHalfStave::LoadHSPixelMatrix( UInt32 ChipSel, UInt32 * matrix, bool rptFirstMatrix, bool Tp){
	//logger->log("SpdHalfStave::LoadHSPixelMatrix: Start"); //SS-debug
	CVErrorCodes status;
	UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();

	if (this->getChActStatus() == 0 ){
		logger->log("ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	if(SkMd == 1 || SkMd == 3) return spdGeneralError;

    HSJTAGFunct.SetSkipMode(SkMd);
	
	UInt32 ** ScanVector = HSJTAGFunct.SetJTSPixelMatrix( ChipSel, matrix, rptFirstMatrix, Tp, this->GetPixChipInChain());
	for(int i = 2; i < 34; i++){
		//logger->log("SpdHalfStave::LoadHSPixelMatrix: Write cycle=%d",i); //SS-debug
		status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[i], ScanVector[1], ScanVector[i+32],false);
		if(status != cvSuccess){
			logger->log("ERROR: error writing to jtag");
			break;
		}
	}
	
	for(int i = 0; i < 66; i++){
		delete [] ScanVector[i];
	}

	// if it is a masking, it will save it in the actual container class
	if ( (Tp== false && status == 0)){
		this->SetNoisyPixelsVectJT( ChipSel, matrix, rptFirstMatrix);
	}
	//logger->log("SpdHalfStave::LoadHSPixelMatrix: Stop"); //SS-debug
	delete [] ScanVector;
	return status;
}


// method to write the matrix from the database or configuration file into the detector
CVErrorCodes SpdHalfStave::LoadHSPixelMatrixDefault( bool Tp){

	if (this->getChActStatus() == 0 ){
		logger->log("ERROR: channel %d is not active", this->getChannelNumber());
		return spdChannelNotActive;
	}

	CVErrorCodes status;

	
	UInt8 SkMd = this->GetDPI()->GetDPIConfSkipMode();
	if(SkMd == 1 || SkMd == 3) return spdGeneralError;

    HSJTAGFunct.SetSkipMode(SkMd);
	
	if (Tp) {
		logger->log("ERROR: defaul matrix is only available for masking, not for test pulse");
		return spdGeneralError;
	}

	
	logger->logToScr("********* HS %d *************", this->getChannelNumber());

	// gets the matrix from the default class
	UInt32  matrix[numberOfChips*numberOfRows];
	this->GetNoisyPixelVectDefault(matrix);
	// will apply the changes to all pixels
	UInt32 ChipSel=1023;

	UInt32 ** ScanVector = HSJTAGFunct.SetJTSPixelMatrix(ChipSel, matrix, false, Tp, this->GetPixChipInChain());

	for(int i = 2; i < 34; i++){
		status = VME->JTAGWriter(chNumber,ScanVector[0], ScanVector[i], ScanVector[1], ScanVector[i+32],false);
		if(status != cvSuccess){
			logger->log("ERROR: ERROR writing JTAG\n");
			break;
		}
	}
	

	for(int i = 0; i < 66; i++){
		delete [] ScanVector[i];
	}

	// if it is a masking, it will save it in the actual container class
	if ( (Tp== false && status == cvSuccess)){
		this->SetNoisyPixelsVectJT(ChipSel, matrix, false);
	}

	delete [] ScanVector;
	return status;

}

// writes one diff value into the database
void SpdHalfStave::writeDiffValueToDB( int id, 
						int spd_ver, 
						unsigned chip, 
						std::string config_type, 
						std::string data_default,
						std::string data_actual
						){



	// builds the query
	std::string query = "insert into spd_temp_compare_values";

	query += " (id, hs_number, db_ver, pixel_chip, config_type, data_default, data_actual)\n\t";
	query += " values ( ";
	query += spdGlobals::intToStr(id) + ", ";
	query += spdGlobals::intToStr(this->chNumber) + ", ";
	query += spdGlobals::intToStr(spd_ver) + ", ";
	query += spdGlobals::intToStr(chip) + ", '";
	query += config_type + "', '";
	query += data_default + "', '";
	query += data_actual + "' )";
	


	// connects to the database and send the insert into query
	spdDbConnection *con = spdDbConnection::subscribe();
	con->sendSqlCommand( query);
}


// gets the dac name from the dac number and creates a string like "PRE_VTH=180"
std::string formatDacValue(unsigned dacNumber, int dacValue){
	std::string out;

	//using namespace spdGlobals;

	out = spdGlobals::getDacName(dacNumber) ;
	out += "=" + spdGlobals::intToStr(dacValue);

	return out;
}


//the same thing as formatDacValue but for the analog pilot values 
std::string formatAPIValue(unsigned apiDac, int dacValue){
	std::string out;

	out = spdGlobals::getApiDacName(apiDac) ;
	out += "=" + spdGlobals::intToStr(dacValue);

	return out;

}

// function to write all differences to the database 
void SpdHalfStave::writeDiffToDB( int id, int db_ver ){

	using namespace spdGlobals;
	using namespace std;


	// mcm values
	

	const int nApiDacs=6;


	//-----------------------------
	// Mcm values

	DigitalPilot * pilotDef = this->GetDPIDefault();
	DigitalPilot * pilotActual = this->GetDPI();

	if (pilotDef->GetDPIConfWaitBefRow() != pilotActual ->GetDPIConfWaitBefRow() ){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI WaitBefRow =" +intToStr(pilotDef->GetDPIConfWaitBefRow()),
							"DPI WaitBefRow =" +intToStr(pilotActual->GetDPIConfWaitBefRow()) );
	}
	if (pilotDef->GetDPIConfSebMeb() != pilotActual ->GetDPIConfSebMeb()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI SebMeb =" +intToStr(pilotDef->GetDPIConfSebMeb()),
							"DPI SebMeb =" +intToStr(pilotActual->GetDPIConfSebMeb()) );
	}
	if (pilotDef->GetDPIConfMaskChip() != pilotActual ->GetDPIConfMaskChip()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPIMaskChip =" +intToStr(pilotDef->GetDPIConfMaskChip()),
							"DPIMaskChip =" +intToStr(pilotActual->GetDPIConfMaskChip()) );
	}
	if (pilotDef->GetDPIConfEventNumb() != pilotActual ->GetDPIConfEventNumb()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI EventNumber =" +intToStr(pilotDef->GetDPIConfEventNumb()),
							"DPI EventNumber =" +intToStr(pilotActual->GetDPIConfEventNumb()) );
	}

	if (pilotDef->GetDPIConfStrobeLength() != pilotActual ->GetDPIConfStrobeLength()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPIStrobeLength =" +intToStr(pilotDef->GetDPIConfStrobeLength()),
							"DPIStrobeLength =" +intToStr(pilotActual->GetDPIConfStrobeLength()) );
	}
	if (pilotDef->GetDPIConfHoldRow() != pilotActual ->GetDPIConfHoldRow()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI HoldRow =" +intToStr(pilotDef->GetDPIConfHoldRow()),
							"DPI HoldRow =" +intToStr(pilotActual->GetDPIConfHoldRow()) );
	}	
	if (pilotDef->GetDPIConfSkipMode() != pilotActual ->GetDPIConfSkipMode()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPISkipMode =" +intToStr(pilotDef->GetDPIConfSkipMode()),
							"DPISkipMode =" +intToStr(pilotActual->GetDPIConfSkipMode()) );
	}

	if (pilotDef->GetDPIConfTDO8TDO9() != pilotActual ->GetDPIConfTDO8TDO9()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI DO8TDO9 =" +intToStr(pilotDef->GetDPIConfTDO8TDO9()),
							"DPI DO8TDO9 =" +intToStr(pilotActual->GetDPIConfTDO8TDO9()) );
	}
	if (pilotDef->GetDPIConfEnableCESeq() != pilotActual ->GetDPIConfEnableCESeq()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI EnableCESeq =" +intToStr(pilotDef->GetDPIConfEnableCESeq()),
							"DPI EnableCESeq =" +intToStr(pilotActual->GetDPIConfEnableCESeq()) );
	}
	if (pilotDef->GetDPIConfDataFormat() != pilotActual ->GetDPIConfDataFormat()){
		writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							"DPI DataFormat =" +intToStr(pilotDef->GetDPIConfDataFormat()),
							"DPI DataFormat =" +intToStr(pilotActual->GetDPIConfDataFormat()) );
	}
	//----------------------------------------------
	// analog pilot values

	UInt8 * apiDacDef = this->GetAPIdacVectDefault();
	UInt8 * apiDacActual = this->GetAPIConf();

	for (int apiDac = 0 ; apiDac < nApiDacs ; ++apiDac){
		if (apiDacDef[apiDac] != apiDacActual[apiDac]){
			writeDiffValueToDB(	id,
							db_ver,
							0,
							"MCM",
							formatAPIValue( apiDac, apiDacDef[apiDac]),
							formatAPIValue( apiDac, apiDacActual[apiDac]) );

			
		}
	}
	
	//----------------------------------------------------
	// dac values 
	for (unsigned dac = 0 ; dac < NPixelDacs ; ++dac){
		for (unsigned pixel = 0; pixel < NPixels ; ++pixel){

			const UInt8 dacActual = this->GetPxDAC( pixel, dac);
			const UInt8 dacDefault = this->GetPixelDACDefault( pixel, dac);

			if (dacActual != dacDefault){
				writeDiffValueToDB(	id,
									db_ver,
									pixel,
									"DAC",
									formatDacValue( dac, dacDefault),
									formatDacValue( dac, dacActual) );

			}
		}
	}
}

// function that gets the number of differences between what is in the hardware now and what was loaded from the DB
void SpdHalfStave::getDiffCount(unsigned &mcmDiff, unsigned &dacDiff, unsigned &noisyDiff){
	using namespace spdGlobals;
	using namespace std;


	// mcm values

	const int nApiDacs=6;

	//-----------------------------
	// Mcm values

	DigitalPilot * pilotDef = this->GetDPIDefault();
	DigitalPilot * pilotActual = this->GetDPI();

	if (pilotDef->GetDPIConfStrobeLength() != pilotActual ->GetDPIConfStrobeLength()){
		++mcmDiff;
	}

	if (pilotDef->GetDPIConfMaskChip() != pilotActual ->GetDPIConfMaskChip()){
		++mcmDiff;
	}

	if (pilotDef->GetDPIConfSkipMode() != pilotActual ->GetDPIConfSkipMode()){
		++mcmDiff;
	}

	//----------------------------------------------
	// analog pilot values

	UInt8 * apiDacDef = this->GetAPIdacVectDefault();
	UInt8 * apiDacActual = this->GetAPIConf();

	for (int apiDac = 0 ; apiDac < nApiDacs ; ++apiDac){
		if (apiDacDef[apiDac] != apiDacActual[apiDac]){
			++mcmDiff;		
		}
	}
	
	//----------------------------------------------------
	// dac values 
	for (unsigned dac = 0 ; dac < NPixelDacs ; ++dac){
		for (unsigned pixel = 0; pixel < NPixels ; ++pixel){

			const UInt8 dacActual = this->GetPxDAC( pixel, dac);
			const UInt8 dacDefault = this->GetPixelDACDefault( pixel, dac);

			if (dacActual != dacDefault){
				++dacDiff;

			}
		}
	}

}

// resets the Jtag controller for this channel
void SpdHalfStave::resetJtagController(){

	AddressGenerator & addresses = AddressGenerator::getInstance();

	UInt32 Addr = addresses.getRoutAddr(this->chNumber).GetJTResetStateMacAddr(this->chNumber);
	VME->VMEWriteRegister( Addr, 0x1 ); 

	Addr = addresses.getRoutAddr(this->chNumber).GetJTResetFIFOsAddr(this->chNumber);
	VME->VMEWriteRegister( Addr, 0x1 ); 

	Addr = addresses.getRoutAddr(this->chNumber).GetJTResetChAddr(this->chNumber);
	VME->VMEWriteRegister( Addr, 0x1 ); 

}

// configures a halfstave with the default values 
int SpdHalfStave::configureDefault( bool jtagReset, bool digitalPilot, bool analogPilot, bool pixdacs){


	isConfigured = true;
	

	UInt32 status_dpi(0), status_api(0), status_dacs(0), statusReset(0);
	int status;


	if (this->getChActStatus()==0){
		return 16;
	}
	

	if (jtagReset) {
		statusReset=this->jtagReset();
		logger->log("JTAG RESET status: %d\n",statusReset);
	}

	//Sleep(10);

	// DPI default settings
	if (digitalPilot) {
		status_dpi = this->LoadHSDpiConfRegCompare();
		logger->log("Digital pilot status: %d\n",status_dpi);
	}
	// Analog pilot default settings
	if (analogPilot){ 
		status_api = this->LoadHSApiDACCompare();
		logger->log("Analog pilot status: %d\n",status_api);
	}
	// pixel DACS settings
	if (pixdacs){
		status_dacs = this->LoadHSPixelAllDAC();
		logger->log("DACs status: %d\n",status_dacs);
	}

	status = 0;

	if (statusReset != 0){
		status |= 1;
	}

	if (status_dpi != 0){
		status |= 2;
	}

	if (status_api != 0){
		status |= 4;
	}

	if(status_dacs != 0){
		status |= 8;
	}
	

	logger->log("(SpdHalfStave::configureDefault) performed default config channel %d, status = %d",this->chNumber, status );

	return status;
}

// reads the temperatures from the hardware and updates the dim service
void SpdHalfStave::refreshTemperatures(){
	AddressGenerator & addresses = AddressGenerator::getInstance();

	Uint32 tempDigitalValue;
	char mode = 'R';
		// get the address for the registers in the routers through 
	UInt32 tempBusAddr = addresses.getRoutAddr(this->chNumber).GetRdTempChAddr(this->chNumber);

	CVErrorCodes status = VME->VMERegisterAcc( tempBusAddr,  &tempDigitalValue, 1, mode);

	tempBus = this->convertTempBUS( tempDigitalValue);
	tempMcm = this->convertTempMCM( tempDigitalValue);

		// updates the corresponding services
	this->serviceTempBus->updateService();
	this->serviceTempMcm->updateService();
}
