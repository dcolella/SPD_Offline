#pragma once
#include "StdAfx.h"
#include ".\HSConfData.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdDBGlobalVersion.h"
#include "../spdDbConfLib/NoisyPixelDB.h"

HSConfData::HSConfData(void){


	DbVersNumber = 0xffffffff;
	log =&spdLogger::getInstance();
    conn = spdDbConnection::subscribe();
	this->side = 'A';

}

HSConfData::~HSConfData(void){
}




int HSConfData::dumpActualConfToFile(){
	int startHS = 0;
	int endHS = 60;

	
	for (int hs = startHS ; hs < endHS ; hs ++){
		PixelConfDb dbData;
		char fileName[500];
		sprintf_s( fileName, "%sactualHS%03d.txt", dumpConfFolder,HS[hs]->getChannelNumber());
		HS[hs]->SetClassToDb( dbData);
		dbData.dumpToText( fileName, hs);
		
	}
	return 0;
}


int HSConfData::dumpDefaultConfToFile(){
	int startHS = 0;
	int endHS = 60;


	for (int hs = startHS ; hs < endHS ; hs ++){
		PixelConfDb dbData;
		char fileName[500];
		sprintf_s( fileName, "%sdefaultHS%03d.txt", dumpConfFolder,HS[hs]->getChannelNumber());
		HS[hs]->SetClassDefaultsDb( dbData);
		dbData.dumpToText( fileName, hs);
		
	}
	return 0;
}





 

// method to get the data from the data base
UInt32 HSConfData::GetFromDB( long int spdver){

	conn->connect();

	

	cout << "getting version for side "<< side << endl;
	if (spdver < 0)  cout <<"getting the latest version available\n";

	timer t;
		// starts a timer when getting the data to display the time later
	t.start("\tgetting data from the database");

		// gets the values for the top level version
	spdDBGlobalVersion topVersion;
	topVersion.setVersionNumber( spdver);

	cout << "\tglobal version number : "<< dec<< topVersion.getVersionNumber()<< endl;
	cout << "\tversion side A: " << topVersion.getHsVerSideA() << endl;
	cout << "\tversion side C: " << topVersion.getHsVerSideC() << endl;
	cout << "\trun type for this configuration: " ;
	cout << topVersion.getRunType()<<", ";
	cout << topVersion.getRunTypeName().c_str() << endl;
	SpdDetectorConf detectorConf;


	if (side == 'a' || side =='A'){
		sideDbVer= topVersion.getHsVerSideA();
	}
	else{
		sideDbVer= topVersion.getHsVerSideC();
	}
	
	
		// gets the version number
	detectorConf.setVersionNumber(sideDbVer);
		// gets the global version number
	DbVersNumber = topVersion.getVersionNumber();

		// gets data from the data base
	detectorConf.getDataFromDB();

	
	

	for (int i=0; i<60; ++i){
		HS[i]->LoadClassDefaultsDb( detectorConf[i]); 
	}


	t.stop();
	cout << "\t"<< t << " s elapsed, ";
	cout <<"the version currently loaded is: "<< dec<< DbVersNumber<< " -> "<< dec << sideDbVer << "\n\n";
	
	conn->disconnect();

	return 0;
}

Uint32 HSConfData::SaveEnChannelsInDB(){
	std::vector<unsigned int> channelList;
	
	int nChannels = 60;

	cout << "getting enabled channels for side " << side << endl;
	
	
	for( int i = 0; i < NHalfStaves; ++i){
			// if in "test" or in "on" will add the channel number to the channel list
		//if(VME->AG->CHD->GetChActStatus(i)==1 || VME->AG->CHD->GetChActStatus(i)==2){
		if( this->HS[i]->getChActStatus()==1 || this->HS[i]->getChActStatus()==2){
			cout << "adding channel " << HS[i]->getChannelNumber() << " to the list\n";
			channelList.push_back(i);
		}
	}

	cout<< "channels = "<< (int) channelList.size() << endl;
	// will save the data here
	return this->SaveInDB(  channelList);
	this->conn->disconnect();


}


// function to save the dat in the database with only a selection of channels 
long int  HSConfData::SaveInDB(  std::vector<unsigned int> channelList, bool useDefaults){

	cout << "received command to update side " << dec<< side << endl;
	SpdDetectorConf detectorConf;
	conn->connect();
	
	detectorConf.setVersionNumber(sideDbVer);
	detectorConf.getDataFromDB();

	// here we select the actual configuration or the default configuration to save in the database 
	if (useDefaults){
			// will loop over the channel  selection list 
		cout << "\twriting default values in the data base\n";
		for (unsigned i=0; i< channelList.size(); ++i){
			
					// will copy the data here to the db class
			HS[ channelList[i] ]->SetClassDefaultsDb(detectorConf[channelList[i]]);
		}
	}
	else {

		// will loop over the channel  selection list 
		cout << "\twriting actual values in the data base\n";
		cout << "number of enabled channels is "<< (unsigned) channelList.size();
   		for (unsigned i=0; i< channelList.size(); ++i){
			
			unsigned int channel = channelList[i];
			cout << " ******************** changing channel " << (HS[channel]->getChannelNumber()) << "*******************\n";
				// will copy the data here to the db class
			 HS[channel]->SetClassToDb(detectorConf[channel]);
				// copies the data from the defaults class also
			 HS[channel]->LoadClassDefaultsDb(detectorConf[channel]);
		}
	}

	timer t;

	t.start();

	detectorConf.update();
	conn->commit();
	cout <<"\t" << t << " s elapsed, new version created version is: "<< dec << detectorConf.getVersionNumber() << endl;

	// now the version number is the one in the database
	sideDbVer = detectorConf.getVersionNumber();
	cout << "local version is : "<<(int) sideDbVer << "\n";
	
	t.stop();

	conn->commit();
	conn->disconnect();

	return 0;
	
}


// method to save all channels and save them in the database
UInt32 HSConfData::SaveInDB( bool useDefaults ){

	SpdDetectorConf detectorConf;
	conn->connect();
	
	detectorConf.setVersionNumber(sideDbVer);
	
	//int sideOffset = 0;
	//if ( side =='A' || side =='a')sideOffset = 0;
	//else sideOffset = 60;

	// here we select the actual configuration or the default configuration to save in the database 
	if (useDefaults){
		cout << "\twriting default values in the data base\n";
		 for (int i=0; i<60; ++i){
			 HS[i]->SetClassDefaultsDb(detectorConf[i]);
		}
	}
	else {
		cout << "\twriting actual values in the data base\n";
   		for (int i=0; i<60; ++i){
			HS[i]->SetClassToDb(detectorConf[i]);
			HS[i]->LoadClassDefaultsDb(detectorConf[i]);
		}
	}

	timer t;

	t.start();

	detectorConf.update();
	conn->commit();
	cout <<"\t" << t << " s elapsed, new version created version is: " << detectorConf.getVersionNumber() << endl;

	sideDbVer = detectorConf.getVersionNumber();
	cout << "local version is : "<<(int) sideDbVer << "\n";
	
	t.stop();
	conn->commit();
	conn->disconnect();

	return 0;
	
}


// this will link two side database versions together the parameters are all of the versions 
// detector side A version, detector side c version, router side a, router side c and run number
long int HSConfData::linkDbVersions( long int hsSideA, 
				  long int hsSideC, 
				  long int routerSideA,
				  long int routerSideC,
				  long int typeOfRun){

	
	conn->connect();
	spdDBGlobalVersion topVersion;
	topVersion.setVersionNumber(DbVersNumber);
	
	cout << "linking versions "<< hsSideA << " , " << hsSideC << endl;
	long int version = topVersion.update(hsSideA,hsSideC,routerSideA, routerSideC, typeOfRun);

	if (version!=DbVersNumber)cout << "new global version number is: " << version<< endl;
	else cout << "old global version (" <<version <<") is still valid, no changes made in the database\n";

	conn->commit();
	conn->disconnect();

	return version;
}

// links the current side version with  the other versions 
// side - ther side of this version, otherSideConf - the version of the other side
// then router version side A, router version side c, and run type
long int HSConfData::linkDbVersions( 
					long int otherSideConf, 
					long int routerConfA,
					long int routerConfC,
					long int typeOfRun){

	if (side == 'a' || side =='A'){
		return linkDbVersions( sideDbVer, otherSideConf,routerConfA, routerConfC, typeOfRun);
	}
	else return linkDbVersions( otherSideConf, sideDbVer, routerConfA, routerConfC, typeOfRun);
}

// masks the noisy pixels with the data straight from the DB
int HSConfData::maskDetectorFromDB(int version ){
	NoisyPixelDB noisy;

	log->log("Received command to mask from the DB version=%d", version);

	int error = noisy.getFromDb( this->side, version);
	if( error != 0){
		log->log("(maskDetectorFromDB) Error gettting the mask from the database");
		return error;
	}

	this->maskVerNumber = noisy.getVersionNumber();

	log->log("got masking version %d", noisy.getVersionNumber());
	log->log("Setting hs default containers");
	for(unsigned hs = 0 ; hs < nHalfstaves ; hs ++){
		for(unsigned chip = 0 ; chip < nChips ; chip ++){
			
			this->HS[hs]->SetNoisyPixelVectDefault(chip, noisy.getHsNoisy(hs, chip));
		}
	}


	// here it will write the mask to the hardware
	for(unsigned hs = 0 ; hs < nHalfstaves ; hs ++){
		if (HS[hs]->getChActStatus() > 0){
			log->log("masking hs %d from the database", (this->side == 'A' )?hs:60+hs);
			int error = HS[hs]->LoadHSPixelMatrixDefault();
			if (error){	
				log->log("(maskDetectorFromDB) error masking HS %d", (this->side == 'A' )?hs:60+hs);
				return error;
			}
		}
	}
	
	conn->disconnect();
	return 0;
}

// masks the noisy pixels of one half-stave with the data from the DB
int HSConfData::maskHalfStaveFromDB(int version, int halfStave ){
	NoisyPixelDB noisy;

	log->log("Received command to mask half-stave %d from the DB version=%d", halfStave, version);

	int error = noisy.getFromDb( this->side, version);
	if( error != 0){
		log->log("(maskDetectorFromDB) Error gettting the mask from the database");
		return error;
	}

	this->maskVerNumber = noisy.getVersionNumber();

	log->log("got masking version %d", noisy.getVersionNumber());
	log->log("Setting hs default containers");
	for(unsigned chip = 0 ; chip < nChips ; chip ++){
		
		this->HS[halfStave]->SetNoisyPixelVectDefault(chip, noisy.getHsNoisy(halfStave, chip));
	}

	// here it will write the mask to the hardware
	if (HS[halfStave]->getChActStatus() > 0){
		log->log("masking half-stave %d from the database", (this->side == 'A' )?halfStave:60+halfStave);
		int error = HS[halfStave]->LoadHSPixelMatrixDefault();
		if (error){	
			log->log("(maskDetectorFromDB) error masking HS %d", (this->side == 'A' )?halfStave:60+halfStave);
			return error;
		}
	}

	conn->disconnect();
	return 0;
}