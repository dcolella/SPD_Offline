#pragma once

#include "StdAfx.h"



#include "..\spdDbConfLib\spdDbConnection.h"
#include "..\spdDbConfLib\SpdDetectorConf.h"
#include "..\spdDbConfLib\spdLogger.h"


#include "SpdHalfStave.h"

const char dumpConfFolder[] =".\\DumpConf\\" ;

const int NHalfStaves = 60;

class SpdHalfStave;

//! Instance to manage database configuration of the halfstaves
/*! this class somehow shares functionalitie with the SPDConfig class
it was created long time ago when Ivan was in the development, 
I think that this class should slowly disapear and move the functionality to the SPD config class */
class HSConfData{
private:
	SpdHalfStave * HS[NHalfStaves];
    
	spdLogger *log;
    spdDbConnection * conn;
	
	
	long int DbVersNumber;
	long int sideDbVer;
	long int maskVerNumber;
	
	char side;

	 
public:
	HSConfData(); 
	HSConfData( SpdHalfStave * HS[NHalfStaves]); 
	~HSConfData(void);

	void SetHSFilePointer( SpdHalfStave *halfstaves[], char detSide){
		for (int hs = 0; hs < NHalfStaves ; hs ++)HS[hs] = halfstaves[hs];
		side = detSide;

	};	    	

	//! method to save the enabled channels configuration in the database
	Uint32 SaveEnChannelsInDB();

	//! gets the current side version number of this onjec
	long int getSideDBVersionNumber(){return sideDbVer;};
	//! gets the current global version of the configuration
	long int getGlobalDBVersionNumber(){return DbVersNumber;};

	//! this one disapeared because the default of -1 will make the same
	//UInt32 GetFromDB(char side );
	UInt32 GetFromDB( long int spdver=-1);

		//! for the moment the useDefaults is for debugging only, 
		//!its possible to read from file and save in db this way
	UInt32 SaveInDB(  bool useDefaults=false);

		
	
		// in this one we select a list of hs to update the data
	long int SaveInDB( std::vector<unsigned int> channelList,  bool useDefaults=false);

	
    UInt8 *  GetFromFile(UInt8 ChNumb);
		
		// methods to dump the configurations to file 
	int dumpActualConfToFile();
	int dumpDefaultConfToFile();


	// this will link two side database versions together the parameters are all of the versions 
	// detector side A version, detector side c version, router side a, router side c and run number
	long int linkDbVersions( long int hsConfA, 
				  long int hsConfC, 
				  long int routerConfA,
				  long int routerConfC,
				  long int typeOfRun);

	//! links the current side version with  the other versions 
	/*! \param otherSideConf the version of the other side
		\param routerConfA router configuration for side A
		\param routerConfC router configuration for side C
		\param typeOfRun run type*/
	long int linkDbVersions(  
				  long int otherSideConf, 
				  long int routerConfA=0,
				  long int routerConfC=0,
				  long int typeOfRun=0);

	//! masks the noisy pixels with the data straight from the DB
	int maskDetectorFromDB(int version = -1);

	//! masks the noisy pixels of one half-stave with the data from the DB
	int maskHalfStaveFromDB(int version, int halfStave);

	long int getMaskDbVersion(){return this->maskVerNumber;};
};

