#include "StdAfx.h"
#include ".\spdrouter.h"

#include "..\spdDbConfLib\spdGlobals.h"

using spdGlobals::extractFromInt32;

SpdRouter::SpdRouter( unsigned routNumber, VMEAccess * vme)
{

	log = &(spdLogger::getInstance());


	this->vmeDriver = vme;
	this->routerNumber = routNumber;
	
		// this will do the shift of the data 
	this->routerAddresses.ShiftAddresses(16);

		// this will initialize all internal addresses 
	this->routerAddresses.SetRoutBoardAddr( routNumber +1);

	// will initialize the router error services
	this->routerError = 0;
	stringstream serviceName;

	if (this->routerNumber < 10){
		serviceName << "spd_feDimServerA/ROUTER_ERROR"<< routerNumber;
	}
	else{
		serviceName << "spd_feDimServerC/ROUTER_ERROR"<< routerNumber;
	}

	this->serviceRouterError = new DimService(serviceName.str().c_str(),routerError);

}

SpdRouter::~SpdRouter(void)
{
	delete this->serviceRouterError;
}


CVErrorCodes SpdRouter::setMcmStimuly(UInt8 channelInRouter, UInt32 enable,  UInt32 ColumnToSet){

	AddressGenerator &adresses = AddressGenerator::getInstance();

	bool isRouterStopped = this->getStopSM();

	// if the router is not stopped already we will stop the state machine (required to set or read lrx registers)
	if(isRouterStopped == false){
		this->setStopSM(true);
	}

	
	UInt8 channel = 6*this->routerNumber + channelInRouter;
	
	
	UInt32 addr = adresses.getLRxAddr(channel).GetEnableMCMStimuliDataAddr();


	CVErrorCodes status = cvSuccess;


	log->log("setting mcm stimuly enable: channel %d  address = 0x%x, value = 0x%x",channel, addr, enable );
	status = vmeDriver->VMERegisterAcc( addr, &enable, 1, 'W');   //enable MCM stimuly data out
	

	// now will write the column in the register
	addr = adresses.getLRxAddr(channel).GetMCMStimuliDataAddr();

	// this is particualr weird why this register value has something to do with the 
	// router number
	//UInt32 column = 0xffffffff - ((2^ColumnToSet - 1) << this->routerNumber);
	
	// will insert a zero in the correct column to set 
	UInt32 column = 0xffffffff;
	insertIntoInt32(  column, 0,this->routerNumber%10, ColumnToSet);

	//log->log("setting mcm stimuly column: channel %d  address = 0x%x, value = 0x%x",channel, addr, column );
	
	status = status | vmeDriver->VMERegisterAcc( addr, &column, 1, 'W');   //set the column to send out

	addr = adresses.getLRxAddr(channel).GetTestRegAddr();
	//log->log("column:  channel %d  address = 0x%x, value = 0x%x",channel, addr, column );
	
	status = status | vmeDriver->VMERegisterAcc( addr, &enable, 1, 'W');   //start MCM Stimuly
	
			
	if(isRouterStopped == false){
		this->setStopSM(false);
	}
	
	return status;
	
}


// sends triggers to the router
CVErrorCodes SpdRouter::SendTrigger( UInt32 TriggerN, bool Delay, bool l2y, bool l1, bool tp, bool l2n){ //Trigger Number = 0..26 
	
	UInt32 Addr = this->routerAddresses.GetSendTriggSeqAddr();

	UInt32 Data = 0;
	//UInt32 Data = (  (  ((UInt32)(!tp) << 4) + ( (UInt32)(!l1) << 3) +  
	//				   ((UInt32)(!l2y) << 1) + (UInt32)(!Delay)  )       << 27) + TriggerN; 


	insertIntoInt32( Data,!Delay,27,1);

	// for the moment the l2n is always disactivated (the logic is inverted)
	insertIntoInt32( Data,!l2n,28,1);

	insertIntoInt32( Data,!l2y,29,1);
	insertIntoInt32( Data,!l1,30,1);
	insertIntoInt32( Data,!tp,31,1);
	
	Data +=TriggerN;

	//log->log("writting data word  addr %x, data %x ",Addr, Data);

	return vmeDriver->VMEWriteRegister( Addr, Data);

}
// writes the calibration header in the router
CVErrorCodes SpdRouter::WriteHeader( UInt32 header[], UInt32 headerLenght){
	CVErrorCodes result;
	//log->log("SpdRouter::WriteHeader - start");//SS-debug
	UInt32 Addr = routerAddresses.GetResetExtraHeaderFifoAddr();
	//log->log("SpdRouter::WriteHeader Addr:%d, Header[0]:%d",Addr,header[0]);//SS-debug
	result = vmeDriver->VMEWriteRegister( Addr, header[0] ); 
	//log->log("SpdRouter::WriteHeader result:%d",result);//SS-debug

	Addr = routerAddresses.GetExtraHeaderFifoAddr();
	//log->log("SpdRouter::WriteHeader Addr:%d, HeaderLength:%d",Addr,headerLenght);
	result =  vmeDriver->VMERegisterAcc( Addr, header , headerLenght, 'W'); 
	//log->log("SpdRouter::WriteHeader result:%d",result);//SS-debug
	return result;
}

//writes the busy flag
CVErrorCodes SpdRouter::setBusyFlag( bool busy){
	
	UInt32 Addr = routerAddresses.GetCntrRegAddr();   
	UInt32 cntrlRegister;

	vmeDriver->VMEReadRegister( Addr, &cntrlRegister);  

			// inserts the new busy bit in the old control register
	spdGlobals::insertIntoInt32( cntrlRegister, busy, 8, 1);

    return vmeDriver->VMEWriteRegister( Addr, cntrlRegister); 
}


bool SpdRouter::getIdleFlag(){
	UInt32 RoutAddr = routerAddresses.GetStatusReg2Addr(); 
	UInt32 RouterStatus ;
	
	vmeDriver->VMEReadRegister(RoutAddr, &RouterStatus);

	return (RouterStatus & 0x4)? true:false;

}
// stops the state machine of the router 
bool SpdRouter::getStopSM(){
	UInt32 RouterCtlOrig;
	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEReadRegister(RoutAddr, &RouterCtlOrig);

			// will return true if the bit is not zero
	bool stopSM = ((RouterCtlOrig & 0x400) != 0) ;
	return stopSM;
}


// resets all enabled halfstaves in one router
CVErrorCodes SpdRouter::dataResetHS(){
	UInt32 resetAddress = routerAddresses.GetResetPixelAddr();

	// here I am writting 2 times because of an unknown problem while 
	//performing this data reset
	CVErrorCodes status = vmeDriver->VMEWriteRegister(resetAddress, 0);
	status = status | vmeDriver->VMEWriteRegister(resetAddress, 1);

	log->log("INFO: Performed data reset router %d, adress %x",this->routerNumber, resetAddress );

	return status;
}


// sets the stop state machine in the router control register (bit 10)
CVErrorCodes SpdRouter::setStopSM( bool stopSM){

	UInt32 RouterCtlOrig;
	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEReadRegister(RoutAddr, &RouterCtlOrig);

	UInt32 RouterCtl;

	if (stopSM){
		RouterCtl = RouterCtlOrig | 0x400;
	}
	else {
		RouterCtl = RouterCtlOrig & 0xfffffffffffffbff;// 0x400;
	}

	return vmeDriver-> VMEWriteRegister( RoutAddr, RouterCtl);   //setting stopFSM mode
	
}


// will read all errors from a router and return them in a nice vector
vector<SpdRouter::RouterError>  SpdRouter::readErrorList(){

	// starts reading from the spm base address
	UInt32 spmPointer = routerAddresses.GetSPMBaseAddr();
	UInt32 spmRegister;
	vector<RouterError> out;
	unsigned const numberOferrorRegisters = 4;

	while (1){
		UInt32 errorRegister[numberOferrorRegisters];

		// the reading two times is because of a problem in the router 
		// michelle is trying to fix it 
		vmeDriver->VMEReadRegister(spmPointer, &spmRegister);
		vmeDriver->VMEReadRegister(spmPointer, &spmRegister);

		spmPointer +=4;

		unsigned byteCheck = extractFromInt32(spmRegister,24,8 );

		if (byteCheck != 0xaa){
			//log->logToScr("ERROR: no valid error found in the router %d, register = %x  ", routerNumber,spmRegister );
			break;
		}
		if ( (spmRegister & 0x000fffff)== 0x00055555 ){
			break;
			// the 0x55555555 marks the end of data in the router error
		}
		if (spmRegister == 0xFFFFFFFF){

			//log->logToScr("ERROR: format error in the SPM register (0x%x), is router %d plugged? ",spmRegister, routerNumber );
			
			break;
		}

		errorRegister[0]= spmRegister;
		
		for (unsigned index =1 ; index < numberOferrorRegisters; index ++){
			vmeDriver->VMEReadRegister(spmPointer, &spmRegister);
			vmeDriver->VMEReadRegister(spmPointer, &spmRegister);

			spmPointer +=4;
			
			errorRegister[index]= spmRegister;
		}

		// after a router reset, the first errorRegister is AA, the second is 55555
		// in this case we skip the parsing of the data
		if ( (errorRegister[2] & 0x000fffff)== 0x00055555 ){
			break;
		}

		// now it will parse the error
		RouterError error;

		// it will 
		error.bunchCrossing = extractFromInt32( errorRegister[0], 0 , 12);
		error.errorOrder = extractFromInt32(errorRegister[0], 12, 12);
		error.errorID =  extractFromInt32(errorRegister[1], 0, 10);
		error.errorClass = extractFromInt32(errorRegister[1], 10, 22);
		error.details1 = errorRegister[2];
		error.details2 = errorRegister[3];

		// inserts the error in the list
		out.push_back(error);

		// if we reach the limit of the number of errors we will stop here
		// TODO: clean the router memory if maximum number of errors is reached
		// TODO: add an errors class and error type in the router database so to add this error directly in the router error table
		if (out.size() >= maxNumberOfRouterErrors){
			log->log("ERROR: reached maximum number of errors in router %d, the remaining errors will be ignored", this->routerNumber);

			// this is the maximum value limited in the verilog code for router errors 
			const UInt32 maxAddress =  routerAddresses.GetSPMBaseAddr()+ 0x3fffff;
			vmeDriver->VMEReadRegister( maxAddress, &spmRegister);

			// resets the router error manager address state machine 
			const UInt32 addr = routerAddresses.GetErrManagerReset();
			vmeDriver->VMEWriteRegister( addr, 0x1);
			
			break;
		}
	}

	if (out.size() > 0){
		routerError =(int) out.size();
		log->log("INFO: ERROR HANDLER - router %d with %d errors during last pooling", this->routerNumber, routerError);
		// if there are errors it will send the DIM service
		this->serviceRouterError->updateService(routerError);
	}

	// returns the list of errors
	return out;
}


// reads the error mask register
UInt32 SpdRouter::readErrorMask(){
	this->setNoDataToDAQ(1);
	UInt32 errorMaskAddress = routerAddresses.GetErrorMask();
	UInt32 maskRegister;

	vmeDriver->VMEReadRegister(errorMaskAddress, &maskRegister);
	this->setNoDataToDAQ(0);

	log->log("Reading router error mask %x", maskRegister);
	return maskRegister;
}


UInt32 SpdRouter::readRouterBusyTimes(UInt32 values[]){

	UInt32 address, routerRegister;

	address = routerAddresses.GetTimeBusyDaqAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[0]=routerRegister;

	address = routerAddresses.GetTimeBusyRouterAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[1]=routerRegister;

	address = routerAddresses.GetTimeBusyHsAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[2]=routerRegister;

	address = routerAddresses.GetTimeBusyTriggersL1FifoAddr();
	vmeDriver->VMEReadRegister(address, &routerRegister);

	values[3]=routerRegister;

	//address = routerAddresses.GetL0CounterAddr();
	//vmeDriver->VMEReadRegister(address, &routerRegister);

	//values[4]=routerRegister;

	return 0;
}


// will write the error mask register
void SpdRouter::writeErrorMask(UInt32 mask){
	log->log("Writing router error mask %x", mask);
	this->setNoDataToDAQ(1);
	UInt32 errorMaskAddress = routerAddresses.GetErrorMask();
	vmeDriver->VMEWriteRegister(errorMaskAddress, mask);
	this->setNoDataToDAQ(0);
}



// sets the no data to DAQ bit in the control register
void SpdRouter::setNoDataToDAQ(bool value){
	UInt32 controlReg;
	controlReg = this->readControlReg();

		// insert the no data to DAQ bit (bit 1) in the old control register
	int offset = 1;
	spdGlobals::insertIntoInt32(controlReg, value, offset, 1);

	this->writeControlReg(controlReg);
}


// gets the no data to DAQ bit from the control register
bool SpdRouter::getNoDataToDAQ(){
	UInt32 controlReg;
	controlReg = this->readControlReg();

	int offset = 1;
	UInt32 value = spdGlobals::extractFromInt32(controlReg, offset, 1);

	return (value? true:false);
}



// sets the dual port memory sample mode bit in the control register
void SpdRouter::setDpmSampleMode(bool value){
	UInt32 controlReg;
	controlReg = this->readControlReg();

		// insert the DPM sample mode bit (bit 0) in the old control register
	int offset = 0;
	spdGlobals::insertIntoInt32(controlReg, value, offset, 1);

	this->writeControlReg(controlReg);
}

// gets the dual port memory sample mode bit from the control register
bool SpdRouter::getDpmSampleMode(){
	UInt32 controlReg;
	controlReg = this->readControlReg();

	int offset = 0;
	UInt32 value = spdGlobals::extractFromInt32(controlReg, offset, 1);

	return (value? true:false);
}

// sets the exclude ttc bit in the control register
CVErrorCodes SpdRouter::setExcludeTriggersFromTTC(bool value){
	CVErrorCodes status = cvSuccess;
	UInt32 controlReg;
	controlReg = this->readControlReg();

		// insert the exclude ttc bit (bit 21) in the old control register
	int offset = 21;
	spdGlobals::insertIntoInt32(controlReg, value, offset, 1);

	this->writeControlReg(controlReg);

	return status;
}

// gets the exclude ttc bit from the control register
bool SpdRouter::getExcludeTriggersFromTTC(){
	UInt32 controlReg;
	controlReg = this->readControlReg();

	int offset = 21;
	UInt32 value = spdGlobals::extractFromInt32(controlReg, offset, 1);

	return (value? true:false);
}


// will read the control register of the router
UInt32 SpdRouter::readControlReg(){
	UInt32 controlRegister;
	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	vmeDriver->VMEReadRegister(RoutAddr, &controlRegister);

	return controlRegister;
}

//writes the control register of one router
/*	control reg information:
		bit 0 :		DPM Sample mode
		bit 1 :		no data to DAQ
		bits 2-7 :	HS 0-5 mask (0 -> hs ennabled, 1 -> hs masked)
		bit 8:		Busy flag
		bit 9 :		enable TP in L0
		bit 10:		stop all state machine
		bit 19 :	enable router header
		bit 20:		enable orbit counter
		bit 21:		exclude ttc			*/
CVErrorCodes SpdRouter::writeControlReg(UInt32 regValue){
	CVErrorCodes status = cvSuccess;

	UInt32 RoutAddr = routerAddresses.GetCntrRegAddr();
	status = vmeDriver->VMEWriteRegister( RoutAddr, regValue);

	return status;
}


CVErrorCodes SpdRouter::writeControlReg(	bool dpmSampleMode,
											bool noDataToDaq,
											bool enableHS[6],
											bool busy,
											bool enTpL0,
											bool stopAllSM,
											bool enRouterHeader,
											bool enOrbitCounter,
											bool excludeTTC ){

	CVErrorCodes status = cvSuccess;
	UInt32 regValue = 0 ;

	insertIntoInt32( regValue, dpmSampleMode, 0,1);
	insertIntoInt32( regValue, noDataToDaq, 1,1);

	for (unsigned hs = 0 ; hs < NHalfstavesInRouter ; hs ++){
		// inserts the mask in the control register
		insertIntoInt32( regValue,!enableHS[hs],2+hs,1);
	}
	
	insertIntoInt32( regValue, busy, 8,1);
	insertIntoInt32( regValue, enTpL0, 9,1);
	insertIntoInt32( regValue, stopAllSM, 10,1);
	insertIntoInt32( regValue, enRouterHeader, 19,1);
	insertIntoInt32( regValue, enOrbitCounter, 20,1);
	insertIntoInt32( regValue, excludeTTC, 21,1);

	status = this->writeControlReg(regValue);
	return status;
}

CVErrorCodes SpdRouter::writeControlReg(	bool dpmSampleMode,
											bool noDataToDaq,
											bool busy,
											bool enTpL0,
											bool stopAllSM,
											bool enRouterHeader,
											bool enOrbitCounter,
											bool excludeTTC ){

	CVErrorCodes status = cvSuccess;
	UInt32 regValue =this->readControlReg();;

	insertIntoInt32( regValue, dpmSampleMode, 0,1);
	insertIntoInt32( regValue, noDataToDaq, 1,1);
	insertIntoInt32( regValue, busy, 8,1);
	insertIntoInt32( regValue, enTpL0, 9,1);
	insertIntoInt32( regValue, stopAllSM, 10,1);
	insertIntoInt32( regValue, enRouterHeader, 19,1);
	insertIntoInt32( regValue, enOrbitCounter, 20,1);
	insertIntoInt32( regValue, excludeTTC, 21,1);

	status = this->writeControlReg(regValue);
	return status;
}


// performs a router reset and 3 link receivers resets
void SpdRouter::reset(){

	
	// first we stop the error handling 
	UInt32 originalMask = this->readErrorMask();
	//log->logToScr("stopping error mask original value = %x", originalMask);
	this->setNoDataToDAQ(1);
	this->writeErrorMask(0);
	

	// performes a rotuer reset and a flush dpm command
	UInt32 RoutAddr = routerAddresses.GetRtVMEResetAddr();
	vmeDriver->VMEWriteRegister( RoutAddr, 0x1);

	RoutAddr = routerAddresses.GetFlushDPMAddr();
	vmeDriver->VMEWriteRegister( RoutAddr, 0x1);

	// this performs a link receiver reset
	UInt32 lrxResetAddr =routerAddresses.GetResetLinkRxAddr();
	vmeDriver->VMEWriteRegister(lrxResetAddr, 0x1);


	//log->logToScr("restarting error mask with value = %x", originalMask);
	// rewrites the error handling
	this->setNoDataToDAQ(0);
	this->writeErrorMask(originalMask);
	
}

// performs just a router reset (to be used if the readout time is larger than 0)
void SpdRouter::resetRouterBusy(){
	UInt32 RoutAddr;
	UInt32 L0L1value;
	UInt32 controlRegister;

	// first we stop the error handling 
	UInt32 originalMask = this->readErrorMask();
	//log->logToScr("stopping error mask original value = %x", originalMask);
	this->setNoDataToDAQ(1);
	this->writeErrorMask(0);

	// reads the L0-L1 time and control register
	RoutAddr = routerAddresses.GetTimeL0L1();
	vmeDriver->VMEReadRegister(RoutAddr, &L0L1value);
	controlRegister = this->readControlReg();

	// performes a router reset 
	log->log("resetting router %d", this->routerNumber);
	RoutAddr = routerAddresses.GetRtVMEResetAddr();
	vmeDriver->VMEWriteRegister( RoutAddr, 0x1);

	// writes the L0-L1 time and control register
	RoutAddr = routerAddresses.GetTimeL0L1();
	vmeDriver->VMEWriteRegister( RoutAddr, L0L1value);
	this->writeControlReg(controlRegister);

	//log->logToScr("restarting error mask with value = %x", originalMask);
	// rewrites the error handling
	this->setNoDataToDAQ(0);
	this->writeErrorMask(originalMask);
	
}

// writes the L1 link receiver fastor delay 
CVErrorCodes SpdRouter::writeLrxFastorL1Delay(unsigned lrx, UInt32 fastorDelay){
	CVErrorCodes status = cvSuccess;
	AddressGenerator & addresses = AddressGenerator::getInstance();

	bool isRouterStopped = this->getStopSM();
	if (isRouterStopped == false){
		this->setStopSM( true);
	}
	unsigned chNumb = 6*routerNumber + lrx;
	log->logToScr( "writing lrx fastor delay  channel %d, value %d", chNumb, fastorDelay);

	UInt32 lrxAddr = addresses.getLRxAddr( chNumb).GetLRXFastorL1DelayAddr();

	log->logToScr("Writing lrx fo delay addres %x, value" , lrxAddr, fastorDelay);
	status = vmeDriver->VMEWriteRegister(lrxAddr, fastorDelay);

	status = this->setStopSM(isRouterStopped);
	return status;
}

// writes the L1 time in one router
void SpdRouter::writeL0L1Time(UInt32 value){

	UInt32 RoutAddr = routerAddresses.GetTimeL0L1();
	vmeDriver->VMEWriteRegister( RoutAddr, value);
}

// configures the router 
// performs the reset of both the router and link receivers
// writes the L0-L1 time, writes the L1 fastor delay in the lrx's
void SpdRouter::configureRouter(UInt32 options,
								UInt32 ctlrReg, 
								UInt32 L0L1Time, 
								UInt32 lrxFoDelays[]){

	AddressGenerator & addresses = AddressGenerator::getInstance();

		//options : bit0 enable router reset, bit1 configure router, bit2 L0L1 time, bit3 lrx fastor L1 time
	bool routerReset =		(extractFromInt32( options,0,1)== 1) ? true: false;
	bool routerConfigure =	(extractFromInt32( options,1,1)== 1) ? true: false;
	bool setL0L1Time =		(extractFromInt32( options,2,1)== 1) ? true: false;
	bool setL1FastorDelay = (extractFromInt32( options,3,1)== 1) ? true: false;

	if (routerReset){ 
		this->reset();
		this->resetJtagcontrollers(); // resets also the jtag controllers in the router just to be sure(Michele bug)
	}

	if (routerConfigure){ 
		this->writeControlReg(ctlrReg);
	}

	if ( setL0L1Time){
		this->writeL0L1Time(L0L1Time);
	}

	if (setL1FastorDelay){
		unsigned const nHsInRouter = 6;
		UInt32 lrxAddr;

		// we have to stop the state machine to set a register in the lrx
		bool isRouterStopped = this->getStopSM();

		if (isRouterStopped == false){this->setStopSM( true);}

		for (unsigned lrx =0; lrx < nHsInRouter ; lrx ++){
			
			unsigned chNumb = 6*routerNumber + lrx;
			
			lrxAddr = addresses.getLRxAddr( chNumb).GetLRXFastorL1DelayAddr();

			log->logToScr("Writing lrx fo delay addres %x, value" , lrxAddr, lrxFoDelays[lrx]);
			vmeDriver->VMEWriteRegister(lrxAddr, lrxFoDelays[lrx]);

		}
		
		if (isRouterStopped == false){this->setStopSM( false);}

	}

	
}

// resets only one halfstave for the moment its not working, problem in the router
void SpdRouter::resetHalfStave(unsigned halfstave){


	if (halfstave > 5){
		log->log("(SpdRouter::resetHalfStave) halfstave as to be between 0-5 found %d operation ignored", halfstave);
		return;
	}

	UInt32 RouterCtlOrig = this->readControlReg();
	UInt32 mask = 1 << halfstave;

	UInt32 ctlMasked = RouterCtlOrig;

	// inserts the mask in the control register
	insertIntoInt32( ctlMasked, mask,2,6);
	
	this->writeControlReg(ctlMasked);

	// then sends the command to reset the detector with only one channel enabled
	this->resetDetector();

	//rewrites the original mask in the router 
	this->writeControlReg(RouterCtlOrig);

}
// resets all 6 HS in one router at the same time (the reset detector in the labview)
void  SpdRouter::resetDetector(){
	UInt32 addr = routerAddresses.GetResetDetectorAddr();
	// writting anything to this register will start the router reset of the channels 
	vmeDriver->VMEWriteRegister( addr, 0x1);
}


// resets all jtag controllers 
void SpdRouter::resetJtagcontrollers(){

	log->log("INFO: resetting jtag controllers router %d ", this->routerNumber );

	const int numberHSInRouter = 6;
	for (unsigned hs= 0 ; hs < numberHSInRouter ; hs ++){


		UInt32 Addr = this->routerAddresses.GetJTResetStateMacAddr(hs);
		this->vmeDriver->VMEWriteRegister( Addr, 0x1 ); 

		Addr = this->routerAddresses.GetJTResetFIFOsAddr(hs);
		this->vmeDriver->VMEWriteRegister( Addr, 0x1 ); 

		Addr = this->routerAddresses.GetJTResetChAddr(hs);
		this->vmeDriver->VMEWriteRegister( Addr, 0x1 ); 
	}

}

//checks if all jtag controllers are in idle mode
void SpdRouter::checkJtagIdles(){	

	const int numberHSInRouter = 6;
	for (unsigned hs= 0 ; hs < numberHSInRouter ; hs ++){

		if( this->isJtagIdle(hs)){
			log->log("Caution jtag chain %d in router %d is not iddle", hs, this->routerNumber);
		}
	}

}


// checks if one jtag controller is idle
bool SpdRouter::isJtagIdle(int hs){
	UInt32 addr = this->routerAddresses.GetJTResetStateMacAddr(hs);
	UInt32 jtagStatusReg;

	CVErrorCodes status = this->vmeDriver->VMEReadRegister( addr, &jtagStatusReg);
	if (status){
		log->log("ERROR: Error while reading jtag status register address=0x%x",addr);
	}

		
	return (jtagStatusReg & 0x1);
	
}



// writes the halfstave mask in the router 
void SpdRouter::writeEnabledHs(unsigned mask){

	UInt32 ctrlRegister = this->readControlReg();

	// inserts the mask in the control register
	insertIntoInt32( ctrlRegister, mask,2,6);
	
	// and writes the mask
	this->writeControlReg(ctrlRegister);
	
}


// writes the enabled halfstaves 
void SpdRouter::writeEnabledHs( bool enabledHS[6]){
	UInt32 ctrlRegister = this->readControlReg();

	for (unsigned hs = 0 ; hs < NHalfstavesInRouter ; hs ++){
		unsigned mask = enabledHS[hs]?0:1;
		// inserts the mask in the control register
		spdGlobals::insertIntoInt32( ctrlRegister,mask,2+hs,1);
	}
	
	// and writes the mask
	this->writeControlReg(ctrlRegister);

}

// reads the enable half-staves
void SpdRouter::getEnabledHs(bool enablHS[6]){
	UInt32 ctrlRegister = this->readControlReg();

	for (unsigned hs = 0 ; hs < NHalfstavesInRouter ; hs ++){
		// reads the mask in the control register
		 enablHS[hs] =(extractFromInt32( ctrlRegister, 2+hs,1)==0)?true:false;
	}
}

// gets the start address, end address and size of one event in the router
void SpdRouter::getDpmEventSettings(UInt32 &startAddr, UInt32 &endAddr, UInt32 &lenght){
	// reads the start address of the dpm 
	UInt32 addr = this->routerAddresses.GetDPMfifoStartAddr();
	CVErrorCodes status =  this->vmeDriver->VMEReadRegister( addr, &startAddr);

	// reads the end address of the dpm
	addr = this->routerAddresses.GetDPMfifoEndAddr();
	status =  this->vmeDriver->VMEReadRegister( addr, &endAddr);
	
	if ( endAddr >= startAddr){
		//lenght= endAddr - startAddr + 1;
		// I have to ask michelle why this has one more data cell
		lenght= endAddr - startAddr + 2;
	}
	else{
		log->log("WARNING: end address smaller than start address, start = 0x%x end=0x%x", startAddr, endAddr);
		lenght= startAddr - endAddr + 1;
		//lenght=0x80000 + endAddr - startAddr + 1;
	}
}

SpdHitData  SpdRouter::readEventsFromMemory(){
	SpdHitData hits;
	
	unsigned eventNumber = 0;
	UInt32 addr,  lenght;
	UInt32 startAddr, endAddr, oldStartAddr(-1);

	// will take all events from the router memory
	while(true){
		
		this->getDpmEventSettings(startAddr, endAddr, lenght);

			// if the start address is not incrementing then we reached the end of the data
		if (startAddr == oldStartAddr) break;
		
		log->logToScr("reading contents of the dpm, lenght = %d, start=0x%x, end=0x%x", lenght, startAddr,  endAddr);
		
		UInt32 *data= new UInt32[lenght];
		//starts reading from the base address + where the data is now 
		addr = this->routerAddresses.GetDPMBaseAddr() +4*startAddr;
		
		this->vmeDriver->VMERegisterAcc( addr, data, lenght,'R');
		// parses the data filling up the class with the hits		
		hits.parseRawData( data, lenght);
		delete [] data;

		// assigns oldStartAddr so that it can stop on the next iteraction
		oldStartAddr = startAddr;
		eventNumber++;
	}

	return hits;
}

void SpdRouter::saveDPMDataToFile(const char * filename){
	ofstream file;
	UInt32 addr, lenght;
	UInt32 startAddr, endAddr, oldStartAddr(-1);
	unsigned eventNumber = 0;

	file.open( filename, ios::out);	
	file << "dual por memory dump for router "<<dec << this->routerNumber << endl;

		// will take all events from the router memory
	while(true){
		
		this->getDpmEventSettings(startAddr, endAddr, lenght);
					// if the start address is not incrementing then we reached the end of the data
		if (startAddr == oldStartAddr) break;

		log->logToScr("reading contents of the dpm, lenght = %d, start=0x%x, end=0x%x", lenght, startAddr,  endAddr);
		
		UInt32 *data= new UInt32[lenght];
		//starts reading from the base address + where the data is now 
		addr = this->routerAddresses.GetDPMBaseAddr() +4*startAddr;
		
		this->vmeDriver->VMERegisterAcc( addr, data, lenght,'R');
	
	
		file << "Event "<< dec << eventNumber<< " data size = "<<dec << lenght << endl;
		for (unsigned index = 0 ; index < lenght ; index ++){
			file<< hex << index << ": 0x"<< hex << data[index]<<endl;
		}

		delete [] data;
		// assigns  so that it can stop on the next iteraction
		oldStartAddr = startAddr;
	
		eventNumber++;
	}


}

//flushes the dpm memory cleaning all events
void SpdRouter::resetEventsInDpm(void){

	UInt32 addr = this->routerAddresses.GetFlushDPMAddr();

	log->logToScr("INFO: flushing dpm in router %d, address = %x value = 0x1", this->routerNumber, addr);

	this->vmeDriver->VMEWriteRegister( addr, 0x01);
}

// selects the single event buffer or multi event buffer
void SpdRouter::setSebMebMode( bool selector){

	UInt32 addr = routerAddresses.GetSebMebSet();
	
	if (selector == true){

		log->log("setting router %d in SEB mode", this->routerNumber);
		this->vmeDriver->VMEWriteRegister( addr, ROUTER_SEB_MODE);
	}
	else{
		log->log("setting router %d in MEB mode", this->routerNumber);
		this->vmeDriver->VMEWriteRegister( addr, ROUTER_MEB_MODE);
	}

	
	
}


// selects the single event buffer or multi event buffer
bool SpdRouter::getSebMebMode( void){

	UInt32 addr = routerAddresses.GetSebMebSet();
	UInt32 code = 0;

	this->vmeDriver->VMEReadRegister(  addr, &code);

	if (code == 0){
		return true;
	}
	else if (code==3){
		return false;
	}
	else{
		log->log("ERROR: error reading SEB MEB register, value read = 0x%x, expected 0 or 3", code);
	}
	return false;
}

// Function to write one arbitrary router register
void SpdRouter::writeRouterRegister(UInt32 routerAddress, unsigned addrSelection, UInt32 value){
		// creates the complete router address for the register
		/* bits 31:27 -> router board number
		   bits 26:24 -> 000 for router internal registers, 001 for single port memory, 010 for dual port memory 
		   bits 23:16 -> router register address
		*/	
	UInt32 routerCompleteAddress = routerAddress << 16;
	UInt32 routerBoard = this->routerNumber + 1;

	spdGlobals::insertIntoInt32(routerCompleteAddress, routerBoard, 27, 5);
	spdGlobals::insertIntoInt32(routerCompleteAddress, addrSelection, 24, 3);

	this->log->log("Writing router %d, address %x, value %x", routerNumber, routerCompleteAddress, value);

	vmeDriver->VMEWriteRegister(routerCompleteAddress, value);
}

// Function to read one arbitary router register
UInt32 SpdRouter::readRouterRegister(UInt32 routerAddress, unsigned addrSelection){
	UInt32 output;

		// creates the complete router address for the register
		/* bits 31:27 -> router board number
		   bits 26:24 -> 000 for router internal registers, 001 for single port memory, 010 for dual port memory 
		   bits 23:16 -> router register address
		*/
	UInt32 routerCompleteAddress = routerAddress << 16;
	UInt32 routerBoard = this->routerNumber + 1;

	spdGlobals::insertIntoInt32(routerCompleteAddress, routerBoard, 27, 5);
	spdGlobals::insertIntoInt32(routerCompleteAddress, addrSelection, 24, 3);

	this->log->log("Reading router %d, address %x", this->routerNumber, routerCompleteAddress);
	
	vmeDriver->VMEReadRegister(routerCompleteAddress, &output);
	return output;
}

// Function to write one arbitrary register inside one link receiver
void SpdRouter::writeLrxRegister(UInt32 chNumber, UInt32 lrxAddress, UInt32 value){
		// stops the router state machine to write one register in the link rx
	bool isRouterStopped = this->getStopSM();
	if (isRouterStopped == false){
		this->setStopSM(true);
	}

		// creates the complete lrx address
		/* bits 31:27 -> router board number
		   bits 26:25 -> = 10 to access the link receiver
		   bit 24 -> = 1 for access to link rx FPGA internal registers
		   bits 23:22 -> link receiver number
		   bit 21 -> channel number inside the link rx
		   bits 17:2 -> address link receiver
		*/
	UInt32 lrxCompleteAddress = lrxAddress << 2;
	
	UInt32 chInRouter = chNumber%6;
	UInt32 routerBoard = this->routerNumber + 1;

	spdGlobals::insertIntoInt32(lrxCompleteAddress, routerBoard, 27, 5); // bits 32:27
	spdGlobals::insertIntoInt32(lrxCompleteAddress, 1, 26, 1);			 // bit 26 -> lrx access
	spdGlobals::insertIntoInt32(lrxCompleteAddress, 1, 24, 1);			 // bit 24 -> lrx FPGA
	spdGlobals::insertIntoInt32(lrxCompleteAddress, chInRouter, 21, 3);	 // bits 23:21

	log->logToScr("Writing router %d, lrx number %d, register addres %x, value %x" ,
		chNumber/6, chInRouter/2, lrxCompleteAddress, value);
	vmeDriver->VMEWriteRegister(lrxCompleteAddress, value);

	this->setStopSM(isRouterStopped);
}

// Function to read one arbitrary register inside one link receiver
UInt32 SpdRouter::readLrxRegister(UInt32 chNumber, UInt32 lrxAddress){
	UInt32 output;

		// stops the router state machine to access one register in the link rx
	bool isRouterStopped = this->getStopSM();
	if (isRouterStopped == false){
		this->setStopSM(true);
	}

		// creates the complete lrx address
		/* bits 31:27 -> router board number
		   bits 26:25 -> = 10 to access the link receiver
		   bit 24 -> = 1 for access to link rx FPGA internal registers
		   bits 23:22 -> link receiver number
		   bit 21 -> channel number inside the link rx
		   bits 17:2 -> address link receiver
		*/
	UInt32 lrxCompleteAddress = lrxAddress << 2;

	UInt32 chInRouter = chNumber%6;
	UInt32 routerBoard = this->routerNumber + 1;

	spdGlobals::insertIntoInt32(lrxCompleteAddress, routerBoard, 27, 5); // bits 32:27
	spdGlobals::insertIntoInt32(lrxCompleteAddress, 1, 26, 1);			 // bit 26 -> lrx access
	spdGlobals::insertIntoInt32(lrxCompleteAddress, 1, 24, 1);			 // bit 24 -> lrx FPGA
	spdGlobals::insertIntoInt32(lrxCompleteAddress, chInRouter, 21, 3);	 // bits 23:21

	log->logToScr("Reading router %d, lrx number %d, register addres %x",
		chNumber/6, chInRouter/2, lrxCompleteAddress);

	vmeDriver->VMEReadRegister(lrxCompleteAddress, &output);
	this->setStopSM(isRouterStopped);

	return output;
}

// Function to set the fastor strobe length in one router
CVErrorCodes SpdRouter::setFoStrobeLength(unsigned int strobeLength){
	AddressGenerator & addresses = AddressGenerator::getInstance();
	CVErrorCodes status = cvSuccess;

	// stop the state machine to access the link rx
	bool isRouterStopped = this->getStopSM();
	if (isRouterStopped == false){
		this->setStopSM( true);
	}

	// loops through the channels, the command is sent per link rx
	for (unsigned int chNumber = routerNumber*6; chNumber < routerNumber*6+6; chNumber += 2){
		UInt32 lrxAddr = addresses.getLRxAddr(chNumber).GetFoStrobeLength();
		status = vmeDriver->VMEWriteRegister(lrxAddr, strobeLength);
	}

	this->setStopSM(isRouterStopped);
	return status;
}

// Function to set the timeout for ready events in one router
CVErrorCodes SpdRouter::setTimeoutReadyEvent(unsigned int timeout){
	CVErrorCodes status = cvSuccess;

	UInt32 routerAddr = routerAddresses.GetTimeoutReadyEvent();
	status = vmeDriver->VMEWriteRegister(routerAddr, timeout);

	return status;
}