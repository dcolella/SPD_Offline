#pragma once
#include "spdscan.h"

#include "..\VMEAcc\VMEAccess.h"
#include "stdafx.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"

#include "spdDacScan.h"

class SPDConfig;

//! Minimum Threshold scan for the detector 
/*!	 Minimum Threshold scan for the detector 
	pre_vth dac will be scanned to access what is the maximum threshold (minimum value of pre_vth)
	that can be used without noise in the detector
	because of the ocupancy limit in the link receiver this scan 
	is performed with only one pixel chip activated per halfstave
*/
class SpdMinThresScan : public SpdScan
{
	//! stores if the scan is being performed or not
	bool active;

	//! logger of the class
	spdLogger *log;
	
	//! will use a SpdDacScan object to perform the dac scans
	SpdDacScan *dacScan;

	//! parent SPDConfig to handle the hardware
	SPDConfig *parent;

	//! stores the prevth index
	unsigned prevthIndex;

	/*! scanPhase of the run 
		this made with only pixel chip activated at a time
		so we there are 10 scan phases
	*/
	unsigned scanPhase;

	///*!	stores the original prevth value of all the half-staves
	//	it will be set again at the end of the scan
	//*/
	//UInt32* prevthValue[60];

public:

		//! Method to start the dac scan
	unsigned start( UInt32 triggerNumber, 
				   UInt8 dacMin, 
				   UInt8 dacMax, 
				   UInt8 step, 
				   bool InternTrig, 
				   UInt32 waitTime);

		//! Method to restart the dac scan
	unsigned restart(void);

	bool isActive(){return this->active;}

		//! Method to stop the dac scan
	void stop(){this->active = false;};
	
		//! Method to lead the dac scan
	void scan();

	//! constructor
	SpdMinThresScan(SPDConfig * spdConf);
	~SpdMinThresScan(void);
};
