#pragma once

#include "stdafx.h"
#include "..\VMEAcc\VMEAccess.h"
#include ".\hsconfiguration.h"
#include ".\hsjtagstreams.h"
#include ".\HSConfData.h"

#include <dis.hxx>


const int numberOfDacs = 44;
const int numberOfApiDacs = 6;
const int numberOfApiAdcs = 17;

//! class to manage a halstave 
/*!class to manage a halstave 
	this class will try to implement a better aproach:
	it will not just have data members for the configuration but also 
	access the vme bus to configure the halfstave  */
class SpdHalfStave:public HSConfiguration
{

	//! member varaiable that keeps track if the detector is configured or not
	/*!this variable is set to true in the method configureDefault */
	bool isConfigured;

	spdLogger *logger;
		//!Member to store the channel number
	unsigned chNumber;
		//! data member connected to the DIM service for the BUS temperature
		/*! this one is updated automatically when the method read temperatre is called*/
	double tempBus;
		//! data member connected to the DIM service for the MCM temperature
		/*! this one is updated automatically when the method read temperatre is called*/
	double tempMcm;

		//! DIM service for the BUS temperature 
	DimService *serviceTempBus;
		//! DIM service for the MCM temperature
	DimService *serviceTempMcm;

		//! data member for the chip selected might be useful for compatibility with the old code
	UInt32 chipSelect;

		//! internal method to keep track of the activation status for this channel;
		/*!for this moment iths is not used, it implemented all over the code by an uggly class called ChToAddrDecoder*/
	UInt8 activationStatus;

		//! writes one difference value into the database
	void writeDiffValueToDB( int id, 
						int spd_ver, 
						unsigned chip, 
						std::string config_type, 
						std::string data_default,
						std::string data_actual
						);

public:
		//! constructor of the class 
	SpdHalfStave(unsigned chNumb,  VMEAccess * VME);
	//SpdHalfStave(unsigned chNumb, HSConfiguration hsData, VMEAccess * VME);

	~SpdHalfStave(void);

	//!VME access for the configuration, 
    VMEAccess * VME;

			//! sets te internal ChipSelected
	void setChipSelected( UInt32 value){chipSelect = value;}; 
			//! gets the internal chip selected
	UInt32 getChipSelected (){return chipSelect & 0xff;}

		//! method to read the bus temperature
	double getTempBus(){return this->tempBus;};
		//! method to read the MCM temperature
	double getTempMCM(){return this->tempMcm;};
		//! getter for the activation status of this channel
	UInt8 getChActStatus(){return activationStatus;};
		//! setter for the activation status of this channel
	void setChActStatus(UInt8 actStatus){activationStatus = actStatus;};

	unsigned getChannelNumber(){return chNumber;};
	
		//! Sets all pixel dacs (10*44) in the hardware
		/*!, if it is NULL will sets the default values 
			\param DACVect dac vector 440  bytes 
			\return status of the vme access*/
	CVErrorCodes LoadHSPixelAllDAC( UInt8 * DACVect = NULL);  //DACVect is 440 bytes
		//! Sets all pixel chips with the same dac value for a given dac number
		/*!	It will return the decoded read dac value  from the jtag command in the DACOutVect if this is different from NULL
			\param ChipSel hip selected: 10 bit defining which chips to change, 
			\param DACn dac number: identifying the dac, 
			\param DACValue: the DACValues to set,
			\return returns 0 if everything is ok*/
	CVErrorCodes LoadHSPixelDAC( UInt32 ChipSel, UInt8 DACn, UInt8 DACValue){return LoadHSPixelDAC(ChipSel, DACn, & DACValue ,true);};  //chip sel = 10 bit 1 to 1 with the bit postion  

	//! Sets all pixel chips with differente  dac values for a given dac number
		/*!	It will return the decoded read dac value  from the jtag command in the DACOutVect if this is different from NULL
			\param ChipSel hip selected: 10 bit defining which chips to change, 
			\param DACn dac number: identifying the dac, 
			\param DACValue: the DACValues to set
			\param all if this value is true sets all pixel chips with the first value of the input array
			\return returns 0 if everything is ok*/
	CVErrorCodes LoadHSPixelDAC( UInt32 ChipSel, UInt8 DACn, UInt8 *DACValue,  bool all = false);

		//! Reads the pixel dacs for one channel
		/*!returns in DACOutVect the pixel 10 dacs values for one dac number
			@param DACn - dac number to read (0..43)
			@param DACOutVect array were the 10 dac values will be written		*/
	CVErrorCodes ReadHSPixelDAC( UInt8 DACn, UInt32 * DACOutVect );  //chip sel = 10 bit 1 to 1 with the bit postion  


		//!Sets the pixel matrix in one pixel chip
		/*! \param ChipSel chip selected: 10 bit defining which chips to change 
			\param matrix the  matrix to be set 256*32 bits (jtag format)
			\param RptFirstMatrix: boolean telling if we want to repeat the first matrix to all chips
			\param Tp defines if it is a test pulse or mask matrix if Tp=true means that it is a test pulse */
	CVErrorCodes LoadHSPixelMatrix( UInt32 ChipSel, UInt32 * matrix, bool RptFirstMatrix, bool Tp); 

		//! will load the matrix stored in the defaul settings class
		/*! \param Tp if true means a test pulse matrix if false means a mask matrix*/
	CVErrorCodes LoadHSPixelMatrixDefault(  bool Tp=false ); 

        /*! sets the analog pilot setting to the hardware
			@param dacNumber analog pilot dac vector: if null it will set the values from the default class
				0: Dac Ref High
				1: Dac ref mid
				2: Gtl Ref A
				3: gtl ref D
				4: test high
				5: test low
			@param dacValue the value to set to this DAC*/
	CVErrorCodes setAnalogPilotDAC(unsigned dacNumber, UInt8 dacValue); 

    CVErrorCodes LoadHSApiDAC( UInt8* DACVect = NULL); 

		//! reads the analop pilot settings from the hardware
	CVErrorCodes ReadHSApiDAC( UInt8* DACVect);
		//! reads the API adcs
	CVErrorCodes ReadHSApiADC( UInt32* ADCVect);
    
		//! sets the digital pilot settings, if the input array is NUUL it will set the default values
	CVErrorCodes LoadHSDpiConfReg( UInt32* DPIVect = NULL);
		//! reads the digital pilot settings
	CVErrorCodes ReadHSDpiConfReg( UInt32 * DpiVector); // to add decoding function
		//! sets the digital pilot internal settings
    CVErrorCodes LoadHSDpiInternalReg( UInt32* RegOutVect, UInt32* DPIVect=NULL);
	
		// functions to write values to the hardware read back and compare
		//! writes the analog pilot values, reads back and comprares 
	CVErrorCodes LoadHSApiDACCompare( UInt8* DACVect = NULL); 
		//! writes the digital pilot settings, reads back and compares
	CVErrorCodes LoadHSDpiConfRegCompare( UInt32* DPIVect = NULL);

		//! function to refresh the temperatures of one hs
	void refreshTemperatures();

		/*! function to write the differences to the database temporary table
			will loop through all internal containers comparing the defaul and actual values 
			and writing them in the database
			@param id : id of these differences
			@param db_ver : global version database the default data belongs to	*/
	void writeDiffToDB( int id, int db_ver );

		/*! function to get the number of differences between default and actual containers
			will loop through all internal containers comparing the defaul and actual values 
			and counting the number of differente settings in mcm values, dac values and noisy pixel values
			@param mcmDiff: argument by reference it will be incremented inside the funcion to contain the number of mcm differences
			@param dacDiff: argument by reference it will be incremented inside the funcion to contain the number of pixel dac differences
			@param noisyDiff: argument by reference it will be incremented inside the funcion to contain the number of noisy pixels differences	*/
	void getDiffCount(unsigned &mcmDiff, unsigned &dacDiff, unsigned &noisyDiff);

	/*! Performs a jtag reset in the halfstave*/
	CVErrorCodes jtagReset();

	/*! Performs a series of validation loops for the jtag chain*/
	CVErrorCodes jtagValidation(UInt32 cycles, UInt32* errors);

	/*@Configures the one halfstave with the default values 
	@param jtagReset if true performs a jtag reset 
	@param digitalPilot if true performs a digital pilot configuration
	@param analogPilot if true performs a analog pilot configuration
	@param pixdacs if true performs a pixel dacs configuration
	@return 
	bit 1 enabbled -> problem in the jtag reset, 
	bit 2 problem  in the digital pilot configuration, 
	bit 3 problem in the analog pilot  configuration,
	bit 4 problem in the pixel dacs  configuration,
	bit 5 if the channel was not ennabled (skipped the configuration)*/
	int configureDefault( bool jtagReset=true, bool digitalPilot=true, bool analogPilot=true, bool pixdacs=true);
	
	/*! Resets the JTAG controller of one channel*/
	void resetJtagController();
};
