#pragma once

#include <dic.hxx>
#include "..\VMEAcc\VMEAccess.h"
#include "stdafx.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"
#include "spdScan.h"

class SPDConfig;

const UInt8 typeOfScan = 1;
const int tpHighNum = 4;
const int tpLowNum = 5;

//!Class to handle the mean threshold scan 
/*! This class performs the mean threshold class 
	a test pulse will be set and then the test pulse amplitude will be increased in steps.
	test-pulse high will be fixed and test-pulse low will be looped 
	increasing the charge injected in the detector.
	so its basicaly a Uniformity matix with several steps of test pulse amplitude per row
*/
class spdMeanTHScan: public SpdScan
{
	//! the parent class to handle heardware access 
	SPDConfig *parent;
	//! logger instance for this class
	spdLogger *log;
	//! stores if the scan is active or not
	bool active;
	//! timer to measure the scan result
	timer timerScan;

	//! minimum value of test pulse low for the scan
	int tpLowMinimum;
	//! maximum value of test pulse low for the scan
	int tpLowMaximum;
	//! the step of the scan
	int tpLowStep;
	//! the current value for a certain test pulse low step
	int tpLowCurrent;
	//! stores the test pulse low index to be written in the router header 
	int tpLowIndex;
	//! stores the original test pulse low value of all half-staves read from the hardware
	UInt8 tpFromHalfstave[60];

	//! first row for the test pulse 
	unsigned int rowStart;
	//! last row for the test pulse
	unsigned int rowEnd;
	//! row step
	unsigned int rowStep;
	//! current number of row
	unsigned int rowCurrent;
	//! number of rows activated at the same time for test pulse
	unsigned int activeRows;

	//! number of triggers sent for each test pulse low step
	unsigned int triggerNum;

		/*! Method to set the scan header in all active routers
			@return the error state of the operation (0=ok, not 0=problems with one of the channels)
		*/
	UInt32 setMeanTHScanHeader (void);

		/*! Method to get the test pulse amplitude in mV
			@param routerNum- number of the router
			@param tpAmplitude- array of test pulse amplitudes for the 6 half-staves in a router
			@return the error state of the operation (0=ok, not 0=problems with one of the channels)
		*/
	UInt32 getTPAmplitude(unsigned int routerNum, float *tpAmplitude);

		/*!	Method to set several rows in a matrix*/
	UInt32 SetMatrixToAllEnabled(UInt8 rowStart,UInt8 nRows);

		/*! Method to set the value of test pulse low in all enabled half staves
			@param testLowValue- value of the test pulse low DAC
			@return error condition
		*/
	UInt32 SetTestLowToAllEnabled(int testLowValue);

	//!method to conver the analog pilot ADC value 
	/*! \param adcCount ADC digital value*/
	float convertAdcValue(unsigned adcCount);

	//! Performs a testpulse low step (sends triggers and writes headers also)
	/*! \param  currentValue current value of test pulse
		\param sleepTime wait time between steps*/
	UInt32 testLowStep(int currentValue, unsigned sleepTime);

public:

	//!constructor of the class
	spdMeanTHScan(SPDConfig *spdConfig);

	~spdMeanTHScan(void);

		//! Method to start the mean threshold scan
	unsigned start(	unsigned int tpLowMinimum,
				unsigned int tpLowMaximum,
				unsigned int tpLowStep,
				unsigned int rowStart,
				unsigned int rowEnd,
				unsigned int rowStep,
				unsigned int triggerNumber);

	//! returns if this scan is being performed or not
	bool isActive(){return active;};
		//! Method to restart the mean threshold scan
	unsigned restart();

		//! Method to stop the mean threshold scan
	void stop(){this->active=false;};
	
		//! Method for the mean threshold scan
	void scan();
};