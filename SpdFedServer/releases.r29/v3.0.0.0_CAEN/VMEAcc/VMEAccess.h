#pragma once

#include "JTAGAccess.h"
#include "../addressgenerator/addressgenerator.h"
#include "../HSConfig/hsconfiguration.h"

//! class for the VME acess 
class VMEAccess: public JTAGAccess{
	//HSConfiguration * HS;

	UInt8 DataType;      /*Data Type
						  0 = normal data
						  1 = DAC Scan 
						  2 = TP Scan
						  3 = Matrix
						  */
	VMEAccess();

public:
	
	static VMEAccess & getInstance();

	~VMEAccess(void);

	   
	UInt32 * GetEvent(int ChNumb);    //ok

	UInt8 GetDataType(void){return DataType;};
	void SetDataType(UInt8 val){DataType = val;};


};
