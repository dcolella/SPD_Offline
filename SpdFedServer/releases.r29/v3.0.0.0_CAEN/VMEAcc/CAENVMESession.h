#pragma once
#include "StdAfx.h"
#include "../spdDbConfLib/spdLogger.h"

class CAENVMESession
{
public:
	CAENVMESession(void);
	~CAENVMESession(void);

	CVErrorCodes InitSession(void);
	CVErrorCodes EndSession(void);
	int32_t GetHandle(void) {return VMEacc_handle;}
	bool isInDebugMode(){return debugMode;}
	void setDebugMode(bool value){debugMode=value;}

private:
	bool debugMode;
	int32_t VMEacc_handle;
	spdLogger *log;
};

