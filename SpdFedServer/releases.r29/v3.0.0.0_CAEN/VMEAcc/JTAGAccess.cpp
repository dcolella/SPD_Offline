#pragma once
#include "StdAfx.h"
#include ".\jtagaccess.h"

JTAGAccess::JTAGAccess(){
	_Zero=0;
	_ScanType = 0 ;    
	_BitNumber = 0;   
	_ChNumb = 0;       
	_CLKSpeed = 0;     
	_JTAGHeader = 0xffffffff;
    _VectorOut = new UInt32;
    BuildContrWords(0);

	log = &spdLogger::getInstance(); 
    VME_handle=GetHandle();

	AddressesVector = new uint32_t;
	DataVectorIn = new uint32_t;
	DataVectorOut = new uint32_t;
	AMVector = new CVAddressModifier;
	DWVector = new CVDataWidth;
	ReturnVector = new CVErrorCodes;
}

JTAGAccess::~JTAGAccess(void){
	if (_VectorOut != NULL) delete _VectorOut;
}


CVErrorCodes JTAGAccess::JTAGWriter(UInt8  ChN, UInt32 * VectIn, UInt32  ScType, UInt32 BtNumb, bool doRead){
	this->SetJTChannel(ChN);
	_VectorIn = VectIn+1;
	_ScanType = ScType;
	_BitNumber = BtNumb;
	return this->JTAGScanRegV2(doRead);
	//return this->JTAGScanReg();
}

CVErrorCodes JTAGAccess::JTAGWriter(UInt8 ChN, UInt32 * IRVect, UInt32 * DRVect, bool doRead){
	this->SetJTChannel(ChN);
    
	_BitNumber = IRVect[0];
	_ScanType = IR;
	_VectorIn = IRVect + 1;

	CVErrorCodes status = JTAGScanRegV2(false);
	//CVErrorCodes status = JTAGScanReg();
	if(status != cvSuccess) return status;
	
    _BitNumber = DRVect[0];
	_ScanType = DR;
	_VectorIn = DRVect + 1;
	status = JTAGScanRegV2(doRead);
	//status = JTAGScanReg();
	return status;
}


CVErrorCodes JTAGAccess::JTAGWriter(UInt8 ChN, UInt32 * IRVect0, UInt32 * DRVect0, UInt32 * IRVect1, UInt32 * DRVect1, bool doRead){
	CVErrorCodes status = this->JTAGWriter(ChN, IRVect0, DRVect0, false);
	if(status != cvSuccess) return status;
	status = this->JTAGWriter(ChN, IRVect1, DRVect1, doRead);
	return status; 
}


void JTAGAccess::SetJTClkSpeed(UInt32 CLKSp){
	if(CLKSp >=0 && CLKSp < 5){
		_CLKSpeed = CLKSp;
		log->log("INFO: JTAG clock speed set at %d MHz",(UInt32)(2^_CLKSpeed) );
	}else{
		log->log("ERROR: The JTAG clock speed specified is out of range 0..4");
	}
}

	
//Scan the JTAG register: IstrType tell if scan DR (0) or IR (1) or RESET (2)   
CVErrorCodes JTAGAccess::JTAGScanReg(void){
	//uint32_t Zero=0;
	//UInt32 Zero=0;
	if(isInDebugMode()){
		//cout << "|";
		return cvSuccess;
	}
	
    BuildContrWords(2);
	this->SetJTChannel(_ChNumb);

	if(!this->IsControllerIdle(FALSE)){
		log->log("WARNING: JTAG controller in idle, reseting channel %d", this->_ChNumb);
		this->ResetJTAGContr();
	}
	this->BuildContrWords(1);
    if(_ScanType == 2){      //RESET istruction
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_ContrWord1,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_ExStartAddr,&_Zero,cvA32_S_DATA, cvD32);
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_ContrWord1,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_ExStartAddr,&Zero,1,'W');
	}else{
		
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_ContrWord1,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_ContrWord2,cvA32_S_DATA, cvD32);
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_ContrWord1,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_ContrWord2,1,'W');
		//log->log("nWords:%d",_WordsNumber);
		for(UInt32 i=0; i < _WordsNumber; i++){
			//log->log("VectorIn word %d: %x", i, this->_VectorIn[i]);
			CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_VectorIn[i],cvA32_S_DATA, cvD32);
		    //VMERegisterAcc(_RdWrDataAddr,&_VectorIn[i],1,'W');
		}
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_RdWrDataAddr,&_JTAGHeader,cvA32_S_DATA, cvD32);
		CAENVME_WriteCycle(VME_handle,_ExStartAddr,&_Zero,cvA32_S_DATA, cvD32);
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_RdWrDataAddr,&_JTAGHeader,1,'W');
		//VMERegisterAcc(_ExStartAddr,&Zero,1,'W');
        UInt32 startTime;
		startTime =	GetTickCount();  

        while(!this->IsControllerIdle(FALSE)){  				
			if((GetTickCount() - startTime) > 500){ //SS 14.04.2014 Try to increase the timeout (was 500)
				log->log("ERROR: JTAG Timeout channel %d , reseting it's controler",this->_ChNumb );
				this->ResetJTAGContr();
				return spdTimeoutError; 
			}
		}
		//int timeout = GetTickCount() - startTime;
		//if (timeout) printf("Timeout = %d\n",timeout); // to remove SS
		if(_VectorOut != NULL) delete[] _VectorOut;
		_VectorOut = new UInt32 [_WordsNumber + 1];
		CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + _WordsNumber,cvA32_S_DATA, cvD32);
		CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + _WordsNumber,cvA32_S_DATA, cvD32);
		CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + _WordsNumber,cvA32_S_DATA, cvD32);
		//VMERegisterAcc(_RdWrDataAddr,_VectorOut + _WordsNumber,1,'R');
		//VMERegisterAcc(_RdWrDataAddr,_VectorOut + _WordsNumber,1,'R');
		//VMERegisterAcc(_RdWrDataAddr,_VectorOut + _WordsNumber,1,'R');
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
			
        for(UInt32 i=0; i < _WordsNumber; i++){
			CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + i,cvA32_S_DATA, cvD32);
		    //VMERegisterAcc(_RdWrDataAddr,_VectorOut + i,1,'R');
			//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + i);
			//log->log("VectorOut word %d: %x", i, this->_VectorOut[i]);
		}
            
		_VectorOut[_WordsNumber - 1] >>=  (32 - _LastWordBits);
		//log->log("VectorOutLast: %x", _VectorOut[_WordsNumber - 1]);

		CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + _WordsNumber,cvA32_S_DATA, cvD32);
		CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + _WordsNumber,cvA32_S_DATA, cvD32);
		CAENVME_ReadCycle(VME_handle,_RdWrDataAddr, _VectorOut + _WordsNumber,cvA32_S_DATA, cvD32);
		//VMERegisterAcc(_RdWrDataAddr,_VectorOut + _WordsNumber,1,'R');
		//VMERegisterAcc(_RdWrDataAddr,_VectorOut + _WordsNumber,1,'R');
		//VMERegisterAcc(_RdWrDataAddr,_VectorOut + _WordsNumber,1,'R');
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
			
		if(!this->IsControllerIdle(TRUE)) this->ResetJTAGContr();
	}
	return cvSuccess;
}


bool JTAGAccess::IsControllerIdle(bool reset){
	if(isInDebugMode()){
		//cout << "|";
		return false;
	}

	UInt32 StatusContr;


	CVErrorCodes status = CAENVME_ReadCycle(VME_handle,_StatusRegAddr, &StatusContr,cvA32_S_DATA, cvD32);
	//CVErrorCodes status = VMERegisterAcc(_StatusRegAddr,&StatusContr,1,'R');
		//viIn32(VisSesObjGen(0), VI_A32_SPACE, _StatusRegAddr, );
	//cout<<"Output of read cycle:"<<StatusContr<<endl;
	if(status != cvSuccess ){
		log->log("ERROR: error in VME access during jtag scan on channel %d", this->_ChNumb);
		return false;
	}

	// this magic number 0xCC018001 appears after a reset. 
	// this is a weird use of this function, basicaly its checking also if it is idle 
	// or if its after a reset so I will rewrite the code
	// if(( (StatusContr == 0xCC018001) && reset) || ((StatusContr & 0x1) && !reset)) return TRUE;
	if (reset){
		// checks for reset
		if (StatusContr == 0xCC018001) return true;
	}
	else{
		//checks for normal idle
		if (StatusContr & 0x1) return true;
	}

	return false;
}


CVErrorCodes JTAGAccess::ResetJTAGContr(){
	//uint32_t Zero=0;
	//UInt32 Zero=0;
	if(isInDebugMode()){
		//cout << "|";
		return cvSuccess;
	}
    CVErrorCodes status = cvSuccess;
	
	status = CAENVME_WriteCycle(VME_handle,_ResetControllerSM,&_Zero,cvA32_S_DATA, cvD32);
	//status = VMERegisterAcc(_ResetControllerSM,&Zero,1,'W');
    if (status != cvSuccess)log->log("ERROR: error in VME access during jtag state machine reset on channel %d", this->_ChNumb);
	return status;
}		

void JTAGAccess::SetJTChannel(UInt8 ChN){
	AddressGenerator & addresses = AddressGenerator::getInstance();
	 _ChNumb = ChN;
	 _RdWrDataAddr = addresses.getRoutAddr(ChN).GetJTRdWrDataAddr(ChN);
	 _ExStartAddr = addresses.getRoutAddr(ChN).GetJTExStartAddr(ChN);
	 _StatusRegAddr = addresses.getRoutAddr(ChN).GetJTStatusRegAddr(ChN);
	 _ResetControllerSM = addresses.getRoutAddr(ChN).GetJTResetStateMacAddr(ChN);
}


void JTAGAccess::BuildContrWords(UInt8 Mode){
	   
	if(Mode == 0 || Mode == 1){
	   _ContrWord1 = (_CLKSpeed << 12) + (_ChNumb << 7) + _ScanType; 
	}
	if(Mode == 0 || Mode == 2){
       _WordsNumber = (_BitNumber / 32);
       _LastWordBits = _BitNumber % 32;
	   if (_LastWordBits == 0){ 
		   _LastWordBits = 32;
	   } else {
		   _WordsNumber++;
	   }
       _ContrWord2 = _WordsNumber + (_LastWordBits - 1) * 0x4000000;
	}
}

//Scan the JTAG register: IstrType tell if scan DR (0) or IR (1) or RESET (2)   
CVErrorCodes JTAGAccess::JTAGScanRegV2(bool doRead){
	//uint32_t Zero=0;
	//UInt32 Zero=0;
	if(isInDebugMode()){
		//cout << "|";
		return cvSuccess;
	}
	
    BuildContrWords(2);
	this->SetJTChannel(_ChNumb);

	if(!this->IsControllerIdle(FALSE)){
		log->log("WARNING: JTAG controller in idle, reseting channel %d", this->_ChNumb);
		this->ResetJTAGContr();
	}
	this->BuildContrWords(1);

    if(_ScanType == 2){      //RESET istruction

		nCycles=6;

		if(AddressesVector != NULL) delete[] AddressesVector;
		AddressesVector = new uint32_t[nCycles];

		if(DataVectorIn != NULL) delete[] DataVectorIn;
	    DataVectorIn = new uint32_t[nCycles];

		if(DataVectorOut != NULL) delete[] DataVectorOut;
	    DataVectorOut = new uint32_t[nCycles];

		if(AMVector != NULL) delete[] AMVector;
	    AMVector = new CVAddressModifier[nCycles];

		if(DWVector != NULL) delete[] DWVector;
	    DWVector = new CVDataWidth[nCycles];

		if(ReturnVector != NULL) delete[] ReturnVector;
	    ReturnVector = new CVErrorCodes[nCycles];

		for(int i=0;i<nCycles-1;i++){
		    AddressesVector[i] = _RdWrDataAddr;
		    AMVector[i] = cvA32_S_DATA;
		    DWVector[i] = cvD32;
	    }
		
		AddressesVector[nCycles-1] = _ExStartAddr;
		AMVector[nCycles-1] = cvA32_S_DATA;
		DWVector[nCycles-1] = cvD32;

		DataVectorIn[0] = _JTAGHeader;
	    DataVectorIn[1] = _ContrWord1;
	    DataVectorIn[2]=_JTAGHeader;
	    DataVectorIn[3]=_JTAGHeader;
	    DataVectorIn[4]=_JTAGHeader;
	    DataVectorIn[5]=_Zero;

		CAENVME_MultiWrite(VME_handle,AddressesVector,DataVectorIn,nCycles,AMVector,DWVector,ReturnVector);

	}
	else{

		nCycles=_WordsNumber+7;

		if(AddressesVector != NULL) delete[] AddressesVector;
		AddressesVector = new uint32_t[nCycles];

		if(DataVectorIn != NULL) delete[] DataVectorIn;
	    DataVectorIn = new uint32_t[nCycles];

		if(DataVectorOut != NULL) delete[] DataVectorOut;
	    DataVectorOut = new uint32_t[nCycles];

		if(AMVector != NULL) delete[] AMVector;
	    AMVector = new CVAddressModifier[nCycles];

		if(DWVector != NULL) delete[] DWVector;
	    DWVector = new CVDataWidth[nCycles];

		if(ReturnVector != NULL) delete[] ReturnVector;
	    ReturnVector = new CVErrorCodes[nCycles];

		for(int i=0;i<nCycles-1;i++){
		    AddressesVector[i] = _RdWrDataAddr;
		    AMVector[i] = cvA32_S_DATA;
		    DWVector[i] = cvD32;
	    }
		
		AddressesVector[nCycles-1] = _ExStartAddr;
		AMVector[nCycles-1] = cvA32_S_DATA;
		DWVector[nCycles-1] = cvD32;

	    DataVectorIn[0] = _JTAGHeader;
	    DataVectorIn[1] = _ContrWord1;
	    DataVectorIn[2] = _ContrWord2;
	    for(UInt32 i=0; i < _WordsNumber; i++){
			//log->log("VectorIn word %d: %x", i, this->_VectorIn[i]);
		    DataVectorIn[3+i]=_VectorIn[i];
	    }
	    DataVectorIn[3+_WordsNumber]=_JTAGHeader;
	    DataVectorIn[3+_WordsNumber+1]=_JTAGHeader;
	    DataVectorIn[3+_WordsNumber+2]=_JTAGHeader;
	    DataVectorIn[3+_WordsNumber+3]=_Zero;

		CAENVME_MultiWrite(VME_handle,AddressesVector,DataVectorIn,nCycles,AMVector,DWVector,ReturnVector);
		
        UInt32 startTime;
		startTime =	GetTickCount();  

        while(!this->IsControllerIdle(FALSE)){  				
			if((GetTickCount() - startTime) > 500){
				log->log("ERROR: JTAG Timeout channel %d , reseting it's controler",this->_ChNumb );
				this->ResetJTAGContr();
				return spdTimeoutError; 
			}
		}

		if(doRead){
		    if(_VectorOut != NULL) delete[] _VectorOut;
		    _VectorOut = new UInt32 [_WordsNumber + 1];

		    CAENVME_MultiRead(VME_handle,AddressesVector,DataVectorOut,nCycles-1,AMVector,DWVector,ReturnVector);

            for(UInt32 i=0; i < _WordsNumber; i++){
				//log->log("VectorOut word %d: %x", i, this->_VectorOut[i]);
			    _VectorOut[i]=DataVectorOut[i+3];
		    }          
		    _VectorOut[_WordsNumber - 1] >>=  (32 - _LastWordBits);
		    _VectorOut[_WordsNumber]=DataVectorOut[nCycles-2];
		}

		if(!this->IsControllerIdle(TRUE)) this->ResetJTAGContr();
	}
	return cvSuccess;
}