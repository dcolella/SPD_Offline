#pragma once
#include "StdAfx.h"
#include ".\VisaSessContr.h"

VisaSessControl::VisaSessControl(void){
	MaxOpenSess = 240;
	NVisSessOpened = 0;
	
	strcpy(DevLogicAddr, "VXI0::300::INSTR");

	VisaSess = new ViSession [MaxOpenSess];
	Mapped = new bool [MaxOpenSess];
	
	for(int i =0; i < MaxOpenSess; i++){
		VisaSess[i] = 0;
		Mapped[i] =0;
	}
	debugMode=false;
	ResourceMen=0;
}   

VisaSessControl::~VisaSessControl(void){

	if (ResourceMen != NULL)viClose(ResourceMen);

	for (int session = 0 ; session < MaxOpenSess ; session ++){
		if (VisaSess[session]!= NULL) viClose(VisaSess[session]);
	}

	delete[] VisaSess;
	delete[] Mapped;
	
	
}


UInt8 VisaSessControl::GetUnmappedSessNumb(void){
	UInt8 i = 0;
	while(Mapped[i]) i++;

 return i;
}


UInt8 VisaSessControl::MapAddr(UInt32 Offset, UInt32 CellsNumber, ViAddr * RetAddr){
	UInt8 VirtSesNumb = GetUnmappedSessNumb();
	ViStatus status = viMapAddress(VisSesObjGen(VirtSesNumb), VI_A32_SPACE, Offset, (CellsNumber * 4), VI_FALSE, VI_NULL, RetAddr);
	////CheckStatus();
	Mapped[VirtSesNumb] = 1; 
	
	return VirtSesNumb; 
}




void VisaSessControl::UnMapAddr(UInt8 VirtSesNumb){
	//CheckStatus(viUnmapAddress(VisSesObjGen(VirtSesNumb)));

	ViStatus status = viUnmapAddress(VisSesObjGen(VirtSesNumb));
	Mapped[VirtSesNumb] =0;
}


ViSession VisaSessControl::VisSesObjGen(int SesNumb){
	ViStatus status;

	if (SesNumb == -1) SesNumb = NVisSessOpened;
	if (SesNumb >= MaxOpenSess) {
		cerr << "Number of session exceding the maximum allowed"<<endl;
		SesNumb = -1;
		system("exit");
	}

	if(SesNumb >= NVisSessOpened){
       	status=viOpen(ResourceMen, DevLogicAddr, VI_NULL, VI_NULL, (VisaSess + SesNumb));
		//CheckStatus();
		NVisSessOpened++;
	}

	return VisaSess[SesNumb];
}


ViStatus VisaSessControl::RunResorceMenager(void){
	
    ViStatus status = viOpenDefaultRM(&ResourceMen);
	return status;
}	





ViStatus VisaSessControl::CloseSess(int SesNumb){
    ViStatus status;    

	if(SesNumb < 0){
		for(int i =0 ; i < NVisSessOpened; i++){ 
			status = viClose(VisaSess[i]);
			//CheckStatus();
			Mapped[i] = 0;
			NVisSessOpened--;
		}
	}
	else{ 
	    status = viClose(VisaSess[SesNumb]);   
		//CheckStatus();
		Mapped[SesNumb] = 0;	
		NVisSessOpened--;
	}


	return status;
}

ViStatus VisaSessControl::CloseUnmappedSess(void){
	ViStatus status;

	for(int i =0 ; i < NVisSessOpened; i++){ 
		if (!Mapped[i]){
				status = viClose(VisaSess[i]);
				//CheckStatus();
				Mapped[i] = 0;
				NVisSessOpened--;
		}
  			
	}
	
	return status;
}