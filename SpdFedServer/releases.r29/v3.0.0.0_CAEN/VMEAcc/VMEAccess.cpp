#pragma once
#include "StdAfx.h"
#include ".\vmeaccess.h"

VMEAccess::VMEAccess(){
	
	DataType = 0;
 }

VMEAccess::~VMEAccess(void){
}


VMEAccess & VMEAccess::getInstance(){
	static  VMEAccess singleton;
	return singleton;
}

UInt32 * VMEAccess::GetEvent(int ChNumb){
	AddressGenerator & addresses = AddressGenerator::getInstance();
	UInt32 Data;
	UInt32 * DtPt, * DataStream = NULL;
	char mode;
	UInt32 Addr, DataHeaderLenght;

	if(DataType == 2){
		DataHeaderLenght = 12;   
	}else{
		DataHeaderLenght = 5;
	}

	mode = 'R';
	Addr = addresses.getRoutAddr(ChNumb).GetStatusReg2Addr();
	DtPt = &Data;

	VMERegisterAcc( Addr, DtPt, 1, mode);

	if((Data & 1) == 0){

		UInt32 StartAddr, EndAddr;	
		
		Addr = addresses.getRoutAddr(ChNumb).GetDPMfifoStartAddr();
		VMERegisterAcc(Addr, DtPt, 1, mode);
		StartAddr = Data  & 0x7FFFF;

	//	printf("Start Addr %d\n", StartAddr); //to remove
	//	Sleep(1);                             //to remove, i don't know yet

		Addr = addresses.getRoutAddr(ChNumb).GetDPMfifoEndAddr();
		VMERegisterAcc(Addr, DtPt, 1, mode);
		EndAddr = Data  & 0x7FFFF;

		//   printf("End Addr %d\n", EndAddr);      //to remove
				    
		Addr = addresses.getRoutAddr(ChNumb).GetDPMBaseAddr();
				 	
	    
		UInt32 lenght;
		if (EndAddr >= StartAddr){
			lenght = EndAddr - StartAddr + 1;
			DataStream = new UInt32 [lenght +  DataHeaderLenght];
			StartAddr <<= 2;
			Addr += StartAddr;
			VMERegisterAcc(Addr, DataStream + DataHeaderLenght, lenght, mode);
		}else{
			lenght = (UInt32)0x80000 - StartAddr + EndAddr + 1;
			DataStream = new UInt32 [lenght +   DataHeaderLenght];  
			StartAddr <<= 2;
			Addr += StartAddr;
			VMERegisterAcc(Addr, DataStream +  DataHeaderLenght, lenght, mode);
		}
	    
		DataStream[0] = lenght + DataHeaderLenght;   
		DataStream[1] = ChNumb;
		DataStream[2] = DataType; 			
		DataStream[3] = 0; 			
		DataStream[4] = 0; 			

		for(UInt8 i = 0; i< 6; i++){

			DataStream[i/3 + 3] += (UInt32)0x3ff << (i%3 * 10);
		}

		if(DataType == 2) for(int i=5; i<= 11; i++) DataStream[i] = 0;
		
	}
	return DataStream; 

	return NULL;
}

//ViStatus VMEAccess::SendTrigger(UInt8 ChN, UInt32 TriggerN, bool Delay, bool l2y, bool l1, bool tp){ //Trigger Number = 0..26 
//	AddressGenerator & addresses = AddressGenerator::getInstance();
//
//	char mode = 'W';
//	UInt32 Addr = addresses.getRoutAddr(ChN).GetSendTriggSeqAddr();
//    UInt32 Data = ((((UInt32)(!tp) << 4) + ((UInt32)(!l1) << 3) +  ((UInt32)(!l2y) << 1) + (UInt32)(!Delay))<< 27) + TriggerN; 
//	return VMERegisterAcc( Addr, &Data , 1, mode); 
//}
