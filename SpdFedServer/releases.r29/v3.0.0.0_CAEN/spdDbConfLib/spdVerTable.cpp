#include "stdafx.h"
#include "SpdVerTable.h"
#include "spdDbConnection.h"
#include "spdGlobals.h"
#include "spdLogger.h"

template <class T>
std::string toString( const T &value )
{
   std::ostringstream oss;

   oss << value;

   return oss.str();
}


//Template function to compare two vectors 
template <class T>
bool compareVectors(const std::vector<T> &v1,const std::vector<T> &v2){

	size_t nItems = std::min( v1.size(), v2.size());

	for(unsigned int index = 0 ; index < nItems ; index ++){
		if (v1[index]!= v2[index])	return false;
	}
	return true;
}

// this function is used only to create the oracle querys 
// just adds the "'" if it is a string 
std::string formatValue(const std::string &value, spdDbTypes type){
	std::string out;
	switch( type){
		case spdNumeric:
			out = value;
			break;
		case spdSTRING:
			out = "'" + value +"'";
			break;
	}

	return out;
};

//**********************************************************************
// constructors 
SpdVerTable::SpdVerTable(void)
{
	version=0;

}

SpdVerTable::SpdVerTable(const std::string &table,
						const std::string &newVersionField,
						const std::vector<std::string> &newFields){

	version =0;
	this->setTableSettings( table, newVersionField, newFields);
}
SpdVerTable::~SpdVerTable(void)
{

}
/*
	intializer with the settings 
*/
SpdVerTable::SpdVerTable(const std::string &table,
						 const std::string &newVersionField,
						 const std::vector<std::string> &newFields,
						 const std::vector<spdDbTypes> &newTypes){

	version =0;
	this->setTableSettings( table, newVersionField, newFields, newTypes);

}

void SpdVerTable::setTableSettings(const std::string &table,
								   const std::string &newVersionField,
								   const std::vector<std::string> &newFields,
								   const std::vector<spdDbTypes> &newTypes){
	tableName = table;
	versionField=newVersionField;
	fields = newFields;
	types = newTypes;
}

void SpdVerTable::setTableSettings( const std::string &table,
								   const std::string &newVersionField,
								   const std::vector<std::string> &newFields){

	tableName = table;
	versionField=newVersionField;
	fields = newFields;
						// intializes all new fields as numeric if not specified
	std::vector<spdDbTypes> newTypes(newFields.size(), spdNumeric);
	types=newTypes;

}

//creates insert query string from string values
std::string SpdVerTable::createInsertQuery(long int newVersion,const std::vector<std::string> &values){
		

	std::string command = "INSERT INTO " + tableName+"( ";
	command +=   versionField ;
	
	for (unsigned int i = 0 ; i < fields.size(); i ++){
		//command += ",\n" + fields[i] ;
		command += "," + fields[i] ;
	}
	
	command += ") VALUES (";
	command += toString( newVersion);


	for (unsigned int i = 0 ; i < values.size(); i ++){
		command += "," + formatValue(values[i], types[i]);
	}
	
	command +=")";


	return command;

}

/*
	Creates the insert query string with the input values
	this template function will create the numeric values
*/
template<class num>
std::string SpdVerTable::createInsertQuery(long int newVersion,const std::vector<num> &values){
	
	std::string command = "INSERT INTO " + tableName+"( ";
	command +=   versionField  ;
	

	for (unsigned int i = 0 ; i < fields.size(); i ++){
		//command += ",\n" + fields[i]  ;
		command += ", " + fields[i]  ;
	}
	
	command += ") VALUES (";
	command += toString( newVersion);


	for (unsigned int i = 0 ; i < values.size(); i ++){
		command += "," + toString( values[i]);
	}
	
	command +=")";


	return command;
}

// creates the select query string 
std::string SpdVerTable::createSelectQuery(){

	std::string command = "SELECT ";

	size_t nFields = fields.size() -1;

			// adds all the fields
	for (unsigned int i = 0 ; i < nFields; i ++){
		command += fields[i] + ",\n" ;
	}

	command += fields[ nFields] ;

			// selects the current version number
	command += " FROM "+ tableName +" WHERE " + versionField + "=";
	command += toString(version);

	return command;

}

// template function to create a new version
template <class T>
long int SpdVerTable::createNewVersion(const std::vector<T> &values){

	int newVersion = -1;

	Statement * stmt;
	spdDbConnection * conn;
	std::string command;

	try{
					//gets the next version
		newVersion = getNextVersionNumber();
		command = createInsertQuery( newVersion, values);

					// gets the oracle statement object through the spdConnection singleton
		conn = spdDbConnection::subscribe();

		stmt = conn->createStatement();		
		ResultSet *rs =  stmt->executeQuery( command);
		stmt->closeResultSet (rs);

					// updates the new version to next time 
		version=newVersion;

	}
	catch(exception &str) {
		spdLogger &logger = spdLogger::getInstance();

        logger.log( str.what());
		logger.log(command);

    }

	conn->terminateStatement (stmt);

	return newVersion;
}

// compares values with the data base
template <class T>
bool SpdVerTable::compareWithDb(const std::vector<T> &values){

	size_t nItems= fields.size();
	std::vector<T> dbValues(nItems);

	getValues( dbValues);
								// compares both
	return compareVectors(dbValues, values);
	
}



// updates values,
// 1 -goes to the database, 
// 2 -compare values,
// 3 -if needed creates a new version
long int SpdVerTable::update(const std::vector<long int> &values){

						// compares both
	if (!compareWithDb(values)){
						// if they are different then creates a new version
			return createNewVersion( values);
	}
	else {
					// if the values are the same does not need to update the values
		return version;
	}

	
}

// same function as above but for strings
// don't know why the linker did not like to make this one a template function also
long int SpdVerTable::update(const std::vector<std::string> &values){

						// compares both
	if (!compareWithDb(values)){
						// if they are different then creates a new version
			return createNewVersion( values);
	}
	else {
					// if the values are the same does not need to update the values
		return version;
	}

	
}

long int SpdVerTable::update(const std::vector<unsigned long int> &values){

						// compares both
	if (!compareWithDb(values)){
						// if they are different then creates a new version
			return createNewVersion( values);
	}
	else {
					// if the values are the same does not need to update the values
		return version;
	}

	
}

long int SpdVerTable::update(const std::vector<float> &values){

						// compares both
	if (!compareWithDb(values)){
						// if they are different then creates a new version
			return createNewVersion( values);
	}
	else {
					// if the values are the same does not need to update the values
		return version;
	}

	
}

// gets the latest version number in the database for this table
long int SpdVerTable::getMaxVersionNumber(){
	Statement * stmt;
	spdDbConnection * conn;
	long int nVersion=-1;
	std::string command;

	try {
		command = "SELECT MAX("+versionField+") FROM "+tableName;
		conn = spdDbConnection::subscribe();
		stmt = conn->createStatement();
		ResultSet *rs =  stmt->executeQuery( command);
		
	
		if (rs->next()){
			nVersion =(long int) rs->getUInt(1);
		
		}
		else{
			nVersion=0;
		}

		stmt->closeResultSet (rs);

	}
	catch(exception &str) {
        spdLogger &logger = spdLogger::getInstance();
        logger.log(str.what());
		logger.log(command);
    }

	conn->terminateStatement (stmt);

	return nVersion;
}


// gets the next version number
long int SpdVerTable::getNextVersionNumber(){

	long int maxVersion = getMaxVersionNumber();

	if (maxVersion >= 0) return maxVersion +1;
	else return maxVersion;

}

//************************************************************************
// Properies to get results
template<class T>
void SpdVerTable::getIntResults( ResultSet * rs, std::vector<T> &values){
	
					// fills up the values 
	if(rs->next()){
		for(unsigned int n = 0 ; n < fields.size() ; n ++){
			values[n] =(T) rs->getInt(n+1);

		}	
	}
		
}

template<class T>
void SpdVerTable::getUIntResults( ResultSet * rs, std::vector<T> &values){
	
					// fills up the values 
	if(rs->next()){
		for(unsigned int n = 0 ; n < fields.size() ; n ++){
			values[n] = (T) rs->getUInt(n+1);
		}	
	}
}

template<class T>
void SpdVerTable::getFloatResults( ResultSet * rs, std::vector<T> &values){
	
					// fills up the values 
	if(rs->next()){
		for(unsigned int n = 0 ; n < fields.size() ; n ++){
			values[n] =(T) rs->getFloat(n+1);
		}	
	}
}

void SpdVerTable::getStringResults(ResultSet * rs, std::vector<std::string> &values){
	const int size = 4000;
	ub2  *strSize;
	char (*buffer)[size];

	strSize = new ub2[fields.size()];
	buffer = new char[fields.size()][size];

							// reads all values with the setDataBuffer
	for (unsigned int n = 0 ; n < fields.size() ; n ++){
		rs->setDataBuffer(n+1, buffer[n], OCCI_SQLT_STR, size, &strSize[n]);
	}

	rs->next(1);
						// assigns all values to the vector
	for (unsigned int n = 0 ; n < fields.size() ; n ++){
		values[n] = (buffer[n]);
	}

					//deletes temporary buffers
	delete [] strSize;
	delete [] buffer;
}

// this function was embebed in the template getValues
// but it was flagging a error, so the only way to remove it 
// was to separate it in a new function
void SpdVerTable::getValues( std::vector<std::string> &values){
	Statement * stmt;
	spdDbConnection * conn;
	std::string command;
	spdLogger &logger = spdLogger::getInstance();

	try 
	{
	
		if (values.size() < fields.size()){
			values.resize( fields.size());
		}

							// sends the commands
		command = this->createSelectQuery();
		
		conn = spdDbConnection::subscribe();
		stmt = conn->createStatement();	

		ResultSet *rs =  stmt->executeQuery( command);
		
		getStringResults( rs, values);
		
		
		stmt->closeResultSet (rs);
	}
	catch(exception &str) {
        logger.log(str.what());
		logger.log(command);
    }
	
	conn->terminateStatement (stmt);
}

// template function to get values from the db
// for the moment takes care of the numeric values
// see above why strings have a dedicated function
template<class T>
void SpdVerTable::getValues( std::vector<T> &values){
	Statement * stmt;
	spdDbConnection * conn;
	std::string command;
	spdLogger &logger = spdLogger::getInstance();

	try 
	{

	
		if (values.size() < fields.size()){
			values.resize( fields.size());
		}

		
							// sends the commands
		command = this->createSelectQuery();
		
		conn = spdDbConnection::subscribe();
		stmt = conn->createStatement();	

		ResultSet *rs =  stmt->executeQuery( command);

		if (typeid(values[0])==typeid(long)){
			getIntResults( rs, values);
		}
		else if(typeid(values[0])==typeid(unsigned long)){
			getUIntResults( rs,values);
		}
		else if (typeid(values[0])==typeid(float)){
			getFloatResults(rs,values);
		}
		else{
			logger.log("Type not supported yet in the versioning table class");
		}

	
		stmt->closeResultSet (rs);
	}
	catch(exception &str) {
        logger.log(str.what());
		logger.log(command);
    }
	
	conn->terminateStatement (stmt);
}
