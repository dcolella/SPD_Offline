


#ifndef PIXELCONFFILE_H
#define PIXELCONFFILE_H

#include "stdafx.h"
#include<vector>


const int NGolConfig = 4;
const int NPixels = 10;
const int NPixelDacs = 44;


//! smal struct jus to have the cordinates
struct PixelCoordinate{
	Uint8 column;
	Uint8 row;
};

//using namespace std;
//!	Class to act as a container for the pixel configuration with the capability to handle files.
/*!	Class to act as a container for the pixel configuration with the capability to handle files.	
	It inherits from the base class pixelconf whish is only an interface*/
class PixelConfFile 
{

private:
	Uint8 DpiWaitBefRow;
	Uint8 DpiSebMeb;
	Uint32 DpiMaskChip;
	Uint32 DpiEventNumber;
	Uint8 DpiStrobeLenght;
	bool DpiHoldRow;
	Uint8 DpiSkipMode;
	bool DpiTdo8Tdo9;
	bool DpiEnableCeSeq;
	bool DpiDataFormat;
	Uint8 ApiDacRefHi;
	Uint8 ApiDacRefMid;
	Uint8 ApiGtlRefA;
	Uint8 ApiGtlRefD;
	Uint8 ApiAnTestHi;
	Uint8 ApiAnTestLow;
	Uint8 GolConfig[NGolConfig];
	Uint8 PixelDac[NPixels][NPixelDacs];

   	std::vector <PixelCoordinate> noisyPixels[NPixels];
	std::vector <PixelCoordinate> deadPixels[NPixels];

	friend class PixelConfDb;


public:

	Uint8 getDpiWaitBefRow() const;
	void setDpiWaitBefRow(Uint8 item);

	Uint8 getDpiSebMeb() const;
	void setDpiSebMeb(Uint8 item);

	Uint32 getDpiMaskChip() const;
	void setDpiMaskChip(Uint32 item);

	int getDpiEventNumber() const;
	void setDpiEventNumber(Uint32 item);

	Uint8 getDpiStrobeLenght() const;
	void setDpiStrobeLenght(Uint8 item);

	bool getDpiHoldRow() const;
	void setDpiHoldRow(bool item);

	Uint8 getDpiSkipMode() const;
	void setDpiSkipMode(Uint8 item);

	bool getDpiTdo8Tdo9() const;
	void setDpiTdo8Tdo9(bool item);

	bool getDpiEnableCeSeq() const;
	void setDpiEnableCeSeq(bool item);

	bool getDpiDataFormat() const;
	void setDpiDataFormat(bool item);

	Uint8 getApiDacRefHi() const;
	void setApiDacRefHi(Uint8 item);

	Uint8 getApiDacRefMid() const;
	void setApiDacRefMid(Uint8 item);

	Uint8 getApiGtlRefA() const;
	void setApiGtlRefA(Uint8 item);

	Uint8 getApiGtlRefD() const;
	void setApiGtlRefD(Uint8 item);

	Uint8 getApiAnTestHi() const;
	void setApiAnTestHi(Uint8 item);

	Uint8 getApiAnTestLow() const;
	void setApiAnTestLow(Uint8 item);

	Uint32 getGolConfig() const;
	Uint8 getGolConfig(int pos) const;
	void setGolConfig(int pos, Uint8 item);
	void setGolConfig( Uint32 intem);

	Uint8 getPixelDac(int pixel, int dac) const;
	void setPixelDac(int pixel, int dac, Uint8 item);

	float getAnalogConvValue(int pos) const;
	void setAnalogConvValue(int pos, float item);

	int getNoisyPixelCount(int pixelChip)const;
	void insertNoisyPixel(int pixelChip, int column, int row);
	void getNoisyPixel( int pixelChip, int pos, int &column, int &row)const;
	PixelCoordinate getNoisyPixel( int pixel, int pos)const{return noisyPixels[pixel][pos];};

	void removeNoisyPixel( int pixelChip, int pos);
	void insertNoisyPixel(int pixel, PixelCoordinate noisy){noisyPixels[pixel].push_back(noisy);} ;
	void clearNoisyPixels(int pixel){ noisyPixels[pixel].clear();};
	
	void setNoisyPixelsVect(unsigned chip, std::vector <PixelCoordinate> noisyPixelsVect){noisyPixels[chip] = noisyPixelsVect;};
	std::vector <PixelCoordinate> getNoisyPixelsVect(unsigned chip)const{ return noisyPixels[chip];};
	std::vector <PixelCoordinate> &getNoisyPixelsVect(unsigned chip){ return noisyPixels[chip];};

	int getDeadPixelCount(int pixelChip)const;
	void getDeadPixel(int pixelChip, int pos, int &column, int &row)const;
	PixelCoordinate getDeadPixel( int pixel, int pos)const{return deadPixels[pixel][pos];};
	void insertDeadPixel( int pixelChip,  int column, int row);
	void insertDeadPixel(int pixel, PixelCoordinate deadPix){deadPixels[pixel].push_back(deadPix);} ;
	void removeDeadPixel( int pixelChip, int pos);
	void clearDeadPixels(int pixel){deadPixels[pixel].clear();};

	int openFile(  const char * filename);
	void printData( )const;

	void dumpToText(const char * filename, int channel)const;
	void printNoisyAllPixels();
	void printNoisyInPixel(int pixel);

	PixelConfFile(void);
	~PixelConfFile(void);
};

#endif //PixelConfFile_h