#ifndef SPDGLOBALS_H
#define SPDGLOBALS_H

#include "spdvertable.h"

//using namespace std;

const char sqlDebug[]=".\\debug\\sqlDebug.sql";

namespace spdGlobals{
	
		//! functions to get a dac number and a dac name
	const char * getDacName(int index);
	int getDacNumber( const char *dacName);

	std::string intToStr( int number);

	void saveQuerytoFile( std::string coment, std::string query);
	//!void insertNumber(std::string &str, int number, const char * separator);
	void deleteSqldebug(void);
		//! functions to pack a value in a 32 bit register and to unpack
	Uint32 extractFromInt32( Uint32 input, int offset, int bits);
	void insertIntoInt32( Uint32 &input, Uint32 value, int offset, int bits );
	
		//! template function to convert to string 
	template <class T>
	std::string toString( T value );

		//! template function to get a value from a string	
	template<typename T>
	T fromString( const std::string& s);

	std::vector<std::string> getMcmVerFields();
	std::vector <std::string> getNoisyVerFields();
	std::vector<spdDbTypes> getNoisyVerTypes();

		//! split string function
	std::vector<std::string> splitStr(const std::string& text, const std::string& delimeter);

	//! inserts bit into and array
	void insertBitsIntoArray( Uint32 *array, Uint32 data, unsigned offset, Uint32 nBits );

	//! sets a row in a pixel matrix 
	void setRowInMatrix(Uint32 matrix[], unsigned row);

	//! sets a hit in a pixel matrix
	inline void setPixelInMatrix(Uint32 matrix[], unsigned row, unsigned column){
		insertIntoInt32 (matrix[(8*column) + row/32], 1, row%32, 1);
	};

		//!returns the analog pilot name
	const char * getApiDacName(unsigned apiDac);
};
#endif
