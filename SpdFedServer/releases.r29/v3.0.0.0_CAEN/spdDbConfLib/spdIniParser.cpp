#include "StdAfx.h"
#include ".\spdIniParser.h"
#include <sstream>


namespace spdCalib{
IniParser::IniParser(void)
{

}

IniParser::~IniParser(void)
{
}



IniParser::IniParser( const char filename[]){

	this->openFile(filename);
}


// will search for an '=' and something 
Entry IniParser::getEntryFromLine(const string &line){
	
	stringstream strLine(line);
	string firstPart;
	Entry out;


	getline( strLine, firstPart, '=');
	stringstream aux(firstPart);
	
	aux >> out.name;
	strLine >> out.value ;

	return out;
}

string IniParser::getCategoryFromLine( const string &line){
	stringstream strLine(line);

	size_t pos1 = line.find('[')+1;
	size_t pos2 = line.find(']');
	
	if(  (pos2-pos1) >= line.size() ) return "" ;

		// gets the substring from the first match to the second
	string out = line.substr( pos1, pos2-pos1);

	return out;
	
}

// method to remove coments from a line 
string IniParser::removeComments( const string &line){
	stringstream strLine(line);

	string out;
	getline( strLine, out ,'#');

	//if (strLine.str().size()) cout << "got comment :" <<  strLine.str() << endl;

	return out;

}
/**********************************************************
	parses one line, using the regular exp 
		defined as static variables
***********************************************************/
void IniParser::parseLine( string &line){
	
	string cleanLine = removeComments( line);
	string cat = getCategoryFromLine( cleanLine);
	if (cat != ""){
		insertCategory( cat);
		return;
	}
	
	Entry value = getEntryFromLine( cleanLine);

	if (value.name != "") {
		insertEntry( value);
	}

}


/**********************************************************
	private method to inser a new category
***********************************************************/
void IniParser::insertCategory(string categoryName){


	Category newCategory;
	newCategory.name =categoryName;
	currCategory = categoryName;
					// inserts the new category
	categoryList[categoryName] = newCategory;
}

/**********************************************************
	inserts one entry in the current category
***********************************************************/
void IniParser::insertEntry( Entry newEntry){

	categoryList[currCategory].entries[newEntry.name]= newEntry.value;
}

/***************************************************************
Open a file and parses it 
***************************************************************/
void IniParser::openFile(const char filename[]){

	using std::runtime_error;
	
	fstream infile(filename, fstream::in );

	if (!infile.is_open()){
		
		throw(runtime_error ("(SpdIniParser::openFile) could not open file") );
	}

	string line;
			// parses all the lines 
	while(getline(infile, line)){
		
		parseLine( line);
	}
}


/**********************************************************************
	returns a vector with all the categories existing in the parser
************************************************************************/
std::vector<string> IniParser::categories(){

	CategoryMap::iterator it;
							// alocates a vector with the size of the number of entries
	std::vector<string> keys( categoryList.size() );

	int n=0;
	for( it = categoryList.begin(); it != categoryList.end(); ++it){
		keys[n]=it->first;
		++n;
	}
	
	return keys;
	
	
}

}