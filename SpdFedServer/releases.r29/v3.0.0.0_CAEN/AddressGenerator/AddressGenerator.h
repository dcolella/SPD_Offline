#pragma once
#ifndef AddressGenerator_h
#define AddressGenerator_h

#include "stdafx.h"
#include "ChToAddrDecoder.h"
#include "LinkRxIntAddr.h"
#include "RoutIntAddr.h"
#include "BusyCardIntAddr.h"
#include "../spdDbConfLib/spdLogger.h"

const int nHalfStaves= 120;
const int nRouters = 20;
const int nBusyCards = 2;

const int nHsInSide=60;
const int nHsInRouter = 6;

//! Class that keeps all memory address of registers in routers, LRXs and Busy Card
/*!	its a sigleton class. It keeps as internal members classes 
	for all router adresses, link rx's and busy card's 
	it initializes them and has getters for these*/
class AddressGenerator{
  private:
	//! internal container of link receiver adresses 
    LinkRxIntAddr LRxAddr[nHalfStaves];
	//! internal container of router adresses
    RoutIntAddr RoutAddr[nRouters];
	//! internal container of busy card adresses 
	BusyCardIntAddr BusyCardAddr[nBusyCards];

	//! logger
	spdLogger *log;
	//! private constructor this is a singleton class 
	AddressGenerator();

  public:
	//! method to get the only instance of the adress generator 
	static AddressGenerator &getInstance();
	                  
	~AddressGenerator(void);
	
	//! calls all router and link receiver objects making them recalculate all memory addresses 
    void ReDefineAddr(void);                
    
	//! returns a reference of a lrx addresses object
	/*! \param ChNumb channel number 0..119
		\return lrx addresses object reference*/
	LinkRxIntAddr & getLRxAddr(unsigned ChNumb);
	//! returns a reference of a router addresses object
	/*! \param ChNumb channel number 0..119
		\return rotuer addresses object reference*/
	RoutIntAddr & getRoutAddr(unsigned ChNumb);
	//! returns a reference of a busy card addresses object
	/*! \param ChNumb channel number 0..119
		\return busy card addresses object reference*/
	BusyCardIntAddr & getBusyCardAddr(unsigned ChNumb);	


};


#endif