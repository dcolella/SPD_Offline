#pragma once
#include "../BitManager/bitsmanage.h"


//! Channel to address decoder class (should be removed)
/*!	it keeps in memory the cards numbers router, busy cards and halfstaves*/
class ChToAddrDecoder : public BitsManage {
  private:	
	
	UInt32 RouterBoardAddresses[20];
    UInt32 BusyBoardsAddresses[2];
	UInt8 PositionToChannel[10][6][2]; 
    
  public:
	ChToAddrDecoder(void);                                            
	~ChToAddrDecoder(void); 
	
	
	UInt32 GetVectRoutBoardAddr(UInt8 BoardNumb){return RouterBoardAddresses[BoardNumb];};        
    
	UInt32 GetVectBusyBoardAddr(UInt8 BoardNumb){return BusyBoardsAddresses[BoardNumb];};;

	UInt8 GetChannNumbFromPosition(UInt8 SectorNumb, UInt8 StaveNumb, UInt8 Side); 
    
	                        
	UInt32 GetBoardAddr(UInt8 ChNumb);                        
	UInt32 GetRouterCh(UInt8 ChNumb){return ChNumb % 6;};   
    

};
