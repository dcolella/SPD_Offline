#pragma once
#ifndef BusyCardIntAddr_h
#define BusyCardIntAddr_h

#include "../BitManager/bitsmanage.h"
//! class to keep busy card memory addresses
class BusyCardIntAddr : public BitsManage{
private:
	UInt32 BoardAddr;

	UInt32 VMEResetAddr;
	UInt32 VersionNumberAddr;
	UInt32 EnDriverOrReceiverAddr;
    UInt32 L0DelayAddr[24];
	UInt32 ControlRegAddr;
	UInt32 BusyMaskAddr;
    UInt32 L0CounterAddr;
    UInt32 ReadInputAddr;
	UInt32 CnCounterAddr[4];
public:
	BusyCardIntAddr(void);
	~BusyCardIntAddr(void);
	void SetBusyCardAddr(UInt32 BrdAdd);  //ok 
    void ShiftAddresses(int Nshift);       //ok

    void ModifyBusyCardAddresses(void);      //ok
    

	

	//busyCard
	UInt32 GetVMEResetAddr(UInt8 ChNumb){return VMEResetAddr;};
	UInt32 GetVersionNumberAddr(UInt8 ChNumb){return VersionNumberAddr;};
	UInt32 GetEnDriverOrReceiverAddr(UInt8 ChNumb){return EnDriverOrReceiverAddr;};
	UInt32 GetL0DelayAddr(UInt8 ChNumb, UInt8 regNumber){return L0DelayAddr[regNumber];};
	UInt32 GetControlRegAddr(UInt8 ChNumb){return ControlRegAddr;};
	UInt32 GetBusyMaskAddr(UInt8 ChNumb){return BusyMaskAddr;};
	UInt32 GetL0CounterAddr(UInt8 ChNumb){return L0CounterAddr;};
	UInt32 GetReadInputAddr(UInt8 ChNumb){return ReadInputAddr;};
	UInt32 GetCnCounterAddr(UInt8 ChNumb, UInt8 regNumber){return CnCounterAddr[regNumber];};

	friend class AddressGenerator;  
};



#endif