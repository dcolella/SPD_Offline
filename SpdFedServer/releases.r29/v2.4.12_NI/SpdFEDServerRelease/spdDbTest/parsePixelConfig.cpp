
#include "stdafx.h"
#include <iostream>

#include "../spdDbConfLib/spdIniParser.h"
#include "../spdDbConfLib/spdDbConnection.h"
#include "../spdDbConfLib/NoisyPixelDB.h"

const char iniFileName[] = "./spdFedIni.ini";
using namespace spdCalib;


void printIni(spdCalib::IniParser & parsed);
void printNoisyPixel( NoisyPixelDB &noisy);


int main(int argc, char* argv[])
{
	
	spdCalib::IniParser test;
	NoisyPixelDB noisy;



	printIni(test);
					// gets the values from the ini file
	IniParser inifile(iniFileName);
	Category settings = inifile["DataBase"];
									// sets the settings in the connection to database object
	spdDbConnection * conn = spdDbConnection::subscribe();
	conn->setDbSettings(settings);
	

	cout << "got DB user name: "<< conn->getDbUserName().c_str() << endl ;
	cout << "DB connection string: "<< conn->getDbConstring().c_str() << endl;
	

	
	noisy.getFromDb('C');

	cout << noisy.inspect();
	getchar();
	cout << "---------------------------------------\n";
	printNoisyPixel(noisy);
	getchar();
	return 0;
}



// prints the contend of an ini file  
void printIni(spdCalib::IniParser & parsed){


	std::vector <string> keys = parsed.categories();

	for (unsigned i = 0; i < keys.size() ; ++i){

		spdCalib::Category cat = parsed[keys[i]];
		cout << cat.name.c_str() << "\n";

		for( unsigned int n = 0 ; n < cat.entries.size(); ++n){
			cout << "--->"<< cat.entries[n].name ;
			cout << "=" << cat.entries[n].value  << "\n";

		}
	}
	
}
// will print the noisy pixel list of one instance of the class 
void printNoisyPixel( NoisyPixelDB &noisy){

	char side = noisy.getSide();
	int version = noisy.getVersionNumber();

	cout << "noisy pixel list, side " << side << ", version= " << version << endl;
	for (unsigned hs = 0 ; hs < nHalfstaves ; hs ++){
		unsigned count = 0;
				// will count the total number of noisy pixels
		for(unsigned chip =0; chip < nChips ; chip ++){
			count +=noisy.getHsNoisy(hs,chip).size();
		}
		if (count > 0 )cout << "halfstave " << hs << endl;
		
		for (unsigned chip = 0 ; chip < nChips ; chip ++){
			std::vector<PixelCoordinate> noisypix = noisy.getHsNoisy( hs, chip);
			for (unsigned pixel = 0 ; pixel < noisypix.size() ; pixel ++){
				cout << "\t"<< chip << ": " << (int) noisypix[pixel].column ;
				cout << ", " <<(int) noisypix[pixel].row << endl;
			
			}
		}
	}
	

}
