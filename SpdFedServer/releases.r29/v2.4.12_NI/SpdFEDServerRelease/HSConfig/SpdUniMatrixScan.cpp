#include "StdAfx.h"
#include ".\spdunimatrixscan.h"

#include "SPDConfig.h"

SpdUniMatrixScan::SpdUniMatrixScan(SPDConfig *spdconf)
{
	this->parent = spdconf;
	this->log = &spdLogger::getInstance();
	this->scanType =3;
	this->active=false;
	this->triggerNumber=0;
	this->rowStart=0;
	this->rowEnd=0;
	this->maskNoActive=false;
	this->currentRow=0;

}

SpdUniMatrixScan::~SpdUniMatrixScan(void)
{
}


// sets a certain number of TP rows all enabled channels
void SpdUniMatrixScan::SetMatrixToAllEnabled(UInt8 rStart, UInt8 nRows){

	UInt32 matrix[256];

	// clears the matrix
	for (int row = 0 ; row < 256 ; row ++){
		matrix[row]= 0 ;
	}
	
	if ( (rStart + nRows) > 256){
		log->log("ERROR: Error trying to set a row bigger than the number of rows this step will be skipped");
		return;
	}

	// will insert the number of '1s' defined by nRows 
	// starting from the row position (each 8 32 bit = 255 bits)
	for(UInt8 i =0; i < 32; i++){
		spdGlobals::insertBitsIntoArray( &matrix[i*8], 0xffffffff, rStart, nRows);
	}

			// loading the matrix with active rows in all the half-staves in test state
	for (unsigned int channel = 0; channel < NHalfStaves ; channel++){

		if (parent->halfStaves[channel]->getChActStatus()==2){
			UInt32 error = parent->halfStaves[channel]->LoadHSPixelMatrix(1023, matrix, true, true);
			if (error){
				log->log("ERROR: error setting test pulse matrix in hs %d",channel );
			}
		}
	}
}

unsigned SpdUniMatrixScan::start( unsigned trigNumber, unsigned rStart, unsigned rEnd, bool mskNActive){

	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	this->triggerNumber = trigNumber;
	this->rowStart = rStart;
	this->rowEnd = rEnd;
	this->currentRow = rStart;
	this->maskNoActive = mskNActive;
	this->active = true;
	timerScan.start("starting uniformity matrix scan");
	return 0;

}

unsigned SpdUniMatrixScan::restart(void){
	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	this->active=true;
	return 0;
}

void SpdUniMatrixScan::SetScansHeader(UInt8 routerNumber){

	const int sizeOfHeader = 17;
	UInt32 header[sizeOfHeader];
	unsigned chipsEnabled[6];

	for (unsigned i = 0 ; i < sizeOfHeader ; ++i)header[i] =0;

		// Creates the array with the chips activated for the calibration 
	for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter ++) {
		unsigned int HSnumber = routerNumber*6 + HSinRouter;

		if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
			chipsEnabled[HSinRouter] = parent->halfStaves[HSnumber]->GetDPI()->GetDPIConfMaskChip();
		}
		else chipsEnabled[HSinRouter] = 0;
	}	

	unsigned sideOffsetRouter = 0;
	// if we are on side C, the routerNum will go from 10 to 19
	if (this->parent->side == 'C'){
		sideOffsetRouter = 10;
	}

	header[0] = routerNumber + sideOffsetRouter;
	header[1] = scanType;
	header[2] = triggerNumber;
	header[3] = chipsEnabled[0] + (chipsEnabled[1] << 10) + (chipsEnabled[2] << 20);
	header[4] = chipsEnabled[3] + (chipsEnabled[4] << 10) + (chipsEnabled[5] << 20);

	header[5] = (parent->HSData.getGlobalDBVersionNumber() << 16) + 0x1ff;
	
	header[6] = ((UInt32)this->rowStart << 24) + ((UInt32) this->rowEnd << 16) 
				+ ((UInt32) this->currentRow << 8);
	
	parent->routers[routerNumber]->WriteHeader( header, sizeOfHeader);
}


void SpdUniMatrixScan::scan(){
	if(this->active == false) return;

	int nRows;
	int maxRows = 2;

	int rowsMissing= this->rowEnd - this->currentRow +1;
	
	if( rowsMissing > maxRows ){
		nRows=maxRows;
	}
	else{
		nRows=rowsMissing;
	}
		
	// sets the rows in the matrix to all enabled channels
	this->SetMatrixToAllEnabled(this->currentRow,nRows);

	for(unsigned router = 0; router < NRouters; router++){
		if( parent->isRouterActive(router)){
//			log->log("setting header in router %d", router);
			this->SetScansHeader( router);  
		}
		else{
			//log->log("skipping setting of header in router %d", router);
		}
	}

	parent->sendTriggerToEnabledRouters(triggerNumber);

	log->log("Uniformity matrix : finished rows %d  - %d", this->currentRow,this->currentRow+nRows-1);
	
	if( this->currentRow + nRows >  this->rowEnd){
		this->active = false;

		// clean the last test pulse with an empty matrix
		this->SetMatrixToAllEnabled(0, 0);
		
		timerScan.stop();
		log->log("Uniformity Matrix scan finished, time elapsed = %f", timerScan.elapsed_time());
	}

	this->currentRow+= nRows;
	return;
}