#pragma once

#include ".\DigitalPilot.h"
#include ".\APIConvParam.h"
#include "..\spdDbConfLib\PixelConfDb.h"
#include "..\spdDbConfLib\spdGlobals.h"

const int numberOfChips = 10;
const int numberOfRows = 256;
const int numberOfCols = 32;

//! Class to keep the default configuration settings of one halfstave
/*! Mainly a container class */
class HSConfDefault : public APIConvParam{
    //!Keeps the configuration file name
	std::string FileName;
	UInt32 FileSize;
	

     
	 //!Pixel DAC Values 0..43 chip 0, 44..87 chip 1, ...
	 UInt8 PixDACVect[440];
     
	 //!API DAC Values 0 = REF HI, REF MID, GTL REF, TEST HI, TEST LOW
	 UInt8  APIdacVect[6];
     
	 //!DPI configuration values
	 DigitalPilot DPI;
	 //!GOL configuration values
     UInt32 GOLConfVect;
     
     
		//! noisy pixel default values
	 std::vector <PixelCoordinate> noisyPixelsDefault[numberOfChips];
	// std::vector <PixelCoordinate> deadPixelsDefault[numberOfChips];

	void LoadToMemory(UInt8 * FileContent);

public:
	HSConfDefault(void);
	~HSConfDefault(void);
    
	//! getter for the file name 
	const char * GetFileName(void){return FileName.c_str();};
	void SetFileName(const char * filename);
    
	UInt32 GetFileSize(void){return FileSize;};
	void SetFileSize(UInt32 size){FileSize = size;};
	int LoadDefaultsFromFile(const char * filename);



	DigitalPilot * GetDPIDefault(void){return &this->DPI;};
	UInt8 * GetPixelDACVectDefault(void){return PixDACVect;};

	UInt8 GetPixelDACDefault(unsigned chip, unsigned dacNumber){return PixDACVect[chip*NPixelDacs+dacNumber];};

    UInt8 * GetAPIdacVectDefault(void){return APIdacVect;};
	UInt32 * GetDPIConfVectDefault(void){return DPI.GetDPIConf();};
	UInt32 * GetDPIInternalVectDefault(void){return DPI.GetDPIInternal();};
	UInt32  GetGOLConfVectDefault(void){return GOLConfVect;};
    
	UInt32 LoadClassDefaultsDb(const PixelConfDb &dbData);
	UInt32 SetClassDefaultsDb(PixelConfDb &dbData);

	//APIConvParam * GetAPIConversion(void){return &APIConversion;};
    
	
    void SetPixDACVectDefault(UInt8 * Input);                 //all to be reviewed
	void SetAPIdacVectDefault(UInt8 * Input); 
	void SetDPIConfVectDefault(UInt32 * Input){this->DPI.SetDPIConf(Input);};
	void SetGOLConfVectDefault(UInt32 Input){GOLConfVect = Input;}

	//! wil set the internal noisy pixel vector using as input an array liek reading from a configuration file
	UInt16 SetNoisyPixelVectDefault(UInt8 * Input);
	
	//! will set the internal noisy pixels with a proper vector 
	void SetNoisyPixelVectDefault(unsigned chip,std::vector<PixelCoordinate> noisy){this->noisyPixelsDefault[chip] = noisy;};
	
	//! wil set the internal noisy pixel vector using as input the matrix like the one received by pvss
	void SetNoisyPixelVectDefault(UInt32 Input[numberOfChips][256]);
	//! function to get the noisy pixels with the firmat of the jtag command for one pixel chip, 
	/*!the matrix will be returned in the argument 'output' 
		\param chip pixel chip number to get the noisy pixel/mask matrix
		\param output an array which wil be filled inside this function with the output data*/
	void GetNoisyPixelVectDefault(unsigned chip, UInt32 output[numberOfRows]);
	//! function to get the noisy pixels with the firmat of the jtag command for all 10 chips in one HS
	/*!the matrix will be returned in the argument 'output' 
		\param output this array will be filled inside with the data for all pixels */
	void GetNoisyPixelVectDefault( UInt32 output[numberOfChips*numberOfRows]);

	
	      
	//! will return the number of noisy pixels in one chip
	unsigned GetNoisyPixelNumberDefault(int chip){return (unsigned) noisyPixelsDefault[chip].size();};
	//! will return the total number of noisy pixels for all 10 chios
	unsigned GetNoisyPixelNumberDefault(void);
	
	std::vector <PixelCoordinate> & GetNoisyPixVectDefault(int chip){return noisyPixelsDefault[chip];};
	//std::vector <PixelCoordinate> & GetDeadPixVectDefault(int chip){return deadPixelsDefault[chip];};



};



/*
Configuration file content (byte)
200 spare location
16 TestCondDef
40 LadderIV
440 PixDACVect
6 apiVect
8 DPIVect
4 GOLVect
1 Channel
16000AnalConv
2 NoisyPixlengh
n NoisipixVect 1stbyte chip, 2nd bute row 3th byte column
2 DeadPixLengh
n DeadPixelVect
2 NoteLenght
n Note
120 THVect
*/


