#include "StdAfx.h"
#include ".\spddelayscan.h"
#include "SPDConfig.h"


SpdDelayScan::SpdDelayScan(SPDConfig *spdconf)
{
	this->misControlIndex= 43;
	this->delayControlIndex = 42;
	this->dac.current=0;
	this->dac.minimum=0;
	this->dac.maximum=0;
	this->dac.index=delayControlIndex;
	this->dac.step=0;

    this->triggerN=0;
	this->waitTime=0;
    this->scanType=5;

	this->chipSelect=0x3ff;
	
	this->internalTrigger = true;

	this->misControlValue = 0;

	this->parent = spdconf;
	this->log = &spdLogger::getInstance();
	this->active=false;


}

SpdDelayScan::~SpdDelayScan(void)
{

}

// start of the scan
unsigned SpdDelayScan::start( UInt32 triggerNumber, 
						UInt8 dacStart, 
						UInt8 dacEnd, 
						UInt8 newStep, 
						bool internTrig, 
						UInt32 newWaitTime){

	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	this->internalTrigger = internTrig;
	this->triggerN = triggerNumber;
	
	this->dac.minimum = dacStart;
	this->dac.maximum =dacEnd;
    this->dac.step = newStep;
	this->dac.current = dacStart;

	this->waitTime = newWaitTime;
	this->active = true;

	// first starts with the minimum value of misc control
	this->misControlValue = MinMiscControl;
	timerScan.start("starting delay scan");

	return 0;
	
}

unsigned SpdDelayScan::restart(void){

	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	this->active=true;
	return 0;
}
// writes the router header for this scan						
void SpdDelayScan::SetScansHeader(UInt8 routerNumber){

	const int sizeOfHeader = 17;
	UInt32 header[sizeOfHeader];
	unsigned chipsEnabled[6];

	for (unsigned i = 0 ; i < sizeOfHeader ; ++i)header[i] =0;

		// Creates the array with the chips activated for the calibration 
	for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter ++) {
		unsigned int HSnumber = routerNumber*6 + HSinRouter;

		if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
			chipsEnabled[HSinRouter] = parent->halfStaves[HSnumber]->GetDPI()->GetDPIConfMaskChip();
		}
		else chipsEnabled[HSinRouter] = 0;
	}	


	unsigned sideOffsetRouter = 0;
	// if we are on side C, the routerNum will go from 10 to 19
	if (this->parent->side == 'C'){
		sideOffsetRouter = 10;
	}

	header[0] = routerNumber+sideOffsetRouter;
	header[1] = scanType;
	header[2] = this->triggerN;
	header[3] = chipsEnabled[0] + (chipsEnabled[1] << 10) + (chipsEnabled[2] << 20);
	header[4] = chipsEnabled[3] + (chipsEnabled[4] << 10) + (chipsEnabled[5] << 20);

	header[5] = ((UInt32)dac.minimum << 24) + 	
				((UInt32)dac.maximum<< 16) + 
				((UInt32)dac.step << 8) + 
				dac.index;
	
	header[6] = dac.current;
	header[7] = this->misControlValue << 24;
	//Misc_Ctrl Delay scan only	8	( Word[7]>>24 ) & 0x000000ff
	//Chip Scanned MinTh scan only	10	( Word[7]>>(16+chip) ) & 0x00000001

	
	parent->routers[routerNumber]->WriteHeader( header, sizeOfHeader);
	
}

// performs a step in the scan
void SpdDelayScan::delayStep(){
	log->log("delay scan: current value %d, misc control %d, step %d , max %d",this->dac.current, this->misControlValue, this->dac.step,this->dac.maximum);

	for(unsigned router = 0; router < NRouters; router++){
		if( parent->isRouterActive(router)){
			parent->routers[router]->setBusyFlag(true);
		}

	}
	
		// will set the dacs in all enabled halfstaves
	for(unsigned hs = 0 ; hs < NHalfStaves; hs++){
		if(parent->halfStaves[hs]->getChActStatus()==2){
				// sets the misc control and delay control
			parent->halfStaves[hs]->LoadHSPixelDAC( chipSelect ,misControlIndex, this->misControlValue);  
			parent->halfStaves[hs]->LoadHSPixelDAC( chipSelect ,delayControlIndex, this->dac.current);  

	    }
	}

	
	for(unsigned router = 0; router < NRouters; router++){
		if( parent->isRouterActive(router)){
			this->SetScansHeader(router);
			parent->routers[router]->setBusyFlag(false);
		}

	}

	if(internalTrigger){
		
		parent->sendTriggerToEnabledRouters(triggerN);
	}

	
	cout << "waiting between scans: ";
			// time to wait between dac steps
	UInt32 startTime = GetTickCount();						
	while( (GetTickCount() - startTime ) < this->waitTime){
		Sleep(10);
		cout << "*";
	}			
	cout << endl;


	

}

void SpdDelayScan::scan(){
	if(!active) return;
    
	this->delayStep();
	
	// will switch over miscontrol values
	if (this->misControlValue == MinMiscControl){
		this->misControlValue = MaxMiscControl;
	}
	else if(this->misControlValue == MaxMiscControl){

		if(this->dac.current == this->dac.maximum){
			timerScan.stop();
			log->log("INFO: delay scan finished, elapsed time = %f", timerScan.elapsed_time());
			active = false;
			return;
		}

		this->dac.current += this->dac.step;

		if(this->dac.current > this->dac.maximum) this->dac.current = this->dac.maximum;

		this->misControlValue = MinMiscControl;
	}
	else{

		log->log("ERROR: misc control value %d out of range ", this->misControlValue);
		this->misControlValue = MinMiscControl;
	}
	
					   
}