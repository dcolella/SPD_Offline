#include "StdAfx.h"
#include ".\spddacscan.h"
#include "SPDConfig.h"

SpdDacScan::SpdDacScan(SPDConfig * spdConf)
{
    
	this->dac.current=0;
	this->dac.minimum=0;
	this->dac.maximum=0;
	this->dac.index=0;
	this->dac.step=0;

    this->triggerN=0;
	this->waitTime=0;
    this->scanType=2;

	this->chipSelect=0x3ff;
	
	internalTrigger = true;

	this->parent = spdConf;
	log = &spdLogger::getInstance();

	this->active = false;
}

// starts a scan
unsigned SpdDacScan::start( UInt32 triggerNumber, 
				   UInt8 dacStart, 
				   UInt8 dacEnd, 
				   UInt8 newStep, 
				   UInt8 dacIdentifier, 
				   bool internTrig, 
				   UInt32 newWaitTime){

	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	this->internalTrigger = internTrig;
	this->dac.index = dacIdentifier;    
	this->triggerN = triggerNumber;
	
	this->dac.minimum = dacStart;
	this->dac.maximum =dacEnd;
	this->dac.current = dacStart;

    this->dac.step = newStep;
	this->waitTime = newWaitTime;


	this->active = true;

	return 0;
	
}

unsigned SpdDacScan::restart(void){
	if(this->parent->isScanActive()){
		log->log("ERROR: Another scan is already active");
		return 1;
	}

	this->active=true;
	return 0;
}
				   /*
header to send to DAQ during scans
	Header lenght
	0 Router Number
	1 Type (bits 0..7 operation mode, bit 8 : 0 = Normal data, 1 = Histogram format)
	2 Triggers
	3 Chip Present 1 (HS 2, 1, 0)
	4 Chip Present 2 (HS 5, 4, 3)
	5 DACStart, DACEnd, Step, DAC Identifier (8 bit registers)
	6 Row start, Row end, Row inex, DAC value/ row index (8,8,8, 8 bits)
	7 DACHigh0, DACLow0, DACHigh1, DACLow1 (HS 0,1)  
	8 DACHigh2, DACLow2, DACHigh3, DACLow3 (HS 2,3)
	9 DACHigh4, DACLow4, DACHigh5, DACLow5 (HS 4,5)
	10 TPAmplitude0 (*100)
	11 TPAmplitude1 (*100)
	12 TPAmplitude2 (*100)
	13 TPAmplitude3 (*100)
	14 TPAmplitude4 (*100)
	15 TPAmplitude5 (*100)

	Note: DelayScan point 7 DCAHight0 become misc_control
		MinThScan point 7 DCAHight0 become chip to scan

Operation Mode:

	* 0 Minimum Th
	* 1 MeanTH
	* 2 DAC scan
	* 3 Uniformity Scan
	* 4 Noise Scan
	* 5 Delay Scan
	
DAC Identifier:

	* 0 .. 44 pixel chip DACs
	* 100 .. 105 API DACs

*/
void SpdDacScan::SetScansHeader(UInt8 routerNumber){

	const int sizeOfHeader = 17;
	UInt32 header[sizeOfHeader];
	unsigned chipsEnabled[6];

	for (unsigned i = 0 ; i < sizeOfHeader ; ++i)header[i] =0;

		// Creates the array with the chips activated for the calibration 
	for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter ++) {
		unsigned int HSnumber = routerNumber*6 + HSinRouter;

		if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
			chipsEnabled[HSinRouter] = parent->halfStaves[HSnumber]->GetDPI()->GetDPIConfMaskChip();
		}
		else chipsEnabled[HSinRouter] = 0;
	}	

	unsigned sideOffsetRouter = 0;
	// if we are on side C, the routerNum will go from 10 to 19
	if (this->parent->side == 'C'){
		sideOffsetRouter = 10;
	}

	header[0] = routerNumber +sideOffsetRouter;
	header[1] = scanType;
	header[2] = this->triggerN;
	header[3] = chipsEnabled[0] + (chipsEnabled[1] << 10) + (chipsEnabled[2] << 20);
	header[4] = chipsEnabled[3] + (chipsEnabled[4] << 10) + (chipsEnabled[5] << 20);

	header[5] = ((UInt32)dac.minimum << 24) + 	
				((UInt32)dac.maximum<< 16) + 
				((UInt32)dac.step << 8) + 
				dac.index;
	
	header[6] = dac.current;
	
	//Misc_Ctrl Delay scan only	8	( Word[7]>>24 ) & 0x000000ff
	//Chip Scanned MinTh scan only	10	( Word[7]>>(16+chip) ) & 0x00000001
	header[7] = this->getChipSelected() << 16;

	parent->routers[routerNumber]->WriteHeader( header, sizeOfHeader);
	
}



void SpdDacScan::scan(){
	if(this->active== false) return;
    
	log->log("dac scan: current value: %d, step %d , max %d",this->dac.current,this->dac.step,this->dac.maximum);

	for(unsigned router = 0; router < NRouters; router++){
		if( parent->isRouterActive(router)){
			parent->routers[router]->setBusyFlag(true);
		}

	}
	
		// will set the dacs in all enabled halfstaves
	for(unsigned hs = 0 ; hs < NHalfStaves; hs++){
		if(parent->halfStaves[hs]->getChActStatus()==2){

			// sets the dacs in all halfstaves
			parent->halfStaves[hs]->LoadHSPixelDAC( chipSelect ,this->dac.index, this->dac.current);  

	    }
	}


	for(unsigned router = 0; router < NRouters; router++){
		if( parent->isRouterActive(router)){
			this->SetScansHeader(router);
			parent->routers[router]->setBusyFlag(false);
		}

	}

	if(internalTrigger){
		
		parent->sendTriggerToEnabledRouters(triggerN);
	}

	
	cout << "waiting between scans: ";
			// time to wait between dac steps
	UInt32 startTime = GetTickCount();						
	while( (GetTickCount() - startTime ) < this->waitTime){
		Sleep(10);
		cout << "*";
	}			
	cout << endl;

	// checks if it is the end of the scan
	if(this->dac.current == this->dac.maximum){

		log->log("DAC scan finished");
		active = false;
		
		return;
	}
	
	this->dac.current += this->dac.step;
	if(this->dac.current > this->dac.maximum) this->dac.current = this->dac.maximum;

					   
}

SpdDacScan::~SpdDacScan(void)
{
}
