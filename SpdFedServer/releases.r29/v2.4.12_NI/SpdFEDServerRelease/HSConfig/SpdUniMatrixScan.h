#pragma once
#include "spdscan.h"

#include "..\VMEAcc\VMEAccess.h"
#include "stdafx.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"

class SPDConfig;


//!Class to perform a uniformity matrix scan
/*! Class to perform a uniformity matrix scan	
	in this scans basically the test pulses are set in all the detector, row by row
	and its checked if all the pixels receive the same number of hits
*/
class SpdUniMatrixScan : public SpdScan
{
	bool active;
	unsigned triggerNumber;
	unsigned rowStart;
	unsigned rowEnd;
	bool maskNoActive;
	unsigned currentRow;

	//! timer for the scan
	timer timerScan;

		//! logger of the class
	spdLogger *log;

	UInt8 scanType;
	//! parent SPDConfig to handle the hardware
	SPDConfig *parent;

	//! sets a certain number of TP rows all enabled channels
	/*
	\param rowStart the row start 
	\param nRows the number of rows to be enabled after the row start
	*/
	void SetMatrixToAllEnabled(UInt8 rowStart, UInt8 nRows);
		//! will write the router header for this scan
		//! \param routerNumber router number
	void SetScansHeader(UInt8 routerNumber);
public:


	/*! method to start the scan
		\param trigNumber number of triggers to send
		\param rStart row where the scan starts 
		\param rEnd row where the scan finishes 
		\param mskNActive enable or disable the masking of not used rows 	*/
	unsigned start( unsigned trigNumber, 
					unsigned rStart, 
					unsigned rEnd, 
					bool mskNActive);

			//! Method to restart the dac scan
	unsigned restart(void);

		//! Method to stop the dac scan
	void stop(){this->active = false;};
	
		//! Method to lead the dac scan
	void scan();
		//! method to get if the scan is active or not 
	bool isActive(){return this->active;};


	SpdUniMatrixScan(SPDConfig *spdconf);
	~SpdUniMatrixScan(void);
};
