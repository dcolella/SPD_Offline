#pragma once
#pragma once

#include "../addressgenerator/routintaddr.h"
#include "..\VMEAcc\VMEAccess.h"
#include "../spdDbConfLib/spdLogger.h"

// spdHalfstaveEvent for an half stave is a vector of  pixel chips includes a vector of hits 
typedef vector< vector< PixelCoordinate> > spdHalfstaveEvent ;

//keeps data for one halfstave
typedef vector< spdHalfstaveEvent> spdEventHits;

//! Class to parse and raw data of the SPD into hits
/*!This class keeps works like a vector of events each event having 6 half-stave
	each half-stave having 10 pixel chips and each chip having a vector of hits (column, row)*/
class SpdHitData
{
	//! keeps all the data for all events 
	vector< spdEventHits>  events;
	//! during the parsing used to know event is enabled now
	unsigned currentEvent;
	//! during the parsing used to know which chip is enabled now
	unsigned currentChip;
	//! during the parsing used to know which half-stave is enabled now
	unsigned currentHalfstave;

	//! parses one 16 bit pixel raw data cell
	/*\param cell 16 bit integer from the dpm memory*/
	void parcePixelCell(UInt32 cell) ;
	//! search the raw data for the start of nex event (0xfffffff)
	/*!\param data the pointer to the data
		\param pos the position from where it should start searching
		\param size the of the raw data*/
	unsigned int nextEvent( UInt32 *data, unsigned pos, unsigned size);

	//! initializes all variable and inserts a new event in the vector
	void startEvent();


		/*!Logging instance for the data parser*/
	spdLogger *log;


public:

	//! parses an array of raw data from the detector 
	/*!\param data array with the raw data to be parsed
		\param size the size of the input data
	*/	
	void  parseRawData(UInt32 *data, unsigned size);
	
	//! deletes all events 
	inline void clear();

	//!returns the data of one event
	/*!\param eventNumber number of the even*/
	spdEventHits &getEvent(unsigned eventNumber);

	//!returns the data of one event doing bound checking
	/*!\param eventNumber number of the event to retrieve
	\return reference to the event in question, if out of bound returns an empty event*/
	inline spdEventHits & operator [](unsigned eventNumber){return getEvent(eventNumber);};

	//!Returns the number of events in the data structure
	inline size_t size(){return events.size();};
	
	//! gets the total number of hits in one event
	/*! \param eventNumber event to check
		\return total number of hits */
	unsigned getNumberOfHitsInEvent(unsigned eventNumber);


	SpdHitData(void);
	~SpdHitData(void);
};

