#pragma once
#include "spdScan.h"
#include "SpdHitData.h"

//#include "..\VMEAcc\VMEAccess.h"
#include "stdafx.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"


class SPDConfig;

const unsigned nChips = 10;
const unsigned nHStaves = 60;
const unsigned mebSize = 4;

class spdMEBufferScan : public SpdScan
{
		//! defines if the scan is active or not
	bool active;
		//! defines if we use l1 or l2n while correcting the MEB in a halfstave
	bool useL1;
		//! defines if we correct the misalignment or not after checking it
	bool correctMisalign;
		//! parent SPDConfig to handle the hardware
	SPDConfig *parent;
		//! timer to measure the scan duration
	timer timerScan;

		//! logger of the class
	spdLogger *log;

	UInt32 triggerNum;

		//! stores the control registers of the routers
	UInt32 controlRegisters[10];

		//! stores the status of the MEB check for all the chips
		/*	2D array [halfStave][chip]
			-1 not checked
			0 MEB aligned
			1 MEB misalignment of 1 position
			2 MEB misalignment of 2 positions
			3 MEB misalignment of 3 positions
		*/
	int checkResults[nHStaves][nChips];
	
		//! function to set all test pulse matrices in all enabled channels 
		/*!	\param currentTestPulse- the number of the current test pulse to set [0-1]
			\return the error state of the operation (0=ok, not 0 means one problem with one of the channels)
		*/
	UInt32 setTpMatrixAllEnabled( int currentTestPulse);

		//! method to check the MEB in all halfstaves
	void checkAllMEB(void);

		//! method to check the MEB in one halfstaves
		/*! \param readEvent- reference to one event
			\param hs- half-stave number (global number in this FED)
		*/
	void checkMebForHs(SpdHitData & readEvent, unsigned hs);

		//! method to correct the MEB in all halfstaves 
		/*	uses the member useL1 to define how to correct the MEB
		*/
	void correctAllMEB(void);

		//! gets the value of the  misalignment for one half-stave
		/*!	this function returns the value of the misalignment of the first misaligned chip
			\param hs half-stave number (global number in this FED)
			\return value of the misalignment of the first misaligned chip*/
	int getMisalignmentForHs(unsigned hs);

	
public:
		//! method to start the scan
		/*! \param l1 defines if we use l1 triggers or l2n to correct the misalignment of the multi event buffer
			\	param correct defines if we correct the misalignment after checking it
		*/
	unsigned start(bool correct, bool l1);

		//! method to restart the MEB scan
	unsigned restart(void);

		//! method to stop the scan
	void stop(){this->active = false;};

		//! method to lead the scan
		/*!	left empty to avoid multi threading problems
			it can be called inside this class but also from the poolingFunction
		*/
	void scan();
	
		//! method to execute the scan
		/*!	it contains all the code needed for the scan and is called only inside this class
		*/
	void performScan();

		//! method to get if the scan is active or not 
	bool isActive(){return this->active;};

		//! returns the misalignment of the corresponding chip since the last scan
		/*!	\param hs halstave number in the fed (0-59)
			\param chip chip number in the halstave (0-9)*/
	int getChipMisalignment( unsigned hs, unsigned chip);

		//! constructor
	spdMEBufferScan(SPDConfig * spdConf);
	~spdMEBufferScan(void);
};
