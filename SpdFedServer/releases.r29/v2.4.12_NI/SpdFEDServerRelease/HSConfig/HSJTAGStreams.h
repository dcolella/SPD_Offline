#pragma once

#ifndef hsconfiguration_h
#define hsconfiguration_h


#include ".\hsconfiguration.h"

const UInt32 DPI_CONF_RW = 0x09;
const UInt32 DPI_CONF_R  = 0x0a;
const UInt32 DPI_INT_RW = 0x0b;
const UInt32 API_CONF_RW = 0x0b;
const UInt32 API_CONF_R_DAC = 0x0c;
const UInt32 API_CONF_R_ADC = 0x0a;
const UInt32 API_START_CONV = 0x09;
const UInt32 GOL_CONF_RW = 0x09;
const UInt32 GOL_CONF_R  = 0x0a;

const UInt32 IDCODE = 0x1;
const UInt32 BYPASS = 0x1f;
const UInt32 BYPASSPix = 0xf;
const UInt32 SelENBL = 0x1;
const UInt32 SelGLOBAL = 0x4;
const UInt32 SelTM = 0x3;
const UInt32 RstPixel = 0xd;	// value of capture IR


#define DAC_SET 0
#define DAC_SET_SENSE 1
#define SET_TP 2
#define SET_MASK 3

#define IRSelEnable 0
#define IRSelGlobal 1
#define	IRSelTM 2
#define	IRIdCode 3
#define	IRbypass 4
#define	IRfree 5
#define	IRRead 6
#define IRStartConv 7
#define JTAG_RESET 8

const int NInstructions = 9;

//these codes are used by the decoder to address the right element
#define	GOL 0
#define API_DAC 1
#define API_ADC 2
#define	PIX_DAC 3
#define	PIX_COLUMN 4
#define	DPIConf 5
#define	DPIInternal 6
#define	ID_CODE 7

//! Class to manage the jtag vectors, both IR and DR
 /*!This class keeps in memory all IR vectors for all operations in the detector,
	generates the DR vectors acording to the elements included in the chain, checking also the IR instruction.
	If a device is put in bypass it puts automatically the 1 bit cell
	IRIstrucVect[i][j]: [i] select the environment and the operation to do:
      0 is used for default load HS default. first step where you select the global register.
      1 is used for write in the proper selected register. second step for load register. 
      2 for mask or set TP
      3 ID code
      4 Bypass
	  5 user define vector
	  6 read vector
	  7 start conv
	  8 jtag reset*/
class HSJTAGStreams{
	
	UInt32 IRIstrucVect[NInstructions][13];


	UInt32 bitNumber;
	UInt32 ModeSel;
	UInt32 * ScanVector;

	 /*! Jtag operation
		0 = DAC operation
		1 = DAC with sense on
		2 = set TP
		3 = setMask*/
	UInt8 JTOperation; 
	//! skip mode in the mc
	UInt8 SkipMode;

	//! temporary pointer/keeper of data 
    UInt32 GOLConfTemp;
	//! temporary pointer/keeper of data CAUTION: dangerous
	UInt8  * APIConfTemp;
	//! temporary pointer/keeper of data CAUTION: dangerous
	UInt32 * DPIConfTemp;

	//! method to get the IR instruction of a certain position in the JTAG chain
	/*! \param position position in the jtag chain : 0-gol, 1-api 2..11-pixel chip, 12-DPI
		\return IR instruction for the device in this jtag scan*/
	UInt32 GetInstruction(UInt8 position);
	//! method to append a certain ammount of bits from one array to another
	 /*!it uses bitNumber internal variable to know where to start to append the data
		\param Stream data output, where de data will be appended 
		\param bitNum number of bits to append 
		\param Data input data, data to be appended to the first array
		\return returns always zero for the moment*/
	UInt32 AppendData(UInt32 * Stream, UInt32 bitNum, UInt32 * Data);

	//! method to extract data from an array
	/*!It know the ammount of bits to extract by the Type of IR instruction supplied 
		\param Stream input data
		\param StartPosition  position to start taking data 
		\param Type type of jtag instruction, this tells the ammount of bits to extract from the data
		\return returns zero if everything ok*/
	UInt32 * ExtractData(UInt32 * Stream, UInt32 StartPosition, UInt32 Type);
    
public:
	HSJTAGStreams(void);
	~HSJTAGStreams(void);
    

    
	//! returns mode sel (I need to ask Iavan what is this ModeSel)
	UInt32 GetModeSel(void){return ModeSel;};
	//! method to set the ModeSel
	void SetModeSel(UInt32 Mode){ModeSel = Mode;};
	//! Returns the current jtag operation (IR or DR)
	UInt8 GetJTOperation(void){return JTOperation;};
	//! Sets the current JTAG operation (IR or DR)
	void SetJTOperation(UInt8 OperTemp){JTOperation = OperTemp;};

	//! gets the current skip mode in the mcm for this scan 
	UInt8 GetSkipMode(void){return SkipMode;};
	//! set the current skip mode of this mcm for this scan
	void SetSkipMode(UInt8 Mode){SkipMode = Mode;};

	//! sets one instruction for an IR scan
	/*\param SelVector jtag device for the instruction 0-gol, 1-api 2..11-pixel chip, 12-DPI*/
	void SetScanVector(UInt8 SelVector){ScanVector = IRIstrucVect[SelVector];};
    //! generates the IR vector to send to router jtag player
	UInt32 * IRGenerator(void);
	//! generates the DR vector to send to the router player
	UInt32 * DRGenerator(UInt32 *DataIn, bool ConstValue);


	//! decodes a DR vector comming from the routers's jtag player
	/*! \param DRVect array with the DR data
		\return returns a double dimmension array with the decoded data this depends of the type of scan made*/
	UInt32** DecodeJTStream(UInt32 * DRVect);

		//! function to generate the jtag vectors to set one pixel dac
		/*!
			@return 2 dimension array with jtag ir, dr,ir,dr vectors to set the dacs
			@param DACn - dac number to chane
			@param DACValues - array with the different values for every chip in chain
			@param ChipSelect- 10 bit number saying which chips we want to change in this scan 
				(00000001) means we will put all chips except number 0 in bypass
			@param All - specifies if we are setting all chips with the same value (DACValues[0])
			@param pixInChain - parameter defining the chips in chain (if any chip is being skiped in the chain)
		*/
	UInt32** SetJTSPixelDAC(  UInt8 * DACn, UInt8 * DACValues, UInt32 ChipSelect, bool All, UInt32 pixInChain);

		//!Function get the jtag vectors to set all pixel 44*10 dacs in one halfstave
		/*! 
			@return 2 dimension array with 90 arrays 0,1 ir vectors for IRSelEnable and IRSelGlobal
			@param ChipSelect- 10 bit number saying which chips we want to change in this scan 
				(00000001) means we will put all chips except number 0 in bypass
			@param DACVect - array with the different values  44*10 dac values
			@param pixInChain - parameter defining the chips in chain (if any chip is being skiped in the chain)

		*/
	UInt32** SetJTSPixelAllDAC(UInt32 ChipSelect, UInt8 * DACVect,UInt32 pixInChain=1023);

		//!Function to get the jtag vectors to set a pixel matrix i nthe pixel chips, test pulse or mask.
	UInt32** SetJTSPixelMatrix( UInt32 ChipSelect, UInt32 * Matrix, bool All, bool TP, UInt32 pixInChain=1023);
    	//! function to get the jtag vectors to set the analog pilot dacs
	UInt32** SetJTSApiDAC( UInt8 * ApiDac, UInt32 pixInChain=1023);
		//! function to get the jtag vectors to send the start conversion of the API ADC 
	UInt32** SetJTSApiADCStartConv( UInt32 pixInChain=1023);
		//! method to get the vectors for digital pilot registers 
	UInt32** SetJTSDpiConfReg(UInt32 * DpiReg, UInt32 pixInChain=1023);
		//! method to get the vectors for setting the internal dpi register
	UInt32** SetJTSDpiInternalReg( UInt32 * DpiReg, UInt32 pixInChain=1023);
		//! method to get the vectors to read the analog pilot dacs
	UInt32** GetJTSApiDAC( UInt32 pixInChain=1023);
		//! method to get the vectors to read the analog pilot ADCs
	UInt32** GetJTSApiADC( UInt32 pixInChain=1023);
		//! method to get the vectors to read the digital pilot registers
	UInt32** GetJTSDpiConfReg( UInt32 pixInChain=1023);
		//! method to get the vec tors to read the DPI internal registers
    UInt32** GetJTSDpiInternalReg( UInt32 pixInChain=1023);
		//! method to get the vector to send IR reset to the pixels 
	UInt32* GetJTSResetPixel( UInt32 ChipSelect, UInt32 pixInChain=1023);


};

#endif