#pragma once

#ifndef APIConvParam_h
#define APIConvParam_h

#include "..\BitManager\BitsManage.h"
 //!Class to manage the Analog pilot configuration from the Bari configuration
/*!it keeps internal member of all the vectors from the DAC characterization from the studies from Bari*/
class APIConvParam : public BitsManage{
	float IPt;
	float VddMCM;
	float GNDDiff;
	float TempTest;
	float DACRefHi[256];
	float DACRefMid[256];
	float DACGtlRef[256];
	float DACTestHi[256];
	float DACTestLow[256];

	float DeltaADCRefHi;
    float DeltaADCRefMid;
	float DeltaADCGtlRef;
	float DeltaADCTestHi;
	float DeltaADCTestLow;

	float ADCSenseV[1024];
	float ADCSenseI2[1024];
	float ADCPixelVdd[400];

    
public:
	APIConvParam(void);
	~APIConvParam(void);

	float GetIPt1000(void){return IPt;};
	float GetVddMCM(void){return VddMCM;};
	float GatGNDDiff(void){return GNDDiff;};
	float GetTestTemperature(void){return TempTest;};
	float GetDACRefHi(UInt8 DACValue){return DACRefHi[DACValue];};
	float GetDACRefMid(UInt8 DACValue){return DACRefMid[DACValue];};
	float GetDACGtlRef(UInt8 DACValue){return DACGtlRef[DACValue];};
	float GetDACTestHi(UInt8 DACValue){return DACTestHi[DACValue];};
	float GetDACTestLow(UInt8 DACValue){return DACTestLow[DACValue];};

	float GetADCRefHi(UInt32 ADCValue){return ADCSenseV[ADCValue] + DeltaADCRefHi;};
    float GetADCRefMid(UInt32 ADCValue){return ADCSenseV[ADCValue] + DeltaADCRefMid;};
	float GetADCGtlRef(UInt32 ADCValue){return ADCSenseV[ADCValue] + DeltaADCGtlRef;};
	float GetADCTestHi(UInt32 ADCValue){return ADCSenseV[ADCValue] + DeltaADCTestHi;};
	float GetADCTestLow(UInt32 ADCValue){return ADCSenseV[ADCValue] + DeltaADCTestLow;};
	float GetADCSenseV(UInt32 ADCValue){return ADCSenseV[ADCValue];};
	float GetADCSenseI2(UInt32 ADCValue){return ADCSenseI2[ADCValue];};
	float GetADCPixelVdd(UInt32 ADCValue){return ADCPixelVdd[ADCValue];};
    


	float *GetDACRefHiPointer(void){return DACRefHi;};
	float *GetDACRefMidPointer(void){return DACRefMid;};
	float *GetDACGtlRefPointer(void){return DACGtlRef;};
	float *GetDACTestHiPointer(void){return DACTestHi;};
	float *GetDACTestLowPointer(void){return DACTestLow;};

	float GetDeltaADCRefHi(void){return DeltaADCRefHi;};
    float GetDeltaADCRefMid(void){return DeltaADCRefMid;};
	float GetDeltaADCGtlRef(void){return DeltaADCGtlRef;};
	float GetDeltaADCTestHi(void){return DeltaADCTestHi;};
	float GetDeltaADCTestLow(void){return DeltaADCTestLow;};

	float *GetADCSenseVPointer(void){return ADCSenseV;};
	float *GetADCSenseI2Pointer(void){return ADCSenseI2;};
	float *GetADCPixelVddPointer(void){return ADCPixelVdd;};

    void LoadConversionParam(UInt8 * ConverStream);
	void CreateConversionParamStream(UInt8 * ConverStream);
};


/*
in the stream to class
4      bytes        IPt
4                     VddMCM                
4                     GNDdiff
4                     TemperatureTest
5120               DAC conv
5                     Delta ref Hi   
5                     Delta ref Mid
5                     Delta gtl  Ref
5                     Delta test Hi
5                     Delta test Low
4096               Sense V
4096               Sense I2
1600               Pixel Vdd/2


*/
#endif