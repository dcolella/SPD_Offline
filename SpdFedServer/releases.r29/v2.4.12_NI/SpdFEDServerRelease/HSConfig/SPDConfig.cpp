#include "StdAfx.h"
#include ".\SPDConfig.h"



SPDConfig::SPDConfig(char inSide){

   
	// starts a logging instance
	this->log = &spdLogger::getInstance();

	VMEAccess * vme = & VMEAccess::getInstance();

	switch(inSide){
		case 'a':
		case 'A':
			side = 'A';
			break;
		case 'c':
		case 'C':
			side='C';
			break;
		default:
			
			log->log("ERROR: ERROR side should be A or C found %c", inSide);
			throw std::out_of_range("side as to be 'A' or 'C'");
	}
	

	/*will initialize all dacs scans*/

	
	this->scansVector.resize( NUMBER_OF_SCANS);
	this->scansVector[FO_CALIBRATION_POS]	= dynamic_cast<SpdScan *> (new spdFastorCalibration( this));
	this->scansVector[MEAN_TH_SCAN_POS]		= dynamic_cast<SpdScan *> (new spdMeanTHScan( this));
	this->scansVector[DAC_SCAN_POS]			= dynamic_cast<SpdScan *> (new SpdDacScan(this));
	this->scansVector[DELAY_SCAN_POS]		= dynamic_cast<SpdScan *> (new SpdDelayScan(this));
	this->scansVector[MINTH_RESHSCAN_POS]	= dynamic_cast<SpdScan *> (new SpdMinThresScan(this));
	this->scansVector[UNI_MATRIX_SCAN_POS]	= dynamic_cast<SpdScan *> (new SpdUniMatrixScan(this));
	this->scansVector[MEB_BUFF_SCAN_POS]	= dynamic_cast<SpdScan *> (new spdMEBufferScan(this));



		// if we are in side C then the channel numbers start in 60
		// and router number should start in 10
	unsigned hsOffset = 0;
	unsigned routerOffset = 0;
	if (side == 'C'){ 
		hsOffset =60; 
		routerOffset = 10;
	}


		// will initialize 60 halfstaves 
	for (int hs = 0 ; hs < NHalfStaves ; hs ++){
		this->halfStaves[hs] = new SpdHalfStave( hsOffset + hs, vme);
	}

	
	// will initialize 10 routers
	for (int router = 0 ; router < NRouters ; router++ ){
		this->routers[router] = new SpdRouter( router + routerOffset, vme);
	}

	this->HSData.SetHSFilePointer( halfStaves, side );

	
}



SPDConfig::~SPDConfig(void){
			// will delete all halfstaves
	for (int hs = 0 ; hs < NHalfStaves ; hs ++){
		delete this->halfStaves[hs];
	}

	// will delete all routers
	for (unsigned router = 0 ; router < NRouters ; router ++ ){
		delete this->routers[router];
	}

	//will delete all calibration scans

	for (unsigned scan = 0 ; scan < this->scansVector.size() ; scan ++){
		delete this->scansVector[scan];
	}

}





SpdHalfStave &SPDConfig::getHS( unsigned chNumber){

	switch (this->side){
		case 'A':
		case 'a':	
			if (chNumber >= 60){
				log->log("ERROR: channel %d out of range operation will be done in HS %d", chNumber, chNumber%60);
			}
			
			// this kind of trick is done because there is no eleant way of getting out of this error
			//this is the kind of situation where returning an empty object would be perfect (some fake hs with no VME acess)
			return *halfStaves[chNumber%60];
		case 'C':
		case 'c':
			if (chNumber < 60 || chNumber>= 120){
				log->log("ERROR: channel %d out of range operation will be done in HS %d", chNumber, chNumber%60 + NHalfStaves);
			}
			return *halfStaves[chNumber -NHalfStaves];

		default:
			log->log("ERROR: Side %c not valid, possible corruption, FED server exiting", this->side);
			throw(" side is not valid corruption in the system");
			
	}
	
}


bool SPDConfig::isScanActive(){



	for (unsigned scan = 0 ; scan < this->scansVector.size() ; scan ++){
		if  (this->scansVector[scan]->isActive() ) return true;
	}

	return false;
}

SpdRouter & SPDConfig::getRouter( unsigned routerNumber){

	
	if (this->side == 'A'){
		
		if(routerNumber >= NRouters){
			log->log("ERROR: router %d out of range operation will be done in router %d", routerNumber, routerNumber%NRouters);

			// this kind of trick is done because there is no eleant way of getting out of this error
			//this is the kind of situation where returning an empty object would be perfect (some fake router with no VME acess)
			routerNumber= routerNumber%NRouters;
		}
		return *routers[routerNumber];
	}
	else{
		if(routerNumber >= 2*NRouters || routerNumber < NRouters){
			log->log("ERROR: router %d out of range operation will be done in router %d", routerNumber -NRouters, (routerNumber -NRouters)%NRouters);

		}
		return *routers[(routerNumber -NRouters)%NRouters];
	}

}

// unmasks all halfstaves
unsigned SPDConfig::UnMaskAllPixels(){

	ViStatus status=0;
	


	bool mask = true;
	bool repeatForAll = true;
	// will enable the masking (and not the test pulse)

	UInt32 matrix[256];
	for(int i = 0; i < 256; ++i) matrix[i] = 0;

		// sets the matrix repeating the settings for all 10 pixel chips;
	for (int hs = 0 ; hs < NHalfStaves ; hs ++){
		if (halfStaves[hs]->getChActStatus()){
			status += halfStaves[hs]->LoadHSPixelMatrix( 1023,matrix, repeatForAll, mask);
		}
	}
	
	return status; 
}

// will reset the digital pilot internal registers in all all halfstaves
unsigned SPDConfig::resetInternalDPI_All(){
	ViStatus status=0;
	UInt32 dpiRegisters[3]= {0,0,0};

		// sets the digital pilot internal registers with the default values
	for (int hs = 0 ; hs < NHalfStaves ; hs ++){
		if (halfStaves[hs]->getChActStatus()){

			status = halfStaves[hs]->LoadHSDpiInternalReg( dpiRegisters);
			if(status != 0)	return 1;
		}
	}

	return 0;
}


bool SPDConfig::isRouterActive(unsigned router){

	if( router >= NRouters) return false;

	for (unsigned hs= HSInRouter*router; hs < HSInRouter*(router+1) ; hs++){
		if (halfStaves[hs]->getChActStatus()==2) {
			return true;
		}
	}

	return false;
}

// will make the data reset in all halfstaves
unsigned SPDConfig::DataReset_All(void){
	ViStatus status = 0;

	// loops through all routers, if the router has one enabled halfstave 
	// will send a data reset command 

	for(unsigned router = 0; router < NRouters; router++){
		//if( isRouterActive(router)){
		status += routers[router]->dataResetHS();
		if (status !=0){
			log->log("ERROR: error writting data reset to router %d", router);
		}
	
			// sleeps one milli second  beween data resets
		Sleep(1);


	}
	return status;

}



unsigned SPDConfig::refreshAllTemperatures(){

	
	for(int hs=0; hs< NHalfStaves; hs++){
		this->halfStaves[hs]->refreshTemperatures();
	}

	return 0;

}


void SPDConfig::setMCMStimuliToAll( UInt32 Enable, UInt32 ColumnToSet){   //Enable = 1 enable, = 0 disable
	unsigned chMin, chMax;

	if(side == 'A' ){
		chMin = 0;
		chMax = 60;
		
	}
	else if( side == 'C'){
		chMin = 60;
		chMax = 120;
	}
	
	for(UInt8 channel = chMin; channel < chMax; channel++){

		SpdRouter &router =this->getRouter( channel/6);
		// will set the mcm stimuly only if the channel is enabled 
		if (this->getHS(channel).getChActStatus() == 2 || this->getHS(channel).getChActStatus() == 1){
			router.setMcmStimuly(channel%6,Enable,ColumnToSet);
		}

	}


}


void SPDConfig::setMCMStimuli(UInt8 channel, UInt32 Enable, UInt32 ColumnToSet){   //Enable = 1 enable, = 0 disable
	
	SpdRouter &router =this->getRouter( channel/6);
	// will set the mcm stimuly only if the channel is enabled 
	if (this->getHS(channel).getChActStatus() == 2 || this->getHS(channel).getChActStatus() == 1){
		router.setMcmStimuly(channel%6,Enable,ColumnToSet);
	}
}


/*
header to send to DAQ during scans
	Header lenght
	0 Router Number
	1 Type (bits 0..7 operation mode, bit 8 : 0 = Normal data, 1 = Histogram format)
	2 Triggers
	3 Chip Present 1 (HS 2, 1, 0)
	4 Chip Present 2 (HS 5, 4, 3)
	5 DACStart, DACEnd, Step, DAC Identifier (8 bit registers)
	6 Row start, Row end, Row inex, DAC value/ row index (8,8,8, 8 bits)
	7 DACHigh0, DACLow0, DACHigh1, DACLow1 (HS 0,1)  
	8 DACHigh2, DACLow2, DACHigh3, DACLow3 (HS 2,3)
	9 DACHigh4, DACLow4, DACHigh5, DACLow5 (HS 4,5)
	10 TPAmplitude0 (*100)
	11 TPAmplitude1 (*100)
	12 TPAmplitude2 (*100)
	13 TPAmplitude3 (*100)
	14 TPAmplitude4 (*100)
	15 TPAmplitude5 (*100)

	Note: DelayScan point 7 DCAHight0 become misc_control
		MinThScan point 7 DCAHight0 become chip to scan

Operation Mode:

	* 0 Minimum Th
	* 1 MeanTH
	* 2 DAC scan
	* 3 Uniformity Scan
	* 4 Noise Scan
	* 5 Delay Scan
	
DAC Identifier:

	* 0 .. 44 pixel chip DACs
	* 100 .. 105 API DACs

*/
//void SPDConfig::SetScansHeader(UInt8 DataType, UInt8 ScanType, unsigned RoutN, UInt8 DACIndetifier,UInt8 ActualRow,UInt8 StartRow ,UInt8 EndRow){
//
//  
//	const UInt8 HeaderLength = 16; 
//	UInt8 ChN = RoutN * 6;
//
//	UInt32 Header[HeaderLength];
//	
//	Header[0] = this->routers[RoutN]->getRouterNumber();
//    Header[1] = ScanType + (DataType << 8);
//	Header[2] = TriggerN;
//	
//	UInt32 AuxVect[6], AuxDACHi[6], AuxDACLow[6];
//	for(int i=0; i < 6; ++i){
//		AuxVect[i] = 0;
//		AuxDACHi[i] = 0;
//        AuxDACLow[i] = 0;
//	}
//
//
//	UInt32 HSPresents = 0x3f;          // this information should be retrieved by the router and it is used 
//                                       // for understand witch HS should be active
//	for(UInt8 i=0; i < 6; ++i){
//	  if((HSPresents >> i) & 1){
//		  AuxVect[i] = halfStaves[ChN+i]->GetDPI()->GetDPIConfMaskChip();
//	  } else{
//		  AuxVect[i] = 0;
//	  }
//	  if(ScanType == 5){
//		AuxDACHi[i] = halfStaves[ChN + i]->GetPxDAC(0, 43);
//	  }else{
//       	AuxDACHi[i] = halfStaves[ChN + i]->GetDACTestHiAPIConf();
//	  }
//	  
//	  if(ScanType == 0){ 
//        AuxDACLow[i] = ChipSelect[ChN + i];
//	  } else {
//		AuxDACLow[i] = halfStaves[ChN + i]->GetDACTestLowAPIConf();
//	  }
//	  
//	  Header[10 + i] =(UInt32) (halfStaves[ChN + i]->GetDACTestHi((UInt8) AuxDACHi[i]) 
//						- halfStaves[ChN + i]->GetDACTestLow( (Uint8) AuxDACHi[i]) )* 100;
//	}
//    
//	Header[3] =  AuxVect[0] + (AuxVect[1]<< 10) + (AuxVect[2] << 20); 
//	Header[4] =  AuxVect[3] + (AuxVect[4]<< 10) + (AuxVect[5]  << 20); 
//
//    Header[5] = ((UInt32)DACMin << 24) + 	((UInt32)DACMax<< 16) + ((UInt32)Step << 8) + DACIndetifier;
//	Header[6] = ((UInt32)StartRow << 24) + ((UInt32)EndRow << 16) 
//						+ ((UInt32)ActualRow << 8) + (UInt32)ActualDACVal;
//    
//	
//	for(UInt8 i = 0; i< 3; ++i){
//		Header[7 + i] =  (AuxDACHi[i * 2] << 24) + (AuxDACLow[i * 2] << 16) 
//						+ (AuxDACHi[i*2+1] << 8) + AuxDACLow[i*2+1];
//	}
//	
//
//
//	this->routers[RoutN]->WriteHeader( Header, HeaderLength);
//	
//}


void SPDConfig::SetScansHeader(UInt8 routerNumber, UInt8 scanType,UInt32 triggern ){

	const int sizeOfHeader = 17;
	UInt32 header[sizeOfHeader];
	unsigned chipsEnabled[6];

	for (unsigned i = 0 ; i < sizeOfHeader ; ++i)header[i] =0;

		// Creates the array with the chips activated for the calibration 
	for (unsigned int HSinRouter = 0; HSinRouter < 6; HSinRouter ++) {
		unsigned int HSnumber = routerNumber*6 + HSinRouter;

		if (this->halfStaves[HSnumber]->getChActStatus() == 2){
			chipsEnabled[HSinRouter] = this->halfStaves[HSnumber]->GetDPI()->GetDPIConfMaskChip();
		}
		else chipsEnabled[HSinRouter] = 0;
	}	


	header[0] = routerNumber;
	header[1] = scanType;
	header[2] = triggern;
	header[3] = chipsEnabled[0] + (chipsEnabled[1] << 10) + (chipsEnabled[2] << 20);
	header[4] = chipsEnabled[3] + (chipsEnabled[4] << 10) + (chipsEnabled[5] << 20);
	
	this->routers[routerNumber]->WriteHeader( header, sizeOfHeader);
	
}

unsigned SPDConfig::StartNoiseScan (UInt8 ChNumber, UInt32 triggern, bool InternTrig){
	log->log("Noise Scan Started"); 
	



	for(unsigned router = 0; router < NRouters; router++){
		if( isRouterActive(router)){
			SetScansHeader(router, 4 ,triggern );
		}
	}

	if (InternTrig){
		this->sendTriggerToEnabledRouters(triggern);
	}

	log->log("Trigger request performed. Please reset the router(s) when the DAQ finished");
	return 0;
}



void SPDConfig::scans(){
	
	/*this->minThreshScan->scan();
	this->dacScan->scan();
	this->meanThScan->scan();
	this->delayScan->scan();
	this->uniMatrixScan->scan();
	this->foCalibration->scan();
*/
	for (unsigned scanIndex = 0 ; scanIndex < this->scansVector.size() ; scanIndex ++){
		this->scansVector[scanIndex]->scan() ;
	}

}



// will write to the temporary table the differences
int SPDConfig::writeConfigDiffTable(){

	spdDbConnection * conn = spdDbConnection::subscribe();

	int status;
	try {

		conn->connect();
		for(int hs= 0 ; hs < NHalfStaves ; ++hs){			

			if (halfStaves[hs]->getChActStatus() > 0){
				halfStaves[hs]->writeDiffToDB( 1, this->HSData.getGlobalDBVersionNumber());
			}
		}

		// commits the change
		conn->commit();
		conn->disconnect();
		status = 0;
	}
	catch (exception &error){
		log->log("ERROR: errors writing the diff table: %s",error.what());
		status = 1;
	}
	catch (std::string &error){
		log->log("ERROR: errors writing the diff table: %s",error.c_str());
		status = 1;
	}
	catch(...){
		log->log("ERROR: errors writing the diff table");
		status = 1;
	}

	return status;
}



// will count all differences between default and actual containers at the moment
// useful before saving data to the database
void SPDConfig::countConfigDiff(unsigned &mcmDiff, unsigned &dacDiff, unsigned &noisyDiff){


		// cleans the values to start cleanly
	mcmDiff=0;
	dacDiff=0;
	noisyDiff=0;

	// loops through all enabled ahlfstaves each one of them will increment the variable
	for(int hs= 0 ; hs < NHalfStaves ; ++hs){			

		//cout<<dec << " enabled status hs " << hs << " : " << (int) halfStaves[hs]->getChActStatus()<< endl; 
		if (halfStaves[hs]->getChActStatus() > 0){
			halfStaves[hs]->getDiffCount( mcmDiff, dacDiff, noisyDiff );
		}
	}
}


int SPDConfig::sendTriggerToEnabledRouters( UInt32 TriggerN, bool Delay, bool l2y, bool l1, bool tp, bool l2n){

	int error;

	//log->log("sending trigegr to all enabled routers");
	// first sends triggers to all routers
	for(unsigned index = 0; index < NRouters; index++){

		if( isRouterActive(index)== true){
			//log->log("sending trigger to router %d", index);
			error = routers[index]->SendTrigger(TriggerN, Delay, l2y, l1, tp, l2n);
			if (error) {
				log->log("ERROR: Error sending trigger router %d", index);
				return error;

			}
		}
		else{
			//log->log("skipping router %d", index);
		}


	}

	// then waits till all routers are not iddle anymore
	for(unsigned index = 0; index < NRouters; index++){
		if( isRouterActive(index)){
			//log->log("checking idle for router %d", index);
			// checks the idle flag of this router and waits till it finishes sending all triggers
			DWORD timeStart = GetTickCount();
			DWORD timeElapsed;
			while(routers[index]->getIdleFlag()==false){
				timeElapsed = GetTickCount()-timeStart;
				// if the time is bigger than 10 s then we issue a timeout
				if  ( timeElapsed > 10000){
					log->log("ERROR: ERROR timeout checking router %d ", index);
					break;
					
				}
                
			}
		}

	}
	

	return 0;
}



// configures all enabled halfstaves with te default values
void SPDConfig::configureSPDDefault( UInt32 * report, bool routerReset, bool digitalPilot, bool analogPilot, bool pixdac){

	
	int routerStart = 0;
	
	if (this->side == 'A'){
		routerStart = 0;
	}
	else if (this->side == 'C'){
		routerStart = 10;
	}
	else{
		log->log("(SPDConfig::configureSPDDefault) side has to be 'A' or 'C' found %c", this->side);
		return;
	}
	int routerEnd = routerStart + NRouters;

	int hsNumber = 0;
	int index = 0;

	for (int router = routerStart ; router < routerEnd ;  router ++){

		if (isRouterActive( router)==true){

			log->log("WARNING: reseting all channels in router %d", router);
			// resets the detector
			this->getRouter(router).resetDetector();
			Sleep(30);
			this->getRouter(router).resetJtagcontrollers();
		}
		
		// configures all halfstaves of this router
		for(int hs = 0; hs < HSInRouter ; hs ++){
			hsNumber= HSInRouter*router +hs;
			
			index = hsNumber;
			// if the side is C subtracts 60 from the index
			if (this->side == 'C'){	index-=60;}

			Sleep(1);
			// resets the jtag controller for this halfstave
			this->getHS(hsNumber).resetJtagController();
			report[index] = this->getHS(hsNumber).configureDefault(routerReset,
																	digitalPilot,
																	analogPilot,
																	pixdac);
			
			if (report[index] == 0){
				log->log("INFO: configured hs %d with default values", hsNumber);
			}
			else if (report[index] == 16){
				//log->logToDim("ERROR: hs %d is not ennabled", hsNumber);
			}
			else{
				log->log("ERROR: failed configuration in hs %d ", hsNumber);
			}
			
		}
		
	}


}

// Function to set the fastor strobe length for all enabled halfstaves
UInt32 SPDConfig::setFoStrobeLength_All(unsigned int strobeLength){
	ViStatus status = 0;

	// loops through all the routers and sends the command if one has 1 enabled HS
	for (unsigned router = 0; router < NRouters; router ++){
		//if (this->isRouterActive(router) == true){
			status += this->routers[router]->setFoStrobeLength(strobeLength);
		//}
		if (status != 0){
			log->log("ERROR: error writing FO strobe length in router %d", router);
		}
	}

	return status;
}

// Function to set the timeout for ready events for all enabled halfstaves
UInt32 SPDConfig::setTimeoutReadyEvent_All(unsigned int timeout){
	ViStatus status = 0;

	// loops through all the routers and sends the command if one has 1 enabled HS
	for (unsigned router = 0; router < NRouters; router ++){
		status += this->routers[router]->setTimeoutReadyEvent(timeout);
		if (status != 0){
			log->log("ERROR: error writing timeout ready events in router %d", router);
		}
	}

	return status;
}


//bool SPDConfig::isRouterActive(int router){
//
//	for(int hs = 0; hs < HSInRouter ; hs ++){
//
//		if ( this->getHS( 6*router + hs).getChActStatus() == 1 ||
//			 this->getHS( 6*router + hs).getChActStatus() ==2 ){
//				return true;
//		}
//
//	}
//
//	return false;
//}