#pragma once
#include <dis.hxx>

#ifndef SPDConfig_h
#define SPDConfig_h

#include "stdafx.h"
#include "..\VMEAcc\VMEAccess.h"
#include ".\hsconfiguration.h"
#include ".\hsjtagstreams.h"
#include ".\HSConfData.h"


#include "SpdHalfStave.h"
#include "SpdRouter.h"

#include "../spdDbConfLib/timer.h"
#include "spdFastorCalibration.h"

#include "spdDacScan.h"
#include "spdMeanTHScan.h"
#include "SpdDelayScan.h"
#include "SpdUniMatrixScan.h"
#include "SpdMinThresScan.h"
#include "spdScan.h"
#include "spdMEBufferScan.h"


#pragma warning( once : 4006 )
//Operation to JTAG scan required
#define DAC_SET 0
#define DAC_SET_SENSE 1
#define SET_TP 2
#define SET_MASK 3

// spd scans definitions 
#define NUMBER_OF_SCANS			7
#define FO_CALIBRATION_POS		0
#define MEAN_TH_SCAN_POS		1
#define DAC_SCAN_POS			2
#define DELAY_SCAN_POS			3
#define MINTH_RESHSCAN_POS		4
#define UNI_MATRIX_SCAN_POS		5
#define MEB_BUFF_SCAN_POS		6

//const int numberOfDacs = 44;
//const int NHalfStaves = 60;
const unsigned int HSInRouter = 6;
const unsigned int NRouters = 10;

//! Class to perform global operations in the detectors and calibration scans 
/*! this class contains instances of halfstaves routers and classes
to perform calibration scans and global operations in the detector 
 at the moment I've made all these calibration scans methods friends of this class
most probably a huge crime in encapsulation  but, for historical reasons, 
this was the easiest way of doing it */
class SPDConfig{
protected:
	char side;

private:

	spdLogger *log;

				//! this will contain the HalfStave classes, 60 per side
    SpdHalfStave * halfStaves[NHalfStaves];
		//! contains the routers per side 
	SpdRouter * routers[NRouters];

	/*! Contains a vector of the possible scans 
		these are classes that inherit from SpdScan
	*/
	std::vector< SpdScan * > scansVector;

	
  public:	

	SPDConfig(char side);
	~SPDConfig(void);

	/*! Instance to manage database configuration of the halfstaves
		For the moment is a public member for historic reasons 	*/
    HSConfData HSData;
    
		//! returns reference of the fastor calibration scans
	inline spdFastorCalibration &getFoScan(){
		return * static_cast <spdFastorCalibration *> ( scansVector[FO_CALIBRATION_POS]);
	};

	//! returns reference of the mean threshold calibration scans
	spdMeanTHScan &getMeanThScan(){
		return  * static_cast <spdMeanTHScan *> ( scansVector[MEAN_TH_SCAN_POS]);
	};

	//! returns reference of the dac scan
	SpdDacScan &getDacScan(){ 
		return * static_cast <SpdDacScan *> ( scansVector[DAC_SCAN_POS]);
	};

	//! returns reference to the delay scan
	SpdDelayScan &getDelayScan(){ 
		return * static_cast <SpdDelayScan *> ( scansVector[DELAY_SCAN_POS]);
	};

	//! returns reference to the min threshold scan
	SpdMinThresScan &getMinThresScan(){
		return * static_cast <SpdMinThresScan *> ( scansVector[MINTH_RESHSCAN_POS]); 
	};

	//!returns reference to the uniformity matrix scan
	SpdUniMatrixScan &getUniMatrixScan(){
		return * static_cast <SpdUniMatrixScan *> ( scansVector[UNI_MATRIX_SCAN_POS]);
	};

	//!returns reference to the multi event buffer scan
	spdMEBufferScan &getMebBufferScan(){
		return * static_cast <spdMEBufferScan *> ( scansVector[MEB_BUFF_SCAN_POS]);
	};
		//! method that returns if any of the scans is active
	bool isScanActive();



		//! methos to return a reference of one halfstave 
		/*there is bound protections inside, even though not done in the best way*/
	SpdHalfStave &getHS( unsigned chNumber);
		//! methos to return a reference of one router 
		/*there is bound protections inside, even though not done in the best way*/
	SpdRouter &getRouter(unsigned routerNumber);


	//!getter for the side 
	char getSide(){return side;};

	//! starts a noisy scan
	unsigned StartNoiseScan(UInt8 ChNumber, UInt32 triggern, bool InternTrig);

	//! sets the MCM stimuly for one link receiver channel only
	/*! \param ChN chanel number (halfstave) to set
		\param Enable enable or disable flag for the mcm stimuly
		\param ColumnToSet defines the column to set in the mcm stimuly*/
	void setMCMStimuli(UInt8 ChN, UInt32 Enable, UInt32 ColumnToSet = 1); 

	//! sets the MCM stimuly to all enable channels  
	/*! \param Enable enable or disable flag for the mcm stimuly
		\param ColumnToSet defines the column to set in the mcm stimuly*/
	void setMCMStimuliToAll(UInt32 Enable, UInt32 ColumnToSet = 1); 

	
	//void SetScansHeader(UInt8 DataType, UInt8 ScanType, unsigned RoutN, UInt8 DACIdentifier,UInt8 ActualRow ,UInt8 StartRow ,UInt8 EndRow);
    //!Method to write the scan header in the router for the noise scan only
	void SetScansHeader(UInt8 routerNumber, UInt8 scanType,UInt32 triggern );


	//! checks "loops" over the scans calling their scan method
	void scans();
	

	//! unmasks all pixel chips
	unsigned int UnMaskAllPixels(void);

	//!refreshes the temperatures in all enabled halfstaves
	unsigned int refreshAllTemperatures();
	

	//! Method to set the internal digital pilot with the default values in all enabled halfstaves
	unsigned int resetInternalDPI_All(void);

	//! performs a data reset in all half-staves
	unsigned int DataReset_All(void);

		/*! function to write the differences to the database temporary table
			will loop through all halfstaves and send a command to compare the default and actual values 
			writing them in the database in a temporary table
		*/
	int writeConfigDiffTable();

		/*! function to get the number of differences between the default and the actual data
			will loop through all internal containers comparing the defaul and actual values 
			and counting the number of differente settings in mcm values, dac values and noisy pixel values
			@param mcmDiff: argument by reference it will be incremented inside the funcion to contain the number of mcm differences
			@param dacDiff: argument by reference it will be incremented inside the funcion to contain the number of pixel dac differences
			@param noisyDiff: argument by reference it will be incremented inside the funcion to contain the number of noisy pixels differences
		*/
	void countConfigDiff(unsigned &mcmDiff, unsigned &dacDiff, unsigned &noisyDiff);

	/*! Method to send triggers to all enabled routers 
		you can define the number of triggers to send and the rest of the varaiables enable the type of triggers to send
		\param triggerN number of triggers to send
		\param delay have to ask what this means to michele 
		\param l2y enables the level 2 yes trigger
		\param l1 enables the level 1 trigger
		\param tp  enables the test pulse trigger
	*/
	int sendTriggerToEnabledRouters(UInt32 triggerN, bool delay = true, bool l2y= true, bool l1=true, bool tp=true, bool l2n = false);



	/*@Configures the all halfstaves with the default values 
	@param the inputs define which configurations will be performed
	writes in the report the status of the 60 hs*/
	void configureSPDDefault(UInt32 * report,bool routerReset=true, bool digitalPilot=true, bool analogPilot=true, bool pixdacs=true);

	/*!returns if a router as at least one acitve channel or not
	CAUTION it uses the local router number, so if side is C the router  is still from 0-9*/
	bool isRouterActive(unsigned routerNumber);

// TO DO: create a separate class with the methods to configure the off-detector electronics
	/*! method to set the fastor strobe length for all the enabled halfstaves
		\param strobe length
		\return error condition
	*/
	UInt32 setFoStrobeLength_All(unsigned int strobeLength);

	/*! method to set the timeout between the arrival of a L2 and the message of ready event from linkrx
		\param timeout in us
		\return error condition
	*/
	UInt32 setTimeoutReadyEvent_All(unsigned int timeout);

	
	// at the moment I've made all these calibration scans methods friends of this class
	// A huge crime in encapsulation and I probably deserve to shot at 
	//but for historical reasons this was the easiest way of doing it
	friend class spdFastorCalibration;
	friend class spdMeanTHScan;
	friend class SpdDacScan;
	friend class SpdDelayScan;
	friend class SpdUniMatrixScan;
	friend class SpdMinThresScan;
	friend class SpdScan;
	friend class spdMEBufferScan;
};


#endif