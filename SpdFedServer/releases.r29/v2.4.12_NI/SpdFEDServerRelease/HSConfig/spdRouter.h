#pragma once

#include "../addressgenerator/routintaddr.h"
#include "..\VMEAcc\VMEAccess.h"
#include "../spdDbConfLib/spdLogger.h"
#include <dis.hxx>

#include "SpdHitData.h"



//!	Constant defining the maximum number of errors that will be read by the fed server 	for every router, like this we can avoid locking up the FED server*/
const unsigned maxNumberOfRouterErrors = 500;

#define NHalfstavesInRouter 6

#define	ROUTER_SEB_MODE	0x20070526
#define	ROUTER_MEB_MODE	0x20090106

//! SPD Router class 
/*! THis is an higher level class to handle routers 
	has clear methods to perform all router operations*/
class SpdRouter
{

		/*!Dim service displaying if there are router errors in the memory or not*/
	DimService *serviceRouterError;

		/*!Internal member to keep the status of the router error, connected with serviceRouterError*/
	int routerError;

		/*!Logging instance for the spd router */
	spdLogger *log;

	//! intenal member storing the router number
	unsigned int routerNumber;

	//! internal member class to store the router addresses
	RoutIntAddr routerAddresses;
	
	//! vme access class, will act as the lower level driver
	VMEAccess * vmeDriver;

	//!Gets the start address, end address and size of one event in the router dual port memory
	/*! \param startAddr start address in the memory, this value is filled inside the function
		\param endAddr end address in the memory, this value is filled inside the function
		\param size size of the event in the memory, this value is filled inside the function
	*/
	void getDpmEventSettings(UInt32 &startAddr, UInt32 &endAddr, UInt32 &size);

public:
	//!simple struct to hold one router error 
	struct RouterError{
		unsigned bunchCrossing;
		unsigned errorOrder;
		unsigned errorClass;
		unsigned errorID;
		UInt32 details1;
		UInt32 details2;

	};

	
	/*! Performs a router reset and the reset of all lrx in this router*/
	void reset(void);

	/*!Sets the memc stimuly, will stop router state machine and write in the correct link receiver the settings
	@param channelInRouter : chanel in this router to enable the mcm stimuly (0-5)
	@param enable: 1 -> enable mcm stimuly, 0 -> disable it
	@param ColumnToSet to set (parameter to set in the mcm stimuly register see documentation of the router/lrx to be sure)*/
	ViStatus setMcmStimuly(UInt8 channelInRouter, UInt32 enable,  UInt32 ColumnToSet = 1);

	/*!constructor of the class
		@param routNumber: number of the router 
		@param vmeDriver: VMEAcess class to get the vme access
	*/
	SpdRouter( unsigned routNumber, VMEAccess * vmeDriver);

	~SpdRouter(void);
		/*! method to send triggers to one router, by default all trigger types are true
		   @param triggerN: number of triggers to send
		   @param delay: delay option
		   @param l2y: level 2 yes trigger option
		   @param l1: level 1 trigger
		   @param tp: test pulse trigger */
	ViStatus SendTrigger(UInt32 triggerN, bool delay = true, bool l2y= true, bool l1=true, bool tp=true, bool l2n = false);

	/*! Method to write a raw data in the calibration header in one router 
		@param data: array of 32 bit words to write
		@param size: number of words to write
	*/
	ViStatus WriteHeader( UInt32 data[], UInt32 size);

	/*! method to set the busy flag
		reads the control register of the router and writes 
		the same value just setting the flag
	*/
	ViStatus setBusyFlag(bool busy);

	/*! method to return if a router is idle or not
		reads the control register 2 and makes an and with 0x4
	*/
	bool getIdleFlag();

	/*!Stets the stop state machine bit in the control register*/
	ViStatus setStopSM(bool stopSM);

	/*! method to get if a router has the stop machine stopped or not
		reads the control register 10 and makes an and with 0x400
	*/
	bool getStopSM();

	//! getter for the router number
	unsigned getRouterNumber(){return routerNumber;};

	ViStatus dataResetHS();

	/*!Method to read the error list from the router memory
		It will read the router memory and parse each error (4 32 bit registers)
		word0 = Errors_counting [23:12], bc_id  [11:0]
		word1 = Errors Class [31:10] , 	Error Name [9:0]
		word2 = Detail 1[31:0]
		word3 = Detail 2[31:0]
	*/
	vector<RouterError> readErrorList();

	/*!Method to read the error mask register of one router
		this is address 0xf0, the format of this register bits its the following:
			0	Enable / Disable Error Handling
			1	Mask ' TTCRX and QPLL link error
			2	Mask ' Trigger Errors from TSM
			3	Mask ' Timeout BC Reset error
			4	Mask ' Trigger error from Router FSM 
			5	Mask ' Fatal error DAQ from Router DAQ FSM
			6	Mask ' Error Optical link (RxReady & RxError)
			7	Mask ' Rx Error (HS optical link)
			8	Mask ' Error Format (HS error format optical communication) 
			9	Mask ' Error Data Transfer (HS error optical data transfer not coherent)
			10	Mask ' Error Control Int (command not properly recognized from the MCM)
			11	Mask ' Error Event Number (Error in MCM Event Number)
			12	Mask ' HS_0 Global Error (Idle, Busy violation, link RX fatal errors, etc)
			13	Mask ' HS_1 Global Errors (     //    )
			14	Mask ' HS_2 Global Errors (     //    )
			15	Mask ' HS_3 Global Errors (     //    )
			16	Mask ' HS_4 Global Errors (     //    )
			17	Mask ' HS_5 Global Errors (     //    )
			18	Mask ' HSs Timeout during data acquisitions 
			19	Mask ' Error Data Format ( from Router Data Format Check)
			20	Mask ' Error FastOR in Data Stream ( Fast OR not coherent in the Data Stream)
			21 .. 32	XXXX don't care
		@return the value of the mask register	*/
	UInt32 readErrorMask();

	/*!Writes the a maks value in the error mask register*/
	void writeErrorMask(UInt32 mask);

	/*! will send a router reset only to one halfstave
		it writes in the router control register masking all other channels,
		sends the router reset and then rewrites the original router control 	*/
	void resetHalfStave(unsigned halfstave);

	//! writes the value in the control register of the router 
	/*!	control reg information:
		bit 0 :		DPM Sample mode
		bit 1 :		no data to DAQ
		bits 2-7 :	HS 0-5 mask (0 -> hs ennabled, 1 -> hs masked)
		bit 8:		Busy flag
		bit 9 :		enable TP in L0
		bit 10:		stop all state machine
		bit 19 :	enable router header
		bit 20:		enable orbit counter
		bit 21:		exclude ttc			*/
	void writeControlReg(UInt32 regValue);

	//! writes the the control register of the router with all the data separated
	void writeControlReg( bool dpmSampleMode,
						  bool noDataToDaq,
						  bool enableHS[6],
						  bool busy,
						  bool enTpL0,
						  bool stopAllSM,
						  bool enRouterHeader,
						  bool enOrbitCounter,
						  bool excludeTTC );

	//! writes the the control register of the router with all the data separated except the enabled half-staves 
	void writeControlReg( bool dpmSampleMode,
						  bool noDataToDaq,
						  bool busy,
						  bool enTpL0,
						  bool stopAllSM,
						  bool enRouterHeader,
						  bool enOrbitCounter,
						  bool excludeTTC );


	/*!reads the contends of the control register in ther router */
	UInt32 readControlReg();

	//! sets the no data to DAQ bit in the control register
	/*!reads the current router control register, changes the corresponding bit with the value (bit number 1)
	  and rewrites the new control register with only this setting changed
	  \param value true=no data to DAQ, false= data to DAQ*/
	void setNoDataToDAQ(bool value);

	//! gets the no data to DAQ bit from the control register
	bool getNoDataToDAQ();

	//! sets the dual port memory sample mode bit in the control register
	/*!reads the current router control register, changes the corresponding bit with the value (bit number 0)
	  and rewrites the new control register with only this setting changed
	  \param value true= sampling events to the memory, false= not writing events to the memory*/
	void setDpmSampleMode(bool value);

	//! gets the dual port memory sample mode bit from the control register
	bool getDpmSampleMode();

	//! sets the exclude ttc bit in the control register
	/*!reads the current router control register, changes the corresponding bit with the value (bit number 21)
	  and rewrites the new control register with only this setting changed
	  \param value true= ignores trigger from ttc false= enables triggers from ttc*/
	void setExcludeTriggersFromTTC(bool value);

	//! gets the exclude ttc bit from the control register
	bool getExcludeTriggersFromTTC();

	/*! Will write the router halfstave mask in the control reg */
	void writeEnabledHs(unsigned enableHS);

	//! writes the enabled halfstaves in the router control register 
	/*! \param enabledHS boolean array containing the values to set in this register */
	void writeEnabledHs(bool enabledHS[NHalfstavesInRouter]);

	//! reads the enabled halfstaves in the router control register 
	/*!\param enabledHS this array is filled inside this function with the values of the enabbled HS*/
	void getEnabledHs(bool enabledHS[NHalfstavesInRouter]);
	
	/*!Will reset all enabled hs in the router */
	void resetDetector();

	/*!Function to configure a router 
	 It peforms a router and lrx resets 
	 write the control register, sets L0L1 time 
	 and the fastorL1 time for all the lrx 
	 @param options : bit0 enable router reset, bit1 configure router, bit2 L0L1 time, bit3 lrx fastor L1 time
	 @param ctlrReg: router control reg
	 @param L0L1 time
	 @param lrxFoDelays fastor L1 delay for al link receivers (0-2) */
	void configureRouter(UInt32 options,
							UInt32 ctlrReg, 
							UInt32 L0L1, 
							UInt32 lrxFoDelays[]);

	/*!Function read the router busy times
	 it receives as input an array that is filled inside
	 @param values array to be filled inside: 
		0 = busy daq, 
		1= busy router, 
		2 = busy HS, 
		3 = busy trigger 
	 @return error condition  */
	UInt32 readRouterBusyTimes(UInt32 values[]);

	/*!Resets all jtag controlllers 
	Send the command to the common jtag controllers area to reset the routers
	its resets the JTAG state machine, FIFOs and channel address*/
	void resetJtagcontrollers();

	//!Method to check all idles in all the jtag controllers
	/*!Loops to over the 6 jtag controllers and logs if they are not in idle*/
	void checkJtagIdles();

	//!Checks if one jtag controller is in idle
	/*!Reads the idle bit in one jtag controller status register
	\param hs jtag controller number (0-6)
	\return idle status */
	bool isJtagIdle(int hs);

	//!reads and parses all events available in the dpm of the router
	/*!\return returns the parsed data */
	SpdHitData readEventsFromMemory();

	//!saves the content of the memory of one router to file
	/*!\param filename the name of the file where the raw data will be saved*/
	void saveDPMDataToFile(const char * filename);

	//! resets the dual port memory in the router cleaning all events
	void resetEventsInDpm(void);

	//! Function to write the fastor strobe length in one router
	/*	\param strobe length value
		\return error condition
	*/
	UInt32 setFoStrobeLength(unsigned int strobeLength);

	//! Function to write one arbitrary router register
	/*! \param address of the router with the data to read
		\param value to write in the register
	*/
	void writeRouterRegister(UInt32 routerAddress, unsigned addrSelection, UInt32 value);

	//! Function to read one arbitrary router register
	/*! \param address of the router with the data to read
		\return data read from the chosen register
	*/
	UInt32 readRouterRegister(UInt32 routerAddress, unsigned addrSelection);

	//! Function to write one arbitrary register inside one link receiver
	/*! \param channer number
		\param address of the register inside the link-rx
		\param value to write in the register
	*/
	void writeLrxRegister(UInt32 chNumber, UInt32 lrxAddress, UInt32 value);

	//! Function to read one arbitrary register inside one link receiver
	/*! \param channer number
		\param address of the register inside the link-rx
		\return data read from the register
	*/
	UInt32 readLrxRegister(UInt32 chNumber, UInt32 lrxAddress);


	///***************** ROUTER INITIALIZATION ********************///

	//! Selects single event buffer or multi event buffer in the router
	/*! \param value true means single event buffer selected, false means multi event buffer selected*/
	void setSebMebMode(bool value);

	//! Selects single event buffer or multi event buffer in the router
	/*! \return true is SEB is selected, false if MEB is selected*/
	bool getSebMebMode();
	
	/*!Function to write the fastor L1 delay in one link receiver
	@param lrx: link receiver number 0-2
	@param fastorDelay - the L1 fastor delay */
	void writeLrxFastorL1Delay(unsigned lrx, UInt32 fastorDelay);

	/*!writes the LoL1 time in the router */
	void writeL0L1Time(UInt32 value);

	//! Function to set the timeout for ready events in one router
	/*	\param timeout value
		\return error condition
	*/
	UInt32 setTimeoutReadyEvent(unsigned int timeout);
};
