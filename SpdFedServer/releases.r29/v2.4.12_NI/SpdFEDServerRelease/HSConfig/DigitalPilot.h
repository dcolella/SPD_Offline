#pragma once

#include "..\BitManager\BitsManage.h"


//!Class to manage the digital pilot 
/*!	it basically decodes all the different settings : strobe lenght, wait before row etc
	into the 2 32 bit registers for the the normal digial pilot settings
	and the 3 32 bit registers for the internal digital pilot settings 
	its has mainly inline methods*/
class DigitalPilot : BitsManage {
private:
	UInt32 DPIConf[2];
	UInt32 DPIInternal[3];

public:
	DigitalPilot(void);
	~DigitalPilot(void);
    
	//! gets the pointer to the internal register CAUTION: this is the internal members of the class
	UInt32 * GetDPIConf(void) {return this->DPIConf;};
	void SetDPIConf(UInt32 * Vector);
	//! gets internaln register number one
	UInt32 GetDPIConf0(void){return this->DPIConf[0];};
	void SetDPIConf0(UInt32 val){this->DPIConf[0] = val;};
	UInt32 GetDPIConf1(void){return this->DPIConf[1];};
	void SetDPIConf1(UInt32 val){this->DPIConf[1] = val;};

	//! gets an array with tall the settings parsed
	/*!DPIElements[0] = this->GetDPIConfWaitBefRow();
	DPIElements[1] = this->GetDPIConfSebMeb();
	DPIElements[2] = this->GetDPIConfMaskChip();
	DPIElements[3] = this->GetDPIConfEventNumb();
	DPIElements[4] = this->GetDPIConfStrobeLength();
	DPIElements[5] = this->GetDPIConfHoldRow();
	DPIElements[6] = this->GetDPIConfSkipMode();
	DPIElements[7] = this->GetDPIConfTDO8TDO9();
	DPIElements[8] = this->GetDPIConfEnableCESeq();
	DPIElements[9] = this->GetDPIConfDataFormat();
	DPIElements[10] = this->GetDPIConfMebValIn_RO(); 
	DPIElements[11] = this->GetDPIConfL2nFifoIn_RO(); 
	DPIElements[12] = this->GetDPIConfL2yFifoIn_RO(); 
	DPIElements[13] = this->GetDPIConfL2WrPtr_RO(); 
	DPIElements[14] = this->GetDPIConfL2RdPtr_RO(); */
    UInt32 * GetDPIConfElements();
    void SetDPIConfElements(UInt32 * VectorInput);

	void SetDPIConfWaitBefRow(UInt8 Val){ModifyUInt32(this->DPIConf[0],Val, 0, 3);}; 
	UInt8 GetDPIConfWaitBefRow(void){return (UInt8)ExtractFromUInt32(this->DPIConf[0], 0, 3);}; 
	
    void SetDPIConfSebMeb(UInt8 Val){ModifyUInt32(this->DPIConf[0],Val, 3, 1);};
	bool GetDPIConfSebMeb(void){return ExtractFromUInt32(this->DPIConf[0], 3, 1)? true:false;}; 

	void SetDPIConfMaskChip(UInt16 Val){ModifyUInt32(this->DPIConf[0],Val, 4, 10);};
    UInt16 GetDPIConfMaskChip(void){return (UInt16)ExtractFromUInt32(this->DPIConf[0], 4, 10);}; 

	void SetDPIConfEventNumb(UInt16 Val){ModifyUInt32(this->DPIConf[0],Val, 14, 10);};
    UInt16 GetDPIConfEventNumb(void){return (UInt16)ExtractFromUInt32(this->DPIConf[0], 14, 10);}; 

	void SetDPIConfStrobeLength(UInt16 Val){ModifyUInt32(this->DPIConf[0],Val, 24, 4);};
    UInt8 GetDPIConfStrobeLength(void){return (UInt8)ExtractFromUInt32(this->DPIConf[0], 24, 4);};

	void SetDPIConfHoldRow(bool Val){ModifyUInt32(this->DPIConf[0],Val, 28, 1);};
	bool GetDPIConfHoldRow(void){return ExtractFromUInt32(this->DPIConf[0], 28, 1)?true:false;}; 

	void SetDPIConfSkipMode(UInt8 Val){ModifyUInt32(this->DPIConf[0],Val, 29, 2);};
    UInt8 GetDPIConfSkipMode(void){return (UInt8)ExtractFromUInt32(this->DPIConf[0], 29, 2);}; 

	void SetDPIConfTDO8TDO9(bool Val){ModifyUInt32(this->DPIConf[0],Val, 31, 1);};
	bool GetDPIConfTDO8TDO9(void){return ExtractFromUInt32(this->DPIConf[0], 31, 1)?true:false;}; 

	void SetDPIConfEnableCESeq(bool Val){ModifyUInt32(this->DPIConf[1],Val, 0, 1);};
    bool GetDPIConfEnableCESeq(void){return ExtractFromUInt32(this->DPIConf[1], 0, 1)?true:false;};

    void SetDPIConfDataFormat(bool Val){ModifyUInt32(this->DPIConf[1],Val, 1, 1);};
    bool GetDPIConfDataFormat(void){return ExtractFromUInt32(this->DPIConf[1], 1, 1)?true:false;}; 
    
	UInt16 GetDPIConfReadOnly(void){return (UInt16)ExtractFromUInt32(this->DPIConf[1], 2, 15);}; 
	
	UInt16 GetDPIConfMebValIn_RO(void){return (UInt16)ExtractFromUInt32(this->DPIConf[1], 2, 3);}; 
	UInt16 GetDPIConfL2nFifoIn_RO(void){return (UInt16)ExtractFromUInt32(this->DPIConf[1], 5, 4);}; 
	UInt16 GetDPIConfL2yFifoIn_RO(void){return (UInt16)ExtractFromUInt32(this->DPIConf[1], 9, 4);}; 
	UInt16 GetDPIConfL2WrPtr_RO(void){return (UInt16)ExtractFromUInt32(this->DPIConf[1], 13, 2);}; 
	UInt16 GetDPIConfL2RdPtr_RO(void){return (UInt16)ExtractFromUInt32(this->DPIConf[1], 15, 2);}; 

	//Internal Register
	//---------------------------------------------------------------------------------------------
	UInt32 * GetDPIInternal(void) {return this->DPIInternal;};
	void SetDPIInternal(UInt32 * Vector);
	UInt32 GetDPIInternal0(void){return this->DPIInternal[0];};
	void SetDPIInternal0(UInt32 val){this->DPIInternal[0] = val;};
	UInt32 GetDPIInternal1(void){return this->DPIInternal[1];};
	void SetDPIInternal1(UInt32 val){this->DPIInternal[1] = val;};
	UInt32 GetDPIInternal2(void){return this->DPIInternal[2];};
	void SetDPIInternal2(UInt32 val){this->DPIInternal[2] = val;};
    UInt32 * GetDPIInternalElements();
    void SetDPIInternalElements(UInt32 * VectorInput);
    
	//scan_strobe_busy register
	void SetDPIInternalStrobe_i(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 0, 1);};
    bool GetDPIInternalStrobe_i(void){return ExtractFromUInt32(this->DPIInternal[0], 0, 1)?true:false;};

	void SetDPIInternalStrobeCycCnt(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 1, 4);};
    UInt8 GetDPIInternalStrobeCycCnt(void){return (UInt8) ExtractFromUInt32(this->DPIInternal[0], 1, 4);};

	void SetDPIInternalMebVal(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 5, 3);};
	UInt8 GetDPIInternalMebVal(void){return (UInt8)  ExtractFromUInt32(this->DPIInternal[0], 5, 3);};

	void SetDPIInternalBusy(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 8, 1);};
    bool GetDPIInternalBusy(void){return ExtractFromUInt32(this->DPIInternal[0], 8, 1)?true:false;};

	void SetDPIInternalBusyViolation(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 9, 1);};
    bool GetDPIInternalBusyViolation(void){return ExtractFromUInt32(this->DPIInternal[0], 9, 1)?true:false;};

	void SetDPIInternalIdleViolation(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 10, 1);};
    bool GetDPIInternalIdleViolation(void){return ExtractFromUInt32(this->DPIInternal[0], 10, 1)?true:false;};

	//scan_queue_scan
	void SetDPIInternalL2WrPtr(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 11, 2);};
    UInt8 GetDPIInternalL2WrPtr(void){return (UInt8) ExtractFromUInt32(this->DPIInternal[0], 11, 2);};

	void SetDPIInternalL2yFIFO(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 13, 4);};
    UInt8 GetDPIInternalL2yFIFO(void){return (UInt8)ExtractFromUInt32(this->DPIInternal[0], 13, 4);};

	void SetDPIInternalL2nFIFO(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 17, 4);};
    UInt8 GetDPIInternalL2nFIFO(void){return (UInt8) ExtractFromUInt32(this->DPIInternal[0], 17, 4);};

	void SetDPIInternalId3(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 21, 1);};
    bool GetDPIInternalId3(void){return ExtractFromUInt32(this->DPIInternal[0], 21, 1)?true:false;};

	void SetDPIInternalIdleCnt(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 22, 2);};
    UInt8 GetDPIInternalIdleCnt(void){return  (UInt8)ExtractFromUInt32(this->DPIInternal[0], 22, 2);};

	void SetDPIInternalL2RdPtr(UInt8 Val){ModifyUInt32(this->DPIInternal[0],Val, 24, 2);}; //to fix
    UInt8 GetDPIInternalL2RdPtr(void){return (UInt8)ExtractFromUInt32(this->DPIInternal[0], 24, 2);};
	
	void SetDPIInternalStartRow(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 26, 1);};
    bool GetDPIInternalStartRow(void){return ExtractFromUInt32(this->DPIInternal[0], 26, 1)?true:false;};

	void SetDPIInternalClear(bool Val){ModifyUInt32(this->DPIInternal[0],Val, 27, 1);};
    bool GetDPIInternalClear(void){return ExtractFromUInt32(this->DPIInternal[0], 27, 1)?true:false;};

	//scan_pilot_sm2003
	
    void SetDPIInternalState(UInt8 Val){
		ModifyUInt32(this->DPIInternal[0],Val, 28, 4); 
		ModifyUInt32(this->DPIInternal[1],(Val >> 4), 0, 1);
	};

	UInt8 GetDPIInternalState(void){
		return (UInt8) ( ExtractFromUInt32(this->DPIInternal[0], 28, 4) 
		+ ((ExtractFromUInt32(this->DPIInternal[1], 0, 1)) << 4));
    };
	
    

	
	void SetDPIInternalWaitBeforeRow(UInt8 Val){ModifyUInt32(this->DPIInternal[1],0x07 & Val, 1, 3);};
    UInt8 GetDPIInternalWaitBeforeRow(void){return (UInt8) ExtractFromUInt32(this->DPIInternal[1], 1, 3);};

	void SetDPIInternalRowAdd(UInt8 Val){ModifyUInt32(this->DPIInternal[1],Val, 4, 8);};
    UInt8 GetDPIInternalRowAdd(void){return (UInt8) ExtractFromUInt32(this->DPIInternal[1], 4, 8);};

	void SetDPIInternalEventNumb(UInt16 Val){ModifyUInt32(this->DPIInternal[1],Val, 12, 10);};
    UInt16 GetDPIInternalEventNumb(void){return (UInt16) ExtractFromUInt32(this->DPIInternal[1], 12, 10);};

	void SetDPIInternalRemainingChips(UInt16 Val){ModifyUInt32(this->DPIInternal[1],Val, 22, 10);};
    UInt16 GetDPIInternalRemainingChips(void){return (UInt16) ExtractFromUInt32(this->DPIInternal[1], 22, 10);};

	void SetDPIInternalFirstEv(bool Val){ModifyUInt32(this->DPIInternal[2],Val, 0, 1);};
    bool GetDPIInternalFirstEv(void){return ExtractFromUInt32(this->DPIInternal[2], 0, 1)?true:false;};
	

};
