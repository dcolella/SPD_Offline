#pragma once
#include "StdAfx.h"
#include ".\jtagaccess.h"

JTAGAccess::JTAGAccess(){
	_ScanType = 0 ;    
	_BitNumber = 0;   
	_ChNumb = 0;       
	_CLKSpeed = 0;     
	_JTAGHeader = 0xffffffff;
    _VectorOut = new UInt32;
    BuildContrWords(0);


	log = &spdLogger::getInstance(); 
}

JTAGAccess::~JTAGAccess(void){
	if (_VectorOut != NULL) delete _VectorOut;
}


ViStatus JTAGAccess::JTAGWriter(UInt8  ChN, UInt32 * VectIn, UInt32  ScType, UInt32 BtNumb){
	this->SetJTChannel(ChN);
	_VectorIn = VectIn+1;
	_ScanType = ScType;
	_BitNumber = BtNumb;
	return this->JTAGScanReg();
}

ViStatus JTAGAccess::JTAGWriter(UInt8 ChN, UInt32 * IRVect, UInt32 * DRVect){
	this->SetJTChannel(ChN);
    
	_BitNumber = IRVect[0];
	_ScanType = IR;
	_VectorIn = IRVect + 1;

	ViStatus status = JTAGScanReg();
	if(status != 0) return status;
	
    _BitNumber = DRVect[0];
	_ScanType = DR;
	_VectorIn = DRVect + 1;
	status = JTAGScanReg();
	return status;
}


ViStatus JTAGAccess::JTAGWriter(UInt8 ChN, UInt32 * IRVect0, UInt32 * DRVect0, UInt32 * IRVect1, UInt32 * DRVect1){
	ViStatus status = this->JTAGWriter(ChN, IRVect0, DRVect0);
	if(status != 0) return status;
	status = this->JTAGWriter(ChN, IRVect1, DRVect1);
	return status; 
}


void JTAGAccess::SetJTClkSpeed(UInt32 CLKSp){
	if(CLKSp >=0 && CLKSp < 5){
		_CLKSpeed = CLKSp;
		log->log("INFO: JTAG clock speed set at %d MHz",(UInt32)(2^_CLKSpeed) );
	}else{
		log->log("ERROR: The JTAG clock speed specified is out of range 0..4");

	}
}


	
//Scan the JTAG register: IstrType tell if scan DR (0) or IR (1) or RESET (2)   
ViStatus JTAGAccess::JTAGScanReg(void){
           
	if(isInDebugMode()){
		//cout << "|";
		return 0;
	}
    BuildContrWords(2);
	
	this->SetJTChannel(_ChNumb);

	if(!this->IsControllerIdle(FALSE)){
		log->log("WARNING: JTAG controller in idle, reseting channel %d", this->_ChNumb);
		this->ResetJTAGContr();
	}

	//if(!this->IsControllerIdle(TRUE)) {
	//	log->log("WARNING: JTAG controller in idle, reseting it, channel %d", this->_ChNumb);
	//	this->ResetJTAGContr();
	//}

	this->BuildContrWords(1);
	    		

    if(_ScanType == 2){      //RESET istruction
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _ContrWord1); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _ExStartAddr,  0); 
		
	}else{
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _ContrWord1); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _ContrWord2); 
		
		for(UInt32 i=0; i < _WordsNumber; i++){
//			log->log("VectorIn word %d: %x", i, this->_VectorIn[i]);
			viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _VectorIn[i]); 
		}
    
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
		viOut32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr,  _JTAGHeader); 
        viOut32(VisSesObjGen(0), VI_A32_SPACE, _ExStartAddr, 0);
    
        UInt32 startTime;
		startTime =	GetTickCount();  

        while(!this->IsControllerIdle(FALSE)){  				
			if((GetTickCount() - startTime) > 500){
				log->log("ERROR: JTAG Timeout channel %d , reseting it's controler",this->_ChNumb );
				this->ResetJTAGContr();
				return 1;
			}
		}
		                
		if(_VectorOut != NULL) delete[] _VectorOut;
		_VectorOut = new UInt32 [_WordsNumber + 1];
			
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
			
        for(UInt32 i=0; i < _WordsNumber; i++){
			viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + i);
//			log->log("VectorOut word %d: %x", i, this->_VectorOut[i]);
		}
            
		_VectorOut[_WordsNumber - 1] >>=  (32 - _LastWordBits);
//		log->log("VectorOutLast: %x", _VectorOut[_WordsNumber - 1]);

		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
		viIn32(VisSesObjGen(0), VI_A32_SPACE, _RdWrDataAddr, _VectorOut + _WordsNumber);
			
		if(!this->IsControllerIdle(TRUE)) this->ResetJTAGContr();
	}
    
	return 0;
}


bool JTAGAccess::IsControllerIdle(bool reset){

	

	if(isInDebugMode()){
		//cout << "|";
		return 0;
	}

	UInt32 StatusContr;
	ViStatus status = viIn32(VisSesObjGen(0), VI_A32_SPACE, _StatusRegAddr, &StatusContr);

	if(status != 0 ){
		log->log("ERROR: error in VME access during jtag scan on channel %d", this->_ChNumb);
		return FALSE;
	}

	// this magic number 0xCC018001 appears after a reset. 
	//this is a weird use of this function, basicaly its checking also if it is idle 
	// or if its after a reset so I will rewrite the code
	//if(( (StatusContr == 0xCC018001) && reset) || ((StatusContr & 0x1) && !reset)) return TRUE;
	if (reset){
		// checks for reset
		if (StatusContr == 0xCC018001) return true;
	}
	else{
		//checks for normal idle
		if (StatusContr & 0x1) return true;
	}

	
	return false;
}


ViStatus JTAGAccess::ResetJTAGContr(){  

	if(isInDebugMode()){
		//cout << "|";
		return 0;
	}
    ViStatus status = 1;
	
	status = viOut32(VisSesObjGen(0), VI_A32_SPACE,  _ResetControllerSM, 0);
    if (status)log->log("ERROR: error in VME access during jtag state machine reset on channel %d", this->_ChNumb);
	return status;
}		

void JTAGAccess::SetJTChannel(UInt8 ChN){
	AddressGenerator & addresses = AddressGenerator::getInstance();
	 _ChNumb = ChN;
	 _RdWrDataAddr = addresses.getRoutAddr(ChN).GetJTRdWrDataAddr(ChN);
	 _ExStartAddr = addresses.getRoutAddr(ChN).GetJTExStartAddr(ChN);
	 _StatusRegAddr = addresses.getRoutAddr(ChN).GetJTStatusRegAddr(ChN);
	 _ResetControllerSM = addresses.getRoutAddr(ChN).GetJTResetStateMacAddr(ChN);
}


void JTAGAccess::BuildContrWords(UInt8 Mode){
	   
	if(Mode == 0 || Mode == 1){
	   _ContrWord1 = (_CLKSpeed << 12) + (_ChNumb << 7) + _ScanType; 
	}
	if(Mode == 0 || Mode == 2){
       _WordsNumber = (_BitNumber / 32);
       _LastWordBits = _BitNumber % 32;
	   if (_LastWordBits == 0){ 
		   _LastWordBits = 32;
	   } else {
		   _WordsNumber++;
	   }
       _ContrWord2 = _WordsNumber + (_LastWordBits - 1) * 0x4000000;
	}
}
