#pragma once

//#include "StdAfx.h"
#include "visasesscontr.h"
#include "../addressgenerator/addressgenerator.h"


//! class to get the VME register access 
class VMERegAccess : public VisaSessControl{
  private:
	  UInt8 VRAOperation;  // bit 0 =  Map Addr
	                        // bit 1 = Unmap Addr
	                     

	  UInt8 SessMappedNumb;
	  ViAddr MappedRetNumb;
  public:

    //AddressGenerator * AG;

	//void SetAGPointer(AddressGenerator * AGI){AG = AGI;};          //ok
   
	VMERegAccess(void);                                                      //ok
	~VMERegAccess(void);                                                   //ok
     
	UInt8 GetSessMappedNumb(void){return SessMappedNumb;};                  //ok

	ViAddr GetMappedRetNumb(void){return MappedRetNumb;};                   //ok
	void SetSessMappedNumb(UInt8 &SsMpNb){SessMappedNumb=SsMpNb;};          //ok
	void SetMappedRetNumb(ViAddr MpRtNb){MappedRetNumb=MpRtNb;};           //ok  


    ViStatus VMERegisterAcc(UInt32  RegAddr, UInt32 * Value, UInt32 dataSize, char mode);  //mode specify if write or read

		//! method to write one register in the vme
	ViStatus VMEWriteRegister(UInt32  address, UInt32 value){return VMERegisterAcc(address,&value,1,'W');};
	ViStatus VMEReadRegister(UInt32  address, UInt32 *value){return VMERegisterAcc(address,value,1,'R');};

		
};
