// DIMServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
using namespace std;


#include "dis.hxx" 
#include "../AddressGenerator/ChToAddrDecoder.h"
#include "../AddressGenerator/addressgenerator.h"
#include "../VMEAcc/errorhandler.h"
#include "../VMEAcc/VMEAccess.h"
#include ".\DimServerCommandHandler.h"
#include ".\DimServerServiceHandler.h"

#include "../HSConfig/SPDConfig.h"


const char iniFileName[] = "spdFedIni.ini";

			
int main(){
    
	// remember to add the \0 for the string termination
	char Version[] = "2.4.12 access to single chip\0";
	char Side = 'A';
	bool deBugMode=false;
  	string jamFilename;

	spdLogger &log = spdLogger::getInstance();
	log.log("********************** starting spd fed software **********************");
	log.log("parsing ini file 'spdFedIni.ini'");

//******************** gets the values from the ini file*******************************************
	
						// parses the inifile
	spdCalib::IniParser inifile(iniFileName);
						// gets the general settings
	spdCalib::Category genSettings = inifile["General"];

	Side = genSettings.entries["side"][0];

	// checks the side
	switch(Side){
		case 'a':
		case 'A':
			Side = 'A';
			break;
		case 'c':
		case 'C':
			Side='C';
			break;
		default:
			
			log.log("ERROR side should be 'A' or 'C' found %c, please check the %s file", Side,iniFileName);
			throw std::out_of_range("side as to be 'A' or 'C'");
	}
	


	if (genSettings.entries["debugMode"]=="true"){
		deBugMode=true;
		log.log("**********************************************************");
		log.log("***        Fed server in Debug mode no VME acess       ***");
		log.log("**********************************************************");
		deBugMode= true;
	}
	else{
		deBugMode=false;
	}


	// gets the db settings from the ini file
	spdCalib::Category dbSettings = inifile["DataBase"];

						// puts the settings in the connection object
	spdDbConnection *conn = spdDbConnection::subscribe();

	conn->setDbSettings( dbSettings);

	spdCalib::Category jamSettings = inifile["jam player"];
	
	jamFilename = jamSettings.entries["executable"];


//************************************************************************


	VMEAccess &VMEAcc = VMEAccess::getInstance(); 
			// will set here the debug Mode from the inin file
	VMEAcc.setDebugMode(deBugMode);

	
	VMEAcc.RunResorceMenager();
	log.log("automatic INIT_VME");

	DimServerCommandHandler Commands(  Side);
	Commands.setJamPlayerFilename( jamFilename);

	

	log.log("starting spdFED server for side %c version =%s", Side, Version);

    if( Side == 'A'){
	  DimServer::start("spd_feDimServerA");
	} else {
      DimServer::start("spd_feDimServerC");
	}


	// will be sending the heartbit
	while (1){
		Sleep(1);
		Commands.PoolingFunction();
		
		if( (GetTickCount() % 3000) == 0){
			Commands.SendHearthbit();  
		}
	}
	return 0;
}

	
