#include "StdAfx.h"
#include ".\dimserverservicehandler.h"

#include "dis.hxx" 
#include "../AddressGenerator/ChToAddrDecoder.h"
#include "../AddressGenerator/addressgenerator.h"
#include "../VMEAcc/errorhandler.h"
#include "../VMEAcc/VMEAccess.h"
#include ".\DimServerCommandHandler.h"
#include ".\DimServerServiceHandler.h"

#include "../HSConfig/SPDConfig.h"


DimServerServiceHandler::DimServerServiceHandler(char Side){

  	
  side = Side;
  logger = &spdLogger::getInstance();

  this->heartBitTime = GetTickCount();

  if( Side == 'A'){
	SendChNumber = new DimService("spd_feDimServerA/CH_NUMBER", ChannelNumber);
	SendCommandName = new DimService("spd_feDimServerA/CMD_NAME", "no command");
	SendErrorReportCode = new DimService("spd_feDimServerA/ERROR_CODE", "I:1", this);
	SendErrorReport = new DimService("spd_feDimServerA/ERRORS_REPORT", "INFO: fed started");
	SendRetData = new DimService("spd_feDimServerA/RET_DATA", "I", this);
	SendDataStream = new DimService("spd_feDimServerA/DATA_STREAM", "I", this);
	Hearthbit = new DimService("spd_feDimServerA/HEATHBIT", heartBitTime);
		// will set the logging dim service also
	logger->setDimServiceName("spd_feDimServerA/LOG");

	

	
  } else {
	  SendChNumber = new DimService("spd_feDimServerC/CH_NUMBER",ChannelNumber);
	SendCommandName = new DimService("spd_feDimServerC/CMD_NAME",  "no command");
	SendErrorReportCode = new DimService("spd_feDimServerC/ERROR_CODE", "I:1", this);
	SendErrorReport = new DimService("spd_feDimServerC/ERRORS_REPORT",  "INFO: fed started");
	SendRetData = new DimService("spd_feDimServerC/RET_DATA", "I", this);
	SendDataStream = new DimService("spd_feDimServerC/DATA_STREAM", "I", this);
	Hearthbit = new DimService("spd_feDimServerC/HEATHBIT", heartBitTime);
	//will set the logging dim service also
	logger->setDimServiceName("spd_feDimServerC/LOG");
	
	
  }
  
  logger->logToFile("dim log service started");


}

DimServerServiceHandler::~DimServerServiceHandler(void){


	// deletes all services
	delete SendChNumber ;
	delete SendCommandName;
	delete SendErrorReportCode;
	delete SendErrorReport ;
	delete SendRetData; 
	delete SendDataStream;
	delete Hearthbit;


}





void DimServerServiceHandler::SetCommandName(char * cmdName){
		CommandName = cmdName; 
		sizeCommandName = strlen(CommandName);
}


void DimServerServiceHandler::SetErrorName(char * errName, int errCode){
		ErrorCode = errCode;
	    ErrorName = errName; 
		sizeErrorName = strlen(ErrorName);
}

void DimServerServiceHandler::SendHearthbit(void){
	heartBitTime = GetTickCount();
	Hearthbit->updateService(heartBitTime);
}

int DimServerServiceHandler::UpdateServices(void){

	SendChNumber->updateService( ChannelNumber);
	SendRetData->updateService(DataToSend, (sizeDataToSend * 4));
	SendCommandName->updateService(CommandName);

	SendErrorReportCode->updateService(ErrorCode);
	SendErrorReport->updateService(ErrorName);
	logger->log(ErrorName);

	
	return 0;
} 


int DimServerServiceHandler::UpdateServices(int ChNumber, char * cmdName, int errCode, char * errName, unsigned int * dttoSnd, unsigned int szDtToSend){
	
	ChannelNumber = ChNumber;

	CommandName = cmdName; 
	sizeCommandName = strlen(CommandName);
    
	ErrorCode = errCode;
	ErrorName = errName; 
	sizeErrorName = strlen(ErrorName);
	
    DataToSend = dttoSnd;
    sizeDataToSend = szDtToSend;
    return UpdateServices();
}


int DimServerServiceHandler::UpdateChNumber(){
	return SendChNumber->updateService(ChannelNumber);
	
}

int DimServerServiceHandler::UpdateChNumber(int ChNumber){
	ChannelNumber = ChNumber;
	return UpdateChNumber();
	
}


int DimServerServiceHandler::UpdateCommandName(){
	return SendCommandName->updateService(CommandName);
}

int DimServerServiceHandler::UpdateCommandName(char * cmdName ){
    CommandName = cmdName; 
	sizeCommandName = strlen(CommandName);
	return UpdateCommandName();
}


int DimServerServiceHandler::UpdateErrorName(){
	SendErrorReportCode->updateService(ErrorCode);
	SendErrorReport->updateService(ErrorName);

	logger->log(ErrorName);
	return 0;
}

int DimServerServiceHandler::UpdateErrorName(char * errName, int errCode ){
	ErrorCode = errCode;
	ErrorName = errName; 
	sizeErrorName = strlen(ErrorName);
	return UpdateErrorName();
}

int DimServerServiceHandler::UpdateDataToSend(){
	return SendRetData->updateService(DataToSend, sizeDataToSend * 4);
}


int DimServerServiceHandler::UpdateDataToSend(unsigned int* dttoSnd, unsigned int szDtToSend){
    DataToSend = dttoSnd;
    sizeDataToSend = szDtToSend;
	return UpdateDataToSend();
}

int DimServerServiceHandler::UpdateDataStream(){
	return SendDataStream->updateService(DataStream, sizeDataStream * 4);
}


int DimServerServiceHandler::UpdateDataStream(unsigned int* dttoSnd, unsigned int szDtToSend){
    DataStream = dttoSnd;
    sizeDataStream = szDtToSend;
	return UpdateDataStream();
}