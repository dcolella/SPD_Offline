#pragma once

#include <stringstream>
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"

int DimServerCommandHandler::StringCmdHandler(void){
	if(strncmp(CommString, "STR_", 4) != 0) return 2;

	VMEAccess & VME = VMEAccess::getInstance();

	int dataOutSize = 0;
    status = 0;

	stringstream inputStream(CommString);

	string command;

	inputStream >> command;
	
	if (command == "STR_PROGRAM_ROUTERS"){
		string mode;
		string filename;
		inputStream >> mode;
		inputStream >> filename;

		if (dataSize < 10){
			status = 1;
			SRV->UpdateServices(ChNumber, CommString, status, CheckStatus(CommStringOriginal,status, ChNumber),(unsigned int*)DataOut, dataOutSize);	
			logger->log("(DimServerCommandHandler::StringCmdHandler) data in as to be bigger than 10 for STR_PROGRAM_ROUTERS command");
			return status;
		}

		char systemCommand[200];

		int sideOffset = (DetSide ==0)?0:10;
		unsigned sideRouter; 

		for (int router = 0 ; router < 10 ; router ++){
			if (this->DataIn[router] != 0){
				sideRouter = router + sideOffset;
				

				if (VME.isInDebugMode()) {
					logger->log("(StringCmdHandler)fed server is in debug mode jam player will not be called");
					sprintf(systemCommand, "echo %s  -I%d %s %s ", jamPlayerExecutable.c_str(), sideRouter, mode.c_str(),filename.c_str());
				}
				else{
					sprintf(systemCommand, "%s -I%d %s %s ", jamPlayerExecutable.c_str(), sideRouter, mode.c_str(),filename.c_str());
				}

				logger->log("(DimServerCommandHandler::StringCmdHandler)starting programming router %d", sideRouter);
				logger->log("starting external command '%s'", systemCommand);

				status = system( systemCommand);

				if (status > 1) status = 1;
								
				SRV->UpdateServices(sideRouter,	CommString, status, CheckStatus("programming router", status, sideRouter), NULL, dataOutSize);	
			}
		}
	
	}
	else if (command == "STR_SAVE_DPM_TO_FILE"){
		string filename;
		inputStream >> filename;

		SPD->getRouter(ChNumber/6).saveDPMDataToFile( filename.c_str());
	}

	return status;
	
}