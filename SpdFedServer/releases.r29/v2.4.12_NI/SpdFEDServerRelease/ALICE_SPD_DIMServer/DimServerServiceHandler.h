#pragma once
#include "StdAfx.h"
#include <dis.hxx>
#include "..\spdDbConfLib\spdLogger.h"

const int ChannelsPerSide=60;
//! Class to maintain all DIM services to PVSS
class DimServerServiceHandler : public DimService{
	private:
		//! logger instance
		spdLogger *logger;
		//! DIM service sending back the channel number received
        DimService * SendChNumber;
		//! Dim service sending back the command string received
		DimService * SendCommandName;
		//! Dim command sending back the error code of the last command (0 if everything is ok)
		DimService * SendErrorReportCode;
		//! Dim service sending a string with the status 
		DimService * SendErrorReport;
		//! sends the data output from a command (does not resend the data in received)
		DimService * SendRetData;
		//! sends the data stream to the external analisys tool
		DimService * SendDataStream;
		//! sends the ehartbit to PVSS
		DimService * Hearthbit;

		//! keeps the side of this FED server
		char side;
		//! keeps the channel number that will be send to the DIM service
		int ChannelNumber;
		//! keeps the command name to send through the DIM service
		char * CommandName;
		//! keeps the size of the command name string
        unsigned int sizeCommandName;
        //! keeps the error code to be sent
		int ErrorCode;
		//! string with the error string to be sent 
		char * ErrorName;
		//! keeps the size of the error string
		unsigned int sizeErrorName;
		//! keeps the data to send through the dim service to PVSS
		unsigned int * DataToSend;
		//! keeps the size of the data that was sent 
        unsigned int sizeDataToSend;  
		//! keeps the pixel raw data to be sent to the external analisys tool
		unsigned int * DataStream;
		//! keeps the size of the data stream
        unsigned int sizeDataStream;
		//! the actual heartbit value 
		int  heartBitTime;
		
        		 
	public:
		DimServerServiceHandler(char);
		~DimServerServiceHandler(void);
	    
		//! setter for the channel number
		void SetChNumber(int ChNumber){ChannelNumber = ChNumber;}; 
		void SetCommandName(char *);													
		void SetErrorName(char *, int);                                        
		void SetDataToSend(unsigned int * dttoSnd){DataToSend = dttoSnd;}; 
		void SetDataToSendSize(unsigned int szDtToSend){sizeDataToSend = szDtToSend;}; 
	    
		//! updates all services
		int UpdateServices();	
		//! updates all services settting the internal members, it calls UpdateServices();
		int UpdateServices(int ChNumber, char * cmdName, int errCode, char * errName, unsigned int * dttoSnd, unsigned int szDtToSend); 
		//! send the heartbit it gets th system milliseconds and sends it
		void SendHearthbit();
		//! updates only thec hannel number
		int UpdateChNumber();			
		int UpdateChNumber(int );								
		int UpdateCommandName();										
		int UpdateCommandName(char *);										
		int UpdateErrorName();									
		int UpdateErrorName(char *, int);								
		int UpdateDataToSend();										
		int UpdateDataToSend(unsigned int*, unsigned int);			
		int UpdateDataStream();											
		int UpdateDataStream(unsigned int* dttoSnd, unsigned int szDtToSend); 
		//void UpdateTemperature(const double * busTemp, const double * mcmTemp);
};