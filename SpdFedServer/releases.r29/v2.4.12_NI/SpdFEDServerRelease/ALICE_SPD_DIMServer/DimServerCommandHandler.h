#pragma once
#include "StdAfx.h"
#include ".\PoolingOperationControl.h"
#include "..\BitManager\BitsManage.h"
#include ".\DimServerServiceHandler.h"
#include "../HSConfig/SPDConfig.h"
#include "../HSConfig/DigitalPilot.h"



//!Tol level class who to manages all DIM commands.
/*!	It works basically as the top level class containing instance of all main subclasses and managing commands. 
	It uses the commandHandler method inherit from DimServer to get the data from PVSS.
*/
class DimServerCommandHandler : public DimServer , public ErrorHandler{
 protected:

	 //!integer dim command for the analisys tool channel number (this can be removed)
    DimCommand * cmdSetRootChNumber;
	//!integer dim command to receive the channel number for PVSS operation
	DimCommand * cmdSetChNumber;
	//!string dim commad to handle the commands received from PVSS
	DimCommand * cmdRxCommand;
	//!integer array (32 bit) dim command to receive the data for the commands 
	DimCommand * cmdDataIn;
	//!integer (32 bit) dim command for offset (not really used either)
	DimCommand * cmdSetOffset;
	
	/*! The internal member storing the jam player executable filename
	The fed server will call this file as an external executable on the program router command*/
	string jamPlayerExecutable;

	//! keeps the detector side 0 = A 1 = C
    UInt32  DetSide;
	//! Dim service handler to update the data to PVSS 
	DimServerServiceHandler * SRV;
	//! takes care of all pooling operations
    PoolingOperationControl * POOL;
	//! SPD config containing halfstaves and routers
    SPDConfig * SPD;
	//! counts the number of commands
    UInt32 commNumb;
	//! keeps the analisys tool channel number
    Int8 RootChNumber; 
	//! keeps the channel number from the dim command
    Int8 ChNumber;
    //! keeps the offset (a VME thing) its now deprecated
	UInt32 Offset;
	//! keeps the size of the data array
	UInt32 dataSize;

	//! keeps the data in array, this is deleted and reallocated in the receiving of the DimCommand  cmdDataIn
	UInt32 * DataIn;
	//! Data out array
	UInt32 * DataOut;
	//! data for the analisys tool deprecated
	UInt32 * DataStream;
	//! Command name input string 
	char * CommString;
	//! keeps an copy of the command name (now its here only for historical reasons)
	char * CommStringOriginal;	 
	
	//! Callback method to handle all DIM commands
	void commandHandler();
	//! logger instance
	spdLogger *logger;

	/*!Variable to store the run number of a command*/
	unsigned runNumber;
	
	/*@will get the VME adress of a router register from the command string received
	@param command 
	@return the corresponding VME address returns -1 (0xffffffff) if the command is unkown*/
	UInt32 getRouterAddressFromCommand(const char * command);
	//! gets VME the address of a LRX register according to command string
	UInt32 getLrxAdressFromCommand(const char * command);
	//! gets VME the address of the busy box acording to command string
	UInt32 getBusyAddressFromCommand(const char * command);

	/*! method to be used when the number of arguments is not correct
	it will update all the DIM services informing the PVSS side of the error*/
	void argumentError();

		//UInt32 GetDataSize(void){return dataSize;};
	//!comand handler for register access in the router
	int RegisterCmdHandlerRt(void);
	//!command handler for router access in the link receiver 
	int RegisterCmdHandlerLRx(void);
	//! command handler for register access in the busy card
	int RegisterCmdHandlerBusy(void);
	//! command handler for initializing things, is this needed?!
	int InitCmdHandler(void);
	//! command handler for jtag access in the halfstaves
	int JTAGCmdHandler(void);
	//! command handler for configuyration issues (this is used for global router access also=
	int ConfCmdHandler(void);
	//! command handler for the SPD analisys tool
	int ROOTCmdHandler(void);
	//! command handler for halfstave configuration commands 
	int HSConfCmdHandler(void);
	//! scan command handler
	int ScanCmdHandler(void);

	/*!New command decoder function that will implement a new way of parsing strings
		this function will expect to receive a white space separated string
		first the command and then all the arguments ex: "command arg1 arg2 ... argn"
		the prefix for this command is "STR_"	*/
	int StringCmdHandler(void);
 public: 
  
	DimServerCommandHandler( char side);
	~DimServerCommandHandler(void);
    


	/*! Setter for the jamplayer external executable filename*/
	void setJamPlayerFilename(string filename);
	/*! makes all pooling operations in this loop*/
	int PoolingFunction(){return POOL->PoolingFunction() ;};
	/*! sends the heartbit to PVSS*/
	void SendHearthbit(){ SRV->SendHearthbit() ;};



};
