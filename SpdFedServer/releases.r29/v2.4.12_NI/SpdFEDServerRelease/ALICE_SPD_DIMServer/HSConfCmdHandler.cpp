#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"
#include "hscfg_cmd_def.h"
int DimServerCommandHandler::HSConfCmdHandler(void){

			// if this returns a error bigger than two 
	if(strncmp(CommString, "HSCNF_", 6) != 0) return 2;

			// if there is a scan running commands to the fed are not allowed
		if (this->SPD->isScanActive() == true){
			
			sprintf(OutMsgText, "ERROR: A scan is Active, please stop the scan first, command %s will not be executed", CommStringOriginal);
			SRV->UpdateErrorName(OutMsgText, 1);
			return 1;
		}

		// this will make by default to update the dim service for data out to send 0 bytes 
		// and the the operation successed 
	int dataOutSize = 0;
    status = 0;
	// configures one halfstave with the defaul values from the DB
	if (strcmp(CommString, HSCNF_CONFIGURE_DEF) ==0){

		if (this->dataSize < 4){
			this->argumentError();
			return 1;
		}

		dataOutSize = 1;
		delete[] DataOut;
		DataOut = new UInt32 [dataOutSize];

		bool routerReset	= DataIn[0]?true:false;
		bool digitalPilot	= DataIn[1]?true:false;
		bool analogPilot	= DataIn[2]?true:false;
		bool pixDacs		= DataIn[3]?true:false;

		
		//configures one HS
		DataOut[0]=SPD->getHS(ChNumber).configureDefault(routerReset,digitalPilot,analogPilot,pixDacs);
		if (DataOut[0] == 16){
			logger->log("(HSConfCmdHandle) channel %d is not active",ChNumber);
		}
		if ( DataOut[0]) status =1;
		
	}
	// configures the full detector with the default values from the DB
	else if (strcmp(CommString, HSCNF_CONFIGURE_DEF_ALL) ==0){
		

		if (this->dataSize < 4){
			this->argumentError();
			return 1;
		}

		dataOutSize = NHalfStaves;
		delete[] DataOut;
		DataOut = new UInt32 [dataOutSize];

		bool routerReset	= DataIn[0]?true:false;
		bool digitalPilot	= DataIn[1]?true:false;
		bool analogPilot	= DataIn[2]?true:false;
		bool pixdacs		= DataIn[3]?true:false;


		// configures all hs
		SPD->configureSPDDefault(DataOut, routerReset, digitalPilot, analogPilot,pixdacs); 		
	}
	// performs a jtag reset in one halfstave
	else if(strcmp(CommString, HSCNF_JTAG_RESET) ==0){
		status = SPD->getHS(ChNumber).jtagReset();
	}
	// sets the chips included in the JTAG chain
	else if(strcmp(CommString, HSCNF_CHIP_JTAG) ==0){
		UInt32 chipsInChain = 0;

		// creates a UInt32 setting to 1 the bits corresponding to chips in the jtag chain
		for (int i = 0; i < DataIn[0]; i++){
			spdGlobals::insertIntoInt32(chipsInChain, 1, i, 1);
		}
		logger->logToScr("chips in chain %x", chipsInChain);

		SPD->getHS(ChNumber).SetPixChipInChain(chipsInChain);
	}
	//************************************ analog pilot commands ******************************
	else if(strcmp(CommString, HSCNF_API__SETDAC ) == 0){

		if (this->dataSize < 6){
			this->argumentError();
			return 1;
		}
		
		
		UInt8 received[6];
		for (int dac = 0; dac < 6 ; dac ++){
			received[dac]=(UInt8)DataIn[dac];
		}					
	
		status = SPD->getHS(ChNumber).LoadHSApiDACCompare( received);   
	}
	// Configures the analog pilot
	else if(strcmp(CommString, HSCNF_API__SET_ONE_DAC ) == 0){
		if (this->dataSize < 2){
			this->argumentError();
			return 1;
		}
		
		status = SPD->getHS(ChNumber).setAnalogPilotDAC( DataIn[0], (UInt8) DataIn[1]);   
	}	
	// configures the analog pilot with the delfault values
	else if(strcmp(CommString, HSCNF_API__DACDEF) ==0){
		status = SPD->getHS(ChNumber).LoadHSApiDACCompare();
	}
	// read the analog pilot values
	else if(strcmp(CommString, HSCNF_API__READ) ==0){
		dataOutSize = 6;
		delete[] DataOut;
		DataOut = new UInt32 [dataOutSize];
		UInt8 * DACVect = new UInt8 [dataOutSize];
		status = SPD->getHS(ChNumber).ReadHSApiDAC( DACVect);

		if(status == 0){
			for(int i = 0; i< dataOutSize; ++i) DataOut[i] = DACVect[i];
		}

		delete[] DACVect;
    }
	// reads the analog pilot adcs
	else if(strcmp(CommString, HSCNF_API__READADC) ==0){
		dataOutSize = 17;
		delete[] DataOut;
		DataOut = new UInt32 [dataOutSize];
        status = SPD->getHS(ChNumber).ReadHSApiADC( DataOut);
		
	}
//*************************************** digital pilot commands *******************************************
	// writes the digital pilot configuration
	else if(strcmp(CommString, HSCNF_DPI_SET ) == 0){

		if (this->dataSize < 10){
			this->argumentError();
			return 1;
		}

		DigitalPilot DigPil;
		DigPil.SetDPIConfElements(DataIn);

		UInt32* DigData = DigPil.GetDPIConf();
		status = SPD->getHS(ChNumber).LoadHSDpiConfRegCompare( DigData);

	}
	//configures the digital pilot with the values from the database
	else if(strcmp(CommString, HSCNF_DPI_DEF) ==0){
		status = SPD->getHS(ChNumber).LoadHSDpiConfRegCompare();
	}
	// reads the analog pilot settings
	else if(strcmp(CommString, HSCNF_DPI_READ) ==0){
		dataOutSize = 15;
		UInt32 dpiRegisters[2]= {0,0};
        status = SPD->getHS(ChNumber).ReadHSDpiConfReg( dpiRegisters);
        if(status == 0){
			DigitalPilot DigPil;
			DigPil.SetDPIConf(dpiRegisters);
			if(DataOut != NULL) delete[] DataOut;
			DataOut= DigPil.GetDPIConfElements();
		}
		
    }
	// sets the analog pilot internal values
	else if(strcmp(CommString, HSCNF_DPI_INT_SET) ==0){

		if (this->dataSize < 20){
			this->argumentError();
			return 1;
		}

		dataOutSize = 0;
		UInt32 dpiRegisters[3]= {0,0,0};

		DigitalPilot DigPil;
		DigPil.SetDPIInternalElements(DataIn);

		UInt32* DigData = DigPil.GetDPIInternal();
		
        status = SPD->getHS(ChNumber).LoadHSDpiInternalReg( dpiRegisters, DigData);

        if(status == 0){
			dataOutSize = 20;
			DigPil.SetDPIInternal(dpiRegisters);
			if(DataOut != NULL) delete[] DataOut;
			DataOut= DigPil.GetDPIInternalElements();
		}
	}
	// sets the analog pilot internal values with the default ones (internal pilot reset)
	else if(strcmp(CommString, HSCNF_DPI_INT_DEF) ==0){
		dataOutSize = 0;
		UInt32 dpiRegisters[3]= {0,0,0};

        status = SPD->getHS(ChNumber).LoadHSDpiInternalReg( dpiRegisters);
        if(status == 0){
			dataOutSize = 20;
			DigitalPilot DigPil;
			DigPil.SetDPIInternal( dpiRegisters);

			if(DataOut != NULL) delete[] DataOut;
			DataOut= DigPil.GetDPIInternalElements();
		}

	}
	// performs an internal digital pilot reset in all halfstaves
	else if (strcmp(CommString, HSCNF_DPI_INT_DEF_ALL) ==0){
		status = SPD->resetInternalDPI_All();

	}
	// performs a data reset in all halfstaves
	else if (strcmp(CommString, HSCNF_DATA_RESET_ALL) ==0){
		status = SPD->DataReset_All();
	}
//****************************************** test pulse and masking commands*************************************
	//sets a test pulse or mask in the detector
	else if (strcmp(CommString, HSCNF_PIXMTX_SET ) == 0){

		if (this->dataSize < 6){
			this->argumentError();
			return 1;
		}

		UInt32 Size = dataSize;
        UInt32 * Matrix = new UInt32 [Size];
        for(UInt32 i= 0; i < Size; ++i) Matrix[i] = DataIn[i+ 3];
		status = SPD->getHS(ChNumber).LoadHSPixelMatrix(  DataIn[0], Matrix, DataIn[1]?true:false, DataIn[2]?true:false); 

		delete [] Matrix;
	}
	// masks the spd with the defaul values from the database
	else if  (strcmp(CommString, HSCNF_PIXMASK_DEF ) == 0){
		status = SPD->getHS(ChNumber).LoadHSPixelMatrixDefault(); 
	}
	else if (strcmp(CommString, HSCNF_PIX_UNMASK_ALL ) == 0){
		// this trick is to make it return always 0 or 1 and not 2,3..n 
		// because of the way we check the commands (if I return 2 it will recheck the string in the other command handlers)
		if (SPD->UnMaskAllPixels( ) !=0)status= 1;
	}

//************************************************** pixel dacs commands ************************************************		
	//sets the pixel dacs of one HS withthe default values
	else if(strcmp(CommString, HSCNF_PXDAC_DEFAULT ) == 0){
		status = SPD->getHS(ChNumber).LoadHSPixelAllDAC();
	}
	// sets the all pixel dacs of one halfstave (the input is an array with all dac values)
	else if(strcmp(CommString, HSCNF_PXDAC_ALL) ==0){

			// from PVSS I receive a UInt32, that needs to be resized to UInt8
		UInt8 DataInResized[numberOfChips*numberOfDacs];

		for (unsigned index = 0; index < 440; index++){
			DataInResized[index] = (UInt8)DataIn[index];
		}
		status = SPD->getHS(ChNumber).LoadHSPixelAllDAC(DataInResized);

	}
	// sets the one dac in one halfstave
	else if(strcmp(CommString, HSCNF_PXDAC_CH) ==0){

		if (this->dataSize < 3){
			this->argumentError();
			return 1;
		}
			
			// if I clean this warning makes the fed server crash, the only way is through an automatic conversion
		status = SPD->getHS(ChNumber).LoadHSPixelDAC(  DataIn[0], (UInt8) DataIn[1],  DataIn[2]);
	}
	// reads the pixel dacs
	else if(strcmp(CommString, HSCNF_READ_PXDAC) ==0){

		if (this->dataSize < 3){
			this->argumentError();
			return 1;
		}


		dataOutSize= 14;
		if(DataOut != NULL) delete [] DataOut;
		DataOut = new UInt32 [dataOutSize];
		for(int i =0; i< dataOutSize; ++i) DataOut[i] =0;
		status = SPD->getHS(ChNumber).ReadHSPixelDAC((UInt8) (UInt8) DataIn[0], DataOut+4);

		if(status == 0){					
            DataOut[0] = ChNumber;
			DataOut[1]=DataIn[0];
			DataOut[2]=DataIn[1];
			DataOut[3]=DataIn[2];
		}
	}

	 //**************************************** database commands ****************************************************
	// gets the data from the database into the FED server
	else if(strcmp(CommString, HSCNF_DB_GET ) == 0){

		if (this->dataSize < 1){
			this->argumentError();
			return 1;
		}

		status = SPD->HSData.GetFromDB( DataIn[0]);

			// it will send the current global version to PVSS
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0]= SPD->HSData.getGlobalDBVersionNumber();
    }
	// masks the detector with the values from the database temporary table
	else if(strcmp(CommString, HSCNF_DB_MASK ) == 0){

		if (this->dataSize < 1){
			this->argumentError();
			return 1;
		}
		
		status= SPD->HSData.maskDetectorFromDB(DataIn[0]);

			// it will send the current mask version to PVSS
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0] = SPD->HSData.getMaskDbVersion();
    }
	// masks one half-stave with the values from the database temporary table
	else if(strcmp(CommString, HSCNF_DB_MASK ) == 0){

		if (this->dataSize < 1){
			this->argumentError();
			return 1;
		}
		
		status= SPD->HSData.maskHalfStaveFromDB(DataIn[0], DataIn[1]);

			// it will send the current mask version to PVSS
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0] = SPD->HSData.getMaskDbVersion();
    }
	// writes the current configuration in the detector to the database
	else if(strcmp(CommString, HSCNF_DB_SET) ==0){
			// will store the current side configuration in the database
			// returns the version number
		
		status = SPD->HSData.SaveEnChannelsInDB();

		// will send the version number
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0]= SPD->HSData.getSideDBVersionNumber();;

	}
	// compares the values in the detector with the database values 
	else if(strcmp(CommString, HSCNF_DB_COMPARE) ==0){
		
		// will write the differences in the database for comparison
		status = SPD->writeConfigDiffTable();

	}
	else if(strcmp(CommString, HSCNF_DB_DIFF_COUNT) ==0){

		unsigned mcmDiff, dacDiff, noisyDiff;

				// will write fill the 3 integers inside the function
		SPD->countConfigDiff( mcmDiff,dacDiff,noisyDiff);

		delete []  DataOut;
			// will send 3 counters ( mcm differences, dac differences and noisy differences)
		dataOutSize =3;
		DataOut = new UInt32[3];
		DataOut[0] = mcmDiff;
		DataOut[1] = dacDiff;
		DataOut[2] = noisyDiff;
	}
	// links two side versions with one global version
	else if(strcmp(CommString, HSCNF_DB_LINK) ==0){
	
				// will link two database side versions in the DB
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
				// gets the current version number being used
		DataOut[0]= SPD->HSData.linkDbVersions(DataIn[0]);

	}
	// gets the side version of this fed server
	else if(strcmp(CommString, HSCNF_DB_GET_SIDE_VERSION) ==0){
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0]=SPD->HSData.getSideDBVersionNumber();
	}
	// gets the global version of this FED
	else if(strcmp(CommString, HSCNF_DB_GET_GLOBAL_VERSION) ==0){
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0]=SPD->HSData.getGlobalDBVersionNumber();
	}
	// gets the mask version of this fed
	else if(strcmp(CommString, HSCNF_DB_GET_MASK_VERSION) ==0){
		dataOutSize =1;
		delete []  DataOut;
		DataOut = new UInt32[1];
		DataOut[0]=SPD->HSData.getMaskDbVersion();
	}
	// writes the actual configuration of the detector to files
	else if(strcmp(CommString, HSCNF_ACTUALS_TO_FILE) ==0){
		SPD->HSData.dumpActualConfToFile();
	}
	// writes the defaul configuration of the detector to file
	else if(strcmp(CommString, HSCNF_DEFAULTS_TO_FILE) ==0){
		SPD->HSData.dumpDefaultConfToFile();
	}
//*************************************** conf. file commands *************************************************************	
	// gets the file name of the configuration onf one halfstave (only if files were used instead of the database)
	else if(strcmp(CommString, HSCNF_FILE_GETNAME) ==0){
		const char * File = SPD->getHS(ChNumber).GetFileName();
	}

	// reads the default data of one HS from file
	else if(strcmp(CommString, HSCNF_FILE_LOAD) ==0){

		char *filename = new char [dataSize+1];

		for(UInt32 i= 0; i < dataSize; i++){
			filename[i] = (char)DataIn[i];
		}
		filename[dataSize]  = NULL;

		status = SPD->getHS(ChNumber).LoadDefaultsFromFile(filename);

		delete [] filename;
	}

//*********************************************** default values to PVSS commands *********************************
	// sends the defaul values of the dacs to PVSS (I hope this is not used anymore)
	else if(strcmp(CommString, HSCNF_PIXEL_DAC) ==0){
		dataOutSize = 440;
		delete[] DataOut;
	    DataOut = new UInt32 [dataOutSize]; 
		for(int i =0; i < dataOutSize; i++){
			DataOut[i] = SPD->getHS(ChNumber).GetPixelDACVectDefault()[i];
		}
	}
	// sends the default analog pilot values to PVSS
	else if(strcmp(CommString, HSCNF_APIDAC) ==0){
		dataOutSize = 6;
		delete[] DataOut;
	    DataOut = new UInt32 [dataOutSize];
		for(int i =0; i < dataOutSize; i++){
			DataOut[i] = SPD->getHS(ChNumber).GetAPIdacVectDefault()[i];
		}
	}
	// sends the default digital pilot values to PVSS
	else if(strcmp(CommString, HSCNF_DPICONF) ==0){
		dataOutSize = 10;
		delete[] DataOut;
	    DataOut = SPD->getHS(ChNumber).GetDPIDefault()->GetDPIConfElements();
	}
	//sends the default GOL values to PVSS
	else if(strcmp(CommString, HSCNF_GOLCONF) ==0){
		dataOutSize = 1;
		delete[] DataOut;
	    DataOut = new UInt32 [dataOutSize];
        DataOut[0] = SPD->getHS(ChNumber).GetGOLConfVectDefault();
	}
	
//*************** the commands below here are only for default settings in the old configuration files **************
	//gets the noisy pixels table to PVSS
	else if(strcmp(CommString, HSCNF_NOISYPIX) ==0){
		dataOutSize = SPD->getHS(ChNumber).GetNoisyPixelNumberDefault() * 3;
		delete[] DataOut;
	    DataOut = new UInt32 [dataOutSize];
		int n = 0;
		for(int chip =0; chip < 10 ; chip++){
			for (unsigned i = 0 ; i < SPD->getHS(ChNumber).GetNoisyPixelNumberDefault(chip); i ++){
				DataOut[n]= chip;
				DataOut[n+1] = SPD->getHS(ChNumber).GetNoisyPixVectDefault(chip)[i].column;
				DataOut[n+2] = SPD->getHS(ChNumber).GetNoisyPixVectDefault(chip)[i].row;
				n +=3;
			}
		}
	}

//*************** configuration of the off-detector electronics **************
	// sets the fastor strobe length in all enabled halfstaves
	else if (strcmp(CommString, HSCNF_FO_STROBE_LENGTH_ALL) ==0){
		status = SPD->setFoStrobeLength_All(DataIn[0]);
	}
	// sets the timeout for ready events in all enabled halfstaves
	else if (strcmp(CommString, HSCNF_TIMEOUT_READY_EV_ALL) ==0){
		status = SPD->setTimeoutReadyEvent_All(DataIn[0]);
	}
	
	else{
		sprintf(OutMsgText, "ERROR: HSCONF command %s not recognized\n", CommStringOriginal);
		printf(OutMsgText);
		status = 1;
	}

	SRV->UpdateServices(ChNumber, CommStringOriginal, status, CheckStatus(CommStringOriginal,status, ChNumber),(unsigned int*)DataOut, dataOutSize);

    return status;
}