#pragma once

#include <stringstream>
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"

int DimServerCommandHandler::InitCmdHandler(void){
    if(strncmp(CommString, "INIT_", 5) != 0) return 2;
	
    VMEAccess & VME = VMEAccess::getInstance();
	if(strcmp(CommString, "INIT_VME") == 0){
		status = VME.RunResorceMenager();
		SRV->UpdateErrorName(CheckStatus("INIT_VME", status, 120), (int)status);
	}

	else{
		sprintf(OutMsgText, "ERROR: INIT command %s not ricognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}

	return 0; 
}

