#pragma once

#include "../HSConfig/SPDConfig.h"
#include ".\DimServerServiceHandler.h"


#include "../spdDbConfLib/spdLogger.h"

//! Class control the pooling operations*/
class PoolingOperationControl{	
	//! also has a pointer to the service hand;er
	DimServerServiceHandler * SRV;
	//! as a pointer to the SPd config object
	SPDConfig  * SPD;
	//! a logger object
	spdLogger *log;
    //! refresh time in ms 
	unsigned TempRefreshTime;     //number of ms

	//! boolean variable stating if the temperature polling is operational
	bool TemperaturePoolActive;
	//! boolean variable stating if the router error handling is active or not
	bool routerErrorPoolActive;
    //! keeps the last time the temperature was read 
	UInt32 TempStartTime;

	//!variable storing the time when the last time there was a router error read
	UInt32 RouterErrorTime;

	//! variable storing the refresh time, the time in ms that we want to wait between erading of the router errors 
	UInt32 RouterRefreshTime;

	//! keeps the current run number, mainly for  the router error handler
	unsigned runNumber;
	
public:


	//! function to read the router control register 
	UInt32 readControlReg();

	//! method to read all routers get errors and store them in the database
	void readRouterErrors();
	//! setter for the runNumber intenrl member
	void setRunNumber(unsigned value){runNumber = value;};
	//! getter for the runNumber intenrl member
	unsigned getRunNumber(){return runNumber;};


	PoolingOperationControl(SPDConfig *, DimServerServiceHandler *);
	~PoolingOperationControl(void);
    
	//! Setter for the temperature refresh time interval
    void SetTempRefreshTime(UInt32 value){TempRefreshTime = value;};
	
	//! sets the refresh time for the router error pooling
	void setRouterErrorRefreshTime(UInt32 value){RouterRefreshTime = value;};


	/*!Starts the router error pooling*/
	void startRouterErrorPooling(unsigned refreshTime = 10000);

	/*!Stops the router error pooloing*/
	void stopRouterErrorPooling(){routerErrorPoolActive = false;};

	//! inserts one router error in the database
	void insertRouterErrorInTheDB(SpdRouter::RouterError error, unsigned  routerNumber);

	//! Method to start the HS temperature polling
	void startHSTempPool(unsigned refreshTime = 1000){
		TempRefreshTime=refreshTime;
		TemperaturePoolActive = true;
		TempStartTime = GetTickCount();
	};
	//! stops the half stave temperature polling 
	void stopHSTempPool(){TemperaturePoolActive=false;};
	//! Returns if the temperature pooling is active or not 
    bool isTempPoolActive(void){return TemperaturePoolActive;};

	//! Functoin to perform all pooling operations 
	UInt32 PoolingFunction(void);


};
