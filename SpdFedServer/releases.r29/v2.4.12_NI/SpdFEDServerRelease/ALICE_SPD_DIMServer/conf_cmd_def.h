#define CNF_CHSTATUS_ALL			"CNF_CHSTATUS_ALL"			//!< command to refresh the status of all half staves @param array with 120 values
#define CNF_CHSTATUS_CH				"CNF_CHSTATUS_CH"			//!< command to refresh the status of one half stave	 @param value 0: off, 1 on, 2 test

#define CNF_PL_TEMPERATURE_ON		"CNF_PL_TEMPERATURE_ON"		//!< command to start pooling the temperature @param refresh time (ms)
#define CNF_PL_TEMPERATURE_OFF		"CNF_PL_TEMPERATURE_OFF"	//!< command to stop the ooling of the temperature


#define CNF_MCMSTIMULI_ON			"CNF_MCMSTIMULI_ON"			//!< command to set the mcm stimuli on in one channel  @param channelNumber \param columnToSet
#define CNF_MCMSTIMULI_OFF			"CNF_MCMSTIMULI_OFF"		//!< command to clear the mcm stimuli in one channel  @param channelNumber \param columnToSet
#define CNF_MCMSTIMULI_ON_ALL		"CNF_MCMSTIMULI_ON_ALL"		//!< command to set the mcm stimuli on  in all enabled channels @param columnToSet
#define CNF_MCMSTIMULI_OFF_ALL		"CNF_MCMSTIMULI_OFF_ALL"	//!< command to clear the mcm stimuli in all enabled channels @param  columnToSet


#define CNF_PL_RT_ERRORS_START		"CNF_PL_RT_ERRORS_START"    //!< Starts pooling the router memory for errors 
#define CNF_PL_RT_ERRORS_STOP		"CNF_PL_RT_ERRORS_STOP"     //!< Stops pooling the router memory for errors 

#define CNF_RUN_START				"CNF_RUN_START"				//!< start of run commmand to initialize the run number variable
#define CNF_RUN_STOP				"CNF_RUN_STOP"				//!< Stop of run variable to clean the run number variable
#define CNF_READ_ROUTER_ERRORS		"CNF_READ_ROUTER_ERRORS"     //!< Reads the error list in one router
#define CNF_AUTO_CONF_ROUTER		"CNF_AUTO_CONF_ROUTER"		//!< Auto configures the router \param options \param controlReg \param L0L1Time \param LrxL1FastorDelay0 \param LrxL1FastorDelay1 \param LrxL1FastorDelay2

#define CNF_READ_BUSY_ROUTER		"CNF_READ_BUSY_ROUTER"		//!< reads the router busy times and sends them all to PVSS, 0 = busy daq, 1= busy router, 2 = busy HS, 3 = busy trigger 

#define CNF_RESTART_LOG_FILE		"CNF_RESTART_LOG_FILE"		//!< Renames the old FED log file and restarts a clean one 

#define CNF_SET_SEB					"CNF_SET_SEB"				//!< command to set a router in single event buffer mode
#define CNF_SET_MEB					"CNF_SET_MEB"				//!< command to set a router in multi event buffer mode

#define CNF_SET_SEB_ALL				"CNF_SET_SEB_ALL"			//!< command to set all routers in single event buffer mode
#define CNF_SET_MEB_ALL				"CNF_SET_MEB_ALL"			//!< command to set all routers in multi event buffer mode

#define CNF_LRX_READ_REGISTER		"CNF_LRX_READ_REGISTER"		//!< Command to read an arbitrary register of one link receiver \param channel number \param lrx address \return value
#define CNF_LRX_WRITE_REGISTER		"CNF_LRX_WRITE_REGISTER"	//!< Command to write an arbitrary register of one link receiver \param channel number \param lrx address \param value to write

#define CNF_RT_READ_REGISTER		"CNF_RT_READ_REGISTER"		//!< Command to read an arbitrary register of one router \param channel number \param lrx address \return value
#define CNF_RT_WRITE_REGISTER		"CNF_RT_WRITE_REGISTER"		//!< Command to write an arbitrary register of one router \param channel number \param lrx address \param value to write

//******************************** scans commands *********************************


//! Starts a Uniformity matrix scan
/*! \param TriggerNumber \param StartRow \param EndRow \param MaskNoActivePixels*/
#define SCN_UNIFORMITY_START			"SCN_UNIFORMITY_START"			
#define SCN_UNIFORMITY_STOP				"SCN_UNIFORMITY_STOP"			//!< command to stop an active uniformity scan 
#define SCN_UNIFORMITY_RESTART			"SCN_UNIFORMITY_RESTART"		//! command to restart an active uniformity scan


//! Starts a fastor calibration scan 
/*! \param fopolMinimum \param fopolMaximum \param fopolStep \param convpolMinimum \param convpolMaximum \param convpolStep \param comprefMinimum \param comprefMaximum \param comprefStep \param cgpolMinimum \param cgpolMaximum \param cgpolStep \param prevthMinimum \param prevthMaximum \param prevthStep \param NumberOfTriggers \param Matrices*/
#define SCN_FO_SCAN_START		"SCN_FO_SCAN_START"		
#define SCN_FO_SCAN_STOP		"SCN_FO_SCAN_STOP"		//!< stops a fastor calibration scan 
#define SCN_FO_SCAN_TEST		"SCN_FO_SCAN_TEST"		//!< Used only to debug a fastor scan,  performs one single step of this scan


//! Starts a Mean threshold calibration scan 
/*! \param dacMin \param dacMax \param Steps \param startRow \param endRow \param rowStep \param triggerNumber*/
#define SCN_MEANTH_START		"SCN_MEANTH_START"
#define SCN_MEANTH_STOP			"SCN_MEANTH_STOP"		//!< stops a Mean threshold scan
#define SCN_MEANTH_RESTART		"SCN_MEANTH_RESTART"		//!< Restarts a stopped mean threshold calibration scan


//! Starts dac scan 
/*! \param TirggerNumber \param dacMin \param dacMax \param steps \param dacNumber \param InternalTrigger \param WaitTime*/
#define SCN_DAC_START			"SCN_DAC_START"
#define SCN_DAC_STOP			"SCN_DAC_STOP"		//!< stops a dac scan
#define SCN_DAC_RESTART			"SCN_DAC_STOP"		//!< Restarts a stopped dac scan


//! Starts a delay scan 
/*! \param TirggerNumber \param delayMin \param delayMax \param steps \param InternalTrigger \param WaitTime*/
#define SCN_DELAY_START			"SCN_DELAY_START"
#define SCN_DELAY_STOP			"SCN_DELAY_STOP"		//!< stops a delay scan
#define SCN_DELAY_RESTART		"SCN_DELAY_STOP"		//!< Restarts a stopped delay scan


//! Starts a minimum threshold delay scan
/*! \param TirggerNumber \param pre_vthMin \param pre_vthMax \param steps \param InternalTrigger \param WaitTime*/
#define SCN_MINTH_START			"SCN_MINTH_START"
#define SCN_MINTH_STOP			"SCN_MINTH_STOP"		//!< stops a minimum threshold scan
#define SCN_MINTH_RESTART		"SCN_MINTH_RESTART"		//!< Restarts a stopped minimum threshold scan

//! Starts a noise scan
/*! \param TirggerNumber \param InternalTrigger*/
#define SCN_NOISE				"SCN_NOISE"
#define SCN_NOISE_STOP			"SCN_NOISE_STOP"		//!< stops a noise scan
	
#define SCN_TEST_DPM			"SCN_TEST_DPM"			//!< command to test the parsing of events in the dual port memory 

//!Starts a multi event buffer scan 
/*!\param correctMEB \param l1Orl2n*/
#define SCN_MEB_START			"SCN_MEB_START"