#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"


DimServerCommandHandler::DimServerCommandHandler(   char Side){
	
	logger = & spdLogger::getInstance();


	 if( Side == 'A'){												 
		cmdSetRootChNumber = new DimCommand("spd_feDimServerA/ROOT_CHANNEL_NUMBER", "I:1", this);
		cmdSetChNumber = new DimCommand("spd_feDimServerA/CHANNEL_NUMBER", "I:1", this);
		cmdRxCommand = new DimCommand("spd_feDimServerA/COMMAND_NAME", "C", this);
		cmdDataIn = new DimCommand("spd_feDimServerA/DATA_IN", "I", this);
		cmdSetOffset = new DimCommand("spd_feDimServerA/OFFSET", "I:1", this);

		DetSide = 0;
	 } else {
		cmdSetRootChNumber = new DimCommand("spd_feDimServerC/ROOT_CHANNEL_NUMBER", "I:1", this);
		cmdSetChNumber = new DimCommand("spd_feDimServerC/CHANNEL_NUMBER", "I:1", this);
		cmdRxCommand = new DimCommand("spd_feDimServerC/COMMAND_NAME", "C", this);
		cmdDataIn = new DimCommand("spd_feDimServerC/DATA_IN", "I", this);
		cmdSetOffset = new DimCommand("spd_feDimServerC/OFFSET", "I:1", this);
	
		DetSide = 1;
	 }
   

    commNumb = 0;
	ChNumber = 0;
    
	Offset = 0;
	dataSize = 0;
	
   
	DataIn = NULL;
	DataOut = NULL;
    DataStream = NULL; 


	SPD = new SPDConfig( Side);

	SRV = new DimServerServiceHandler( Side);
	POOL = new PoolingOperationControl(SPD, SRV);

	
	jamPlayerExecutable = "C:\\jp_25\\VisualStudio\\JamPlayer25\\Debug\\JamPlayer25.exe";

	logger->logToFile("(DimServerCommandHandler::DimServerCommandHandler) started DimServerCommandHandler");
	

}


DimServerCommandHandler::~DimServerCommandHandler(void){

	delete cmdSetRootChNumber;
	delete cmdSetChNumber;
	delete cmdRxCommand;
	delete cmdDataIn;
	delete cmdSetOffset;
	delete SRV;
	delete POOL;

	if (DataIn != NULL) delete DataIn;
		
	if (DataOut != NULL) delete DataOut;
	if (DataStream != NULL) delete DataStream; 

	delete SPD;

}



void DimServerCommandHandler::commandHandler(void){

	if(getCommand() == cmdSetChNumber){
		ChNumber = cmdSetChNumber->getInt();
	}

	if(getCommand() == cmdSetRootChNumber){
		RootChNumber = cmdSetRootChNumber->getInt();
	}
	
	if(getCommand() == cmdSetOffset){
		Offset = cmdSetOffset->getInt();
	}

	if(getCommand() == cmdDataIn){
		UInt32 * Data = (UInt32 *)cmdDataIn->getData();  
        dataSize = Data[0];
        Data++;
		// deletes the old memory and realocates to the new one 
		delete[] DataIn;
		DataIn = new UInt32 [dataSize];
		for(UInt32 i = 0; i < dataSize; i++){
			DataIn[i] = Data[i];
		}
	}


	if(getCommand() == cmdRxCommand){

		// I have to do this stupid trick because a lot of panels 
		// use 120 to mean commands to boths FEDs
		// this potencially can lead to mistakes 
		if (ChNumber == 120){
			if (DetSide == 0)ChNumber = 0;
			else ChNumber = 60;
		}
		else if(DetSide == 0 && ChNumber >= 60 ){
			logger->log("ERROR: channel number has to be between 0 and 59 for side A");
			sprintf(OutMsgText, "ERROR: channel %d out of range", ChNumber);
			SRV->UpdateErrorName(OutMsgText, 1);
			return;
		}
		else if(DetSide == 1 && (ChNumber < 60 || ChNumber >= 120)){
			logger->log("ERROR: channel number has to be between 60 and 119 for side C");
			sprintf(OutMsgText, "ERROR: channel %d out of range", ChNumber);
			SRV->UpdateErrorName(OutMsgText, 1);
			return;
		}
		

		CommString = cmdRxCommand->getString();
        CommStringOriginal = CommString;

		logger->logToFile(" INFO:Received command %s, channel = %d", CommString, ChNumber);
	
		commNumb++;
		


		int err = InitCmdHandler();	
		if (err <= 1) return;
	    err =ConfCmdHandler();
		if (err <= 1) return;
		err = RegisterCmdHandlerRt();
		if (err <= 1) return;
	    err = RegisterCmdHandlerLRx();
		if (err <= 1) return;
        err = RegisterCmdHandlerBusy();
		if (err <= 1) return;
		err = JTAGCmdHandler();
		if (err <= 1) return;
		//err = ROOTCmdHandler();
		//if (err <= 1) return;
		err = HSConfCmdHandler();
		if (err <= 1) return;
		err = ScanCmdHandler();
		if (err <= 1) return;
		err = StringCmdHandler();
		if (err <= 1) return;

		sprintf(OutMsgText, "ERROR: Command %s doesn't exist", CommStringOriginal);
		SRV->UpdateErrorName(OutMsgText, 1);
		
	}

}

void DimServerCommandHandler::setJamPlayerFilename(string filename){
	
	this->jamPlayerExecutable = filename;
	//logger->log( "(DimServerCommandHandler::setJamPlayerFilename) jamplayer executable = '%s'", jamPlayerExecutable.c_str() );
}


void DimServerCommandHandler::argumentError(){
	ViStatus status = 1;
	logger->log("ERROR: wrong number of arguments command will not be executed");
	
	SRV->UpdateServices(ChNumber, 
						CommString, status, 
						CheckStatus(CommString,status, ChNumber),
						(unsigned int*)DataOut, 
						0);
}