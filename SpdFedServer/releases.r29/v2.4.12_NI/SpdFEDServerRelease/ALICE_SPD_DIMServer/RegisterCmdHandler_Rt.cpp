#pragma once
#include "StdAfx.h"
#include "./DimServerCommandHandler.h"
#include "./register_cmd_def.h"


int DimServerCommandHandler::RegisterCmdHandlerRt(void){ 
	
	


	if(!(strncmp(CommString, "R_RT_", 5)== 0) && !(strncmp(CommString, "W_RT_", 5)==0)){
		return 2;
	}
	
	UInt32 BaseAddr; 
	UInt32 Aux =0;
	char RdWrSelector = CommString[0];
	CommString += 2;
	
	VMEAccess & VME = VMEAccess::getInstance();

	//Router Commands
	//-------------------------------------------------------------
	BaseAddr=getRouterAddressFromCommand(CommString);

	// -1 (0xffffffff) marks that the command is unkown
	if (BaseAddr == -1){	
		sprintf(OutMsgText, "ERROR: Router command %s not ricognized\n", CommStringOriginal);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}

	//BaseAddr += Offset;                //add protection: if it is register Offsett must be 0
	
	if(RdWrSelector == 'R'){
		dataSize = 1;
		delete[] DataOut;
		DataOut = new UInt32 [dataSize];

		status = VME.VMEReadRegister( BaseAddr, DataOut);
		logger->logToScr(" read Address %x, data out: %x in router %d", BaseAddr, DataOut[0], ChNumber/6);

		SRV->UpdateServices( ChNumber, CommStringOriginal, (int)status, CheckStatus(CommStringOriginal, status, ChNumber),(unsigned int*)DataOut, dataSize);
    }
	else if (RdWrSelector == 'W'){ 	
		UInt32 originalMask = 0;

		if (DataIn == NULL){
			logger->log("ERROR::(RegisterCmdHandlerRt) register input is NULL");
			status = 1;
			SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), (int)status);
			return 0;
		}

		// on a router reset or a link receiver reset we should stop the error handler 
		if (strcmp(CommString, RT_VMERESET) == 0 || strcmp(CommString, RT_RESET_LRX) == 0 ){
			originalMask = this->SPD->getRouter( ChNumber/6).readErrorMask();
			logger->logToScr("Stopping error handling in router %d", ChNumber/6);
			this->SPD->getRouter( ChNumber/6).writeErrorMask( 0);
		}

		logger->logToScr(" writing in Address %x, data in: %x in router %d", BaseAddr, DataIn[0], ChNumber/6);
		status = VME.VMEWriteRegister( BaseAddr, DataIn[0]);
		

		// now restartin the error handling
		if (strcmp(CommString, RT_VMERESET) == 0 || strcmp(CommString, RT_RESET_LRX) == 0 ){
			logger->logToScr("re-writting error mask in router %d, mask=%x", ChNumber/6, originalMask);
			this->SPD->getRouter( ChNumber/6).writeErrorMask( originalMask);

			//will reset all jtag controllers
			// problem with the router reset
			this->SPD->getRouter( ChNumber/6).resetJtagcontrollers();
		}

		SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), (int)status);
	}
	else{
		status =1;
		logger->log("ERROR::(RegisterCmdHandlerRt) first character as to be 'W' or 'R'");
		SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), (int)status);
	}
	
	return 0; 
}


UInt32 DimServerCommandHandler::getRouterAddressFromCommand(const char * CommString){
	UInt32 BaseAddr; 
	AddressGenerator & addresses = AddressGenerator::getInstance();

	if (strcmp(CommString, RT_REGISTER) == 0){
		BaseAddr = DataIn[1];
	}
	else if(strcmp(CommString, RT_DPM) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetDPMBaseAddr();          	
	}else if(strcmp(CommString, RT_VERSION_NUMBER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRtFPGAVersion();
	}else if(strcmp(CommString, RT_SPM) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetSPMBaseAddr();
	}else if(strcmp(CommString, RT_LRX_MEM_BASEADDR) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetLinkRxBaseAddr();
	}else if(strcmp(CommString, RT_CONTROL) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetCntrRegAddr();
	}else if(strcmp(CommString, RT_STATUS1) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusReg1Addr();
	}else if(strcmp(CommString, RT_STATUS2) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusReg2Addr();
	}else if(strcmp(CommString, RT_STATUS3) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusReg3Addr();
    }else if(strcmp(CommString, RT_STATUS_LINKRX0) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 0);
	}else if(strcmp(CommString, RT_STATUS_LINKRX1) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 1);
	}else if(strcmp(CommString, RT_STATUS_LINKRX2) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 2);			
	}else if(strcmp(CommString, RT_STATUS_LINKRX3) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 3);			
	}else if(strcmp(CommString, RT_STATUS_LINKRX4) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 4);			
	}else if(strcmp(CommString, RT_STATUS_LINKRX5) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 5);			
	}else if(strcmp(CommString, RT_STATUS_LINKRX6) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 6);
	}else if(strcmp(CommString, RT_STATUS_LINKRX7) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 7);
	}else if(strcmp(CommString, RT_STATUS_LINKRX8) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 8);
	}else if(strcmp(CommString, RT_STATUS_LINKRX9) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 9);
	}else if(strcmp(CommString, RT_STATUS_LINKRX10) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 10);
	}else if(strcmp(CommString, RT_STATUS_LINKRX11) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 11);
	}else if(strcmp(CommString, RT_STATUS_LINKRX12) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 12);
	}else if(strcmp(CommString, RT_STATUS_LINKRX13) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 13);
	}else if(strcmp(CommString, RT_STATUS_LINKRX14) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 14);
	}else if(strcmp(CommString, RT_STATUS_LINKRX15) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegLinkRxAddr(ChNumber, 15);
	}else if(strcmp(CommString, RT_WRHISTO) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetWtHistogramAddr();
    }else if(strcmp(CommString, RT_RDHISTO) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdHistogramAddr();
	}else if(strcmp(CommString, RT_JPLAYER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJPlayerAddr();
	}else if(strcmp(CommString, RT_L0ID) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdL0IdAddr();
	}else if(strcmp(CommString, RT_L1ID) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdL1IdAddr();
	}else if(strcmp(CommString, RT_L2ID) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdL2aIdAddr();
	}else if(strcmp(CommString, RT_RSTBNC) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetBcntAddr();
	}else if(strcmp(CommString, RT_IRQPUSHB) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetIrqPushButtonAddr();
	}else if(strcmp(CommString, RT_RST_TEMPLIM) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetTempLimitAddr();
	}else if(strcmp(CommString, RT_FO_FROMVME) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFOFromVMEAddr();
	}else if(strcmp(CommString, RT_VMERESET) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRtVMEResetAddr();
    }else if(strcmp(CommString, RT_STATUS_JTAG_SELECT) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetStatusRegJTAGSelAddr();
	}else if(strcmp(CommString, RT_DATA_SELECT) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetDataRegSelAddr();
	}else if(strcmp(CommString, RT_SELECT) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRegSelAddr();
	}else if(strcmp(CommString, RT_DDL_STATUS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFeDDLStatusWordAddr();
	}else if(strcmp(CommString, RT_RESET_DETECTOR) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetDetectorAddr();
	}else if(strcmp(CommString, RT_RD_EV_START) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdStartAddr();
	}else if(strcmp(CommString, RT_EV_LENGTH_OF_BLOCK) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdLEnghtOfBlockAddr();
	}else if(strcmp(CommString, RT_RESET_TTC) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetTTCrxAddr();
	}else if(strcmp(CommString, RT_RESET_LRX) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetLinkRxAddr();
	}else if(strcmp(CommString, RT_RESET_HS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetHalfStaveAddr(ChNumber);
	}else if(strcmp(CommString, RT_SEND_TRIGGER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetSendTriggSeqAddr();
	}else if(strcmp(CommString, RT_FIFO_STARTADDR) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetDPMfifoStartAddr();
	}else if(strcmp(CommString, RT_FIFO_ENDADDR) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetDPMfifoEndAddr();
	}else if(strcmp(CommString, RT_FLUSH_DPM) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFlushDPMAddr();
	}else if(strcmp(CommString, RT_FO_NUMBER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdFONumbAddr();
	}else if(strcmp(CommString, RT_TEMPERATURE) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRdTempChAddr(ChNumber);
	}else if(strcmp(CommString, RT_JT_RESET_STMACHINE) == 0){
	    BaseAddr = addresses.getRoutAddr(ChNumber).GetJTResetStateMacAddr(ChNumber);      	
	}else if(strcmp(CommString, RT_JT_RESET_FIFO) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTResetFIFOsAddr(ChNumber);
	}else if(strcmp(CommString, RT_JT_RDWR_DATA) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTRdWrDataAddr(ChNumber);
	}else if(strcmp(CommString, RT_JT_EXEC_START) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTExStartAddr(ChNumber);
	}else if(strcmp(CommString, RT_JT_STATUS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTStatusRegAddr(ChNumber);
	}else if(strcmp(CommString, RT_JT_RESET_CH) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTResetChAddr(ChNumber);
	}else if(strcmp(CommString, RT_JT_RDEN_FIFOIN) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTRdEnFIFOin(ChNumber);
	}else if(strcmp(CommString, RT_JT_RDNUMB_FIFOIN) == 0){ 
		BaseAddr = addresses.getRoutAddr(ChNumber).GetJTRdNumbFIFOin(ChNumber);
	}else if(strcmp(CommString, RT_TEMPERATURE_LIMIT_MCM) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTempLimitMcmAddr(ChNumber);
	}else if(strcmp(CommString, RT_TEMPERATURE_LIMIT_BUS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTempLimitBusAddr(ChNumber);
	}else if(strcmp(CommString, RT_FO_GLOBAL_COUNTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFOGlobalCountAddr();
	}else if(strcmp(CommString, RT_FO_COINCIDENCE_COUNTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFOCoincedenceCountAddr();
    }else if(strcmp(CommString, RT_FO_TIME_COUNTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFOTimeCountAddr();
	}else if(strcmp(CommString, RT_FO_LINKRX_COUNTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetFOLinkRxCountAddr(ChNumber);
	}else if(strcmp(CommString, RT_SCOPE_SELECTOR0) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetScopeSelectorAddr( 0);
	}else if(strcmp(CommString, RT_SCOPE_SELECTOR1) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetScopeSelectorAddr( 1);
	}else if(strcmp(CommString, RT_SCOPE_SELECTOR2) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetScopeSelectorAddr( 2);
	}
	else if(strcmp(CommString, RT_ERROR_MASK) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetErrorMask();          	
	}
	else if (strcmp(CommString, RT_RESET_PIXEL) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetPixelAddr();
	}
	else if (strcmp(CommString, RT_TIME_L0L1) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTimeL0L1();
	}
	else if(strcmp(CommString, RT_DATA_RESET) == 0){
		//BaseAddr = addresses.getRoutAddr(ChNumber).GetDigPilotResetAddr();
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetPixelAddr();
	}
	else if (strcmp(CommString, RT_RXREADY) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetRxReadyAddr();
	}
	else if (strcmp(CommString, RT_L1_IN_FIFO) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetLrxL1InFifo();
	}
	else if (strcmp(CommString, RT_RESET_BUSYRESOLVER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetResetBusyresolverAddr();
	}
	else if (strcmp(CommString, RT_TIME_BUSY_DAQ) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTimeBusyDaqAddr();
	}
	else if (strcmp(CommString, RT_TIME_BUSY_ROUTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTimeBusyRouterAddr();
	}
	else if (strcmp(CommString, RT_TIME_BUSY_HS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTimeBusyHsAddr();
	}
	else if (strcmp(CommString, RT_TIME_BUSY_TRIGGERS_L1_FIFO) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTimeBusyTriggersL1FifoAddr();
	}
	else if (strcmp(CommString, RT_NUM_TRANS_BUSY_DAQ) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetNumTransBusyDaqAddr();
	}
	else if (strcmp(CommString, RT_NUM_TRANS_BUSY_ROUTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetNumTransBusyRouterAddr();
	}
	else if (strcmp(CommString, RT_NUM_TRANS_BUSY_HS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetNumTransBusyHsAddr();
	}
	else if (strcmp(CommString, RT_NUM_TRANS_BUSY_TRIGGERS_L1_FIFO) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetNumTransBusyTriggersL1FifoAddr();
	}
	//else if (strcmp(CommString, RT_MEM_COUNTERS) == 0){
	//	BaseAddr = addresses.getRoutAddr(ChNumber).GetMemCountersAddr();
	//}
	//else if (strcmp(CommString, RT_ADDRESS_MEM_COUNTERS_SELECTED) == 0){
	//	BaseAddr = addresses.getRoutAddr(ChNumber).GetAddressMemCountersSelectedAddr();
	//}
	else if (strcmp(CommString, RT_L0_COUNTER) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetL0CounterAddr();
	}
	else if (strcmp(CommString, RT_INTERLOCK_STATUS) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetInterlockStatus();
	}
	else if (strcmp(CommString, RT_BC_CLOCK_LOCK) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetClockPhaseFineDelay();
	}
	else if (strcmp(CommString, RT_ERROR_MANAGER_RESET) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetErrManagerReset();
	}
	else if (strcmp(CommString, RT_TP_L1_DELAY) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTpL1Delay();
	}
	else if (strcmp(CommString, RT_SEB_MEB_SET) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetSebMebSet();
	}
	else if (strcmp(CommString, RT_THRESHOLD_BUSY_TIME) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetThresholdBusyTime();
	}
	else if (strcmp(CommString, RT_L1_FINE_DELAY) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetL1FineDelay();
	}
	else if (strcmp(CommString, RT_TIMEOUT_READY_EVENT) == 0){
		BaseAddr = addresses.getRoutAddr(ChNumber).GetTimeoutReadyEvent();
	}
	else{	
		BaseAddr= -1;
	}

	return BaseAddr;

}