#pragma once

#include "PixelConfFile.h"
#include "spdVerTable.h"
#include "stdafx.h"



const unsigned nHalfstaves = 60 ;
const unsigned nChips = 10;

#define NOISY_TABLE "spd_noisypix"

//!Class to get the noisy pixel mask from the database
/*! This class will download the list of noisy pixels from the database, 
from the new table "spd_noisypix" include it into internal storage
*/
class NoisyPixelDB
{
	long int  version;
	char side;

	std::vector<PixelCoordinate> hsNoisyPixels[nHalfstaves][nChips];

	void getDbDataForHs( unsigned hs);
	//! internal method to get the last version available in the database
	/*!if in the method getFromDB the versionNumber is not defined it will default to -1
	then this function is called to get the lastest version from the database*/
	int getMaxVersion();

public:


	//!constructer with the side 
	NoisyPixelDB();
	~NoisyPixelDB(void);

	char getSide()const{return side;};

	//! returns the version number currently loaded in this object
	/*! if none is loaded it will return -1*/
	int getVersionNumber()const{return version;};
	//!gets the noisy pixel list from the database 
	/*!\param side the side of the noisy pixels we want if side=C then only get information for hs 60 and higher
	   \param versionNumber the masking version number to get from the DB. -1 means the maximum*/
	int getFromDb(char side, int versionNumber = -1);

	//! returns the corresponding with the noisy pixels for the corresponding hs and pixel chip
	/*! if the arguments are out of bound it will return an empty vector*/
	std::vector<PixelCoordinate> getHsNoisy (unsigned hs, unsigned pixel)const;

	//! Debug method which prints the internal members to astring 
	std::string inspect()const;

};
