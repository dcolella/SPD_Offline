#ifndef SPDVERTABLE_H
#define SPDVERTABLE_H


//using namespace std;


#include <occi.h>


using namespace oracle::occi;

//! Emun with the possible database types
enum spdDbTypes{
	spdNumeric,
	spdSTRING,
	spdFloat
};


//!This class is supposed to be able to manage a versioning table.
/*!	This class is supposed to be able to manage a versioning table.
	It needs to be initialized with the table name, versioning field and the field list.	*/
class SpdVerTable{

private:

	std::vector <std::string> fields;
	std::vector <spdDbTypes> types;

	std::string versionField;
	std::string tableName;

	long int version;
	
	

						// Insert query generation for string and integers only
						// the string one is the generic
	template <class num>
	std::string createInsertQuery(long int newVersion, const std::vector<num> &values);
	std::string createInsertQuery(long int newVersion, const std::vector<std::string> &values);

	template<class T>
	void getIntResults( ResultSet * rs, std::vector<T> &values);
	template<class T>
	void getUIntResults( ResultSet * rs, std::vector<T> &values);
	template<class T>
	void getFloatResults(ResultSet * rs, std::vector<T> &values);

	void getStringResults( ResultSet * rs, std::vector<std::string> &values);
	
	


public:
					// to get the next version number and to get the maximum version number
	long int getMaxVersionNumber();
	long int getNextVersionNumber();

					// creates the select query string
	std::string createSelectQuery();

						// gets values from the database 
						// the one with string will convert all values to string
						// the template num identifies a numeric value, later floats can be added
	template<class num>
	void getValues(std::vector<num> &values);
	void getValues(std::vector<std::string> &values);


						// update the values in the database
						// convert values to string to insert generic values
	long int update(const std::vector<unsigned long int> &values);
	long int update(const std::vector<long int> &values);
	long int update(const std::vector<float> &values);

	long int update(const std::vector<std::string> &values);

				// this does not work I don't know why
				// update call only template functions don't know why this does not work
	//template<class T>
	//long int update(const std::vector<T> &values);

	
						// template function to compare values with the data base				
	template <class T>
	bool compareWithDb(const std::vector<T> &values);

						// template function to create a new version
	template <class T>
	long int createNewVersion(const std::vector<T> &values);

						
						// set and get version number
	unsigned int getVersionNumber(){return version;};
	void setVersionNumber( long int newVersion){version=newVersion;};
	
					
	std::string getTableName(){return tableName;};
	std::string getVersionField (){return versionField;};
	std::vector<std::string> getFields(){	return fields;};
	std::vector<spdDbTypes> getTypes(){return types;};
	
				// initializer options, 
				// if you do not add the types the default will be spdNumeric
	void setTableSettings(const std::string &table,const std::string &versionField,const std::vector<std::string> &fields);
	void setTableSettings(const std::string &table,const std::string &versionField,const std::vector<std::string> &fields,const std::vector<spdDbTypes> &types);

	SpdVerTable(const std::string &table,const std::string &versionField,const std::vector<std::string> &fields);
	SpdVerTable(const std::string &table,const std::string &versionField,const std::vector<std::string> &fields,const std::vector<spdDbTypes> &types);

	SpdVerTable(void);
	~SpdVerTable(void);
};

#endif
