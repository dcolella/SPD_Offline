#include "StdAfx.h"
#include ".\noisypixeldb.h"

#include "stdafx.h"
#include "spdDbConnection.h"
#include "spdGlobals.h"
#include "spdLogger.h"

using namespace std;

NoisyPixelDB::NoisyPixelDB(){
	this->version = -1;

	side = 'A';
}

NoisyPixelDB::~NoisyPixelDB(void){
}

// gets the maximum version available for a noisy pixel mask
int NoisyPixelDB::getMaxVersion(){
Statement * stmt;
	stringstream command;

	spdDbConnection * conn = spdDbConnection::subscribe();
	spdLogger &logger = spdLogger::getInstance();

				// sends the commands
	int outVersion = 0;
	try{
		stmt = conn->createStatement();	
		
		command << "select max(version_number) from " << NOISY_TABLE ;
		ResultSet *rs =  stmt->executeQuery( command.str());
		
		if(rs->next()){
			outVersion = rs->getUInt(1);
		}
		else {
			logger.log("(NoisyPixelDB::getMaxVersion) the following query returned an empty resultset :");
			logger.log(command.str());
			throw std::runtime_error(" there is no data in the noisy pixels table");
		}
			
			
		stmt->closeResultSet (rs);
		conn->terminateStatement (stmt);
	}
	catch(exception &error) {
        logger.log(error.what());
		logger.log(command.str());
		throw error;
    }
	catch (...){
		throw std::runtime_error(" (NoisyPixelDB::getMaxVersion)  unknow exception");
	}

	return outVersion;
}


// gets the data from the database for all halfstaves
int NoisyPixelDB::getFromDb(char newSide, int versionNumber ){


	this->side = newSide;

	spdDbConnection * conn= spdDbConnection::subscribe();
	spdLogger &logger = spdLogger::getInstance();
	

	try {
		if (conn->isConnected() == false)conn->connect();
		
		if (versionNumber < 0){
			this->version = getMaxVersion();
		}
		else{
			this->version = versionNumber;
		}
		
		// will get all the data inside this
		for (unsigned hs = 0 ; hs < nHalfstaves ; hs ++){
			getDbDataForHs( hs);
		}
	}
	catch(exception &error) {
		// neat trick of creating a string just to pass it to the log and concatenate the exception
		logger.log( string("(NoisyPixelDB::getFromDb) ") + error.what());
        return -1;

    }
	catch(...){
		logger.log("(NoisyPixelDB::getFromDb) unknown exception");
		return -1;
	}

	return 0;
}

// gets the data from the database halfstave by halfstave 
void NoisyPixelDB::getDbDataForHs(unsigned hs){

	Statement * stmt;
	stringstream command;

	spdDbConnection * conn = spdDbConnection::subscribe();
	spdLogger &logger = spdLogger::getInstance();

				// sends the commands
	
	try{
			
		stmt = conn->createStatement();	
		int hsNumber = hs;
		
		if (this->side == 'a' || this->side == 'A')  hsNumber = hs;
		else  hsNumber = hs+60;

		
		command << "select chip, pix_column, pix_row "; 
		command << "from " << NOISY_TABLE << " where ";
		command << "version_number = " << this->version;
		command << " and halfstave = "<< hsNumber;

		ResultSet *rs =  stmt->executeQuery( command.str());
		
		
		unsigned chip;
		for ( chip = 0 ; chip < nChips ; chip ++){
			this->hsNoisyPixels[hs][chip].clear();
		}

		while(rs->next()){
			PixelCoordinate noisy;

			chip = rs->getUInt(1);
			noisy.column=rs->getUInt(2);
			noisy.row = rs->getUInt(3);


			if (chip > nChips){
				throw std::runtime_error(" (getDbDataForHs)chip number returned is not valid");
			}

			this->hsNoisyPixels[hs][chip].push_back( noisy);
		}
			
			
		stmt->closeResultSet (rs);
		conn->terminateStatement (stmt);
	}
	catch(exception &error) {
        logger.log(error.what());
		logger.log(command.str());
		throw error;
    }
	catch (...){
		throw std::runtime_error(" (getDbDataForHs) unknow exception");
	}

}

// gets a vector with the noisy pixels list for one halfstave, chip
std::vector<PixelCoordinate> NoisyPixelDB::getHsNoisy(unsigned hs, unsigned pixel)const{

	if (hs > nHalfstaves || pixel > nChips){
		vector<PixelCoordinate> empty(0);
		return empty;
	}
	else{
		return this->hsNoisyPixels[hs][pixel];
	}
}

//debugging method will print the contents of the object to a string
std::string NoisyPixelDB::inspect() const{

	stringstream out;
	out << "noisy pixel list, side " << side << ", version= " << version << endl;
	for (unsigned hs = 0 ; hs < nHalfstaves ; hs ++){
		unsigned halfstave;

		if(side=='A' || side =='a') halfstave = hs;
		else halfstave = hs+60;
		unsigned count= 0;

		// will count the total number of noisy pixels
		for(unsigned chip =0; chip < nChips ; chip ++){
			count += (unsigned)this->hsNoisyPixels[hs][chip].size();
		}
		if (count > 0 )out << "halfstave " << halfstave << endl;
		
		for(unsigned chip =0; chip < nChips ; chip ++){
	
			const vector< PixelCoordinate> &noisy = this->hsNoisyPixels[hs][chip];

			for (unsigned n = 0 ; n < noisy.size(); n++ ){
				out << "\t"<< chip << ": " << (int) noisy[n].column << ", " <<(int) noisy[n].row << endl;
			}
		}
	}

	return out.str();
}
