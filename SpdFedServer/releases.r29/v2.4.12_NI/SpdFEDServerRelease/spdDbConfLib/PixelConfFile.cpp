#include ".\pixelconffile.h"
#include "stdafx.h"
#include ".\spdGlobals.h"
#include "spdLogger.h"
#include "PixelConfFile.h"

using namespace std ;

//using namespace std::cout;
//using namespace std::ios;

PixelConfFile::PixelConfFile(void)
{
	DpiWaitBefRow=0;
	DpiSebMeb=0;
	DpiMaskChip=0;
	DpiEventNumber=0;
	DpiStrobeLenght=0;
	DpiHoldRow=0;
	DpiSkipMode=0;
	DpiTdo8Tdo9=0;
	DpiEnableCeSeq=0;
	DpiDataFormat=0;
	ApiDacRefHi=0;
	ApiDacRefMid=0;
	ApiGtlRefA=0;
	ApiGtlRefD=0;
	ApiAnTestHi=0;
	ApiAnTestLow=0;

}

PixelConfFile::~PixelConfFile(void)
{

}

Uint8 PixelConfFile::getDpiWaitBefRow()const{
	return DpiWaitBefRow;
}

void PixelConfFile::setDpiWaitBefRow(Uint8 item ){
	DpiWaitBefRow=0x7 & item;
}

Uint8 PixelConfFile::getDpiSebMeb()const{
	return DpiSebMeb;
}

void PixelConfFile::setDpiSebMeb(Uint8 item ){
	DpiSebMeb=0x1 & item;
}

Uint32 PixelConfFile::getDpiMaskChip()const{
	return 0x3ff & DpiMaskChip;
}

void PixelConfFile::setDpiMaskChip(Uint32  item ){
				
	DpiMaskChip= 0x3ff & item;
}

int PixelConfFile::getDpiEventNumber()const{
	return DpiEventNumber;
}

void PixelConfFile::setDpiEventNumber(Uint32 item ){
	DpiEventNumber=0x3ff & item;
}

Uint8 PixelConfFile::getDpiStrobeLenght()const{
		
	return DpiStrobeLenght;
}

void PixelConfFile::setDpiStrobeLenght(Uint8 item ){

	DpiStrobeLenght= 0xf & item;
}

bool PixelConfFile::getDpiHoldRow()const{
	return DpiHoldRow;
}

void PixelConfFile::setDpiHoldRow(bool item ){
	DpiHoldRow=0x1 & item;
}

Uint8 PixelConfFile::getDpiSkipMode()const{
	return DpiSkipMode;
}

void PixelConfFile::setDpiSkipMode(Uint8 item ){
	DpiSkipMode=0x3 & item ;
}

bool PixelConfFile::getDpiTdo8Tdo9()const{
	return DpiTdo8Tdo9;
}

void PixelConfFile::setDpiTdo8Tdo9(bool item ){
	DpiTdo8Tdo9= 0x1 & item;
}

bool PixelConfFile::getDpiEnableCeSeq()const{
	return DpiEnableCeSeq;
}

void PixelConfFile::setDpiEnableCeSeq(bool item ){
	DpiEnableCeSeq= 0x1 & item;
}

bool PixelConfFile::getDpiDataFormat()const{
	return DpiDataFormat;
}

void PixelConfFile::setDpiDataFormat(bool item ){
	DpiDataFormat=0x1 & item;
}

Uint8 PixelConfFile::getApiDacRefHi()const{
	return ApiDacRefHi;
}

void PixelConfFile::setApiDacRefHi(Uint8 item ){
	ApiDacRefHi= 0xff & item;
}

Uint8 PixelConfFile::getApiDacRefMid()const{
	return ApiDacRefMid;
}

void PixelConfFile::setApiDacRefMid(Uint8 item ){
	ApiDacRefMid= 0xff & item;
}

Uint8 PixelConfFile::getApiGtlRefA()const{
	return ApiGtlRefA;
}

void PixelConfFile::setApiGtlRefA(Uint8 item ){
	ApiGtlRefA= 0xff & item;
}

Uint8 PixelConfFile::getApiGtlRefD()const{
	return ApiGtlRefD;
}

void PixelConfFile::setApiGtlRefD(Uint8 item ){
	ApiGtlRefD= 0xff & item;
}

Uint8 PixelConfFile::getApiAnTestHi()const{
	return ApiAnTestHi;
}

void PixelConfFile::setApiAnTestHi(Uint8 item ){
	ApiAnTestHi=0xff & item;
}

Uint8 PixelConfFile::getApiAnTestLow()const{
	return ApiAnTestLow;
}

void PixelConfFile::setApiAnTestLow(Uint8 item ){
	ApiAnTestLow=0xff & item;
}

Uint32 PixelConfFile::getGolConfig()const{
	Uint32 golConf;

	golConf =	getGolConfig(0);
	golConf +=	0x100*getGolConfig(1);
	golConf +=	0x10000*getGolConfig(2);
	golConf +=	0x1000000*getGolConfig(3); 

	return golConf;
}



Uint8 PixelConfFile::getGolConfig(int pos)const{
	return GolConfig[pos];
}

void PixelConfFile::setGolConfig(int pos, Uint8 item ){
	GolConfig[pos]= 0xff & item;
}


void PixelConfFile::setGolConfig(Uint32 item){
	for (int i = 0 ; i < NGolConfig ; ++i){
		setGolConfig(i,(Uint8) spdGlobals::extractFromInt32(item,8*i,8));
	}
}
Uint8 PixelConfFile::getPixelDac(int pixel, int dac )const{
	return PixelDac[pixel][dac];
}

void PixelConfFile::setPixelDac(int pixel, int dac ,Uint8 item ){
	PixelDac[pixel][dac]=0xff & item;
}



PixelConfFile::openFile( const char * filename){

	int n, i;
	int inbyte[4];
	
	FILE *infile;

	Uint8 ApiVector[6];
	unsigned int DipVector[2];
	float ladderIV[10], testCond[4];
	
	infile= fopen (filename, "rb" );

	if (infile == NULL){
		
		std::string msg = "Could not open file ";
		msg += filename;
		spdLogger::getInstance().log( msg);
		return -1;
	}


	fseek( infile, 0, SEEK_SET);
	for (n = 0 ; n < 200 ; n ++) fgetc( infile);

	// Test conditions
	for ( n = 0 ; n < 4 ; n ++)	{
		for (i = 0 ; i < 4 ; i ++)inbyte[i]=fgetc(infile);
		testCond[n] = (float) (inbyte[0]*0x1000000 + inbyte[1]*0x10000  + inbyte[2]*100 + inbyte[3])/100;		
	}

	//Ladder IV
	for ( n = 0 ; n < 10 ; n ++){
		for (i = 0 ; i < 4 ; i ++)inbyte[i]=fgetc(infile);
		ladderIV[n] =(float) inbyte[3]*0x1000000 + inbyte[2]*0x10000  + inbyte[1]*100 + inbyte[0];
	}

	//PixDacVect
	for(int  pixel = 0 ; pixel < 10 ; pixel ++) {
		for (int dac = 0 ; dac < 44 ; dac ++){
			PixelDac[pixel][dac]=fgetc( infile);
		}
	}
	
	//ApiVector
	for( n = 0 ; n < 6 ; n ++){
		ApiVector[n]=fgetc( infile);
	}
	
	
	//DipVector
	for( n = 0 ; n < 2 ; n ++){
		for (i = 0 ; i < 4 ; i ++)inbyte[i]=fgetc(infile);
		DipVector[n] =(unsigned int) inbyte[3]*0x1000000 + inbyte[2]*0x10000  + inbyte[1]*100 + inbyte[0];
	}
		
	//GolVect
	for( n = 0 ; n < NGolConfig ; n ++){
		GolConfig[n]=fgetc( infile);
	}
	
	

	
	
	DpiWaitBefRow	=  (Uint8) spdGlobals::extractFromInt32( DipVector[0], 0,3);
	DpiSebMeb		= (Uint8) spdGlobals::extractFromInt32(DipVector[0],3,1);

					// this is just to remove the warning: 
					//warning C4800: 'unsigned long' : forcing value to bool 'true' or 'false' (performance warning)
	DpiMaskChip	 = spdGlobals::extractFromInt32( DipVector[0], 4,10) ? true : false;
		
	DpiEventNumber	= spdGlobals::extractFromInt32( DipVector[0], 14,10);
	DpiStrobeLenght = (Uint8) spdGlobals::extractFromInt32(DipVector[0], 24,4);
	DpiHoldRow		= spdGlobals::extractFromInt32( DipVector[0], 28,1)? true : false;
	DpiSkipMode		= (Uint8) spdGlobals::extractFromInt32( DipVector[0], 29,2);
	DpiTdo8Tdo9		=spdGlobals::extractFromInt32( DipVector[0], 31,1)? true : false;
	DpiEnableCeSeq	= spdGlobals::extractFromInt32( DipVector[1], 0, 1)? true : false;
	DpiDataFormat	= spdGlobals::extractFromInt32( DipVector[1], 1, 1)? true : false;

	
	ApiDacRefHi		= ApiVector[0];
	ApiDacRefMid	= ApiVector[1];
	ApiGtlRefA		= ApiVector[2];
	ApiGtlRefD		= ApiVector[3];
	ApiAnTestHi		= ApiVector[4];
	ApiAnTestLow	= ApiVector[5];
	

	fclose(infile);
	return 0;
}

void PixelConfFile::printData()const{

	printf(" DpiWaitBefRow - %x\n",DpiWaitBefRow);
	printf(" DpiSebMeb - %x\n",DpiSebMeb);
	printf(" DpiMaskChip - %x\n",DpiMaskChip);
	printf(" DpiEventNumber - %x\n",DpiEventNumber);
	printf(" DpiStrobeLenght - %x\n",DpiStrobeLenght);
	printf(" DpiHoldRow - %x\n",DpiHoldRow);
	printf(" DpiSkipMode - %x\n",DpiSkipMode);
	printf(" DpiTdo8Tdo9 - %x\n",DpiTdo8Tdo9);
	printf(" DpiEnableCeSeq - %x\n",DpiEnableCeSeq);
	printf(" DpiDataFormat - %x\n",DpiDataFormat);
	printf(" ApiDacRefHi - %x\n",ApiDacRefHi);
	printf(" ApiDacRefMid - %x\n",ApiDacRefMid);
	printf(" ApiGtlRefA - %x\n", ApiGtlRefA);
	printf(" ApiGtlRefD - %x\n", ApiGtlRefD);
	printf(" ApiAnTestHi - %x\n",ApiAnTestHi);
	printf(" ApiAnTestLow - %x\n",ApiAnTestLow);
/*
	printf(" noisyCount - %x\n",noisyCount);
	printf(" deadCount - %x\n",deadCount);*/	

}

void PixelConfFile::dumpToText(const char *filename, int channel)const{
	ofstream outfile;
	
	outfile.open( filename, ios::out);

	if (!outfile.is_open()){

		std::string msg = "Could not open file ";
		msg += filename;
		spdLogger::getInstance().log(msg);
		return ;
	}
		
		// will write the router and the hs acording to the one needed by the calibration
	int router = channel /6;
	int hs = channel%6;

	outfile << "#******* dump of database object  ******************\n";
	outfile << "channel = " << channel<< " -> router " << router << ", hs " << hs << endl;
	outfile << "\n[DigitalPilot]\n";

	outfile <<"DpiWaitBefRow = " << (int) (DpiWaitBefRow & 0xff)<< "\n";
	outfile <<"DpiSebMeb = " << (int) (DpiSebMeb & 0xff)<< "\n";
	outfile <<"DpiMaskChip = " << hex <<(int) DpiMaskChip << "\n";
	outfile <<"DpiEventNumber = " << dec <<(int) DpiEventNumber << "\n";
	outfile <<"DpiStrobeLenght = " << (int) DpiStrobeLenght << "\n";
	outfile <<"DpiHoldRow = " << (int) DpiHoldRow << "\n";
	outfile <<"DpiSkipMode = " << (int) DpiSkipMode << "\n";
	outfile <<"DpiTdo8Tdo9 = " << (int) DpiTdo8Tdo9 << "\n";
	outfile <<"DpiEnableCeSeq = " << (int) DpiEnableCeSeq << "\n";
	outfile <<"DpiDataFormat = " << (int) DpiDataFormat << "\n";

	outfile << "\n[Analog Pilot]\n";
	outfile <<"ApiDacRefHi = " << (int) (ApiDacRefHi & 0xff)<< "\n";
	outfile <<"ApiDacRefMid = " << (int) (ApiDacRefMid & 0xff)<< "\n";
	outfile <<"ApiGtlRefA = " << (int) (ApiGtlRefA & 0xff)<< "\n";
	outfile <<"ApiGtlRefD = " << (int) (ApiGtlRefD & 0xff)<< "\n";
	outfile <<"ApiAnTestHi = " << (int) (ApiAnTestHi & 0xff) << "\n";
	outfile <<"ApiAnTestLow = " << (int) (ApiAnTestLow & 0xff)<< "\n";


	outfile << "\n";

	for (int i = 0 ; i < NGolConfig ; i ++){
		outfile << "GolConfig" << i << " = " << hex << (int) (GolConfig[i]& 0xff) << "\n";
	}
 
	outfile << dec;

	outfile <<  "\n[DACvalues]\n";
	
	for ( int dac = 0 ; dac < NPixelDacs ; dac ++){
		for (int pixel = 0 ; pixel < NPixels ; pixel ++){
			int aux =  (PixelDac[pixel][dac] & 0xff);
			outfile<< "\t" <<dec<< dac << ","<<router<<","<<hs <<","<<pixel<<"=" << aux << "\n";
		}
	}

	outfile <<  "\n[Noisy]\n";

	for (int pixel = 0 ; pixel < NPixels ; pixel ++){
		outfile << "-"<< router << "," << hs << "," << pixel << "\n";
		for ( unsigned noisy = 0 ; noisy < noisyPixels[pixel].size() ; noisy ++){
			PixelCoordinate aux =  noisyPixels[pixel][noisy];
			outfile<< "\t" <<dec<< (int)aux.column<< ","<<(int) aux.row << "\n";
		}
	}
}





int PixelConfFile::getDeadPixelCount(int pixel)const{

	return (int) deadPixels[pixel].size();
}

void PixelConfFile::getDeadPixel( int pixel, int pos, int &column, int &row)const{


	row = deadPixels[pixel][pos].row;
	column = deadPixels[pixel][pos].column;
}

void PixelConfFile::removeDeadPixel( int pixel, int pos){
	deadPixels[pixel].erase( deadPixels[pixel].begin()+pos);
}


void PixelConfFile::insertDeadPixel( int pixel, int column, int row){
	PixelCoordinate pDead;

	pDead.row= row;
	pDead.column=column;

	deadPixels[pixel].push_back( pDead);


}

int PixelConfFile::getNoisyPixelCount( int pixel)const{
	return (int) noisyPixels[pixel].size();

}

void PixelConfFile::getNoisyPixel( int pixel, int pos, int &column, int &row)const{
	
	column = noisyPixels[pixel][pos].column;
	row = noisyPixels[pixel][pos].row;
}

void PixelConfFile::removeNoisyPixel( int pixel, int pos){

	noisyPixels[pixel].erase( noisyPixels[pixel].begin()+pos);

}

void PixelConfFile::insertNoisyPixel( int pixel, int column, int row){
	
	PixelCoordinate pnoisy;

	pnoisy.row= row;
	pnoisy.column=column;

	noisyPixels[pixel].push_back( pnoisy);

}

void PixelConfFile::printNoisyAllPixels(){

	for(int i = 0; i < NPixels ; i ++){
		printNoisyInPixel(i);
	}
	
}

void PixelConfFile::printNoisyInPixel(int pixel){
	std::vector<PixelCoordinate>::iterator it;

	cout << (unsigned int) noisyPixels[pixel].size() << " noisy pixels in ";
	cout << "pixel" << pixel << ":";

	for( it = noisyPixels[pixel].begin() ; it < noisyPixels[pixel].end() ; it ++){
		cout << (int)it->column  << "," << (int) it->row<< "|";
	}

	cout << endl;
}


float PixelConfFile::getAnalogConvValue( int pos)const{
	return 0;
}

void PixelConfFile::setAnalogConvValue( int pos, float item){
}