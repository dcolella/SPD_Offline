#include "stdafx.h"
#include ".\pixelconfdb.h"
#include ".\spdGlobals.h"

int PixelConfDb::getDataFromDB(){

	getMcmDbValues();
	getNoisyPixDbValues();

	for (int pixel = 0; pixel < NPixels ;pixel ++){
		getPixDacDbValues(pixel);
	}
	
	return 0;
}

// gets noisy pictures from the database
int PixelConfDb::getNoisyPixDbValues(){

	std::vector<std::string>  values(20,"");

	noisyTable.getValues( values);

	for (int pixel = 0 ; pixel < NPixels; pixel ++){
		noisyPixels[pixel]= strToNoisy( values[2*pixel+1]);	
	}

	return 0;
}

// updates values with the database
long int PixelConfDb::update(){
	std::vector<long int> values(12);

	values[0]=updateMcmVer();
	
	for (int pixel = 0 ; pixel < NPixels ; pixel++){
		values[pixel+1]= updatePixDacVer(pixel);
	}

	values[NPixels +1]=updateNoisyPixVer();

	return hsTable.update( values);
	
}

//tranforms from noisy vector to string
std::string PixelConfDb::noisyToStr( int pixel){

	std::string out("#");
	for( unsigned int n=0; n < noisyPixels[pixel].size() ; n ++){
		out += spdGlobals::intToStr( noisyPixels[pixel][n].column ) + ",";
		out += spdGlobals::intToStr( noisyPixels[pixel][n].row) + "|";

	}
	
	return out;
}

// transforms from string to noisy
std::vector<PixelCoordinate> PixelConfDb::strToNoisy(const std::string &str){
	std::vector<PixelCoordinate> out;
	std::vector<std::string> pairs;


				// removes the first character
	std::string aux = str.substr(1,str.size());
	//pairs = spdGlobals::splitStr( str,"|");
	pairs = spdGlobals::splitStr( aux,"|");
	size_t size = pairs.size();

			// then removes all the pairs of values	
	for (unsigned int n = 0 ; n < size; n++){
		PixelCoordinate newValue;
		std::vector<std::string> values = spdGlobals::splitStr( pairs[n],",");
		if(values.size() >= 2){
			newValue.column =atoi( values[0].c_str());
			newValue.row =atoi( values[1].c_str());
			out.push_back( newValue);
		}
		
	}

	return out;

}

// updates the noisy table
long int PixelConfDb::updateNoisyPixVer(){
	// for the moment I need to change the type of column 
	// to acept different type of for the noisy pixels 
	
	std::vector<std::string> values(20,"");

	for (int pixel = 0 ; pixel < NPixels; pixel ++){
		values[2*pixel] = spdGlobals::intToStr(getNoisyPixelCount(pixel));
		values[2*pixel+1]=noisyToStr(pixel);
		
	}



	return noisyTable.update( values);
	
}

// function just to return the type of value of the noisyver table 
// this function can go to a global function in spd namespace
std::vector<spdDbTypes> PixelConfDb::getNoisyVerTypes(){
	std::vector<spdDbTypes> types(20);

	types[0]=spdNumeric;
	types[1]=spdSTRING;
	types[2]=spdNumeric;
	types[3]=spdSTRING;
	types[4]=spdNumeric;
	types[5]=spdSTRING;
	types[6]=spdNumeric;
	types[7]=spdSTRING;
	types[8]=spdNumeric;
	types[9]=spdSTRING;
	types[10]=spdNumeric;
	types[11]=spdSTRING;
	types[12]=spdNumeric;
	types[13]=spdSTRING;
	types[14]=spdNumeric;
	types[15]=spdSTRING;
	types[16]=spdNumeric;
	types[17]=spdSTRING;
	types[18]=spdNumeric;
	types[19]=spdSTRING;

	return types;

}

//gets dac values from the database
int PixelConfDb::getPixDacDbValues( int pixel){
	std::vector<long int> values(NPixelDacs,0);

	dacTable[pixel].getValues( values);
	

	for (int dac = 0 ; dac < NPixelDacs  ; dac++ ){
		setPixelDac( pixel,dac,(Uint8) values[dac]);
		
	}


	return 0;
}

// writes if needed pixel dac values to the database
long int PixelConfDb::updatePixDacVer(int pixel){

	std::vector<long int> values(NPixelDacs);

	for (int dac = 0 ; dac < NPixelDacs  ; dac++ ){
		values[dac]= (long int) getPixelDac( pixel, dac);
	}

	return dacTable[pixel].update( values);


}

// constructor of this class :)
PixelConfDb::PixelConfDb(void)
{


			// sets the settings of the tables
	mcmTable.setTableSettings( "MCM_VER2","MCM_VER", getMcmVerFields());

			// sets the settings of the noisy ver with the correct types also
	noisyTable.setTableSettings( "NOISYPIX_VER2", "NOISYPIX_VER", getNoisyVerFields(), getNoisyVerTypes());

	for (int pixel = 0; pixel < NPixels ; pixel ++){
		dacTable[pixel].setTableSettings( "PIXDAC_VER2","PIXDAC_VER", getPixDacVerFields());
	}

	hsTable.setTableSettings( "HS_VER2", "HS_VER", getHsVersionFields());


}

PixelConfDb::~PixelConfDb(void)
{

}


// sets the version of this configuration
void PixelConfDb::setVersionNumber(long int version){
	std::vector<long int> versions(12,0);
	
	hsTable.setVersionNumber( version);

	hsTable.getValues(versions);

	mcmTable.setVersionNumber( versions[0]);

	for (int pixel = 0 ; pixel < NPixels; pixel ++){
		dacTable[pixel].setVersionNumber( versions[pixel+1]);
	}
	
	noisyTable.setVersionNumber( versions[NPixels+1]);
	
	
}

// gets mcm values from the database
int PixelConfDb::getMcmDbValues(){

	std::vector<long int> values(20,0);

	mcmTable.getValues(values);

	setDpiWaitBefRow((Uint8) values[0]);
	setDpiSebMeb((Uint8) values[1]);
	setDpiMaskChip((Uint32) values[2]);			
	setDpiEventNumber((Uint32) values[3]);
	setDpiStrobeLenght((Uint8) values[4]);
	setDpiHoldRow(values[5]? true : false);		// the only way to cast from long to bool 
												// without the annoing warning
												// very interesting :)
	setDpiSkipMode((Uint8) values[6]);
	setDpiTdo8Tdo9(values[7]? true : false);
	setDpiEnableCeSeq(values[8]? true : false);
	setDpiDataFormat(values[9]? true : false);
	setApiDacRefHi((Uint8) values[10]);
	setApiDacRefMid((Uint8) values[11]);
	setApiGtlRefA((Uint8) values[12]);
	setApiGtlRefD((Uint8) values[13]);
	setApiAnTestHi((Uint8) values[14]);
	setApiAnTestLow((Uint8) values[15]);
	setGolConfig(0,(Uint8)values[16]);
	setGolConfig(1,(Uint8)values[17]);
	setGolConfig(2,(Uint8)values[18]);
	setGolConfig(3,(Uint8)values[19]);

	return 0;
}

/********************************************************************

Creates a new mcm configuration record

*********************************************************************/
long int PixelConfDb::updateMcmVer(){
	std::vector<long int> values(20);

				// gets the values from the members 
	values[0]=(long int)getDpiWaitBefRow();
	values[1]=(long int) getDpiSebMeb();
	values[2]=(long int) getDpiMaskChip();
	values[3]=(long int) getDpiEventNumber();
	values[4]=(long int) getDpiStrobeLenght();
	values[5]=(long int) getDpiHoldRow();
	values[6]=(long int) getDpiSkipMode();
	values[7]=(long int) getDpiTdo8Tdo9();
	values[8]=(long int) getDpiEnableCeSeq();
	values[9]=(long int) getDpiDataFormat();
	values[10]=(long int) getApiDacRefHi();
	values[11]=(long int) getApiDacRefMid();
	values[12]=(long int) getApiGtlRefA();
	values[13]=(long int) getApiGtlRefD();
	values[14]=(long int) getApiAnTestHi();
	values[15]=(long int) getApiAnTestLow();
	values[16]=(long int) getGolConfig(0);
	values[17]=(long int) getGolConfig(1);
	values[18]=(long int) getGolConfig(2);
	values[19]=(long int) getGolConfig(3);

	return mcmTable.update( values);
		
	


}


/************************************************************
Gets the fields for Noisy pixels table
*************************************************************/
std::vector <std::string> PixelConfDb::getNoisyVerFields(){
	std::vector<std::string> fields(20);

	fields[0]="NOISYCOUNT0";
	fields[1]="NOISYVECT0";
	fields[2]="NOISYCOUNT1";
	fields[3]="NOISYVECT1";
	fields[4]="NOISYCOUNT2";
	fields[5]="NOISYVECT2";
	fields[6]="NOISYCOUNT3";
	fields[7]="NOISYVECT3";
	fields[8]="NOISYCOUNT4";
	fields[9]="NOISYVECT4";
	fields[10]="NOISYCOUNT5";
	fields[11]="NOISYVECT5";
	fields[12]="NOISYCOUNT6";
	fields[13]="NOISYVECT6";
	fields[14]="NOISYCOUNT7";
	fields[15]="NOISYVECT7";
	fields[16]="NOISYCOUNT8";
	fields[17]="NOISYVECT8";
	fields[18]="NOISYCOUNT9";
	fields[19]="NOISYVECT9";

	return fields;


}

/************************************************************
	Gets the fields for the pixel dac version table
************************************************************/
std::vector <std::string> PixelConfDb::getPixDacVerFields(){

	std::vector<std::string> fields(44);

	fields[0]="DIS_BIASTH";
	fields[1]="DIS_VCASD4";
	fields[2]="DIS_VCASD21";
	fields[3]="DIS_VIBCOMP";
	fields[4]="DIS_VIBIASCARD";
	fields[5]="DIS_VIDISC";
	fields[6]="DIS_VREF2DIS";
	fields[7]="EU_VBIAS";
	fields[8]="EU_VBN";
	fields[9]="EU_VBNBUFFER";
	fields[10]="EU_VBNBUSLATCH";
	fields[11]="EU_VBNG";
	fields[12]="EU_VBNLHCB";
	fields[13]="EU_VBPPULLDOWN";
	fields[14]="FAST_CGPOL";
	fields[15]="FAST_CGPOLFM";
	fields[16]="FAST_COMPREF";
	fields[17]="FAST_CONVPOL";
	fields[18]="FAST_CONVPOLFM";
	fields[19]="FAST_FMPOL";
	fields[20]="FAST_FOPOL";
	fields[21]="KEN_EOCVBN";
	fields[22]="KEN_VBN";
	fields[23]="KEN_VBNM";
	fields[24]="KEN_VBNS";
	fields[25]="KEN_VBUFFVBN";
	fields[26]="PRE_VI1";
	fields[27]="PRE_VI2";
	fields[28]="PRE_VI3";
	fields[29]="PRE_VI4";
	fields[30]="PRE_VI5";
	fields[31]="PRE_VIFB";
	fields[32]="PRE_VIPREAMP";
	fields[33]="PRE_VREF1";
	fields[34]="PRE_VREF2";
	fields[35]="PRE_VREF3";
	fields[36]="PRE_VREF4";
	fields[37]="PRE_VREF5";
	fields[38]="PRE_VREF6";
	fields[39]="PRE_VTH";
	fields[40]="VAL_BUFFIN";
	fields[41]="VAL_BUFFOUT";
	fields[42]="DELAY_CONTROL";
	fields[43]="MISC_CONTROL";

	return fields;

}

/****************************************************

 Gets the fields for the mcm version table
******************************************************/
std::vector<std::string> PixelConfDb::getMcmVerFields(){
	std::vector<std::string> fields(20);

	fields[0] = "DPI_WaitBefRow";
	fields[1] = "DPI_SebMeb";
	fields[2] = "DPI_MaskChip";
	fields[3] = "DPI_EventNumber";
	fields[4] = "DPI_StrobeLenght";
	fields[5] = "DPI_HoldRow";
	fields[6] = "DPI_SkipMode";
	fields[7] = "DPI_Tdo8Tdo9";
	fields[8] = "DPI_EnableCeSeq";
	fields[9] = "DPI_DataFormat";
	fields[10] = "API_DacRefHi";
	fields[11] = "API_DacRefMid";
	fields[12] = "API_GtlRefA";
	fields[13] = "API_GtlRefD";
	fields[14] = "API_AnTestHi";
	fields[15] = "API_AnTestLow";
	fields[16] = "GOL_Config0";
	fields[17] = "GOL_Config1";
	fields[18] = "GOL_Config2";
	fields[19] = "GOL_Config3";

	return fields;
}


std::vector <std::string>  PixelConfDb::getHsVersionFields(){
	std::vector<std::string> fields(12);

	fields[0] = "MCM_VER";
	fields[1] = "DAC_VER0";
	fields[2] = "DAC_VER1";
	fields[3] = "DAC_VER2";
	fields[4] = "DAC_VER3";
	fields[5] = "DAC_VER4";
	fields[6] = "DAC_VER5";
	fields[7] = "DAC_VER6";
	fields[8] = "DAC_VER7";
	fields[9] = "DAC_VER8";
	fields[10] = "DAC_VER9";
	fields[11] = "NOISYPIX_VER";

	return fields;


}


