#ifndef SPDLOGGER_H
#define SPDLOGGER_H


#include "stdAfx.h"
#include <dis.hxx>


//using namespace std;

//! Spd logger class 
/*!its an simple class loggin class
its a singleton, can log to screen (cout), file and DIM
Keeps the handle of the file at all time.
*/
class spdLogger
{

protected:
	spdLogger(void);

private:
	//! String containing the file name
	std::string logFile;

	//! function to format time
	void formatTime(char *time);

	//! dim service logging channel
	DimService *logService;
	//! the log message
	char logMsg[2000];
	//! the handle for the output file
	std::ofstream outfile;

public:
	//! static function to get the only instance of the software
	static spdLogger &getInstance();
	//! changes the log file
	void setLogFile( std::string filename);
	//! returns the current file name 
	std::string getLogFile(){return logFile;};
	//! log to everything using a string as input
	int log( std::string msg);
	//! Logs to screen and to file but not to DIM
	int logToFile(std::string msg);
	//! logs to everything using a C type string
	int log (const char * format, ...);
	//! Logs to screen only, not to file nor to DIM
	int logToScr(const char * format, ...);
	//! Logs to DIM and screen, not to file 
	int logToDim(const char * format, ...);
	//! logs to file and to screen and not to DIM 
	int logToFile(const char * format, ...);

	//! deletes the logfile
	void deleteLogFile();

	//! set the DIM service log name
	void setDimServiceName(const char * serviceName);

	//! renames the old log file and restarts the a clean file
	/*! the old log file is remaned to:	[old filename].[counter].[day]_[month]_[year].log*/
	void restartLogFile();
	
	~spdLogger(void);
};
#endif