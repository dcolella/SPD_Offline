
#include "stdafx.h"
#include <iostream>

#include "../spdDbConfLib/spdIniParser.h"
#include "../spdDbConfLib/spdDbConnection.h"
#include "spdCalibParser.h"


const char iniFileName[] = "./settings.ini";
using namespace spdCalib;

// command list
const char cmdHelp[] ="-h";
const char cmdFileList[]="-f";
const char cmdAction[]="-a";
const char cmdVersion[]="-v";
const char cmdRunType[]="-r";



void printHelp(void){
	cout << "SPD calibration parser " << endl;
	cout << "Usage :\n";
	cout << "\t spdCalibParser -f [list in file] -a [action(0- 3)] [file1].. [fileN] \n";
	cout << "commands:\n";
	cout << "\t-h prints this help\n";
	cout << "\t-f  specifies a text file with a list of files to load\n";
	cout << "\t-v specifies a version to start the configuration from\n";
	cout << "\t-r specifies the run type for the new configuration\n";
	cout << "\t-a specifies the action to make\n";
	cout << "\t\t0-Creates new configuration only for the same run type \n\t\t   specified by [version]\n";
	cout << "\t\t1-Creates new configuration from version specified by [version]\n\t\t   but generates it for the new [runt type] \n";
	cout << "\t\t2-Applies DAC changes to all run types available\n";
	cout << "\t\t3-Applies all changes to all run types available\n";
}


int main(int argc, char* argv[])
{
	
	spdCalib::IniParser test;
	spdCalib::Parser parser;

					// gets the values from the ini file
	IniParser inifile(iniFileName);
	Category settings = inifile["DataBase"];
									// sets the settings in the connection to database object
	spdDbConnection * conn = spdDbConnection::subscribe();
	conn->setDbSettings(settings);
	

	cout << "got DB user name:"<< conn->getDbUserName().c_str() ;
	cout << ",DB connection string :"<< conn->getDbConstring().c_str() ;
	

	bool changeAction= false;
	bool changeVersion=false;
	bool changeRunType=false;

	int  runType=0;
	long action=0, version=-1;

	string command;
	
	
	if (argc <= 1){
		printHelp();
		return 0;
	}

	

		// will parse the list of arguments here
	for (int arg = 1; arg < argc ; ++arg ){
		
		
		if (argv[arg][0]=='-'){
			command=argv[arg];
		}
		else{
			if ( command ==""){
				parser.openFile(argv[arg]);
			}
			else if (command==cmdHelp){
				printHelp();
				return 0;
			}
			else if ( command == cmdAction){
				action = atoi( argv[arg]);
				parser.setDbOperationMode(action);
				
			}
			else if ( command == cmdFileList){
				parser.openFileList( argv[arg]);
			}
			else if ( command== cmdVersion){
				version = atol( argv[arg]);
				parser.setDbVersion(version);
				
			}
			else if ( command == cmdRunType){
				runType = atol( argv[arg]);
				parser.setDbRunType(runType);
				
			}
			else{
				cout << "command '" << command.c_str() << "' not known\n";
				printHelp();
				return 1;
			}
			command.clear();
		}	
		
	}

		//if (changeAction)parser.setDbOperationMode(action);
		//if (changeRunType)parser.setDbRunType(runType);
		//if (changeVersion)parser.setDbVersion(version);
		
		parser.updateDB();
		
	return 0;
}



// prints the parsed code for after parsing 
void printIni(spdCalib::IniParser & parsed){


	std::vector <string> keys = parsed.categories();

	for (unsigned i = 0; i < keys.size() ; ++i){

		spdCalib::Category cat = parsed[keys[i]];
		cout << cat.name.c_str() << "\n";

		for( unsigned int n = 0 ; n < cat.entries.size(); ++n){
			cout << "--->"<< cat.entries[n].name ;
			cout << "=" << cat.entries[n].value  << "\n";

		}
	}
	
}

void printNoisyPixel( NoisyList &nPixel){
	cout << "Noisy Pixels = rout"<< nPixel.router;
	cout << ", hs" << nPixel.hs ;
	cout << ", p" << nPixel.pixel << endl;

	for ( size_t i = 0; i < nPixel.coordinates.size(); ++i){
		PixelCoordinate noisy = nPixel.coordinates[i];
		cout << "\t"<<(int) noisy.column << "," << (int)noisy.row << "\n";
	}

}

void printDacValues(spdCalib::DacValue &dacValue){

	cout<< "dac(" << dacValue.dacNumber ;
	cout << ")->r"<< dacValue.router;
	cout << ", hs" << dacValue.hs ;
	cout << ", p" << dacValue.pixel ;
	cout << " = " <<(int) dacValue.value<< "\n" ;
	
}

void printParser( spdCalib::Parser & parsed){

	cout << "RunNumber = " << parsed.getRunNumber()<< endl;
	cout << "Type = " << parsed.getType() << endl;
	cout << "Router = " << parsed.getRouter() << endl;
	cout << "DetConf = " << parsed.getDbOperationMode() ;
	cout << ","<< parsed.getDbVersion() << "," << parsed.getDbRunType()<<endl;

	for (int i = 0; i < parsed.getNoisyListCount(); ++i){
		printNoisyPixel(parsed.getNoisyList(i));

	}
	for (int i =0 ; i < parsed.getDacValueCount(); ++i){
		printDacValues( parsed.getDacValue(i));
	}
	
}
