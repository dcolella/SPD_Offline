#include "StdAfx.h"
#include "./spdcalibparser.h"
#include "../spdDbConfLib/spdIniParser.h"
#include "../spdDbConfLib/spdDbConnection.h"
#include "../spdDbConfLib/SpdDbGlobalVersion.h"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

namespace spdCalib{
	
	using namespace boost;

	Parser::Parser(void)
	{
		RunNumber=0;
		Type=0;
		Router=0;
		DbOperationMode=0;
		DbConfig=-1;
		DbType=0;

	}

	Parser::~Parser(void)
	{
	}

	//****************************************************************
	// function to open a file and parse it
	// uses another IniParser to parse the ini file into categories and entries
	//****************************************************************
	void Parser::openFile( const char filename[]){
		IniParser inifile(filename);		// opens the ini file with the data
		
		parseHeader( inifile["SPD SCAN"]);	// parses the header file
		parseNoisy( inifile["NOISY"]);
		parseDacValues(inifile["DACvalues"]);
	}

	////////////////////////////////////////////////////////////////////////////
	//
	// string conversion from string to typename T 
	// e.g: int d = FromString<int>( s );
	////////////////////////////////////////////////////////////////////////////
	
	template<typename T>
	T fromString( const string& s){
		std::istringstream is(s);
		T t;
		is >> t;
		return t;
	}

	//****************************************************************
	// function to parse the header of the text file
	//****************************************************************
	void Parser::parseHeader(const Category &header){

		std::vector<Entry> entries= header.entries;
		std::vector<std::string> values;
			
				// loops over all entries in the header category
		for (size_t i = 0; i < entries.size () ; ++i){

			if (entries[i].name == "RunNumber"){
				RunNumber= fromString <long int>(entries[i].value);
			}
			else if (entries[i].name == "Type"){
				Type= fromString <int>(entries[i].value);
			}
			else if (entries[i].name == "Router"){
				Router = fromString<int>(entries[i].value);
			}
			else if (entries[i].name == "ActualDetCoonfiguration"){
						// the values are separeted by comans ex:ActualDetCoonfiguration=0,-1,-1					
				split(values, entries[i].value, is_any_of(","));
				DbOperationMode=fromString<int>(values[0]);
				DbConfig = fromString<int>(values[1]);
				DbType =fromString <int>(values[2]);
			}
			else{
				cout << "entry " << entries[i].name.c_str() << "in 'SPD Scan' not known\n";
			}
				
		}
	}

	//****************************************************************
	// function to parse all dacvalues from ini parser category
	//****************************************************************
	void Parser::parseDacValues( const Category &dacs){
		std::vector<Entry> entries= dacs.entries;
		static const boost::regex dacValueExp("(\\d+),(\\d+),(\\d+),(\\d+)");

		for (size_t i=0; i < entries.size(); ++i){
			vector<std::string> values;
			boost::smatch what;
			trim(entries[i].name);

			if ( regex_match( entries[i].name, what,dacValueExp)){
				DacValue pixelDac;
				pixelDac.dacNumber = fromString<int>( what[1]);
				pixelDac.router = fromString<int>(what[2]);
				pixelDac.hs = fromString<int>(what[3]);
				pixelDac.pixel= fromString<int>(what[4]);
				pixelDac.value= fromString<int>( entries[i].value);

				DacValues.push_back( pixelDac);
			}
			else {
				cout << "could not understand: '"<<  entries[i].name.c_str();
				cout << "' in noisy DacValues category \n";
			}

		}
	}

	//****************************************************************
	// function to parse the noisy pixels from entry list
	//****************************************************************
	void Parser::parseNoisy( const Category & noisyCat){
		std::vector<Entry> entries= noisyCat.entries;

					// regular expressions to parse noisy pixels
					//start of noisy list  -'router','hs','pixel chip'
		static const boost::regex pixelExp("-(\\d+),(\\d+),(\\d+)");
					// noisy pixel 'column','row'
		static const boost::regex coordinatesExp("(\\d+),(\\d+)");
	
		bool first = true;

		for (size_t i=0; i < entries.size(); ++i){
			
			boost::smatch what;
						// trims, spaces where making it not detect some entries
			trim(entries[i].name);	

			if ( regex_match(entries[i].name , what, pixelExp)){

				NoisyList noisyInPixel;
				noisyInPixel.router= fromString<int>(what[1]);
				noisyInPixel.hs= fromString<int>(what[2]);
				noisyInPixel.pixel = fromString<int>(what[3]);
						// restart the noisy list coordinates
				noisyInPixel.coordinates.clear();
						// inserts new pixel chip noisy list
				NoisyPixels.push_back(noisyInPixel);
			}
			else if (regex_match(entries[i].name , what, coordinatesExp)){

				PixelCoordinate  pixel;
				pixel.column = fromString<int>(what[1]);
				pixel.row =  fromString<int>(what[2]);

				vector<NoisyList>::iterator last = NoisyPixels.end()-1;
				
						// inserts noisy pixel in pixel chip list
				last->coordinates.push_back(pixel);
			}
		
			else{
				cout << "could not understand: '"<<  entries[i].name.c_str();
				cout << "' in noisy Pixels category \n";
			}
		}
	}

	//****************************************************************
	// opens a list stored in a vector
	//****************************************************************
	Parser::openFileList( std::vector<std::string> filelist){
		for (size_t i=0 ; i < filelist.size() ; ++i){
			openFile( filelist[i].c_str());
		}
		return 0;
	}

	//****************************************************************
	// opens a list of files stored in a text file
	//****************************************************************
	Parser::openFileList( const char filename[]){
		fstream infile(filename, fstream::in );

		if (!infile.is_open()){
			cout << "could not open "<< filename << "\n";
			return -1;
		}

				//opens all files in one file
		while(!infile.eof()){
			char line[256];			
			infile.getline(line, 256);	// it would be nice if I could transform this in a string somehow
										// to not have the limit to 256 characters
			openFile( line);
		}
		return 0;
	}

	//****************************************************************
	// function to update the database with the internal data
	//****************************************************************
	int Parser::updateDB(){

		
				// gets the connection to the database
		spdDbConnection * conn = spdDbConnection::subscribe();
		conn->connect();

				// one object to check the global version of the configuration
		spdDBGlobalVersion spdDbGlobal;

				//calls one instance of the class per side	
		SpdDetectorConf detectorConf_A;
		SpdDetectorConf detectorConf_C;

				// this will get the versions for both sides
		spdDbGlobal.setVersionNumber(this->DbConfig);

				// gets the right number of a configuration
		detectorConf_A.setVersionNumber( spdDbGlobal.getHsVerSideA() );
		detectorConf_C.setVersionNumber( spdDbGlobal.getHsVerSideC() );

				// gets the data from the db
		detectorConf_A.getDataFromDB();
		detectorConf_C.getDataFromDB();

				// changes the dac values
		dacValuesToConf( detectorConf_A, detectorConf_C);

				// changes the noisy pixels
		noisyToConf( detectorConf_A, detectorConf_C);

		
		cout << "Updating database from version " << spdDbGlobal.getVersionNumber() <<"\n";
		cout << "Run number ="<< this->RunNumber << endl;
		cout << "Operation Mode " << DbOperationMode << endl;
		cout << "for run type = " << DbType << endl;


				// writes the new configuration to the database
		long int versionA = detectorConf_A.update();
		long int versionC = detectorConf_C.update();

		spdDbGlobal.setHsVerSideA( versionA);
		spdDbGlobal.setHsVerSideC( versionC);

		spdDbGlobal.update();

		cout << "Final version in the database is " << spdDbGlobal.getVersionNumber() << " \n";

				// commits to write the data
		conn->commit();
		conn->disconnect();

		return 0;
	}

	//****************************************************************
	// puts the dac values into the dbConf object
	//****************************************************************
	void Parser::dacValuesToConf(SpdDetectorConf &dbConf_A, SpdDetectorConf &dbConf_C){

			// set all dacValues to the dbConf container
		for (size_t i=0 ; i < DacValues.size(); ++i){

			unsigned int hsNumber = 6 * DacValues[i].router + DacValues[i].hs;

			if (hsNumber < 60){
				dbConf_A[hsNumber].setPixelDac( DacValues[i].pixel, 
												DacValues[i].dacNumber, 
												DacValues[i].value);
			}
			else{
				dbConf_A[hsNumber-60].setPixelDac( DacValues[i].pixel, 
													DacValues[i].dacNumber, 
													DacValues[i].value);
			}
			
		}
		
	}

	//****************************************************************
	// puts the noisy pixels into de dbConf object
	//****************************************************************
	void Parser::noisyToConf(SpdDetectorConf &dbConf_A, SpdDetectorConf &dbConf_C){

		for (size_t i=0 ; i < NoisyPixels.size() ; ++i){
			int hsNumber = 6*NoisyPixels[i].router + NoisyPixels[i].hs;
			int pixel = NoisyPixels[i].pixel;
			
				// I don't know if I can delete them like this, for the moment I will keep it.
			if (hsNumber < 60)dbConf_A[hsNumber].clearNoisyPixels(pixel);
			else dbConf_C[hsNumber-60].clearNoisyPixels(pixel);

			for (size_t n = 0 ; n < NoisyPixels[i].coordinates.size() ; ++n){
				PixelCoordinate noisy = NoisyPixels[i].coordinates[n];

				if (hsNumber < 60)dbConf_A[hsNumber].insertNoisyPixel( pixel, noisy);
				else dbConf_C[hsNumber-60].insertNoisyPixel( pixel, noisy);
			}
		}

	}
}