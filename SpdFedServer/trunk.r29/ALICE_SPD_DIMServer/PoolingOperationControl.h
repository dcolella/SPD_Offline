#pragma once

#include "../HSConfig/SPDConfig.h"
#include ".\DimServerServiceHandler.h"
#include "../spdDbConfLib/spdLogger.h"

//! Class control the pooling operations*/
class PoolingOperationControl{	
	//! also has a pointer to the service handler
	DimServerServiceHandler * SRV;
	//! as a pointer to the SPD config object
	SPDConfig  * SPD;
	//! a logger object
	spdLogger *log;
    //! refresh time in ms 
	unsigned TempRefreshTime;     //number of ms

	//! boolean variable stating if the temperature polling is operational
	bool TemperaturePoolActive;
	//! boolean variable stating if the router error handling is active or not
	bool routerErrorPoolActive;
    //! keeps the last time the temperature was read 
	UInt32 TempStartTime;

	//!variable storing the time when the last time there was a router error read
	UInt32 RouterErrorTime;
	//! variable storing the refresh time, the time in ms that we want to wait between erading of the router errors 
	UInt32 RouterRefreshTime;

	//! keeps the current run number, mainly for  the router error handler
	unsigned runNumber;

/*
	//! keeps the last time the Anapil values were read
	UInt32 anapilDACStartTime;
	//! refresh time in ms 
	unsigned anapilDACRefreshTime;     //number of ms
	bool anapilDACStop;

	//! keeps the last time the Anapil values were read
	UInt32 anapilADCStartTime;
	//! refresh time in ms 
	unsigned anapilADCRefreshTime;     //number of ms
	bool anapilADCStop;
*/

public:
/*
	//! boolean variable stating if the Anapil values polling is operational
	bool anapilDACPoolActive;
	//! boolean variable stating if the Anapil values polling is operational
	bool anapilADCPoolActive;
*/
	unsigned hsSide;

	//! function to read the router control register 
	UInt32 readControlReg();

	//! method to read all routers get errors and store them in the database
	void readRouterErrors();
	//! setter for the runNumber intenrl member
	void setRunNumber(unsigned value){runNumber = value;};
	//! getter for the runNumber intenrl member
	unsigned getRunNumber(){return runNumber;};


	PoolingOperationControl(SPDConfig *, DimServerServiceHandler *);
	~PoolingOperationControl(void);
    
	//! Setter for the temperature refresh time interval
    void SetTempRefreshTime(UInt32 value){TempRefreshTime = value;};
	
	//! sets the refresh time for the router error pooling
	void setRouterErrorRefreshTime(UInt32 value){RouterRefreshTime = value;};
	
//	//! Setter for the anapil DAC refresh time interval
//    void SetAnapilDACRefreshTime(UInt32 value){anapilDACRefreshTime = value;};
//	//! Setter for the anapil ADC refresh time interval
//    void SetAnapilADCRefreshTime(UInt32 value){anapilADCRefreshTime = value;};

	/*!Starts the router error pooling*/
	void startRouterErrorPooling(unsigned refreshTime = 10000);

	/*!Stops the router error pooloing*/
	void stopRouterErrorPooling(){routerErrorPoolActive = false;};

	//! inserts one router error in the database
	void insertRouterErrorInTheDB(SpdRouter::RouterError error, unsigned  routerNumber);

	//! Method to start the HS temperature polling
	void startHSTempPool(unsigned refreshTime = 1000){
		TempRefreshTime=refreshTime;
		TemperaturePoolActive = true;
		TempStartTime = GetTickCount(); };
	//! stops the half stave temperature polling 
	void stopHSTempPool(){TemperaturePoolActive=false;};
	//! Returns if the temperature pooling is active or not 
    bool isTempPoolActive(void){return TemperaturePoolActive;};

/*
	//! Method to start the DAC values polling
	void startDACAnapilPool(unsigned refreshTime = 10000);
	//! stops the half stave temperature polling 
	void stopDACAnapilPool(){anapilDACStop = true;};
	//! Returns if the temperature pooling is active or not 
	bool isDACAnapilPoolActive(void){return anapilDACPoolActive;};
*/

/*
	//! Method to start the ADC values polling
	void startADCAnapilPool(unsigned refreshTime = 10000);
	//! stops the half stave temperature polling 
	void stopADCAnapilPool(){anapilADCStop = true;};
	//! Returns if the temperature pooling is active or not 
	bool isADCAnapilPoolActive(void){return anapilADCPoolActive;};
*/

	//! Function to perform all pooling operations 
	UInt32 PoolingFunction(void);
};
