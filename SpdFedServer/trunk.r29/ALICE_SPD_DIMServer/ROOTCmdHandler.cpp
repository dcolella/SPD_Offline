#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"


int DimServerCommandHandler::ROOTCmdHandler(void){
	bool checkNull = FALSE;
	UInt32 pos = 0;


	if(strncmp(CommString, "ROOTC_", 6) != 0) return 2;
	
	VMEAccess & VME = VMEAccess::getInstance();
	
    
	if(strcmp(CommString, "ROOTC_GETDATA") == 0){
		DataStream = VME.GetEvent(RootChNumber);
		if(DataStream == NULL){                 //to remove
		    DataStream = new UInt32 [4];
			DataStream[0] = 4;
			DataStream[1] = RootChNumber;
			DataStream[2] = 0;
			DataStream[3] = 0;
		}

		SRV->UpdateChNumber(RootChNumber);
		SRV->UpdateDataStream((unsigned int*)DataStream, (unsigned int)DataStream[0]);

        delete [] DataStream;

	}
	else if(strcmp(CommString, "ROOTC_GETDATA_AUTO") == 0){
		
				
	} else{
		sprintf_s(OutMsgText, "ERROR: ROOT command %s not ricognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}
    
    return 0;
}