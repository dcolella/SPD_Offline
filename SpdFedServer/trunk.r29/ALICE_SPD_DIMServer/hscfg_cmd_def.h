#define HSCNF_DB_DIFF_COUNT			"HSCNF_DB_DIFF_COUNT"	//!< command to count the diferences between the data in the default containers and actual containers in the fed @return McmDifferences, DacDifferences, NoisyDifferences 
#define HSCNF_DB_COMPARE			"HSCNF_DB_COMPARE"		//!< command to compare the data in the default containers and actual containers in the fed and save it to the temporary table in the database 

//! command to configure an halfstave with the default values, DPI, API and DACS 
/*!@return bit 0 indicates an error doing the jtag reset, bit 1 an error setting the digital pilot, bit 2 an error setting the analog pilot, bit 3 and error setting the pixcel dacs*/
#define HSCNF_CONFIGURE_DEF			"HSCNF_CONFIGURE_DEF"	
//! command to configure all of the halfstaves with the default values 
/*! @return an array with 60 elements each one with the status of one Hs bit 0 indicates an error doing the jtag reset, bit 1 an error setting the digital pilot, bit 2 an error setting the analog pilot, bit 3 and error setting the pixcel dacs*/
#define HSCNF_CONFIGURE_DEF_ALL		"HSCNF_CONFIGURE_DEF_ALL"	

#define HSCNF_CHIP_JTAG				"HSCNF_CHIP_JTAG"			//!< command to set the chips included in the JTAG chain \param chips in JTAG chain
#define HSCNF_JTAG_VALIDATE			"HSCNF_JTAG_VALIDATE"		//!< command to validate the jtag chain \param validation cycles \param skip mode \param scan errors \return status

#define HSCNF_JTAG_RESET			"HSCNF_JTAG_RESET"			//!< performs a jtag reset in the halfstave, its a IR scan only with IR code 0xb for the pixel chips
#define HSCNF_API__SETDAC			"HSCNF_API__SETDAC"			//!< command to set the dacs in the analog pilot  \param analog pilot array- 0: Dac Ref High, 1: Dac ref mid, 2: Gtl Ref A, 3: gtl ref D, 4: test high,	5: test low
#define HSCNF_API__DACDEF			"HSCNF_API__DACDEF"			//!< command to set the dacs in the analog pilot with the default values  
#define HSCNF_API__READ				"HSCNF_API__READ"			//!< command to read the dacs in the analog pilot \return array with analog pilot values- 0: Dac Ref High, 1: Dac ref mid, 2: Gtl Ref A, 3: gtl ref D, 4: test high,5: test low
#define HSCNF_API__READADC			"HSCNF_API__READADC"		//!< command to read the adcs in the analog pilot \return analog pilot ADCs
#define HSCNF_API__SET_ONE_DAC		"HSCNF_API__SET_ONE_DAC"	//!< command to set only one dac of the analog pilot @param dac number (follows the same order as in the jtag scan) \param dac value

//! command to set the digital pilot settings	
/*! \param digital pilot settings array- 0: ConfWaitBefRow, 1: SebMeb, 2: Mask Chip, 3: Event Number, 4: Strobe Length, 5: HoldRow, 6: SkipMode, 7: TD08TDO9, 8: Enable Ce seq, 9: Data format*/
#define HSCNF_DPI_SET				"HSCNF_DPI_SET"			
#define HSCNF_DPI_DEF				"HSCNF_DPI_DEF"				//!< command to set the digital pilot settings with the default values 
#define HSCNF_DPI_READ				"HSCNF_DPI_READ"			//!< command to read the digital pilot settings \return digital pilot settings array- 0: ConfWaitBefRow, 1: SebMeb, 2: Mask Chip, 3: Event Number, 4: Strobe Length, 5: HoldRow, 6: SkipMode, 7: TD08TDO9, 8: Enable Ce seq, 9: Data format
#define HSCNF_DPI_INT_SET			"HSCNF_DPI_INT_SET"			//!< command to set the digital pilot internal settings
#define HSCNF_DPI_INT_DEF			"HSCNF_DPI_INT_DEF"			//!< command to set the digital pilot internal settings with the default values
#define HSCNF_DPI_INT_DEF_ALL		"HSCNF_DPI_INT_DEF_ALL"		//!< command to set the digital pilot internal settings with the default values in all enabled halfstaves

#define HSCNF_DATA_RESET_ALL		"HSCNF_DATA_RESET_ALL"		//!< command to set the digital pilot internal settings with the default values in all enabled halfstaves

#define HSCNF_PIXMTX_SET			"HSCNF_PIXMTX_SET"			//!< command to set the matrix settings \param chip selected \param repeat first: if true sets the first matrix to all pixel chips \param TP: if true test pulse if false mask \param matrix: matrix array with matrices the same principal has in the jtag scan 8 32 bits for column 0 then another 8 32 bits for column 1 etc.
#define HSCNF_PIXMASK_DEF			"HSCNF_PIXMASK_DEF"			//!< command to set the matrix settings with the values from the database
#define HSCNF_PIX_UNMASK_ALL		"HSCNF_PIX_UNMASK_ALL"		//!< command to unmask all pixel chips

#define HSCNF_PXDAC_DEFAULT			"HSCNF_PXDAC_DEFAULT"		//!< command to set the pixel dacs with the defualt values
#define HSCNF_PXDAC_ALL				"HSCNF_PXDAC_ALL"			//!< command to set all pixel dacs in one hs
#define HSCNF_PXDAC_CH				"HSCNF_PXDAC_CH"			//!< command to set only one pixel dac in one hs
#define HSCNF_READ_PXDAC			"HSCNF_READ_PXDAC"			//! command to read the one pixel dac for one all 10 pixel chips of one channel \return pixel dac array  

#define HSCNF_DB_GET				"HSCNF_DB_GET"					//!< command to get the current configuration of the detector from the database
#define HSCNF_DB_SET				"HSCNF_DB_SET"					//!< command to save the current configuration of the detector in the database
#define HSCNF_DB_LINK				"HSCNF_DB_LINK"					//!< command to link the two side versions of each fed in the global version in the database
#define HSCNF_DB_GET_SIDE_VERSION	"HSCNF_DB_GET_SIDE_VERSION"		//!< command to return the current database side version assigned to this fed server
#define HSCNF_DB_GET_GLOBAL_VERSION	"HSCNF_DB_GET_GLOBAL_VERSION"	//!< command to return the current database global version loaded in the FED
#define HSCNF_DB_MASK				"HSCNF_DB_MASK"					//!< command to mask the full detector with the data from the database \param versionNumber
#define HSCNF_DB_GET_MASK_VERSION	"HSCNF_DB_GET_MASK_VERSION"		//!< command to return the current mask (noisy pixels) version assigned to this fed server \return mask version number
#define HSCNF_DB_HS_MASK			"HSCNF_DB_HS_MASK"				//!< command to mask one half-stave with the data from the database \param versionNumber \param halfStave number


#define HSCNF_ACTUALS_TO_FILE		"HSCNF_ACTUALS_TO_FILE"			//!< command to dump all actual settings to file, it will generate 60 files in the folder 'DumpConf' with the name actualHS001.txt, actualHS002 etc
#define HSCNF_DEFAULTS_TO_FILE		"HSCNF_DEFAULTS_TO_FILE"		//!< command to dump all default settings to file, it will generate 60 files in the folder 'DumpConf' with the name defaultHS001.txt, actualHS002 etc


#define HSCNF_FILE_GET				"HSCNF_FILE_GET"				//!< command the read a file for a certain channel 
#define HSCNF_FILE_GETNAME			"HSCNF_FILE_GETNAME"			//!< command to return the file name assigned to a certain channel \return filename
#define HSCNF_FILE_REFRESH			"HSCNF_FILE_REFRESH"			//!< command to reload a file for a certain channel
#define HSCNF_FILE_LOAD				"HSCNF_FILE_LOAD"				//!< command to load a file to memory !? I think it is connected with the fact that PVSS cannot read binary files (should it be there!?) 

#define HSCNF_PIXEL_DAC				"HSCNF_PIXEL_DAC"				//!< gets the current default configuration from the fed server for the pixel dacs
#define HSCNF_APIDAC				"HSCNF_APIDAC"					//!< gets the current default configuration from the fed server for the analog pilot dacs
#define HSCNF_DPICONF				"HSCNF_DPICONF"					//!< gets the current default configuration from the fed server for the digital pilot 
#define HSCNF_GOLCONF				"HSCNF_GOLCONF"					//!< gets the current configuration for the gol from the fed server

#define HSCNF_NOISYPIX				"HSCNF_NOISYPIX"				//!< gets the default configuration in the fed memory (from configuration file only)

///// TO DO: create a separate class for the configuration of the off-detector electronics
#define HSCNF_FO_STROBE_LENGTH_ALL	"HSCNF_FO_STROBE_LENGTH_ALL"	//!< command to set the fastor strobe length for all the enabled halfstaves
#define HSCNF_TIMEOUT_READY_EV_ALL	"HSCNF_TIMEOUT_READY_EV_ALL"	//!< command to set the timeout for ready event for all enabled halfstaves
