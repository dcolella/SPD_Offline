#pragma once

#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"

int DimServerCommandHandler::StringCmdHandler(void){
	if(strncmp(CommString, "STR_", 4) != 0) return 2;

	VMEAccess & VME = VMEAccess::getInstance();

	int dataOutSize = 0;
    int intstatus = 0;

	stringstream inputStream(CommString);

	string command;

	inputStream >> command;
	
	if (command == "STR_PROGRAM_ROUTERS"){
		string mode;
		string filename;
		inputStream >> mode;
		inputStream >> filename;

		if (dataSize < 10){
			intstatus = 1;
			SRV->UpdateServices(ChNumber, CommString, intstatus, CheckStatus(CommStringOriginal,intstatus, ChNumber),(unsigned int*)DataOut, dataOutSize);	
			logger->log("(DimServerCommandHandler::StringCmdHandler) data in has to be bigger than 10 for STR_PROGRAM_ROUTERS command");
			return intstatus;
		}

		char systemCommand[200];

		int sideOffset = (DetSide ==0)?0:10;
		unsigned sideRouter; 

		for (int router = 0 ; router < 10 ; router ++){
			if (this->DataIn[router] != 0){
				sideRouter = router + sideOffset;
				

				if (VME.isInDebugMode()) {
					logger->log("(StringCmdHandler)fed server is in debug mode jam player will not be called");
					sprintf_s(systemCommand, "echo %s  -I%d %s %s ", jamPlayerExecutable.c_str(), sideRouter, mode.c_str(),filename.c_str());
				}
				else{
					sprintf_s(systemCommand, "%s -I%d %s %s", jamPlayerExecutable.c_str(), sideRouter, mode.c_str(),filename.c_str());
				}

				logger->log("(DimServerCommandHandler::StringCmdHandler)starting programming router %d", sideRouter);
				logger->log("starting external command '%s'", systemCommand);

				intstatus = system( systemCommand);

				if (intstatus > 1) intstatus = 1;
								
				SRV->UpdateServices(sideRouter,	CommString, intstatus, CheckStatus("programming router", intstatus, sideRouter), NULL, dataOutSize);	
			}
		}
	
	}
	else if (command == "STR_SAVE_DPM_TO_FILE"){
		string filename;
		inputStream >> filename;

		SPD->getRouter(ChNumber/6).saveDPMDataToFile( filename.c_str());
	}

	return intstatus;
	
}