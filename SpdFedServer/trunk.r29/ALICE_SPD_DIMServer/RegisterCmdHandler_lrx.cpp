#pragma once
#include "StdAfx.h"
#include ".\DimServerCommandHandler.h"


int DimServerCommandHandler::RegisterCmdHandlerLRx(void){ 
	UInt32 BaseAddr; 

	if(!(strncmp(CommString, "R_LRX_", 6)== 0) && !(strncmp(CommString, "W_LRX_", 6)==0)){
		return 2;
	}
	UInt32 Aux =0;
	char RdWrSelector = CommString[0];
	CommString += 6;

	VMEAccess & VME = VMEAccess::getInstance();
		
	BaseAddr=getLrxAdressFromCommand(CommString);

	if (BaseAddr==-1){
		sprintf_s(OutMsgText, "ERROR: LinkRx command %s not recognized\n", CommStringOriginal);
		printf(OutMsgText);
		SRV->UpdateErrorName(OutMsgText, 1);
		return 1;
	}


	UInt32 originalMask=0;

		// on a router reset or a link receiver reset we should stop the error handler 
	if(strcmp(CommString, "RESET") == 0){
		this->SPD->getRouter(ChNumber/6).setNoDataToDAQ(1);						//noDataToDaq set to 1 to retrieve the mask without the errors masked by default
		originalMask = this->SPD->getRouter( ChNumber/6).readErrorMask();
		logger->logToScr("Stopping error handling in router %d", ChNumber/6);
		this->SPD->getRouter( ChNumber/6).writeErrorMask(0);

	}
       
	//BaseAddr += Offset;                //add protection: if it is register Offsett must be 0


	// to test this method
	bool isRouterStopped = SPD->getRouter( ChNumber/6).getStopSM();

	if (isRouterStopped){
		//cout << "router "<< ChNumber/6 << " is already stopped\n";
	}
	else{
		logger->logToScr("Stopping state machine in router %d", ChNumber/6);
		
		this->SPD->getRouter( ChNumber/6).setStopSM(true);
	}

	//2015.05.27 - adding additional check that the router is stopped
	bool isRouterStoppedNow = SPD->getRouter( ChNumber/6).getStopSM();
	if(!isRouterStoppedNow){
		logger->logToScr("SM is not stopped in router %d", ChNumber/6);
		return 1;
	}
	
	if(RdWrSelector == 'R'){
		dataSize = 1;
		delete[] DataOut;
		DataOut = new UInt32 [dataSize];
		status = VME.VMERegisterAcc( BaseAddr, DataOut, dataSize, RdWrSelector);
		logger->logToScr(" read address in lrx Address %x, data out: %x ", BaseAddr, DataOut[0], ChNumber/6);

		SRV->UpdateServices(ChNumber, CommStringOriginal, (int)status, CheckStatus(CommStringOriginal, status, ChNumber),(unsigned int*)DataOut, dataSize);

		
		

    }else{ 

		logger->logToScr(" writing address in lrx Address %x, data in: %x ", BaseAddr, DataIn[0], ChNumber/6);
		status = VME.VMERegisterAcc( BaseAddr, DataIn, dataSize, RdWrSelector);
		SRV->UpdateErrorName(CheckStatus(CommStringOriginal, status, ChNumber), (int)status);
		
	}

	if ( isRouterStopped != true){
		logger->logToScr("restarting state machine in router %d", ChNumber/6);
		
		this->SPD->getRouter( ChNumber/6).setStopSM(false);
	}
	else{
		//cout << "router "<< ChNumber/6 << " was already stopped\n";
	}

	// now restarting the error handling
	if (strcmp(CommString, "RESET") == 0){

		logger->logToScr("re-writting error mask in router %d, mask=%x", ChNumber/6, originalMask);
		this->SPD->getRouter( ChNumber/6).writeErrorMask( originalMask);
		this->SPD->getRouter(ChNumber/6).setNoDataToDAQ(0);

	}

	return 0; 
}


UInt32 DimServerCommandHandler::getLrxAdressFromCommand(const char * CommString){
	AddressGenerator & addresses = AddressGenerator::getInstance();
	UInt32 BaseAddr;

	if(strcmp(CommString, "DPM") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDPMBaseAddr(); // GetLRxDPMBaseAddr(ChNumber);	              	
	}
	else if(strcmp(CommString, "VERSION") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetVersionAddr();
	}
	else if(strcmp(CommString, "CONTROL") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetCntrRegAddr();
	}
	else if(strcmp(CommString, "STATUS") == 0){
        BaseAddr = addresses.getLRxAddr(ChNumber).GetStatusRegAddr();	
	}
	else if(strcmp(CommString, "TEMPERATURE") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetTempAddr();
	}
	else if(strcmp(CommString, "CLK_DELAY") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetClkDelayAddr();	
	}
	else if(strcmp(CommString, "DATA_DELAY") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataDelayAddr();
	}
	else if(strcmp(CommString, "DPM_ADDRESS_COUNTER") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDPMAddrCountAddr();
	}
	else if(strcmp(CommString, "MASK_COLUMN") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetColumMaskAddr();
	}
	else if(strcmp(CommString, "RESET") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetResetAddr();
	}
	else if(strcmp(CommString, "SOFT_RESET") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetResetSoftAddr();
	}
	else if(strcmp(CommString, "FLUSH") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetFlushAddr();
	}
	else if(strcmp(CommString, "TEST_REG") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetTestRegAddr();
	}
	else if(strcmp(CommString, "MCM_STIMULI") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetEnableMCMStimuliDataAddr();
		
	}
	else if(strcmp(CommString, "MCM_STIMULI_TEST_REG") == 0){
	
		BaseAddr = addresses.getLRxAddr(ChNumber).GetTestStimAddr();
	}
	
	else if(strcmp(CommString, "MCM_STIMULI_DATA") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetMCMStimuliDataAddr();
	}
	else if(strcmp(CommString, "BUSY_MASK") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetBusyMaskAddr();
	}
	else if(strcmp(CommString, "HISTOGRAM") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetHistAddr();
	}
	else if(strcmp(CommString, "ERROR_COUNTER") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetRxErrorCount();
	}
	else if(strcmp(CommString, "L1_MEM_POINTER") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetL1MemPointAddr();
	}
	else if(strcmp(CommString, "L1_ROUT_COUNT") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetL1RoutCountAddr();
	}
	else if(strcmp(CommString, "BUSY_CONTROL") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetBusyControlAddr();
	}
	else if(strcmp(CommString, "SHIFT_SEL") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetShiftSelAddr();
	}
	else if(strcmp(CommString, "EN_MOVEBIT") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetEnMoveBitAddr();
	}
	else if(strcmp(CommString, "EV_START") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetEventStartAddr();
	}
	else if(strcmp(CommString, "EV_END") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetEventEndAddr();
	}
	else if(strcmp(CommString, "MEM_START") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetL1MemStartAddr();
	}
	else if(strcmp(CommString, "MEM_END") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetL1MemEndAddr();
	}
	//else if(strcmp(CommString, "FORMAT_ERROR") == 0){
	//	BaseAddr = addresses.getLRxAddr(ChNumber).GetErrorFormatAddr();
	//}
	else if(strcmp(CommString, "INPUT_ST_STATUS0") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetInputStageStatus0Addr();
	}
	else if(strcmp(CommString, "INPUT_ST_STATUS1") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetInputStageStatus1Addr();
	}
	else if(strcmp(CommString, "EV_DESCR_RAM_STATUS") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetEvDescrRamStatusAddr();
	}
	else if(strcmp(CommString, "FIFO_PIX_DATA_STATUS") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetFIFOPixDataStatusAddr();
	}
	else if(strcmp(CommString, "FIFO_EV_DATA_STATUS") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetFIFOEvDataStatusAddr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS0") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus0Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS1") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus1Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS2") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus2Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS3") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus3Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS4") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus4Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS5") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus5Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS6") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus6Addr();
	}
	else if(strcmp(CommString, "DATA_ENC_STATUS7") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetDataEncoderStatus7Addr();
	}
	else if(strcmp(CommString, "FASTOR_L1_DELAY") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetLRXFastorL1DelayAddr();
	}
	else if(strcmp(CommString, "FO_STROBE_LENGTH") == 0){
		BaseAddr = addresses.getLRxAddr(ChNumber).GetFoStrobeLength();
	}
	else{
		BaseAddr=-1;
	}
	
       
	return BaseAddr;
}