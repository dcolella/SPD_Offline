#pragma once
#include "StdAfx.h"
#include <dis.hxx>
#include "..\spdDbConfLib\spdLogger.h"

const int ChannelsPerSide=60;
//! Class to maintain all DIM services to PVSS
class DimServerServiceHandler : public DimService{
	private:
		//! logger instance
		spdLogger *logger;
		//! DIM service sending back the channel number received
        DimService * ChNumberService;
		//! Dim service sending back the command string received
		DimService * CommandNameService;
		//! Dim command sending back the error code of the last command (0 if everything is ok)
		DimService * ReturnCodeService;
		//! Dim service sending a string with the status 
		DimService * ReturnTextService;
		//! sends the data output from a command (does not resend the data in received)
		DimService * DataOutService;
		//! sends the data stream to the external analisys tool
		DimService * SendDataStream;
		//! sends the ehartbit to PVSS
		DimService * HearthbitService;

		//! keeps the side of this FED server
		char side;
		//! keeps the channel number that will be send to the DIM service
		int ChNumber;
		//! keeps the command name to send through the DIM service
		char * CommandName;
		//! keeps the size of the command name string
        unsigned int sizeCommandName;
        //! keeps the error code to be sent
		int ReturnCode;
		//! string with the error string to be sent 
		char * ReturnText;
		//! keeps the size of the error string
		unsigned int sizeReturnText;
		//! keeps the data to send through the dim service to PVSS
		unsigned int * DataOut;
		//! keeps the size of the data that was sent 
        unsigned int sizeDataOut;  
		//! keeps the pixel raw data to be sent to the external analisys tool
		unsigned int * DataStream;
		//! keeps the size of the data stream
        unsigned int sizeDataStream;
		//! the actual heartbit value 
		int  heartBitTime;
		
        		 
	public:
		DimServerServiceHandler(char);
		~DimServerServiceHandler(void);
	    
		//! setter for the channel number
		void SetChNumber(int ChNumber){ChNumber = ChNumber;}; 
		void SetCommandName(char *);													
		void SetErrorName(char *, int);                                        
		void SetDataToSend(unsigned int * dttoSnd){DataOut = dttoSnd;}; 
		void SetDataToSendSize(unsigned int szDtToSend){sizeDataOut = szDtToSend;}; 
	    
		//! updates all services
		int UpdateServices();	
		//! updates all services settting the internal members, it calls UpdateServices();
		int UpdateServices(int ChNumber, char * cmdName, int errCode, char * errName, unsigned int * dttoSnd, unsigned int szDtToSend); 
//		int UpdateServicesPool(char * errName);
		//! send the heartbit it gets th system milliseconds and sends it
		void SendHearthbit();
		//! updates only thec hannel number
		int UpdateChNumber();			
		int UpdateChNumber(int );								
		int UpdateCommandName();										
		int UpdateCommandName(char *);										
		int UpdateErrorName();									
		int UpdateErrorName(char *, int);								
		int UpdateDataToSend();										
		int UpdateDataToSend(unsigned int*, unsigned int);			
		int UpdateDataStream();											
		int UpdateDataStream(unsigned int* dttoSnd, unsigned int szDtToSend); 
		//void UpdateTemperature(const double * busTemp, const double * mcmTemp);
};