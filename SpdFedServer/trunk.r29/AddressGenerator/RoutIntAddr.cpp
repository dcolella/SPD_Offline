#pragma once
#include "StdAfx.h"
#include "routintaddr.h"




RoutIntAddr::RoutIntAddr(void){
	
	RtFPGAVersion = 0x01;
	CntrRegAddr = 0x02;
	StatusReg1Addr = 0x03;
	StatusReg2Addr = 0x04;
	StatusReg3Addr = 0x05;
	FeDDLStatusWordAddr = 0x07;
	ResetDetectorAddr = 0x09;
	RdStartAddr = 0x0a;
	RdLEnghtOfBlockAddr = 0x0b;
    WtHistogramAddr = 0x0c;
	ResetTTCrxAddr = 0x0e;
	ResetLinkRxAddr = 0x0f;
	SendTriggSeqAddr = 0x10;
	DPMfifoStartAddr = 0x11;
	FlushDPMAddr = 0x12;
	RdL1IdAddr = 0x13;
	RdFONumbAddr = 0x14;
	JPlayerAddr = 0x15;
	DPMfifoEndAddr = 0x22;
	VMEResetAddr = 0x23;
	IrqPushButtonAddr = 0x2A;
	RdL0IdAddr = 0x2B;
	RdL2aIdAddr = 0x2C;
	ResetBcntAddr = 0x2D;
	ExtraHeaderFifoAddr = 0x2E; 
	ResetExtraHeaderFifoAddr = 0x2F;
    FOGlobalCountAddr = 0x6C;
	FOCoincedenceCountAddr  = 0x6D;
	FOTimeCountAddr = 0x71;
    ResetTempLimitAddr = 0x76;
    FOFromVMEAddr = 0x7f;
    DigPilotResetAddr = 0xE0;
	ResetPixelAddr	= 0xE2;
	TimeL0L1 = 0x75;

	const int numberHs = 6;

	for (UInt32 i=0; i< numberHs; ++i){
	  for (UInt32 j=0; j < 16; ++j){
		StatusRegLinkRxAddr[i][j] = i*16 + j + 0x80; 
	  }
	}


	for (UInt32 i=0; i < 3; ++i){
		FOLinkRxCountAddr[i] = (i + 0x6E);
		ScopeSelectorAddr[i] = (i + 0x72);
	}
	

    for (UInt32 i=0; i < numberHs; ++i){
		RdTempChAddr[i] = (i + 0x16);
		TempLimitMcmAddr[i] = (i + 0x1C);
		TempLimitBusAddr[i] = (i + 0x24);
        
		ResetHalfStaveAddr[i] = (i + 0x66);
	}

	// its 7 jtag controllers because the last one is the common one
	unsigned numberJtagControllers = 7;
	for (UInt32 i=0; i < numberJtagControllers; ++i){
		JTResetStateMacAddr[i] = (i*8 + 0x32 );
		JTResetFIFOsAddr[i] = (i*8 + 0x33 );
		JTResetChAddr[i] = (i*8 + 0x36 );
		JTRdWrDataAddr[i] = (i*8 + 0x31 );
		JTExStartAddr[i] = (i*8 + 0x34 );
		JTStatusRegAddr[i] = (i*8 + 0x35 );
		JTRdEnFIFOin[i] = (i*8 + 0x37 );
		JTRdNumbFIFOin[i] = (i*8 + 0x38);
	}
    		
	RoutSPMBaseAddr = 0x100;
	RoutDPMBaseAddr = 0x200;


    RoutLinkRxBaseAddr = 0x400;	
	LrxL1InFifo= 0xE4;
	RxReady = 0xe5;
	ResetBusyresolver = 0xe6;
	TimeBusyDaq = 0xe7;
	TimeBusyRouter = 0xe8;
	TimeBusyHs = 0xe9;
	TimeBusyTriggersL1Fifo = 0xea;
	NumTransBusyDaq = 0xeb;
	NumTransBusyRouter = 0xec;
	NumTransBusyHs = 0xed;
	NumTransBusyTriggersL1Fifo = 0xee;

//	MemCounters = 0xef;

//	AddressMemCountersSelected = 0xf0;

	RouterErrorMask = 0xf0;

	L0Counter = 0xf1;

	// test pulse L1 delay
	TpL1Delay = 0xf5;
	// New registers available in the new Michele router firmware version
	// Read_interlock_Status_selected --> only read, you received  7 bits with the following format:
	ReadInterlockStatus = 0xf4;
	//! Clock_phase_fine_delay_selected = (areg =='hf3) --> write and read register with 2 bits with the follow format:
	ClockPhaseFineDelay = 0xf3;
	//! Error handler Global RESET (for all FSMs and SPM) 
	ErrManagerReset = 0xf2;

	SebMebSet = 0xf6;

	//! register to define the time that the router waits for the transition busy->dead
	ThresholdBusyTime = 0x0C;

	//! register to define the fine delay applied to L1 in terms of BC
	L1FineDelay = 0x0d;

	//! timeout between arrival of L2 and message of ready event from link rx
	TimeoutReadyEvent = 0xe3;
}

RoutIntAddr::~RoutIntAddr(void)
{
}


void RoutIntAddr::ModifyRouterAddresses(void){
	ModifyUInt32(RoutDPMBaseAddr);
    ModifyUInt32(RoutSPMBaseAddr);    
	ModifyUInt32(RoutLinkRxBaseAddr);

    ModifyUInt32(WtHistogramAddr);
	ModifyUInt32(RdHistogramAddr);
    ModifyUInt32(JPlayerAddr);
	ModifyUInt32(RdL0IdAddr);
	ModifyUInt32(RdL2aIdAddr);
	ModifyUInt32(ResetBcntAddr);
	ModifyUInt32(IrqPushButtonAddr);
    ModifyUInt32(ResetTempLimitAddr);
	ModifyUInt32(FOFromVMEAddr);

	ModifyUInt32(RtFPGAVersion);
	ModifyUInt32(CntrRegAddr);
	ModifyUInt32(StatusReg1Addr);
	ModifyUInt32(StatusReg2Addr);
	ModifyUInt32(StatusReg3Addr);
	ModifyUInt32(VMEResetAddr);
	ModifyUInt32(DigPilotResetAddr);
    ModifyUInt32(StatusRegJTAGSelAddr);
	ModifyUInt32(DataRegSelAddr);
	ModifyUInt32(RegSelAddr);
	ModifyUInt32(FeDDLStatusWordAddr);
	ModifyUInt32(ResetDetectorAddr);
	ModifyUInt32(RdStartAddr);
	ModifyUInt32(RdLEnghtOfBlockAddr);
	
	ModifyUInt32(ResetTTCrxAddr);
	ModifyUInt32(ResetLinkRxAddr);
	ModifyUInt32(SendTriggSeqAddr);
	ModifyUInt32(DPMfifoStartAddr);
	ModifyUInt32(DPMfifoEndAddr);
	ModifyUInt32(FlushDPMAddr);
	ModifyUInt32(RdL1IdAddr);
	ModifyUInt32(RdFONumbAddr);
	ModifyUInt32(ExtraHeaderFifoAddr);
    ModifyUInt32(ResetExtraHeaderFifoAddr);

	ModifyUInt32(FOGlobalCountAddr);
	ModifyUInt32(FOCoincedenceCountAddr);
    ModifyUInt32(FOTimeCountAddr);
	ModifyUInt32( ResetPixelAddr);

	ModifyUInt32(TimeL0L1);

	for (int i=0; i < 3; ++i){
		ModifyUInt32(FOLinkRxCountAddr[i]);
		ModifyUInt32(ScopeSelectorAddr[i]);
	}
    for (int i=0; i < 6; ++i){
		ModifyUInt32(RdTempChAddr[i]);
	    ModifyUInt32(TempLimitMcmAddr[i]);
		ModifyUInt32(TempLimitBusAddr[i]);
		ModifyUInt32(ResetHalfStaveAddr[i]);
	}
	for (int i=0; i < 7; ++i){
		ModifyUInt32(JTResetStateMacAddr[i]);
		ModifyUInt32(JTResetFIFOsAddr[i]);
		ModifyUInt32(JTResetChAddr[i]);
		ModifyUInt32(JTRdWrDataAddr[i]);
		ModifyUInt32(JTExStartAddr[i]);
		ModifyUInt32(JTStatusRegAddr[i]);
		ModifyUInt32(JTRdEnFIFOin[i]);
		ModifyUInt32(JTRdNumbFIFOin[i]);
	}
    for (UInt32 i=0; i< 6; ++i){
	  for (UInt32 j=0; j < 16; ++j){
		ModifyUInt32(StatusRegLinkRxAddr[i][j]);
	  }
	}
		// newly added addresses for the router 
	ModifyUInt32( LrxL1InFifo);
	ModifyUInt32(RxReady);
	ModifyUInt32(ResetBusyresolver);
	ModifyUInt32(TimeBusyDaq);
	ModifyUInt32(TimeBusyRouter);
	ModifyUInt32(TimeBusyHs);
	ModifyUInt32(TimeBusyTriggersL1Fifo);
	ModifyUInt32(NumTransBusyDaq);
	ModifyUInt32(NumTransBusyRouter);
	ModifyUInt32(NumTransBusyHs);
	ModifyUInt32(NumTransBusyTriggersL1Fifo);
	//ModifyUInt32(MemCounters);
	//ModifyUInt32(AddressMemCountersSelected);
	ModifyUInt32(L0Counter);

	ModifyUInt32(RouterErrorMask);

	ModifyUInt32(ReadInterlockStatus);
	ModifyUInt32(ClockPhaseFineDelay);
	ModifyUInt32(ErrManagerReset);
	ModifyUInt32(TpL1Delay);

	ModifyUInt32(SebMebSet);

	ModifyUInt32(ThresholdBusyTime);
	ModifyUInt32(L1FineDelay);
	ModifyUInt32(TimeoutReadyEvent);

}
void RoutIntAddr::SetRoutBoardAddr(UInt32 BrdAdd){
	NewTerm = BrdAdd;
	Nbit= 5;
	Position = 27;  
	BMOperation = 0;	
    SetClWord();

	BoardAddr = BrdAdd << Position;

	ModifyRouterAddresses();
}



void RoutIntAddr::ShiftAddresses(int Nshift){
	if (Nshift != 0){
		NShift = Nshift;
		BMOperation = 1;

		ModifyRouterAddresses();
	}

}



