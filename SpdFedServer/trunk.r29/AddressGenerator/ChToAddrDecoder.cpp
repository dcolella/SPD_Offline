#include "StdAfx.h"
#include "chtoaddrdecoder.h"


ChToAddrDecoder::ChToAddrDecoder(void){
	int sect, stave, side;

	// will initialize 
	for (int i = 0; i < 120; i++){
				
		if (i >= 60){
			sect = (i - 60)/6;
			stave = (i - 60) % 6;
			side = 1;
		} else {
			sect = i / 6;
			stave = i % 6;
			side = 0;
		}
		
		PositionToChannel[sect][stave][side] = i; 
	}

	
	for (UInt32 j =0; j < 20; j++){
		RouterBoardAddresses[j] = j+1; 
	}
	
	BusyBoardsAddresses[0] = 22;
	BusyBoardsAddresses[1] = 23;
}


ChToAddrDecoder::~ChToAddrDecoder(void){
}


UInt8  ChToAddrDecoder::GetChannNumbFromPosition(UInt8 SectorNumb, UInt8 StaveNumb, UInt8 Side){
	return PositionToChannel[SectorNumb][StaveNumb][Side];
}

UInt32 ChToAddrDecoder::GetBoardAddr(UInt8 ChNumb){
	return RouterBoardAddresses[ChNumb / 6];
}


