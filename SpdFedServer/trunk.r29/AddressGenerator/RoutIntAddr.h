#pragma once
#include "../BitManager/bitsmanage.h"

//!Class to keep in memory the register addresses for one Router
class RoutIntAddr : public BitsManage{
  private:
    UInt32 BoardAddr;
	UInt32 RoutDPMBaseAddr;
    UInt32 RoutSPMBaseAddr;
	UInt32 RoutLinkRxBaseAddr; 
    
	UInt32 RtFPGAVersion;
	UInt32 CntrRegAddr;
	UInt32 StatusReg1Addr;
	UInt32 StatusReg2Addr;
	UInt32 StatusReg3Addr;
	UInt32 VMEResetAddr;
    UInt32 DigPilotResetAddr;
    UInt32 StatusRegJTAGSelAddr;
	UInt32 DataRegSelAddr;
	UInt32 RegSelAddr;
	UInt32 FeDDLStatusWordAddr;
	UInt32 ResetDetectorAddr;
	UInt32 RdStartAddr;
	UInt32 RdLEnghtOfBlockAddr;
	
	UInt32 WtHistogramAddr;
	UInt32 RdHistogramAddr;
    UInt32 JPlayerAddr;
	UInt32 RdL0IdAddr;
	UInt32 RdL2aIdAddr;
	UInt32 ResetBcntAddr;
	UInt32 IrqPushButtonAddr;
    UInt32 ResetTempLimitAddr;
	UInt32 StatusRegLinkRxAddr[6][16]; 
	UInt32 FOFromVMEAddr;

	UInt32 ResetTTCrxAddr;
	UInt32 ResetLinkRxAddr;
	UInt32 SendTriggSeqAddr;
	UInt32 DPMfifoStartAddr;
	UInt32 DPMfifoEndAddr;
	UInt32 FlushDPMAddr;
	UInt32 RdL1IdAddr;
	UInt32 RdFONumbAddr;
	UInt32 ExtraHeaderFifoAddr; 
	UInt32 ResetExtraHeaderFifoAddr;
    
	UInt32 FOGlobalCountAddr;
	UInt32 FOCoincedenceCountAddr;
    UInt32 FOLinkRxCountAddr[3];
    UInt32 FOTimeCountAddr;

	UInt32 ScopeSelectorAddr[3];
	UInt32 ResetHalfStaveAddr[6];
    
	UInt32 RdTempChAddr[6];
	UInt32 TempLimitMcmAddr[6];
	UInt32 TempLimitBusAddr[6];

	UInt32 JTResetStateMacAddr[7]; // Position 7 is the common JTAG controller
	UInt32 JTResetFIFOsAddr[7];
    UInt32 JTResetChAddr[7];
	UInt32 JTRdWrDataAddr[7];
    UInt32 JTExStartAddr[7];
    UInt32 JTStatusRegAddr[7];
	UInt32 JTRdEnFIFOin[7];
	UInt32 JTRdNumbFIFOin[7];
	UInt32 ResetPixelAddr;
	UInt32 TimeL0L1;		
	
	//! memory adresses added by Michele 	
	UInt32 LrxL1InFifo;
	UInt32 RxReady;
	UInt32 ResetBusyresolver;
	UInt32 TimeBusyDaq;
	UInt32 TimeBusyRouter;
	UInt32 TimeBusyHs;
	UInt32 TimeBusyTriggersL1Fifo;
	UInt32 NumTransBusyDaq;
	UInt32 NumTransBusyRouter;
	UInt32 NumTransBusyHs;
	UInt32 NumTransBusyTriggersL1Fifo;

//	UInt32 MemCounters;
//	UInt32 AddressMemCountersSelected;
	UInt32 L0Counter;

	UInt32 RouterErrorMask;

	// New registers available in the new Michele router firmware version
	/*! Read_interlock_Status_selected = (areg =='hf4) --> only read, you received  7 bits with the following format:
		 Router_Interlock  Interlock on HS_5	Interlock on HS_4 	Interlock on HS_3 	Interlock on HS_2 	Interlock on HS_1 	Interlock on HS_0 
	*/
	UInt32 ReadInterlockStatus;
	//! Clock_phase_fine_delay_selected = (areg =='hf3) --> write and read register with 2 bits with the follow format:
	UInt32 ClockPhaseFineDelay;
	//! Error handler Global RESET (for all FSMs and SPM) 
	UInt32 ErrManagerReset;
	
	/*! defines the L1 delay for the test pulse, 
		like this we can have the same delay for calibration and real particles
		*/
	UInt32 TpL1Delay;

	//!register to select multi event buffer or single event buffer
	/*!Address 0xf6 can only have 2 values -> 20090106 and 20070526 */
	UInt32 SebMebSet;

	//! register to define the time that the router waits for the transition busy->dead
	UInt32 ThresholdBusyTime;

	//! register to define the fine delay applied to L1 in terms of BC
	// 2 bits from 0 to 3
	UInt32 L1FineDelay;

	//! timeout ready event, between arrival of L2 and message of ready event from link rx
	UInt32 TimeoutReadyEvent;

	
 public:
    RoutIntAddr(void);                     //ok
    ~RoutIntAddr(void);
	void SetRoutBoardAddr(UInt32 BrdAdd);  //ok 
    void ShiftAddresses(int Nshift);       //ok

    void ModifyRouterAddresses(void);      //ok
	//Router							
	UInt32 GetDPMBaseAddr(){return RoutDPMBaseAddr;};
    UInt32 GetSPMBaseAddr(){return RoutSPMBaseAddr;};
	UInt32 GetLinkRxBaseAddr(){return RoutLinkRxBaseAddr;};          
	UInt32 GetRtFPGAVersion(){return RtFPGAVersion;};
	UInt32 GetCntrRegAddr(){return CntrRegAddr;};
	UInt32 GetStatusReg1Addr(){return StatusReg1Addr;};
	UInt32 GetStatusReg2Addr(){return StatusReg2Addr;};
	UInt32 GetStatusReg3Addr(){return StatusReg3Addr;};
	UInt32 GetRtVMEResetAddr(){return VMEResetAddr;};
    UInt32 GetDigPilotResetAddr(){return DigPilotResetAddr;};
    UInt32 GetStatusRegJTAGSelAddr(){return StatusRegJTAGSelAddr;};
	UInt32 GetDataRegSelAddr(){return DataRegSelAddr;};
	UInt32 GetRegSelAddr(){return RegSelAddr;};
	UInt32 GetFeDDLStatusWordAddr(){return FeDDLStatusWordAddr;};
	UInt32 GetResetDetectorAddr(){return ResetDetectorAddr;};
	UInt32 GetRdStartAddr(){return RdStartAddr;};
	UInt32 GetRdLEnghtOfBlockAddr(){return RdLEnghtOfBlockAddr;};
	UInt32 GetWtHistogramAddr(){return WtHistogramAddr;};
	UInt32 GetRdHistogramAddr(){return RdHistogramAddr;};
	UInt32 GetJPlayerAddr(){return JPlayerAddr;};
	UInt32 GetRdL0IdAddr(){return RdL0IdAddr;};
	UInt32 GetRdL2aIdAddr(){return RdL2aIdAddr;};
	UInt32 GetResetBcntAddr(){return ResetBcntAddr;};
	UInt32 GetIrqPushButtonAddr(){return IrqPushButtonAddr;};
	UInt32 GetResetTempLimitAddr(){return ResetTempLimitAddr;};
	UInt32 GetStatusRegLinkRxAddr(UInt8 ChNumb, UInt8 regNumb){return StatusRegLinkRxAddr[ChNumb%6][regNumb];};
	UInt32 GetFOFromVMEAddr(){return FOFromVMEAddr;};
  
	UInt32 GetResetTTCrxAddr(){return ResetTTCrxAddr;};
	UInt32 GetResetLinkRxAddr(){return ResetLinkRxAddr;};
	UInt32 GetSendTriggSeqAddr(){return SendTriggSeqAddr;};
	UInt32 GetDPMfifoStartAddr(){return DPMfifoStartAddr;};
	UInt32 GetDPMfifoEndAddr(){return DPMfifoEndAddr;};
	UInt32 GetFlushDPMAddr(){return FlushDPMAddr;};
	UInt32 GetRdL1IdAddr(){return RdL1IdAddr;};
	UInt32 GetRdFONumbAddr(){return RdFONumbAddr;};
	UInt32 GetExtraHeaderFifoAddr(){return ExtraHeaderFifoAddr;};
	UInt32 GetResetExtraHeaderFifoAddr(){return ResetExtraHeaderFifoAddr;};

	UInt32 GetTimeL0L1(){return TimeL0L1;};

	UInt32 GetFOGlobalCountAddr(){return FOGlobalCountAddr;};
	UInt32 GetFOCoincedenceCountAddr(){return FOCoincedenceCountAddr;};
	UInt32 GetFOTimeCountAddr(){return FOTimeCountAddr;};
    
	UInt32 GetFOLinkRxCountAddr(UInt8 ChNumb){return FOLinkRxCountAddr[(ChNumb % 6)/2];};
	UInt32 GetScopeSelectorAddr( UInt8 ScopeSelec){return ScopeSelectorAddr[ScopeSelec];};
	UInt32 GetResetHalfStaveAddr(UInt8 ChNumb){return ResetHalfStaveAddr[ChNumb % 6];};

    UInt32 GetTempLimitMcmAddr(UInt8 ChNumb){return TempLimitMcmAddr[ChNumb % 6];};
	UInt32 GetTempLimitBusAddr(UInt8 ChNumb){return TempLimitBusAddr[ChNumb % 6];};
	UInt32 GetRdTempChAddr(UInt8 ChNumb){return RdTempChAddr[ChNumb % 6];};
	UInt32 GetResetPixelAddr(){return ResetPixelAddr;};

    UInt32 GetJTResetStateMacAddr(UInt8 ChNumb){return JTResetStateMacAddr[ChNumb % 6];};
	UInt32 GetJTResetFIFOsAddr(UInt8 ChNumb){return JTResetFIFOsAddr[ChNumb % 6];};
    UInt32 GetJTRdWrDataAddr(UInt8 ChNumb){return JTRdWrDataAddr[ChNumb % 6];};
    UInt32 GetJTExStartAddr(UInt8 ChNumb){return JTExStartAddr[ChNumb % 6];};
    UInt32 GetJTStatusRegAddr(UInt8 ChNumb){return JTStatusRegAddr[ChNumb % 6];};
    UInt32 GetJTResetChAddr(UInt8 ChNumb){return JTResetChAddr[ChNumb % 6];};
	UInt32 GetJTRdEnFIFOin(UInt8 ChNumb){return JTRdEnFIFOin[ChNumb % 6];};
	UInt32 GetJTRdNumbFIFOin(UInt8 ChNumb){return JTRdNumbFIFOin[ChNumb % 6];};

			// getters for the michelles functions
	UInt32 GetLrxL1InFifo(){return LrxL1InFifo; };
	UInt32 GetRxReadyAddr(){return RxReady; };
	UInt32 GetResetBusyresolverAddr(){return ResetBusyresolver;};
	UInt32 GetTimeBusyDaqAddr(){return TimeBusyDaq;};
	UInt32 GetTimeBusyRouterAddr(){return TimeBusyRouter;};
	UInt32 GetTimeBusyHsAddr(){return TimeBusyHs;};
	UInt32 GetTimeBusyTriggersL1FifoAddr(){return TimeBusyTriggersL1Fifo;};
	UInt32 GetNumTransBusyDaqAddr(){return NumTransBusyDaq;};
	UInt32 GetNumTransBusyRouterAddr(){return NumTransBusyRouter;};
	UInt32 GetNumTransBusyHsAddr(){return NumTransBusyHs;};
	UInt32 GetNumTransBusyTriggersL1FifoAddr(){return NumTransBusyTriggersL1Fifo;};
	//UInt32 GetMemCountersAddr(){return MemCounters;};
	//UInt32 GetAddressMemCountersSelectedAddr(){return AddressMemCountersSelected;};
	UInt32 GetL0CounterAddr(){return L0Counter;};
	UInt32 GetErrorMask(){return RouterErrorMask;};

	UInt32 GetInterlockStatus(){return this->ReadInterlockStatus;};
	UInt32 GetClockPhaseFineDelay(){return this->ClockPhaseFineDelay;};
	UInt32 GetErrManagerReset(){return this->ErrManagerReset;};
	UInt32 GetTpL1Delay(){return this->TpL1Delay;};

	UInt32 GetSebMebSet(){return this->SebMebSet;};
	
	UInt32 GetThresholdBusyTime(){return this->ThresholdBusyTime;};
	UInt32 GetL1FineDelay(){return this->L1FineDelay;};
	UInt32 GetTimeoutReadyEvent(){return this->TimeoutReadyEvent;};
};
