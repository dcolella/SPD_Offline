// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once 


#define DllExport __declspec(dllexport)

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
//#include <visa.h>
//#include <visatype.h>
#include "Caenvmelib.h"
#include <fstream>
#include <iostream>
#include <tchar.h>
#include <stdlib.h>



typedef char Int8;
typedef unsigned char UInt8;
typedef unsigned short UInt16;
typedef unsigned long int UInt32;




using namespace std; 

// TODO: reference additional headers your program requires here
