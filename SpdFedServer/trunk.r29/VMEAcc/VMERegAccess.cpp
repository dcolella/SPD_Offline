#pragma once
#include "StdAfx.h"
#include ".\vmeregaccess.h"

VMERegAccess::VMERegAccess(){
    //VRAOperation = 3;
}

VMERegAccess::~VMERegAccess(void)
{
}

CVErrorCodes VMERegAccess::VMERegisterAcc(UInt32  RegAddr, UInt32 * Value, UInt32 dataSize, char mode){

	CVErrorCodes ret;
	int count=0;
	
	// if in debug mode returns
    if(this->isInDebugMode()){
		//cout << "|";
		return cvSuccess;
	}

	//if(VRAOperation && 1) SessMappedNumb = MapAddr(*RegAddr, dataSize, &MappedRetNumb);
	if(dataSize ==0) dataSize =1;

	if(dataSize == 1){
		if (mode == 'R'){
			ret = CAENVME_ReadCycle(GetHandle(), RegAddr, Value, cvA32_S_DATA, cvD32); 
		} 
		else if (mode == 'W'){
            ret = CAENVME_WriteCycle(GetHandle(), RegAddr, Value, cvA32_S_DATA, cvD32); 
		}

	}else if (dataSize > 1){ 		
		if (mode == 'R'){
			ret = CAENVME_BLTReadCycle(GetHandle(), RegAddr, Value, dataSize*4, cvA32_S_DATA, cvD32, &count); 
		} 
		else if (mode == 'W'){

            ret = CAENVME_BLTWriteCycle(GetHandle(), RegAddr, Value, dataSize*4, cvA32_S_DATA, cvD32, &count);
			//printf("VMERegAccess::VMERegisterAcc bytes written: %d\n",count);
		}
	}
	
	return ret;
}
