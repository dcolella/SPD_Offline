#include "CAENVMESession.h"


CAENVMESession::CAENVMESession(void)
{
	VMEacc_handle=0;
	debugMode=false;
	log = &spdLogger::getInstance();
}


CAENVMESession::~CAENVMESession(void)
{
}

CVErrorCodes CAENVMESession::InitSession(void){

	CVErrorCodes ret=cvSuccess;

	if(!debugMode){
	  ret=CAENVME_Init(cvV1718,0,0,&VMEacc_handle);
	  if(ret==cvSuccess) log->log("INFO: Connection to CAEN VME-USB bridge is open");
	  else log->log("ERROR: Connection to CAEN VME-USB bridge failed to open");
	  return ret;
	}
	else{
      if(ret==cvSuccess) log->log("INFO: debugMode - Connection to CAEN VME-USB bridge isn't open");
	  return cvSuccess;
	}
}

CVErrorCodes CAENVMESession::EndSession(void){
	return CAENVME_End(VMEacc_handle);
}
