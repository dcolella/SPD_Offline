#pragma once
#include "CAENVMElib.h"
#ifndef ErrorHandler_h

//! Class to manage error handler (do we need this?)
class ErrorHandler{
  public:
    CVErrorCodes status;
    char OutMsgText[400];
	char errorText[200];
	
	ErrorHandler(void);
	~ErrorHandler(void);
	void CheckStatus(void);
	void CheckStatus(CVErrorCodes);

	//cesar 27/05/2008: changed this declaraion so to be more meaningful
	//char * CheckStatus(char * , ViStatus,  unsigned int );
	char * CheckStatus(char * command , CVErrorCodes error,  unsigned int channel);
	char * CheckStatus(char * command , int error,  unsigned int channel);

	//string DecodeError(CVErrorCodes);
	char* DecodeErrorCode(CVErrorCodes);
};

#define ErrorHandler_h
#endif
