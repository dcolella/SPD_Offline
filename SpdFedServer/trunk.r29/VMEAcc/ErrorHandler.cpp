#pragma once
#include "StdAfx.h"
#include "errorhandler.h"
#include "..\spdDbConfLib\spdLogger.h"


ErrorHandler::ErrorHandler(void){
	status = cvSuccess;
}

ErrorHandler::~ErrorHandler(void){

}

void ErrorHandler::CheckStatus(CVErrorCodes InStatus){ 	

	if (InStatus != cvSuccess){
       cerr << "Error" << InStatus << endl;
       exit(1);
    }
}


void ErrorHandler::CheckStatus(void){
	CheckStatus(status);
}

char * ErrorHandler::CheckStatus(char * CommandName, CVErrorCodes InStatus , unsigned int  channel){
    
	if (InStatus != cvSuccess){
		sprintf_s(OutMsgText,  "ERROR: Operation %s not succeeded, channel %d due to %s\n", CommandName, channel, DecodeErrorCode(InStatus));
	} else{
		sprintf_s(OutMsgText,  "INFO: Executed %s, channel %d\n", CommandName, channel);
	}
	return  OutMsgText;
}

char * ErrorHandler::CheckStatus(char * CommandName, int InStatus , unsigned int  channel){
	if (InStatus != 0){
		sprintf_s(OutMsgText,  "ERROR: Operation %s not succeeded, channel %d. Error code: %d \n", CommandName, channel,InStatus);
	} else{
		sprintf_s(OutMsgText,  "INFO: Executed %s, channel %d\n", CommandName, channel);
	}
	
	return  OutMsgText;
}

char* ErrorHandler::DecodeErrorCode(CVErrorCodes errorCode){
	switch (errorCode){
	case -1:
		sprintf_s(errorText, "bus error");
		break;
	case -2:
		sprintf_s(errorText,"communication error");
		break;
	case -4:
		sprintf_s(errorText,"generic VME error");
		break;
	case -8:
		sprintf_s(errorText,"invalid parameter");
		break;
	case -16:
		sprintf_s(errorText,"VME timeout");
		break;
	case -32:
		sprintf_s(errorText,"SPD timeout");
		break;
	case -64:
		sprintf_s(errorText,"channel not active");
		break;
	case -128:
		sprintf_s(errorText,"SPD general error");
		break;
	case -256:
		sprintf_s(errorText,"wrong number of arguments");
		break;
	case -512:
		sprintf_s(errorText,"spdDB error");
		break;	
	default:
	    sprintf_s(errorText,"unknown error code: %d",errorCode);
		break;
    }
	return errorText;
}