#pragma once
#include "StdAfx.h"
#include "bitsmanage.h"

BitsManage::BitsManage(void){
	this->BMOperation = 0;
}

BitsManage::~BitsManage(void){
}

UInt32 BitsManage::ExtractFromUInt32(UInt32 Input, UInt8 position, UInt8 bitNumber){
	Position = position;
	Nbit = bitNumber;
	SetClWord();
	return (UInt32)((Input & (~CleanerWord) ) >> position);
}

void BitsManage::ModifyUInt32(UInt32 &OldUInt32, UInt32 NewEle, UInt8 position, UInt8 bitNumber){
	NewTerm = NewEle;
	Position = position;
	Nbit = bitNumber;
	SetClWord();
	BMOperation = 0;
	ModifyUInt32(OldUInt32);
}



void BitsManage::ModifyUInt32(UInt32 &OldUInt32){
	if(BMOperation == 0 || BMOperation == 2){
		OldUInt32 = (OldUInt32 & CleanerWord) + (((UInt32)NewTerm << Position)& ~CleanerWord);
	}
	if(BMOperation == 1 || BMOperation == 2){
		OldUInt32 <<= NShift;
	}
}

UInt32 BitsManage::UInt8ToUInt32(UInt8 * dataIn){
	UInt32 DataOut = 0;
	for(UInt32 i = 0; i< 4; i++){
		DataOut += (UInt32)dataIn[3-i] << (i*8);
	}
	return DataOut;
}

void BitsManage::UInt32ToUInt8(UInt8 * dataOut, UInt32 dataIn){
	for(UInt8 i = 0; i< 4; i++){
		dataOut[3-i] = (UInt8)dataIn;
		dataIn >>=8;
	}
}