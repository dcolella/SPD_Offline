#include "StdAfx.h"
#include ".\spdhitdata.h"

SpdHitData::SpdHitData(void)
{
	this->log = & spdLogger::getInstance();

	this->currentChip = -1;
	this->currentHalfstave = -1;
	this->currentEvent = -1;
}


SpdHitData::~SpdHitData(void)
{
}


// searches for the start of next event in the data
unsigned int SpdHitData::nextEvent( UInt32 *data, unsigned pos, unsigned size){

	unsigned n;
	for (n = pos ; n <= size ; n ++){
		if (data[n] == 0xffffffff)	break;
	}
	return n;
}

// initializes all vectors for a new event
void SpdHitData::startEvent(){
	
	this->currentChip = -1;	
	this->currentHalfstave =-1;

	this->currentEvent ++;

	unsigned const nHsInRouter = 6;
	unsigned const nChipsInHS = 10;

	spdEventHits newEvent(nHsInRouter);

	for(int n = 0; n < nHsInRouter; n++){
		newEvent[n].resize(nChipsInHS);
	}

	this->events.push_back(newEvent);
}

// parses an array of raw data
void SpdHitData::parseRawData(UInt32 *data, unsigned int size){
	unsigned nEvent;
	unsigned long lWord, hWord;

	this->startEvent();
	
	if (  8 >= size) return ;

	nEvent = nextEvent ( data, 0, size);	
	nEvent += 8; // does not parses the router header
	
	
	for (unsigned i =nEvent ; i < size; i ++){
		// new event
		if(data[i] == 0xffffffff){
			//log->logToScr("case of new event: parsed event, number of hits = %d, events in memory= %d", getNumberOfHitsInEvent(currentEvent), events.size() );
			this->startEvent();
			i+=8;	// does not parses the router header

		}
			//separetes the two 16 bit data
		lWord= data[i] & 0xFFFF;		
		hWord=( data[i] & 0xFFFF0000 ) >> 16;
			//parses them
		parcePixelCell(hWord);									
		parcePixelCell(lWord);
	}

	//log->logToScr(" parsed event, number of hits = %d, events in memory= %d", getNumberOfHitsInEvent(currentEvent), events.size() );

}

// gets one even doing bound checking
spdEventHits & SpdHitData::getEvent(unsigned eventNumber){
	if (eventNumber >= this->events.size() ){
		log->log("ERROR: (SpdHitData::getEvent) eventNumber out of range");
		//returns an empty event in case of errors
		static spdEventHits empty;
		return empty;
	}

	return events[eventNumber];

}

// returns the total number of hits in one event
unsigned SpdHitData::getNumberOfHitsInEvent( unsigned eventNumber){

	if (eventNumber >= this->events.size() ){
		log->log("ERROR: (SpdHitData::getNumberOfHitsInEvent) eventNumber out of range");
		return 0;
	}


	unsigned total = 0;
	unsigned const nHsInRouter = 6;
	unsigned const nChipsInHS = 10;


	for (int hs = 0 ; hs <nHsInRouter ; hs ++ ){
		for(int chip = 0; chip < nChipsInHS; chip++){
			total += (unsigned) events[eventNumber][hs][chip].size();
		}
	}

	return total;

}

void SpdHitData::clear(){
	events.clear();

	this->currentChip = -1;
	this->currentHalfstave = -1;
	this->currentEvent = -1;
}

// parses one 16 bit cell of pixel raw data
void SpdHitData::parcePixelCell(UInt32 data){

	unsigned int  hitCount;
	unsigned int header;
	unsigned currTrigger;


	header= data /0x4000;	
	
	switch (header)	{
		
		case 0:			// chip trailer with hit counts				
			hitCount=data & 0x1fff;
			break;

		case 1:			// chip header: HS, event number, chip address
			
			currentChip= data & 0xF ;
			currTrigger = (data & 0x3F0)>> 4;
			currentHalfstave = ( data & 0x3800)>> 11;
			break;

		case 2:			// pixel hit: row, column

			PixelCoordinate hit;
			hit.column =(unsigned char) data & 0x1F;
			hit.row = (unsigned char)(data >> 5) & 0xFF;

			//log->logToScr("hit event %d, hs %d, %d: %d,%d", currentEvent, currentHalfstave, currentChip, hit.column, hit.row);

			if (currentChip >= 10){
				this->log->log("ERROR: data format error chip number %d out of range ", currentChip);
				break;
			}
			if (currentHalfstave >= 6){
				this->log->log("ERROR: data format error HS number %d out of range ", currentHalfstave);
				break;
			}

			this->getEvent(currentEvent)[currentHalfstave][currentChip].push_back(hit);
			

			break;

		case 3:			// fill word: used if chip # is odd
			break;
	}


}
