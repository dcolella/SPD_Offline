#include "StdAfx.h"
#include ".\hsconfiguration.h"
#include <math.h>

HSConfiguration::HSConfiguration(void){
	PixChipInChain = 0x3ff;
  for(int i=0; i<10; i++){
     PxDAC[i][0] = 128;     //dis_BIASTH
     PxDAC[i][1] = 128;     //dis_VCASD4
     PxDAC[i][2] = 128;     //dis_VCASD21
     PxDAC[i][3] = 128;     //dis_VIBCOMP
     PxDAC[i][4] = 128;     //dis_VIBIASCURD
     PxDAC[i][5] = 128;     //dis_VIDISC
     PxDAC[i][6] = 128;     //dis_VREF2DISC  

     PxDAC[i][7] = 0;       //eu_VBIAS
     PxDAC[i][8] = 128;     //eu_VBN
     PxDAC[i][9] = 0;       //eu_VBNBUFFER
     PxDAC[i][10] = 128;     //eu_VBNBUSLATCH
     PxDAC[i][11] = 128;     //eu_VBNG
     PxDAC[i][12] = 128;     //eu_VBNLHCB
     PxDAC[i][13] = 200;     //eu_VBPPULLDOWN

     PxDAC[i][14] = 128;     //Fast_CGPOL
     PxDAC[i][15] = 128;     //Fast_CGPOLFM
     PxDAC[i][16] = 128;     //Fast_COMPREF
     PxDAC[i][17] = 128;     //Fast_CONVPOL
     PxDAC[i][18] = 128;     //Fast_CONVPOLFM
     PxDAC[i][19] = 128;     //Fast_FMPOL
     PxDAC[i][20] = 128;     //Fast_FOPOL

     PxDAC[i][21] = 128;    //Ken_EOCVBN
     PxDAC[i][22] = 128;     //Ken_VBN
     PxDAC[i][23] = 128;     //Ken_VBNM
     PxDAC[i][24] = 128;     //Ken_VBNS
     PxDAC[i][25] = 0;     //Ken_VBUFFVBN

     PxDAC[i][26] = 128;     //Pre_VI1
     PxDAC[i][27] = 128;     //Pre_VI2
     PxDAC[i][28] = 128;     //Pre_VI3
     PxDAC[i][29] = 128;     //Pre_VI4
     PxDAC[i][30] = 128;     //Pre_VI4
     PxDAC[i][31] = 128;     //Pre_VIFB
     PxDAC[i][32] = 128;     //Pre_VIPREAMP
     PxDAC[i][33] = 128;     //Pre_VREF1
     PxDAC[i][34] = 128;     //Pre_VREF2
     PxDAC[i][35] = 128;     //Pre_VREF3
     PxDAC[i][36] = 128;     //Pre_VREF4
     PxDAC[i][37] = 128;     //Pre_VREF5
     PxDAC[i][38] = 128;     //Pre_VREF6
     PxDAC[i][39] = 190;     //Pre_VTH

     PxDAC[i][40] = 128;     //Val_BUFFIN
     PxDAC[i][41] = 200;     //Val_BUFFOUT

     PxDAC[i][42] = 33;     //Dealy_Control
     PxDAC[i][43] = 192;    //Misc_Control
     for(int j = 0; j < 256; j++){
		PxTP[i][j] = 0;  
		PxMASK[i][j] = 0;
     }
  }
    
    //MCM Parameters    
               
     
     
     APIConf[0] = 128;    //API Default after reset
     APIConf[1] = 128;
     APIConf[2] = 128;
     APIConf[3] = 128;
     APIConf[4] = 128;
     APIConf[5] = 128;
     
     GOLConf = 0x20101f33; //GOL Default after reset  
    
}

HSConfiguration::~HSConfiguration(void){
}


UInt32 HSConfiguration::LoadClassDefaults(void){
	if(GetFileSize() == 0) return 1;
	
	for(int i = 0; i< numberOfChips; i++){
	  for(int j = 0; j< NPixelDacs ; j++){
		  PxDAC[i][j] = GetPixelDACVectDefault()[i*NPixelDacs + j]  ;              
	  }
	}

    //MCM Parameters
	DPI.SetDPIConf0(GetDPIConfVectDefault()[0]);
    DPI.SetDPIConf1(GetDPIConfVectDefault()[1]);
	GOLConf = GetGOLConfVectDefault();
    
	for(int i = 0; i < 6; i++){
		APIConf[i] = GetAPIdacVectDefault()[i];
	}

	return 0;
}

void HSConfiguration::SetPxDACVector(UInt8 * inputStream, UInt8 ChipN){
	for(int i=0; i< 44; i++){
		PxDAC[ChipN][i] = inputStream[i];
	}
}

void HSConfiguration::SetPxTP(UInt32 * Input, UInt8 ChipN){
	for(int i=0; i< numberOfRows; i++){
		PxTP[ChipN][i] = Input[i];
	}	
}


void HSConfiguration::SetPxMASK(UInt32 * Input, UInt8 ChipN){
	for(int i=0; i< numberOfRows; i++){
		PxMASK[ChipN][i] = Input[i];
	}	
}

void HSConfiguration::SetAPIConf(UInt8 * Input){
	for(int i=0; i< 6; i++){
		APIConf[i] = Input[i];
	}	
}

// copies the internal members of the class to the database class
UInt32 HSConfiguration::SetClassToDb( PixelConfDb &dbData){


	dbData.setDpiWaitBefRow(this->DPI.GetDPIConfWaitBefRow());
	dbData.setDpiSebMeb(this->DPI.GetDPIConfSebMeb());
	dbData.setDpiMaskChip( this->DPI.GetDPIConfMaskChip());
	dbData.setDpiEventNumber(this->DPI.GetDPIConfEventNumb());
	dbData.setDpiStrobeLenght(this->DPI.GetDPIConfStrobeLength());
	dbData.setDpiHoldRow((UInt8)this->DPI.GetDPIConfHoldRow());
	dbData.setDpiSkipMode(this->DPI.GetDPIConfSkipMode());
	dbData.setDpiTdo8Tdo9((UInt8)this->DPI.GetDPIConfTDO8TDO9());
	dbData.setDpiEnableCeSeq((UInt8)this->DPI.GetDPIConfEnableCESeq());
	dbData.setDpiDataFormat((UInt8)this->DPI.GetDPIConfDataFormat());
	

	dbData.setApiDacRefHi(this->GetDACRefHiAPIConf());
	dbData.setApiDacRefMid(this->GetDACRefMidAPIConf());
	dbData.setApiGtlRefA(this->GetDACGtlRefAPIConf());
	dbData.setApiGtlRefD(this->GetDACGtlRefDAPIConf());
	dbData.setApiAnTestHi(this->GetDACTestHiAPIConf());
	dbData.setApiAnTestLow(this->GetDACTestLowAPIConf());

	dbData.setGolConfig( this->GetGOLConf());
	
	UInt8 * apiValues = this->GetAPIConf();
	dbData.setApiDacRefHi( apiValues[0] );
	dbData.setApiDacRefMid( apiValues[1] );
	dbData.setApiGtlRefA( apiValues[2] );
	dbData.setApiGtlRefD( apiValues[3] );
	dbData.setApiAnTestHi( apiValues[4] );
	dbData.setApiAnTestLow( apiValues[5] );


	for (int pixel = 0 ; pixel < numberOfChips ; ++pixel){
		
		for (int dac = 0 ; dac < NPixelDacs ; ++dac){
			
			dbData.setPixelDac(pixel,dac,PxDAC[pixel][dac]);
		}

		// mask matrix
		//dbData.setNoisyPixelsVect( pixel, this->getNoisyPixelVector( pixel));
	}



	return 0;
}


// transforms the matrix into a nice vector with the noisy pixels
std::vector <PixelCoordinate> HSConfiguration::getNoisyPixelVector(UInt8 ChipN){
	
	std::vector <PixelCoordinate> noisyPixelsVect;

	// will scan the matrix converting and insert the noisy pixels in the vector 
	for(int i=0; i < numberOfRows; ++i){
	
		for(int  j=0; j < numberOfCols; ++j){
			UInt32 value = PxMASK[ChipN][i];
			// if there is a mask here 
			if((PxMASK[ChipN][i] >> j) & 1){

				PixelCoordinate noisyPix;
				noisyPix.row = (UInt8) j + 32 * (i % 8); //row
				noisyPix.column= (UInt8) i / 8; //column
				noisyPixelsVect.push_back(noisyPix);
				cout<<dec << "noisy pixel " << dec << (int)ChipN << " row  " <<(int) noisyPix.row << " col "<<(int) noisyPix.column <<"\n";
			}
		}
	}


	return noisyPixelsVect;

}
/*  The structure of ModeSel is:
    union{
        UInt32 ChipNumb :10,   //Select the chip that you want to include
               GOLSelec :1,
               APISelec :1,
               DPISelec :1,   
               GenerFunctionScan :4,  //GeneralFuntion:
                                         // 0 global register 
                                         // 1 global with DAC_OUT_EN
                                         // 2 set TP
                                         // 3 set mask  
        UInt32 ModeSel;
        };
    */
    

// converts the BUS temperature reading to degrees
double HSConfiguration::convertTempBUS( UInt32 digitalValue){

	UInt32 adcCount = extractFromInt32( digitalValue, 0, 10);
	double R = (1.379*adcCount + 513)/0.242;
	
	double T = (R-5000)/19.25;		//3.85*5 which is the pt1000 coeficient 

	return T;
}

// converts the MCM temperature reading to degrees
double HSConfiguration::convertTempMCM( Uint32 digitalValue){

	UInt32 adcCount = extractFromInt32( digitalValue, 10, 10);
	double R = (1.379*adcCount + 513)/0.242;

	double R0=4700.00;
	double T0 =298.15; // te formula is in kelvin so t0 is 25C�=298.15K
	double B=3500.00;

	double T= B/( log(R/R0) + B/T0 ) - 273.15 ; // this formula would return kelvins so that's why the -273.15

	return T;
}



// will set the noisy pixel matrix acording to the PVSS input
void HSConfiguration::SetNoisyPixelsVectJT(UInt32 chipSel, Uint32 * Matrix,  bool repeatFirstMatrix){
	const int numberOfChips = 10;
	Uint32 *matrixPos= Matrix;
	const int matrixSize = 256;

	for(unsigned chip =0; chip < numberOfChips; ++chip){
		if ( (chipSel >> chip) & 1 ){

			SetPxMASK(  matrixPos,  chip);
				// will increment the pointer for the next pixel chip in the chain
			if (repeatFirstMatrix == false ) matrixPos += matrixSize;
		}
	}
}


