#pragma once
#include "StdAfx.h"
#include ".\apiconvparam.h"
 
APIConvParam::APIConvParam(void){
}

APIConvParam::~APIConvParam(void){
}
 

void APIConvParam::LoadConversionParam(UInt8 * ConverStream){
	IPt = (float)UInt8ToUInt32(ConverStream) / 100;
	ConverStream += 4;
	
	GNDDiff = (float)UInt8ToUInt32(ConverStream + 4) / 100;
		
	VddMCM = ((float)UInt8ToUInt32(ConverStream) / 100 - GNDDiff) * 2;
	
	ConverStream += 8;
		
	TempTest = (float)UInt8ToUInt32(ConverStream) / 100;
	ConverStream += 4;


	for(int i = 0; i < 256; i++){
		DACRefHi[i] = (float)UInt8ToUInt32(ConverStream) / 100; //- GNDDiff;
		DACRefMid[i] = (float)UInt8ToUInt32(ConverStream + 1024) / 100;// - GNDDiff;
		DACGtlRef[i] = (float)UInt8ToUInt32(ConverStream  + 2048) / 100; // - GNDDiff;
		DACTestHi[i] = (float)UInt8ToUInt32(ConverStream + 3072) / 100; //- GNDDiff;
		DACTestLow[i] = (float)UInt8ToUInt32(ConverStream + 4096) / 100;// - GNDDiff;
		ConverStream += 4;
	}
	ConverStream += 4096;


	DeltaADCRefHi = (float)(ConverStream[0] ? -0.01 : 0.01);
    ConverStream ++;
	DeltaADCRefHi *= (float)UInt8ToUInt32(ConverStream);
	ConverStream += 4;
	
	DeltaADCRefMid = (float)(ConverStream[0] ? -0.01 : 0.01);  
	ConverStream ++;
    DeltaADCRefMid *= (float)UInt8ToUInt32(ConverStream);
	ConverStream += 4;
	
	DeltaADCGtlRef = (float)(ConverStream[0] ? -0.01 : 0.01);  
	ConverStream ++;
	DeltaADCGtlRef *= (float)UInt8ToUInt32(ConverStream);
	ConverStream += 4;

	DeltaADCTestHi = (float)(ConverStream[0] ? -0.01 : 0.01);  
	ConverStream ++;
	DeltaADCTestHi *= (float)UInt8ToUInt32(ConverStream);
	ConverStream += 4;

	DeltaADCTestLow = (float)(ConverStream[0] ? -0.01 : 0.01);  
	ConverStream ++;
	DeltaADCTestLow *= (float)UInt8ToUInt32(ConverStream);
	ConverStream += 4;
    

	for( int i = 0; i < 1024; i++){
        ADCSenseV[i] = (float)UInt8ToUInt32(ConverStream) / 100;// - GNDDiff; 
		ADCSenseI2[i] = (float)UInt8ToUInt32(ConverStream + 4096) / 100;// - GNDDiff;
		ConverStream += 4;
	}
	ConverStream += 4096;


	for( int i = 0; i < 400; i++){
		ADCPixelVdd[i] = ((float)UInt8ToUInt32(ConverStream) / 100)*2;// - GNDDiff) * 2;
		ConverStream += 4;
	}
}

void APIConvParam::CreateConversionParamStream(UInt8 * ConverStream){
	UInt32ToUInt8(ConverStream,(UInt32) IPt * 100);
	ConverStream += 4;
	
	UInt32ToUInt8(ConverStream,(UInt32) (VddMCM/2 + GNDDiff) * 100);
	ConverStream += 4;

	UInt32ToUInt8(ConverStream, (UInt32)GNDDiff* 100);
	ConverStream += 4;
		
	UInt32ToUInt8(ConverStream, (UInt32)TempTest* 100);
	ConverStream += 4;
	

	for(int i = 0; i < 256; i++){
		UInt32ToUInt8(ConverStream, (UInt32) DACRefHi[i] * 100); //- GNDDiff;
		UInt32ToUInt8(ConverStream + 1024,(UInt32) DACRefMid[i] * 100);// - GNDDiff;
		UInt32ToUInt8(ConverStream  + 2048,(UInt32) DACGtlRef[i] * 100); // - GNDDiff;
		UInt32ToUInt8(ConverStream + 3072,(UInt32) DACTestHi[i] * 100); //- GNDDiff;
		UInt32ToUInt8(ConverStream + 4096,(UInt32) DACTestLow[i] * 100);// - GNDDiff;
		ConverStream += 4;
	}
	ConverStream += 4096;


	ConverStream[0] = (DeltaADCRefHi < 0)  ? 1 : 0;
    ConverStream ++;
	UInt32ToUInt8(ConverStream,(UInt32) DeltaADCRefHi * 100);
	ConverStream += 4;
	
	ConverStream[0] = (DeltaADCRefMid < 0)  ? 1 : 0;
    ConverStream ++;
	UInt32ToUInt8(ConverStream, (UInt32)DeltaADCRefMid * 100);
	ConverStream += 4;

	ConverStream[0] = (DeltaADCGtlRef < 0)  ? 1 : 0;
    ConverStream ++;
	UInt32ToUInt8(ConverStream, (UInt32)DeltaADCGtlRef * 100);
	ConverStream += 4;

	ConverStream[0] = (DeltaADCTestHi < 0)  ? 1 : 0;
    ConverStream ++;
	UInt32ToUInt8(ConverStream,(UInt32) DeltaADCTestHi * 100);
	ConverStream += 4;

	ConverStream[0] = (DeltaADCTestLow < 0)  ? 1 : 0;
    ConverStream ++;
	UInt32ToUInt8(ConverStream, (UInt32)DeltaADCTestLow * 100);
	ConverStream += 4;
	
	
	for(int i = 0; i < 1024; i++){
        UInt32ToUInt8(ConverStream, (UInt32)ADCSenseV[i] * 100);// - GNDDiff; 
		UInt32ToUInt8(ConverStream + 4096,(UInt32) ADCSenseI2[i]* 100);// - GNDDiff;
		ConverStream += 4;
	}
	ConverStream += 4096;


	for(int i = 0; i < 400; i++){
		UInt32ToUInt8(ConverStream,(UInt32) ADCPixelVdd[i] * 50);// - GNDDiff) * 2;
		ConverStream += 4;
	}
}

/*
in the stream to class
4      bytes        IPt
4                     VddMCM                
4                     GNDdiff
4                     TemperatureTest
5120               DAC conv
5                     Delta ref Hi   
5                     Delta ref Mid
5                     Delta gtl  Ref
5                     Delta test Hi
5                     Delta test Low
4096               Sense V
4096               Sense I2
1600               Pixel Vdd/2


*/