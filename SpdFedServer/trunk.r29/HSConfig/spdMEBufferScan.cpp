#include "StdAfx.h"
#include ".\spdmebufferscan.h"

#include "SPDConfig.h"

spdMEBufferScan::spdMEBufferScan(SPDConfig * spdConf)
{
	this->parent = spdConf;
	this->log = &spdLogger::getInstance();

	this->useL1 = true;
	this->correctMisalign = true;
	this->active = false;
}

spdMEBufferScan::~spdMEBufferScan(void){
}

void spdMEBufferScan::scan(){
	//	if (this->active == false) return;
}


void spdMEBufferScan::performScan(){

		// check the MEB alignment   
	this->checkAllMEB();
	
	for (unsigned hstave = 0; hstave < nHStaves; hstave++){
			// error if the multi event buffer of a chip is misaligned 
		for (unsigned chip = 0; chip < nChips; chip++){
			
			if (checkResults[hstave][chip] > 0){
				log->log("ERROR: MEB of half stave %d, chip %d has misalignment of %d",
					hstave, chip, checkResults[hstave][chip]);
			}
		}
	}

		// the MEB is aligned depending on the correctMisalign option 
		// in this case we also check again the results
	if (correctMisalign == true){
		log->log("INFO: MEB misalignment correction will be done if needed");
				
		this->correctAllMEB();

			// check again the MEB alignment   
		this->checkAllMEB();
	}

	int router = -1;


	for (int router = 0 ; router < 10 ; router ++){
		if (parent->isRouterActive(router)){
			parent->routers[router]->writeControlReg(controlRegisters[router]);
			parent->routers[router]->resetEventsInDpm();

				// clean the test pulse
			this->setTpMatrixAllEnabled(0);
		}
	}

	if(correctMisalign == true){//check results again if checkAllMEB() was done for the second time
	    for (unsigned hstave = 0; hstave < nHStaves; hstave++){
			// error if the multi event buffer of a chip is misaligned 
		    for (unsigned chip = 0; chip < nChips; chip++){
			
			    if (checkResults[hstave][chip] > 0){
				    log->log("ERROR: MEB of half stave %d, chip %d has misalignment of %d",
					hstave, chip, checkResults[hstave][chip]);
			    }
		    }
	    }
	}

	this->timerScan.stop();
	log->log("Multi Event Buffer scan finished, time elapsed = %f", timerScan.elapsed_time());
	this->active = false;
}

UInt32 spdMEBufferScan::setTpMatrixAllEnabled( int currentTestPulse){
	UInt32 testPulseMatrix[256];
	for (unsigned int n = 0; n < 256; n++) testPulseMatrix[n] = 0;

	switch(currentTestPulse){
		case 0:			// empty matrix, used to clean the MEB
			break;
		case 1:
			spdGlobals::setRowInMatrix(testPulseMatrix, 128);			 
			break;
		default:
			log->log("ERROR: unknown test pulse, please check the MEB scan");
			return 1;
			break;
	}

	// will set the matrix to all half-staves enabled in test mode
	const UInt32 allChips =  0x3ff;
	for(int hs = 0; hs < nHStaves; hs++ ){
		if (parent->halfStaves[hs]->getChActStatus() == 2){		
			parent->halfStaves[hs]->LoadHSPixelMatrix(allChips,testPulseMatrix,true,true);
		}
	}

	return 0;
}

void spdMEBufferScan::checkAllMEB(void){

	SpdHitData readEvent;
	triggerNum = mebSize;

	for (unsigned routerNumber = 0; routerNumber < NRouters; routerNumber++ ){

		if (parent->isRouterActive(routerNumber)){

				// 4 triggers without test pulse to clean the memory
			this->setTpMatrixAllEnabled(0);
			parent->routers[routerNumber]->SendTrigger(triggerNum);

				// 4 triggers with test pulse
			this->setTpMatrixAllEnabled(1);
			parent->routers[routerNumber]->SendTrigger(triggerNum);

				// reads event from router memory
			log->log("Reading events from router #%d",routerNumber);
			readEvent = parent->routers[routerNumber]->readEventsFromMemory();
		}

			// check multi event buffer misalignment only for the hs in test
		for (unsigned hs = 0; hs < NHalfstavesInRouter; hs++ ){

			unsigned HSnumber = NHalfstavesInRouter*routerNumber+hs;

			if (parent->halfStaves[HSnumber]->getChActStatus() == 2){
				log->log("checking MEB status for HS #%d",HSnumber);
 				this->checkMebForHs(readEvent, HSnumber);                   
			}
		}
	}
}

void spdMEBufferScan::checkMebForHs(SpdHitData &readEvent, unsigned hs){
	int sizeLowLimit = 20;
	unsigned hsInRouter = hs%NHalfstavesInRouter;

		// check the size of the event per chip
	log->log("readEvent.size() = %d",readEvent.size());
	for (unsigned chip = 0; chip < nChips; chip ++){
		log->log("Chip #%d",chip);
		//check first if there is enough events
		
		// 4 means that the value was never found
		int oldcheckResults = checkResults[hs][chip];
		checkResults[hs][chip]=4;
		for (unsigned meb = 0; meb < mebSize; meb++){
			int hitDimensions;
			
			if (readEvent.size() > mebSize+meb){
				hitDimensions = (int)readEvent[mebSize+meb][hsInRouter][chip].size();
			}
			else{
				log->log("ERROR: not enough events were read for MEB scan in hs:%d chip:%d (meb=%d). Skipping it.", hs, chip, meb);
				checkResults[hs][chip]=oldcheckResults;
				return;
			}
			log->log("hitDimensions = %d at meb = %d",hitDimensions,meb);
			if (hitDimensions > sizeLowLimit){
				checkResults[hs][chip] = meb;

				if (meb > 0){
					log->log("ERROR: misalignment hs:%d, chip:%d, value:%d", hs, chip, checkResults[hs][chip]);
				}
				break;
			}
			else if(mebSize-meb==1){
				log->log("ERROR: value not found in hs:%d, chip:%d, value:%d",hs,chip,checkResults[hs][chip]);
			}
		}
	}
}

void spdMEBufferScan::correctAllMEB(void){

	/*	the triggers are sent to an entire half stave
		the correction is done considering the first misaligned chip
	*/
	bool enabledHS[6];
	bool tp;
	bool l1;
	bool l2y;
	bool l2n;
	int router=-1;

	for (unsigned hstave = 0; hstave < nHStaves; hstave++){
		unsigned misalignment = getMisalignmentForHs(hstave);
		
		if ( misalignment > 0){
			log->log("Correcting misalignment in hs:%d with useL1=%d",hstave,useL1);
			unsigned routerNum = hstave/NHalfstavesInRouter;

			if (router == routerNum)break;

			unsigned HSinRouter = hstave%NHalfstavesInRouter;
			enabledHS[HSinRouter] = true;

			if (useL1 == true){			// correction with L1
				tp = true;
				l1 = true;
				l2y = false;
				l2n = false;
				triggerNum = 4 -misalignment;
				log->log("Sending %d triggers",triggerNum);
				parent->routers[routerNum]->SendTrigger(triggerNum, true, l2y, l1, tp, l2n);
				
			}
			else {						// correction with L2n
				tp = false;
				l1 = false;
				l2y = false;
				l2n = true;
				triggerNum = misalignment;
				log->log("Sending %d triggers",triggerNum);
				parent->routers[routerNum]->SendTrigger(triggerNum, true, l2y, l1, tp, l2n);
			}

			router = routerNum;
		}
	}

	parent->DataReset_All();
	parent->resetInternalDPI_All();
}

int spdMEBufferScan::getMisalignmentForHs(unsigned hs){
	for (unsigned chip = 0 ; chip < nChips; chip ++){
        if (checkResults[hs][chip] > 0 ) return checkResults[hs][chip];
	}
	return 0;
}

	//! method to start the MEB scan
unsigned spdMEBufferScan::start(bool correct, bool l1){

	if (this->parent->isScanActive() == true){
		log->log("ERROR in MEB scan: another scan is already active");
		return 1;
	}

	timerScan.start("Starting multi event buffer scan");

	this->useL1 = l1;
	this->correctMisalign = correct;

	int router = -1;

	for(int router = 0 ; router < 10 ; router ++){
		if (parent->isRouterActive(router)){

			parent->routers[router]->resetEventsInDpm();

				// read and store the control register of the router
			controlRegisters[router] = parent->routers[router]->readControlReg();

			bool dpmSampleMode=true;
			bool noDataToDaq=true;
			bool busy=false;
			bool enTpL0=false;
			bool stopAllSM=false;
			bool enRouterHeader=false;
			bool enOrbitCounter=false;
			bool excludeTTC=true;
	
			this->parent->routers[router]->writeControlReg(  dpmSampleMode,
																 noDataToDaq,
																 busy,
																 enTpL0,
																 stopAllSM,
																 enRouterHeader,
																 enOrbitCounter,
																 excludeTTC );
		}
	}

		// initialize the results array to -1
	for (unsigned hstave = 0; hstave < nHStaves; hstave++){
		for (unsigned chip = 0; chip < nChips; chip++) {
			checkResults[hstave][chip] = -1;
		}
	}


	this->active = true;
	return 0;
}

	//! method to restart the MEB scan
unsigned spdMEBufferScan::restart(void){
	if (this->parent->isScanActive() == true){
		log->log("ERROR in MEB scan: another scan is already active");
		return 1;
	}

	this->active = true;
	this->scan();
	return 0;
}


int spdMEBufferScan::getChipMisalignment( unsigned hs, unsigned chip){
	if (hs >= nHStaves ){
		log->log("ERROR: halfstave %d is out of range", hs);
		return -1;
	}
	if (chip >= nChips ){
		log->log("ERROR: chip %d is out of range", chip);
		return -1;
	}

	return this->checkResults[hs][chip];
}