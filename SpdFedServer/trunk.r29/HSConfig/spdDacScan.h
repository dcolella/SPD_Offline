#pragma once

#include "spdScan.h"

#include "..\VMEAcc\VMEAccess.h"
#include "stdafx.h"
#include "../spdDbConfLib/timer.h"
#include "../spdDbConfLib/spdLogger.h"


class SPDConfig;

//!Class to manage the generic dac scan of the detector*/
class SpdDacScan : public SpdScan
{    
	//! stores if the channel is active or not
	bool active;
	//! stores the parameters of a dac scan
	DACParameters dac;
	//! logger of the class
	spdLogger *log;
	//! stores if we use internal trigger or not
	bool internalTrigger;
	//! stores the number of triggers
    UInt32 triggerN;
	//! keeps the chip selection for this scan (10 bits)
	UInt32 chipSelect;

	//! waiting time between steps
	UInt32 waitTime;
	//! this is a constant and should be 2 for a dac scan
    UInt8 scanType;

	//! parent SPDConfig to handle the hardware
	SPDConfig *parent;

	// will write the router header for this scan
	void SetScansHeader(UInt8 routerNumber);
public:

		/*! Method to start the dac scan
			\param triggerNumber number of triggers to send
			\param dacMin dac value to start the scan
			\param dacMax dac value for the end of the scan
			\param step the number of steps of the scan
			\param dacIdentifier index identifying the pixel dac for the scan
			\param InternTrig enables internal trigger of not
			\param waitTime time to wait between steps
		*/
	unsigned start( UInt32 triggerNumber, 
				   UInt8 dacMin, 
				   UInt8 dacMax, 
				   UInt8 step, 
				   UInt8 dacIdentifier, 
				   bool InternTrig, 
				   UInt32 waitTime);

		//! Method to restart the dac scan
	unsigned restart(void);

		//! Method to stop the dac scan
	void stop(){this->active = false;};
	
		//! Method to lead the dac scan
	void scan();
		//! method to get if the scan is active or not 
	bool isActive(){return this->active;};

	//	// this exists just to restart the scan
	//void setActive(bool value){this->active = value;};

	//! sets the chip selected
	void setChipSelected(UInt32 value){this->chipSelect = value;};
	//! gets the chip selected 
	UInt32 getChipSelected(void){return this->chipSelect;};

	//! constructor
	SpdDacScan(SPDConfig * spdConf);
	~SpdDacScan(void);

	friend class SpdMinThresScan;

};
