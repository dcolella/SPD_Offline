#pragma once
#include "StdAfx.h"
#include ".\hsjtagstreams.h"

HSJTAGStreams::HSJTAGStreams(void){
	SkipMode = 0;
	ModeSel = 0x7fffff;
	
	// will initialize all internal sequences
	IRIstrucVect[IRSelEnable][0]= GOL_CONF_RW;
	IRIstrucVect[IRSelEnable][1]= API_CONF_RW;
	IRIstrucVect[IRSelEnable][2]= SelENBL; 
	IRIstrucVect[IRSelEnable][3]= SelENBL; 
	IRIstrucVect[IRSelEnable][4]= SelENBL; 
	IRIstrucVect[IRSelEnable][5]= SelENBL; 
	IRIstrucVect[IRSelEnable][6]= SelENBL; 
	IRIstrucVect[IRSelEnable][7]= SelENBL; 
	IRIstrucVect[IRSelEnable][8]= SelENBL; 
	IRIstrucVect[IRSelEnable][9]= SelENBL; 
	IRIstrucVect[IRSelEnable][10]= SelENBL; 
	IRIstrucVect[IRSelEnable][11]= SelENBL; 
	IRIstrucVect[IRSelEnable][12]= DPI_CONF_RW;

	IRIstrucVect[IRSelGlobal][0]= GOL_CONF_RW;
	IRIstrucVect[IRSelGlobal][1]= API_CONF_RW;
	IRIstrucVect[IRSelGlobal][2]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][3]= SelGLOBAL;  
	IRIstrucVect[IRSelGlobal][4]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][5]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][6]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][7]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][8]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][9]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][10]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][11]= SelGLOBAL; 
	IRIstrucVect[IRSelGlobal][12]= DPI_CONF_RW; 

	IRIstrucVect[IRSelTM][0]= GOL_CONF_RW;
	IRIstrucVect[IRSelTM][1]= API_CONF_RW;
	IRIstrucVect[IRSelTM][2]= SelTM; 
	IRIstrucVect[IRSelTM][3]= SelTM;  
	IRIstrucVect[IRSelTM][4]= SelTM; 
	IRIstrucVect[IRSelTM][5]= SelTM; 
	IRIstrucVect[IRSelTM][6]= SelTM; 
	IRIstrucVect[IRSelTM][7]= SelTM; 
	IRIstrucVect[IRSelTM][8]= SelTM; 
	IRIstrucVect[IRSelTM][9]= SelTM; 
	IRIstrucVect[IRSelTM][10]= SelTM; 
	IRIstrucVect[IRSelTM][11]= SelTM; 
	IRIstrucVect[IRSelTM][12]= DPI_CONF_RW; 
	
	IRIstrucVect[IRIdCode][0]= IDCODE;
	IRIstrucVect[IRIdCode][1]= IDCODE;
	IRIstrucVect[IRIdCode][2]= BYPASSPix; 
	IRIstrucVect[IRIdCode][3]= BYPASSPix;  
	IRIstrucVect[IRIdCode][4]= BYPASSPix; 
	IRIstrucVect[IRIdCode][5]= BYPASSPix; 
	IRIstrucVect[IRIdCode][6]= BYPASSPix; 
	IRIstrucVect[IRIdCode][7]= BYPASSPix; 
	IRIstrucVect[IRIdCode][8]= BYPASSPix; 
	IRIstrucVect[IRIdCode][9]= BYPASSPix; 
	IRIstrucVect[IRIdCode][10]= BYPASSPix; 
	IRIstrucVect[IRIdCode][11]= BYPASSPix; 
	IRIstrucVect[IRIdCode][12]= IDCODE;

	IRIstrucVect[IRbypass][0]= BYPASS;
	IRIstrucVect[IRbypass][1]= BYPASS;
	IRIstrucVect[IRbypass][2]= BYPASSPix; 
	IRIstrucVect[IRbypass][3]= BYPASSPix;  
	IRIstrucVect[IRbypass][4]= BYPASSPix; 
	IRIstrucVect[IRbypass][5]= BYPASSPix; 
	IRIstrucVect[IRbypass][6]= BYPASSPix; 
	IRIstrucVect[IRbypass][7]= BYPASSPix; 
	IRIstrucVect[IRbypass][8]= BYPASSPix; 
	IRIstrucVect[IRbypass][9]= BYPASSPix; 
	IRIstrucVect[IRbypass][10]= BYPASSPix; 
	IRIstrucVect[IRbypass][11]= BYPASSPix; 
	IRIstrucVect[IRbypass][12]= BYPASS;
	
	IRIstrucVect[IRfree][0]= BYPASS;
	IRIstrucVect[IRfree][1]= BYPASS;
	IRIstrucVect[IRfree][2]= BYPASSPix; 
	IRIstrucVect[IRfree][3]= BYPASSPix;  
	IRIstrucVect[IRfree][4]= BYPASSPix; 
	IRIstrucVect[IRfree][5]= BYPASSPix; 
	IRIstrucVect[IRfree][6]= BYPASSPix; 
	IRIstrucVect[IRfree][7]= BYPASSPix; 
	IRIstrucVect[IRfree][8]= BYPASSPix; 
	IRIstrucVect[IRfree][9]= BYPASSPix; 
	IRIstrucVect[IRfree][10]= BYPASSPix; 
	IRIstrucVect[IRfree][11]= BYPASSPix; 
	IRIstrucVect[IRfree][12]= BYPASS;
			
	IRIstrucVect[IRRead][0]= GOL_CONF_R;
	IRIstrucVect[IRRead][1]= API_CONF_R_DAC;
	IRIstrucVect[IRRead][2]= SelGLOBAL; 
	IRIstrucVect[IRRead][3]= SelGLOBAL;  
	IRIstrucVect[IRRead][4]= SelGLOBAL; 
	IRIstrucVect[IRRead][5]= SelGLOBAL; 
	IRIstrucVect[IRRead][6]= SelGLOBAL; 
	IRIstrucVect[IRRead][7]= SelGLOBAL; 
	IRIstrucVect[IRRead][8]= SelGLOBAL; 
	IRIstrucVect[IRRead][9]= SelGLOBAL; 
	IRIstrucVect[IRRead][10]= SelGLOBAL; 
	IRIstrucVect[IRRead][11]= SelGLOBAL; 
    IRIstrucVect[IRRead][12]= DPI_CONF_R;
	
	IRIstrucVect[IRStartConv][0]= BYPASS;
	IRIstrucVect[IRStartConv][1]= API_START_CONV;
	IRIstrucVect[IRStartConv][2]= BYPASSPix; 
	IRIstrucVect[IRStartConv][3]= BYPASSPix;  
	IRIstrucVect[IRStartConv][4]= BYPASSPix; 
	IRIstrucVect[IRStartConv][5]= BYPASSPix; 
	IRIstrucVect[IRStartConv][6]= BYPASSPix; 
	IRIstrucVect[IRStartConv][7]= BYPASSPix; 
	IRIstrucVect[IRStartConv][8]= BYPASSPix; 
	IRIstrucVect[IRStartConv][9]= BYPASSPix; 
	IRIstrucVect[IRStartConv][10]= BYPASSPix; 
	IRIstrucVect[IRStartConv][11]= BYPASSPix; 
	IRIstrucVect[IRStartConv][12]= BYPASS;
	

	IRIstrucVect[JTAG_RESET][0]= BYPASS;
	IRIstrucVect[JTAG_RESET][1]= BYPASS;
	IRIstrucVect[JTAG_RESET][2]= RstPixel; 
	IRIstrucVect[JTAG_RESET][3]= RstPixel;  
	IRIstrucVect[JTAG_RESET][4]= RstPixel; 
	IRIstrucVect[JTAG_RESET][5]= RstPixel; 
	IRIstrucVect[JTAG_RESET][6]= RstPixel; 
	IRIstrucVect[JTAG_RESET][7]= RstPixel; 
	IRIstrucVect[JTAG_RESET][8]= RstPixel; 
	IRIstrucVect[JTAG_RESET][9]= RstPixel; 
	IRIstrucVect[JTAG_RESET][10]= RstPixel; 
	IRIstrucVect[JTAG_RESET][11]= RstPixel; 
	IRIstrucVect[JTAG_RESET][12]= BYPASS;


	DPIConfTemp = NULL;
	APIConfTemp = NULL;
	JTOperation = 5;
	ScanVector = NULL;

}

HSJTAGStreams::~HSJTAGStreams(void){
}
 

UInt32** HSJTAGStreams::SetJTSDpiConfReg( UInt32 * DpiReg, UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [2];
    
	UInt32 aux;

	ScanVector = IRIstrucVect[IRSelGlobal];
	ModeSel = (pixInChain << 13) + 0x1000;
	DPIConfTemp = DpiReg;

	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator(&aux , 1);

	APIConfTemp = NULL;

	return VectorsOut;
}


UInt32* HSJTAGStreams::GetJTSResetPixel( UInt32 ChipSelect, UInt32 pixInChain){

	ScanVector = IRIstrucVect[JTAG_RESET];
	ModeSel = (pixInChain << 13) + (ChipSelect << 2);
		
	return IRGenerator();

}


UInt32** HSJTAGStreams::SetJTSDpiInternalReg( UInt32 * DpiReg, UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [2];
    
	UInt32 aux;

    IRIstrucVect[IRfree][12]= DPI_INT_RW;
	ScanVector = IRIstrucVect[IRfree];
	ModeSel = (pixInChain << 13) + 0x1000;
	DPIConfTemp = DpiReg;

	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator(&aux , 1);
	DPIConfTemp = NULL;
	return VectorsOut;
}


UInt32** HSJTAGStreams::GetJTSDpiConfReg( UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [2];
    UInt32  DpiReg[2];

	UInt32 aux;

	ScanVector = IRIstrucVect[IRRead];
	ModeSel = (pixInChain << 13) + 0x1000;
	DPIConfTemp = DpiReg;
	
	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator(&aux , 1);

	DPIConfTemp = NULL;
	return VectorsOut;
}


UInt32** HSJTAGStreams::SetJTSApiDAC( UInt8 * ApiDac, UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [2];
    
	UInt32 aux;

	ScanVector = IRIstrucVect[IRSelEnable];
	ModeSel = (pixInChain << 13) + 0x2;

	//cout << ModeSel << endl; //2014.03.06 - commented out by Senyukov

	/* 	this flags an non existing error in cppcheck 
		( Mismatching allocation and deallocation: HSJTAGStreams::APIConfTemp)
	*/
	//if (APIConfTemp != NULL) delete APIConfTemp;
	APIConfTemp = ApiDac;
	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator(&aux , 1);
	APIConfTemp = NULL;
	return VectorsOut;
}


UInt32** HSJTAGStreams::SetJTSApiADCStartConv( UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [1];
    

	//IRIstrucVect[IRfree][1] = API_START_CONV;
	ScanVector = IRIstrucVect[IRStartConv];
	ModeSel = (pixInChain << 13) + 0x2;
	VectorsOut[0] = IRGenerator();
	return VectorsOut;
}

UInt32** HSJTAGStreams::GetJTSApiADC( UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [2];
    
	UInt32 aux;

	IRIstrucVect[IRfree][1] = API_CONF_R_ADC;
	ScanVector = IRIstrucVect[IRfree];
	ModeSel = (pixInChain << 13) + 0x2;

	/* very , very ugly Ivan is using this APIConfTemp
		and using it inside the DRGenerator function
		this was not deleted at all and has to be deleted here

		this flags an non exixting error in cppcheck 
		( Mismatching allocation and deallocation: HSJTAGStreams::APIConfTemp)
	*/
	//if (APIConfTemp != NULL) delete APIConfTemp;
	APIConfTemp = new UInt8[17 * 4];
	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator(&aux , 1);


	delete APIConfTemp;
	APIConfTemp = NULL;
	return VectorsOut;
}


UInt32** HSJTAGStreams::GetJTSApiDAC( UInt32 pixInChain){   
    UInt32 ** VectorsOut = new UInt32* [2];
    
	UInt32 aux;
	
	ScanVector = IRIstrucVect[IRRead];
	ModeSel = (pixInChain << 13) + 0x2;
	
	//if(APIConfTemp != NULL)delete APIConfTemp;
	APIConfTemp = new UInt8[6];
	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator(&aux , 1);

	/*	this flags an non exixting error in cppcheck 
		( Mismatching allocation and deallocation: HSJTAGStreams::APIConfTemp)
	*/
	delete APIConfTemp;
    APIConfTemp = NULL;
	return VectorsOut;
}



UInt32** HSJTAGStreams::SetJTSPixelMatrix( UInt32 ChipSelect, UInt32 * Matrix, bool All, bool TP, UInt32 pixInChain){
	UInt32 ** VectorsOut = new UInt32* [66];
    	

	ModeSel = (pixInChain << 13) + (ChipSelect << 2);

	if(TP) JTOperation = SET_TP;
	else JTOperation = SET_MASK;

	ScanVector = IRIstrucVect[IRSelEnable];
	VectorsOut[0] = IRGenerator();
    ScanVector = IRIstrucVect[IRSelTM];
	VectorsOut[1] = IRGenerator();

    for(UInt32 i = 0; i< 32 ; i++){

		ScanVector = IRIstrucVect[IRSelEnable];
		VectorsOut[i+2] = DRGenerator(&i, 1); 
		ScanVector = IRIstrucVect[IRSelTM];

		if(All){
			VectorsOut[i+34] = DRGenerator(Matrix + (i*8), All);
		} else {
			UInt32 * MatrixToSend = new UInt32 [80];
			for(int j=0; j<80; j++){
				MatrixToSend[j] = Matrix[i*8 + (j/8)*256 + (j%8)];
			}
			VectorsOut[i+34] = DRGenerator(MatrixToSend, 0);
		    delete [] MatrixToSend;
		}	
    }
	return VectorsOut;
}



UInt32** HSJTAGStreams::SetJTSPixelDAC( UInt8 * DACn, UInt8 * DACValue, UInt32 ChipSelect, bool All, UInt32 pixInChain){                                                                           
	/*  if All = 1 then all the active chips are loaded with the same DACValue
		ChipSelect defines the chips where the operation is done	
	*/

	UInt32 ** VectorsOut = new UInt32 *[4];
    
	ModeSel = (pixInChain << 13) + (ChipSelect << 2);
	JTOperation = DAC_SET;
	ScanVector = IRIstrucVect[IRSelEnable];
	VectorsOut[0] = IRGenerator();
	VectorsOut[1] = DRGenerator((UInt32 *)DACn, true); 
    ScanVector = IRIstrucVect[IRSelGlobal];
	VectorsOut[2] = IRGenerator();    
	
	// we need to copy from the UInt8 vector a casting will not work
	UInt32 dacValuesAux[numberOfChips];
	if (All == false){
		for (int n = 0 ; n < numberOfChips ; n ++){
			dacValuesAux[n] = DACValue[n] & 0xff;
		}
	}
	else{
		dacValuesAux[0] =  DACValue[0];
	}
	VectorsOut[3] = DRGenerator( dacValuesAux, All); 
	
	return VectorsOut;
}




UInt32** HSJTAGStreams::SetJTSPixelAllDAC( UInt32 ChipSelect, UInt8 * DACVect, UInt32 pixInChain){ 
	//DACVect is the 440 dac vector
	UInt32 ** VectorsOut = new UInt32* [90];
	UInt32 DACValue[10];

	ModeSel = (pixInChain << 13) + (ChipSelect << 2);
	JTOperation = DAC_SET;
	ScanVector = IRIstrucVect[IRSelEnable];
	VectorsOut[0] = IRGenerator();
    ScanVector = IRIstrucVect[IRSelGlobal];
	VectorsOut[1] = IRGenerator();
	
	for(UInt32 i = 0; i< 44 ; i++){
		for(UInt32 j = 0; j<10; j++){
			DACValue[j] = DACVect[j*44 + i];  
		}
		ScanVector = IRIstrucVect[IRSelEnable];
		VectorsOut[i+2] = DRGenerator(&i, 1); 
		ScanVector = IRIstrucVect[IRSelGlobal];
	    VectorsOut[i+46] = DRGenerator((UInt32 *)DACValue, 0); 
	}
	return VectorsOut;
}




UInt32 HSJTAGStreams::GetInstruction(UInt8 position){
	UInt32 SelCode = 0x1;
	SelCode <<= position;
	if(ModeSel & SelCode) return ScanVector[position];
	return IRIstrucVect[IRbypass][position];
}

UInt32 * HSJTAGStreams::ExtractData(UInt32 * Stream, UInt32 StartPosition, UInt32 Type){
	
	UInt32 bitInStartWord, StrStartPosition, VecLenght;
    int Vec32;

	StrStartPosition = StartPosition/32;
	bitInStartWord =  StartPosition % 32;
	if(Type ==DPIConf){
		VecLenght = 2;
		Vec32 = 2;
	} else if(Type ==DPIInternal){
		VecLenght = 3;
		Vec32 = 3;
	} else if(Type == API_DAC){
		VecLenght = 6;
		Vec32 = 2;
	}else if (Type == API_ADC){
		VecLenght = 17;
		Vec32 = 6;
	}else if(Type == PIX_COLUMN){
		VecLenght = 8;
		Vec32 = 8;
	} else if(Type == PIX_DAC){
		VecLenght = 1;
		Vec32 = 1;
	} else if(Type == GOL || Type ==ID_CODE){
		VecLenght = 1;
		Vec32 = 2;
	}
	
	UInt32 * VectOut = new UInt32 [VecLenght];

	for(int i = 0; i < Vec32; ++i){ 
		VectOut[i]= Stream[StrStartPosition+i] >> bitInStartWord |
	                 Stream[StrStartPosition+i+1] << (32 - bitInStartWord);
		}
	
	if(Type ==DPIConf){
		VectOut[1] = VectOut[1] & 0x1ffff;
	}else if(Type ==DPIInternal){
		VectOut[2] = VectOut[2] & 0x1;
	} else if(Type == API_DAC){
		for(int i = 5; i >= 0; --i){
			VectOut[i] = (VectOut[i/4] >> ((i%4)*8))& 0xff;
		}
	}else if (Type == API_ADC){

		UInt32 WordNumber, BitInWord, BitNumber = 0;
		UInt32 * aux = new UInt32 [Vec32];

		for(int i =0; i < Vec32; ++i) aux[i] = VectOut[i];
		
		for(int i = 0 ; i < 17 ; ++i){

            WordNumber = BitNumber / 32;
			BitInWord= BitNumber % 32;

			if((BitInWord + 10) > 32){
               UInt32 BitsToEnd = 32 - BitInWord; 
               VectOut[i] = ((aux[WordNumber] >> BitInWord) + (aux[WordNumber +1] << BitsToEnd)) & 0x3ff;
			}else{
				VectOut[i] = (aux[WordNumber] >> BitInWord) & 0x3ff;
			}
			BitNumber += 10; 
		}

		delete[] aux;
	} else if(Type == PIX_DAC){
		VectOut[0] = VectOut[0]& 0xFF;
	}
	
	return VectOut;    
}

UInt32 HSJTAGStreams::AppendData(UInt32 * Stream, UInt32 bitNumb, UInt32 * DataToAppend){

	UInt32 MaxVal = bitNumb / 32;
	UInt32 bitInCycle = 0, i, k;
	if((bitNumb % 32) != 0) MaxVal++;

	for(UInt32 j = 0; j < MaxVal; j++){ 
		if(((bitNumb % 32) != 0) && (j == MaxVal-1)){
			bitInCycle = bitNumb % 32;
		}else{
			bitInCycle = 32;
		}

		i = this->bitNumber / 32;
        k = this->bitNumber % 32;
       	Stream[i] = (DataToAppend[j] << k) | Stream[i];
	    if (bitInCycle > (32 - k))Stream[i+1] = (DataToAppend[j] >> (32 - k)) | Stream[i+1];		
		this->bitNumber += bitInCycle;
	}
	
	return 0;
}



UInt32 * HSJTAGStreams::IRGenerator(){                 //this function requires to be set before:
															//ScanVector
															//SkipMode
															//modesel
	
	UInt32 * VectorOut = new UInt32 [3];        //ModeSel: 23 bit chip: bypass(0) or not(1)
												//	bit 0 =	gol,
												//	bit 1 = api,
												//  bit 2..11 = 10 chip 
												//	bit 12 = dpi
												//	bit 13..22 chips in chain (13 = chip 0 ...)
	bitNumber = 0;
	UInt32 Instruction;
	int i;
    
	for(i =0; i<3; i++){
		VectorOut[i]=0;
	}

	if (SkipMode < 2){
        
		Instruction = GetInstruction(0);				//GOL 
		AppendData(VectorOut + 1, 5, &Instruction);
               
		Instruction = GetInstruction(1);				//API 
		AppendData(VectorOut + 1, 5, &Instruction);
	} 
    
    

    if (SkipMode != 1 && SkipMode != 3){				//Pixels
      UInt32 ChipPresTemp = 0x400000;
	  for (i=9; i>=0; --i){
        if (ModeSel & ChipPresTemp){
          Instruction = GetInstruction((UInt8)(i+2));
          AppendData(VectorOut + 1, 4, &Instruction);
		}
        ChipPresTemp >>= 1;  
	  }    
    }
    
       
    Instruction = GetInstruction(12);				//DIP  
	AppendData(VectorOut + 1, 5, &Instruction);
   
	VectorOut[0] = bitNumber;
	return VectorOut;
}




UInt32 * HSJTAGStreams::DRGenerator(UInt32 * DataIn, bool ConstValue){ 
	//ConstValue force to take to all the chip the DataIn[0]
	//this function requires to be set before:
		//ScanVector
		//SkipMode
		//ModeSel
		//JTOperation  if operation on pixel chip and IR=selEnable
	    //chnumber if use mcm chips conf.
    
    //tell the genaral operation that we want do  
    // imp only with sel see ModeSel descr
    //imp: when you dend TM or global, pDataIn must
    //have the value of the preceding DR instruction
    // in ex. if prec Datain was 32 (colum) also
    UInt32 Istruction;                                                                    //this one must be 32.
	int i, j;

		// wtf is this, this register will never be deleted!!!!!!!!
		// why to define a pointer and a new instead of declaring a UInt32
		// a memory leak of one byte is still a byte
    //UInt32  *Data = new UInt32;
	UInt32  Data;
	UInt32 * DRVect = new UInt32 [120];
	
	for(i =0; i<120; i++){
		DRVect[i]=0;
	}
													
	bitNumber = 0;										//pDataIn can be also only 10 because it is for the 10 chips   
                                                                        //also ModeSel must remain equivalent                                                                                       
    if (SkipMode < 2){
        
        Istruction = GetInstruction(0);           //GOL 
        if(Istruction == BYPASS){
			Data = 1;
            AppendData(DRVect + 1, 1, &Data);
		} 
        else if(Istruction == GOL_CONF_RW || Istruction == GOL_CONF_R || Istruction == IDCODE){
			AppendData(DRVect + 1, 32, &GOLConfTemp);                              //GOLConf
		} 
        else {
          cerr << "No defined command in IRFunctVect[0] for GOL" << endl;      
          return 0;
        }    
        
        
             
	    Istruction = GetInstruction(1);      //API
        if (Istruction == BYPASS){
            Data = 1;
            AppendData(DRVect + 1, 1, &Data);
		 } else if( Istruction == API_CONF_R_DAC || Istruction == API_CONF_RW){
          for(i=0; i <6; i++){    
			  AppendData(DRVect + 1, 8, (UInt32*)(APIConfTemp + i));
		  }
	    } else if(Istruction == API_CONF_R_ADC){
			UInt32 VectToAppend[6] = {0,0,0,0,0,0};
				
            AppendData(DRVect + 1, 170, VectToAppend); 
			
		} else if(Istruction == IDCODE){
			Data = 0xffffffff;
            AppendData(DRVect + 1, 32, &Data);
		}else {
          cerr << "No defined command IRFunctVect[1] for API" << endl;      
          return 0;
        }
    } 
    
    

    if (SkipMode != 1 && SkipMode != 3){		//Pixels
      UInt32 ChipPresTemp = 0x400000;
	  j=0;
	  for (i=9; i>=0; --i){
        if(!ConstValue) j = i;
        if (ModeSel & ChipPresTemp){
		  Istruction = GetInstruction((UInt8)(i+2));
          if (Istruction == BYPASSPix){
			 Data = 1;
             AppendData(DRVect + 1, 1, &Data);
		   } else if(Istruction == SelENBL){
             switch (JTOperation){
                case DAC_SET:
				  Data = DataIn[j] & 0x3f;                // global egister  (Dac Numb)
                break;  
                case DAC_SET_SENSE:                                                           
                  Data = (DataIn[j] & 0x3f)| 0x40;         //global reg with DAC_OUT_EN (Dac Numb)
                break;
                case SET_TP:
                  Data = (DataIn[j] & 0x1f) | 0x20;         //set TP (Column Number)
                break;
                case SET_MASK:
                  Data = (DataIn[j] & 0x1f) | 0x40;         //mask pixel (Column Number)
                break;
                default:;
             }
			 AppendData(DRVect + 1, 7, &Data);
          }    
          else if( Istruction == SelGLOBAL){
             DataIn[j] = DataIn[j] &0xff;
			 AppendData(DRVect + 1, 8, DataIn + j);
		  }
          else if( Istruction == SelTM){
             AppendData(DRVect + 1, 256, DataIn + (UInt32)(j*8));      //each 8 UInt32 there is a column
          }   
          else {
            cerr << "No defined command IRFunctVect[1] For Pixels" << endl;      
            return 0;
          }
        }  
        ChipPresTemp >>= 1;          
      }    
    } 
    
        
    Istruction = GetInstruction(12);  
    if (Istruction == BYPASS){
		Data = 1;	   
        AppendData(DRVect + 1, 1, &Data);
    }else if( (Istruction == DPI_CONF_RW) || (Istruction == DPI_CONF_R)){
          for(i=0; i <3; i++){

			AppendData(DRVect + 1, 32, &DPIConfTemp[0] );
            AppendData(DRVect + 1, 17, &DPIConfTemp[1] );

          }
	}else if(Istruction == DPI_INT_RW){
	       for(i=0; i <3; i++){
    			AppendData(DRVect + 1, 32, &DPIConfTemp[0] );
                AppendData(DRVect + 1, 32, &DPIConfTemp[1] );
				AppendData(DRVect + 1, 1, &DPIConfTemp[2] );
          }
	}else if(Istruction == IDCODE){
			Data = 0xffffffff;
            AppendData(DRVect + 1, 32, &Data);
    } else {
          cerr << "No defined command IRFunctVect[1] for DPI" << endl;      
          return 0;
    }
  
  DRVect[0] = bitNumber;
  
  return DRVect;
}


UInt32 ** HSJTAGStreams::DecodeJTStream(UInt32 * DRVect){ 
     										//this function requires to be set before:
	     									//ScanVector
											//SkipMode
											//modesel
											//JTOperation  if operation on 
	                                        //pixel chip and IR=selEnable
	                                        //chnumber if use mcm chips conf.
    
                                     //tell the genaral operation that we want do  
                                     // imp only with sel see ModeSel descr
                                     //imp: when you dend TM or global, pDataIn must
                                     //have the value of the preceding DR instruction
                                     // in ex. if prec Datain was 32 (colum) also this one must be 32
    UInt32  Instruction;  
	int i;
    UInt32 ** DataOut = new UInt32*[14];

	for(i =0; i<14; i++){
		DataOut[i]=NULL;
	}

	UInt32 * DecodedElements = new UInt32 [1];	

	DataOut[0] = DecodedElements;
	DecodedElements[0] = 0;
	UInt32 StreamPosition = 0;			                                                                                       
                     
		
    if (SkipMode < 2){
        //////////// GOL //////////////
        Instruction = GetInstruction(0);            
        if(Instruction == BYPASS){
			StreamPosition++;
		} 
        else if(Instruction == GOL_CONF_RW || Instruction == GOL_CONF_R || Instruction == IDCODE){
			DataOut[1] = ExtractData(DRVect, StreamPosition, GOL); 
			StreamPosition+= 32;
			DecodedElements[0] = DecodedElements[0] | 0x1;
		} 
        else {
          cerr << "No defined command in IRFunctVect[0]" << endl;      
          return 0;
        }    
        
		////////////// API ///////////////
        Instruction = GetInstruction(1);     
        if (Instruction == BYPASS){
            StreamPosition++;
		} else if(Instruction == API_CONF_R_DAC || Instruction == API_CONF_RW){
			DataOut[2] = ExtractData(DRVect, StreamPosition, API_DAC); 
			StreamPosition+= 48;
			DecodedElements[0] = DecodedElements[0] | 0x2;
        } else if(Instruction == API_CONF_R_ADC){
			DataOut[2] = ExtractData(DRVect, StreamPosition, API_ADC); 
			StreamPosition+= 170;
			DecodedElements[0] = DecodedElements[0] | 0x2;
		} else if(Instruction == IDCODE){
			DataOut[2] = ExtractData(DRVect, StreamPosition, ID_CODE); 
			StreamPosition+= 32;
			DecodedElements[0] = DecodedElements[0] | 0x2;
		}else {
          cerr << "No defined command IRFunctVect[1]" << endl;      
          return 0;
        }
    } 
          
	//////////// PIXELS ///////////////
    if (SkipMode != 1 && SkipMode != 3){                                 
      UInt32 ChipPresTemp = 0x400000;
	  for (i=9; i>=0; --i){
          if (ModeSel & ChipPresTemp){
		  Instruction = GetInstruction((UInt8)(i+2));
          if (Instruction == BYPASSPix){
			StreamPosition++;
		  }else if( Instruction == SelGLOBAL){
            DataOut[3 + i] = ExtractData(DRVect, StreamPosition, PIX_DAC); 
			StreamPosition+= 8;
			DecodedElements[0] = DecodedElements[0] | (0x1 << (i + 2));
          }
          else if( Instruction == SelTM){
            DataOut[3 + i] = ExtractData(DRVect, StreamPosition, PIX_COLUMN); 
			StreamPosition+= 256;
			DecodedElements[0] = DecodedElements[0] | (0x1 << (i + 2));      //each 8 UInt32 there is a column
          }   
          else {
            cerr << "No defined command IRFunctVect[1]" << endl;      
            return 0;
          }
        }  
        ChipPresTemp >>= 1;          
      }    
    } 
  
	/////////// DPI ///////////////
    Instruction = GetInstruction(12);  
    if (Instruction == BYPASS){
		StreamPosition++;
	}else if( Instruction == DPI_CONF_RW || Instruction == DPI_CONF_R){
          DataOut[13] = ExtractData(DRVect, StreamPosition, DPIConf); 
		  StreamPosition+= 49;
		  DecodedElements[0] = DecodedElements[0] | 0x1000;
	}else if( Instruction == DPI_INT_RW){
          DataOut[13] = ExtractData(DRVect, StreamPosition, DPIInternal); 
		  StreamPosition+= 65;
		  DecodedElements[0] = DecodedElements[0] | 0x1000;
	}else if(Instruction == IDCODE){
		  DataOut[13] = ExtractData(DRVect, StreamPosition, ID_CODE); 
		  StreamPosition+= 32;
		  DecodedElements[0] = DecodedElements[0] | 0x1000;   
	} else {
          cerr << "No defined command IRFunctVect[1]" << endl;      
          return 0;
    }
  
  return DataOut;
}

