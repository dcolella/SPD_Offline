# Ruby script to parse through the spd fed server code and generate reports
# about the lines of code to comments ratio

# Class to contain the comment report data
# its almost a struct, just added a operator overload
class CommentsReport
	attr_accessor :total_lines,:comments,:code_lines 
	
	def initialize (total, comments, code_lines)
		@total_lines = total
		@comments = comments
		@code_lines = code_lines
	end
	
	def  +(other)
		return CommentsReport.new( @total_lines+ other.total_lines ,@comments + other.comments , @code_lines+ other.code_lines)
		
	end
end

# Counts the number of comments, code lines and total number of lines in a file
def count_comments(filename)
	
	infile = File.open filename, "r" 
	line_number = 0
	report = CommentsReport.new(0,0,0)
	in_block_comment = false
	
	infile.each_line do |line|
	
		line_number += 1	
		
		if (in_block_comment == false)
# 			splits the line using a regular expression "//" or "/*" the escape character makes weirder			
			a = line.split /(\/\/|\/\*)/
# 			if there are non empty characters before the any eventual comment 
			if a[0] =~/\S+/
				report.code_lines +=1 
			end
# 			if there is something after the comment line 
			if (a.size > 2 )
				report.comments +=1 
			end
# 			if the block comment is in the line, but the close comment is not 
			if (a[1] == "/*" and a.fetch(2, "").include?("*/") ==false)
				in_block_comment = true 
			end	
			
		else
		
			if (line.include?('*/'))
				in_block_comment = false
				report.comments +=1 
			else
				report.comments +=1
			end
		end
	end
	
	infile.close
	report.total_lines = report.comments+ report.code_lines
	return report
end


# creates a report of all files in one folder
def comment_report_path(path)

	Dir.chdir(path)
	
	file_list =  Dir.glob "*.h"
	file_list += Dir.glob "*.cpp"
	
	outfile = File.open "comment_report.txt" , "w"
	total = CommentsReport.new(0,0,0)
	file_list.each do |file|
	
	 	report = count_comments(file)
	 	outfile.print "report file #{file}\n"
		outfile.print "\t total = #{report.total_lines}, "
		outfile.print "comments = #{report.comments}, "
		outfile.print "code lines = #{report.code_lines}\n"
		
		outfile.print "\t comment ratio = #{ "%0.2f" % (100*report.comments.to_f/report.total_lines.to_f)} %\n"
		
		total += report
	end
	
 	outfile.print "total report \n"
	outfile.print "\t total = #{total.total_lines}, "
	outfile.print "comments = #{total.comments}, "
	outfile.print "code lines = #{total.code_lines}\n"
	
	outfile.print "\t comment ratio = #{ "%0.2f" % (100*total.comments.to_f/total.total_lines.to_f)} %\n"
	outfile.close
	return total
	
end

# puts count_comments ( filename)


# path = "F:/projectos/SpdFEDServerPackage/SpdFEDServerRelease/"

# project_solutions = ["AddressGenerator",
# 						"ALICE_SPD_DIMServer",
# 						"BitManager",
# 						"HSConfig",
# 						"spdCalibParser",
# 						"spdDbConfLib",
# 						"SPD_FEDServer",
# 						"VMEAcc"]

 path = "F:/projectos/pixeltrigger/workspace/"

project_solutions = ["pixeltrigger/src","PitDbConfiguration/src"]						

project_solutions = project_solutions.collect {|dir| path+dir}


total = CommentsReport.new(0,0,0)
outfile = File.open path+"comment_report.txt" , "w"

project_solutions.each do |path|
	report = comment_report_path(path)
	
	outfile.print "report file #{path}\n"
	outfile.print "\t total = #{report.total_lines}, "
	outfile.print "comments = #{report.comments}, "
	outfile.print "code lines = #{report.code_lines}\n"
	
	outfile.print "\t comment ratio = #{ "%0.2f" % (100*report.comments.to_f/report.total_lines.to_f)} %\n"
	
	total += report
end


outfile.print "total report \n"
outfile.print "\t total = #{total.total_lines}, "
outfile.print "comments = #{total.comments}, "
outfile.print "code lines = #{total.code_lines}\n"

outfile.print "\t comment ratio = #{ "%0.2f" % (100*total.comments.to_f/total.total_lines.to_f)} %\n"

outfile.close