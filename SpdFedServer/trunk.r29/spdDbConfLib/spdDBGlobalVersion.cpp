#include "StdAfx.h"
#include ".\spddbglobalversion.h"


// constructor setting the global table settings
spdDBGlobalVersion::spdDBGlobalVersion(void)
{

	topTable.setTableSettings("SPDCONF_VER2","SPDCONF_VER",getSpdVerFields());
	runTypeTable.setTableSettings("RUNTYPE2", "RUNTYPE",getRunTypeFields());
}

spdDBGlobalVersion::~spdDBGlobalVersion(void)
{
}


// will get the version fields names constants to set the spdVerTable object
std::vector<std::string> spdDBGlobalVersion::getSpdVerFields(){
	std::vector<std::string> fields(5);

	fields[0] = "DET_SIDEA_VER";
	fields[1] = "DET_SIDEC_VER";
	fields[2] = "READELECT_SIDEA_VER";
	fields[3] = "READELECT_SIDEC_VER";
	fields[4] = "RUNTYPE";

	return fields;
}

std::vector<std::string> spdDBGlobalVersion::getRunTypeFields(){
	std::vector<std::string> fields(1);

	fields[0] = "RUNNAME";
	return fields;
}


// updates the version in the database with the internal settings
long int spdDBGlobalVersion::update(){
	std::vector<long int> values(5);

	values[0]= detSideA_Ver;
	values[1]= detSideC_Ver;
	values[2]= routSideA_Ver;
	values[3]= routSideC_Ver;
	values[4]= runType;

	runTypeTable.setVersionNumber(runType);
	std::vector<std::string> runTypeValues(1,"");

	runTypeTable.getValues(runTypeValues);
	runTypeName=runTypeValues[0];

	return topTable.update( values);
	

}



// set the internal setting and updates a new database version
long int spdDBGlobalVersion::update( long int hsSideA, 
								long int hsSideC, 
								long int routerSideA,
								long int routerSideC,
								long int typeOfRun){

	detSideA_Ver= hsSideA; 
	detSideC_Ver= hsSideC; 
	routSideA_Ver= routerSideA;
	routSideC_Ver= routerSideC;
	runType=typeOfRun;

	return this->update();
}


// will set a new version number and get the data from the database
void spdDBGlobalVersion::setVersionNumber( long version){
	
	if (version < 0){
		int maxVer= topTable.getMaxVersionNumber();
		topTable.setVersionNumber( maxVer);
	}
	else{
		topTable.setVersionNumber( version);
	}

	std::vector<long int> values(5,0);
	topTable.getValues(values);

	detSideA_Ver  = values[0]; 
	detSideC_Ver  = values[1]; 
	routSideA_Ver = values[2];
	routSideC_Ver = values[3];
	runType		  = values[4];

			// will get the runtype name from the database
	runTypeTable.setVersionNumber(runType);
	std::vector<std::string> runTypeValues(1,"");

	runTypeTable.getValues(runTypeValues);
	runTypeName=runTypeValues[0];
}