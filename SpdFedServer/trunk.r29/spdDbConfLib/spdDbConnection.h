#ifndef SPDDBCONNECTION_H
#define SPDDBCONNECTION_H





#include "spdLogger.h"
#include <occi.h>
#include "spdIniParser.h"

using namespace oracle::occi; 
//using namespace std;

//!	class to manage the database connection	
/*!class to manage the database connection	
It is designed as a singleton so to be easier managed */
class spdDbConnection
{
protected:
	spdDbConnection(void);
	
private:
	//static spdDbConnection * pinstance;
	Environment *env;
	Connection *conn;

			// variables to store the database connections
	std::string conString;
	std::string user;
	std::string passwd;

public:

	static spdDbConnection *subscribe();
	int release();

	
	
	bool isConnected();

	int connect(const char *conString,const char *user,const char *passwd);
	int connect();

	void setDbSettings(const char *conString,const char *user,const char *passwd);
	void setDbSettings( spdCalib::Category &settings);

	int disconnect();

	int sendSqlCommand(std::string command);
	Statement * createStatement();

	int commit();
	int rollback();
	void terminateStatement (Statement *stmt);
	
	std::string getDbUserName(){return this->user;};
	std::string getDbConstring(){return this->conString;}


	~spdDbConnection(void);
};

#endif