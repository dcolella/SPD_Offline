#include "stdafx.h"
#include ".\spdLogger.h"
#include <ctime>
#include <stdio.h>
#include <stdarg.h>
#include <windows.h>


using namespace std;

spdLogger::spdLogger(void)
{
	logFile = "spd.log";
	logService = NULL;

	this->outfile.open( logFile.c_str(), std::ios::app);

	if ( this->outfile.is_open()== false){
		cout << "unable to open the file " << logFile.c_str() << endl;
	}

}

spdLogger::~spdLogger(void)
{
}

void spdLogger::setLogFile( std::string filename){ 

	
	logFile = filename;

	this->outfile.close();
	this->outfile.open( logFile.c_str(), std::ios::app);

	if ( this->outfile.is_open() == false){
		cout << "unable to open the file " << logFile.c_str() << endl;
	}
}


// logs one message, adds date 
int spdLogger::log(std::string msg){

	
	char time[50];
	formatTime( time);

	sprintf_s(logMsg, "%s::%s\n", time, msg.c_str());
	cout << logMsg;

	

	if ( this->outfile.is_open() == false){

		cout << "log file is not open " << logFile.c_str() << endl;
		return -1;
	}

	this->outfile << logMsg;
	this->outfile.flush();

	if (this->logService != NULL){
		this->logService->updateService(logMsg);
	}

	return 0;
}

int spdLogger::logToFile(std::string msg){

	
	char time[50];
	formatTime( time);

	sprintf_s(logMsg, "%s::%s\n", time, msg.c_str());
	cout << logMsg;

	if ( this->outfile.is_open() == false){
		cout << "log file is not open " << logFile.c_str() << endl;
		return -1;
	}

	this->outfile << logMsg;
	this->outfile.flush();


	return 0;
}

void spdLogger::formatTime(char * time){

	SYSTEMTIME  lt;
    GetLocalTime(&lt);

	sprintf(time, "%d/%d/%d %d:%d:%d,%03d", lt.wDay,
											lt.wMonth,
											lt.wYear,
											lt.wHour,
											lt.wMinute,
											lt.wSecond,
											lt.wMilliseconds);

    

}

// logs one message, adds date 
int spdLogger::log(const char * format, ...){

	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf_s(msg, format, va);

	sprintf_s(logMsg, "%s::%s\n", time, msg);
	cout<< logMsg;


	if ( this->outfile.is_open() ==false){
		cout << "log file is not open " << logFile.c_str() << endl;
		return -1;
	}

	this->outfile << logMsg;
	this->outfile.flush();

	if(this->logService != NULL){
		this->logService->updateService(logMsg);
	}

	
	return 0;
}

int spdLogger::logToFile(const char * format, ...){

	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf_s(msg, format, va);

	sprintf_s(logMsg, "%s::%s\n", time, msg);
	cout<< logMsg;



	if ( this->outfile.is_open() == false){

		cout << "unable to open the file " << logFile;
		return -1;
	}

	this->outfile << logMsg;
	this->outfile.flush();
	
	return 0;
}

// logs one message, adds date 
int spdLogger::logToScr(const char * format, ...){
	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf_s(msg, format, va);

	sprintf_s(logMsg, "%s::%s\n", time, msg);
	cout<< logMsg;


	return 0;
}


int spdLogger::logToDim(const char * format, ...){

	if (this->logService == NULL){
		return 1;
	}

	char time[50];
	formatTime( time);
	
	va_list va;
	va_start(va,format);
	char msg[500];

	vsprintf_s(msg, format, va);

	sprintf_s(logMsg, "%s::%s\n", time, msg);
	
	
	cout<< logMsg;

	this->logService->updateService(logMsg);
	
	return 0;


}

spdLogger & spdLogger::getInstance(){

	static spdLogger pinstance;
	return pinstance;

}

void spdLogger::setDimServiceName( const char * serviceName){
	if (logService != NULL){
		delete logService;
	}

	logService = new DimService( serviceName, "log service started");

	
}

void spdLogger::deleteLogFile(){
	this->outfile.close();
	
	char command[200];

	sprintf_s( command , "del %s \n", logFile.c_str());
	system ( command);

	this->outfile.open( logFile.c_str(), std::ios::app);

	if ( this->outfile.is_open() == false){
		cout << "unable to open the file " << logFile.c_str() << endl;
	}
}


void spdLogger::restartLogFile(){

	static int file_counter(0);
	
	
	SYSTEMTIME  lt;
    GetLocalTime(&lt);

	// generates the new filename
	char filename[200];
	sprintf_s(filename, "%s.%d.%d_%d_%d.log", logFile.c_str(),
											file_counter,
											lt.wDay,
											lt.wMonth,
											lt.wYear);

	file_counter++;
	this->outfile.close();

	char command[500];
	sprintf_s( command , "rename %s  %s\n", logFile.c_str(), filename);
	system ( command);

	this->outfile.open( logFile.c_str(), std::ios::app);

	if ( this->outfile.is_open() == false){
		cout << "unable to open the file " << logFile.c_str() << endl;
	}

}